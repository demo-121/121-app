import React, { PureComponent } from "react";
import { View, ScrollView, Text, Alert } from "react-native";
import {
  TIME_FORMAT,
  REDUCER_TYPES,
  REDUCER_TYPE_CHECK,
  utilities
} from "eab-web-api";
import * as _ from "lodash";
import moment from "moment";
import PropTypes from "prop-types";
import TranslatedText from "../../../../containers/TranslatedText";
import icCompleted from "../../../../assets/images/icCompleted.png";
import icIncomplete from "../../../../assets/images/icIncomplete.png";
import APP_REDUCER_TYPE_CHECK from "../../../../constants/REDUCER_TYPE_CHECK";
import { TEXT_STORE } from "../../../../constants/REDUCER_TYPES";
import EABSegmentedControl from "../../../../components/EABSegmentedControl";
import EABTextField from "../../../../components/EABTextField";
import EABButton from "../../../../components/EABButton";
import { RECTANGLE_BUTTON_1 } from "../../../../components/EABButton/constants";
import SelectorField from "../../../../components/SelectorField";
import EABTextSelection from "../../../../components/EABTextSelection";
import {
  FLAT_LIST,
  DATE_PICKER,
  SEARCH_BOX
} from "../../../../components/SelectorField/constants";
import Theme from "../../../../theme";
import EABi18n from "../../../../utilities/EABi18n";
import getPostalCodeFileName from "../../../../utilities/getPostalCodeFileName";
import { ContextConsumer } from "../../../../context";
import { languageTypeConvert } from "../../../../utilities/getEABTextByLangType";
import {
  getOptionValue,
  getOptionList
} from "../../../../utilities/getOptionList";
import styles from "./styles";

const { getCurrency } = utilities.common;
const { common } = utilities;
const { getOptionTitle } = common;

/**
 * ApplicationFormPersonalDetails
 * @description ApplicationFormPersonalDetails page.
 * @param {object} textStore - reducer textStore state
 * */

class ApplicationFormPersonalDetails extends PureComponent {
  constructor(props) {
    super(props);
    this.scrollView = React.createRef();
    this.state = {
      selectedProfile: "proposer"
    };
    this.genProfileButtonCompleteIcon = this.genProfileButtonCompleteIcon.bind(
      this
    );
  }

  componentDidUpdate(prevProps, prevState) {
    if (
      prevState.selectedProfile !== this.state.selectedProfile ||
      prevProps.selectedSectionKey !== this.props.selectedSectionKey
    ) {
      this.scrollView.current.scrollTo({ x: 0, y: 0, animated: false });
    }
  }

  get isDisabled() {
    const { application } = this.props;
    return application.isStartSignature;
  }

  get isShield() {
    const { application } = this.props;
    return application.quotation.quotType === "SHIELD";
  }

  getTargetProfileDetails() {
    const { application } = this.props;
    const { selectedProfile } = this.state;
    if (typeof selectedProfile === "number") {
      return _.get(
        application,
        `applicationForm.values.insured[${selectedProfile}].personalInfo`
      );
    }
    return _.get(
      application,
      `applicationForm.values.${selectedProfile}.personalInfo`
    );
  }

  getSingPostBranchDetails() {
    const { application, textStore, error, optionsMap } = this.props;
    const { selectedProfile } = this.state;
    if (_.isEmpty(application) || typeof selectedProfile === "number") {
      return null;
    }
    const details = this.getTargetProfileDetails();
    if (
      application.applicationForm.values[selectedProfile].extra.channelName !==
      "SINGPOST"
    ) {
      return null;
    }
    const branchInfo = details.branchInfo
      ? Object.assign(
          {},
          {
            branch: "",
            bankRefId: ""
          },
          details.branchInfo
        )
      : {
          branch: "",
          bankRefId: ""
        };
    return (
      <ContextConsumer>
        {({ language }) => (
          <View style={styles.singpostBranchDetailsView}>
            <TranslatedText
              style={styles.detailsOfProposerTitle}
              path="application.form.section.singpostBranchDetailsTitle"
            />
            <View style={styles.singpostBranchDetailsTextView}>
              <SelectorField
                editable={!this.isDisabled}
                isRequired
                style={styles.branchSelector}
                value={branchInfo.branch}
                placeholder={EABi18n({
                  language,
                  textStore,
                  path: "application.form.section.singpostBranchDetails.branch"
                })}
                isError={_.get(
                  error,
                  `application.${selectedProfile}.personalInfo.branch.hasError`,
                  false
                )}
                hintText={_.get(
                  error,
                  `application.${selectedProfile}.personalInfo.branch.message.${language}`,
                  ""
                )}
                selectorType={SEARCH_BOX}
                options={getOptionList({
                  optionMap: optionsMap.branches,
                  language
                })}
                onChange={option => {
                  if (option) {
                    this.updateValue({
                      path: `applicationForm.values.${selectedProfile}.personalInfo.branchInfo.branch`,
                      newValue: option.value,
                      validateObj: {
                        field: `${selectedProfile}.personalInfo.branch`,
                        type: "string",
                        mandatory: true,
                        value: option.value
                      }
                    });
                  }
                }}
              />
              <EABTextField
                editable={!this.isDisabled}
                style={styles.branchText}
                value={branchInfo.bankRefId}
                placeholder={EABi18n({
                  language,
                  textStore,
                  path:
                    "application.form.section.singpostBranchDetails.referrerID"
                })}
                isError={_.get(
                  error,
                  `application.${selectedProfile}.personalInfo.bankRefId.hasError`,
                  false
                )}
                hintText={_.get(
                  error,
                  `application.${selectedProfile}.personalInfo.bankRefId.message.${language}`,
                  ""
                )}
                onChange={newValue => {
                  this.updateValue({
                    path: `applicationForm.values.${selectedProfile}.personalInfo.branchInfo.bankRefId`,
                    newValue: newValue.replace(/[^0-9]+/g, ""),
                    validateObj: {
                      field: `${selectedProfile}.personalInfo.bankRefId`,
                      type: "string",
                      mandatory: true,
                      value: newValue.replace(/[^0-9]+/g, "")
                    }
                  });
                }}
                isRequired
                maxLength={4}
              />
            </View>
          </View>
        )}
      </ContextConsumer>
    );
  }

  getDetailsOfTargetProfile(language) {
    const { application, textStore, optionsMap } = this.props;
    if (_.isEmpty(application)) {
      return null;
    }
    const { selectedProfile } = this.state;
    const isShield = application.quotation.quotType === "SHIELD";
    const details = this.getTargetProfileDetails();
    if (details) {
      let isProposerLA = false;
      if (isShield) {
        if (
          application.iCids.includes(application.pCid) ||
          _.get(
            application,
            "applicationForm.values.proposer.extra.isPhSameAsLa"
          ) === "Y"
        ) {
          isProposerLA = true;
        }
      }

      return (
        <View style={styles.personalDetailOfProposerView}>
          {details.cid === application.pCid ? (
            <Text style={styles.detailsOfProposerTitle}>
              {EABi18n({
                path: "application.form.section.detailsOfProposerTitle",
                language,
                textStore
              })}
            </Text>
          ) : (
            <Text style={styles.detailsOfProposerTitle}>
              {EABi18n({
                path:
                  "application.form.section.detailsOfProposerALifeAssuredTitle",
                language,
                textStore
              })}
            </Text>
          )}
          {selectedProfile === "proposer" ? (
            <View style={styles.detailsOfProposerFieldView}>
              <Text style={[styles.detailsOfProposerFieldTitle, styles.bold]}>
                {EABi18n({
                  path:
                    "application.form.section.detailsOfProposerFieldProposerIsALifeAssured",
                  language,
                  textStore
                })}
              </Text>
              <Text style={[styles.detailsOfProposerFieldText, styles.bold]}>
                {application.iCid === application.pCid || isProposerLA
                  ? `${EABi18n({
                      path: "application.form.section.proposerIsLifeAssured",
                      language,
                      textStore
                    })}`
                  : `${EABi18n({
                      path: "application.form.section.proposerIsNotLifeAssured",
                      language,
                      textStore
                    })}`}
              </Text>
            </View>
          ) : null}
          {details.relationship ? (
            <View style={styles.detailsOfProposerFieldView}>
              <Text style={styles.detailsOfProposerFieldTitle}>
                {EABi18n({
                  path:
                    "application.form.section.detailsOfProposerFieldRelationship",
                  language,
                  textStore
                })}
              </Text>
              <Text style={styles.detailsOfProposerFieldText}>
                {details.relationship !== "OTH"
                  ? getOptionValue({
                      optionMap: optionsMap.relationship,
                      language,
                      value: details.relationship
                    }) || "-"
                  : details.relationshipOther || "-"}
              </Text>
            </View>
          ) : null}
          <View style={styles.detailsOfProposerFieldView}>
            <Text style={styles.detailsOfProposerFieldTitle}>
              {EABi18n({
                path: "application.form.section.detailsOfProposerFieldSurname",
                language,
                textStore
              })}
            </Text>
            <Text style={styles.detailsOfProposerFieldText}>
              {details.lastName || "-"}
            </Text>
          </View>
          <View style={styles.detailsOfProposerFieldView}>
            <Text style={styles.detailsOfProposerFieldTitle}>
              {EABi18n({
                path:
                  "application.form.section.detailsOfProposerFieldGivenName",
                language,
                textStore
              })}
            </Text>
            <Text style={styles.detailsOfProposerFieldText}>
              {details.firstName || "-"}
            </Text>
          </View>
          <View style={styles.detailsOfProposerFieldView}>
            <Text style={styles.detailsOfProposerFieldTitle}>
              {EABi18n({
                path:
                  "application.form.section.detailsOfProposerFieldEnglishOrOtherName",
                language,
                textStore
              })}
            </Text>
            <Text style={styles.detailsOfProposerFieldText}>
              {details.othName || "-"}
            </Text>
          </View>
          <View style={styles.detailsOfProposerFieldView}>
            <Text style={styles.detailsOfProposerFieldTitle}>
              {EABi18n({
                path:
                  "application.form.section.detailsOfProposerFieldHanYuName",
                language,
                textStore
              })}
            </Text>
            <Text style={styles.detailsOfProposerFieldText}>
              {details.hanyuPinyinName || "-"}
            </Text>
          </View>
          <View style={styles.detailsOfProposerFieldView}>
            <Text style={styles.detailsOfProposerFieldTitle}>
              {EABi18n({
                path: "application.form.section.detailsOfProposerFieldName",
                language,
                textStore
              })}
            </Text>
            <Text style={styles.detailsOfProposerFieldText}>
              {details.fullName || "-"}
            </Text>
          </View>
          <View style={styles.detailsOfProposerFieldView}>
            <Text style={styles.detailsOfProposerFieldTitle}>
              {EABi18n({
                path:
                  "application.form.section.detailsOfProposerFieldNationality",
                language,
                textStore
              })}
            </Text>
            <Text style={styles.detailsOfProposerFieldText}>
              {getOptionValue({
                optionMap: optionsMap.nationality,
                language,
                value: details.nationality
              })}
            </Text>
          </View>
          {details.prStatus && details.nationality !== "N1" ? (
            <View style={styles.detailsOfProposerFieldView}>
              <Text style={styles.detailsOfProposerFieldTitle}>
                {EABi18n({
                  path: "application.form.section.prStatus",
                  language,
                  textStore
                })}
              </Text>
              <Text style={styles.detailsOfProposerFieldText}>
                {details.prStatus === "Y"
                  ? EABi18n({
                      path: "cka.second.question.yes",
                      language,
                      textStore
                    })
                  : EABi18n({
                      path: "cka.second.question.no",
                      language,
                      textStore
                    })}
              </Text>
            </View>
          ) : null}
          {details.idDocType && details.idDocType !== "other" ? (
            <View style={styles.detailsOfProposerFieldView}>
              <Text style={styles.detailsOfProposerFieldTitle}>
                {EABi18n({
                  path:
                    "application.form.section.detailsOfProposerFieldIDDocumentType",
                  language,
                  textStore
                })}
              </Text>
              <Text style={styles.detailsOfProposerFieldText}>
                {getOptionValue({
                  optionMap: optionsMap.IDType,
                  language,
                  value: details.idDocType
                })}
              </Text>
            </View>
          ) : (
            <View style={styles.detailsOfProposerFieldView}>
              <Text style={styles.detailsOfProposerFieldTitle}>
                {EABi18n({
                  path:
                    "application.form.section.detailsOfProposerFieldIDTypeName",
                  language,
                  textStore
                })}
              </Text>
              <Text style={styles.detailsOfProposerFieldText}>
                {details.idDocTypeOther}
              </Text>
            </View>
          )}
          <View style={styles.detailsOfProposerFieldView}>
            <Text style={styles.detailsOfProposerFieldTitle}>
              {EABi18n({
                path: "application.form.section.detailsOfProposerFieldIDNumber",
                language,
                textStore
              })}
            </Text>
            <Text style={styles.detailsOfProposerFieldText}>
              {details.idCardNo || "-"}
            </Text>
          </View>
          <View style={styles.detailsOfProposerFieldView}>
            <Text style={styles.detailsOfProposerFieldTitle}>
              {EABi18n({
                path: "application.form.section.detailsOfProposerFieldGender",
                language,
                textStore
              })}
            </Text>
            <Text style={styles.detailsOfProposerFieldText}>
              {getOptionValue({
                optionMap: optionsMap.gender,
                language,
                value: details.gender
              })}
            </Text>
          </View>
          <View style={styles.detailsOfProposerFieldView}>
            <Text style={styles.detailsOfProposerFieldTitle}>
              {EABi18n({
                path:
                  "application.form.section.detailsOfProposerFieldDateOfBirth",
                language,
                textStore
              })}
            </Text>
            <Text style={styles.detailsOfProposerFieldText}>
              {moment(details.dob).format(TIME_FORMAT.DATE_TIME_FORMAT_3)}
            </Text>
          </View>
          <View style={styles.detailsOfProposerFieldView}>
            <Text style={styles.detailsOfProposerFieldTitle}>
              {EABi18n({
                path:
                  "application.form.section.detailsOfProposerFieldMaritalStatus",
                language,
                textStore
              })}
            </Text>
            <Text style={styles.detailsOfProposerFieldText}>
              {getOptionValue({
                optionMap: optionsMap.marital,
                language,
                value: details.marital
              })}
            </Text>
          </View>
          {details.cid === application.pCid ? (
            <View style={styles.detailsOfProposerFieldView}>
              <Text style={styles.detailsOfProposerFieldTitle}>
                {EABi18n({
                  path: "application.form.section.detailsOfProposerFieldEmail",
                  language,
                  textStore
                })}
              </Text>
              <Text style={styles.detailsOfProposerFieldText}>
                {details.email}
              </Text>
            </View>
          ) : null}
          {!isShield || details.cid === application.pCid ? (
            <View style={styles.detailsOfProposerFieldView}>
              <Text style={styles.detailsOfProposerFieldTitle}>
                {EABi18n({
                  path: "application.form.section.detailsOfProposerFieldMobile",
                  language,
                  textStore
                })}
              </Text>
              <Text style={styles.detailsOfProposerFieldText}>{`${
                details.mobileCountryCode
              } ${details.mobileNo}`}</Text>
            </View>
          ) : null}
        </View>
      );
    }
    return null;
  }

  getProfessionalDetails(language) {
    const { application, textStore, optionsMap, error } = this.props;
    const { selectedProfile } = this.state;
    const details = this.getTargetProfileDetails();
    const targetProfile = _.isNumber(selectedProfile)
      ? `insured[${selectedProfile}]`
      : selectedProfile;
    if (details) {
      const isShield = application.quotation.quotType === "SHIELD";
      return (
        <View style={styles.sectionView}>
          <Text
            testID="Application__Appform__lblProfessionalDetail"
            style={styles.sectionTitle}
          >
            {EABi18n({
              path: "application.form.section.professionalDetailsTitle",
              language,
              textStore
            })}
          </Text>
          <View style={styles.inputContentView}>
            <EABTextField
              testID="Application__Appform__txtNameOfEmployer"
              style={styles.inputShort}
              value={details.organization || ""}
              isRequired
              editable={!this.isDisabled}
              placeholder={EABi18n({
                language,
                textStore,
                path:
                  "application.form.section.professionalDetailsFieldNameofEmployer"
              })}
              isError={_.get(
                error,
                `application.${targetProfile}.personalInfo.organization.hasError`,
                false
              )}
              hintText={_.get(
                error,
                `application.${targetProfile}.personalInfo.organization.message.${language}`,
                ""
              )}
              maxLength={50}
              onChange={newValue => {
                this.updateValue({
                  path: `applicationForm.values.${targetProfile}.personalInfo.organization`,
                  newValue,
                  validateObj: {
                    field: `${targetProfile}.personalInfo.organization`,
                    type: "string",
                    mandatory: true,
                    value: newValue
                  }
                });
              }}
            />
            {!isShield ? (
              <SelectorField
                testID="Application__Appform__searchBoxCountryOfEmployer"
                style={styles.inputShort}
                value={details.organizationCountry || ""}
                editable={!this.isDisabled}
                isRequired
                placeholder={EABi18n({
                  language,
                  textStore,
                  path:
                    "application.form.section.professionalDetailsFieldCountryofEmployer"
                })}
                isError={_.get(
                  error,
                  `application.${targetProfile}.personalInfo.organizationCountry.hasError`,
                  false
                )}
                hintText={_.get(
                  error,
                  `application.${targetProfile}.personalInfo.organizationCountry.message.${language}`,
                  ""
                )}
                selectorType={SEARCH_BOX}
                options={getOptionList({
                  optionMap: optionsMap.residency,
                  language
                })}
                onChange={option => {
                  if (option) {
                    this.updateValue({
                      path: `applicationForm.values.${targetProfile}.personalInfo.organizationCountry`,
                      newValue: option.value,
                      validateObj: {
                        field: `${targetProfile}.personalInfo.organizationCountry`,
                        type: "string",
                        mandatory: true,
                        value: option.value
                      }
                    });
                  }
                }}
              />
            ) : null}
            {details.nationality !== "N1" && details.prStatus === "N" ? (
              <EABTextField
                testID="Application__Appform__txtTypeOfPass"
                style={styles.inputShort}
                value={getOptionTitle({
                  value: details.pass,
                  optionsMap: optionsMap.pass,
                  language: languageTypeConvert(language)
                })}
                editable={false}
                placeholder={EABi18n({
                  language,
                  textStore,
                  path: "application.form.section.typeOfPass"
                })}
              />
            ) : null}
            {details.pass === "o" ? (
              <EABTextField
                testID="Application__Appform__txtOtherPass"
                style={styles.inputShort}
                value={details.passOther}
                editable={false}
                placeholder={EABi18n({
                  language,
                  textStore,
                  path: "application.form.section.typeOfPassOther"
                })}
              />
            ) : null}
            {details.nationality !== "N1" && details.prStatus === "N" ? (
              <SelectorField
                testID="Application__Appform__dateExpireDate"
                style={styles.inputShort}
                value={details.passExpDate}
                placeholder={EABi18n({
                  language,
                  textStore,
                  path: "clientForm.passExpiryDateField"
                })}
                selectorType={DATE_PICKER}
                datePickerIOSOptions={{
                  mode: "date",
                  date:
                    !details.passExpDate || details.passExpDate === ""
                      ? new Date()
                      : new Date(details.passExpDate),
                  onDateChange: value => {
                    if (value) {
                      const newValue = moment(value).valueOf();
                      this.updateValue({
                        path: `applicationForm.values.${targetProfile}.personalInfo.passExpDate`,
                        newValue,
                        validateObj: {
                          field: `${targetProfile}.personalInfo.passExpDate`,
                          type: "string",
                          mandatory: true,
                          value: newValue
                        }
                      });
                    }
                  },
                  minimumDate: new Date()
                }}
                isError={_.get(
                  error,
                  `application.${targetProfile}.personalInfo.passExpDate.hasError`,
                  false
                )}
                hintText={_.get(
                  error,
                  `application.${targetProfile}.personalInfo.passExpDate.message.${language}`,
                  ""
                )}
                isRequired
              />
            ) : null}
            <EABTextField
              testID="Application__Appform__txtIndustry"
              style={styles.inputShort}
              value={getOptionValue({
                optionMap: optionsMap.industry,
                language,
                value: details.industry
              })}
              editable={false}
              placeholder={EABi18n({
                language,
                textStore,
                path:
                  "application.form.section.professionalDetailsFieldIndustry"
              })}
            />
            <EABTextField
              testID="Application__Appform__txtOccupation"
              style={
                details.occupation === "O921"
                  ? styles.inputContentView
                  : styles.inputShort
              }
              value={getOptionValue({
                optionMap: optionsMap.occupation,
                language,
                value: details.occupation
              })}
              editable={false}
              placeholder={EABi18n({
                language,
                textStore,
                path:
                  "application.form.section.professionalDetailsFieldOccupation"
              })}
            />
            {details.occupation === "O921" ? (
              <EABTextField
                testID="Application__Appform__txtOtherOccupation"
                style={styles.inputContentView}
                value={details.occupationOther || " "}
                editable={false}
                placeholder={EABi18n({
                  language,
                  textStore,
                  path:
                    "application.form.section.professionalDetailsFieldOccupationOther"
                })}
              />
            ) : null}
            <EABTextField
              testID="Application__Appform__txtMonthlyIncome"
              style={styles.inputShort}
              prefix="S$"
              value={getCurrency({
                value: details.allowance,
                sign: "",
                decimals: 0
              })}
              editable={false}
              placeholder={EABi18n({
                language,
                textStore,
                path:
                  "application.form.section.professionalDetailsFieldMonthlyIncome"
              })}
            />
          </View>
        </View>
      );
    }
    return null;
  }

  getResidentialAddress(language) {
    const {
      application,
      textStore,
      optionsMap,
      error,
      updateAddress
    } = this.props;
    const { selectedProfile } = this.state;
    if (
      application.quotation.quotType === "SHIELD" &&
      _.isNumber(selectedProfile)
    ) {
      return null;
    }
    const details = this.getTargetProfileDetails();
    const targetProfile = _.isNumber(selectedProfile)
      ? `insured[${selectedProfile}]`
      : selectedProfile;
    if (details) {
      return (
        <View style={styles.sectionView}>
          <View style={styles.sectionTitleGroup}>
            <Text style={styles.sectionResidentialTitle}>
              {EABi18n({
                path: "application.form.section.residentialAddressTitle",
                language,
                textStore
              })}
            </Text>
            {details.cid !== application.pCid ? (
              <EABButton
                testID="Application__Appform__btnPopulateResodentialAddress"
                buttonType={RECTANGLE_BUTTON_1}
                style={styles.titleBarPopulateButton}
                onPress={() => {
                  this.populateResidentialAddress();
                }}
              >
                <TranslatedText
                  style={styles.textButtonLabelNormalEmphasizedAccent}
                  path="application.form.section.residentialAddressPopulateResidentialAddress"
                />
              </EABButton>
            ) : null}
          </View>
          <View style={styles.inputContentView}>
            <EABTextField
              testID="Application__Appform__txtResidentCountry"
              style={styles.inputShort}
              value={getOptionValue({
                optionMap: optionsMap.residency,
                language,
                value: details.addrCountry
              })}
              placeholder={EABi18n({
                language,
                textStore,
                path: "application.form.section.residentialAddressFieldCountry"
              })}
              editable={false}
            />
            <EABTextField
              testID="Application__Appform__txtResidentPostalCode"
              style={styles.inputMarginLeft}
              value={details.postalCode}
              keyboardType="numeric"
              isRequired
              editable={!this.isDisabled}
              placeholder={EABi18n({
                language,
                textStore,
                path:
                  "application.form.section.residentialAddressFieldPostalCode"
              })}
              maxLength={10}
              isError={_.get(
                error,
                `application.${targetProfile}.personalInfo.postalCode.hasError`,
                false
              )}
              hintText={_.get(
                error,
                `application.${targetProfile}.personalInfo.postalCode.message.${language}`,
                ""
              )}
              onChange={newValue => {
                this.updateValue({
                  path: `applicationForm.values.${targetProfile}.personalInfo.postalCode`,
                  newValue,
                  validateObj: {
                    field: `${targetProfile}.personalInfo.postalCode`,
                    type: "int",
                    mandatory: true,
                    value: newValue
                  }
                });
                // only get postal code when Country = "Singapore" (Residential Address)
                if (details.addrCountry === "R2") {
                  const fileName = getPostalCodeFileName(newValue);
                  if (fileName) {
                    updateAddress({
                      fileName,
                      postalCode: newValue,
                      targetProfile,
                      fieldType: "residentialAddress",
                      callback: () => {
                        Alert.alert(
                          EABi18n({
                            language,
                            textStore,
                            path: "clientForm.error.postalCodeNotFound"
                          })
                        );
                      }
                    });
                  }
                }
              }}
            />
          </View>
          <View style={styles.inputContentView}>
            <EABTextField
              testID="Application__Appform__txtResidentBlock"
              style={styles.inputShort}
              value={details.addrBlock}
              keyboardType="numeric"
              maxLength={12}
              isRequired
              editable={!this.isDisabled}
              placeholder={EABi18n({
                language,
                textStore,
                path: "application.form.section.residentialAddressFieldBlock"
              })}
              isError={_.get(
                error,
                `application.${targetProfile}.personalInfo.addrBlock.hasError`,
                false
              )}
              hintText={_.get(
                error,
                `application.${targetProfile}.personalInfo.addrBlock.message.${language}`,
                ""
              )}
              onChange={newValue => {
                this.updateValue({
                  path: `applicationForm.values.${targetProfile}.personalInfo.addrBlock`,
                  newValue,
                  validateObj: {
                    field: `${targetProfile}.personalInfo.addrBlock`,
                    type: "int",
                    mandatory: true,
                    value: newValue
                  }
                });
              }}
            />
            <EABTextField
              testID="Application__Appform__txtResidentStreet"
              style={styles.inputMarginLeft}
              value={details.addrStreet}
              placeholder={EABi18n({
                language,
                textStore,
                path: "application.form.section.residentialAddressFieldStreet"
              })}
              isRequired
              editable={!this.isDisabled}
              maxLength={24}
              isError={_.get(
                error,
                `application.${targetProfile}.personalInfo.addrStreet.hasError`,
                false
              )}
              hintText={_.get(
                error,
                `application.${targetProfile}.personalInfo.addrStreet.message.${language}`,
                ""
              )}
              onChange={newValue => {
                this.updateValue({
                  path: `applicationForm.values.${targetProfile}.personalInfo.addrStreet`,
                  newValue,
                  validateObj: {
                    field: `${targetProfile}.personalInfo.addrStreet`,
                    type: "string",
                    mandatory: true,
                    value: newValue
                  }
                });
              }}
            />
          </View>
          <View style={styles.inputContentView}>
            <EABTextField
              testID="Application__Appform__txtResidentUnit"
              style={styles.inputShort}
              value={details.unitNum || ""}
              editable={!this.isDisabled}
              placeholder={EABi18n({
                language,
                textStore,
                path:
                  "application.form.section.residentialAddressFieldUnitNumber"
              })}
              maxLength={
                (details.unitNum || "").toLowerCase().includes("unit") ? 13 : 8
              }
              onChange={newValue => {
                if (newValue.length === 0) {
                  this.isUnitEmpty = true;
                } else {
                  if (this.isUnitEmpty || this.isUnitEmpty === undefined) {
                    newValue = `Unit ${newValue}`;
                  }
                  this.isUnitEmpty = false;
                }
                this.updateValue({
                  path: `applicationForm.values.${targetProfile}.personalInfo.unitNum`,
                  newValue,
                  validateObj: {
                    field: `${targetProfile}.personalInfo.unitNum`,
                    type: "string",
                    value: newValue
                  }
                });
              }}
            />
            <EABTextField
              testID="Application__Appform__txtResidentBuildingName"
              style={styles.inputMarginLeft}
              value={details.addrEstate || ""}
              editable={!this.isDisabled}
              placeholder={EABi18n({
                language,
                textStore,
                path:
                  "application.form.section.residentialAddressFieldBuiliding"
              })}
              maxLength={40}
              onChange={newValue => {
                this.updateValue({
                  path: `applicationForm.values.${targetProfile}.personalInfo.addrEstate`,
                  newValue,
                  validateObj: {
                    field: `${targetProfile}.personalInfo.addrEstate`,
                    type: "string",
                    value: newValue
                  }
                });
              }}
            />
          </View>
          {this.getCityFieldType(details, targetProfile, language)}
          {details.cid === application.pCid ? (
            <View style={styles.residentialAddressQuestion}>
              <Text>
                {EABi18n({
                  language,
                  textStore,
                  path: "application.form.section.residentialAddressQuestion"
                })}
                <Text style={Theme.headlineNegative}> *</Text>
              </Text>
              <EABTextSelection
                testID="Application__Appform__ResidentialSameMailing"
                style={styles.residentialAddressQuestionButton}
                editable={!this.isDisabled}
                options={[
                  {
                    key: "residentialAddress-yes",
                    testID: "Yes",
                    title: EABi18n({
                      path:
                        "application.form.section.residentialAddressQuestionYes",
                      language,
                      textStore
                    }),
                    isSelected: details.isSameAddr === "Y"
                  },
                  {
                    key: "residentialAddress-no",
                    testID: "No",
                    title: EABi18n({
                      path:
                        "application.form.section.residentialAddressQuestionNo",
                      language,
                      textStore
                    }),
                    isSelected: details.isSameAddr === "N"
                  }
                ]}
                isError={_.get(
                  error,
                  `application.${targetProfile}.personalInfo.isSameAddr.hasError`,
                  false
                )}
                hintText={_.get(
                  error,
                  `application.${targetProfile}.personalInfo.isSameAddr.message.${language}`,
                  ""
                )}
                onPress={option => {
                  const newValue =
                    option.key === "residentialAddress-yes" ? "Y" : "N";
                  this.updateValue({
                    path: `applicationForm.values.${targetProfile}.personalInfo.isSameAddr`,
                    newValue,
                    validateObj: {
                      field: `${targetProfile}.personalInfo.isSameAddr`,
                      type: "string",
                      mandatory: true,
                      value: newValue
                    }
                  });
                  if (newValue === "N") {
                    this.scrollView.current.scrollTo(1000);
                  }
                }}
              />
              {details.isSameAddr === "Y" || !details.isSameAddr ? (
                <View style={styles.footerHeight} />
              ) : null}
            </View>
          ) : null}
        </View>
      );
    }
    return null;
  }

  getMailingAddress(language) {
    const { textStore, optionsMap, error, updateAddress } = this.props;
    const { selectedProfile } = this.state;
    const details = this.getTargetProfileDetails();
    if (_.isNumber(selectedProfile)) {
      return null;
    }
    const targetProfile = _.isNumber(selectedProfile)
      ? `insured[${selectedProfile}]`
      : selectedProfile;
    if (details && details.isSameAddr === "N") {
      if (!details.mAddrCountry) {
        this.updateValue({
          path: `applicationForm.values.${targetProfile}.personalInfo.mAddrCountry`,
          newValue: "R2",
          validateObj: {
            field: `${targetProfile}.personalInfo.mAddrCountry`,
            type: "string",
            mandatory: true,
            value: "R2"
          }
        });
      }

      return (
        <View style={styles.sectionView}>
          <Text style={styles.sectionTitle}>
            {EABi18n({
              path: "application.form.section.mailingAddressTitle",
              language,
              textStore
            })}
          </Text>
          <View style={styles.inputContentView}>
            <SelectorField
              testID="Application__Appform__searchBoxMailingCountry"
              isRequired
              style={styles.inputShort}
              value={details.mAddrCountry || ""}
              editable={!this.isDisabled}
              placeholder={EABi18n({
                language,
                textStore,
                path: "application.form.section.mailingAddressFieldCountry"
              })}
              isError={_.get(
                error,
                `application.${targetProfile}.personalInfo.mAddrCountry.hasError`,
                false
              )}
              hintText={_.get(
                error,
                `application.${targetProfile}.personalInfo.mAddrCountry.message.${language}`,
                ""
              )}
              selectorType={SEARCH_BOX}
              options={getOptionList({
                optionMap: optionsMap.residency,
                language
              })}
              onChange={option => {
                if (option) {
                  this.updateValue({
                    path: `applicationForm.values.${targetProfile}.personalInfo.mAddrCountry`,
                    newValue: option.value,
                    validateObj: {
                      field: `${targetProfile}.personalInfo.mAddrCountry`,
                      type: "string",
                      mandatory: true,
                      value: option.value
                    }
                  });
                }
              }}
            />
            <EABTextField
              testID="Application__Appform__txtMailingPostalCode"
              style={styles.inputMarginLeft}
              value={details.mPostalCode || ""}
              isRequired
              keyboardType="numeric"
              editable={!this.isDisabled}
              placeholder={EABi18n({
                language,
                textStore,
                path: "application.form.section.mailingAddressFieldPostalCode"
              })}
              maxLength={10}
              isError={_.get(
                error,
                `application.${targetProfile}.personalInfo.mPostalCode.hasError`,
                false
              )}
              hintText={_.get(
                error,
                `application.${targetProfile}.personalInfo.mPostalCode.message.${language}`,
                ""
              )}
              onChange={newValue => {
                this.updateValue({
                  path: `applicationForm.values.${targetProfile}.personalInfo.mPostalCode`,
                  newValue,
                  validateObj: {
                    field: `${targetProfile}.personalInfo.mPostalCode`,
                    type: "int",
                    mandatory: true,
                    value: newValue
                  }
                });
                // only get postal code when Country = "Singapore" (Mailing Address)
                if (details.mAddrCountry === "R2") {
                  const fileName = getPostalCodeFileName(newValue);
                  if (fileName) {
                    updateAddress({
                      fileName,
                      postalCode: newValue,
                      targetProfile,
                      fieldType: "mailingAddress",
                      callback: () => {
                        Alert.alert(
                          EABi18n({
                            language,
                            textStore,
                            path: "clientForm.error.postalCodeNotFound"
                          })
                        );
                      }
                    });
                  }
                }
              }}
            />
          </View>
          <View style={styles.inputContentView}>
            <EABTextField
              testID="Application__Appform__txtMailingBlock"
              style={styles.inputShort}
              value={details.mAddrBlock || ""}
              isRequired
              keyboardType="numeric"
              editable={!this.isDisabled}
              placeholder={EABi18n({
                language,
                textStore,
                path: "application.form.section.mailingAddressFieldBlock"
              })}
              maxLength={12}
              isError={_.get(
                error,
                `application.${targetProfile}.personalInfo.mAddrBlock.hasError`,
                false
              )}
              hintText={_.get(
                error,
                `application.${targetProfile}.personalInfo.mAddrBlock.message.${language}`,
                ""
              )}
              onChange={newValue => {
                this.updateValue({
                  path: `applicationForm.values.${targetProfile}.personalInfo.mAddrBlock`,
                  newValue,
                  validateObj: {
                    field: `${targetProfile}.personalInfo.mAddrBlock`,
                    type: "int",
                    mandatory: true,
                    value: newValue
                  }
                });
              }}
            />
            <EABTextField
              testID="Application__Appform__txtMailingStreet"
              style={styles.inputMarginLeft}
              value={details.mAddrStreet || ""}
              isRequired
              editable={!this.isDisabled}
              placeholder={EABi18n({
                language,
                textStore,
                path: "application.form.section.mailingAddressFieldStreet"
              })}
              maxLength={24}
              isError={_.get(
                error,
                `application.${targetProfile}.personalInfo.mAddrStreet.hasError`,
                false
              )}
              hintText={_.get(
                error,
                `application.${targetProfile}.personalInfo.mAddrStreet.message.${language}`,
                ""
              )}
              onChange={newValue => {
                this.updateValue({
                  path: `applicationForm.values.${targetProfile}.personalInfo.mAddrStreet`,
                  newValue,
                  validateObj: {
                    field: `${targetProfile}.personalInfo.mAddrStreet`,
                    type: "string",
                    mandatory: true,
                    value: newValue
                  }
                });
              }}
            />
          </View>
          <View style={styles.inputContentView}>
            <EABTextField
              testID="Application__Appform__txtMailingUnit"
              style={styles.inputShort}
              value={details.mAddrnitNum || ""}
              editable={!this.isDisabled}
              placeholder={EABi18n({
                language,
                textStore,
                path: "application.form.section.mailingAddressFieldUnitNumber"
              })}
              maxLength={
                (details.mAddrnitNum || "").toLowerCase().includes("unit")
                  ? 13
                  : 8
              }
              onChange={newValue => {
                if (newValue.length === 0) {
                  this.isMailingAddressUnitEmpty = true;
                } else {
                  if (
                    this.isMailingAddressUnitEmpty ||
                    this.isMailingAddressUnitEmpty === undefined
                  ) {
                    newValue = `Unit ${newValue}`;
                  }
                  this.isMailingAddressUnitEmpty = false;
                }
                this.updateValue({
                  path: `applicationForm.values.${targetProfile}.personalInfo.mAddrnitNum`,
                  newValue,
                  validateObj: {
                    field: `${targetProfile}.personalInfo.mAddrnitNum`,
                    type: "string",
                    value: newValue
                  }
                });
              }}
            />
            <EABTextField
              testID="Application__Appform__txtMailingBuilding"
              style={styles.inputMarginLeft}
              value={details.mAddrEstate || ""}
              editable={!this.isDisabled}
              placeholder={EABi18n({
                language,
                textStore,
                path: "application.form.section.mailingAddressFieldBuilding"
              })}
              maxLength={40}
              onChange={newValue => {
                this.updateValue({
                  path: `applicationForm.values.${targetProfile}.personalInfo.mAddrEstate`,
                  newValue,
                  validateObj: {
                    field: `${targetProfile}.personalInfo.mAddrEstate`,
                    type: "string",
                    value: newValue
                  }
                });
              }}
            />
          </View>
          <View style={styles.inputContentView}>
            <EABTextField
              testID="Application__Appform__txtMailingCity"
              style={styles.inputShort}
              value={getOptionValue({
                optionMap: optionsMap.city,
                language,
                value: details.mAddrCity || ""
              })}
              editable={!this.isDisabled}
              placeholder={EABi18n({
                language,
                textStore,
                path: "application.form.section.mailingAddressFieldCity"
              })}
              maxLength={12}
              onChange={newValue => {
                this.updateValue({
                  path: `applicationForm.values.${targetProfile}.personalInfo.mAddrCity`,
                  newValue,
                  validateObj: {
                    field: `${targetProfile}.personalInfo.mAddrCity`,
                    type: "string",
                    value: newValue
                  }
                });
              }}
            />
          </View>
          <View style={styles.footerHeight} />
        </View>
      );
    }
    return null;
  }

  getCityFieldType(details, targetProfile, language) {
    const { optionsMap, textStore } = this.props;
    const cityData = optionsMap.city.options.filter(
      value => value.condition === details.addrCountry
    );
    const view = [];
    if (cityData && cityData.length) {
      view.push(
        <SelectorField
          testID="Application__Appform__City"
          editable={false}
          style={{ width: "100%" }}
          value={
            getOptionValue({
              optionMap: optionsMap.city,
              language,
              value: details.addrCity
            }) || ""
          }
          placeholder={EABi18n({
            language,
            textStore,
            path: "application.form.section.residentialAddressFieldCity"
          })}
          selectorType={FLAT_LIST}
          flatListOptions={{
            data: cityData
          }}
        />
      );
    } else {
      view.push(
        <EABTextField
          testID="Application__Appform__txtResidentialCityState"
          style={{ width: "100%" }}
          value={
            getOptionValue({
              optionMap: optionsMap.city,
              language,
              value: details.addrCity
            }) || ""
          }
          editable={!this.isDisabled}
          placeholder={EABi18n({
            language,
            textStore,
            path: "application.form.section.residentialAddressFieldCity"
          })}
          maxLength={12}
          onChange={newValue => {
            this.updateValue({
              path: `applicationForm.values.${targetProfile}.personalInfo.addrCity`,
              newValue,
              validateObj: {
                field: `${targetProfile}.personalInfo.addrCity`,
                type: "string",
                value: newValue
              }
            });
          }}
        />
      );
    }
    if (
      details.addrCountry &&
      (details.addrCountry === "R52" ||
        details.addrCountry === "R53" ||
        details.addrCountry === "R72" ||
        details.addrCountry === "R54" ||
        details.addrCountry === "R120" ||
        details.addrCountry === "R134" ||
        details.addrCountry === "R147") &&
      (details.addrCity &&
        (details.addrCity === "T110" ||
          details.addrCity === "T120" ||
          details.addrCity === "T224" ||
          details.addrCity === "T205" ||
          details.addrCity === "T207" ||
          details.addrCity === "T208" ||
          details.addrCity === "T209"))
    ) {
      view.push(
        <EABTextField
          testID="Application__Appform__txtResidentOtherCity"
          style={{ width: "100%" }}
          value={details.otherResidenceCity || ""}
          editable={!this.isDisabled && !(cityData && cityData.length)}
          placeholder={EABi18n({
            language,
            textStore,
            path: "application.form.section.residentialAddressFieldCityOthers"
          })}
          maxLength={12}
          onChange={newValue => {
            this.updateValue({
              path: `applicationForm.values.${targetProfile}.personalInfo.otherResidenceCity`,
              newValue
            });
          }}
        />
      );
    }
    return <View style={styles.inputContentView}>{view}</View>;
  }

  populateResidentialAddress() {
    const { application } = this.props;
    const proposer = _.get(
      application,
      `applicationForm.values.proposer.personalInfo`
    );
    const { selectedProfile } = this.state;
    const targetProfile = _.isNumber(selectedProfile)
      ? `insured[${selectedProfile}]`
      : selectedProfile;
    const keyList = [
      {
        field: "addrBlock",
        mandatory: true
      },
      {
        field: "addrCity",
        mandatory: false
      },
      {
        field: "addrEstate",
        mandatory: false
      },
      {
        field: "addrStreet",
        mandatory: true
      },
      {
        field: "postalCode",
        mandatory: true
      },
      {
        field: "unitNum",
        mandatory: false
      }
    ];
    Object.values(keyList).forEach(value => {
      setTimeout(() => {
        this.updateValue({
          path: `applicationForm.values.${targetProfile}.personalInfo.${
            value.field
          }`,
          newValue: proposer[value.field],
          validateObj: {
            field: `${targetProfile}.personalInfo.${value.field}`,
            mandatory: value.mandatory,
            value: proposer[value.field]
          }
        });
      }, 300);
    });
  }

  genProfileButtonCompleteIcon(targetProfile) {
    const { error } = this.props;
    const errorFields = _.get(
      error.application,
      `${targetProfile}.personalInfo`
    );
    let imageSource = icCompleted;
    if (errorFields && errorFields !== null && errorFields !== undefined) {
      Object.values(errorFields).some(value => {
        if (value.hasError) {
          imageSource = icIncomplete;
          return true;
        }
        return false;
      });
    }
    return imageSource;
  }

  genProfileButton() {
    const { application } = this.props;
    const segmentedControlArray = [];
    const values = _.get(application, "applicationForm.values");
    if (!values) {
      return null;
    }

    if (values.proposer) {
      segmentedControlArray.push({
        key: "proposer",
        title: values.proposer.personalInfo.firstName,
        isShowingBadge: true,
        source: this.genProfileButtonCompleteIcon("proposer"),
        onPress: () => {
          this.setState({ selectedProfile: "proposer" });
        }
      });
    }

    if (values.insured.length > 0) {
      values.insured.forEach((value, index) => {
        segmentedControlArray.push({
          key: index,
          title: value.personalInfo.firstName,
          isShowingBadge: true,
          source: this.genProfileButtonCompleteIcon(`insured[${index}]`),
          onPress: () => {
            this.setState({ selectedProfile: index });
          }
        });
      });
    }

    return (
      <EABSegmentedControl
        style={[styles.profileButton]}
        segments={segmentedControlArray}
        activatedKey={this.state.selectedProfile}
      />
    );
  }

  updateValue({ path, newValue, validateObj }) {
    const { application, updateApplicationFormPersonalDetails } = this.props;
    const { selectedProfile } = this.state;
    const clonedValue = _.cloneDeep(application);
    const triggerValidate = [];
    if (
      path ===
      `applicationForm.values.${selectedProfile}.personalInfo.isSameAddr`
    ) {
      if (newValue === "Y") {
        // delete some fields (logic from web)
        delete clonedValue.applicationForm.values[selectedProfile].personalInfo
          .mAddrCountry;
        delete clonedValue.applicationForm.values[selectedProfile].personalInfo
          .mPostalCode;
        delete clonedValue.applicationForm.values[selectedProfile].personalInfo
          .mAddrBlock;
        delete clonedValue.applicationForm.values[selectedProfile].personalInfo
          .mAddrStreet;
        delete clonedValue.applicationForm.values[selectedProfile].personalInfo
          .mAddrEstate;
      }

      const newValueMAddrStreet = _.get(
        clonedValue,
        `applicationForm.values.${selectedProfile}.personalInfo.mAddrStreet`,
        ""
      );
      const newValueMAddrBlock = _.get(
        clonedValue,
        `applicationForm.values.${selectedProfile}.personalInfo.mAddrBlock`,
        ""
      );
      const newValueMAddrCountry = _.get(
        clonedValue,
        `applicationForm.values.${selectedProfile}.personalInfo.mAddrCountry`,
        ""
      );
      const newValueMPostalCode = _.get(
        clonedValue,
        `applicationForm.values.${selectedProfile}.personalInfo.mPostalCode`,
        ""
      );

      triggerValidate.push({
        field: `${selectedProfile}.personalInfo.mAddrStreet`,
        type: "string",
        mandatory: newValue === "N",
        value: newValueMAddrStreet
      });

      triggerValidate.push({
        field: `${selectedProfile}.personalInfo.mAddrBlock`,
        type: "int",
        mandatory: newValue === "N",
        value: newValueMAddrBlock
      });

      triggerValidate.push({
        field: `${selectedProfile}.personalInfo.mAddrCountry`,
        type: "string",
        mandatory: newValue === "N",
        value: newValueMAddrCountry
      });

      triggerValidate.push({
        field: `${selectedProfile}.personalInfo.mPostalCode`,
        type: "int",
        mandatory: newValue === "N",
        value: newValueMPostalCode
      });

      // keep
      // set fields empty
      // Object.keys(
      //   clonedValue.applicationForm.values[selectedProfile].personalInfo
      // ).forEach(key => {
      //   if (
      //     key === "mAddrCountry" ||
      //     key === "mPostalCode" ||
      //     key === "mAddrBlock" ||
      //     key === "mAddrStreet" ||
      //     key === "mAddrEstate"
      //   ) {
      //     clonedValue.applicationForm.values[selectedProfile].personalInfo[
      //       key
      //     ] =
      //       "";
      //   }
      // });
    }
    _.set(clonedValue, path, newValue);
    updateApplicationFormPersonalDetails({
      newApplication: clonedValue,
      validateObj,
      triggerValidate
    });
  }

  render() {
    const { selectedProfile } = this.state;
    const { textStore, application } = this.props;

    if (!application || _.isEmpty(application)) {
      return null;
    }
    const targetProfile = _.isNumber(selectedProfile)
      ? `insured[${selectedProfile}]`
      : selectedProfile;
    const crossBorderMsg = _.get(
      application,
      `applicationForm.values[${targetProfile}].extra.isCrossBorder`,
      null
    );
    const getSingPostBranchDetails = this.getSingPostBranchDetails();
    return (
      <ContextConsumer>
        {({ language }) => (
          <View style={styles.container}>
            <View style={styles.profileButtonContainer}>
              <ScrollView
                horizontal
                scrollEnabled={
                  _.get(application, "applicationForm.values.insured", [])
                    .length > 2
                }
                showsHorizontalScrollIndicator={false}
                style={styles.profileButtonScrollView}
                contentContainerStyle={styles.profileButtonContentContainer}
              >
                <View style={styles.profileButtonView}>
                  {this.genProfileButton()}
                </View>
              </ScrollView>
            </View>
            <ScrollView
              style={styles.scrollView}
              contentContainerStyle={styles.scrollViewContent}
              showsVerticalScrollIndicator={false}
              ref={this.scrollView}
            >
              <View style={styles.personalDetailsRedWanningView}>
                <Text style={styles.personalDetailsRedWanning}>
                  {`"${EABi18n({
                    path: "application.form.section.redWanning",
                    language,
                    textStore
                  })}"`}
                </Text>
              </View>
              {crossBorderMsg ? (
                <View style={styles.personalDetailsRedWanningView}>
                  <Text style={styles.personalDetailsRedWanning}>
                    {`${EABi18n({
                      path: "application.form.section.crossBorderNote",
                      language,
                      textStore
                    })}`}
                  </Text>
                </View>
              ) : null}

              {getSingPostBranchDetails ? (
                <View>
                  <View style={styles.analysisSectionBorderLine} />
                  {getSingPostBranchDetails}
                </View>
              ) : null}
              <View style={styles.sectionBorderLine} />
              {this.getDetailsOfTargetProfile(language)}
              <View style={styles.sectionBorderLine} />
              {this.getProfessionalDetails(language)}
              <View style={styles.sectionBorderLine} />
              {this.getResidentialAddress(language)}
              {this.getMailingAddress(language)}
            </ScrollView>
          </View>
        )}
      </ContextConsumer>
    );
  }
}

ApplicationFormPersonalDetails.propTypes = {
  textStore: APP_REDUCER_TYPE_CHECK[TEXT_STORE].isRequired,
  selectedSectionKey: PropTypes.string.isRequired,
  updateApplicationFormPersonalDetails: PropTypes.func.isRequired,
  updateAddress: PropTypes.func.isRequired,
  optionsMap: REDUCER_TYPE_CHECK[REDUCER_TYPES.OPTIONS_MAP].isRequired,
  application:
    REDUCER_TYPE_CHECK[REDUCER_TYPES.PRE_APPLICATION].ApplicationItem.value
      .isRequired,
  error: REDUCER_TYPE_CHECK[REDUCER_TYPES.APPLICATION].error.isRequired
};

export default ApplicationFormPersonalDetails;
