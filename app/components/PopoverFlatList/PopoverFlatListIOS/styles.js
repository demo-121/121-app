import { StyleSheet } from "react-native";
import Theme from "../../../theme";

export default StyleSheet.create({
  flatList: {
    flex: 1
  },
  itemSeparatorStyle: {
    height: 1,
    backgroundColor: Theme.lightGrey,
    marginLeft: Theme.alignmentL
  },
  itemWrap: {
    height: 50,
    paddingHorizontal: Theme.alignmentL,
    width: "100%",
    justifyContent: "center"
  },
  itemTextWrapper: {
    flexBasis: "auto",
    flex: 0
  },
  recordText: {
    ...Theme.bodyPrimary,
    marginLeft: Theme.alignmentL
  },
  hiddenView: {
    width: 1,
    height: 1
  }
});
