import { ACTION_TYPES, REDUCER_TYPES } from "eab-web-api";
import { connect } from "react-redux";
import QuotationEmailSection from "../components/QuotationEmailSection";
import APP_ACTION_TYPES from "../../../constants/ACTION_TYPES";
import { LOGIN, PAYMENT, TEXT_STORE } from "../../../constants/REDUCER_TYPES";

const { CLIENT, PROPOSAL, QUOTATION } = REDUCER_TYPES;

/**
 * QuotationEmailSection
 * @description QuotationEmailSection page.
 * @requires QuotationEmailSection - QuotationEmailSection UI
 * */
const mapStateToProps = state => ({
  profile: state[CLIENT].profile,
  proposal: state[PROPOSAL],
  isHandlingListener: state[LOGIN].isHandlingListener,
  proposalEmail: state[PROPOSAL].proposalEmail,
  tab: state[PROPOSAL].proposalEmail.tab,
  textStore: state[TEXT_STORE]
});

const mapDispatchToProps = dispatch => ({
  openEmailDialog: callback => {
    dispatch({
      type: ACTION_TYPES[QUOTATION].OPEN_EMAIL_DIALOG,
      callback
    });
  },
  checkBoxOnPress: ({ tab, option }) => {
    dispatch({
      type: ACTION_TYPES[QUOTATION].REPORT_ON_CHANGE,
      tab,
      report: option.key,
      isSelected: option.isSelected
    });
  },
  checkTokenExpiryDate: callback => {
    dispatch({
      type: APP_ACTION_TYPES[LOGIN].CHECK_TOKEN_EXPIRY_DATE,
      callback
    });
  },
  checkValidDevice: callback => {
    dispatch({
      type: APP_ACTION_TYPES[LOGIN].CHECK_VALID_DEVICE,
      callback
    });
  },
  loginByOnlineAuthForPayment: () => {
    dispatch({
      type: APP_ACTION_TYPES[PAYMENT].LOGIN_BY_ONLINE_AUTH_FOR_PAYMENT
    });
  },
  setOnlineAuthCodeForPayment: ({ authCode, callback }) => {
    dispatch({
      type: APP_ACTION_TYPES[PAYMENT].ONLINE_AUTH_CODE_FOR_PAYMENT,
      authCode,
      callback
    });
  },
  sendEmail: callback => {
    dispatch({
      type: ACTION_TYPES[QUOTATION].SEND_EMAIL,
      callback
    });
  },
  tabOnPress: option => {
    dispatch({
      type: ACTION_TYPES[QUOTATION].TAB_ON_CHANGE,
      option
    });
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(QuotationEmailSection);
