import { connect } from "react-redux";
import { REDUCER_TYPES } from "eab-web-api";
import Main from "../components/Main";
import {
  login,
  LoginFailByOnlineAuth,
  loginByOnlineAuth,
  onlineAuthCode,
  onlineAuthCode2,
  setEnvironment,
  setLoginFlow,
  checkAppLocked,
  setAppLocked,
  checkLoginStatus
} from "../../../actions/login";
import {
  loginByOnlineAuthForPayment,
  verify121OnlineAuthForPayment,
  getApplication,
  validatePayment,
  getPaymentStatus
} from "../../../actions/payment";
import {
  validateSubmission,
  dataSync,
  submitApplication
} from "../../../actions/submission";
import { LOGIN, TEXT_STORE, PAYMENT } from "../../../constants/REDUCER_TYPES";
import ACTION_TYPES from "../../../constants/ACTION_TYPES";
import { getText } from "../../../reducers/textStore";

const { CONFIG } = REDUCER_TYPES;

/**
 * Main
 * @description main page container.
 * @reducer login
 * @reducer config.isShowingClientNavigator
 * @requires Main - Main UI
 * */
const mapStateToProps = state => ({
  isOnlineAuthCompleted: state[PAYMENT].isOnlineAuthCompleted,
  disableAppActivationPage: state[LOGIN].disableAppActivationPage,
  loginFlow: state[LOGIN].loginFlow,
  isLogin: state[LOGIN].isLogin,
  isAppLocked: state[LOGIN].isAppLocked,
  changeToOnlineAuth: state[LOGIN].changeToOnlineAuth,
  isLogout: state[LOGIN].isLogout,
  isAppActivationDone: state[LOGIN].isAppActivationDone,
  isExceedFailLoginAttemps: state[LOGIN].isExceedFailLoginAttemps,
  userID: state[LOGIN].userID,
  loginFlowCheck: state[LOGIN].loginFlowCheck,
  environment: state[LOGIN].environment,
  isHandlingListener: state[LOGIN].isHandlingListener,
  paymentMethod: state[PAYMENT].paymentMethod,
  isLoading: state[CONFIG].isLoading,
  showNewAppPasswordScene: state[LOGIN].showNewAppPasswordScene,
  showResetAppPasswordScene: state[LOGIN].showResetAppPasswordScene,
  showResetAppPasswordManuallyScene:
    state[LOGIN].showResetAppPasswordManuallyScene,
  isAppPasswordExpired: state[LOGIN].isAppPasswordExpired,
  accessSQLiteDB: state[LOGIN].accessSQLiteDB,
  textStore: state[TEXT_STORE],
  text: {
    passwordDoNotMeetPasswordPolicy: getText({
      state,
      path: "login.appPassword.password_do_not_meet_password_policy"
    })
  }
});

const mapDispatchToProps = dispatch => ({
  LoginFailByOnlineAuth() {
    LoginFailByOnlineAuth({
      dispatch
    });
  },
  checkLoginStatus() {
    checkLoginStatus({ dispatch });
  },
  onlineAuthCode(authCode, callback) {
    onlineAuthCode({ dispatch, authCode, callback });
  },
  onlineAuthCode2(callback) {
    onlineAuthCode2({ dispatch, callback });
  },
  login() {
    login({ dispatch });
  },
  setLoginFlow(loginFlow) {
    setLoginFlow({ dispatch, loginFlow });
  },
  setEnvironment(environment) {
    setEnvironment({ dispatch, environment });
  },
  loginByOnlineAuth(callback) {
    loginByOnlineAuth({ dispatch, callback });
  },
  loginByOnlineAuthForPayment() {
    loginByOnlineAuthForPayment({ dispatch });
  },
  verify121OnlineAuthForPayment() {
    verify121OnlineAuthForPayment({ dispatch });
  },
  getApplication(docId, callback) {
    getApplication({ dispatch, docId, callback });
  },
  validatePayment(docId, callback) {
    validatePayment({ dispatch, docId, callback });
  },
  validateSubmission(docId, callback) {
    validateSubmission({ dispatch, docId, callback });
  },
  dataSync(docId, applicationResult, callback) {
    dataSync({ dispatch, docId, applicationResult, callback });
  },
  submitApplication(docId, callback) {
    submitApplication({ dispatch, docId, callback });
  },
  getPaymentStatus(docId, callback) {
    getPaymentStatus({ dispatch, docId, callback });
  },
  navigateToOnlineLoginPage() {
    dispatch({
      type: ACTION_TYPES[LOGIN].USE_ONLINE_AUTH
    });
    dispatch({
      type: ACTION_TYPES[LOGIN].BACK_ONLINE_AUTH
    });
  },
  navigateToAppPassword() {
    dispatch({
      type: ACTION_TYPES[LOGIN].USE_ONLINE_AUTH
    });
    dispatch({
      type: ACTION_TYPES[LOGIN].BACK_ONLINE_AUTH
    });
  },
  navigateToEnvironmentPage() {
    dispatch({
      type: ACTION_TYPES[LOGIN].USE_ACTIVATION
    });
  },
  navigateToOnlineAuth(changeToOnlineAuth) {
    dispatch({
      type: ACTION_TYPES[LOGIN].CHANGE_TO_ONLINE_AUTH,
      changeToOnlineAuth
    });
  },
  checkAppLocked(callback) {
    checkAppLocked({ dispatch, callback });
  },
  setAppLocked(isLocked) {
    setAppLocked({ dispatch, isLocked });
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Main);
