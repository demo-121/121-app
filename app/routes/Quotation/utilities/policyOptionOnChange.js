import { Alert } from "react-native";
import EABi18n from "../../../utilities/EABi18n";

export default function({
  quotation,
  quote,
  resetQuot,
  textStore,
  language,
  policyOption,
  newValue
}) {
  const { id, onChange, clearFunds, alertOnValue } = policyOption;
  const { fund } = quotation;

  if (onChange) {
    const ruleA =
      !onChange.to ||
      onChange.to === newValue ||
      onChange.to.indexOf(newValue) > -1;
    const ruleB = onChange.action === "resetQuot";

    if (ruleA && ruleB) {
      Alert.alert(
        EABi18n({
          textStore,
          language,
          path: "alert.confirm"
        }),
        EABi18n({
          language,
          textStore,
          path: "quotation.warning.resetQuotation"
        }),
        [
          {
            text: EABi18n({ language, textStore, path: "button.no" }),
            onPress: () => {}
          },
          {
            text: EABi18n({ language, textStore, path: "button.yes" }),
            onPress: () => {
              const params = onChange.params || {};
              const { keepConfigs, keepPlans, keepFunds } = params;
              resetQuot({
                quotation: {
                  ...quotation,
                  policyOptions: {
                    ...quotation.policyOptions,
                    [id]: newValue
                  }
                },
                keepConfigs,
                keepPolicyOptions: true,
                keepPlans,
                keepFunds
              });
            }
          }
        ]
      );
    }
  } else {
    let cleanFund = false;
    const quoteFunction = () => {
      quote({
        ...quotation,
        fund: cleanFund ? null : quotation.fund,
        policyOptions: {
          ...quotation.policyOptions,
          [id]: newValue
        }
      });
    };

    if (clearFunds === "Y" && fund && fund.funds) {
      cleanFund = true;

      Alert.alert(
        EABi18n({
          textStore,
          language,
          path: "alert.warning"
        }),
        EABi18n({
          language,
          textStore,
          path: "quotation.warning.changePaymentMethod"
        }),
        [
          {
            text: EABi18n({ language, textStore, path: "button.ok" }),
            onPress: () => {
              if (alertOnValue && alertOnValue[newValue]) {
                Alert.alert(
                  EABi18n({
                    textStore,
                    language,
                    path: "alert.warning"
                  }),
                  EABi18n({
                    language,
                    textStore,
                    path: "quotation.alert.".concat(newValue)
                  }),
                  [
                    {
                      text: EABi18n({ language, textStore, path: "button.ok" }),
                      onPress: quoteFunction
                    }
                  ]
                );
              } else {
                quoteFunction();
              }
            }
          }
        ]
      );
    } else {
      quoteFunction();
    }
  }
}
