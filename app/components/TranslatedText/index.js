import { utilities } from "eab-web-api";
import * as _ from "lodash";
import PropTypes from "prop-types";
import React, { PureComponent } from "react";
import { Text } from "react-native";
import REDUCER_TYPE_CHECK from "../../constants/REDUCER_TYPE_CHECK";
import { TEXT_STORE } from "../../constants/REDUCER_TYPES";
import { ContextConsumer } from "../../context";
import EABi18n from "../../utilities/EABi18n";

const { fillTextVariable } = utilities.common;

/**
 * @description translated text component
 * @param {string} path - path(key) stored in @/locales/textStore/ENGLISH
 * @param {array} [replace] - array of path / string to be replaced to the
 *   translated text
 * @param {string|number} [replace[].value] - path / string to be replaced
 * @param {bool} [replace[].isPath] - indicate value in replace array is path
 *   or not
 * @param {function} funcOnText - function to run on translated text before
 *   display
 * @example <TranslatedText path="demo"/>
 * @example <TranslatedText path="demo" replace={[ {value: "path", isPath: true
 *   }]}/>
 * */
class TranslatedText extends PureComponent {
  render() {
    // =========================================================================
    // variables
    // =========================================================================
    const { path, textStore, replace, funcOnText } = this.props;
    // =========================================================================
    // render functions
    // =========================================================================
    const translatText = language =>
      fillTextVariable({
        text: EABi18n({ path, language, textStore }),
        data: replace,
        EABi18n,
        language,
        textStore
      });

    return (
      <ContextConsumer>
        {({ language }) => (
          <Text {...this.props}>
            {_.isFunction(funcOnText)
              ? funcOnText(translatText(language))
              : translatText(language)}
          </Text>
        )}
      </ContextConsumer>
    );
  }
}

TranslatedText.propTypes = {
  path: PropTypes.string.isRequired,
  textStore: REDUCER_TYPE_CHECK[TEXT_STORE].isRequired,
  replace: PropTypes.arrayOf(
    PropTypes.shape({
      value: PropTypes.oneOfType([PropTypes.string, PropTypes.number])
        .isRequired,
      isPath: PropTypes.bool.isRequired
    })
  ),
  funcOnText: PropTypes.func
};

TranslatedText.defaultProps = {
  replace: [],
  funcOnText: text => text
};

export default TranslatedText;
