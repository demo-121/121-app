import ACTION_TYPES from "../constants/ACTION_TYPES";
import { TEXT_STORE } from "../constants/REDUCER_TYPES";
import {TEXT_STORE as textStore, LANGUAGE_TYPES } from "eab-web-api";
import { default as reducer, getText } from "../reducers/textStore";
import textStoreSource from "../locales";

describe("text store reducer", () => {
  it("should return the initial state", () => {
    expect(reducer(undefined, {})).toEqual(textStoreSource);
  });

  it("should handle UPDATE_TEXT_STORE", () => {
    expect(reducer(
      {},
      {
        type: ACTION_TYPES[TEXT_STORE].UPDATE_TEXT_STORE,
        textStore: {
          version: "1.2.3",
          TRADITIONAL_CHINESE: {
            title: "是的"
          },
          ENGLISH: {
            title: "abc"
          }
        }
      }
    )).toEqual({
      version: "1.2.3",
      [LANGUAGE_TYPES.TRADITIONAL_CHINESE]: {
        title: "是的"
      },
      [LANGUAGE_TYPES.ENGLISH]: {
        title: "abc"
      }
    });
  });

  it("getText should work", () => {
    /* get text require root reducer */
    const state = {
      [TEXT_STORE]: reducer({
        version: "1.2.3",
        [LANGUAGE_TYPES.TRADITIONAL_CHINESE]: {
          title: "是的"
        },
        [LANGUAGE_TYPES.ENGLISH]: {
          title: "abc"
        }
      }, {})
    };

    expect(
      getText({
        state,
        path: "title"
      })
    ).toEqual("abc");
  });
});
