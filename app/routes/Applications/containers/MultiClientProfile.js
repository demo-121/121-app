import { connect } from "react-redux";
import { ACTION_TYPES, REDUCER_TYPES } from "eab-web-api";
import { TEXT_STORE } from "../../../constants/REDUCER_TYPES";
import MultiClientProfile from "../components/MultiClientProfile";

const { CLIENT, CLIENT_FORM, PRE_APPLICATION, APPLICATION } = REDUCER_TYPES;

/**
 * MultiClientProfile
 * @description MultiClientProfile in Summary page.
 * @requires MultiClientProfile - MultiClientProfile UI
 * */
const mapStateToProps = state => ({
  textStore: state[TEXT_STORE],
  profile: state[CLIENT].profile,
  dependantProfiles: state[CLIENT].dependantProfiles,
  clientForm: state[CLIENT_FORM],
  agentChannel: state[PRE_APPLICATION].agentChannel,
  hasErrorList: state[PRE_APPLICATION].multiClientProfile.hasErrorList
});

const mapDispatchToProps = dispatch => ({
  applyApplicationForm: ({ quotationId, isShield, callback }) => {
    dispatch({
      type: ACTION_TYPES[APPLICATION].SAGA_APPLY_APPLICATION_FORM,
      quotationId,
      isShield,
      callback
    });
  },
  loadClient({
    cid,
    profile,
    isFNA,
    isProposerMissing,
    isInsuredMissing,
    callback
  }) {
    dispatch({
      type: ACTION_TYPES[PRE_APPLICATION].LOAD_CLIENT_AND_INIT_VALIDATE,
      cid,
      profile,
      configData: {
        isCreate: false,
        isFamilyMember: false,
        isProposerMissing,
        isInsuredMissing,
        isFNA,
        isAPP: true
      },
      callback
    });
  },
  saveClient(callback) {
    dispatch({
      type: ACTION_TYPES[CLIENT_FORM].SAVE_CLIENT,
      confirm: true,
      callback
    });
  },
  cleanMultiClientProfile() {
    dispatch({
      type: ACTION_TYPES[PRE_APPLICATION].CLEAN_MULTI_CLIENT_PROFILE
    });
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MultiClientProfile);
