import { ActionSheetIOS } from "react-native";
import ImagePicker from "react-native-image-picker";
import EABi18n from "./EABi18n";

export default function({ language, textStore, photoOnChange }) {
  const config = {
    quality: 1.0,
    maxWidth: 64,
    maxHeight: 64,
    storageOptions: {
      skipBackup: true
    },
    cameraType: "back",
    mediaType: "photo"
  };

  ActionSheetIOS.showActionSheetWithOptions(
    {
      options: [
        EABi18n({
          language,
          textStore,
          path: "clientForm.actionSheet.takePhoto"
        }),
        EABi18n({
          language,
          textStore,
          path: "clientForm.actionSheet.choosePhoto"
        }),
        EABi18n({
          language,
          textStore,
          path: "clientForm.actionSheet.deletePhoto"
        })
      ],
      destructiveButtonIndex: -1,
      cancelButtonIndex: -1
    },
    buttonIndex => {
      if (buttonIndex === 0) {
        ImagePicker.launchCamera(config, response => {
          const imageBase64 = {
            uri: `data:image/jpeg;base64,${response.data}`
          };
          photoOnChange(imageBase64);
        });
      }
      if (buttonIndex === 1) {
        ImagePicker.launchImageLibrary(config, response => {
          const imageBase64 = {
            uri: `data:image/jpeg;base64,${response.data}`
          };
          photoOnChange(imageBase64);
        });
      }

      if (buttonIndex === 2) {
        photoOnChange("");
      }
    }
  );
}
