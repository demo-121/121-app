import { connect } from "react-redux";
import { REDUCER_TYPES } from "eab-web-api";
import { TEXT_STORE } from "../../../constants/REDUCER_TYPES";
import PaymentStatus from "../components/PaymentStatus";

/**
 * @description Payment status in Payment section.
 * @requires PaymentStatus - PaymentStatus UI
 * */
const mapStateToProps = state => ({
  textStore: state[TEXT_STORE],
  onlinePaymentStatus:
    state[REDUCER_TYPES.PAYMENT_AND_SUBMISSION].onlinePaymentStatus
});

const mapDispatchToProps = () => ({});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PaymentStatus);
