import { StyleSheet } from "react-native";
import Theme from "../../theme";
/**
 * styles
 * */
export default StyleSheet.create({
  doneButton: {
    position: "absolute",
    right: Theme.alignmentXL,
    top: 0
  },
  titleBar: {
    position: "relative",
    marginBottom: Theme.alignmentL,
    alignItems: "center"
  },
  uploadButton: {
    position: "absolute",
    left: Theme.alignmentXL,
    top: 0
  }
});
