import { ACTION_TYPES } from "eab-web-api";
import { appStore } from "../../app/utilities/store";
// import { NavigationActions } from "react-navigation";

export default function(/* dispatch */) {
  appStore.dispatch({
    type: ACTION_TYPES.root.CLEAN_CLIENT_DATA
  });

  // keep, don't know why we need these functions before
  // dispatch(
  //   NavigationActions.navigate({
  //     routeName: "Profile"
  //   })
  // );
  // dispatch(
  //   NavigationActions.navigate({
  //     routeName: "ProductsDetail"
  //   })
  // );
  // dispatch(
  //   NavigationActions.navigate({
  //     routeName: "NeedsDetail"
  //   })
  // );
}
