import { StyleSheet } from "react-native";

export default StyleSheet.create({
  pickerWrap: {
    flex: 1,
    alignItems: "center",
    justifyContent: "flex-start"
  }
});
