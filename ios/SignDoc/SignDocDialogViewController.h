//
//  SignDocDialogViewController.h
//  EASE
//
//  Created by Osric Wong on 11/5/2018.
//  Copyright © 2018 Facebook. All rights reserved.
//
#import <UIKit/UIKit.h>
#import "SignDocSDK-c.h"
#import <signdociosfoundations/SDSignatureHandler.h>
#import <signdociosfoundations/SDSignatureCaptureController.h>
#import <signdociosfoundations/SDDeviceManager.h>
#import "UIImagePickerController+NoRotate.h"
#import "UIImage+Resize.h"
#ifdef __APPLE__
#include "TargetConditionals.h"
#endif

@interface SignDocDialogViewController : UIViewController <SDSignatureHandler, UIImagePickerControllerDelegate, UINavigationControllerDelegate>

@property (strong, nonatomic) UIView * _Nullable scrollViewContainer;
@property (strong, nonatomic) UIScrollView * _Nullable scrollView;
@property (strong, nonatomic) UIImageView * _Nullable imageView;
@property (strong, nonatomic) SDSignatureCaptureController * _Nullable dialog;
@property BOOL handleSignatureDialog;
@property (nonatomic) UILabel * _Nullable signatureFrameLabel;
@property (nonatomic) UIView * _Nullable overlayView;
@property (nonatomic) UIImagePickerController * _Nullable imagePickerController;
@property (nonatomic) NSMutableArray * _Nullable capturedImages;
@property (nonatomic) NSString * _Nullable signatureFieldName;

@property (atomic) double documentWidth;
@property (atomic) double scale;

- (IBAction)handleTapGesture:(UITapGestureRecognizer * _Nullable)sender;
- (BOOL)showCaptureDialog:(double)signX signY:(double)signY pdfFilePath:(NSString *_Nonnull)pdfFilePath pdfFileName:(NSString *_Nonnull)pdfFileName word: (NSString *)word order:(NSString *)order;
- (NSMutableArray *)convertPdfToPng:(NSString *)path;
- (NSMutableArray *)findTexts: (NSMutableArray *)texts;

@end

