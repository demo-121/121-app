import React, { PureComponent } from "react";
import { View, Text } from "react-native";
import moment from "moment";
import * as _ from "lodash";

import { REDUCER_TYPES, REDUCER_TYPE_CHECK, utilities } from "eab-web-api";

import { ContextConsumer } from "../../../../context";
import getEABTextByLangType from "../../../../utilities/getEABTextByLangType";
import EABCard from "../../../../components/EABCard";
import EABTable from "../../../../components/EABTable";
import EABTableHeader from "../../../../components/EABTableHeader";
import EABTableSectionHeader from "../../../../components/EABTableSectionHeader";
import EABTableSection from "../../../../components/EABTableSection";
import EABTableFooter from "../../../../components/EABTableFooter";

import TranslatedText from "../../../../containers/TranslatedText";
import {
  BASIC,
  SHOW_ANNUAL_PREMIUM,
  SHOW_RETIREMENT_INCOME,
  SHOW_GUARANTEED_ANNUAL_INCOME
} from "./constants";

import styles from "./styles";

const { PRE_APPLICATION, OPTIONS_MAP } = REDUCER_TYPES;
const { getCurrencySign, getCurrency } = utilities.common;
const { numberToCurrency } = utilities.numberFormatter;

/**
 * ApplicationItemDetails
 * @description item in ApplicationsList inside Application Summary page.
 * @param {object} value - value of application / quotation item object
 * */

class ApplicationItemDetails extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {};

    this.renderContentTableHeader = this.renderContentTableHeader.bind(this);
    this.renderContentTableSection = this.renderContentTableSection.bind(this);
    this.renderContentTableFooter = this.renderContentTableFooter.bind(this);
  }

  get sign() {
    const { optionsMap } = this.props;
    const { value } = this.props;
    const { ccy } = value;

    return getCurrencySign({
      compCode: "01",
      ccy,
      optionsMap
    });
  }

  renderContentTableHeader(language, displayType) {
    const { value } = this.props;
    const { id, paymentMode } = value;

    let premiumHeaderPath = "";
    switch (paymentMode) {
      case "A":
        premiumHeaderPath = "application.summary.item_header_annualPremium";
        break;
      case "S":
        premiumHeaderPath = "application.summary.item_header_semiannualPremium";
        break;
      case "Q":
        premiumHeaderPath = "application.summary.item_header_quarterlyPremium";
        break;
      case "M":
        premiumHeaderPath = "application.summary.item_header_monthlyPremium";
        break;
      case "L":
        premiumHeaderPath = "application.summary.item_header_singlePremium";
        break;
      default:
        premiumHeaderPath = "";
        break;
    }

    const premiumHeaderText = (
      <TranslatedText
        style={styles.tableHeaderTitle}
        language={language}
        path={premiumHeaderPath}
      />
    );

    const sumAssuredHeaderText = (
      <TranslatedText
        style={styles.tableHeaderTitle}
        language={language}
        path="application.summary.item_header_sumAssured"
      />
    );

    let tableHeader4 = sumAssuredHeaderText;
    let tableHeader5 = premiumHeaderText;
    if (displayType === 1) {
      const annualPremiumHeaderText = (
        <TranslatedText
          style={styles.tableHeaderTitle}
          language={language}
          path="application.summary.item_header_annualPremium"
        />
      );
      tableHeader4 = premiumHeaderText;
      tableHeader5 = annualPremiumHeaderText;
    } else if (displayType === 2) {
      const retirementIncomeHeaderText = (
        <TranslatedText
          style={styles.tableHeaderTitle}
          language={language}
          path="application.summary.item_header_retirementIncome"
        />
      );
      tableHeader4 = retirementIncomeHeaderText;
    } else if (displayType === 3) {
      const guaranteedAnnualIncomeHeaderText = (
        <TranslatedText
          style={styles.tableHeaderTitle}
          language={language}
          path="application.summary.item_header_guaranteedAnnualIncome"
        />
      );
      tableHeader4 = guaranteedAnnualIncomeHeaderText;
    }

    return (
      <EABTableHeader key={`${id}-header`}>
        <View style={styles.tableHeader}>
          <TranslatedText
            style={styles.tableHeaderTitle}
            language={language}
            path="application.summary.item_header_planName"
          />
        </View>
        <View style={styles.tableHeader}>
          <TranslatedText
            style={styles.tableHeaderTitle}
            language={language}
            path="application.summary.item_header_policyTerm"
          />
        </View>
        <View style={styles.tableHeader}>
          <TranslatedText
            style={styles.tableHeaderTitle}
            language={language}
            path="application.summary.item_header_paymentTerm"
          />
        </View>
        <View style={styles.tableHeader}>{tableHeader4}</View>
        <View style={styles.tableHeader}>{tableHeader5}</View>
      </EABTableHeader>
    );
  }

  renderContentTableSection(language, displayType, showBasicPlan) {
    const { sign } = this;
    const { value } = this.props;
    const { id, plans, baseProductCode, paymentMode } = value;

    const showPlanDetails = [];
    const filteredPlans = showBasicPlan
      ? _.filter(plans, plan => plan.covCode === baseProductCode)
      : _.filter(plans, plan => plan.covCode !== baseProductCode);

    if (filteredPlans.length > 0) {
      showPlanDetails.push(
        <EABTableSectionHeader
          key={`${id}-${showBasicPlan ? "basic" : "rider"}`}
          style={styles.tableSectionHeader}
        >
          <View />
        </EABTableSectionHeader>
      );
    }

    _.forEach(filteredPlans, plan => {
      const {
        covCode,
        covName,
        polTermDesc,
        premTermDesc,
        retirementIncome,
        sumInsured,
        yearPrem,
        halfYearPrem,
        quarterPrem,
        monthPrem,
        premium,
        tax,
        incomePayout
      } = plan;

      let premiumDisplay = 0;
      let premiumDsiplayWithTax = 0;
      switch (paymentMode) {
        case "A":
          premiumDisplay = yearPrem;
          premiumDsiplayWithTax = yearPrem + _.get(tax, "yearTax", 0);
          break;
        case "S":
          premiumDisplay = halfYearPrem;
          premiumDsiplayWithTax = halfYearPrem + _.get(tax, "halfYearTax", 0);
          break;
        case "Q":
          premiumDisplay = quarterPrem;
          premiumDsiplayWithTax = quarterPrem + _.get(tax, "quarterTax", 0);
          break;
        case "M":
          premiumDisplay = monthPrem;
          premiumDsiplayWithTax = monthPrem + _.get(tax, "monthTax", 0);
          break;
        default:
          premiumDisplay = premium;
          premiumDsiplayWithTax = premium;
          break;
      }

      let tableDetail4;
      let tableDetail5;
      switch (displayType) {
        case BASIC:
        case SHOW_GUARANTEED_ANNUAL_INCOME:
          if (plan.saViewInd === "Y") {
            tableDetail4 = `${sign}${getCurrency({
              value: sumInsured,
              sign: "",
              decimals: 0
            })}`;
          } else if (incomePayout) {
            tableDetail4 = `${sign}${numberToCurrency({
              value: incomePayout,
              sign: "",
              decimals: 2
            })}`;
          } else {
            tableDetail4 = "-";
          }
          tableDetail5 =
            premiumDsiplayWithTax > 0
              ? `${sign}${numberToCurrency({ value: premiumDsiplayWithTax })}`
              : "-";
          break;
        case SHOW_ANNUAL_PREMIUM:
          tableDetail4 = `${sign}${numberToCurrency({
            value: premiumDisplay
          })}`;
          tableDetail5 = plan.yearPrem
            ? `${sign}${numberToCurrency({ value: yearPrem })}`
            : "-";
          break;
        case SHOW_RETIREMENT_INCOME:
          tableDetail4 =
            plan.retirementIncome > 0
              ? `${sign}${getCurrency({
                  value: retirementIncome,
                  sign: "",
                  decimals: 0
                })}`
              : "-";
          tableDetail5 =
            premiumDsiplayWithTax > 0
              ? `${sign}${numberToCurrency({ value: premiumDsiplayWithTax })}`
              : "-";
          break;
        default:
          throw new Error(
            `unexpected application item displayType: ${displayType}`
          );
      }

      showPlanDetails.push(
        <EABTableSection key={`${id}-${covCode}`}>
          <View style={styles.tableSection}>
            <Text style={styles.tableSectionTitle}>
              {getEABTextByLangType({
                data: covName,
                language
              })}
            </Text>
          </View>
          <View style={styles.tableSection}>
            <Text style={styles.tableSectionTitle}>{polTermDesc}</Text>
          </View>
          <View style={styles.tableSection}>
            <Text style={styles.tableSectionTitle}>{premTermDesc}</Text>
          </View>
          <View style={styles.tableSection}>
            <Text style={styles.tableSectionTitle} numberOfLines={1}>
              {tableDetail4}
            </Text>
          </View>
          <View style={styles.tableSection}>
            <Text style={styles.tableSectionTitle} numberOfLines={1}>
              {tableDetail5}
            </Text>
          </View>
        </EABTableSection>
      );
    });

    return showPlanDetails;
  }

  renderContentTableFooter(language) {
    const { sign } = this;
    const { value } = this.props;
    const { id, lastUpdateDate, paymentMode, totPremium } = value;

    const policyOptions =
      _.get(value, "policyOptions") || _.get(value, "quotation.policyOptions");

    // Total Premium
    let paymentModeTextPath = "";
    switch (paymentMode) {
      case "A":
        paymentModeTextPath = "application.summary.annual";
        break;
      case "S":
        paymentModeTextPath = "application.summary.semiAnnual";
        break;
      case "Q":
        paymentModeTextPath = "application.summary.quarterly";
        break;
      case "M":
        paymentModeTextPath = "application.summary.monthly";
        break;
      case "L":
        paymentModeTextPath = "application.summary.single";
        break;
      default:
        paymentModeTextPath = "";
        break;
    }

    // RSP
    const rspAmount =
      _.get(policyOptions, "rspAmount") || _.get(policyOptions, "rspAmt", 0);
    const rspPayFreq = _.get(policyOptions, "rspPayFreq");
    let rspPayFreqTextPath = "";
    switch (rspPayFreq) {
      case "annual":
        rspPayFreqTextPath = "application.summary.annual";
        break;
      case "semiAnnual":
        rspPayFreqTextPath = "application.summary.semiAnnual";
        break;
      case "quarterly":
        rspPayFreqTextPath = "application.summary.quarterly";
        break;
      case "monthly":
        rspPayFreqTextPath = "application.summary.monthly";
        break;
      default:
        rspPayFreqTextPath = "";
        break;
    }

    // Top-up
    const topUpAmt = _.get(policyOptions, "topUpAmt");

    return (
      <EABTableFooter
        key={`${id}-footer`}
        tableConfig={[
          {
            key: `${id}-lastUpdateDate`,
            style: styles.tableFooterColumn1
          },
          {
            key: `${id}-footerValue`,
            style: styles.tableFooterColumn2
          }
        ]}
      >
        <View style={styles.tableFooterDateView}>
          <Text style={styles.tableFooterColumnDate}>
            {moment(lastUpdateDate).format("DD MMM YYYY HH:mm:ss")}
          </Text>
        </View>
        <View style={styles.tableFooterValueView}>
          <View style={styles.tableFooterDataView}>
            {topUpAmt > 0 ? (
              <TranslatedText
                style={styles.tableFooterDataTitle}
                language={language}
                path="application.summary.item_footer_topAmountSinglePremium"
              />
            ) : null}
            {topUpAmt > 0 ? (
              <Text style={styles.tableFooterDataValue} numberOfLines={1}>
                {`${sign}${numberToCurrency({ value: topUpAmt })}`}
              </Text>
            ) : null}
          </View>
          <View style={styles.tableFooterDataView}>
            {rspAmount > 0 ? (
              <TranslatedText
                style={styles.tableFooterDataTitle}
                language={language}
                path="application.summary.item_footer_rspAmount"
                replace={[{ value: rspPayFreqTextPath, isPath: true }]}
              />
            ) : null}
            {rspAmount > 0 ? (
              <Text style={styles.tableFooterDataValue} numberOfLines={1}>
                {`${sign}${numberToCurrency({ value: rspAmount })}`}
              </Text>
            ) : null}
          </View>
          <View style={styles.tableFooterDataView}>
            <TranslatedText
              style={styles.tableFooterDataTitle}
              language={language}
              path="application.summary.item_footer_totalPremium"
              replace={[{ value: paymentModeTextPath, isPath: true }]}
            />
            <Text style={styles.tableFooterDataValue} numberOfLines={1}>
              {`${sign}${numberToCurrency({ value: totPremium })}`}
            </Text>
          </View>
        </View>
      </EABTableFooter>
    );
  }

  /*
   * displayType:
   * 0 = planName, policyTerm, paymentTerm, sumAssured, [annualPremium / semiAnnualPremium / quarterlyPremium / monthlyPremium]
   * 1 = planName, policyTerm, paymentTerm, [annualPremium / semiAnnualPremium / quarterlyPremium / monthlyPremium], annualPremium
   * 2 = planName, policyTerm, paymentTerm, retirementIncome, [annualPremium / semiAnnualPremium / quarterlyPremium / monthlyPremium]
   * 3 is for ART
   * 3 = planName, policyTerm, paymentTerm, GuaranteedAnnualIncome, [annualPremium / semiAnnualPremium / quarterlyPremium / monthlyPremium]
   */

  render() {
    const { value } = this.props;
    const { id, baseProductCode } = value;

    let displayType;
    switch (baseProductCode) {
      case "AWT":
      case "PUL":
        displayType = SHOW_ANNUAL_PREMIUM;
        break;
      case "RHP":
        displayType = SHOW_RETIREMENT_INCOME;
        break;
      case "NPE":
        displayType = SHOW_GUARANTEED_ANNUAL_INCOME;
        break;
      case "ART2":
        displayType = SHOW_GUARANTEED_ANNUAL_INCOME;
        break;
      default:
        displayType = BASIC;
        break;
    }
    return (
      <ContextConsumer>
        {({ language }) => (
          <EABCard style={styles.container}>
            <EABTable
              tableConfig={[
                {
                  key: `${id}-plan_name`,
                  style: styles.tableSectionColumn1
                },
                {
                  key: `${id}-policy_term`,
                  style: styles.tableSectionColumn2
                },
                {
                  key: `${id}-payment_term`,
                  style: styles.tableSectionColumn3
                },
                {
                  key: `${id}-column4`,
                  style: styles.tableSectionColumn4
                },
                {
                  key: `${id}-column5`,
                  style: styles.tableSectionColumn5
                }
              ]}
            >
              {this.renderContentTableHeader(language, displayType)}
              {this.renderContentTableSection(language, displayType, true)}
              {this.renderContentTableSection(language, displayType, false)}
              {this.renderContentTableFooter(language)}
            </EABTable>
          </EABCard>
        )}
      </ContextConsumer>
    );
  }
}

ApplicationItemDetails.propTypes = {
  value: REDUCER_TYPE_CHECK[PRE_APPLICATION].ApplicationItem.value.isRequired,
  optionsMap: REDUCER_TYPE_CHECK[OPTIONS_MAP].isRequired
};

export default ApplicationItemDetails;
