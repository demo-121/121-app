import { ACTION_TYPES, REDUCER_TYPES } from "eab-web-api";
import { connect } from "react-redux";
import { TEXT_STORE } from "../../../constants/REDUCER_TYPES";
import HeaderLeft from "../components/HeaderLeft";

const { OPTIONS_MAP, CLIENT_FORM, CLIENT } = REDUCER_TYPES;

/**
 * HeaderLeft
 * @requires HeaderLeft - HeaderLeft UI
 * */
const mapStateToProps = state => ({
  clientForm: state[CLIENT_FORM],
  textStore: state[TEXT_STORE],
  optionsMap: state[OPTIONS_MAP],
  profile: state[CLIENT].profile,
  dependantProfiles: state[CLIENT].dependantProfiles
});

const mapDispatchToProps = dispatch => ({
  dispatch,
  reloadContactList(callback) {
    dispatch({
      type: ACTION_TYPES[CLIENT].GET_CONTACT_LIST,
      callback
    });
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(HeaderLeft);
