import * as _ from "lodash";
import { NativeModules } from "react-native";
import Config from "react-native-config";
import { DB } from "../constants/DB_NAME";
// test on ck macbook
// const remoteIP = "172.168.11.160";
// const remotePort = 4985;
// const dbName = "todo";
// const username = "admin";
// const password = "123456";

const dbName = DB;
// const dbName2 = DB_SYNC;

/**
 * set Encryption key for database encryption
 * @param {object} key - Encryption key
 * @param {object} isDeleteDB - flag for control whether database is deleted
 *   before set a new encryption key
 * @param {function} callback - callback after Encryption key is set
 * @return {undefined} no return result
 */
export const setEncryptionKey = (key, isDeleteDB, callback) => {
  NativeModules.CBLiteManager.setEncryptionKey(
    key,
    isDeleteDB,
    dbName,
    result => {
      if (callback) {
        callback(result);
      }
    }
  );
};

/**
 * delete cblite database in time bomb
 * @param {function} callback - return data
 * @return {undefined} no return result
 */
export const deleteDatabase = callback => {
  NativeModules.CBLiteManager.deleteDatabase(dbName, callbackL => {
    callback(callbackL);
  });
  // NativeModules.CBLiteManager.deleteDatabase(dbName2, callback);
};

/**
 * create document into couchbase lite
 * @param {object} docData - couchbase document object
 * @param {function} callback - callback after the document is created
 * @return {undefined} no return result
 */
export const createDocument = (docID, docData, callback) => {
  NativeModules.CBLiteManager.createDocument(dbName, docID, docData, () => {
    callback(callback);
  });
};
/**
 * get attachment by ID from couchbase lite
 * @param {string} docID - couchbase document id
 * @param {string} attName - attachment name
 * @param {function} callback - callback. data is null if no document is found
 * @return {undefined} no return result
 */
export const getAttachmentWithID = (docID, attName, callback) => {
  NativeModules.CBLiteManager.getAttachmentWithID(
    dbName,
    docID,
    attName,
    data => {
      if (callback) {
        callback(data);
      }
    }
  );
};

/**
 * get document by ID from couchbase lite
 * @param {string} docID - couchbase document id
 * @param {function} callback - callback. data is null if no document is found
 * @return {undefined} no return result
 */
export const getDocumentWithID = (docID, callback) => {
  NativeModules.CBLiteManager.getDocumentWithID(dbName, docID, data => {
    if (callback) {
      callback(data);
    }
  });
};

/**
 * update document by ID from couchbase lite
 * @param {string} docID - couchbase document id
 * @param {object} docData - couchbase document object
 * @param {function} callback - callback. data will return false if error is
 *   happened when document update
 * @return {undefined} no return result
 */
export const updateDocumentWithID = (docID, docData, callback) => {
  const saveData = _.cloneDeep(docData);
  delete saveData._id;
  delete saveData._rev;

  NativeModules.CBLiteManager.updateDocumentWithID(
    dbName,
    docID,
    saveData,
    data => {
      if (callback) {
        callback(data);
      }
    }
  );
};
/**
 * write attachment into couchbase lite
 * @param {string} docID - couchbase document id
 * @param {object} docData - couchbase document object
 * @param {function} callback - callback. data will return false if error is
 *   happened when document update
 * @return {undefined} no return result
 */
export const uploadAttachmentWithBase64 = (
  docID,
  attName,
  mime,
  base64Data,
  callback
) => {
  NativeModules.CBLiteManager.uploadAttachmentWithBase64(
    dbName,
    docID,
    attName,
    mime,
    base64Data,
    data => {
      if (callback) {
        callback(data);
      }
    }
  );
};

/**
 * delete attachment by attName from couchbase lite
 * @param {string} docID - couchbase document id
 * @param {string} attName - attachment name
 * @param {function} callback - callback. data is true if deletion is succeed
 * @return {undefined} no return result
 */
export const deleteAttachmentWithAttName = (docID, attName, callback) => {
  NativeModules.CBLiteManager.deleteAttachmentWithAttName(
    dbName,
    docID,
    attName,
    data => {
      if (callback) {
        callback(data);
      }
    }
  );
};

/**
 * delete document by ID from couchbase lite
 * @param {string} docID - couchbase document id
 * @param {function} callback - callback. data is true if deletion is succeed
 * @return {undefined} no return result
 */
export const deleteDocumentWithID = (loginId, docID, callback) => {
  NativeModules.CBLiteManager.deleteDocumentWithID(
    dbName,
    loginId,
    docID,
    data => {
      if (callback) {
        callback(data);
      }
    }
  );
};

/**
 * create couchbase views by js
 * @param {object} viewsMap - a set of functions in the object
 * @param {function} callback - return summary list of views created
 * @return {undefined} no return result
 */
export const createViewsByJs = (viewsMap, callback) => {
  NativeModules.CBLiteManager.createViewsByJs(
    dbName,
    viewsMap,
    `${global.viewVersionNum}`,
    callback
  );
};

/**
 * query data by using existing view
 * @param {string} emitViewNamed - view name
 * @param {object} params - emit key-value for the view
 * @param {function} callback - return data
 * @return {undefined} no return result
 */
export const queryByExistingView = (emitViewNamed, params, callback) => {
  NativeModules.CBLiteManager.queryByExistingView(
    dbName,
    emitViewNamed,
    params,
    callback
  );
};

/**
 * similar to queryByExistingView(), special handing for applications count in client search
 * @param {object} params - emit key-value for the view
 * @param {function} callback - return data
 * @return {undefined} no return result
 */
export const applicationsCount = (params, callback) => {
  NativeModules.CBLiteManager.applicationsCount(
    dbName,
    "applicationsCount",
    params,
    callback
  );
};

/**
 * query data by using existing js view
 * @param {string} emitViewNamed - view name
 * @param {string} jsMapFunc - js mapper function string which will be evaluated
 * @param {string} emitVersionAsString - emit version
 * @param {object} params - couchbase CBLQuery params
 * @param {string} [params.startkey] - couchbase CBLQuery startkey
 * @param {string} [params.endkey] - couchbase CBLQuery endkey
 * @param {function} callback - return data
 * @return {undefined} no return result
 */
export const queryViewByJs = (
  emitViewNamed,
  jsMapFunc,
  emitVersionAsString,
  params,
  callback
) => {
  NativeModules.CBLiteManager.queryViewByJs(
    dbName,
    emitViewNamed,
    jsMapFunc,
    emitVersionAsString,
    params,
    callback
  );
};

/**
 * use couchbase View to query data
 * Please practice "Views Best Practices" document in couchbase official
 * website
 * (https://developer.couchbase.com/documentation/server/current/views/views-writing-views.html)
 * @param {String} emitViewNamed - view name (* it will cache in database at
 *   the first initialization)
 * @param {String} emitDocKey - emit key (* it will cache in database at the
 *   first initialization)
 * @param {array} emitDocValues - array of string which are the value keys (*
 *   it will cache in database at the first initialization)
 * @param {String} emitVersion - view version, increment if emitDocKey and
 *   emitDocValues are updated.
 * @param {function} callback - callback after data is queried
 */
export const queryView = (
  emitViewNamed,
  emitDocKey,
  emitDocValues,
  emitVersion,
  callback
) => {
  NativeModules.CBLiteManager.queryView(
    dbName,
    emitViewNamed,
    emitDocKey,
    emitDocValues,
    emitVersion,
    result => {
      if (callback) {
        callback(result);
      }
    }
  );
};

/**
 * it is data sync function
 * @param {String} environment - it is selected environment at the first initialization
 * @param {String} agentCode - login agent
 * @param {String} authToken - token
 * @param {function} callback - callback function
 */
export const dataSync = (
  environment,
  agentCode,
  loginId,
  authToken,
  isLeftMenu,
  isBackground,
  sessionCookieSyncGatewaySession,
  sessionCookieExpiryDate,
  sessionCookiePath,
  syncDocumentIds,
  callback
) => {
  // the default environment = DEV
  let _remoteURL = "http://sync.ease-sit.intraeab/axasgsit";
  let _username = "";
  let _password = "";
  let _webServiceUrl = "http://app.ease-sit.intraeab";

  if (environment && environment.length > 0) {
    if (environment === "PROD") {
      _remoteURL =
        Config.SYNC_GATEWAY_BASIC_URL +
        (Number(Config.SYNC_GATEWAY_PORT) === 80
          ? `/${Config.SYNC_GATEWAY_DB}`
          : `:${Config.SYNC_GATEWAY_PORT}/${Config.SYNC_GATEWAY_DB}`);
      _username = Config.SYNC_GATEWAY_USERNAME;
      _password = Config.SYNC_GATEWAY_PASSWORD;
      _webServiceUrl = Config.WEB_SERVICE_BASIC_URL;
    } else if (environment === "DEV") {
      _remoteURL =
        Config.DEV_SYNC_GATEWAY_BASIC_URL +
        (Number(Config.DEV_SYNC_GATEWAY_PORT) === 80
          ? `/${Config.DEV_SYNC_GATEWAY_DB}`
          : `:${Config.DEV_SYNC_GATEWAY_PORT}/${Config.DEV_SYNC_GATEWAY_DB}`);
      _username = Config.DEV_SYNC_GATEWAY_USERNAME;
      _password = Config.DEV_SYNC_GATEWAY_PASSWORD;
      _webServiceUrl = Config.DEV_WEB_SERVICE_BASIC_URL;
    } else if (environment === "SIT1_EAB") {
      _remoteURL =
        Config.SIT1_EAB_SYNC_GATEWAY_BASIC_URL +
        (Number(Config.SIT1_EAB_SYNC_GATEWAY_PORT) === 80
          ? `/${Config.SIT1_EAB_SYNC_GATEWAY_DB}`
          : `:${Config.SIT1_EAB_SYNC_GATEWAY_PORT}/${
              Config.SIT1_EAB_SYNC_GATEWAY_DB
            }`);
      _username = Config.SIT1_EAB_SYNC_GATEWAY_USERNAME;
      _password = Config.SIT1_EAB_SYNC_GATEWAY_PASSWORD;
      _webServiceUrl = Config.SIT1_EAB_WEB_SERVICE_BASIC_URL;
    } else if (environment === "SIT2_EAB") {
      _remoteURL =
        Config.SIT2_EAB_SYNC_GATEWAY_BASIC_URL +
        (Number(Config.SIT2_EAB_SYNC_GATEWAY_PORT) === 80
          ? `/${Config.SIT2_EAB_SYNC_GATEWAY_DB}`
          : `:${Config.SIT2_EAB_SYNC_GATEWAY_PORT}/${
              Config.SIT2_EAB_SYNC_GATEWAY_DB
            }`);
      _username = Config.SIT2_EAB_SYNC_GATEWAY_USERNAME;
      _password = Config.SIT2_EAB_SYNC_GATEWAY_PASSWORD;
      _webServiceUrl = Config.SIT2_EAB_WEB_SERVICE_BASIC_URL;
    } else if (environment === "SIT3_EAB") {
      _remoteURL =
        Config.SIT3_EAB_SYNC_GATEWAY_BASIC_URL +
        (Number(Config.SIT3_EAB_SYNC_GATEWAY_PORT) === 80
          ? `/${Config.SIT3_EAB_SYNC_GATEWAY_DB}`
          : `:${Config.SIT3_EAB_SYNC_GATEWAY_PORT}/${
              Config.SIT3_EAB_SYNC_GATEWAY_DB
            }`);
      _username = Config.SIT3_EAB_SYNC_GATEWAY_USERNAME;
      _password = Config.SIT3_EAB_SYNC_GATEWAY_PASSWORD;
      _webServiceUrl = Config.SIT3_EAB_WEB_SERVICE_BASIC_URL;
    } else if (environment === "SIT_AXA") {
      _remoteURL =
        Config.SIT_AXA_SYNC_GATEWAY_BASIC_URL +
        (Number(Config.SIT_AXA_SYNC_GATEWAY_PORT) === 80
          ? `/${Config.SIT_AXA_SYNC_GATEWAY_DB}`
          : `:${Config.SIT_AXA_SYNC_GATEWAY_PORT}/${
              Config.SIT_AXA_SYNC_GATEWAY_DB
            }`);
      _username = Config.SIT_AXA_SYNC_GATEWAY_USERNAME;
      _password = Config.SIT_AXA_SYNC_GATEWAY_PASSWORD;
      _webServiceUrl = Config.SIT_AXA_WEB_SERVICE_BASIC_URL;
    } else if (environment === "UAT") {
      _remoteURL =
        Config.UAT_SYNC_GATEWAY_BASIC_URL +
        (Number(Config.UAT_SYNC_GATEWAY_PORT) === 80
          ? `/${Config.UAT_SYNC_GATEWAY_DB}`
          : `:${Config.UAT_SYNC_GATEWAY_PORT}/${Config.UAT_SYNC_GATEWAY_DB}`);
      _username = Config.UAT_SYNC_GATEWAY_USERNAME;
      _password = Config.UAT_SYNC_GATEWAY_PASSWORD;
      _webServiceUrl = Config.UAT_WEB_SERVICE_BASIC_URL;
    } else if (environment === "PRE_PROD") {
      _remoteURL =
        Config.PRE_PROD_SYNC_GATEWAY_BASIC_URL +
        (Number(Config.PRE_PROD_SYNC_GATEWAY_PORT) === 80
          ? `/${Config.PRE_PROD_SYNC_GATEWAY_DB}`
          : `:${Config.PRE_PROD_SYNC_GATEWAY_PORT}/${
              Config.PRE_PROD_SYNC_GATEWAY_DB
            }`);
      _username = Config.PRE_PROD_SYNC_GATEWAY_USERNAME;
      _password = Config.PRE_PROD_SYNC_GATEWAY_PASSWORD;
      _webServiceUrl = Config.PRE_PROD_WEB_SERVICE_BASIC_URL;
    }

    if (!(syncDocumentIds instanceof Array)) {
      syncDocumentIds = [];
    }

    // show the data sync view
    NativeModules.CBLiteManager.dataSync(
      _remoteURL,
      dbName,
      _username,
      _password,
      agentCode,
      loginId,
      _webServiceUrl,
      authToken,
      isLeftMenu,
      isBackground,
      sessionCookieSyncGatewaySession
        ? "sessionCookieSyncGatewaySession"
        : "sessionCookieSyncGatewaySession",
      sessionCookieExpiryDate ? 0 : 0,
      sessionCookiePath ? "sessionCookiePath" : "sessionCookiePath",
      syncDocumentIds,
      callback
    );
  }
};

/**
 * get the sync gateway password
 * @param {String} authToken - token
 * @param {function} callback - callback function
 */
export const getSyncGatewayPwd = (environment, authToken, callback) => {
  let _url = "http://app.ease-sit3.intraeab/dataSync/accessSG";
  if (environment && environment.length > 0) {
    if (environment === "SIT1_EAB") {
      _url = "http://app.ease-sit.intraeab/dataSync/accessSG";
    } else if (environment === "SIT2_EAB") {
      _url = "http://app.ease-sit2.intraeab/dataSync/accessSG";
    } else if (environment === "SIT3_EAB") {
      _url = "http://app.ease-sit3.intraeab/dataSync/accessSG";
    } else if (environment === "SIT_AXA") {
      _url = "https://app.ease-sit.axa.com.sg/dataSync/accessSG";
    } else if (environment === "UAT") {
      _url = "https://app.ease-uat.axa.com.sg/dataSync/accessSG";
    } else if (environment === "PRE_PROD") {
      _url = "https://app.ease-preprd.axa.com.sg/dataSync/accessSG";
    } else {
      _url = "http://app.ease-sit3.intraeab/dataSync/accessSG";
    }
    NativeModules.CBLiteManager.getSyncGatewayPwd(_url, authToken, callback);
  }
};

/**
 * replication will keep check and process on the backgrond,
 * suggest that the function call on componentDidMount()
 *  (no side effects even if this function is called many times)
 * @return {undefined} no return result
 */
export const genTestData = callback => {
  NativeModules.CBLiteManager.genTestData(dbName, data => {
    if (callback) {
      callback(data);
    }
  });
};

/**
 * get all applications by CID from couchbase lite
 * @param {string} CID - client id
 * @param {function} callback - callback. data is null if no document is found
 * @return {object} all application in object
 */
export const getAllApplicationsWithCID = (CID, callback) => {
  NativeModules.CBLiteManager.getAllApplicationsWithCID(dbName, CID, data => {
    if (callback) {
      callback(data);
    }
  });
};

export default {
  createDocument,
  getAttachmentWithID,
  getDocumentWithID,
  updateDocumentWithID,
  uploadAttachmentWithBase64,
  deleteDocumentWithID,
  dataSync,
  queryView,
  createViewsByJs,
  queryViewByJs,
  genTestData,
  getSyncGatewayPwd
};
