import { StyleSheet } from "react-native";
import Theme from "../../../../theme";

const toolBarSectionA = {
  flex: 1,
  alignItems: "flex-start"
};

export default StyleSheet.create({
  toolBarSectionA,
  toolBarSectionB: {
    ...toolBarSectionA,
    alignItems: "center"
  },
  toolBarSectionC: {
    ...toolBarSectionA,
    alignItems: "flex-end"
  },
  riderDialogToolBar: {
    alignItems: "flex-end",
    flexDirection: "row",
    flexBasis: "auto",
    paddingHorizontal: Theme.alignmentXL,
    paddingBottom: Theme.alignmentL,
    backgroundColor: Theme.white,
    height: 64,
    width: "100%",
    shadowColor: "rgba(0, 0, 0, 0.3)",
    shadowOffset: {
      width: 0,
      height: 0.5
    },
    shadowRadius: 0,
    shadowOpacity: 1
  },
  riderDialogScrollView: {
    flex: 1,
    width: "100%"
  },
  riderDialogContentContainerStyle: {
    paddingHorizontal: Theme.alignmentXL,
    paddingVertical: Theme.alignmentXS
  }
});
