### PopoverDatePicker sample code snippets

```javascript
class Sample extends Component {
  constructor(props, context) {
    super(props, context)
    this.state = {
      popoverVisible: false,
      targetNode: null,
      pLanguage: null,
    }
    this.inputRef = React.createRef();
  }

  _onPresent(event) {
    this.setState({
      popoverVisible: true,
      targetNode: findNodeHandle(this.inputRef.current),
    })
  }

  onShow() {
  }

  onHide() {
    this.setState({
      popoverVisible: false,
    })
  }

  renderWrapperPopover() {
    return (
      <PopoverPicker
        popoverOptions={{
          sourceView: this.state.targetNode,
          onShow: this.onShow.bind(this),
          onHide: this.onHide.bind(this),
          preferredContentSize: [400, 200],
          permittedArrowDirections: [0],
        }}
        pickerOptions={{
          selectedValue: this.state.pLanguage,
          onValueChange: itemValue => this.setState({ pLanguage: itemValue }),
          style: [{ height: 50, width: 100 }],
          items: [
            { label: 'Java', value: 'java' },
            { label: 'JavaScript', value: 'js' },
            { label: 'A1', value: 'a1' },
            { label: 'A2', value: 'a2' },
            { label: 'A3', value: 'a3' },
            { label: 'A4', value: 'a4' },
            { label: 'A5', value: 'a5' },
            { label: 'A6', value: 'a6' },
            { label: 'A7', value: 'a7' },
            { label: 'A8', value: 'a8' },
          ],
        }}
      />
    )
  }

  render() {
    return (
      <View>
        <TouchableOpacity ref={this.inputRef} onPress={this._onPresent.bind(this)}>
          <Text>Popover</Text>
        </TouchableOpacity>
        {this.state.popoverVisible && this.renderWrapperPopover()}
      </View>
    )
  }
}
```