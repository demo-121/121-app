import PropTypes from "prop-types";
import React, { PureComponent } from "react";
import SafariView from "react-native-safari-view";
import Config from "react-native-config";
import {
  Image,
  KeyboardAvoidingView,
  Text,
  View,
  Alert,
  Linking,
  TouchableWithoutFeedback
} from "react-native";
import LinearGradient from "react-native-linear-gradient";
import { ContextConsumer } from "../../../../context";
import { dataSync } from "../../../../utilities/DataManager";
import axaLogo from "../../../../assets/images/axaLogo.png";
import { POLICY_NUMBER_ERROR } from "../../../../constants/ERROR_TYPE";
import EABButton from "../../../../components/EABButton";
import { RECTANGLE_BUTTON_1 } from "../../../../components/EABButton/constants";
import EABTextField from "../../../../components/EABTextField";
import EABHUD from "../../../../containers/EABHUD";
import Theme from "../../../../theme";
import styles from "./styles";
import {
  VERSION_NUMBER,
  BUILD_NUMBER
} from "../../../../constants/BUILD_CONFIG";
import {
  TYPE_FIRST_WARNING,
  TYPE_SECOND_WARNING,
  TYPE_TIME_BOMB
} from "../../../../constants/TIME_BOMB";
import { APP_PASSWORD_STAGE_3 } from "../../../../constants/ERROR_MSG";
import TranslatedText from "../../../../containers/TranslatedText";
/**
 * <AppPasswordLogin />
 * @param {function} loginByAppPassword - method for login app password
 * @param {object} text - UI text store object
 * @param {string} text.passwordDoNotMeetPasswordPolicy - password do not meet password policy text
 * */
export default class AppPasswordLogin extends PureComponent {
  constructor(props) {
    super(props);
    const { environment } = this.props;
    if (Config.ENV_NAME === "PROD") {
      this.path = Config.GET_AUTH_CODE_FULL_URL;
    } else if (environment === "DEV") {
      this.path = Config.DEV_GET_AUTH_CODE_FULL_URL;
    } else if (environment === "SIT1_EAB") {
      this.path = Config.SIT1_EAB_GET_AUTH_CODE_FULL_URL;
    } else if (environment === "SIT2_EAB") {
      this.path = Config.SIT2_EAB_GET_AUTH_CODE_FULL_URL;
    } else if (environment === "SIT3_EAB") {
      this.path = Config.SIT3_EAB_GET_AUTH_CODE_FULL_URL;
    } else if (environment === "SIT_AXA") {
      this.path = Config.SIT_AXA_GET_AUTH_CODE_FULL_URL;
    } else if (environment === "UAT") {
      this.path = Config.UAT_GET_AUTH_CODE_FULL_URL;
    } else if (environment === "PRE_PROD") {
      this.path = Config.PRE_PROD_GET_AUTH_CODE_FULL_URL;
    } else {
      this.path = Config.SIT3_EAB_GET_AUTH_CODE_FULL_URL;
    }

    this.state = {
      pw: "",
      timeBombType: null,
      daysLeftBeforeTimebomb: null
    };
    this.loginByAppPassword = this.loginByAppPassword.bind(this);
    this.pressHandler = this.pressHandler.bind(this);
    if (this.props.userID && this.props.userID.length > 0) {
      this.userID = this.props.userID;
    }
    if (this.props.environment === "DEV") {
      this.userID = "alex.tang";
      // this.loginByAppPassword();
    }
  }
  componentDidMount() {
    this.props.checkFailAttempt(failAttemptCallback => {
      if (Number(failAttemptCallback) === 5) {
        setTimeout(() => {
          Alert.alert(
            "Warning",
            APP_PASSWORD_STAGE_3,
            [
              {
                text: "OK",
                onPress: () => {
                  this.props.exceedFailLoginAttempts();
                },
                style: "cancel"
              }
            ],
            { cancelable: false }
          );
        }, 200);
      }
    });
    const title = "Online Auth is needed for Data Sync";
    const tilteTimeBomb = "Time Bomb";
    const warningMessage2 =
      "The app has been locked as you have not synced your data recently. Please uninstall and install the app again.";
    Linking.addEventListener("url", this.onlineAuthCallback);
    if (
      this.props.loginFlowCheck === "" &&
      this.props.loginFlow !== "activation"
    ) {
      this.props.setLoginFlow("local_auth");
      this.props.checkAppLocked(isLocked => {
        if (isLocked) {
          setTimeout(() => {
            Alert.alert(tilteTimeBomb, warningMessage2);
          }, 500);
        } else {
          this.props.checkTimeBomb(showAlert => {
            if (showAlert) {
              if (!showAlert.hasError) {
                if (showAlert.result === TYPE_TIME_BOMB) {
                  this.setState({
                    timeBombType: TYPE_TIME_BOMB
                  });
                  setTimeout(() => {
                    Alert.alert(
                      `The app has been locked as you have not synced your data recently. Please uninstall and install the app again.`
                    );
                  }, 500);
                } else if (showAlert.result === TYPE_SECOND_WARNING) {
                  const daysLeft = showAlert.daysLeft
                    ? showAlert.daysLeft
                    : null;
                  // 8th days warning
                  this.setState({
                    timeBombType: TYPE_SECOND_WARNING,
                    daysLeftBeforeTimebomb: daysLeft
                  });
                  setTimeout(() => {
                    Alert.alert(
                      title,
                      `App is locked because Timebomb for this App will explode in ${daysLeft} day(s). Please perform data sync first`,
                      [
                        {
                          text: "Online Auth",
                          onPress: () => {
                            this.props.navigateToOnlineAuth(true);
                          },
                          style: "destructive"
                        }
                      ],
                      { cancelable: false }
                    );
                  }, 500);
                } else if (showAlert.result === TYPE_FIRST_WARNING) {
                  const daysLeft = showAlert.daysLeft
                    ? showAlert.daysLeft
                    : null;
                  this.setState({
                    timeBombType: TYPE_FIRST_WARNING,
                    daysLeftBeforeTimebomb: daysLeft
                  });
                } else {
                  // no need to show the alert
                }
              }
            }
          });
        }
      });
    }
  }
  componentWillUnmount() {
    Linking.removeEventListener("url", this.onlineAuthCallback);
  }
  onlineAuthCallback(response) {
    const { setOnlineAuthCodeForPayment } = this.props;
    if (response.url) {
      const urlGet = response.url;
      SafariView.dismiss();
      const segment1 = urlGet.replace("easemobile://?", "");
      const variables = segment1.split("&");
      const codeString = variables.find(variable => variable.includes("code"));
      const [, authCode] = codeString.split("=");
      if (!this.props.isHandlingListener) {
        setOnlineAuthCodeForPayment({
          authCode,
          callback: callback => {
            if (callback.hasError) {
              if (callback.errorMsg) {
                setTimeout(() => {
                  Alert.alert(`[Error] ${callback.errorMsg}`);
                }, 200);
              } else {
                setTimeout(() => {
                  Alert.alert(
                    `[Error] Authorization for submission Failed (Error 001)`
                  );
                }, 200);
              }
            } else {
              const { isOnlineAuthCompleted } = this.props;
              if (isOnlineAuthCompleted) {
                dataSync(
                  this.props.login.environment,
                  this.props.agent.agentCode,
                  this.props.login.profileId,
                  `Bearer ${this.props.login.authToken}`,
                  false,
                  false,
                  this.props.login.sessionCookieSyncGatewaySession,
                  this.props.login.sessionCookieExpiryDate,
                  this.props.login.sessionCookiePath,
                  this.props.login.syncDocumentIds,
                  () => {}
                );
              }
            }
          }
        });
      }
    }
  }
  loginByAppPassword() {
    if (this.props.environment === "DEV") {
      if (this.props.loginFlow === "activation") {
        this.props.saveNewAppPasswordDEV("Password3#", errorResult => {
          if (errorResult.errorType === POLICY_NUMBER_ERROR) {
            setTimeout(() => {
              Alert.alert(
                `[Error] Policy number exhausted, please connect your device to online mode and login again.`
              );
            }, 200);
          } else if (errorResult.error) {
            setTimeout(() => {
              Alert.alert(`[Error] ${errorResult.error}`);
            }, 200);
          } else if (errorResult.errorMsg) {
            setTimeout(() => {
              Alert.alert(`[Error]${errorResult.errorMsg}`);
            }, 200);
          } else {
            setTimeout(() => {
              Alert.alert(`[Error] Authorization Failed (Error 005)`);
            }, 200);
          }
        });
      } else {
        this.props.loginByAppPasswordDEV(
          this.userID,
          "Password3#",
          errorResult => {
            if (errorResult.errorMsg) {
              setTimeout(() => {
                Alert.alert(`[Error]${errorResult.errorMsg}`);
              }, 200);
            }
          }
        );
      }
    } else if (this.userID && this.userID.length > 0 && this.state.pw) {
      if (this.props.loginByAppPassword != null) {
        this.props.loginByAppPassword(
          this.userID,
          this.state.pw,
          errorResult => {
            if (this.state.timeBombType === TYPE_TIME_BOMB) {
              setTimeout(() => {
                Alert.alert(
                  `The app has been locked as you have not synced your data recently. Please uninstall and install the app again.`
                );
              }, 500);
            } else if (errorResult.hasError) {
              if (errorResult.error) {
                if (errorResult.error === APP_PASSWORD_STAGE_3) {
                  setTimeout(() => {
                    Alert.alert(
                      "Warning",
                      APP_PASSWORD_STAGE_3,
                      [
                        {
                          text: "OK",
                          onPress: () => {
                            this.props.exceedFailLoginAttempts();
                          },
                          style: "cancel"
                        }
                      ],
                      { cancelable: false }
                    );
                  }, 200);
                } else {
                  setTimeout(() => {
                    Alert.alert(`[Error] ${errorResult.error}`);
                  }, 200);
                }
              } else if (errorResult.errorMsg) {
                setTimeout(() => {
                  Alert.alert(`[Error] ${errorResult.errorMsg}`);
                }, 200);
              } else {
                setTimeout(() => {
                  Alert.alert(`[Error] Authorization Failed (Error 002)`);
                }, 200);
              }
            } else if (this.state.timeBombType === TYPE_SECOND_WARNING) {
              // Do Nothing for latest implementation
            } else if (this.state.timeBombType === TYPE_FIRST_WARNING) {
              const daysLeft = this.state.daysLeftBeforeTimebomb;
              setTimeout(() => {
                Alert.alert(
                  `Time Bomb for this App will explode in ${daysLeft} day(s). Please connect to internet and Login`
                );
              }, 500);
            }
            if (!this.props.isTimezoneValid) {
              setTimeout(() => {
                Alert.alert(
                  "Warning",
                  `This app requires your device to be in Singapore time zone. Please change the time zone to Singapore and log in again.`,
                  [
                    {
                      text: "OK",
                      onPress: () => {
                        this.props.logout();
                      },
                      style: "cancel"
                    }
                  ],
                  { cancelable: false }
                );
              }, 200);
            }
            if (
              this.props.agent.role !== "04" &&
              !this.props.agent.managerCode &&
              this.props.agent.rawData &&
              this.props.agent.rawData.upline1Code &&
              this.props.agent.agentCode !==
                this.props.agent.rawData.upline1Code
            ) {
              setTimeout(() => {
                Alert.alert(
                  `[Error] You won't be able to submit case as there is no supervisor information linked to you. Please contact your firm's administrator.`
                );
              }, 200);
            }
          }
        );
      }
    }
  }

  pressHandler() {
    SafariView.isAvailable()
      .then(
        SafariView.show({
          url: this.path
          // url: "eabtest://"
        })
      )
      .catch(error => {
        // Fallback WebView code for iOS 8 and earlier
        setTimeout(() => {
          Alert.alert(`[Error]${error}`);
        }, 200);
      });
  }
  render() {
    if (this.props.isAppLocked) {
      return null;
    }
    return (
      <ContextConsumer>
        {({ showPlayGround }) => (
          <LinearGradient
            {...Theme.LinearGradientProps}
            style={styles.container}
          >
            <KeyboardAvoidingView behavior="padding">
              <View style={styles.loginForm}>
                <View style={styles.logoWrapper}>
                  <TouchableWithoutFeedback
                    onPress={
                      process.env.NODE_ENV !== "production"
                        ? showPlayGround
                        : () => {}
                    }
                  >
                    <View>
                      <Image style={styles.logo} source={axaLogo} />
                    </View>
                  </TouchableWithoutFeedback>
                  <Text style={Theme.tagLine1Primary}>Welcome to EAB</Text>
                </View>
                <View style={styles.idWrapper}>
                  <Text testID="lblUserID" style={Theme.bodyPrimary}>
                    {this.userID}
                  </Text>
                </View>
                <EABTextField
                  placeholder="App Password"
                  testID="txtPasswordId"
                  secureTextEntry
                  value={this.state.pw}
                  onChange={pw => {
                    this.setState({
                      pw
                    });
                  }}
                />
                <EABButton
                  containerStyle={styles.loginButton}
                  buttonType={RECTANGLE_BUTTON_1}
                  onPress={this.loginByAppPassword}
                  testID="btnLogIn"
                >
                  <TranslatedText path="login.LogIn" />
                </EABButton>
                <Text style={Theme.subheadSecondary}>
                  {VERSION_NUMBER}-b{BUILD_NUMBER}
                </Text>
              </View>
              <EABHUD />
            </KeyboardAvoidingView>
          </LinearGradient>
        )}
      </ContextConsumer>
    );
  }
}

AppPasswordLogin.propTypes = {
  isAppLocked: PropTypes.bool,
  checkAppLocked: PropTypes.func.isRequired,
  checkFailAttempt: PropTypes.func,
  exceedFailLoginAttempts: PropTypes.func,
  login: PropTypes.oneOfType([PropTypes.object]).isRequired,
  agent: PropTypes.oneOfType([PropTypes.object]).isRequired,
  isOnlineAuthCompleted: PropTypes.bool.isRequired,
  setOnlineAuthCodeForPayment: PropTypes.func.isRequired,
  isHandlingListener: PropTypes.bool.isRequired,
  loginByOnlineAuthForPayment: PropTypes.func.isRequired,
  checkTokenExpiryDate: PropTypes.func.isRequired,
  loginFlow: PropTypes.string,
  navigateToOnlineAuth: PropTypes.func.isRequired,
  navigateToOnlineLoginPage: PropTypes.func.isRequired,
  loginFlowCheck: PropTypes.string.isRequired,
  setLoginFlow: PropTypes.func.isRequired,
  loginByAppPassword: PropTypes.func.isRequired,
  loginByAppPasswordDEV: PropTypes.func.isRequired,
  saveNewAppPasswordDEV: PropTypes.func.isRequired,
  checkTimeBomb: PropTypes.func.isRequired,
  environment: PropTypes.string.isRequired,
  userID: PropTypes.string.isRequired,
  logout: PropTypes.func.isRequired,
  isTimezoneValid: PropTypes.bool.isRequired,
  text: PropTypes.shape({
    passwordDoNotMeetPasswordPolicy: PropTypes.string.isRequired
  }).isRequired
};
AppPasswordLogin.defaultProps = {
  checkFailAttempt: "",
  exceedFailLoginAttempts: "",
  isAppLocked: false,
  loginFlow: ""
};
