import { StyleSheet } from "react-native";
import Theme from "../../../../theme";

const analysisProductHintBase = {
  color: Theme.grey,
  fontSize: Theme.fontSizeS
};

const labelText = Theme.bodySecondary;

export default StyleSheet.create({
  listScrollView: {
    flexBasis: "auto",
    flex: 0,
    flexGrow: 0,
    width: 312,
    height: "100%",
    borderRightWidth: 1,
    borderColor: Theme.lightGrey
  },
  mainContent: {
    flex: 1,
    height: "100%"
  },
  scrollView: {
    flex: 1,
    width: "100%"
  },
  keyboardView: {
    height: "100%",
    width: "100%"
  },
  firTabfirSectionTitle: {
    fontSize: Theme.fontSizeXXM,
    width: Theme.textMaxWidth,
    marginTop: Theme.alignmentXL
  },
  needsFirstSectionDesc: {
    marginTop: Theme.alignmentXL,
    fontSize: Theme.fontSizeXXM
  },
  needsProductsCard: {
    width: 212,
    height: 120
  },
  needsProductsView: {
    marginTop: Theme.alignmentXL,
    flexDirection: "row"
  },
  needsProductsTextView: {
    flexDirection: "column"
  },
  needsProductsText: {
    marginLeft: Theme.alignmentXL,
    fontSize: Theme.fontSizeXXXM
  },
  needsProductsButtonView: {
    flexDirection: "row",
    marginTop: Theme.alignmentL,
    marginLeft: Theme.alignmentXL
  },
  needsProductsButton: {
    marginTop: Theme.alignmentL,
    marginLeft: Theme.alignmentXL
  },
  needsProductsButtonText: {
    marginTop: Theme.alignmentXS,
    marginLeft: Theme.alignmentM,
    fontSize: Theme.fontSizeXS
  },
  analysisTopView: {
    width: "100%",
    height: "100%",
    flexDirection: "row"
  },
  title: {
    fontSize: 26,
    fontWeight: "bold",
    marginTop: 10
  },
  titleRow: {
    flexDirection: "row",
    alignItems: "flex-end"
  },
  analysisLeftColumn: {
    height: "100%",
    flex: 1,
    flexDirection: "column",
    width: 416,
    alignItems: "flex-start"
  },
  analysisLeftTitle: {
    fontSize: Theme.fontSizeXM,
    marginTop: Theme.alignmentXL,
    marginLeft: Theme.alignmentXL,
    marginBottom: Theme.alignmentM,
    color: Theme.brand
  },
  analysisProductActivatedButton: {
    height: 64,
    width: "100%",
    alignItems: "flex-start",
    backgroundColor: Theme.brandSuperTransparent
  },
  analysisProductDisActivateButton: {
    height: 64,
    width: "100%",
    alignItems: "flex-start",
    backgroundColor: Theme.white
  },
  analysisProductButtonIcon: {
    marginTop: Theme.alignmentM,
    marginLeft: Theme.alignmentXL
  },
  analysisProductButtonText: {
    marginTop: Theme.alignmentM,
    marginLeft: Theme.alignmentM
  },
  analysisDependantButtonView: {
    flexBasis: "auto",
    alignItems: "center",
    height: Theme.alignmentXXXL,
    borderBottomWidth: 1,
    borderColor: Theme.lightGrey
  },
  analysisDependantButton: {
    height: 64,
    width: 228,
    color: Theme.brand,
    marginTop: Theme.alignmentS,
    marginBottom: Theme.alignmentS
  },
  analysisProductContent: {
    flex: 1,
    marginHorizontal: Theme.alignmentXL
  },
  analysisProductPointTitle: {
    marginTop: Theme.alignmentXL,
    fontSize: Theme.fontSizeXXXM
  },
  analysisProductPointContent: {
    fontSize: Theme.fontSizeL,
    marginVertical: Theme.fontSizeXXM
  },
  analysisProductHint1: {
    ...analysisProductHintBase,
    marginTop: 17
  },
  analysisTextInputShort: {
    width: 320,
    marginTop: Theme.alignmentL
  },
  analysisTextInputLong: {
    width: "100%",
    marginTop: Theme.alignmentL
  },
  analysisProductPointText: {
    fontSize: Theme.fontSizeXXM,
    marginTop: Theme.alignmentL
  },
  analysisProductTextBox: {
    marginTop: Theme.alignmentL
  },
  requiredHint: {
    ...labelText,
    paddingLeft: Theme.alignmentXXS,
    color: Theme.negative
  },
  analysisSectionBorderLine: {
    borderTopWidth: 3,
    borderColor: Theme.lightGrey,
    marginTop: Theme.alignmentXXL,
    marginBottom: Theme.alignmentXL
  },
  analysisSelectorHalfWidth: {
    width: "48.5%"
  },
  analysisTextFieldHalfWidth: {
    marginLeft: Theme.alignmentXL,
    width: "35%",
    marginRight: Theme.alignmentXS,
    position: "relative"
  },
  analysisAssetsWrapper: {
    marginTop: 18
  },
  analysisRemoveButton: {
    marginTop: Theme.alignmentXL,
    width: 90,
    height: 36
  },
  analysisAddAssetButton: {
    marginTop: Theme.alignmentXL,
    width: 352
  },
  analysisRemoveButtonText: {
    fontSize: Theme.fontSizeXM
  },
  analysisProfileButton: {
    marginLeft: Theme.alignmentXL,
    marginRight: Theme.alignmentXL,
    marginTop: Theme.alignmentS,
    marginBottom: Theme.alignmentS
  },
  typeOfNeedsButtonForSavings: {
    width: 150,
    flex: 1
  },
  typeOfNeedsButtonForProtection: {
    width: 150,
    marginLeft: 10
  },
  analysisHospitalisationButton: {
    width: 150,
    marginLeft: 10,
    flex: 1
  },
  analysisHospitalisationButtonContainer: {
    marginTop: Theme.alignmentS,
    flexDirection: "row"
  },
  shortfallContainer: {
    flexDirection: "row",
    alignSelf: "flex-end"
  },
  shortfallOtherNeedsContainer: {
    flexDirection: "column",
    alignSelf: "flex-end"
  },
  shortfallView: {
    flexDirection: "row",
    backgroundColor: Theme.white,
    paddingHorizontal: 16,
    paddingVertical: 13,
    borderRadius: 100,
    alignSelf: "flex-end",
    marginBottom: 14,
    position: "absolute",
    bottom: 45,
    right: 10
  },
  shortfallWrapper: { flexDirection: "row" },
  subShortfallView: {
    alignSelf: "flex-end",
    marginLeft: 10
  },
  buttonWrapper: {
    flexDirection: "row",
    marginTop: Theme.alignmentS
  },
  firstButton: {
    marginRight: Theme.alignmentXL
  },
  infoActive: {
    width: 16,
    height: 16
  },
  errorMessage: {
    ...Theme.fieldTextOrHelperTextNegative,
    marginVertical: 10
  },
  surplusContent: {
    ...Theme.fieldTextOrHelperTextPositive
  },
  shortfallContent: {
    ...Theme.fieldTextOrHelperTextNegative
  },
  goalsWrapper: {
    paddingHorizontal: Theme.alignmentXL
  }
});
