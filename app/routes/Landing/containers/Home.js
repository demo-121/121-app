import { ACTION_LIST, ACTION_TYPES, REDUCER_TYPES } from "eab-web-api";
import { connect } from "react-redux";
import {
  logout,
  checkLastDataSyncTime,
  setLastDataSyncTime
} from "../../../actions/login";
import { showPolicyNumber } from "../../../actions/policyNumber";
import Home from "../components/Home";
import APP_ACTION_TYPES from "../../../constants/ACTION_TYPES";
import {
  NAVIGATION,
  TEXT_STORE,
  LOGIN,
  PAYMENT
} from "../../../constants/REDUCER_TYPES";

const { CLIENT_FORM, AGENT, ABOUT } = REDUCER_TYPES;
const { cancelCreateClient } = ACTION_LIST[CLIENT_FORM];

/**
 * Home
 * @description Home page container.
 * @requires Home - Home UI
 * */
const mapStateToProps = state => ({
  clientForm: state[CLIENT_FORM],
  agent: state[AGENT],
  environment: state[LOGIN].environment,
  isHandlingListener: state[LOGIN].isHandlingListener,
  triggerInflightAlert: state[LOGIN].triggerInflightAlert,
  isOnlineAuthCompleted: state[PAYMENT].isOnlineAuthCompleted,
  about: state[ABOUT],
  textStore: state[TEXT_STORE],
  login: state[LOGIN]
});

const mapDispatchToProps = dispatch => ({
  logout() {
    logout({ dispatch });
  },
  resetAppPasswordManually() {
    dispatch({
      type: APP_ACTION_TYPES[LOGIN].RESET_APP_PASSWORD_MANUALLY
    });
  },
  cancelCreateClient() {
    cancelCreateClient({ dispatch });
  },
  runView() {
    dispatch({
      type: APP_ACTION_TYPES[LOGIN].RUN_VIEW
    });
  },
  disRunView() {
    dispatch({
      type: APP_ACTION_TYPES[LOGIN].DISABLE_RUN_VIEW
    });
  },
  readyToAddClient(callback) {
    dispatch({
      type: ACTION_TYPES[CLIENT_FORM].INITIAL_CLIENT_FORM,
      configData: {
        isCreate: true,
        isFamilyMember: false,
        isApplication: false,
        isFromProfile: false,
        isFromProduct: false,
        isProposerMissing: false
      },
      callback
    });
  },
  saveClient(callback) {
    dispatch({
      type: ACTION_TYPES[CLIENT_FORM].SAVE_CLIENT,
      confirm: true,
      callback
    });
  },
  navigateToNeeds: () => {
    dispatch({
      type: APP_ACTION_TYPES[NAVIGATION].NAVIGATE_TO_NEEDS
    });
  },
  loginByOnlineAuthForPayment: callback => {
    dispatch({
      type: APP_ACTION_TYPES[PAYMENT].LOGIN_BY_ONLINE_AUTH_FOR_PAYMENT,
      callback
    });
  },
  setOnlineAuthCodeForPayment: ({ authCode, callback }) => {
    dispatch({
      type: APP_ACTION_TYPES[PAYMENT].ONLINE_AUTH_CODE_FOR_PAYMENT,
      authCode,
      callback
    });
  },
  checkTokenExpiryDate: callback => {
    dispatch({
      type: APP_ACTION_TYPES[LOGIN].CHECK_TOKEN_EXPIRY_DATE,
      callback
    });
  },
  showPolicyNumber: callback => {
    showPolicyNumber({ dispatch, callback });
  },
  checkLastDataSyncTime: callback => {
    checkLastDataSyncTime({ dispatch, callback });
  },
  setLastDataSyncTime() {
    setLastDataSyncTime({ dispatch });
  },
  updateAgentProfile() {
    dispatch({
      type: APP_ACTION_TYPES[LOGIN].UPDATE_AGENT_PROFILE
    });
  },
  assignEligProductsToAgent(callback) {
    dispatch({
      type: ACTION_TYPES[REDUCER_TYPES.AGENT].ASSIGN_ELIG_PRODUCTS_TO_AGENT,
      callback
    });
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Home);
