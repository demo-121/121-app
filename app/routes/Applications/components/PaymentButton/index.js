import { EAPP, REDUCER_TYPE_CHECK, REDUCER_TYPES } from "eab-web-api";
import React, { Component } from "react";
import PropTypes from "prop-types";
import {
  View,
  WebView,
  Alert,
  NativeEventEmitter,
  NativeModules,
  Text,
  Linking
} from "react-native";
import Config from "react-native-config";
import SafariView from "react-native-safari-view";
import EABButton from "../../../../components/EABButton";
import { RECTANGLE_BUTTON_1 } from "../../../../components/EABButton/constants";
import EABDialog from "../../../../components/EABDialog/EABDialog.ios";
import PopoverTooltip from "../../../../components/PopoverTooltip/PopoverTooltipIOS";
import checkOnlinePaymentAndSubmission from "../../../../apis/checkOnlinePaymentAndSubmission";
import APP_REDUCER_TYPE_CHECK from "../../../../constants/REDUCER_TYPE_CHECK";
import { TEXT_STORE } from "../../../../constants/REDUCER_TYPES";
import EABHUD from "../../../../containers/EABHUD";
import TranslatedText from "../../../../containers/TranslatedText";
import { ContextConsumer } from "../../../../context";
import ENGLISH from "../../../../locales/textStore/ENGLISH";
import Theme from "../../../../theme";
import EABi18n from "../../../../utilities/EABi18n";
import styles from "./styles";

// =============================================================================
// constants
// =============================================================================
const {
  IS_GETTING_PAYMENT_URL,
  IS_GETTING_PAYMENT_STATUS,
  IS_SHIELD_DATA_SYNC,
  IS_CHECKING_PAYMENT_CONFLICT,
  IS_ONLINE_CHECKING_CONFLICT
} = EAPP.paymentButton;

/**
 * @description PaymentButton in Payment section.
 * @param {object} paymentValues - payment status values
 * */
export default class PaymentButton extends Component {
  constructor(props) {
    super(props);

    this.dataSyncEvent = new NativeEventEmitter(
      NativeModules.DataSyncEventManager
    );
    this.closeWebView = this.closeWebView.bind(this);
    this.buttonOnClick = this.buttonOnClick.bind(this);
    this.dataSyncCallbackHandler = this.dataSyncCallbackHandler.bind(this);
    this.dbsToolTip = this.dbsToolTip.bind(this);
    this.linkingHandler = this.linkingHandler.bind(this);
    this.getPaymentURLWrapper = this.getPaymentURLWrapper.bind(this);
    this.dataSyncWrapper = this.dataSyncWrapper.bind(this);
    this.alertWrapper = this.alertWrapper.bind(this);
    this.refreshPaymentStatusWrapper = this.refreshPaymentStatusWrapper.bind(
      this
    );
    this.setHideApplicationDialog = this.setHideApplicationDialog.bind(this);
    this.conflictCheckWrapper = this.conflictCheckWrapper.bind(this);

    this.state = {
      isLoading: false,
      isWebViewOpen: false,
      workType: null,
      url: "",
      language: ENGLISH,
      hideApplicationDialog: null
    };
  }
  // ===========================================================================
  // life circles
  // ===========================================================================
  componentDidMount() {
    Linking.addEventListener("url", this.linkingHandler);
    const { dataSyncCallbackHandler } = this;

    this.dataSyncEventListener = this.dataSyncEvent.addListener(
      "DataSyncEventHandler",
      dataSyncCallbackHandler
    );
  }
  componentWillUnmount() {
    Linking.removeEventListener("url", this.linkingHandler);
    this.dataSyncEventListener.remove();
    this.dataSyncEventListener = null;
  }
  // ===========================================================================
  // getters
  // ===========================================================================
  /**
   * @description indicate quot type is shield or not
   * */
  get initPayMethod() {
    const { application, payment } = this.props;
    const { quotation } = application;

    return quotation.quotType === "SHIELD"
      ? payment.initPayMethod || ""
      : payment.values.initPayMethod;
  }
  /**
   * @description indicate show component or not
   * @requires CREDIT_CARD
   * @requires E_NETS
   * @requires DBSCRCARDIPP
   * @return {boolean}
   * */
  get shouldShowComponent() {
    const { initPayMethod } = this;
    const { submission } = this.props;

    return (
      [
        EAPP.fieldValue[REDUCER_TYPES.PAYMENT].CREDIT_CARD,
        EAPP.fieldValue[REDUCER_TYPES.PAYMENT].E_NETS,
        EAPP.fieldValue[REDUCER_TYPES.PAYMENT].DBSCRCARDIPP
      ].indexOf(initPayMethod) !== -1 &&
      (submission.values ? submission.values.submitStatus !== "SUCCESS" : true)
    );
  }
  /**
   * @description indicate show button or not
   * @return {boolean}
   * */
  get shouldShowButton() {
    const { onlinePaymentStatus } = this.props;
    const { trxStartTime, trxStatus } = onlinePaymentStatus;

    // rule for value "I"
    const iRule =
      trxStatus === "I" &&
      (!trxStartTime || trxStartTime + 120 * 60000 > new Date().getTime());

    return !trxStatus || trxStatus === "N" || trxStatus === "C" || iRule;
  }
  /**
   * @description indicate trxStatus is "I" or not
   * @return {boolean}
   * */
  get isTrxStatusEqualI() {
    const { onlinePaymentStatus } = this.props;

    return onlinePaymentStatus.trxStatus === "I";
  }
  /**
   * @description indicate show DBS credit card IPP or not
   * @requires DBSCRCARDIPP
   * @return {boolean}
   * */
  get shouldShowIPP() {
    const { initPayMethod } = this;

    return (
      EAPP.fieldValue[REDUCER_TYPES.PAYMENT].DBSCRCARDIPP === initPayMethod
    );
  }
  /**
   * @description indicate application is shield or not
   * @return {boolean}
   * */
  get isShield() {
    const { application } = this.props;
    return application.quotation.quotType === "SHIELD";
  }
  // ===========================================================================
  // class methods
  // ===========================================================================
  /**
   * @description set hideApplicationDialog function to state
   * */
  setHideApplicationDialog(functionSource) {
    const { hideApplicationDialog } = this.state;

    if (hideApplicationDialog === null) {
      this.setState({ hideApplicationDialog: functionSource });
    }
  }
  /**
   * @description get payment url function wrapper
   * */
  getPaymentURLWrapper(language) {
    const { dataSyncWrapper, alertWrapper } = this;
    const {
      getPaymentURL,
      textStore,
      preCheck,
      disableEappButton
    } = this.props;

    this.setState(
      {
        isLoading: true
      },
      () => {
        preCheck({
          alert: () => {
            this.setState(
              {
                isLoading: false,
                workType: IS_GETTING_PAYMENT_URL
              },
              () => {
                setTimeout(() => {
                  alertWrapper(language);
                }, 200);
              }
            );
          },
          callingFunction: () => {
            getPaymentURL({
              callback: url => {
                this.setState(
                  { isLoading: false, url, workType: IS_GETTING_PAYMENT_URL },
                  () => {
                    setTimeout(() => {
                      dataSyncWrapper(language);
                    }, 200);
                  }
                );
              },
              alertFunction: () => {
                this.setState({ isLoading: false }, () => {
                  setTimeout(() => {
                    Alert.alert(
                      EABi18n({
                        language,
                        textStore,
                        path: "alert.error"
                      }),
                      EABi18n({
                        language,
                        textStore,
                        path: "paymentAndSubmission.alert.gerUrlFail"
                      }),
                      [
                        {
                          text: "OK",
                          onPress: () => {
                            disableEappButton(false);
                          },
                          style: "cancel"
                        }
                      ]
                    );
                  }, 200);
                });
              },
              prepareDataSync: () => {
                this.setState(
                  {
                    isLoading: false,
                    workType: IS_GETTING_PAYMENT_STATUS
                  },
                  () => {
                    setTimeout(() => {
                      dataSyncWrapper(language);
                    }, 200);
                  }
                );
              }
            });
          }
        });
      }
    );
  }
  /**
   * @description refresh payment status function wrapper
   * */
  refreshPaymentStatusWrapper(language) {
    const { dataSyncWrapper, alertWrapper } = this;
    const {
      refreshPaymentStatus,
      textStore,
      preCheck,
      disableEappButton
    } = this.props;

    this.setState(
      {
        isLoading: true
      },
      () => {
        preCheck({
          alert: () => {
            this.setState(
              {
                isLoading: false,
                workType: IS_GETTING_PAYMENT_STATUS
              },
              () => {
                setTimeout(() => {
                  alertWrapper(language);
                }, 200);
              }
            );
          },
          callingFunction: () => {
            refreshPaymentStatus({
              callback: () => {
                this.setState(
                  {
                    isLoading: false,
                    workType: IS_GETTING_PAYMENT_STATUS
                  },
                  () => {
                    setTimeout(() => {
                      dataSyncWrapper(language);
                    }, 500);
                  }
                );
              },
              error: message => {
                this.setState({ isLoading: false }, () => {
                  setTimeout(() => {
                    Alert.alert(
                      EABi18n({
                        language,
                        textStore,
                        path: "alert.error"
                      }),
                      message,
                      [
                        {
                          text: EABi18n({
                            language,
                            textStore,
                            path: "button.ok"
                          }),
                          onPress: () => {
                            disableEappButton(false);
                          }
                        }
                      ],
                      { cancelable: false }
                    );
                  }, 200);
                });
              }
            });
          }
        });
      }
    );
  }
  /**
   * @description data sync wrapper
   * */
  dataSyncWrapper(language) {
    const { alertWrapper } = this;
    const { dataSync, disableEappButton } = this.props;

    this.setState(
      {
        isLoading: true
      },
      () => {
        disableEappButton(true);
        dataSync({
          alert: () => {
            alertWrapper(language);
          },
          closeLoadingFunction: callback => {
            this.setState(
              {
                isLoading: false
              },
              callback
            );
          }
        });
      }
    );
  }
  /**
   * @description alert Wrapper
   * */
  alertWrapper(language) {
    const {
      textStore,
      loginByOnlineAuthForPayment,
      environment,
      disableEappButton
    } = this.props;

    loginByOnlineAuthForPayment();
    Alert.alert(
      EABi18n({
        language,
        textStore,
        path: "alert.confirm"
      }),
      EABi18n({
        language,
        textStore,
        path: "payment.PrePayment"
      }),
      [
        {
          text: EABi18n({
            language,
            textStore,
            path: "button.openSafari"
          }),
          onPress: () => {
            disableEappButton(false);
            let url = "";
            if (Config.ENV_NAME === "PROD") {
              url = Config.GET_AUTH_CODE_FULL_URL;
            } else {
              switch (environment) {
                case "DEV":
                  url = Config.DEV_GET_AUTH_CODE_FULL_URL;
                  break;
                case "SIT1_EAB":
                  url = Config.SIT1_EAB_GET_AUTH_CODE_FULL_URL;
                  break;
                case "SIT2_EAB":
                  url = Config.SIT2_EAB_GET_AUTH_CODE_FULL_URL;
                  break;
                case "SIT3_EAB":
                  url = Config.SIT3_EAB_GET_AUTH_CODE_FULL_URL;
                  break;
                case "SIT_AXA":
                  url = Config.SIT_AXA_GET_AUTH_CODE_FULL_URL;
                  break;
                case "UAT":
                  url = Config.UAT_GET_AUTH_CODE_FULL_URL;
                  break;
                case "PRE_PROD":
                  url = Config.PRE_PROD_GET_AUTH_CODE_FULL_URL;
                  break;
                default:
                  break;
              }
            }

            SafariView.isAvailable()
              .then(
                this.setState(
                  {
                    language
                  },
                  () => {
                    SafariView.show({ url });
                  }
                )
              )
              .catch(error => {
                disableEappButton(false);
                // Fallback WebView code for iOS 8 and earlier
                setTimeout(() => {
                  Alert.alert(`[Error]${error}`);
                }, 200);
              });
          }
        },
        {
          text: EABi18n({
            language,
            textStore,
            path: "button.cancel"
          }),
          onPress: () => {
            disableEappButton(false);
          },
          style: "cancel"
        }
      ],
      { cancelable: false }
    );
  }
  /**
   * @description conflict check function wrapper
   * */
  conflictCheckWrapper(language) {
    const {
      isShield,
      getPaymentURLWrapper,
      dataSyncWrapper,
      alertWrapper
    } = this;
    const {
      preCheck,
      application,
      authToken,
      agentCode,
      lastDataSyncTime,
      environment,
      disableEappButton
    } = this.props;
    const { hideApplicationDialog } = this.state;

    if (hideApplicationDialog !== null) {
      this.setState(
        {
          isLoading: true
        },
        () => {
          preCheck({
            alert: () => {
              this.setState(
                {
                  isLoading: false,
                  workType: IS_CHECKING_PAYMENT_CONFLICT
                },
                () => {
                  setTimeout(() => {
                    alertWrapper(language);
                  }, 200);
                }
              );
            },
            callingFunction: () => {
              checkOnlinePaymentAndSubmission({
                authToken,
                clientId: application.pCid,
                agentCode,
                lastDataSyncTime,
                environment,
                callback: () => {}
              })
                .then(response => {
                  this.setState(
                    {
                      isLoading: false
                    },
                    () => {
                      setTimeout(() => {
                        if (response.hasError) {
                          disableEappButton(false);
                          Alert.alert(`[Error] ${response.errorMsg}`, null, [
                            {
                              text: "OK",
                              onPress: null
                            }
                          ]);
                        } else if (response.result.isWebDataConflict) {
                          Alert.alert(
                            "This application has been refreshed as either you have completed online payment or submission. You will be redirected to application summary to proceed further.",
                            null,
                            [
                              {
                                text: "Continue",
                                onPress: () => {
                                  this.setState(
                                    {
                                      language,
                                      workType: IS_ONLINE_CHECKING_CONFLICT
                                    },
                                    () => {
                                      setTimeout(() => {
                                        dataSyncWrapper(language);
                                      }, 200);
                                    }
                                  );
                                }
                              }
                            ]
                          );
                        } else if (isShield) {
                          this.setState(
                            {
                              language,
                              workType: IS_SHIELD_DATA_SYNC
                            },
                            () => {
                              setTimeout(() => {
                                dataSyncWrapper(language);
                              }, 200);
                            }
                          );
                        } else {
                          getPaymentURLWrapper(language);
                        }
                      }, 500);
                    }
                  );
                })
                .catch(response => {
                  disableEappButton(false);
                  this.setState(
                    {
                      isLoading: false
                    },
                    () => {
                      setTimeout(() => {
                        Alert.alert(
                          `[Error] ${response.errorMsg ||
                            response.error ||
                            response}`,
                          null,
                          [
                            {
                              text: "OK",
                              onPress: null
                            }
                          ]
                        );
                      }, 500);
                    }
                  );
                });
            }
          });
        }
      );
    } else {
      disableEappButton(false);
      throw new Error("function hideApplicationDialog is null!");
    }
  }
  /**
   * @description used to handler the linking event
   * */
  linkingHandler(linkingEvent) {
    const {
      dataSyncWrapper,
      getPaymentURLWrapper,
      refreshPaymentStatusWrapper,
      conflictCheckWrapper
    } = this;
    const { setOnlineAuthCodeForPayment, disableEappButton } = this.props;
    const { language, workType } = this.state;
    this.setState({
      isLoading: true
    });
    disableEappButton(true);
    if (workType !== null) {
      if (linkingEvent.url) {
        const urlGet = linkingEvent.url;
        SafariView.dismiss();
        const segment1 = urlGet.replace("easemobile://?", "");
        const variables = segment1.split("&");
        const codeString = variables.find(variable =>
          variable.includes("code")
        );
        const [, authCode] = codeString.split("=");

        setOnlineAuthCodeForPayment({
          authCode,
          callback: callback => {
            this.setState({
              isLoading: false
            });
            if (callback.hasError) {
              disableEappButton(false);
              if (callback.errorMsg) {
                setTimeout(() => {
                  Alert.alert(`[Error]  ${callback.errorMsg}`);
                }, 200);
              } else {
                setTimeout(() => {
                  Alert.alert(
                    `error msg: Authorization for submission Failed (Error 001)`
                  );
                }, 200);
              }
            } else {
              switch (workType) {
                case IS_GETTING_PAYMENT_URL:
                  getPaymentURLWrapper(language);
                  break;
                case IS_SHIELD_DATA_SYNC:
                  dataSyncWrapper(language);
                  break;
                case IS_GETTING_PAYMENT_STATUS:
                  refreshPaymentStatusWrapper(language);
                  break;
                case IS_CHECKING_PAYMENT_CONFLICT:
                  conflictCheckWrapper(language);
                  break;
                default:
                  disableEappButton(false);
                  break;
              }
            }
          }
        });
      }
    }
  }
  /**
   * @description handle payment button onClick event. check if data sync needed
   *   before payment or submission first. then will run different function by
   *   the application is shield or not
   * @param {string} language
   * */
  buttonOnClick(language) {
    const {
      conflictCheckWrapper,
      isTrxStatusEqualI,
      refreshPaymentStatusWrapper
    } = this;

    if (isTrxStatusEqualI) {
      refreshPaymentStatusWrapper(language);
    } else {
      conflictCheckWrapper(language);
    }
  }
  /**
   * @description handle close WebView event
   * */
  closeWebView(language) {
    const { refreshPaymentStatusWrapper } = this;

    this.setState(
      {
        url: "",
        isWebViewOpen: false
      },
      () => {
        refreshPaymentStatusWrapper(language);
      }
    );
  }
  /**
   * @description data sync callback function
   * @param {string} status - data sync result
   * */
  dataSyncCallbackHandler(status) {
    const { getPaymentURLWrapper } = this;
    const {
      getPayment,
      continueApplicationFormShield,
      application,
      setLastDataSyncTime,
      disableEappButton
    } = this.props;
    const { language, workType } = this.state;

    if (status === "SUCCESS") {
      setLastDataSyncTime();
      switch (workType) {
        case IS_GETTING_PAYMENT_URL:
          setTimeout(() => {
            this.setState({
              isWebViewOpen: true,
              workType: null
            });
          }, 500);
          break;
        case IS_GETTING_PAYMENT_STATUS:
          this.setState(
            {
              workType: null
            },
            () => {
              this.setState(
                {
                  isLoading: true
                },
                () => {
                  disableEappButton(false);
                  if (application.quotation.quotType !== "SHIELD") {
                    getPayment({
                      docId: application.id,
                      callback: () => {
                        this.setState({
                          isLoading: false
                        });
                      }
                    });
                  } else {
                    continueApplicationFormShield({
                      applicationId: this.props.application.id,
                      callback: () => {
                        this.setState({
                          isLoading: false
                        });
                      }
                    });
                  }
                }
              );
            }
          );
          break;
        case IS_SHIELD_DATA_SYNC:
          this.setState(
            {
              isLoading: true,
              workType: IS_GETTING_PAYMENT_URL
            },
            () => {
              getPaymentURLWrapper(language);
            }
          );
          break;
        case IS_ONLINE_CHECKING_CONFLICT:
          disableEappButton(false);
          this.props.closeDialogAndGetApplicationList(
            setTimeout(() => {
              this.state.hideApplicationDialog();
            }, 500)
          );
          break;
        default:
          break;
      }
    }
  }
  /**
   * @description dbs tooltip
   * @return {element}
   * */
  dbsToolTip(language) {
    const { textStore, isDisableEappButton } = this.props;

    const creditCardSeries = {
      a: ["411911", "455621", "540042"],
      b: ["419031", "455622", "542089"],
      c: ["425895", "462894", "549831"],
      d: ["428424", "529124", "552038"],
      e: ["431152", "531215", "558702"]
    };

    return (
      <View key="IPP" style={styles.IPP}>
        <PopoverTooltip
          popoverOptions={{
            preferredContentSize: [450, 250]
          }}
          isDisable={isDisableEappButton || this.state.loading === true}
        >
          <View style={styles.IPPWrapper}>
            <Text style={styles.IPPHint}>
              {EABi18n({
                language,
                textStore,
                path: "paymentAndSubmission.dbsToolTip"
              })}
            </Text>
            <View style={styles.IPPTable}>
              {Object.keys(creditCardSeries).map(rowKey => (
                <View key={rowKey} style={styles.IPPRow}>
                  {creditCardSeries[rowKey].map((item, index) => (
                    <View
                      key={item}
                      style={
                        index === 0 ? styles.IPPColumnFirst : styles.IPPColumn
                      }
                    >
                      <Text style={Theme.toolTipLabel}>{item}</Text>
                    </View>
                  ))}
                </View>
              ))}
            </View>
          </View>
        </PopoverTooltip>
      </View>
    );
  }

  render() {
    // =========================================================================
    // variables
    // =========================================================================
    const {
      setHideApplicationDialog,
      shouldShowComponent,
      shouldShowButton,
      shouldShowIPP,
      dbsToolTip,
      closeWebView,
      isTrxStatusEqualI,
      buttonOnClick
    } = this;
    const { disableEappButton, isDisableEappButton } = this.props;
    const { isWebViewOpen, url, isLoading } = this.state;

    return shouldShowComponent ? (
      <ContextConsumer>
        {({ language, hideApplicationDialog }) => [
          shouldShowButton ? (
            <EABButton
              key="button"
              style={styles.container}
              buttonType={RECTANGLE_BUTTON_1}
              onPress={() => {
                disableEappButton(true);
                buttonOnClick(language);
              }}
              isDisable={isWebViewOpen || isDisableEappButton}
            >
              <TranslatedText
                path={
                  isTrxStatusEqualI
                    ? "paymentAndSubmission.button.refresh"
                    : "paymentAndSubmission.button.pay"
                }
              />
            </EABButton>
          ) : null,
          shouldShowIPP ? dbsToolTip(language) : null,
          <EABDialog key="dialog" isOpen={isWebViewOpen}>
            <View style={Theme.dialogHeader}>
              <View style={Theme.headerRow}>
                <EABButton
                  containerStyle={Theme.headerRight}
                  onPress={() => {
                    closeWebView(language);
                  }}
                >
                  <TranslatedText
                    style={Theme.textButtonLabelNormalEmphasizedAccent}
                    path="button.close"
                  />
                </EABButton>
              </View>
            </View>
            <WebView
              style={styles.webView}
              onMessage={() => {
                closeWebView(language);
              }}
              source={{
                uri: url
              }}
            />
          </EABDialog>,
          <EABHUD
            key="hud"
            message="Please wait until you are redirected back to payment screen"
            isOpen={isLoading}
          />,
          setHideApplicationDialog(hideApplicationDialog)
        ]}
      </ContextConsumer>
    ) : null;
  }
}

PaymentButton.propTypes = {
  closeDialogAndGetApplicationList: PropTypes.func.isRequired,
  refreshPaymentStatus: PropTypes.func.isRequired,
  environment: PropTypes.string.isRequired,
  payment: PropTypes.oneOfType([PropTypes.object]).isRequired,
  application: PropTypes.oneOfType([PropTypes.object]).isRequired,
  submission:
    REDUCER_TYPE_CHECK[REDUCER_TYPES.PAYMENT_AND_SUBMISSION].submission
      .isRequired,
  getPayment: PropTypes.func.isRequired,
  continueApplicationFormShield: PropTypes.func.isRequired,
  getPaymentURL: PropTypes.func.isRequired,
  textStore: APP_REDUCER_TYPE_CHECK[TEXT_STORE].isRequired,
  dataSync: PropTypes.func.isRequired,
  preCheck: PropTypes.func.isRequired,
  loginByOnlineAuthForPayment: PropTypes.func.isRequired,
  onlinePaymentStatus: PropTypes.oneOfType([PropTypes.object]).isRequired,
  setOnlineAuthCodeForPayment: PropTypes.func.isRequired,
  authToken: PropTypes.string.isRequired,
  agentCode: PropTypes.string.isRequired,
  lastDataSyncTime: PropTypes.string.isRequired,
  setLastDataSyncTime: PropTypes.func.isRequired,
  isDisableEappButton: PropTypes.bool.isRequired,
  disableEappButton: PropTypes.func.isRequired
};
