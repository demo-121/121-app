### Selector sample code snippets

```javascript
class Sample extends Component {

  constructor() {
    super();

    this.state = {
      checkBoxValue: true,
      selectedValue: "",
      date: new Date("2015-07-21") // Default label
    };
  }

  onShow() {
    console.log('sample onShow')
  }

  onHide() {
    console.log('sample onHide')
  }

  render() {
    return (
      <View>
          <Selector
            selectorType="FLAT_LIST"
            popoverOptions={{
              onShow: this.onShow.bind(this),
              onHide: this.onHide.bind(this),
              preferredContentSize: [300, 200],
              permittedArrowDirections: [0],
            }}
            // disable
            // hasCard={false}
            // selectorTitle="title"
            hasCheckbox
            checkBoxValue={this.state.checkBoxValue}
            checkBoxOnPress={() => {
              this.setState({ checkBoxValue: !this.state.checkBoxValue });
            }}
            hasLabel
            label="Singapore Dollars"
            pickerOptions={{
              items: [
                { label: 'Java', value: 'java' },
                { label: 'JavaScript', value: 'js' },
                { label: 'A1', value: 'a1' },
                { label: 'A2', value: 'a2' },
              ],
              onValueChange: itemValue => {
                this.setState({
                  selectedValue: itemValue
                });
              },
              selectedValue: this.state.selectedValue
            }}
            flatListOptions={{
              renderItemOnPress: label => {
                this.setState({
                  selectedValue: label
                });
              },
              selectedValue: this.state.selectedValue,
              extraData: this.state,
              keyExtractor: item => item.key,
              data: [
                { key: 'a', label: 'A' },
                { key: 'b', label: 'B' },
                { key: 'c', label: 'C' },
                { key: 'd', label: 'D' },
                { key: 'e', label: 'E' },
                { key: 'f', label: 'F' },
                { key: 'g', label: 'G' },
                { key: 'h', label: 'H' },
              ],
            }}
            datePickerIOSOptions={{
              onDateChange: itemValue => {
                this.setState({
                  date: itemValue
                });
              },
              date: this.state.date,
              mode: "date"
            }}
          />
      </View>
    )
  }
}
```