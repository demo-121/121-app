import { connect } from "react-redux";
import { REDUCER_TYPES } from "eab-web-api";
import ProductList from "../components/ProductList";

/**
 * ProductList
 * @description ProductList in Application Summary page.
 * @requires ProductList - ProductList UI
 * */
const mapStateToProps = state => ({
  productList: state[REDUCER_TYPES.PRE_APPLICATION].productList
});

const mapDispatchToProps = () => ({});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProductList);
