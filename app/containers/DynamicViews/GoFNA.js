import { connect } from "react-redux";
import { ACTION_TYPES, REDUCER_TYPES } from "eab-web-api";
import { NavigationActions } from "react-navigation";
import GoFNA from "../../components/DynamicViews/GoFNA";
import { TEXT_STORE } from "../../constants/REDUCER_TYPES";

const { OPTIONS_MAP } = REDUCER_TYPES;
/**
 * GoFNA
 * @description GoFNA in DynamicViews
 * */

const mapStateToProps = state => ({
  optionsMap: state[OPTIONS_MAP],
  textStore: state[TEXT_STORE]
});

const mapDispatchToProps = dispatch => ({
  saveForm: ({ genPDF, actionType, callback }) => {
    dispatch({
      type:
        ACTION_TYPES[REDUCER_TYPES.APPLICATION].SAVE_APPLICATION_FORM_BEFORE,
      actionType,
      genPDF,
      callback
    });
  },
  navigateToFNA: () => {
    dispatch(
      NavigationActions.navigate({
        routeName: "Needs",
        params: {
          financialEvaluationDialogIsOpen: true
        }
      })
    );
  },
  needsRedirectTo: ({ page, callback }) => {
    dispatch({
      type: ACTION_TYPES[REDUCER_TYPES.APPLICATION].SAGA_REDIRECT_TO_FNA,
      page,
      callback
    });
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(GoFNA);
