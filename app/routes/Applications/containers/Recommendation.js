import { connect } from "react-redux";
import { ACTION_LIST, REDUCER_TYPES } from "eab-web-api";
import { TEXT_STORE } from "../../../constants/REDUCER_TYPES";
import Recommendation from "../components/Recommendation";

/**
 * Recommendation
 * @description Recommendation form in Recommendation page.
 * @requires Recommendation - Recommendation form UI
 * */
const mapStateToProps = state => ({
  textStore: state[TEXT_STORE],
  value: state[REDUCER_TYPES.RECOMMENDATION].recommendation,
  completed:
    state[REDUCER_TYPES.RECOMMENDATION].component.completedSection
      .recommendation,
  selectedQuotId: state[REDUCER_TYPES.RECOMMENDATION].component.selectedQuotId
});

const mapDispatchToProps = dispatch => ({
  updateSelectedQuotation: newSelectedQuotId => {
    ACTION_LIST[
      REDUCER_TYPES.RECOMMENDATION
    ].updateRecommendationSelectedQuotation({
      dispatch,
      newSelectedQuotId
    });
  },
  validateRecommendation: init => {
    ACTION_LIST[REDUCER_TYPES.RECOMMENDATION].validateRecommendation({
      dispatch,
      init
    });
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Recommendation);
