import PropTypes from "prop-types";
import React, { PureComponent } from "react";
import { Modal } from "react-native";
import * as DIALOG_TYPES from "./constants";

/**
 * <EABDialog />
 * @description dialog component ios version.
 * @requires DIALOG_TYPES - dialog allowance types
 * @param {boolean} isOpen - indicates whether the dialog is open or not
 * @param {boolean} [type=DIALOG_TYPES.PAGE] - dialog type. must come from DIALOG_TYPES.
 * */
export default class EABDialog extends PureComponent {
  render() {
    return (
      <Modal
        visible={this.props.isOpen}
        presentationStyle={this.props.type}
        animationType="slide"
      >
        {this.props.children}
      </Modal>
    );
  }
}

EABDialog.propTypes = {
  type: PropTypes.string,
  isOpen: PropTypes.bool.isRequired,
  children: PropTypes.node.isRequired
};

EABDialog.defaultProps = {
  type: DIALOG_TYPES.PAGE
};
