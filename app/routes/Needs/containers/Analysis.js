import { connect } from "react-redux";
import { ACTION_TYPES, REDUCER_TYPES, NEEDS, utilities } from "eab-web-api";
import { TEXT_STORE } from "../../../constants/REDUCER_TYPES";

import Analysis from "../components/Analysis";

const { FIPROTECTION, DIPROTECTION, PSGOALS, RPLANNING } = NEEDS;
const { FNA, CLIENT } = REDUCER_TYPES;
const { removeCash } = utilities.naAnalysis;

/**
 * Analysis
 * @requires Analysis - Analysis UI
 * */
const mapStateToProps = state => ({
  textStore: state[TEXT_STORE],
  profile: state[CLIENT].profile,
  pda: state[FNA].pda,
  dependantProfiles: state[CLIENT].dependantProfiles,
  lastStepIndex: state[FNA].na.lastStepIndex,
  fe: state[FNA].fe,
  na: state[FNA].na,
  needs: state[FNA].na.needs,
  isNaError: state[FNA].isNaError,
  isChanged: state[FNA].naIsChanged
});

const mapDispatchToProps = dispatch => ({
  updateLastStepIndex(value) {
    dispatch({
      type: ACTION_TYPES[FNA].UPDATE_LAST_STEP_INDEX,
      newStep: value
    });
  },
  initCompletedStep: value => {
    dispatch({
      type: ACTION_TYPES[FNA].INIT_COMPLETED_STEP_SAGA,
      value
    });
  },
  saveNA({ confirm, callback }) {
    // TODO -> can save profile, but "lastSelectedProduct" or "selectedMenuId"(need to check from web-logic) can't save to cb, so the page render the income protection everytime you trigged save button
    dispatch({
      type: ACTION_TYPES[FNA].SAVE_NA,
      confirm,
      callback
    });
  },
  saveLastSelectedProduct(lastSelectedProduct, callback) {
    dispatch({
      type: ACTION_TYPES[FNA].SAVE_LAST_SELECTED_PRODUCT,
      lastSelectedProduct
    });
    if (callback) {
      callback();
    }
  },
  assetsValueOnChange(data) {
    dispatch({
      type: ACTION_TYPES[FNA].UPDATE_NA_ANALYSIS_DATASET,
      value: {
        targetProduct: data.selectedProduct,
        targetProfile: data.selectedProfile,
        targetData: { assets: data.value },
        targetIndex: data.index,
        targetAction: data.action,
        targetFunction: data.function,
        rule: data.rule,
        data
      }
    });
  },
  removeAsset(data, callback) {
    dispatch({
      type: ACTION_TYPES[FNA].UPDATE_NA_ANALYSIS_DATA,
      value: {
        targetProduct: data.selectedProduct,
        targetProfile: data.selectedProfile,
        targetData: { assets: data.value }
      }
    });
    if (callback) {
      callback();
    }
  },
  income: {
    pmtOnChange(data) {
      data = removeCash(data);
      dispatch({
        type: ACTION_TYPES[FNA].UPDATE_NA_ANALYSIS_DATA,
        value: {
          targetProduct: FIPROTECTION,
          targetProfile: data.selectedProfile,
          targetData: { pmt: data.value },
          rule: data.rule
          // TODO
          // fiProtection: {
          //   [data.selectedProfile]: { pmt: data.value }
          // }
        }
      });
    },
    unMatchPmtReasonOnChange(data) {
      dispatch({
        type: ACTION_TYPES[FNA].UPDATE_NA_ANALYSIS_DATA,
        value: {
          targetProduct: FIPROTECTION,
          targetProfile: data.selectedProfile,
          targetData: { unMatchPmtReason: data.value },
          rule: data.rule
        }
      });
    },
    requireYrIncomeOnChange(data) {
      // special handling
      const incomeValue = data.value.toString();
      dispatch({
        type: ACTION_TYPES[FNA].UPDATE_NA_ANALYSIS_DATA,
        value: {
          targetProduct: FIPROTECTION,
          targetProfile: data.selectedProfile,
          targetData: { requireYrIncome: incomeValue }
        }
      });
    },
    finExpensesOnChange(data) {
      data = removeCash(data);
      dispatch({
        type: ACTION_TYPES[FNA].UPDATE_NA_ANALYSIS_DATA,
        value: {
          targetProduct: FIPROTECTION,
          targetProfile: data.selectedProfile,
          targetData: { finExpenses: data.value }
        }
      });
    },
    othFundNeedsNameOnChange(data) {
      dispatch({
        type: ACTION_TYPES[FNA].UPDATE_NA_ANALYSIS_DATA,
        value: {
          targetProduct: FIPROTECTION,
          targetProfile: data.selectedProfile,
          targetData: { othFundNeedsName: data.value }
        }
      });
    },
    othFundNeedsOnChange(data) {
      data = removeCash(data);
      dispatch({
        type: ACTION_TYPES[FNA].UPDATE_NA_ANALYSIS_DATA,
        value: {
          targetProduct: FIPROTECTION,
          targetProfile: data.selectedProfile,
          targetData: { othFundNeeds: data.value }
        }
      });
    }
  },
  disabilityBenefit: {
    pmtOnChange(data) {
      data = removeCash(data);
      dispatch({
        type: ACTION_TYPES[FNA].UPDATE_NA_ANALYSIS_DATA,
        value: {
          targetProduct: DIPROTECTION,
          targetProfile: data.selectedProfile,
          targetData: { pmt: data.value },
          rule: data.rule
        }
      });
    },
    requireYrIncomeOnChange(data) {
      // special handling
      const incomeValue = data.value.toString();
      dispatch({
        type: ACTION_TYPES[FNA].UPDATE_NA_ANALYSIS_DATA,
        value: {
          targetProduct: DIPROTECTION,
          targetProfile: data.selectedProfile,
          targetData: { requireYrIncome: incomeValue }
        }
      });
    },
    othRegIncomeOnChange(data) {
      data = removeCash(data);
      dispatch({
        type: ACTION_TYPES[FNA].UPDATE_NA_ANALYSIS_DATA,
        value: {
          targetProduct: DIPROTECTION,
          targetProfile: data.selectedProfile,
          targetData: { othRegIncome: data.value },
          rule: data.rule
        }
      });
    }
  },
  planningForSpecificGoals: {
    goalNameOnChange(data) {
      data = removeCash(data);
      const cloneGoalsArray = data.goalsArray;
      cloneGoalsArray[data.index].goalName = data.value;
      dispatch({
        type: ACTION_TYPES[FNA].UPDATE_NA_ANALYSIS_DATA,
        value: {
          targetProduct: PSGOALS,
          targetProfile: data.selectedProfile,
          targetData: { goals: cloneGoalsArray }
        }
      });
    },
    timeHorizonOnChange(data) {
      data = removeCash(data);
      const cloneGoalsArray = data.goalsArray;
      cloneGoalsArray[data.index].timeHorizon = data.value;
      dispatch({
        type: ACTION_TYPES[FNA].UPDATE_NA_ANALYSIS_DATA,
        value: {
          targetProduct: PSGOALS,
          targetProfile: data.selectedProfile,
          targetData: { goals: cloneGoalsArray }
        }
      });
    },
    compFvOnChange(data) {
      data = removeCash(data);
      const cloneGoalsArray = data.goalsArray;
      cloneGoalsArray[data.index].compFv = data.value;
      dispatch({
        type: ACTION_TYPES[FNA].UPDATE_NA_ANALYSIS_DATA,
        value: {
          targetProduct: PSGOALS,
          targetProfile: data.selectedProfile,
          targetData: { goals: cloneGoalsArray }
        }
      });
    },
    projMaturityOnChange(data) {
      data = removeCash(data);
      const cloneGoalsArray = data.goalsArray;
      cloneGoalsArray[data.index].projMaturity = data.value;
      dispatch({
        type: ACTION_TYPES[FNA].UPDATE_NA_ANALYSIS_DATA,
        value: {
          targetProduct: PSGOALS,
          targetProfile: data.selectedProfile,
          targetData: { goals: cloneGoalsArray }
        }
      });
    }
  },
  retirementPlanning: {
    retireAgeOnChange(data) {
      dispatch({
        type: ACTION_TYPES[FNA].UPDATE_NA_ANALYSIS_DATASET,
        value: {
          targetProduct: RPLANNING,
          targetProfile: data.selectedProfile,
          targetFunction: "retireAgeOnChange",
          data
        }
      });
    },
    inReqRetirementOnChange(data) {
      dispatch({
        type: ACTION_TYPES[FNA].UPDATE_NA_ANALYSIS_DATASET,
        value: {
          targetProduct: RPLANNING,
          targetProfile: data.selectedProfile,
          targetFunction: "inReqRetirementOnChange",
          data,
          rule: data.rule
        }
      });
    },
    unMatchPmtReasonOnChange(data) {
      dispatch({
        type: ACTION_TYPES[FNA].UPDATE_NA_ANALYSIS_DATASET,
        value: {
          targetProduct: RPLANNING,
          targetProfile: data.selectedProfile,
          targetFunction: "unMatchPmtReasonOnChange",
          data,
          rule: data.rule
        }
      });
    },
    othRegIncomeOnChange(data) {
      dispatch({
        type: ACTION_TYPES[FNA].UPDATE_NA_ANALYSIS_DATASET,
        value: {
          targetProduct: RPLANNING,
          targetProfile: data.selectedProfile,
          targetFunction: "othRegIncomeOnChange",
          data,
          rule: data.rule
        }
      });
    }
  },
  updateNA: ({
    naData,
    pdaData,
    profileData,
    dependantProfilesData,
    feData,
    resetProdType
  }) => {
    dispatch({
      type: ACTION_TYPES[FNA].UPDATE_NA,
      naData,
      pdaData,
      profileData,
      dependantProfilesData,
      feData,
      resetProdType
    });
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Analysis);
