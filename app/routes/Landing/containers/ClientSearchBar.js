import { connect } from "react-redux";
import { TEXT_STORE } from "../../../constants/REDUCER_TYPES";
import ClientSearchBar from "../components/ClientSearchBar";

/**
 * ClientSearchBar
 * @requires ClientSearchBar - ClientSearchBar UI
 * */
const mapStateToProps = state => ({
  textStore: state[TEXT_STORE]
});

const mapDispatchToProps = () => ({});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ClientSearchBar);
