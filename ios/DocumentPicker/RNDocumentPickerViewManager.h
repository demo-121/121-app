//
//  RNDocumentPickerViewManager.h
//  EASE
//
//  Created by Alex Tang on 25/7/2018.
//  Copyright © 2018 Facebook. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <React/RCTBridgeModule.h>
#import <React/RCTViewManager.h>

@interface RNDocumentPickerViewManager : RCTViewManager <RCTBridgeModule, UIDocumentPickerDelegate>

@end
