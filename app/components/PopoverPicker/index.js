import { Platform } from "react-native";
import PopoverPickerIOS from "./PopoverPickerIOS";

export default Platform.select({
  ios: PopoverPickerIOS,
  android: {}
});
