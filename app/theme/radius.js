import { Platform } from "react-native";

const radius = {
  radiusL: 100,
  radiusM: 18,
  radiusS: 4,
  radiusXS: 2
};

const platformRadius = Platform.select({
  ios: {},
  android: {}
});

export default { ...platformRadius, ...radius };
