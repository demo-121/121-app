import PropTypes from "prop-types";
import React, { PureComponent } from "react";
import * as _ from "lodash";
import { REDUCER_TYPES, REDUCER_TYPE_CHECK } from "eab-web-api";
import EABTextBox from "../../EABTextBox";
import styles from "./styles";
import { ContextConsumer } from "../../../context";

class TextArea extends PureComponent {
  render() {
    const { template, rootValues, path, onUpdateValues, error } = this.props;
    const { id, disabled, mandatory, max } = template;

    const value = _.get(rootValues, `${path}.${id}`, "");

    const onChange = ({ newValue }) => {
      const result = newValue;

      onUpdateValues({ id, value: result, path, iTemplate: template });
    };

    return (
      <ContextConsumer>
        {({ language }) => (
          <EABTextBox
            style={styles.container}
            value={value}
            editable={!disabled}
            isError={_.get(error, `${path}.${id}.hasError`, false)}
            isRequired={mandatory}
            hintText={_.get(error, `${path}.${id}.message.${language}`, "")}
            onChange={newValue => {
              onChange({ newValue });
            }}
            maxLength={max}
          />
        )}
      </ContextConsumer>
    );
  }
}

TextArea.propTypes = {
  onUpdateValues: PropTypes.func.isRequired,
  template: REDUCER_TYPE_CHECK[REDUCER_TYPES.APPLICATION].template.isRequired,
  rootValues:
    REDUCER_TYPE_CHECK[REDUCER_TYPES.APPLICATION].rootValues.isRequired,
  path: PropTypes.string.isRequired,
  error: REDUCER_TYPE_CHECK[REDUCER_TYPES.APPLICATION].error.isRequired
};

export default TextArea;
