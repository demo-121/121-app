import { connect } from "react-redux";
import { REDUCER_TYPES } from "eab-web-api";
import AppPasswordLogin from "../components/AppPasswordLogin";
import {
  setLoginFlow,
  loginByAppPassword,
  logout,
  checkAppLocked,
  checkFailAttempt,
  saveNewAppPasswordDEV,
  loginByAppPasswordDEV
} from "../../../actions/login";
import { LOGIN, PAYMENT } from "../../../constants/REDUCER_TYPES";
import ACTION_TYPES from "../../../constants/ACTION_TYPES";
import { hash } from "../../../utilities/SecureUtils";

const { AGENT } = REDUCER_TYPES;
/**
 * AppPasswordLogin
 * @description AppPasswordLogin container.
 * @reducer login
 * @requires AppPasswordLogin - AppPasswordLogin UI
 * */
const mapStateToProps = state => ({
  userID: state[LOGIN].userID,
  login: state[LOGIN],
  agent: state[AGENT],
  isAppLocked: state[LOGIN].isAppLocked,
  loginFlow: state[LOGIN].loginFlow,
  loginFlowCheck: state[LOGIN].loginFlowCheck,
  isTimezoneValid: state[LOGIN].isTimezoneValid,
  isHandlingListener: state[LOGIN].isHandlingListener,
  isOnlineAuthCompleted: state[PAYMENT].isOnlineAuthCompleted,
  environment: state[LOGIN].environment
});

const mapDispatchToProps = dispatch => ({
  setLoginFlow(loginFlow) {
    setLoginFlow({ dispatch, loginFlow });
  },
  checkTimeBomb(callback) {
    dispatch({
      type: ACTION_TYPES[LOGIN].CHECK_TIME_BOMB,
      callback
    });
  },
  logout() {
    logout({ dispatch });
  },
  navigateToOnlineLoginPage() {
    dispatch({
      type: ACTION_TYPES[LOGIN].USE_ONLINE_AUTH
    });
  },
  navigateToOnlineAuth(changeToOnlineAuth) {
    dispatch({
      type: ACTION_TYPES[LOGIN].CHANGE_TO_ONLINE_AUTH,
      changeToOnlineAuth
    });
  },
  loginByAppPassword(userID, appPassword, callback) {
    const hashedAppPassword = hash(appPassword);
    loginByAppPassword({ dispatch, userID, hashedAppPassword, callback });
  },
  loginByAppPasswordDEV(userID, appPassword, callback) {
    const hashedAppPassword = hash("Password3#");
    loginByAppPasswordDEV({ dispatch, userID, hashedAppPassword, callback });
  },
  loginByOnlineAuthForPayment: callback => {
    dispatch({
      type: ACTION_TYPES[PAYMENT].LOGIN_BY_ONLINE_AUTH_FOR_PAYMENT,
      callback
    });
  },
  setOnlineAuthCodeForPayment: ({ authCode, callback }) => {
    dispatch({
      type: ACTION_TYPES[PAYMENT].ONLINE_AUTH_CODE_FOR_PAYMENT,
      authCode,
      callback
    });
  },
  checkTokenExpiryDate: callback => {
    dispatch({
      type: ACTION_TYPES[LOGIN].CHECK_TOKEN_EXPIRY_DATE,
      callback
    });
  },
  checkAppLocked(callback) {
    checkAppLocked({ dispatch, callback });
  },
  checkFailAttempt(callback) {
    checkFailAttempt({ dispatch, callback });
  },
  exceedFailLoginAttempts() {
    dispatch({
      type: ACTION_TYPES[LOGIN].EXCEED_FAIL_LOGIN_ATTEMPS
    });
  },
  saveNewAppPasswordDEV(appPassword, callback) {
    const hashedAppPassword = hash("Password3#");
    saveNewAppPasswordDEV({ dispatch, hashedAppPassword, callback });
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AppPasswordLogin);
