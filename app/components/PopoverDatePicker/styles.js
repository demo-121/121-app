import { StyleSheet } from "react-native";

export default StyleSheet.create({
  popover: {
    position: "absolute",
    overflow: "hidden",
    opacity: 0,
    backgroundColor: "transparent"
  },
  container: {
    position: "absolute",
    top: 0
  },
  /* dialog style */
  headerRow: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    width: "100%"
  }
});
