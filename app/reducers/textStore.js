import { LANGUAGE_TYPES } from "eab-web-api";
import ACTION_TYPES from "../constants/ACTION_TYPES";
import { TEXT_STORE } from "../constants/REDUCER_TYPES";
import textStoreSource from "../locales";
import EABi18n from "../utilities/EABi18n";

/**
 * textStore
 * @default app source code text store. please check EABi18n get more
 *   information
 * @requires textStoreSource - @/locales
 * @requires EABi18n - @/utilities/EABi18n
 * */
export default (state = textStoreSource, { type, textStore }) => {
  switch (type) {
    case ACTION_TYPES[TEXT_STORE].UPDATE_TEXT_STORE:
      state = textStore;
      return state;
    default:
      return state;
  }
};

export const getText = ({ state, path, language = LANGUAGE_TYPES.ENGLISH }) =>
  EABi18n({
    path,
    language,
    textStore: state[TEXT_STORE]
  });
