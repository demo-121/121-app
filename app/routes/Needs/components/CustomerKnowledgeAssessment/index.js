import React, { Component } from "react";
import { Image, View, Text, ScrollView } from "react-native";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import LinearGradient from "react-native-linear-gradient";
import PropTypes from "prop-types";
import { REDUCER_TYPE_CHECK, REDUCER_TYPES, PRODUCT_TYPES } from "eab-web-api";
import icNext from "../../../../assets/images/icNext.png";
import icPrevious from "../../../../assets/images/icPrevious.png";
import icCircleArrowDown from "../../../../assets/images/icCircleArrowDown.png";
import icCircleArrowUp from "../../../../assets/images/icCircleArrowUp.png";
import EABi18n from "../../../../utilities/EABi18n";
import confirmationAlert from "../../../../utilities/confirmationAlert";
import EABTextSelection from "../../../../components/EABTextSelection";
import EABTextBox from "../../../../components/EABTextBox";
import EABTextField from "../../../../components/EABTextField";
import SelectorField from "../../../../components/SelectorField";
import styles from "./styles";
import EABButton from "../../../../components/EABButton";
import { ContextConsumer } from "../../../../context";
import Theme from "../../../../theme";
import { FLAT_LIST } from "../../../../components/Selector/constants";
import { getOptionList } from "../../../../utilities/getOptionList";
import TranslatedText from "../../../../containers/TranslatedText";

import {
  RECTANGLE_BUTTON_1,
  RECTANGLE_BUTTON_2
} from "../../../../components/EABButton/constants";

/**
 * <CustomerKnowledgeAssessment />
 * @param {func} closeDialog - Close the current Dialog
 * @param {func} openNADialog - Open Need Analysis Dialog
 * @param {func} initNAAspects - correct the aspects in the redux state
 * @param {object} optionsMap - customer knowledge assessment data
 * @param {object} productType - Product Types
 * */

const { OPTIONS_MAP, COMMON } = REDUCER_TYPES;

export default class CustomerKnowledgeAssessment extends Component {
  constructor() {
    super();

    this.state = {
      expandBottomSheet: false,
      loadCompleted: false
    };
  }

  componentDidMount() {
    const callback = () => {
      this.setState({
        loadCompleted: true
      });
    };
    this.props.initCKA(callback);
  }

  /**
   * @description save NA after click yes in confirmation alert
   * */
  saveNaWithConfirm(callback) {
    this.props.saveNA({
      confirm: true,
      callback
    });
  }

  /**
   * @description save NA after clicking Done button
   * */
  saveNaWithoutConfirm({ language, callback }) {
    const { textStore, isChanged, saveNA, updateCKAinit } = this.props;
    const onSave = () => {
      saveNA({
        confirm: false,
        callback: code => {
          if (code) {
            setTimeout(() => {
              confirmationAlert({
                messagePath: `error.${code}`,
                language,
                textStore,
                yesOnPress: () => {
                  this.saveNaWithConfirm(callback);
                }
              });
            }, 200);
          } else {
            callback();
          }
        }
      });
    };
    if (isChanged) {
      updateCKAinit(() => {
        onSave();
      });
    } else {
      callback();
    }
  }

  render() {
    const {
      textStore,
      optionsMap,
      productType,
      course,
      courseOnChange,
      isCourseError,
      institution,
      institutionOnChange,
      isInstitutionError,
      studyPeriodEndYear,
      studyPeriodEndYearOnChange,
      isStudyPeriodEndYearError,
      collectiveInvestment,
      collectiveInvestmentOnChange,
      isCollectiveInvestmentError,
      transactionType,
      transactionTypeOnChange,
      isTransactionTypeError,
      insuranceInvestment,
      insuranceInvestmentOnChange,
      isInsuranceInvestmentError,
      insuranceType,
      insuranceTypeOnChange,
      isInsuranceTypeError,
      profession,
      professionOnChange,
      isProfessionError,
      years,
      yearsOnChange,
      yearsOnBlur,
      isYearsError,
      passCka,
      isValid
    } = this.props;

    const { expandBottomSheet } = this.state;

    const { INVESTLINKEDPLANS, PARTICIPATINGPLANS } = PRODUCT_TYPES;

    const studyPeriodEndYearOptions = () => {
      const d = new Date();
      const startYear = 1917;
      const thisYear = d.getFullYear();
      const yearOptions = [];

      for (let year = thisYear; year >= startYear; year -= 1) {
        yearOptions.push({
          title: {
            en: year.toString()
          },
          value: year
        });
      }
      return yearOptions;
    };

    if (this.state.loadCompleted) {
      return (
        <ContextConsumer>
          {({ language }) => (
            <LinearGradient
              {...Theme.LinearGradientProps}
              style={styles.container}
            >
              <View style={styles.header}>
                <View style={styles.titleBar}>
                  <EABButton
                    testID="CustomerKnowledgeAassessment__btnSave"
                    containerStyle={[
                      Theme.headerLeft,
                      { left: Theme.alignmentXL }
                    ]}
                    onPress={() => {
                      this.saveNaWithoutConfirm({
                        language,
                        callback: () => {
                          this.props.closeDialog();
                        }
                      });
                    }}
                  >
                    <TranslatedText
                      style={Theme.textButtonLabelNormalEmphasizedAccent}
                      path="button.save"
                    />
                  </EABButton>

                  <Text style={Theme.title}>
                    {EABi18n({
                      path: "cka.title",
                      language,
                      textStore
                    })}
                  </Text>
                  <EABButton
                    testID="CustomerKnowledgeAassessment__btnDone"
                    containerStyle={[
                      Theme.headerRight,
                      { right: Theme.alignmentXL }
                    ]}
                    onPress={() => {
                      this.saveNaWithoutConfirm({
                        language,
                        callback: () => {
                          this.props.closeDialog();
                        }
                      });
                    }}
                    isDisable={!isValid}
                  >
                    <Text style={Theme.textButtonLabelNormalEmphasizedAccent}>
                      {EABi18n({
                        path: "button.done",
                        language,
                        textStore
                      })}
                    </Text>
                  </EABButton>
                </View>
              </View>
              <KeyboardAwareScrollView extraScrollHeight={40}>
                <ScrollView
                  testID="CustomerKnowledgeAssessment__PageScroll"
                  style={styles.scrollView}
                  contentContainerStyle={styles.scrollViewContent}
                  ref={scrollView => {
                    this.scrollView = scrollView;
                  }}
                >
                  <View key="a" style={styles.questionWrapperFirst}>
                    <Text style={styles.question}>
                      {EABi18n({
                        path: "cka.questionOne.title",
                        language,
                        textStore
                      })}
                      <Text style={styles.questionStar}> *</Text>
                    </Text>
                    <Text style={styles.questionDescription}>
                      {EABi18n({
                        path: "cka.questionOne.description",
                        language,
                        textStore
                      })}
                    </Text>
                    <View style={styles.questionOneWrapper}>
                      <SelectorField
                        testID="CustomerKnowledgeAassessment__ddlCourseOfStudy"
                        style={styles.questionOneSelectorField}
                        value={
                          course ||
                          EABi18n({
                            path: "component.select",
                            language,
                            textStore
                          })
                        }
                        selectorType={FLAT_LIST}
                        isRequired
                        placeholder={EABi18n({
                          path: "cka.questionOne.courseOfStudy",
                          language,
                          textStore
                        })}
                        isError={isCourseError && isCourseError.hasError}
                        hintText={
                          isCourseError && isCourseError.message[language]
                        }
                        flatListOptions={{
                          data: getOptionList({
                            optionMap: optionsMap.course,
                            language
                          }),
                          keyExtractor: item => item.key,
                          selectedValue: null,
                          renderItemOnPress: (itemKey, item) => {
                            if (item) {
                              courseOnChange(item.value);
                            }
                          }
                        }}
                      />
                    </View>
                    {course !== "notApplicable" &&
                      course !== "" && (
                        <View style={styles.questionOneExtraWrapper}>
                          <View style={styles.EABTextBoxSectionWrapper}>
                            <Text style={styles.subTitle}>
                              {EABi18n({
                                path: "cka.questionOne.nameOfInstitution",
                                language,
                                textStore
                              })}
                              <Text style={styles.questionStar}> *</Text>
                            </Text>
                            <View style={styles.fieldWrapperRow}>
                              <EABTextBox
                                testID="CustomerKnowledgeAassessment__txtNameOfInstitution"
                                style={styles.EABTextBox}
                                numberOfLines={5}
                                value={institution}
                                onChange={institutionOnChange}
                              />
                              <Text style={styles.textBoxErrorMsg}>
                                {isInstitutionError &&
                                  isInstitutionError.hasError &&
                                  isInstitutionError.message[language]}
                              </Text>
                            </View>
                          </View>
                          <SelectorField
                            testID="CustomerKnowledgeAassessment__ddlYearOfGraduation"
                            style={styles.questionOneSelectorField}
                            selectorType={FLAT_LIST}
                            value={studyPeriodEndYear}
                            isRequired
                            isError={
                              isStudyPeriodEndYearError &&
                              isStudyPeriodEndYearError.hasError
                            }
                            hintText={
                              isStudyPeriodEndYearError &&
                              isStudyPeriodEndYearError.message[language]
                            }
                            placeholder={EABi18n({
                              path: "cka.questionOne.yearOfGraduation",
                              language,
                              textStore
                            })}
                            flatListOptions={{
                              data: getOptionList({
                                optionMap: {
                                  options: studyPeriodEndYearOptions()
                                },
                                language
                              }),
                              selectedValue: 1992,
                              renderItemOnPress: (itemKey, item) => {
                                if (item) {
                                  studyPeriodEndYearOnChange(item.value);
                                }
                              }
                            }}
                          />
                        </View>
                      )}
                  </View>
                  <View key="b" style={styles.questionWrapper}>
                    <Text style={styles.question}>
                      {EABi18n({
                        path: "cka.questionTwo.title",
                        language,
                        textStore
                      })}
                      <Text style={styles.questionStar}> *</Text>
                    </Text>
                    <Text style={styles.questionDescription}>
                      {EABi18n({
                        path: "cka.questionTwo.description",
                        language,
                        textStore
                      })}
                    </Text>
                    <Text style={styles.subTitle}>
                      {EABi18n({
                        path: "cka.questionTwo.subTitleOne",
                        language,
                        textStore
                      })}
                      <Text style={styles.questionStar}> *</Text>
                    </Text>
                    <View style={styles.fieldWrapperRowNew}>
                      <EABTextSelection
                        testID="CustomerKnowledgeAassessment"
                        options={[
                          {
                            key: "Y",
                            testID: "CollectiveInvestmentSchemesYes",
                            title: EABi18n({
                              path: "cka.second.question.yes",
                              language,
                              textStore
                            }),
                            isSelected: collectiveInvestment === "Y"
                          },
                          {
                            key: "N",
                            testID: "CollectiveInvestmentSchemesNo",
                            title: EABi18n({
                              path: "cka.second.question.no",
                              language,
                              textStore
                            }),
                            isSelected: collectiveInvestment === "N"
                          }
                        ]}
                        editable
                        onPress={collectiveInvestmentOnChange}
                      />
                      <Text style={styles.selectionErrorMsg}>
                        {isCollectiveInvestmentError &&
                          isCollectiveInvestmentError.hasError &&
                          isCollectiveInvestmentError.message[language]}
                      </Text>
                    </View>
                    {collectiveInvestment === "Y" && (
                      <View style={styles.EABTextBoxSectionWrapper}>
                        <Text style={styles.subTitle}>
                          {EABi18n({
                            path: "cka.questionTwo.subTitleTwo",
                            language,
                            textStore
                          })}
                          <Text style={styles.questionStar}> *</Text>
                        </Text>
                        <View style={styles.fieldWrapperRow}>
                          <EABTextBox
                            testID="CustomerKnowledgeAassessment__txtCISTransactionAndProduct"
                            style={styles.EABTextBox}
                            numberOfLines={5}
                            value={transactionType}
                            onChange={transactionTypeOnChange}
                          />
                          <Text style={styles.textBoxErrorMsg}>
                            {isTransactionTypeError &&
                              isTransactionTypeError.hasError &&
                              isTransactionTypeError.message[language]}
                          </Text>
                        </View>
                      </View>
                    )}
                    <Text style={styles.subTitle}>
                      {EABi18n({
                        path: "cka.questionTwo.subTitleThree",
                        language,
                        textStore
                      })}
                      <Text style={styles.questionStar}> *</Text>
                    </Text>
                    <View style={styles.fieldWrapperRowNew}>
                      <EABTextSelection
                        testID="CustomerKnowledgeAassessment"
                        options={[
                          {
                            key: "Y",
                            testID: "InvestmentLinkedLifeInsuranceYes",
                            title: EABi18n({
                              path: "cka.second.question.yes",
                              language,
                              textStore
                            }),
                            isSelected: insuranceInvestment === "Y"
                          },
                          {
                            key: "N",
                            testID: "InvestmentLinkedLifeInsuranceNo",
                            title: EABi18n({
                              path: "cka.second.question.no",
                              language,
                              textStore
                            }),
                            isSelected: insuranceInvestment === "N"
                          }
                        ]}
                        editable
                        onPress={insuranceInvestmentOnChange}
                      />
                      <Text style={styles.selectionErrorMsg}>
                        {isInsuranceInvestmentError &&
                          isInsuranceInvestmentError.hasError &&
                          isInsuranceInvestmentError.message[language]}
                      </Text>
                    </View>
                    {insuranceInvestment === "Y" && (
                      <View style={styles.EABTextBoxSectionWrapper}>
                        <Text style={styles.subTitle}>
                          {EABi18n({
                            path: "cka.questionTwo.subTitleFour",
                            language,
                            textStore
                          })}
                          <Text style={styles.questionStar}> *</Text>
                        </Text>
                        <View style={styles.fieldWrapperRow}>
                          <EABTextBox
                            testID="CustomerKnowledgeAassessment__txtInvestmentTransactionAndProduct"
                            style={styles.EABTextBox}
                            numberOfLines={5}
                            value={insuranceType}
                            onChange={insuranceTypeOnChange}
                          />
                        </View>
                        <Text style={styles.textBoxErrorMsg}>
                          {isInsuranceTypeError &&
                            isInsuranceTypeError.hasError &&
                            isInsuranceTypeError.message[language]}
                        </Text>
                      </View>
                    )}
                  </View>
                  <View key="c" style={styles.questionWrapper}>
                    <Text style={styles.question}>
                      {EABi18n({
                        path: "cka.questionThree.title",
                        language,
                        textStore
                      })}
                      <Text style={styles.questionStar}> *</Text>
                    </Text>
                    <View style={styles.questionOneWrapper}>
                      <SelectorField
                        testID="CustomerKnowledgeAassessment__ddlWorkExperience"
                        style={styles.questionThreeSelectorField}
                        value={
                          profession ||
                          EABi18n({
                            path: "component.select",
                            language,
                            textStore
                          })
                        }
                        selectorType={FLAT_LIST}
                        isError={
                          isProfessionError && isProfessionError.hasError
                        }
                        hintText={
                          isProfessionError &&
                          isProfessionError.message[language]
                        }
                        flatListOptions={{
                          data: getOptionList({
                            optionMap: optionsMap.profession,
                            language
                          }),
                          keyExtractor: item => item.key,
                          selectedValue: null,
                          renderItemOnPress: (itemKey, item) => {
                            if (item) {
                              professionOnChange(item.value);
                            }
                          }
                        }}
                      />
                    </View>
                    {profession !== "notApplicable" &&
                      profession !== "" && (
                        <View>
                          <Text style={styles.questionThreeDescription}>
                            {EABi18n({
                              path: "cka.questionThree.subTitle",
                              language,
                              textStore
                            })}
                            <Text style={styles.questionStar}> *</Text>
                          </Text>
                          <View>
                            <EABTextField
                              testID="CustomerKnowledgeAassessment__txtYearsOfExperience"
                              style={styles.questionThreeTextField}
                              value={years}
                              placeholder={EABi18n({
                                path: "cka.questionThree.years",
                                language,
                                textStore
                              })}
                              onBlur={yearsOnBlur}
                              isError={isYearsError && isYearsError.hasError}
                              hintText={
                                isYearsError && isYearsError.message[language]
                              }
                              onChange={yearsOnChange}
                              maxLength={2}
                            />
                          </View>
                        </View>
                      )}
                  </View>
                  {isValid &&
                    passCka === "Y" && (
                      <View key="e" style={styles.questionWrapper}>
                        <View style={styles.bottomRow}>
                          <View style={styles.bottomTitleWrap}>
                            <Text style={styles.bottomTitle}>
                              {EABi18n({
                                path: "cka.bottom.outcome",
                                language,
                                textStore
                              })}
                            </Text>
                          </View>
                          <EABButton
                            testID="CustomerKnowledgeAassessment__btnOutcome"
                            style={styles.bottomEABButton}
                            onPress={() => {
                              this.setState(
                                {
                                  expandBottomSheet: !expandBottomSheet
                                },
                                () => {
                                  setTimeout(() => {
                                    this.scrollView.scrollToEnd();
                                  }, 100);
                                }
                              );
                            }}
                          >
                            <Image
                              style={styles.bottomEABButtonImage}
                              source={
                                expandBottomSheet
                                  ? icCircleArrowUp
                                  : icCircleArrowDown
                              }
                            />
                            <Text style={Theme.EABButtonLabelSmallAccent}>
                              {expandBottomSheet
                                ? EABi18n({
                                    path: "cka.bottom.buttonLabel.off",
                                    language,
                                    textStore
                                  })
                                : EABi18n({
                                    path: "cka.bottom.buttonLabel.on",
                                    language,
                                    textStore
                                  })}
                            </Text>
                          </EABButton>
                        </View>
                        <View style={styles.bottomOutcomeTextWrap}>
                          <Text style={styles.bottomOutcomeText}>
                            <Text style={styles.underline}>
                              {EABi18n({
                                path: "cka.bottom.outcome.youHaveMet",
                                language,
                                textStore
                              })}
                            </Text>
                            {EABi18n({
                              path: "cka.bottom.outcome.textOne",
                              language,
                              textStore
                            })}
                          </Text>
                          {expandBottomSheet && (
                            <Text style={styles.bottomOutcomeText}>
                              {EABi18n({
                                path: "cka.bottom.outcome.textTwo",
                                language,
                                textStore
                              })}
                            </Text>
                          )}
                        </View>
                      </View>
                    )}
                  {isValid &&
                    passCka === "N" && (
                      <View key="f" style={styles.questionWrapper}>
                        <View style={styles.bottomRow}>
                          <View style={styles.bottomTitleWrap}>
                            <Text style={styles.bottomTitle}>
                              {EABi18n({
                                path: "cka.bottom.outcome",
                                language,
                                textStore
                              })}
                            </Text>
                          </View>
                        </View>
                        <View style={styles.bottomOutcomeTextWrap}>
                          <Text style={styles.bottomOutcomeText}>
                            <Text style={styles.underline}>
                              {EABi18n({
                                path: "cka.bottom.outcome.youHaveNotMet",
                                language,
                                textStore
                              })}
                            </Text>
                            {EABi18n({
                              path: "cka.bottom.outcome.textThree",
                              language,
                              textStore
                            })}
                          </Text>
                          <Text style={styles.bottomOutcomeText}>
                            {EABi18n({
                              path: "cka.bottom.outcome.textFour",
                              language,
                              textStore
                            })}
                          </Text>
                        </View>
                      </View>
                    )}
                  <View key="d" style={styles.questionWrapper}>
                    <Text style={styles.note}>
                      {EABi18n({
                        path: "cka.notes",
                        language,
                        textStore
                      })}
                    </Text>
                  </View>
                </ScrollView>
              </KeyboardAwareScrollView>
              <View style={styles.footer}>
                <EABButton
                  testID="CustomerKnowledgeAassessment__btnBackToNA"
                  buttonType={RECTANGLE_BUTTON_2}
                  onPress={() => {
                    this.saveNaWithoutConfirm({
                      language,
                      callback: () => {
                        this.props.openNADialog();
                      }
                    });
                  }}
                >
                  <Image style={Theme.iconStyle} source={icPrevious} />
                  <Text style={styles.footerPreviousButtonText}>
                    {EABi18n({
                      path: "cka.footer.backToNA",
                      language,
                      textStore
                    })}
                  </Text>
                </EABButton>
                {(productType.prodType.split(",").indexOf(INVESTLINKEDPLANS) >
                  -1 ||
                  productType.prodType.split(",").indexOf(PARTICIPATINGPLANS) >
                    -1) && (
                  <EABButton
                    testID="CustomerKnowledgeAassessment__btnContinueToRA"
                    buttonType={RECTANGLE_BUTTON_1}
                    onPress={() => {
                      this.saveNaWithoutConfirm({
                        language,
                        callback: () => {
                          this.props.openRADialog();
                        }
                      });
                    }}
                    isDisable={!isValid}
                  >
                    <Text style={styles.footerNextButtonText}>
                      {EABi18n({
                        path: "cka.footer.continueToRA",
                        language,
                        textStore
                      })}
                    </Text>
                    <Image style={Theme.iconStyle} source={icNext} />
                  </EABButton>
                )}
              </View>
            </LinearGradient>
          )}
        </ContextConsumer>
      );
    }
    return null;
  }
}

CustomerKnowledgeAssessment.propTypes = {
  initCKA: PropTypes.func.isRequired,
  saveNA: PropTypes.func.isRequired,
  closeDialog: PropTypes.func.isRequired,
  openNADialog: PropTypes.func.isRequired,
  openRADialog: PropTypes.func.isRequired,
  optionsMap: REDUCER_TYPE_CHECK[OPTIONS_MAP].isRequired,
  productType: PropTypes.oneOfType([PropTypes.object]).isRequired,
  textStore: PropTypes.oneOfType([PropTypes.object]).isRequired,
  course: PropTypes.oneOfType([PropTypes.object]).isRequired,
  courseOnChange: PropTypes.func.isRequired,
  isCourseError: REDUCER_TYPE_CHECK[COMMON].error.isRequired,
  institution: PropTypes.oneOfType([PropTypes.object]).isRequired,
  institutionOnChange: PropTypes.func.isRequired,
  isInstitutionError: REDUCER_TYPE_CHECK[COMMON].error.isRequired,
  studyPeriodEndYear: PropTypes.number.isRequired,
  studyPeriodEndYearOnChange: PropTypes.func.isRequired,
  isStudyPeriodEndYearError: REDUCER_TYPE_CHECK[COMMON].error.isRequired,
  collectiveInvestment: PropTypes.oneOfType([PropTypes.object]).isRequired,
  collectiveInvestmentOnChange: PropTypes.func.isRequired,
  isCollectiveInvestmentError: REDUCER_TYPE_CHECK[COMMON].error.isRequired,
  transactionType: PropTypes.oneOfType([PropTypes.object]).isRequired,
  transactionTypeOnChange: PropTypes.func.isRequired,
  isTransactionTypeError: REDUCER_TYPE_CHECK[COMMON].error.isRequired,
  insuranceInvestment: PropTypes.oneOfType([PropTypes.object]).isRequired,
  insuranceInvestmentOnChange: PropTypes.func.isRequired,
  isInsuranceInvestmentError: REDUCER_TYPE_CHECK[COMMON].error.isRequired,
  insuranceType: PropTypes.oneOfType([PropTypes.object]).isRequired,
  insuranceTypeOnChange: PropTypes.func.isRequired,
  isInsuranceTypeError: REDUCER_TYPE_CHECK[COMMON].error.isRequired,
  profession: PropTypes.oneOfType([PropTypes.object]).isRequired,
  professionOnChange: PropTypes.func.isRequired,
  isProfessionError: REDUCER_TYPE_CHECK[COMMON].error.isRequired,
  years: PropTypes.oneOfType([PropTypes.object]).isRequired,
  yearsOnChange: PropTypes.func.isRequired,
  yearsOnBlur: PropTypes.func.isRequired,
  isYearsError: REDUCER_TYPE_CHECK[COMMON].error.isRequired,
  updateCKAisValid: PropTypes.func.isRequired,
  updateCKAinit: PropTypes.func.isRequired,
  passCka: PropTypes.string.isRequired,
  isChanged: PropTypes.bool.isRequired,
  isValid: PropTypes.bool.isRequired
};
