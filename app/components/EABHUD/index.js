import PropTypes from "prop-types";
import React, { PureComponent } from "react";
import { REDUCER_TYPE_CHECK, REDUCER_TYPES } from "eab-web-api";
import { ActivityIndicator, Modal, Text, View } from "react-native";
import Theme from "../../theme";
import styles from "./styles";

const { CONFIG } = REDUCER_TYPES;

/**
 * <EABHUD />
 * @description HUD component
 * @param {boolean} isOpen - indicates whether the HUD is open or not
 * */
class EABHUD extends PureComponent {
  render() {
    // =========================================================================
    // variables
    // =========================================================================
    const { isLoading, isOpen, message } = this.props;
    const shouldOpen = isOpen !== null ? isOpen : isLoading;

    return shouldOpen ? (
      <Modal visible animationType="none" transparent>
        <View style={styles.HUDContainer}>
          <View
            style={message !== "" ? styles.HUDWrapperBig : styles.HUDWrapper}
          >
            <ActivityIndicator
              style={styles.HUD}
              color={Theme.white}
              size="large"
            />
            {message !== "" ? (
              <Text style={styles.message}>{message}</Text>
            ) : null}
          </View>
        </View>
      </Modal>
    ) : (
      this.props.children
    );
  }
}

EABHUD.propTypes = {
  isOpen: PropTypes.bool,
  message: PropTypes.string,
  isLoading: REDUCER_TYPE_CHECK[CONFIG].isLoading.isRequired,
  children: PropTypes.node
};

EABHUD.defaultProps = {
  isOpen: null,
  message: "",
  children: null
};

export default EABHUD;
