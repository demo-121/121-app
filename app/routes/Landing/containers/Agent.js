import { REDUCER_TYPES } from "eab-web-api";
import { connect } from "react-redux";
import Agent from "../components/Agent";
import { TEXT_STORE } from "../../../constants/REDUCER_TYPES";

const { AGENT, AGENTUX } = REDUCER_TYPES;

/**
 * Agent
 * @requires Agent - Agent UI
 * */
const mapStateToProps = state => ({
  agentProfile: state[AGENT],
  agentUXProfile: state[AGENTUX],
  textStore: state[TEXT_STORE],
  agent: state[AGENT]
});

const mapDispatchToProps = () => ({});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Agent);
