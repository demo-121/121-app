import { jobs } from "eab-core-api";
import * as _ from "lodash";

export default async function(jobName) {
  const job = jobs[jobName];
  if (_.isFunction(job && job.execute)) {
    const jobFunction = job.execute();
    await jobFunction;
  } else {
    throw new Error(`can not get the job or job.execute. jobName: ${jobName}`);
  }
}
