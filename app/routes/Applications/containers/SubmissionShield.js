import { connect } from "react-redux";
import { REDUCER_TYPES, ACTION_TYPES } from "eab-web-api";
import {
  TEXT_STORE,
  PAYMENT as APP_PAYMENT,
  LOGIN
} from "../../../constants/REDUCER_TYPES";
import { setLastDataSyncTime } from "../../../actions/login";
import SubmissionShield from "../components/PaymentAndSubmissionShield/submissionShield";
import { submitApplication } from "../../../apis/submission";
import { dataSync } from "../../../utilities/DataManager";

import APP_ACTION_TYPES from "../../../constants/ACTION_TYPES";
/**
 * SubmissionShield
 * @description SubmissionShield in Application Summary page.
 * @requires SubmissionShield - SubmissionShield UI
 * */
const mapStateToProps = state => ({
  environment: state[LOGIN].environment,
  isHandlingListener: state[LOGIN].isHandlingListener,
  isOnlineAuthCompleted: state[APP_PAYMENT].isOnlineAuthCompleted,
  application: state[REDUCER_TYPES.APPLICATION].application,
  apiPage: state[REDUCER_TYPES.APPLICATION].component.apiPage,
  isDisableEappButton:
    state[REDUCER_TYPES.APPLICATION].component.isDisableEappButton,
  payment: state[REDUCER_TYPES.PAYMENT_AND_SUBMISSION].payment,
  submission: state[REDUCER_TYPES.PAYMENT_AND_SUBMISSION].submission,
  error: state[REDUCER_TYPES.PAYMENT_AND_SUBMISSION].error,
  textStore: state[TEXT_STORE],
  agent: state[REDUCER_TYPES.AGENT],
  agentCode: state[LOGIN].agentCode,
  authToken: state[LOGIN].authToken,
  lastDataSyncTime: state[LOGIN].lastDataSyncTime
});

const mapDispatchToProps = dispatch => ({
  disableEappButton: newIsDisableEappButton => {
    dispatch({
      type: ACTION_TYPES[REDUCER_TYPES.APPLICATION].IS_DISABLE_EAPP_BUTTON,
      newIsDisableEappButton
    });
  },
  getApplicationList: callback => {
    dispatch({
      type:
        ACTION_TYPES[REDUCER_TYPES.APPLICATION]
          .SAGA_CLOSE_DIALOG_AND_GET_APPLICATION_LIST,
      callback
    });
  },
  getSubmission: docId => {
    dispatch({
      type:
        ACTION_TYPES[REDUCER_TYPES.PAYMENT_AND_SUBMISSION].SAGA_GET_SUBMISSION,
      docId
    });
  },
  submitPaymentAndSubmission: ({
    application,
    policyDocument,
    appPdf,
    bundle,
    isShield,
    callback
  }) => {
    dispatch({
      type:
        ACTION_TYPES[REDUCER_TYPES.PAYMENT_AND_SUBMISSION]
          .SAGA_SUBMIT_PAYMENT_AND_SUBMISSION,
      isShield,
      application,
      policyDocument,
      appPdf,
      bundle,
      callback
    });
  },
  callApiSubmitApplication: ({ docId, lang, callback }) => {
    dispatch({
      type:
        ACTION_TYPES[REDUCER_TYPES.PAYMENT_AND_SUBMISSION]
          .SAGA_SUBMIT_APPLICATION,
      docId,
      lang,
      api: submitApplication,
      callback
    });
  },
  dataSync: () => {
    dispatch({
      type:
        ACTION_TYPES[REDUCER_TYPES.PAYMENT_AND_SUBMISSION].SAGA_EAPP_DATA_SYNC,
      api: dataSync
    });
  },
  setLastDataSyncTime() {
    setLastDataSyncTime({ dispatch });
  },
  loginByOnlineAuthForPayment: () => {
    dispatch({
      type: APP_ACTION_TYPES[APP_PAYMENT].LOGIN_BY_ONLINE_AUTH_FOR_PAYMENT
    });
  },
  setOnlineAuthCodeForPayment: ({ authCode, callback }) => {
    dispatch({
      type: APP_ACTION_TYPES[APP_PAYMENT].ONLINE_AUTH_CODE_FOR_PAYMENT,
      authCode,
      callback
    });
  },
  checkTokenExpiryDate: callback => {
    dispatch({
      type: APP_ACTION_TYPES[LOGIN].CHECK_TOKEN_EXPIRY_DATE,
      callback
    });
  },
  setApiResponsePage: ({ apiPage }) => {
    dispatch({
      type: ACTION_TYPES[REDUCER_TYPES.APPLICATION].UPDATE_API_PAGE,
      newApiPage: apiPage
    });
  },
  continueApplicationFormShield: ({ applicationId, callback }) => {
    dispatch({
      type:
        ACTION_TYPES[REDUCER_TYPES.APPLICATION]
          .SAGA_CONTINUE_APPLICATION_FORM_SHIELD,
      applicationId,
      callback
    });
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SubmissionShield);
