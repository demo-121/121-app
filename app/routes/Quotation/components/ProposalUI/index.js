import {
  LANGUAGE_TYPES,
  REDUCER_TYPE_CHECK as WEB_API_TYPES_CHECK,
  REDUCER_TYPES as WEB_API_REDUCER_TYPE
} from "eab-web-api";
import PropTypes from "prop-types";
import * as _ from "lodash";
import React, { Component } from "react";
import { View } from "react-native";
import LinearGradient from "react-native-linear-gradient";
import Pdf from "react-native-pdf";
import REDUCER_TYPE_CHECK from "../../../../constants/REDUCER_TYPE_CHECK";
import { TEXT_STORE } from "../../../../constants/REDUCER_TYPES";
import { getAttachmentWithID } from "../../../../utilities/DataManager";
import EABHUD from "../../../../containers/EABHUD";
import Theme from "../../../../theme";
import styles from "./styles";
import {
  PDF_ID_STANDALONE_BI,
  PDF_ID_BI,
  PDF_ID_PRODUCT_SUMMARY,
  PDF_ID_FPX_PRODUCT_SUMMARY,
  PDF_ID_RODUCT_HIGHLIGHT_SHEET,
  PDF_ID_FUND_INFO_BOOKLET,
  PDF_ID_SHIELD_PRODUCT_SUMMARY_1,
  PDF_ID_SHIELD_PRODUCT_SUMMARY
} from "./constants";
import EABi18n from "../../../../utilities/EABi18n";

const { QUOTATION, PROPOSAL } = WEB_API_REDUCER_TYPE;

/**
 * <ProposalUI />
 * @description Proposal page, routes from Quotation Page, Quick Quote History page and Application page.
 * @param {object} navigation - react navigation props
 * @param {object} textStore - textStore reducer state
 * @param {object} quotation - quotation data
 * @param {object} pdfData - pdf data
 * @param {boolean} canEmail - indicate if Email button is shown
 * @param {boolean} canRequote - indicate if Requote button is shown
 * @param {boolean} canClone - indicate if Clone button is shown
 * @param {boolean} isQuickQuote - indicate if the navigation is quick quote
 * */
export default class ProposalUI extends Component {
  constructor(props) {
    super(props);
    this.state = {
      pdfStr: {},
      isLoading: false
    };
  }

  componentDidMount() {
    const {
      navigation,
      textStore,
      quotation,
      pdfData,
      planDetails,
      canEmail,
      canRequote,
      canClone,
      queryQuickQuote,
      cleanQuotation,
      isQuickQuote,
      getApplicationsList,
      updateUiApplicationListFilter,
      avoidNavigate,
      setAvoidNavigate
    } = this.props;

    const segments = _.map(pdfData, data => {
      let path = "";
      switch (data.id) {
        case PDF_ID_STANDALONE_BI:
        case PDF_ID_BI:
          path = "proposal.segmentedControl.policyIllustration";
          break;
        case PDF_ID_PRODUCT_SUMMARY:
        case PDF_ID_SHIELD_PRODUCT_SUMMARY:
        case PDF_ID_FPX_PRODUCT_SUMMARY:
        case PDF_ID_SHIELD_PRODUCT_SUMMARY_1:
          path = "proposal.segmentedControl.productSummary";
          break;
        case PDF_ID_RODUCT_HIGHLIGHT_SHEET:
          path = "proposal.segmentedControl.productHighlightSheet";
          break;
        case PDF_ID_FUND_INFO_BOOKLET:
          path = "proposal.segmentedControl.fundInfoBooklet";
          break;
        default:
          path = "";
          break;
      }

      return {
        key: data.id,
        title: EABi18n({
          language: LANGUAGE_TYPES.ENGLISH,
          path,
          textStore
        }),
        isShowingBadge: false,
        onPress: () => {
          this.setState(
            {
              isLoading: true
            },
            () => {
              setTimeout(() => {
                navigation.setParams({ activatedKey: data.id });
              }, 100);
            }
          );
        }
      };
    });

    const defaultActivetedKey = _.get(segments, "[0].key");

    navigation.setParams({
      activatedKey: defaultActivetedKey,
      segments,
      canEmail,
      canRequote,
      canClone,
      canIllustrate:
        _.get(
          planDetails,
          `${quotation.baseProductCode}.supportIllustration`
        ) === "Y",
      quotationId: _.get(quotation, "id", ""),
      textStore,
      queryQuickQuote,
      cleanQuotation,
      isQuickQuote,
      getApplicationsList,
      updateUiApplicationListFilter,
      avoidNavigate,
      setAvoidNavigate
    });

    _.forEach(pdfData, data => {
      const docId = _.get(data, "docId");
      const attachmentId = _.get(data, "attachmentId");

      if (
        !_.has(data, "data") &&
        !_.isEmpty(docId) &&
        !_.isEmpty(attachmentId)
      ) {
        getAttachmentWithID(docId, attachmentId, pdfStr => {
          this.setState({ pdfStr: { [data.id]: pdfStr.data } });
        });
      }
    });
  }

  render() {
    /**
     * variables
     * */
    const { navigation, pdfData } = this.props;
    const { pdfStr, isLoading } = this.state;
    const activatedKey = navigation.getParam("activatedKey");

    const pdfDataDetail = _.find(pdfData, pdf => pdf.id === activatedKey);
    let pdf = null;
    if (_.has(pdfDataDetail, "data")) {
      pdf = (
        <Pdf
          style={styles.pdf}
          source={{
            uri: `data:application/pdf;base64,${pdfDataDetail.data}`
          }}
          onLoadComplete={() => {
            this.setState({
              isLoading: false
            });
          }}
        />
      );
    } else if (
      _.has(pdfDataDetail, "id") &&
      _.has(pdfStr, `${pdfDataDetail.id}`)
    ) {
      pdf = (
        <Pdf
          style={styles.pdf}
          source={{
            uri: `data:application/pdf;base64,${pdfStr[pdfDataDetail.id]}`
          }}
          onLoadComplete={() => {
            this.setState({
              isLoading: false
            });
          }}
        />
      );
    }

    return (
      <LinearGradient {...Theme.LinearGradientProps} style={styles.container}>
        <EABHUD isOpen={isLoading} />
        <View style={styles.mainView}>
          <View style={styles.pdfContainer}>{pdf}</View>
        </View>
      </LinearGradient>
    );
  }
}

ProposalUI.propTypes = {
  textStore: REDUCER_TYPE_CHECK[TEXT_STORE].isRequired,
  quotation: WEB_API_TYPES_CHECK[QUOTATION].quotation.isRequired,
  pdfData: WEB_API_TYPES_CHECK[PROPOSAL].proposal.isRequired,
  planDetails: WEB_API_TYPES_CHECK[QUOTATION].planDetails.isRequired,
  canEmail: WEB_API_TYPES_CHECK[PROPOSAL].canEmail.isRequired,
  canRequote: WEB_API_TYPES_CHECK[PROPOSAL].canRequote.isRequired,
  canClone: WEB_API_TYPES_CHECK[PROPOSAL].canClone.isRequired,
  isQuickQuote: WEB_API_TYPES_CHECK[QUOTATION].isQuickQuote.isRequired,
  cleanQuotation: PropTypes.func.isRequired,
  queryQuickQuote: PropTypes.func.isRequired,
  getApplicationsList: PropTypes.func.isRequired,
  updateUiApplicationListFilter: PropTypes.func.isRequired,
  avoidNavigate: PropTypes.bool.isRequired,
  setAvoidNavigate: PropTypes.func.isRequired
};
