//
//  EABTableViewController.m
//  EASE
//
//  Created by MacAdmin on 21/10/18.
//  Copyright © 2018 Facebook. All rights reserved.
//

#import "EABTableViewController.h"
#import <CouchbaseLite/CouchbaseLite.h>

#define SCREEN_WIDTH    ([UIScreen mainScreen].bounds.size.width)
#define SCREEN_HEIGHT   ([UIScreen mainScreen].bounds.size.height)

#define bgTableView [UIColor colorWithRed:235.0f/255.0f green:235.0f/255.0f blue:233.0f/255.0 alpha:1]

@interface EABTableViewController ()<UITableViewDataSource,UITableViewDelegate>
{
    UITableView *MyTableView;
    CGFloat heightCell;
    NSMutableArray *dataArray;
    UITextField* searchField;
}

@end

@implementation EABTableViewController
@synthesize authToken = _authToken;
@synthesize wsurl = _wsurl;

- (void)viewDidLoad {
  [super viewDidLoad];
  
  dataArray = [[NSMutableArray alloc] init];
  
  MyTableView=[[UITableView alloc]initWithFrame:CGRectMake(0, 60, SCREEN_WIDTH, SCREEN_HEIGHT - 60)];
  [self.view addSubview:MyTableView];
  MyTableView.backgroundColor = bgTableView;
  [MyTableView setDataSource:self];
  [MyTableView setDelegate:self];
  MyTableView.scrollEnabled=YES;
  
  MyTableView.tableFooterView = [[UIView alloc]initWithFrame:CGRectZero];
  
  UIView *header =[[UIView alloc]initWithFrame:CGRectMake(0, 20, SCREEN_WIDTH, 40)];
  [header setBackgroundColor:[UIColor whiteColor]];
  
  //search text
  CGFloat textFieldWidth = SCREEN_WIDTH - 200;
  CGFloat textFieldHeight = 30;
  searchField = [[UITextField alloc] initWithFrame:CGRectMake(10 , 5, textFieldWidth, textFieldHeight)];
  searchField.borderStyle = UITextBorderStyleRoundedRect;
  searchField.autocapitalizationType = UITextAutocapitalizationTypeNone;
  searchField.clearButtonMode = UITextFieldViewModeWhileEditing;
  [header addSubview:searchField];
  
  //search button
  UIButton *searchBtn= [UIButton buttonWithType:UIButtonTypeRoundedRect];
  searchBtn.frame=CGRectMake(SCREEN_WIDTH - 180, 5, 80, 30);
  searchBtn.layer.cornerRadius = 2;
  searchBtn.layer.borderWidth = 2;
  searchBtn.layer.borderColor= [[UIColor lightGrayColor] CGColor];
  searchBtn.backgroundColor=[UIColor clearColor];
  [searchBtn setTitle:@"Search" forState:UIControlStateNormal];
  [searchBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
  [header addSubview:searchBtn];
  
  //close button
  UIButton *closeBtn= [UIButton buttonWithType:UIButtonTypeRoundedRect];
  closeBtn.frame=CGRectMake(SCREEN_WIDTH - 90, 5, 80, 30);
  closeBtn.layer.cornerRadius = 2;
  closeBtn.layer.borderWidth = 2;
  closeBtn.layer.borderColor= [[UIColor lightGrayColor] CGColor];
  closeBtn.backgroundColor=[UIColor clearColor];
  [closeBtn setTitle:@"Close" forState:UIControlStateNormal];
  [closeBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
  [header addSubview:closeBtn];
  
  [self.view addSubview:header];
  
  [searchBtn addTarget:self action:@selector(searchBtnEv) forControlEvents:UIControlEventTouchDown];
  [closeBtn addTarget:self action:@selector(closeBtnEv) forControlEvents:UIControlEventTouchDown];
}

-(void) closeBtnEv
{
  [self dismissViewControllerAnimated:YES completion:nil];
}

-(void) searchBtnEv
{
  [self.view endEditing:YES];
  if (searchField.text != nil && searchField.text.length > 0){
    NSLog(@"=====this is search wording : %@", searchField.text);
    // Create the array to hold the table data
    if (dataArray != nil && dataArray.count > 0) {
      [dataArray removeAllObjects];
    }
    
    NSError *error;
    CBLManager *dbManager = [CBLManager sharedInstance];
    CBLDatabase* db;
    db = [dbManager databaseNamed: @"axasg_dev" error: &error];
    
    CBLQuery* query = [[db viewNamed: searchField.text] createQuery];
    //query.descending = YES;
    //query.limit = 20;
    
    CBLQueryEnumerator *rowEnum = [query run:&error];
    if (rowEnum != nil && rowEnum.count > 0) {
      for (CBLQueryRow *row in rowEnum) {
        CBLDocument* doc = row.document;
        if (doc != nil) {
          //NSLog(@"=========>>>>%@",doc[@"total_rows"]);
          NSDictionary* properties = doc.properties;
          
          NSData *postData = [NSJSONSerialization dataWithJSONObject:properties options:0 error:&error];
          NSString *jsonString = [[NSString alloc] initWithData:postData encoding:NSUTF8StringEncoding];
          //NSLog(@"%@", jsonString);
          
          [dataArray addObject:jsonString];
        }
      }
    }else{
      CBLDocument* doc = [db documentWithID: searchField.text];
      if (doc != nil) {
        NSDictionary* properties = doc.properties;
        NSData *postData = [NSJSONSerialization dataWithJSONObject:properties options:0 error:&error];
        NSString *jsonString = [[NSString alloc] initWithData:postData encoding:NSUTF8StringEncoding];
        [dataArray addObject:jsonString];
      }
    }
    [MyTableView reloadData];
  } else {
    //show the alter message
    UIAlertController *alterController = [UIAlertController alertControllerWithTitle:@"Search View" message:@"search field is required!" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
      [searchField becomeFirstResponder];
    }];
    [alterController addAction:okAction];
    [self presentViewController:alterController animated:NO completion:nil];
  }
}

#pragma mark-UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return dataArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
  MyTableView.separatorStyle=UITableViewCellSeparatorStyleNone;
  static NSString *CellIdentifier = @"Cell";
  UITableViewCell*cell = [MyTableView cellForRowAtIndexPath:indexPath];
  if (!cell) {
      cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
      cell.accessoryType = UITableViewCellAccessoryNone;
      cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
  }
  
  UILabel *materialNameLbe=[[UILabel alloc]initWithFrame:CGRectMake(10, 5, SCREEN_WIDTH-10*2, 20)];
  [cell addSubview:materialNameLbe];
  materialNameLbe.textAlignment=NSTextAlignmentLeft;
  materialNameLbe.font=[UIFont systemFontOfSize:16];
  
  materialNameLbe.text=dataArray[indexPath.row];
  materialNameLbe.numberOfLines=0;
  materialNameLbe.lineBreakMode=NSLineBreakByWordWrapping;
  CGSize size=[materialNameLbe sizeThatFits:CGSizeMake(materialNameLbe.frame.size.width, MAXFLOAT)];

  UIView *xianView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 1)];
  [cell addSubview:xianView];
  xianView.backgroundColor=bgTableView;

  if (size.height>20) {
      materialNameLbe.frame=CGRectMake(10, 5, materialNameLbe.frame.size.width, size.height);
      heightCell=size.height+5*2;
      xianView.frame=CGRectMake(0, size.height+5*2, SCREEN_WIDTH, 1);
  }else{
      materialNameLbe.frame=CGRectMake(10,5, materialNameLbe.frame.size.width, 20);
      heightCell=20+5*2;
      xianView.frame=CGRectMake(0, 20+5*2, SCREEN_WIDTH, 1);
  }
  
  return cell;
}
#pragma mark-UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
}

- (BOOL)tableView:(UITableView *)tableView shouldShowMenuForRowAtIndexPath:(NSIndexPath *)indexPath
{
  return YES;
}

- (BOOL)tableView:(UITableView *)tableView canPerformAction:(SEL)action forRowAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender
{
  if (action == @selector(copy:)) {
    return YES;
  }
  return NO;
}

- (void)tableView:(UITableView *)tableView performAction:(SEL)action forRowAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender
{
  if (action == @selector(copy:)) {
    UIPasteboard *pasteBoard = [UIPasteboard generalPasteboard];
    NSLog(@"=====this is [%ld] cell data : %@", (long)indexPath.row, [dataArray objectAtIndex:indexPath.row]);
    [pasteBoard setString: [dataArray objectAtIndex:indexPath.row]];
  }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
  
  UILabel *materialNameLbe=[[UILabel alloc]initWithFrame:CGRectMake(10, 5, SCREEN_WIDTH-10*2, 20)];
  materialNameLbe.textAlignment=NSTextAlignmentLeft;
  materialNameLbe.font=[UIFont systemFontOfSize:16];
  
  materialNameLbe.text=dataArray[indexPath.row];
  
  materialNameLbe.numberOfLines=0;
  materialNameLbe.lineBreakMode=NSLineBreakByWordWrapping;
  CGSize size=[materialNameLbe sizeThatFits:CGSizeMake(materialNameLbe.frame.size.width, MAXFLOAT)];
  if (size.height>20) {
      materialNameLbe.frame=CGRectMake(10, 5, materialNameLbe.frame.size.width, size.height);
      heightCell=size.height+5*2;
  }else{
      materialNameLbe.frame=CGRectMake(10,5, materialNameLbe.frame.size.width, 20);
      heightCell=20+5*2;
  }
  return heightCell+1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
  return 0;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

