import { REDUCER_TYPES, ACTION_TYPES, FILTER_TYPES } from "eab-web-api";
import { connect } from "react-redux";
import { TEXT_STORE } from "../../../constants/REDUCER_TYPES";
import ProposalUI from "../components/ProposalUI";

const { PROPOSAL, QUOTATION } = REDUCER_TYPES;

const mapStateToProps = state => ({
  textStore: state[TEXT_STORE],
  quotation: state[PROPOSAL].quotation,
  pdfData: state[PROPOSAL].pdfData,
  planDetails: state[PROPOSAL].planDetails,
  canEmail: state[PROPOSAL].canEmail,
  canRequote: state[PROPOSAL].canRequote,
  canClone: state[PROPOSAL].canClone,
  isQuickQuote: state[QUOTATION].isQuickQuote,
  avoidNavigate: state[QUOTATION].avoidNavigate
});

const mapDispatchToProps = dispatch => ({
  cleanQuotation: () => {
    dispatch({
      type: ACTION_TYPES[QUOTATION].CLEAN_QUOTATION
    });
  },
  queryQuickQuote: callback => {
    dispatch({
      type: ACTION_TYPES[REDUCER_TYPES.QUOTATION].GET_QUICK_QUOTES,
      callback
    });
  },
  getApplicationsList: callback => {
    dispatch({
      type: ACTION_TYPES[REDUCER_TYPES.PRE_APPLICATION].SAGA_GET_APPLICATIONS,
      callback
    });
  },
  updateUiApplicationListFilter: () => {
    dispatch({
      type:
        ACTION_TYPES[REDUCER_TYPES.PRE_APPLICATION]
          .UPDATE_UI_APPLICATION_LIST_FILTER,
      selectedAppListFilter: FILTER_TYPES.APPLICATION_LIST.PROPOSED
    });
  },
  setAvoidNavigate: bool => {
    dispatch({
      type: ACTION_TYPES[QUOTATION].AVOID_NAVIGATE,
      bool
    });
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProposalUI);
