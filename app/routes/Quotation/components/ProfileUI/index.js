import PropTypes from "prop-types";
import React, { PureComponent } from "react";
import { Text, View } from "react-native";
import Avatar from "../../../../components/Avatar";
import Theme from "../../../../theme";
import styles from "./styles";

/**
 * <ProfileUI />
 * @description quotation profile ui component.
 * @param {string} name - client name
 * @param {string} firstDescription - client information first row
 * @param {object|number|string} [photo=""] - client photo
 * @param {string} [secondDescription=""] - client information first row
 * @param {number} [photoWidth=40] - photo width
 * @param {number} [photoHeight=40] - photo height
 * @param {object} [style={}] - custom style
 * */
export default class ProfileUI extends PureComponent {
  render() {
    /**
     * variables
     * */
    const {
      photo,
      firstDescription,
      secondDescription,
      name,
      photoWidth,
      photoHeight,
      style
    } = this.props;

    return (
      <View style={[styles.container, style]}>
        <Avatar
          key="photo"
          style={styles.avatarWrapper}
          width={photoWidth}
          height={photoHeight}
          source={photo}
        />
        <View style={styles.descriptionWrapper}>
          <Text style={Theme.bodyPrimary}>{name}</Text>
          <Text style={Theme.subheadSecondary}>{firstDescription}</Text>
          {secondDescription !== "" ? (
            <Text style={Theme.subheadSecondary}>{secondDescription}</Text>
          ) : null}
        </View>
      </View>
    );
  }
}

ProfileUI.propTypes = {
  photo: PropTypes.oneOfType([
    PropTypes.object,
    PropTypes.number,
    PropTypes.string
  ]),
  photoWidth: PropTypes.number,
  photoHeight: PropTypes.number,
  name: PropTypes.string.isRequired,
  firstDescription: PropTypes.string.isRequired,
  secondDescription: PropTypes.string,
  style: PropTypes.oneOfType([
    PropTypes.number,
    PropTypes.object,
    PropTypes.array
  ])
};

ProfileUI.defaultProps = {
  photoWidth: 40,
  photoHeight: 40,
  photo: "",
  secondDescription: "",
  style: {}
};
