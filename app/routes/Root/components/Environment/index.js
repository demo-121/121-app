import PropTypes from "prop-types";
import React, { PureComponent } from "react";
import LinearGradient from "react-native-linear-gradient";
import {
  View,
  Image,
  KeyboardAvoidingView,
  NetInfo,
  AsyncStorage
} from "react-native";
import axaLogo from "../../../../assets/images/axaLogo.png";
import EABButton from "../../../../components/EABButton";
import { RECTANGLE_BUTTON_1 } from "../../../../components/EABButton/constants";
import Theme from "../../../../theme";
import styles from "./styles";
import TranslatedText from "../../../../containers/TranslatedText";
import SelectorField from "../../../../components/SelectorField";
import { PICKER } from "../../../../components/SelectorField/constants";
import { IS_APP_LOCKED } from "../../../../constants/ENCRYPTION_FIELD_NAMES";
// import { composeProviders } from "redux-saga-test-plan/providers";
/**
 * <Environment />
 * @param {function} setEnvironment - action after getting the authCode
 * */
export default class Environment extends PureComponent {
  constructor(props) {
    super(props);
    this.envSetting = this.envSetting.bind(this);
    this.state = {
      environment: "SIT3_EAB",
      status: null
    };
    this.handleConnectionChange = this.handleConnectionChange.bind(this);
  }

  componentDidMount() {
    // We don't need check app lock status when agent perform activation or the APP can't log in foverer, so remove the following code.
    // const tilteTimeBomb = "Time Bomb";
    // const warningMessage2 =
    //   "The app has been locked as you have not synced your data recently. Please uninstall and install the app again.";
    // this.props.checkAppLocked(isLocked => {
    //   if (isLocked) {
    //     setTimeout(() => {
    //       Alert.alert(tilteTimeBomb, warningMessage2);
    //     }, 500);
    //   }
    // });
    AsyncStorage.setItem(IS_APP_LOCKED, "false");
    NetInfo.isConnected.addEventListener(
      "connectionChange",
      this.handleConnectionChange
    );

    NetInfo.isConnected.fetch().done(isConnected => {
      this.setState({
        status: isConnected
      });
    });
  }
  componentWillUnmount() {
    NetInfo.isConnected.removeEventListener(
      "connectionChange",
      this.handleConnectionChange
    );
  }

  handleConnectionChange(isConnected) {
    this.setState({ status: isConnected });
  }

  envSetting() {
    const getEnvironment = this.state.environment;
    if (this.props.setEnvironment != null) {
      this.props.setEnvironment(getEnvironment);
    }
  }
  render() {
    if (this.props.isAppLocked) {
      return null;
    } else if (this.state.status) {
      return (
        <View style={styles.box}>
          <LinearGradient
            {...Theme.LinearGradientProps}
            style={styles.container}
          >
            <KeyboardAvoidingView behavior="padding">
              <View style={styles.loginForm}>
                <View style={styles.logoWrapper}>
                  <Image style={styles.logo} source={axaLogo} />
                </View>
                <TranslatedText
                  style={styles.welcome}
                  path="login.EnvironmentSettingHeader"
                />
                <SelectorField
                  value={this.state.environment}
                  placeholder="Select Environment"
                  selectorType={PICKER}
                  popoverOptions={{
                    preferredContentSize: [300, 200],
                    permittedArrowDirections: [0]
                  }}
                  pickerOptions={{
                    items: [
                      { label: "DEV", value: "DEV" },
                      { label: "SIT1_EAB", value: "SIT1_EAB" },
                      { label: "SIT2_EAB", value: "SIT2_EAB" },
                      { label: "SIT3_EAB", value: "SIT3_EAB" },
                      { label: "SIT_AXA", value: "SIT_AXA" },
                      { label: "UAT", value: "UAT" },
                      { label: "PRE_PROD", value: "PRE_PROD" }
                    ],
                    onValueChange: itemValue => {
                      this.setState({
                        environment: itemValue
                      });
                    }
                    // environment: this.state.environment
                  }}
                />
                <EABButton
                  style={styles.button}
                  buttonType={RECTANGLE_BUTTON_1}
                  onPress={this.envSetting}
                >
                  <TranslatedText path="login.EnvironmentButton" />
                </EABButton>
              </View>
            </KeyboardAvoidingView>
          </LinearGradient>
        </View>
      );
    }

    return (
      <View style={styles.container}>
        <View style={styles.loginForm}>
          <View style={styles.logoWrapper}>
            <Image style={styles.logo} source={axaLogo} />
          </View>
          <TranslatedText style={styles.welcome} path="login.connectNetwork" />
        </View>
      </View>
    );
  }
}

Environment.propTypes = {
  isAppLocked: PropTypes.bool,
  setEnvironment: PropTypes.func.isRequired,
  setAppLocked: PropTypes.func.isRequired,
  checkAppLocked: PropTypes.func.isRequired
};
Environment.defaultProps = {
  isAppLocked: false
};
