//
//  ViewController.m
//  couchbase-sample
//
//  Created by MacAdmin on 23/7/18.
//  Copyright © 2018 MacAdmin. All rights reserved.
//

#import "ViewController.h"
#import <CouchbaseLite/CouchbaseLite.h>
#import "AppDelegate.h"
#import <React/RCTBundleURLProvider.h>
#import <React/RCTRootView.h>

@interface ViewController()
  @property AppDelegate *delegate;
@end

@implementation ViewController

-(instancetype) initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil{
  NSLog(@"=======>>>>%s",__FUNCTION__);
  if (self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) {
    NSLog(@"================ViewController :going initWithNibName, storyBoard goto ===========");
    NSLog(@"================ViewController :start sync===========");
    self.delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [self startReplication];
  }
  return self;
}

-(instancetype) initWithCoder:(NSCoder *)aDecoder{
  NSLog(@"=======>>>>%s",__FUNCTION__);
  if (self = [super initWithCoder:aDecoder]) {
    NSLog(@"================goto initWithCoder===========");
  }
  return self;
}

- (void)viewDidLoad {
  
  [super viewDidLoad];
  NSLog(@"====================================");
 
  
}

-(void)startReplication
{
  
  NSError *error;
  CBLManager* dbManager = [CBLManager sharedInstance];
  CBLDatabase* database = [dbManager databaseNamed: @"axasg_devNEW" error: &error];
  
  NSLog(@"===========Here is the synchronization when the program starts============");
  
  NSURL* url = [NSURL URLWithString: @"http://localhost:4985/axasg_dev"];
  CBLReplication *pull = [database createPullReplication: url];
  pull.continuous = NO;
  
  //fixed "CredStore - performQuery - Error copying matching creds.  Error=-25300"
  id<CBLAuthenticator> auth;
  auth = [CBLAuthenticator basicAuthenticatorWithName: @"" password: @""];
  pull.authenticator = auth;
  
  pull.channels = @[@"eab_public_data"];
  //  pull.channels = @[@"master_data",@"000004_private_data"];
  
  [[NSNotificationCenter defaultCenter] addObserver: self
                                           selector: @selector(replicationChanged:)
                                               name: kCBLReplicationChangeNotification
                                             object: pull];
  [pull start];
  
  self.delegate.pull = pull;
}


- (void) replicationChanged: (NSNotification*)n {
  double progress = 0.0;
  double total = self.delegate.pull.changesCount;
  if (total > 0.0) {
    progress = self.delegate.pull.completedChangesCount / total * 100;
  }
  
  //NSLog(@"Synchronize progress when the program starts: %@%%",[NSString stringWithFormat: @"%.2lf", progress]);
  
}

- (void)didReceiveMemoryWarning {
  [super didReceiveMemoryWarning];
  // Dispose of any resources that can be recreated.
}

@end

