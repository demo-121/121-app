import { saga } from "eab-web-api";
import React, { Component } from "react";
import {
  AppState,
  NativeModules,
  YellowBox,
  Alert,
  Linking
} from "react-native";
import Config from "react-native-config";
import SafariView from "react-native-safari-view";
import { Provider } from "react-redux";

import { Sentry } from "react-native-sentry";
import { dataSync } from "./app/utilities/DataManager";
import {
  TYPE_FIRST_WARNING,
  TYPE_SECOND_WARNING,
  TYPE_TIME_BOMB
} from "./app/constants/TIME_BOMB";
import UnActiveScreen from "./app/components/UnActiveScreen";
import ACTION_TYPES from "./app/constants/ACTION_TYPES";
import { TEXT_STORE, LOGIN, PAYMENT } from "./app/constants/REDUCER_TYPES";
import PlayGround from "./app/containers/Playground";
import { ContextProvider, initialContextData } from "./app/context";
import textStoreSource from "./app/locales";
import Root from "./app/routes/Root";
import { INFLIGHT_JOB_ALERT } from "./app/constants/ERROR_MSG";
import rootSaga from "./app/saga";
import { initialTextStore } from "./app/utilities/EABi18n";
import { sagaMiddleware, appStore } from "./app/utilities/store";

import { BUILD_NUMBER, VERSION_NUMBER } from "./app/constants/BUILD_CONFIG";

Sentry.config("https://10a935c12f884043aa5ab6b50d0088f9@sentry.io/1257671", {
  deactivateStacktraceMerging: false
}).install();

NativeModules.EnvConfig.loadEnvConfig(process.env);

// if return false, event will not be sent
Sentry.setShouldSendCallback(() => true);

Sentry.setTagsContext({
  environment: process.env.NODE_ENV,
  buildNumber: BUILD_NUMBER,
  versionNumber: VERSION_NUMBER
});

YellowBox.ignoreWarnings(["Require cycle:"]);

export default class App extends Component {
  // ===========================================================================
  // class methods
  // ===========================================================================
  /**
   * @description app initialize
   * @return {object} app initialized data
   * */
  static initialize() {
    sagaMiddleware.run(saga);
    sagaMiddleware.run(rootSaga);

    initialTextStore(textStoreSource).then(response => {
      if (response !== textStoreSource) {
        appStore.dispatch({
          type: ACTION_TYPES[TEXT_STORE].UPDATE_TEXT_STORE,
          textStore: response
        });
      }
    });

    return { store: appStore };
  }

  constructor() {
    super();

    const { store } = App.initialize();
    this.handleAppStateChange = this.handleAppStateChange.bind(this);

    this.dataSyncTrigger = this.dataSyncTrigger.bind(this);
    this.onlineAuthCallback = this.onlineAuthCallback.bind(this);

    this.state = {
      store,
      appState: AppState.currentState,
      timeBombType: null,
      currentAppState: "active",
      ...initialContextData(this)
    };
  }
  // ===========================================================================
  // life circles
  // ===========================================================================
  componentDidMount() {
    AppState.addEventListener("change", this.handleAppStateChange);
    Linking.addEventListener("url", this.onlineAuthCallback);
    // Subscription: inform you when something change (state change)
    this.state.store.subscribe(() => {
      if (this.state.store.getState().login.triggerInflightAlert) {
        this.state.store.dispatch({
          type: ACTION_TYPES[LOGIN].DISABLE_INFLIGHT_ALERT,
          callback: () => {}
        });
        setTimeout(() => {
          Alert.alert(
            "Warning",
            INFLIGHT_JOB_ALERT,
            [
              {
                text: "OK",
                onPress: () => {},
                style: "cancel"
              }
            ],
            { cancelable: false }
          );
        }, 200);
      }
    });
  }

  componentWillUnmount() {
    AppState.removeEventListener("change", this.handleAppStateChange);
    Linking.removeEventListener("url", this.onlineAuthCallback);
  }
  onlineAuthCallback(response) {
    this.setState({
      timeBombType: null
    });
    if (response.url) {
      const urlGet = response.url;
      SafariView.dismiss();
      const segment1 = urlGet.replace("easemobile://?", "");
      const variables = segment1.split("&");
      const codeString = variables.find(variable => variable.includes("code"));
      const [, authCode] = codeString.split("=");
      if (this.state.store.getState().login.isHandlingListener) {
        this.state.store.dispatch({
          type: ACTION_TYPES[PAYMENT].ONLINE_AUTH_CODE_FOR_PAYMENT,
          authCode,
          callback: callback => {
            if (!callback.hasError) {
              const isOnlineAuthCompletedGet = this.state.store.getState()
                .payment.isOnlineAuthCompleted;
              if (isOnlineAuthCompletedGet) {
                this.dataSyncTrigger();
              }
            } else if (callback.hasError) {
              if (callback.errorMsg || callback.error) {
                Alert.alert(`[Error] ${callback.errorMsg} ${callback.error}`);
              } else {
                Alert.alert(
                  `[Error] Authorization for submission Failed (Error 001)`
                );
              }
            }
          }
        });
      }
    }
  }
  // ===========================================================================
  // class methods
  // ===========================================================================
  /**
   * @description update app status
   * */
  handleAppStateChange(nextAppState) {
    // const title = "Online Auth is needed for Data Sync";
    if (
      this.state.appState.match(/inactive|background/) &&
      nextAppState === "active"
    ) {
      const getTimeZone = NativeModules.SignDocDialogManager;
      getTimeZone.getTimezone(timezone => {
        const timeZone = timezone.value;
        if (
          timeZone.indexOf("Singapore") === -1 &&
          timeZone.indexOf("Hong_Kong") === -1 &&
          timeZone.indexOf("Shanghai") === -1 &&
          this.state.store.getState().login.isLogin
        ) {
          Alert.alert(
            "Warning",
            `This app requires your device to be in Singapore time zone. Please change the time zone to Singapore and log in again.`,
            [
              {
                text: "OK",
                onPress: () => {
                  this.state.store.dispatch({
                    type: ACTION_TYPES[LOGIN].LOGOUT
                  });
                },
                style: "cancel"
              }
            ],
            { cancelable: false }
          );
        }
      });
      if (
        this.state.store.getState().login.isLogin &&
        !this.state.store.getState().login.isHandlingListener
      ) {
        this.state.store.dispatch({
          type: ACTION_TYPES[LOGIN].CHECK_APP_LOCKED,
          callback: isLocked => {
            if (isLocked) {
              Alert.alert(
                "TimeBomb Triggers",
                `The app has been locked as you have not synced your data recently. Please uninstall and install the app again.`,
                [
                  {
                    text: "OK",
                    onPress: () => {
                      this.state.store.dispatch({
                        type: ACTION_TYPES[LOGIN].LOGOUT
                      });
                    }
                  }
                ],
                { cancelable: false }
              );
            } else {
              this.state.store.dispatch({
                type: ACTION_TYPES[LOGIN].CHECK_TIME_BOMB,
                callback: showAlert => {
                  if (!showAlert.hasError) {
                    if (showAlert.result === TYPE_TIME_BOMB) {
                      this.setState({
                        timeBombType: TYPE_TIME_BOMB
                      });
                      Alert.alert(
                        "TimeBomb Triggers",
                        `The app has been locked as you have not synced your data recently. Please uninstall and install the app again.`,
                        [
                          {
                            text: "OK",
                            onPress: () => {}
                          }
                        ],
                        { cancelable: false }
                      );
                    } else if (showAlert.result === TYPE_SECOND_WARNING) {
                      const daysLeft = showAlert.daysLeft
                        ? showAlert.daysLeft
                        : null;
                      this.setState({
                        timeBombType: TYPE_SECOND_WARNING
                      });
                      Alert.alert(
                        "Data Sync",
                        `App is locked because Timebomb for this App will explode in ${daysLeft} day(s). Please perform data sync first`,
                        [
                          {
                            text: "OK",
                            onPress: () => this.handleTimeBombDataSync()
                          }
                        ],
                        { cancelable: false }
                      );
                    } else if (showAlert.result === TYPE_FIRST_WARNING) {
                      const daysLeft = showAlert.daysLeft
                        ? showAlert.daysLeft
                        : null;
                      Alert.alert(
                        `Time Bomb for this App will explode in ${daysLeft} day(s). Please connect to internet and Login`
                      );
                    }
                  }
                }
              });
            }
          }
        });
      }
      if (
        this.state.store.getState().login.isHandlingListener &&
        this.state.timeBombType === TYPE_SECOND_WARNING
      ) {
        this.state.store.dispatch({
          type: ACTION_TYPES[LOGIN].CHECK_TIME_BOMB,
          callback: showAlert => {
            if (!showAlert.hasError) {
              if (showAlert.result === TYPE_SECOND_WARNING) {
                const daysLeft = showAlert.daysLeft ? showAlert.daysLeft : null;
                Alert.alert(
                  "Data Sync 2",
                  `App is locked because Timebomb for this App will explode in ${daysLeft} day(s). Please perform data sync first`,
                  [
                    {
                      text: "OK",
                      onPress: () => this.handleTimeBombDataSync()
                    }
                  ],
                  { cancelable: false }
                );
              }
            }
          }
        });
      }
    }
    this.setState({
      currentAppState: "active",
      appState: nextAppState
    });
  }

  dataSyncTrigger() {
    dataSync(
      this.state.store.getState().login.environment,
      this.state.store.getState().agent.agentCode,
      this.state.store.getState().login.profileId,
      `Bearer ${this.state.store.getState().login.authToken}`,
      false,
      false,
      this.state.store.getState().login.sessionCookieSyncGatewaySession,
      this.state.store.getState().login.sessionCookieExpiryDate,
      this.state.store.getState().login.sessionCookiePath,
      this.state.store.getState().login.syncDocumentIds,
      () => {
        this.state.store.dispatch({
          type: ACTION_TYPES[LOGIN].HANDLE_LISTENER,
          isCheckingScreenOnOff: false
        });
      }
    );
  }
  pressHandler() {
    const environmentGet = this.state.store.getState().login.environment;
    if (Config.ENV_NAME === "PROD") {
      this.path = Config.GET_AUTH_CODE_FULL_URL;
    } else if (environmentGet === "DEV") {
      this.path = Config.DEV_GET_AUTH_CODE_FULL_URL;
    } else if (environmentGet === "SIT1_EAB") {
      this.path = Config.SIT1_EAB_GET_AUTH_CODE_FULL_URL;
    } else if (environmentGet === "SIT2_EAB") {
      this.path = Config.SIT2_EAB_GET_AUTH_CODE_FULL_URL;
    } else if (environmentGet === "SIT3_EAB") {
      this.path = Config.SIT3_EAB_GET_AUTH_CODE_FULL_URL;
    } else if (environmentGet === "SIT_AXA") {
      this.path = Config.SIT_AXA_GET_AUTH_CODE_FULL_URL;
    } else if (environmentGet === "UAT") {
      this.path = Config.UAT_GET_AUTH_CODE_FULL_URL;
    } else if (environmentGet === "PRE_PROD") {
      this.path = Config.PRE_PROD_GET_AUTH_CODE_FULL_URL;
    } else {
      this.path = Config.SIT3_EAB_GET_AUTH_CODE_FULL_URL;
    }
    SafariView.isAvailable()
      .then(
        SafariView.show({
          url: this.path
          // url: "eabtest://"
        })
      )
      .catch(error => {
        // Fallback WebView code for iOS 8 and earlier
        setTimeout(() => {
          Alert.alert(`[Error]${error}`);
        }, 200);
      });
  }

  handleTimeBombDataSync() {
    const title = "Data Sync";
    const warningMessage = "Online authentication is required.";
    this.state.store.dispatch({
      type: ACTION_TYPES[LOGIN].CHECK_TOKEN_EXPIRY_DATE,
      callback: status => {
        if (!status) {
          this.state.store.dispatch({
            type: ACTION_TYPES[PAYMENT].LOGIN_BY_ONLINE_AUTH_FOR_PAYMENT,
            callback: () => {}
          });
          Alert.alert(
            title,
            warningMessage,
            [
              {
                text: "Continue",
                onPress: () => {
                  this.pressHandler();
                  this.state.store.dispatch({
                    type: ACTION_TYPES[LOGIN].HANDLE_LISTENER,
                    isCheckingScreenOnOff: true
                  });
                }
              }
            ],
            { cancelable: false }
          );
        } else {
          this.dataSyncTrigger();
        }
      }
    });
  }

  render() {
    // =========================================================================
    // variables
    // =========================================================================
    const { currentAppState, store } = this.state;
    if (this.state.timeBombType === TYPE_TIME_BOMB) {
      return null;
    }
    return currentAppState === "active" ? (
      <Provider store={store}>
        <ContextProvider value={this.state}>
          {this.state.shouldShowPlayGround ? <PlayGround /> : <Root />}
        </ContextProvider>
      </Provider>
    ) : (
      <UnActiveScreen />
    );
  }
}
