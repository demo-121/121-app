import { StyleSheet } from "react-native";
import Theme from "../../theme";

export default StyleSheet.create({
  placeholder: {
    flexBasis: "auto",
    position: "relative",
    zIndex: 999
  },
  container: {
    position: "absolute",
    bottom: 0,
    left: 0,
    right: 0,
    backgroundColor: Theme.white,
    borderColor: Theme.lightGrey,
    borderWidth: 1
  }
});
