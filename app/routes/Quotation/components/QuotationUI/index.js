import {
  REDUCER_TYPE_CHECK as WEB_API_TYPES_CHECK,
  REDUCER_TYPES as WEB_API_REDUCER_TYPE
} from "eab-web-api";
import * as _ from "lodash";
import React, { Component } from "react";
import LinearGradient from "react-native-linear-gradient";
import PropTypes from "prop-types";
import Theme from "../../../../theme";
import BasicQuotation from "../../containers/BasicQuotation";
import ShieldQuotation from "../../containers/ShieldQuotation";
import * as QUOTATION_TYPES from "./constants";
import styles from "./styles";

const { QUOTATION } = WEB_API_REDUCER_TYPE;

/**
 * <Quotation />
 * */
export default class QuotationUI extends Component {
  constructor(props) {
    super(props);

    this.state = {
      prevQuotation: _.cloneDeep(this.props.quotation)
    };
  }

  // ===========================================================================
  // life circles
  // ===========================================================================
  static getDerivedStateFromProps(nextProps, prevState) {
    if (_.isEqual(nextProps.quotation, prevState.prevQuotation)) {
      return null;
    }
    return {
      prevQuotation: nextProps.quotation
    };
  }

  shouldComponentUpdate(nextProps) {
    return (
      !_.isEmpty(nextProps.inputConfigs) && !_.isEmpty(nextProps.planDetails)
    );
  }

  render() {
    /**
     * variables
     * */
    const { quotation, openSummaryProposal, navigation } = this.props;
    if (openSummaryProposal) {
      navigation.navigate({
        routeName: "ProposalUI"
      });
      return null;
    }
    /**
     * switchQuotation
     * return {element} quotation section
     * */
    const switchQuotation = () => {
      if (!_.has(quotation, "type")) {
        return null;
      }
      switch (quotation.quotType) {
        case QUOTATION_TYPES.SHIELD:
          return <ShieldQuotation />;
        case QUOTATION_TYPES.BASIC:
        default:
          return <BasicQuotation />;
      }
    };

    return (
      <LinearGradient {...Theme.LinearGradientProps} style={styles.container}>
        {switchQuotation()}
      </LinearGradient>
    );
  }
}

QuotationUI.propTypes = {
  quotation: WEB_API_TYPES_CHECK[QUOTATION].quotation.isRequired,
  planDetails: WEB_API_TYPES_CHECK[QUOTATION].planDetails.isRequired,
  inputConfigs: WEB_API_TYPES_CHECK[QUOTATION].inputConfigs.isRequired,
  openSummaryProposal: PropTypes.func.isRequired
};
