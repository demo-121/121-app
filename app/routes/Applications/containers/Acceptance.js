import { connect } from "react-redux";
import { REDUCER_TYPES } from "eab-web-api";
import { TEXT_STORE } from "../../../constants/REDUCER_TYPES";
import Acceptance from "../components/Acceptance";

/**
 * Acceptance
 * @description Acceptance in Recommendation page.
 * @requires Acceptance - Acceptance UI
 * */
const mapStateToProps = state => ({
  textStore: state[TEXT_STORE],
  value: state[REDUCER_TYPES.RECOMMENDATION].acceptance
});

const mapDispatchToProps = () => ({});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Acceptance);
