import { connect } from "react-redux";
import { REDUCER_TYPES, ACTION_TYPES } from "eab-web-api";
import NeedsDetailAppBarRight from "../components/NeedsDetailAppBarRight";

const { FNA } = REDUCER_TYPES;

/**
 * NeedsDetailAppBarRight
 * @requires NeedsDetailAppBarRight - NeedsDetailAppBarRight UI
 * */
const mapStateToProps = () => ({});

const mapDispatchToProps = dispatch => ({
  FNAReportInitialize: callback => {
    dispatch({
      type: ACTION_TYPES[FNA].GET_FNA_REPORT,
      callback
    });
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(NeedsDetailAppBarRight);
