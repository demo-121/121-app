import { connect } from "react-redux";
import { ACTION_TYPES, REDUCER_TYPES, NEEDS } from "eab-web-api";
import { TEXT_STORE } from "../../../constants/REDUCER_TYPES";
import AnalysisPlanningForSpecificGoals from "../components/AnalysisPlanningForSpecificGoals";

const { FNA, CLIENT } = REDUCER_TYPES;
const { PSGOALS } = NEEDS;

/**
 * AnalysisPlanningForSpecificGoals
 * @requires AnalysisPlanningForSpecificGoals - AnalysisPlanningForSpecificGoals UI
 * */
const mapStateToProps = state => ({
  textStore: state[TEXT_STORE],
  na: state[FNA].na,
  fe: state[FNA].fe,
  pda: state[FNA].pda,
  fna: state[FNA],
  profile: state[CLIENT].profile,
  dependantProfiles: state[CLIENT].dependantProfiles,
  isPsGoalsError: state[FNA].isNaError.psGoals
});

const mapDispatchToProps = dispatch => ({
  initNaAnalysis: () => {
    dispatch({
      type: ACTION_TYPES[FNA].INIT_NA_ANALYSIS,
      aspect: PSGOALS
    });
  },
  updateNA: ({
    naData,
    pdaData,
    profileData,
    dependantProfilesData,
    feData
  }) => {
    dispatch({
      type: ACTION_TYPES[FNA].UPDATE_NA,
      naData: {
        ...naData,
        completedStep: 2
      },
      pdaData,
      profileData,
      dependantProfilesData,
      feData
    });
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AnalysisPlanningForSpecificGoals);
