import { connect } from "react-redux";
import { ACTION_TYPES, REDUCER_TYPES } from "eab-web-api";
import { TEXT_STORE } from "../../../constants/REDUCER_TYPES";
import ApplicationDeleteButton from "../components/ApplicationDeleteButton";

/**
 * ApplicationDeleteButton
 * @description Button to delete Applications.
 * @requires ApplicationDeleteButton - ApplicationDeleteButton UI
 * */
const mapStateToProps = state => ({
  textStore: state[TEXT_STORE]
});

const mapDispatchToProps = dispatch => ({
  deleteApplications: callback => {
    dispatch({
      type:
        ACTION_TYPES[REDUCER_TYPES.PRE_APPLICATION].SAGA_DELETE_APPLICATIONS,
      callback
    });
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ApplicationDeleteButton);
