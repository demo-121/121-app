import PropTypes from "prop-types";
import React, { Component } from "react";
import { Text, View } from "react-native";
import * as _ from "lodash";
import {
  utilities,
  REDUCER_TYPE_CHECK as WEB_API_REDUCER_TYPE_CHECK,
  REDUCER_TYPES
} from "eab-web-api";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import EABTextField from "../../../../components/EABTextField";
import APP_REDUCER_TYPE_CHECK from "../../../../constants/REDUCER_TYPE_CHECK";
import { TEXT_STORE } from "../../../../constants/REDUCER_TYPES";
import Theme from "../../../../theme";
import EABi18n from "../../../../utilities/EABi18n";
import EABStepper from "../../../../components/EABStepper";
import { ContextConsumer } from "../../../../context";
import styles from "../Analysis/styles";
import PopoverTooltip from "../../../../components/PopoverTooltip/PopoverTooltipIOS";
import EASEAssets from "../../../../containers/EASEAssets";

const { FNA, CLIENT } = REDUCER_TYPES;
const { numberToCurrency, currencyToNumber } = utilities.numberFormatter;
const { calOtherAsset } = utilities.naAnalysis;

/**
 * AnalysisCriticalIllnessProtection
 * @description Analysis - Critical Illness Protection - Template
 * */
export default class AnalysisCriticalIllnessProtection extends Component {
  constructor(props) {
    super(props);
    this.state = {};

    this.onChange = this.onChange.bind(this);
  }

  componentDidMount() {
    this.props.initNaAnalysis();
  }

  componentDidUpdate(prevProps) {
    if (prevProps.selectedProfile !== this.props.selectedProfile) {
      this.props.initNaAnalysis();
    }
  }

  onChange({ value, key }) {
    const {
      selectedProfile,
      updateNA,
      na,
      pda,
      profile,
      dependantProfiles,
      fe
    } = this.props;

    if (selectedProfile === "owner" || selectedProfile === "spouse") {
      updateNA({
        naData: {
          ...na,
          ciProtection: {
            ...na.ciProtection,
            [selectedProfile]: {
              ...na.ciProtection[selectedProfile],
              [key]: value,
              init: false
            }
          }
        },
        pdaData: pda,
        profileData: profile,
        dependantProfilesData: dependantProfiles,
        feData: fe
      });
    } else {
      const index = na.ciProtection.dependants.findIndex(
        dependant => dependant.cid === selectedProfile
      );
      const dependants = _.cloneDeep(na.ciProtection.dependants);
      dependants[index] = {
        ...dependants[index],
        [key]: value,
        init: false
      };

      updateNA({
        naData: {
          ...na,
          ciProtection: {
            ...na.ciProtection,
            dependants
          }
        },
        pdaData: pda,
        profileData: profile,
        dependantProfilesData: dependantProfiles,
        feData: fe
      });
    }
  }

  checkDependant() {
    const { selectedProfile, data } = this.props;
    if (!data || _.isEmpty(data)) {
      return {};
    }
    if (selectedProfile !== "owner" && selectedProfile !== "spouse") {
      if (data.dependants && data.dependants.length) {
        return data.dependants.find(val => val.cid === selectedProfile);
      }
    } else {
      return data[selectedProfile];
    }
    return "";
  }

  shortfallUI() {
    const data = this.checkDependant();
    const { totShortfall } = data;

    return {
      totShortfallPath:
        totShortfall >= 0
          ? "na.needs.common.surplus"
          : "na.needs.common.shortfall",
      totShortfallStyle:
        totShortfall >= 0 ? styles.surplusContent : styles.shortfallContent,
      totShortfallInfo:
        totShortfall >= 0
          ? "na.needs.common.surplusContent"
          : "na.needs.common.shortfallContent"
    };
  }

  render() {
    const {
      textStore,
      selectedProfile,
      profile,
      na,
      pda,
      fe,
      updateNA,
      isCiProtectionError,
      dependantProfiles
      // pmtOnChange,
      // requireYrIncomeOnChange,
      // mtCostOnChange
    } = this.props;
    const data = this.checkDependant();
    const {
      totShortfallPath,
      totShortfallStyle,
      totShortfallInfo
    } = this.shortfallUI();
    const pmtCurrency = numberToCurrency({
      value: data.pmt || 0,
      decimal: 0
    });
    const mtCostCurrency = numberToCurrency({
      value: data.mtCost || 0,
      decimal: 0
    });

    const selectedAssets = na.ciProtection[selectedProfile].assets;

    const { onChange } = this;

    return (
      <ContextConsumer>
        {({ language }) => (
          <View>
            <KeyboardAwareScrollView
              extraScrollHeight={100}
              style={{ marginBottom: Theme.alignmentXXXXL }}
            >
              <View key={123} style={styles.analysisProductContent}>
                <Text style={styles.title}>
                  {EABi18n({
                    language,
                    textStore,
                    path: "na.needs.ciProtection"
                  })}
                </Text>
                <Text style={styles.analysisProductPointTitle}>
                  {`1. ${EABi18n({
                    language,
                    textStore,
                    path: "na.needs.ciProtection.pmt"
                  })}`}
                  <Text style={styles.requiredHint}> *</Text>
                </Text>
                <EABTextField
                  testID="NeedsAnalysis__CIP__txtPeronsalAccidentAmount"
                  style={styles.analysisTextInputShort}
                  prefix="$"
                  value={pmtCurrency}
                  isError={isCiProtectionError[selectedProfile].pmt.hasError}
                  hintText={EABi18n({
                    path: isCiProtectionError[selectedProfile].pmt.messagePath,
                    language,
                    textStore
                  })}
                  onChange={value =>
                    updateNA({
                      naData: {
                        ...na,
                        ciProtection: {
                          ...na.ciProtection,
                          [selectedProfile]: {
                            ...na.ciProtection[selectedProfile],
                            pmt: value !== "0" ? currencyToNumber(value) : "",
                            init: false
                          }
                        },
                        completedStep: 1
                      },
                      pdaData: pda,
                      profileData: profile,
                      feData: fe,
                      dependantProfilesData: dependantProfiles
                    })
                  }
                  maxLength={11 + pmtCurrency.replace(/[^,]/g, "").length}
                />
                <Text style={styles.analysisProductPointTitle}>
                  {EABi18n({
                    language,
                    textStore,
                    path: "na.needs.ciProtection.annualLivingExp"
                  })}
                </Text>
                <Text style={styles.analysisProductPointContent}>
                  {`$${numberToCurrency({
                    value: data.annualLivingExp,
                    decimal: 0,
                    maxValue: 999999999999999
                  })}`}
                </Text>
                <View style={styles.analysisSectionBorderLine} />

                <Text style={styles.analysisProductPointTitle}>
                  {`2. ${EABi18n({
                    language,
                    textStore,
                    path: "na.needs.ciProtection.requireYrIncome"
                  })}`}
                  <Text style={styles.requiredHint}> *</Text>
                </Text>
                {isCiProtectionError[selectedProfile].requireYrIncome
                  .hasError ? (
                  <View>
                    <Text style={Theme.fieldTextOrHelperTextNegative}>
                      {EABi18n({
                        textStore,
                        language,
                        path:
                          isCiProtectionError[selectedProfile].requireYrIncome
                            .messagePath
                      })}
                    </Text>
                  </View>
                ) : null}
                <EABStepper
                  testID="NeedsAnalysis__CIP"
                  value={
                    data.requireYrIncome === ""
                      ? data.requireYrIncome
                      : Number(data.requireYrIncome)
                  }
                  step={1}
                  min={0}
                  max={99}
                  onChange={value =>
                    updateNA({
                      naData: {
                        ...na,
                        ciProtection: {
                          ...na.ciProtection,
                          [selectedProfile]: {
                            ...na.ciProtection[selectedProfile],
                            requireYrIncome: value.toString(),
                            init: false
                          }
                        },
                        completedStep: 1
                      },
                      pdaData: pda,
                      profileData: profile,
                      feData: fe,
                      dependantProfilesData: dependantProfiles
                    })
                  }
                />
                <View style={styles.analysisSectionBorderLine} />

                <Text style={styles.analysisProductPointTitle}>
                  {`3. ${EABi18n({
                    language,
                    textStore,
                    path: "na.needs.ciProtection.iarRate2"
                  })} `}
                </Text>
                <Text style={styles.analysisProductPointContent}>
                  {`${data.iarRate2 || na.iarRate || 0}%`}
                </Text>
                <View style={styles.analysisSectionBorderLine} />

                <Text style={styles.analysisProductPointTitle}>
                  {`4. ${EABi18n({
                    language,
                    textStore,
                    path: "na.needs.ciProtection.lumpSum"
                  })} `}
                </Text>
                <Text style={styles.analysisProductPointContent}>
                  {`$${numberToCurrency({
                    value: data.lumpSum,
                    decimal: 2,
                    maxValue: 99999999999999999999
                  })}`}
                </Text>
                <View style={styles.analysisSectionBorderLine} />

                <Text style={styles.analysisProductPointTitle}>
                  {`5. ${EABi18n({
                    language,
                    textStore,
                    path: "na.needs.ciProtection.mtCost"
                  })} `}
                </Text>
                <EABTextField
                  testID="NeedsAnalysis__CIP__txtCostOfMedicalTreatment"
                  style={styles.analysisTextInputShort}
                  prefix="$"
                  value={mtCostCurrency}
                  isError={isCiProtectionError[selectedProfile].mtCost.hasError}
                  hintText={EABi18n({
                    path:
                      isCiProtectionError[selectedProfile].mtCost.messagePath,
                    language,
                    textStore
                  })}
                  onChange={value =>
                    updateNA({
                      naData: {
                        ...na,
                        ciProtection: {
                          ...na.ciProtection,
                          [selectedProfile]: {
                            ...na.ciProtection[selectedProfile],
                            mtCost:
                              value !== "0" ? currencyToNumber(value) : "",
                            init: false
                          }
                        },
                        completedStep: 1
                      },
                      pdaData: pda,
                      profileData: profile,
                      feData: fe,
                      dependantProfilesData: dependantProfiles
                    })
                  }
                  maxLength={11 + mtCostCurrency.replace(/[^,]/g, "").length}
                />
                <View style={styles.analysisSectionBorderLine} />

                <Text style={styles.analysisProductPointTitle}>
                  {`6. ${EABi18n({
                    language,
                    textStore,
                    path: "na.needs.ciProtection.totCoverage"
                  })} `}
                </Text>
                <Text style={styles.analysisProductPointContent}>
                  {`$${numberToCurrency({
                    value: Number(data.totCoverage),
                    decimal: 2,
                    maxValue: 99999999999999999999
                  })}`}
                </Text>
                <View style={styles.analysisSectionBorderLine} />

                <Text style={styles.analysisProductPointTitle}>
                  {`7. ${EABi18n({
                    language,
                    textStore,
                    path: "na.needs.ciProtection.ciProt"
                  })} `}
                </Text>
                <Text style={styles.analysisProductPointContent}>
                  {`$${numberToCurrency({
                    value: data.ciProt || 0,
                    decimal: 0,
                    maxValue: 99999999999999999999
                  })}`}
                </Text>
                <View style={styles.analysisSectionBorderLine} />
                <View style={styles.titleRow}>
                  <Text style={styles.analysisProductPointTitle}>
                    {`8. ${EABi18n({
                      language,
                      textStore,
                      path: "na.needs.ciProtection.assets"
                    })}`}
                  </Text>
                  <PopoverTooltip
                    popoverOptions={{ preferredContentSize: [365, 65] }}
                    text={EABi18n({
                      textStore,
                      language,
                      path: "na.needs.assets.toolTip"
                    })}
                  />
                </View>
                <EASEAssets
                  testID="NeedsAnalysis__CIP"
                  style={styles.analysisAssetsWrapper}
                  selectedProduct="ciProtection"
                  selectedProfile={selectedProfile}
                  selectedAssets={selectedAssets}
                  addAssetsOnPress={() => {
                    let cloneAssets = _.cloneDeep(selectedAssets);
                    if (!cloneAssets) {
                      cloneAssets = [];
                    }
                    cloneAssets.push({
                      calAsset: 0,
                      key: "all",
                      usedAsset: 0
                    });
                    onChange({ value: cloneAssets, key: "assets" });
                  }}
                  removeAssetsOnPress={i => {
                    const cloneAssets = _.cloneDeep(selectedAssets);
                    cloneAssets.forEach((assetsData, index) => {
                      if (i === index) {
                        cloneAssets.splice(index, 1);
                        onChange({ value: cloneAssets, key: "assets" });
                      }
                    });
                  }}
                  assetTextOnChange={(value, index) => {
                    const cloneAssets = _.cloneDeep(selectedAssets);
                    cloneAssets[index] = {
                      key: selectedAssets[index].key,
                      usedAsset: currencyToNumber(value)
                    };
                    onChange({ value: cloneAssets, key: "assets" });
                  }}
                  assetSelectorOnChange={(itemKey, item) => {
                    if (
                      !selectedAssets.find(select => select.key === item.value)
                    ) {
                      let cloneAssets = _.cloneDeep(selectedAssets);
                      if (selectedAssets[item.prevData.index]) {
                        cloneAssets[item.prevData.index] = {
                          calAsset: 0,
                          key: item.value,
                          usedAsset: 0
                        };
                      } else {
                        cloneAssets = selectedAssets.concat({
                          calAsset: 0,
                          key: item.value,
                          usedAsset: 0
                        });
                      }
                      onChange({ value: cloneAssets, key: "assets" });
                    }
                  }}
                  isProtectionError={isCiProtectionError[selectedProfile]}
                  calOtherAsset={({ key, selectedProduct }) =>
                    calOtherAsset({
                      key,
                      selectedProduct,
                      selectedProfile,
                      na
                    })
                  }
                />
              </View>
            </KeyboardAwareScrollView>
            <View style={styles.shortfallView}>
              <View style={styles.shortfallWrapper}>
                <PopoverTooltip
                  popoverOptions={{ preferredContentSize: [350, 100] }}
                  text={EABi18n({
                    textStore,
                    language,
                    path: totShortfallInfo
                  })}
                />
                <View style={styles.subShortfallView}>
                  <Text style={Theme.fieldTextOrHelperTextNormal}>
                    {`${EABi18n({
                      language,
                      textStore,
                      path: totShortfallPath
                    })}: `}
                    <Text style={totShortfallStyle}>
                      {`$${numberToCurrency({
                        value: Math.abs(data.totShortfall || 0),
                        decimal: 2,
                        maxValue: 999999999999999
                      })}`}
                    </Text>
                  </Text>
                </View>
              </View>
            </View>
          </View>
        )}
      </ContextConsumer>
    );
  }
}

AnalysisCriticalIllnessProtection.propTypes = {
  selectedProfile: PropTypes.string.isRequired,
  textStore: APP_REDUCER_TYPE_CHECK[TEXT_STORE].isRequired,
  na: WEB_API_REDUCER_TYPE_CHECK[FNA].na.isRequired,
  data: WEB_API_REDUCER_TYPE_CHECK[FNA].analysisData.isRequired,
  initNaAnalysis: PropTypes.func.isRequired,
  updateNA: PropTypes.func.isRequired,
  pda: WEB_API_REDUCER_TYPE_CHECK[FNA].pda.isRequired,
  fe: WEB_API_REDUCER_TYPE_CHECK[FNA].fe.isRequired,
  profile: WEB_API_REDUCER_TYPE_CHECK[CLIENT].profile.isRequired,
  dependantProfiles:
    WEB_API_REDUCER_TYPE_CHECK[CLIENT].dependantProfiles.isRequired,
  isCiProtectionError: PropTypes.shape({}).isRequired
};
