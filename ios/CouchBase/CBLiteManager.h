#if __has_include(<React/RCTBridgeModule.h>)
#import <React/RCTBridgeModule.h>
#else
#import "RCTBridgeModule.h"
#endif
#import <CouchbaseLite/CouchbaseLite.h>
#import <CouchbaseLiteListener/CBLListener.h>

#import <JavaScriptCore/JSContext.h>
#import <JavaScriptCore/JSValue.h>
#import <JavaScriptCore/JSManagedValue.h>
#import <JavaScriptCore/JSVirtualMachine.h>
#import <JavaScriptCore/JSExport.h>

@interface CBLiteManager : NSObject <RCTBridgeModule>
{
  //    CBLListener *listener;
  NSString *encryptionKey;
  CBLManager *manager;
  CBLManager *masterManager;
  CBLDatabase* database;
  //  CBLReplication *push;
  //  CBLReplication *pull;
}

@property (nonatomic, strong) JSContext *context;

@end
