import alignment from "./alignments";
import Colors from "./colors";
import Fonts from "./fonts";
import Radius from "./radius";
import UI from "./ui";

export default {
  ...alignment,
  ...Colors,
  ...Fonts,
  ...Radius,
  ...UI
};
