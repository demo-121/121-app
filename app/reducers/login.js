import { combineReducers } from "redux";
import ACTION_TYPES from "../constants/ACTION_TYPES";
import { LOGIN, PAYMENT } from "../constants/REDUCER_TYPES";
import {
  APP_ACTIVATION,
  LOCAL_AUTH,
  ONLINE_AUTH
} from "../constants/LOGIN_FLOW_TYPES";

/**
 * login
 * @default false
 * */
const isLogin = (state = false, { type }) => {
  switch (type) {
    case ACTION_TYPES[LOGIN].LOGIN_SUCCESS:
      state = true;
      return state;
    case ACTION_TYPES[LOGIN].LOGOUT:
    case ACTION_TYPES[LOGIN].LOGIN_FAIL:
      state = false;
      return state;
    default:
      return state;
  }
};
const isHandlingListener = (state = false, action) => {
  switch (action.type) {
    case ACTION_TYPES[LOGIN].HANDLE_LISTENER:
      state = action.isCheckingScreenOnOff;
      return state;
    default:
      return state;
  }
};
const isAppActivationDone = (state = false, { type }) => {
  switch (type) {
    case ACTION_TYPES[LOGIN].APP_ACTIVATION_DONE:
      state = true;
      return state;
    default:
      return state;
  }
};
const isLogout = (state = true, { type }) => {
  switch (type) {
    case ACTION_TYPES[LOGIN].LOGIN_SUCCESS:
      state = false;
      return state;
    case ACTION_TYPES[LOGIN].LOGOUT:
      state = true;
      return state;
    default:
      return state;
  }
};

const disableAppActivationPage = (state = false, action) => {
  switch (action.type) {
    case ACTION_TYPES[LOGIN].DISABLE_APP_ACTIVATION_PAGE:
      state = true;
      return state;
    default:
      return state;
  }
};

const isExceedFailLoginAttemps = (state = false, action) => {
  switch (action.type) {
    case ACTION_TYPES[LOGIN].EXCEED_FAIL_LOGIN_ATTEMPS:
      state = true;
      return state;
    case ACTION_TYPES[LOGIN].LOGIN_SUCCESS:
    case ACTION_TYPES[LOGIN].RESET_FAIL_LOGIN_ATTEMPS:
      state = false;
      return state;
    default:
      return state;
  }
};
const authToken = (state = null, action) => {
  switch (action.type) {
    case ACTION_TYPES[LOGIN].VERIFY_DEVICE_SUCCESS:
      state = action.authToken;
      return state;
    case ACTION_TYPES[LOGIN].AUTH_TOKEN:
      state = action.authToken;
      return state;
    default:
      return state;
  }
};
const idToken = (state = null, action) => {
  switch (action.type) {
    case ACTION_TYPES[LOGIN].ID_TOKEN:
      state = action.idToken;
      return state;
    // case ACTION_TYPES[LOGIN].VERIFY_DEVICE_SUCCESS:
    //   state = action.idToken;
    //   return state;
    default:
      return state;
  }
};

const sessionCookie = (state = null, action) => {
  switch (action.type) {
    case ACTION_TYPES[LOGIN].FETECH_SESSION_COOKIE:
      state = action.sessionCookie;
      return state;
    default:
      return state;
  }
};
const sessionCookieSyncGatewaySession = (state = null, action) => {
  switch (action.type) {
    case ACTION_TYPES[LOGIN].FETECH_SESSION_COOKIE_SYNC_GATEWAY_SESSION:
      state = action.sessionCookieSyncGatewaySession;
      return state;
    default:
      return state;
  }
};
const sessionCookieExpiryDate = (state = null, action) => {
  switch (action.type) {
    case ACTION_TYPES[LOGIN].FETECH_SESSION_COOKIE_EXPIRY_DATE:
      state = action.sessionCookieExpiryDate;
      return state;
    default:
      return state;
  }
};

const sessionCookiePath = (state = null, action) => {
  switch (action.type) {
    case ACTION_TYPES[LOGIN].FETECH_SESSION_COOKIE_PATH:
      state = action.sessionCookiePath;
      return state;
    default:
      return state;
  }
};

const syncDocumentIds = (state = null, action) => {
  switch (action.type) {
    case ACTION_TYPES[LOGIN].SYNC_DOCUMENT_IDS:
      state = action.syncDocumentId;
      return state;
    default:
      return state;
  }
};
const publicKey = (state = null, action) => {
  switch (action.type) {
    case ACTION_TYPES[LOGIN].PUBLIC_KEY:
      state = action.publicKey;
      return state;
    default:
      return state;
  }
};
const userID = (state = "", action) => {
  switch (action.type) {
    case ACTION_TYPES[LOGIN].SET_USER_ID:
    case ACTION_TYPES[LOGIN].VERIFY_DEVICE_SUCCESS:
    case ACTION_TYPES[LOGIN].LOGIN_SUCCESS:
      state = action.userID;
      if (!state) {
        return null;
      }
      return state;
    default:
      return state;
  }
};
const agentCode = (state = null, action) => {
  switch (action.type) {
    case ACTION_TYPES[LOGIN].AGENTCODE:
      state = action.agent;
      if (!state) {
        return null;
      }
      return state;
    default:
      return state;
  }
};

const agentData = (state = null, action) => {
  switch (action.type) {
    case ACTION_TYPES[LOGIN].AGENTDATA:
      state = action.agentData;
      if (!state) {
        return null;
      }
      return state;
    default:
      return state;
  }
};

const profileId = (state = null, action) => {
  switch (action.type) {
    case ACTION_TYPES[LOGIN].PROFILEID:
      state = action.profile;
      if (!state) {
        return null;
      }
      return state;
    default:
      return state;
  }
};
const lastLoginTime = (state = null, action) => {
  switch (action.type) {
    case ACTION_TYPES[LOGIN].VERIFY_DEVICE_SUCCESS:
    case ACTION_TYPES[LOGIN].LOGIN_SUCCESS:
      state = action.lastLoginTime;
      if (!state) {
        return null;
      }
      return state;
    default:
      return state;
  }
};

const timebombExpiryDate = (state = null, action) => {
  switch (action.type) {
    case ACTION_TYPES[LOGIN].VERIFY_DEVICE_SUCCESS:
      state = action.timebombExpiryDate;
      if (!state) {
        state = null;
        return state;
      }
      return state;
    default:
      return state;
  }
};

const installationID = (state = null, action) => {
  switch (action.type) {
    case ACTION_TYPES[LOGIN].VERIFY_DEVICE_SUCCESS:
      state = action.installationID;
      if (!state) {
        state = null;
        return state;
      }
      return state;
    default:
      return state;
  }
};

const showNewAppPasswordScene = (state = false, { type }) => {
  switch (type) {
    case ACTION_TYPES[LOGIN].VERIFY_DEVICE_SUCCESS:
      state = true;
      return state;
    case ACTION_TYPES[LOGIN].LOGIN_SUCCESS:
    case ACTION_TYPES[LOGIN].LOGOUT:
      state = false;
      return state;
    case ACTION_TYPES[LOGIN].BACK_ONLINE_AUTH:
      state = false;
      return state;
    default:
      return state;
  }
};

const showResetAppPasswordScene = (state = false, { type }) => {
  switch (type) {
    case ACTION_TYPES[LOGIN].EXCEED_FAIL_LOGIN_ATTEMPS:
      state = true;
      return state;
    case ACTION_TYPES[LOGIN].RESET_APP_PASSWORD_MANUALLY_SUCCESS:
      state = false;
      return state;
    // case ACTION_TYPES[LOGIN].RESET_APP_PASSWORD_MANUALLY_FAIL:
    //   state = true;
    //   return state;
    case ACTION_TYPES[LOGIN].LOGOUT:
      state = false;
      return state;
    default:
      return state;
  }
};
const showResetAppPasswordManuallyScene = (state = false, { type }) => {
  switch (type) {
    case ACTION_TYPES[LOGIN].RESET_APP_PASSWORD_MANUALLY:
      state = true;
      return state;
    case ACTION_TYPES[LOGIN].RESET_APP_PASSWORD_MANUALLY_SUCCESS:
      state = false;
      return state;
    case ACTION_TYPES[LOGIN].CANCEL_RESET_APP_PASSWORD_MANUALLY:
      state = false;
      return state;
    case ACTION_TYPES[LOGIN].LOGOUT:
      state = false;
      return state;
    default:
      return state;
  }
};
const accessSQLiteDB = (state = false, { type }) => {
  switch (type) {
    case ACTION_TYPES[LOGIN].ACCESSSQLITESUCCESS:
      state = true;
      return state;
    case ACTION_TYPES[LOGIN].ACCESSSQLITEFAIL:
      state = false;
      return state;
    default:
      return state;
  }
};

const appPassword = (state = null, action) => {
  switch (action.type) {
    case ACTION_TYPES[LOGIN].NEW_APP_PASSWORD:
      state = action.appPassword;
      return state;
    default:
      return state;
  }
};

const isAppPasswordExpired = (state = false, action) => {
  switch (action.type) {
    case ACTION_TYPES[LOGIN].APP_PASSWORD_EXPIRY_DATE:
      state = true;
      return state;
    default:
      return state;
  }
};

const isTokenValid = (state = false, action) => {
  switch (action.type) {
    case ACTION_TYPES[LOGIN].IS_TOKEN_VALID:
      state = action.isValid;
      return state;
    case ACTION_TYPES[LOGIN].LOGOUT:
      return false;
    case ACTION_TYPES[PAYMENT].ONLINE_AUTH_SUCCESS:
      return true;
    default:
      return state;
  }
};

const changeToOnlineAuth = (state = false, action) => {
  switch (action.type) {
    case ACTION_TYPES[LOGIN].CHANGE_TO_ONLINE_AUTH:
      state = action.changeToOnlineAuth;
      return state;
    default:
      return state;
  }
};

const isImportRequired = (state = true, action) => {
  switch (action.type) {
    case ACTION_TYPES[LOGIN].IS_IMPORT_REQUIRED:
      state = action.isImportRequired;
      return state;
    default:
      return state;
  }
};
const isAppLocked = (state = false, action) => {
  switch (action.type) {
    case ACTION_TYPES[LOGIN].IS_APP_LOCKED:
      state = action.isLocked;
      return state;
    default:
      return state;
  }
};
const isTimezoneValid = (state = true, action) => {
  switch (action.type) {
    case ACTION_TYPES[LOGIN].IS_TIMEZONE_VALID:
      state = action.isValid;
      return state;
    default:
      return state;
  }
};
const triggerRunView = (state = false, action) => {
  switch (action.type) {
    case ACTION_TYPES[LOGIN].ENABLE_RUN_VIEW:
      state = true;
      return state;
    case ACTION_TYPES[LOGIN].DISABLE_RUN_VIEW:
      state = false;
      return state;
    default:
      return state;
  }
};
const triggerInflightAlert = (state = false, action) => {
  switch (action.type) {
    case ACTION_TYPES[LOGIN].ENABLE_INFLIGHT_ALERT:
      state = true;
      return state;
    case ACTION_TYPES[LOGIN].DISABLE_INFLIGHT_ALERT:
      state = false;
      return state;
    default:
      return state;
  }
};

const loginFlow = (state = null, action) => {
  switch (action.type) {
    case ACTION_TYPES[LOGIN].USE_LOCAL_AUTH:
      state = LOCAL_AUTH;
      return state;
    case ACTION_TYPES[LOGIN].USE_ACTIVATION:
      state = APP_ACTIVATION;
      return state;
    case ACTION_TYPES[LOGIN].USE_ONLINE_AUTH:
      state = ONLINE_AUTH;
      return state;
    default:
      return state;
  }
};
const lastDataSyncTime = (state = null, action) => {
  switch (action.type) {
    case ACTION_TYPES[LOGIN].LAST_DATA_SYNC_TIME:
      state = action.lastDataSyncTime;
      return state;
    default:
      return state;
  }
};
const loginFlowCheck = (state = "", action) => {
  switch (action.type) {
    case ACTION_TYPES[LOGIN].SET_LOGIN_FLOW:
      return action.loginFlow;
    default:
      return state;
  }
};

const environment = (state = "", action) => {
  switch (action.type) {
    case ACTION_TYPES[LOGIN].ENVIRONMENT:
      state = action.environment;
      return state;
    default:
      return state;
  }
};

const webServiceUrl = (state = "", action) => {
  switch (action.type) {
    case ACTION_TYPES[LOGIN].WEB_SERVICE_URL:
      state = action.webServiceUrl;
      return state;
    default:
      return state;
  }
};

const documentSeq = (state = NaN, { type, documentSeqData }) => {
  switch (type) {
    case ACTION_TYPES[LOGIN].UPDATE_DOCUMENT_SEQ:
      return documentSeqData;
    default:
      return state;
  }
};

export default combineReducers({
  isLogin,
  isHandlingListener,
  isLogout,
  isTimezoneValid,
  isAppLocked,
  lastDataSyncTime,
  changeToOnlineAuth,
  isAppActivationDone,
  isExceedFailLoginAttemps,
  authToken,
  idToken,
  sessionCookie,
  sessionCookieSyncGatewaySession,
  sessionCookieExpiryDate,
  sessionCookiePath,
  syncDocumentIds,
  publicKey,
  userID,
  triggerRunView,
  triggerInflightAlert,
  agentCode,
  agentData,
  lastLoginTime,
  timebombExpiryDate,
  installationID,
  showNewAppPasswordScene,
  disableAppActivationPage,
  showResetAppPasswordManuallyScene,
  showResetAppPasswordScene,
  accessSQLiteDB,
  appPassword,
  isAppPasswordExpired,
  isTokenValid,
  loginFlow,
  loginFlowCheck,
  environment,
  webServiceUrl,
  profileId,
  documentSeq,
  isImportRequired
});
