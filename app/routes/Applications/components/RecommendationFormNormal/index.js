import React, { PureComponent } from "react";
import { View, Text } from "react-native";
import PropTypes from "prop-types";
import * as _ from "lodash";
import {
  REDUCER_TYPE_CHECK as WEB_API_REDUCER_TYPE_CHECK,
  REDUCER_TYPES,
  utilities
} from "eab-web-api";

import APP_REDUCER_TYPE_CHECK from "../../../../constants/REDUCER_TYPE_CHECK";
import { TEXT_STORE } from "../../../../constants/REDUCER_TYPES";
import { RECTANGLE_BUTTON_2 } from "../../../../components/EABButton/constants";
import EABButton from "../../../../components/EABButton";
import EABTextField from "../../../../components/EABTextField";
import EABTextBox from "../../../../components/EABTextBox";
import EABTextSelection from "../../../../components/EABTextSelection";
import EABFormSectionHeader from "../../../../components/EABFormSectionHeader";
import EABTable from "../../../../components/EABTable";
import EABTableHeader from "../../../../components/EABTableHeader";
import EABTableSectionHeader from "../../../../components/EABTableSectionHeader";
import EABTableSection from "../../../../components/EABTableSection";
import EABTableFooter from "../../../../components/EABTableFooter";
import TranslatedText from "../../../../containers/TranslatedText";
import EABi18n from "../../../../utilities/EABi18n";
import getEABTextByLangType from "../../../../utilities/getEABTextByLangType";

import styles from "./styles";
import { ContextConsumer } from "../../../../context";
import Theme from "../../../../theme";

const { getCurrencySign } = utilities.common;
const { numberToCurrency, currencyToNumber } = utilities.numberFormatter;

const { RECOMMENDATION, OPTIONS_MAP } = REDUCER_TYPES;

/**
 * RecommendationFormNormal
 * @description Form for Normal Product in Recommendation page.
 * @param {object} textStore - reducer textStore state
 * @param {string} quotId - Quotation ID for the Recommendation
 * @param {object} value - data for Recommendation page
 * @param {object} error - error after validation for Recommendation page
 * @param {boolean} isChosen - indicate the quotation is in chosen list or not
 * @param {boolean} disabled - change all UI to non-editable
 * @param {func} showRecommendationDetails - action to do after View Details is clicked
 * @param {func} updateRecommendationData - update Recommendation state with inputted data
 * */

class RecommendationFormNormal extends PureComponent {
  constructor(props) {
    super(props);

    this.updateStateValue = this.updateStateValue.bind(this);
    this.renderWarningNote = this.renderWarningNote.bind(this);
    this.renderPolicyTable = this.renderPolicyTable.bind(this);
    this.renderRequiredComponent = this.renderRequiredComponent.bind(this);
    this.renderOptionalComponent = this.renderOptionalComponent.bind(this);
    this.prepareRidersName = this.prepareRidersName.bind(this);
    this.warningNoteStatementsText = [
      {
        key: "0",
        title: "recommendation.recommendation.title.warningNoteStatement1"
      },
      {
        key: "a",
        title: "recommendation.recommendation.title.warningNoteStatement2"
      },
      {
        key: "b",
        title: "recommendation.recommendation.title.warningNoteStatement3"
      },
      {
        key: "c",
        title: "recommendation.recommendation.title.warningNoteStatement4"
      },
      {
        key: "d",
        title: "recommendation.recommendation.title.warningNoteStatement5"
      },
      {
        key: "e",
        title: "recommendation.recommendation.title.warningNoteStatement6"
      },
      {
        key: "f",
        title: "recommendation.recommendation.title.warningNoteStatement7"
      }
    ];

    this.state = {};
  }

  get sign() {
    const { optionsMap } = this.props;

    return getCurrencySign({
      compCode: "01",
      ccy: "SGD",
      optionsMap
    });
  }

  updateStateValue({ path, newPathValue }) {
    const { value, updateRecommendationData } = this.props;
    const newValue = _.cloneDeep(value);
    _.set(newValue, path, newPathValue);

    // reset hidden field value
    if (path === "rop.choiceQ1" && newPathValue === "N") {
      _.set(newValue, "rop.choiceQ1Sub1", "");
      _.set(newValue, "rop.choiceQ1Sub2", "");
      _.set(newValue, "rop.choiceQ1Sub3", "");
      _.set(newValue, "rop.replaceLife", 0);
      _.set(newValue, "rop.replaceTpd", 0);
      _.set(newValue, "rop.replaceCi", 0);
      _.set(newValue, "rop.replacePaAdb", 0);
      _.set(newValue, "rop.replaceTotalPrem", 0);
    }

    if (path === "ropQ1" && newPathValue === "N") {
      _.set(newValue, "ropQ1Sub1", "");
      _.set(newValue, "ropQ1Sub2", "");
      _.set(newValue, "ropQ1Sub3", "");
      _.set(newValue, "ropReplaceLife", 0);
      _.set(newValue, "ropReplaceTpd", 0);
      _.set(newValue, "ropReplaceCi", 0);
      _.set(newValue, "ropReplacePaAdb", 0);
      _.set(newValue, "ropReplaceTotalPrem", 0);
    }

    updateRecommendationData(newValue);
  }

  prepareRidersName(language) {
    const { value } = this.props;
    const ridersName = _.get(value, "extra.ridersName", []);
    let displayRidersName = "";
    _.forEach(ridersName, (rider, index) => {
      displayRidersName +=
        getEABTextByLangType({
          data: rider,
          language
        }) + (index < ridersName.length - 1 ? ", " : "");
    });
    return displayRidersName;
  }

  renderWarningNote(language) {
    return (
      <View>
        <TranslatedText
          style={styles.warningStatementHead}
          language={language}
          path="recommendation.recommendation.title.warningNote"
        />
        {this.warningNoteStatementsText.map(statement => (
          <View
            key={`warningNoteStatement-${statement.key}`}
            style={styles.warningStatementView}
          >
            {statement.key === "0" ? null : (
              <Text style={styles.warningStatementPoint}>{statement.key})</Text>
            )}
            <TranslatedText
              style={styles.warningStatement}
              language={language}
              path={statement.title}
            />
          </View>
        ))}
      </View>
    );
  }

  renderPolicyTable(language) {
    const { sign } = this;
    const { value, error, disabled } = this.props;
    return (
      <EABTable
        tableConfig={[
          {
            key: `policyTable-life`,
            style: styles.tableColumnStart
          },
          {
            key: `policyTable-tpd`,
            style: styles.tableColumnStart
          },
          {
            key: `policyTable-ci`,
            style: styles.tableColumnStart
          },
          {
            key: `policyTable-paAdb`,
            style: styles.tableColumnStart
          },
          {
            key: `policyTable-totalAnnualPremium`,
            style: styles.tableColumnStart
          }
        ]}
        style={styles.table}
      >
        <EABTableHeader key="policyTable-header">
          <View style={styles.tableHeader}>
            <TranslatedText
              style={styles.tableHeaderTitle}
              language={language}
              path="recommendation.recommendation.policyTable.title.life"
            />
          </View>
          <View style={styles.tableHeader}>
            <TranslatedText
              style={styles.tableHeaderTitle}
              language={language}
              path="recommendation.recommendation.policyTable.title.tpd"
            />
          </View>
          <View style={styles.tableHeader}>
            <TranslatedText
              style={styles.tableHeaderTitle}
              language={language}
              path="recommendation.recommendation.policyTable.title.ci"
            />
          </View>
          <View style={styles.tableHeader}>
            <TranslatedText
              style={styles.tableHeaderTitle}
              language={language}
              path="recommendation.recommendation.policyTable.title.paAdb"
            />
          </View>
          <View style={styles.tableHeader}>
            <TranslatedText
              style={styles.tableHeaderTitle}
              language={language}
              path="recommendation.recommendation.policyTable.title.totalAnnualPremiums"
            />
          </View>
        </EABTableHeader>
        <EABTableSectionHeader key="policyTable-exist-header">
          <View>
            <TranslatedText
              style={styles.tableSectionHeaderTitle}
              language={language}
              path="recommendation.recommendation.policyTable.section.title.existPolicy"
              funcOnText={_.toUpper}
            />
          </View>
        </EABTableSectionHeader>
        <EABTableSection key="policyTable-exist-data">
          <View style={styles.tableSection}>
            <Text style={styles.tableSectionTitle}>
              {value.rop.existLife !== 0
                ? `${sign}${numberToCurrency({
                    value: _.get(value, "rop.existLife"),
                    decimal: 0,
                    maxValue: 999999999999999
                  })}`
                : "-"}
            </Text>
          </View>
          <View style={styles.tableSection}>
            <Text style={styles.tableSectionTitle}>
              {value.rop.existTpd !== 0
                ? `${sign}${numberToCurrency({
                    value: _.get(value, "rop.existTpd"),
                    decimal: 0
                  })}`
                : "-"}
            </Text>
          </View>
          <View style={styles.tableSection}>
            <Text style={styles.tableSectionTitle}>
              {value.rop.existCi !== 0
                ? `${sign}${numberToCurrency({
                    value: _.get(value, "rop.existCi"),
                    decimal: 0,
                    maxValue: 999999999999999
                  })}`
                : "-"}
            </Text>
          </View>
          <View style={styles.tableSection}>
            <Text style={styles.tableSectionTitle}>
              {value.rop.existPaAdb !== 0
                ? `${sign}${numberToCurrency({
                    value: _.get(value, "rop.existPaAdb"),
                    decimal: 0,
                    maxValue: 999999999999999
                  })}`
                : "-"}
            </Text>
          </View>
          <View style={styles.tableSection}>
            <Text style={styles.tableSectionTitle}>
              {value.rop.existTotalPrem !== 0
                ? `${sign}${numberToCurrency({
                    value: _.get(value, "rop.existTotalPrem"),
                    decimal: 0,
                    maxValue: 999999999999999
                  })}`
                : "-"}
            </Text>
          </View>
        </EABTableSection>
        <EABTableSectionHeader key="policyTable-replacePolicy">
          <View>
            <TranslatedText
              style={styles.tableSectionHeaderTitle}
              language={language}
              path="recommendation.recommendation.policyTable.section.title.replacePolicy"
              funcOnText={_.toUpper}
            />
          </View>
        </EABTableSectionHeader>
        <EABTableSection key="policyTable-replace-data">
          <View style={styles.tableSection}>
            <EABTextField
              testID="PreAppilcation__Recommendation__txtLife"
              style={styles.fieldFirst}
              inputStyle={styles.inputFont}
              editable={!disabled}
              value={numberToCurrency({
                value: _.get(value, "rop.replaceLife"),
                decimal: 0
              })}
              placeholder=""
              prefix="$"
              keyboardType="numeric"
              onChange={newValue => {
                this.updateStateValue({
                  path: "rop.replaceLife",
                  newPathValue: currencyToNumber(newValue) || 0
                });
              }}
            />
          </View>
          <View style={styles.tableSection}>
            <EABTextField
              testID="PreAppilcation__Recommendation__txtTPD"
              style={styles.fieldFirst}
              inputStyle={styles.inputFont}
              editable={!disabled}
              value={numberToCurrency({
                value: _.get(value, "rop.replaceTpd"),
                decimal: 0
              })}
              placeholder=""
              prefix="$"
              keyboardType="numeric"
              onChange={newValue => {
                this.updateStateValue({
                  path: "rop.replaceTpd",
                  newPathValue: currencyToNumber(newValue) || 0
                });
              }}
            />
          </View>
          <View style={styles.tableSection}>
            <EABTextField
              testID="PreAppilcation__Recommendation__txtCI"
              style={styles.fieldFirst}
              inputStyle={styles.inputFont}
              editable={!disabled}
              value={numberToCurrency({
                value: _.get(value, "rop.replaceCi"),
                decimal: 0
              })}
              placeholder=""
              prefix="$"
              keyboardType="numeric"
              onChange={newValue => {
                this.updateStateValue({
                  path: "rop.replaceCi",
                  newPathValue: currencyToNumber(newValue) || 0
                });
              }}
            />
          </View>
          <View style={styles.tableSection}>
            <EABTextField
              testID="PreAppilcation__Recommendation__txtPA-ADB"
              style={styles.fieldFirst}
              inputStyle={styles.inputFont}
              editable={!disabled}
              value={numberToCurrency({
                value: _.get(value, "rop.replacePaAdb"),
                decimal: 0
              })}
              placeholder=""
              prefix="$"
              keyboardType="numeric"
              onChange={newValue => {
                this.updateStateValue({
                  path: "rop.replacePaAdb",
                  newPathValue: currencyToNumber(newValue) || 0
                });
              }}
            />
          </View>
          <View style={styles.tableSection}>
            <EABTextField
              testID="PreAppilcation__Recommendation__txtTotalAnnualPremiums"
              style={styles.fieldFirst}
              inputStyle={styles.inputFont}
              editable={!disabled}
              value={numberToCurrency({
                value: _.get(value, "rop.replaceTotalPrem"),
                decimal: 0
              })}
              placeholder=""
              prefix="$"
              keyboardType="numeric"
              onChange={newValue => {
                this.updateStateValue({
                  path: "rop.replaceTotalPrem",
                  newPathValue: currencyToNumber(newValue) || 0
                });
              }}
            />
          </View>
        </EABTableSection>
        {_.get(error, "rop.replaceLife.hasError") ? (
          <EABTableFooter key="policyTable-footer">
            <View style={styles.tableFooter}>
              <TranslatedText
                style={styles.errorMsg}
                language={language}
                path="error.801"
              />
            </View>
          </EABTableFooter>
        ) : null}
      </EABTable>
    );
  }

  renderRequiredComponent(language) {
    const { value, isChosen, disabled, textStore } = this.props;
    const isShieldReasonEmpty =
      value.isShield === true
        ? _.get(value, "recommendReason").trim() === ""
        : _.get(value, "reason").trim() === "";
    const isShieldReason =
      value.isShield === true
        ? _.get(value, "recommendReason")
        : _.get(value, "reason");
    const isShieldRopChoiceQ1 = value.isShield
      ? value.ropQ1 !== "Y" && value.ropQ1 !== "N"
      : value.rop.choiceQ1 !== "Y" && value.rop.choiceQ1 !== "N";
    const isShieldBenefit =
      value.isShield === true
        ? _.get(value, "recommendBenefit")
        : _.get(value, "benefit");
    const isShieldLimit =
      value.isShield === true
        ? _.get(value, "recommendLimit")
        : _.get(value, "limitation");
    return (
      <View>
        <View style={styles.blockViewBottom}>
          <EABFormSectionHeader
            isRequired
            tooltipOptions={{
              preferredContentSize: [420, 150]
            }}
            tooltipText={EABi18n({
              path: "recommendation.recommendation.title.reason.tooltip",
              language,
              textStore
            })}
            style={styles.title}
            title={EABi18n({
              path: "recommendation.recommendation.title.reason",
              language,
              textStore
            })}
          />
          <EABTextBox
            testID="PreAppilcation__Recommendation__txtReasonsForPlanAndRiders"
            numberOfLines={5}
            editable={!disabled}
            maxLength={5000}
            style={styles.textBox}
            value={isShieldReason}
            onChange={newValue => {
              this.updateStateValue({
                path: value.isShield ? "recommendReason" : "reason",
                newPathValue: newValue
              });
            }}
          />
          {isShieldReasonEmpty ? (
            <View>
              <TranslatedText
                style={Theme.fieldTextOrHelperTextNegative}
                language={language}
                path="error.302"
              />
            </View>
          ) : null}
        </View>
        <View style={styles.blockViewBottom}>
          <EABFormSectionHeader
            isRequired
            style={styles.title}
            title={EABi18n({
              path: "recommendation.recommendation.title.benefit",
              language,
              textStore
            })}
          />
          <EABTextBox
            testID="PreAppilcation__Recommendation__txtBenefitOfPlan"
            numberOfLines={5}
            editable={!disabled}
            maxLength={5000}
            style={styles.textBox}
            value={isShieldBenefit}
            onChange={newValue => {
              this.updateStateValue({
                path: "benefit",
                newPathValue: newValue
              });
            }}
            onBlur={newValue => {
              if (_.trim(newValue.nativeEvent.text).length === 0) {
                this.updateStateValue({
                  path: "benefit",
                  newPathValue: _.get(value, "extra.recommendBenefitDefault")
                });
              }
            }}
          />
        </View>
        <View style={styles.blockViewBottom}>
          <EABFormSectionHeader
            isRequired
            style={styles.title}
            title={EABi18n({
              path: "recommendation.recommendation.title.limitation",
              language,
              textStore
            })}
          />
          <EABTextBox
            testID="PreAppilcation__Recommendation__txtLimitationForPlanAndRiders"
            numberOfLines={5}
            editable={!disabled}
            maxLength={5000}
            style={styles.textBox}
            value={isShieldLimit}
            onChange={newValue => {
              this.updateStateValue({
                path: "limitation",
                newPathValue: newValue
              });
            }}
            onBlur={newValue => {
              if (_.trim(newValue.nativeEvent.text).length === 0) {
                this.updateStateValue({
                  path: "limitation",
                  newPathValue: _.get(value, "extra.recommendLimitDefault")
                });
              }
            }}
          />
        </View>
        {isChosen ? (
          <EABFormSectionHeader
            style={styles.title}
            title={EABi18n({
              path: "recommendation.recommendation.title.rop",
              language,
              textStore
            })}
          />
        ) : null}
        {isChosen ? (
          <View style={styles.blockView}>
            <EABFormSectionHeader
              isRequired
              style={styles.title}
              title={EABi18n({
                path: "recommendation.recommendation.title.ropQ1",
                language,
                textStore
              })}
            />
            <View style={styles.textSelection}>
              <EABTextSelection
                testID="PreAppilcation__Recommendation"
                options={[
                  {
                    key: "ropQ1-yes",
                    testID: "ROP-Yes",
                    title: EABi18n({
                      path: "textSelection.yes",
                      language,
                      textStore
                    }),
                    isSelected: value.isShield
                      ? _.get(value, "ropQ1") === "Y"
                      : _.get(value, "rop.choiceQ1") === "Y"
                  },
                  {
                    key: "ropQ1-no",
                    testID: "ROP-No",
                    title: EABi18n({
                      path: "textSelection.no",
                      language,
                      textStore
                    }),
                    isSelected: value.isShield
                      ? _.get(value, "ropQ1") === "N"
                      : _.get(value, "rop.choiceQ1") === "N"
                  }
                ]}
                editable={!disabled}
                onPress={option => {
                  const newValue = option.key === "ropQ1-yes" ? "Y" : "N";
                  this.updateStateValue({
                    path: value.isShield ? "ropQ1" : "rop.choiceQ1",
                    newPathValue: newValue
                  });
                }}
              />
              {isShieldRopChoiceQ1 ? (
                <TranslatedText
                  style={Theme.fieldTextOrHelperTextNegative}
                  language={language}
                  path="error.302"
                />
              ) : null}
            </View>
          </View>
        ) : null}
      </View>
    );
  }

  renderOptionalComponent(language) {
    const { value, disabled, textStore } = this.props;

    return (
      <View>
        <View style={styles.blockView}>
          <EABFormSectionHeader
            isRequired
            style={styles.title}
            title={EABi18n({
              path: "recommendation.recommendation.title.ropQ1Sub1",
              language,
              textStore
            })}
          />
          <View style={styles.textSelection}>
            <EABTextSelection
              testID="PreAppilcation__Recommendation"
              options={[
                {
                  key: "ropQ1Sub1-yes",
                  testID: "ReplacementAdvisedYes",
                  title: EABi18n({
                    path: "textSelection.yes",
                    language,
                    textStore
                  }),
                  isSelected: _.get(value, "rop.choiceQ1Sub1") === "Y"
                },
                {
                  key: "ropQ1Sub1-no",
                  testID: "ReplacementAdvisedNo",
                  title: EABi18n({
                    path: "textSelection.no",
                    language,
                    textStore
                  }),
                  isSelected: _.get(value, "rop.choiceQ1Sub1") === "N"
                }
              ]}
              editable={!disabled}
              onPress={option => {
                const newValue = option.key === "ropQ1Sub1-yes" ? "Y" : "N";
                this.updateStateValue({
                  path: "rop.choiceQ1Sub1",
                  newPathValue: newValue
                });
              }}
            />
            {value.rop.choiceQ1Sub1 !== "Y" &&
            value.rop.choiceQ1Sub1 !== "N" ? (
              <TranslatedText
                style={Theme.fieldTextOrHelperTextNegative}
                language={language}
                path="error.302"
              />
            ) : null}
          </View>
        </View>
        <View style={styles.blockView}>
          <EABFormSectionHeader
            isRequired
            style={styles.title}
            title={EABi18n({
              path: "recommendation.recommendation.title.ropQ1Sub2",
              language,
              textStore
            })}
          />
          <View style={styles.textSelection}>
            <EABTextSelection
              testID="PreAppilcation__Recommendation__"
              options={[
                {
                  key: "ropQ1Sub2-yes",
                  testID: "ExplainImplicationsYes",
                  title: EABi18n({
                    path: "textSelection.yes",
                    language,
                    textStore
                  }),
                  isSelected: _.get(value, "rop.choiceQ1Sub2") === "Y"
                },
                {
                  key: "ropQ1Sub2-no",
                  testID: "ExplainImplicationsNo",
                  title: EABi18n({
                    path: "textSelection.no",
                    language,
                    textStore
                  }),
                  isSelected: _.get(value, "rop.choiceQ1Sub2") === "N"
                }
              ]}
              editable={!disabled}
              onPress={option => {
                const newValue = option.key === "ropQ1Sub2-yes" ? "Y" : "N";
                this.updateStateValue({
                  path: "rop.choiceQ1Sub2",
                  newPathValue: newValue
                });
              }}
            />
            {value.rop.choiceQ1Sub2 !== "Y" &&
            value.rop.choiceQ1Sub2 !== "N" ? (
              <TranslatedText
                style={Theme.fieldTextOrHelperTextNegative}
                language={language}
                path="error.302"
              />
            ) : null}
          </View>
        </View>
        <View style={styles.blockViewNoBorder}>
          <EABFormSectionHeader
            isRequired
            style={styles.title}
            title={EABi18n({
              path: "recommendation.recommendation.title.ropQ1Sub3",
              language,
              textStore
            })}
          />
          <EABTextBox
            testID="PreAppilcation__Recommendation__txtBasicPolicyReplacement"
            numberOfLines={5}
            editable={!disabled}
            maxLength={3000}
            style={styles.textBox}
            value={_.get(value, "rop.choiceQ1Sub3")}
            onChange={newValue => {
              this.updateStateValue({
                path: "rop.choiceQ1Sub3",
                newPathValue: newValue
              });
            }}
          />
          {value.ropBlock.ropQ1sub3.trim().length <= 0 ? (
            <View>
              <TranslatedText
                style={[Theme.fieldTextOrHelperTextNegative, { height: 50 }]}
                language={language}
                path="error.302"
              />
            </View>
          ) : null}
        </View>
        {this.renderWarningNote(language)}
        {this.renderPolicyTable(language)}
      </View>
    );
  }

  render() {
    const { quotId, value, showRecommendationDetails, agent } = this.props;
    const isAWIUT =
      _.difference(["M8", "M8A", "IFAST"], agent.authorised).length === 0 &&
      value.extra.basicPlanName === "EAB Wealth Invest (Cash/SRS) (Cash)";
    return (
      <ContextConsumer>
        {({ language }) => (
          <View style={styles.container}>
            <TranslatedText
              style={styles.header}
              path="recommendation.recommendation.header"
              funcOnText={_.toUpper}
            />
            <View style={styles.planDetailBlockView}>
              <View style={styles.planDetailContentView}>
                <View style={styles.planDetailContentViewRow}>
                  <TranslatedText
                    style={styles.planDetailTitle}
                    path="recommendation.recommendation.proposalNo"
                  />
                  <Text style={styles.planDetailContent}>{quotId}</Text>
                </View>
                <View style={styles.planDetailContentViewRow}>
                  <TranslatedText
                    style={styles.planDetailTitle}
                    path="recommendation.recommendation.proposerLifeAssured"
                  />
                  <Text style={styles.planDetailContent}>
                    {_.get(value, "extra.proposerAndLifeAssuredName", "")}
                  </Text>
                </View>
                <View style={styles.planDetailContentViewRow}>
                  <TranslatedText
                    style={styles.planDetailTitle}
                    path="recommendation.recommendation.basicPlan"
                  />
                  <Text style={styles.planDetailContent}>
                    {_.get(value, "extra.basicPlanName", "")}
                  </Text>
                </View>
                {_.get(value, "extra.ridersName", []).length > 0 ? (
                  <View style={styles.planDetailContentViewRow}>
                    <TranslatedText
                      style={styles.planDetailTitle}
                      path="recommendation.recommendation.riders"
                    />
                    <Text style={styles.planDetailContent}>
                      {this.prepareRidersName(language)}
                    </Text>
                  </View>
                ) : null}
              </View>
              <View style={styles.planDetailButtonView}>
                <EABButton
                  buttonType={RECTANGLE_BUTTON_2}
                  onPress={() => {
                    showRecommendationDetails();
                  }}
                >
                  <TranslatedText
                    language={language}
                    path="recommendation.recommendation.button.viewDetails"
                  />
                </EABButton>
              </View>
            </View>
            {isAWIUT && (
              <TranslatedText
                style={styles.AWICashUT}
                language={language}
                path="recommendation.remarks.AWICashUT"
              />
            )}
            {this.renderRequiredComponent(language)}
            {_.get(value, "rop.choiceQ1") === "Y"
              ? this.renderOptionalComponent(language)
              : null}
          </View>
        )}
      </ContextConsumer>
    );
  }
}

RecommendationFormNormal.propTypes = {
  textStore: APP_REDUCER_TYPE_CHECK[TEXT_STORE].isRequired,
  optionsMap: WEB_API_REDUCER_TYPE_CHECK[OPTIONS_MAP].isRequired,
  quotId: PropTypes.string.isRequired,
  value:
    WEB_API_REDUCER_TYPE_CHECK[RECOMMENDATION].recommendation.valueByQuotId
      .isRequired,
  error:
    WEB_API_REDUCER_TYPE_CHECK[RECOMMENDATION].recommendation.error.isRequired,
  isChosen: PropTypes.bool.isRequired,
  disabled: PropTypes.bool.isRequired,
  showRecommendationDetails: PropTypes.func.isRequired,
  updateRecommendationData: PropTypes.func.isRequired,
  agent: PropTypes.oneOfType([PropTypes.object]).isRequired
};

export default RecommendationFormNormal;
