import React, { PureComponent } from "react";
import {
  View,
  Text,
  Linking,
  Alert,
  NativeModules,
  NativeEventEmitter,
  NetInfo
} from "react-native";
import PropTypes from "prop-types";
import SafariView from "react-native-safari-view";
import * as _ from "lodash";
import { REDUCER_TYPES, REDUCER_TYPE_CHECK, EAPP } from "eab-web-api";
import APP_REDUCER_TYPE_CHECK from "../../../../constants/REDUCER_TYPE_CHECK";
import { TEXT_STORE } from "../../../../constants/REDUCER_TYPES";
import Theme from "../../../../theme";
import getEABTextByLangType from "../../../../utilities/getEABTextByLangType";
import checkOnlinePaymentAndSubmission from "../../../../apis/checkOnlinePaymentAndSubmission";

import TranslatedText from "../../../../containers/TranslatedText";
import EABCard from "../../../../components/EABCard";
import { RECTANGLE_BUTTON_1 } from "../../../../components/EABButton/constants";
import EABButton from "../../../../components/EABButton";
import { ContextConsumer } from "../../../../context";
import EABi18n from "../../../../utilities/EABi18n";
import getEnvUrl from "../../../../utilities/getEnvUrl";
import styles from "./styles";
import EABHUD from "../../../../containers/EABHUD";

// =============================================================================
// constants
// =============================================================================
const {
  IS_ONLINE_CHECKING_CONFLICT,
  IS_AFTER_SUBMIT,
  IS_BEFORE_SUBMIT
} = EAPP.dataSyncStep;

/**
 * Submission
 * @description Submission in Application Summary page.
 * @param {array} Submission - array of product item object
 * */

class Submission extends PureComponent {
  constructor(props) {
    super(props);
    const dataSync = new NativeEventEmitter(NativeModules.DataSyncEventManager);
    this.handleConnectionChange = this.handleConnectionChange.bind(this);
    this.dataSyncListener = dataSync.addListener(
      "DataSyncEventHandler",
      data => {
        if (data === "SUCCESS") {
          this.setState({
            showLoading: true
          });
          setTimeout(() => {
            this.dataSyncHandler();
          }, 500);
        }
      }
    );
    this.state = {
      submissionUrl: getEnvUrl(this.props.environment, "GET_AUTH_CODE_FULL_URL")
        .url,
      isNetworkConnected: false,
      showLoading: false,
      workType: "",
      lang: ""
    };
    this.loginCallBack = this.loginCallBack.bind(this);
  }

  componentDidMount() {
    // listen to network connection, isConnected: ${bool}
    NetInfo.isConnected.addEventListener(
      "connectionChange",
      this.handleConnectionChange
    );
    NetInfo.isConnected.fetch().done(this.handleConnectionChange);
    const { getSubmission, application } = this.props;
    if (!_.isEmpty(application)) {
      getSubmission(application.id);
      Linking.addEventListener("url", this.loginCallBack);
    }
  }

  componentWillUnmount() {
    Linking.removeEventListener("url", this.loginCallBack);
    NetInfo.isConnected.removeEventListener(
      "connectionChange",
      this.handleConnectionChange
    );
    this.dataSyncListener.remove();
    this.dataSyncListener = null;
  }

  getSubmissionMessage(language) {
    const { payment, submission, application, agent, error } = this.props;
    const { isNetworkConnected } = this.state;
    if (_.isEmpty(payment) || _.isEmpty(submission)) {
      return null;
    }
    const { agentChannelType, submitStatus } = submission.values;
    if (!isNetworkConnected) {
      return (
        <TranslatedText
          style={Theme.bodyPrimary}
          language={language}
          path="paymentAndSubmission.section.submission.networkDisconnected"
        />
      );
    }
    if (payment.values.initPayMethod === "teleTransfter") {
      if (
        !payment.values.ttRemittingBank ||
        !payment.values.ttDOR ||
        !payment.values.ttRemarks
      ) {
        // 36573
        // return (
        //   <TranslatedText
        //     style={Theme.bodyPrimary}
        //     language={language}
        //     path="paymentAndSubmission.section.submission.cannotSubmitTeleTransfter"
        //   />
        // );
        return null;
      } else if (!application.isMandDocsAllUploaded) {
        return (
          <TranslatedText
            style={Theme.bodyPrimary}
            language={language}
            path="paymentAndSubmission.section.submission.cannotSubmit"
          />
        );
      }
    }
    if (!payment.values.initPayMethod || payment.values.initPayMethod === "") {
      return (
        <TranslatedText
          style={Theme.bodyPrimary}
          language={language}
          path="paymentAndSubmission.section.submission.cannotSubmitNoPaymentSelected"
        />
      );
    } else if (!application.isMandDocsAllUploaded) {
      return (
        <TranslatedText
          style={Theme.bodyPrimary}
          language={language}
          path="paymentAndSubmission.section.submission.cannotSubmit"
        />
      );
    } else if (
      ["crCard", "eNets", "dbsCrCardIpp"].indexOf(
        payment.values.initPayMethod
      ) >= 0 &&
      this.props.onlinePaymentStatus.trxStatusRemark !== "Y"
    ) {
      return (
        <TranslatedText
          style={Theme.bodyPrimary}
          language={language}
          path="paymentAndSubmission.section.submission.cannotSubmitNoPaymentComplete"
        />
      );
    } else if (
      (payment.values.initPayMethod === "srs" ||
        payment.values.initPayMethod === "cpfissa" ||
        payment.values.initPayMethod === "cpfisoa") &&
      _.some(error, "hasError")
    ) {
      return null;
    } else if (agent.role !== "04" && !agent.managerCode) {
      return (
        <TranslatedText
          style={Theme.bodyPrimary}
          language={language}
          path="paymentAndSubmission.text.supervisorNotAvailable"
        />
      );
    } else if (
      agentChannelType === "N" &&
      application.isMandDocsAllUploaded &&
      submitStatus !== "SUCCESS" &&
      submitStatus !== "FAIL"
    ) {
      return (
        <TranslatedText
          style={Theme.bodyPrimary}
          language={language}
          path="paymentAndSubmission.section.submission.canSubmit"
        />
      );
    }
    return (
      <TranslatedText
        style={Theme.bodyPrimary}
        language={language}
        path="paymentAndSubmission.section.submission.canSubmit"
      />
    );
  }

  getSubmissionSection() {
    const {
      submission,
      payment,
      error,
      setApiResponsePage,
      application,
      agent,
      isDisableEappButton,
      disableEappButton
    } = this.props;
    const { isNetworkConnected } = this.state;
    if (_.isEmpty(submission) || _.isEmpty(payment)) {
      return null;
    }

    return (
      <ContextConsumer>
        {({ language, hideApplicationDialog }) => (
          <EABCard style={styles.paymentCard}>
            <View style={styles.paymentHeaderView}>
              <TranslatedText
                style={Theme.headlinePrimary}
                language={language}
                path="paymentAndSubmission.section.submission.title"
              />
            </View>
            <View style={styles.analysisSectionBorderLine} />
            {submission.values.submitStatus === "SUCCESS" ? (
              <View style={styles.contentView}>
                <View style={styles.submitMessageView}>
                  <TranslatedText
                    style={Theme.bodySecondary}
                    language={language}
                    path="paymentAndSubmission.section.payment.submitSuccessText2"
                  />
                  {submission.values.agentChannelType === "N" ||
                  submission.values.receiveEmailFa === "Y" ? (
                    <View style={styles.submitMessageView}>
                      <TranslatedText
                        style={Theme.bodySecondary}
                        language={language}
                        path="paymentAndSubmission.section.payment.submitSuccessText1"
                      />
                    </View>
                  ) : null}
                </View>

                <View
                  style={[styles.submitMessageView, styles.bottomMarginFix]}
                >
                  <View style={styles.sectionContentRowView}>
                    <TranslatedText
                      style={styles.submissionSectionContentTitle}
                      language={language}
                      path="paymentAndSubmission.section.text.policyNumber"
                    />

                    <Text style={styles.blueText}>
                      {submission.values.policyNumber}
                    </Text>
                  </View>
                  <View style={styles.sectionContentRowView}>
                    <TranslatedText
                      style={styles.submissionSectionContentTitle}
                      language={language}
                      path="paymentAndSubmission.section.text.eSubmissionDateAndTime"
                    />

                    <Text style={styles.blueText}>
                      {submission.values.eSubmitDateTime}
                    </Text>
                  </View>
                  <View style={styles.sectionContentRowView}>
                    <TranslatedText
                      style={styles.submissionSectionContentTitle}
                      language={language}
                      path="paymentAndSubmission.section.text.proposerName"
                    />

                    <Text style={styles.blueText}>
                      {submission.values.proposerName}
                    </Text>
                  </View>
                  <View style={styles.sectionContentRowView}>
                    <TranslatedText
                      style={styles.submissionSectionContentTitle}
                      language={language}
                      path="paymentAndSubmission.section.text.planName"
                    />

                    <Text style={styles.blueText}>
                      {getEABTextByLangType({
                        data: submission.values.planName,
                        language
                      })}
                    </Text>
                  </View>
                </View>
              </View>
            ) : null}
            {submission.values.submitStatus === "FAIL" ? (
              <View style={styles.contentView}>
                <View style={styles.submitMessageView}>
                  <Text style={Theme.bodySecondary}>
                    {submission.values.failMsg}
                  </Text>
                </View>
                <View
                  style={[styles.submitMessageView, styles.bottomMarginFix]}
                >
                  <View style={styles.sectionContentRowView}>
                    <TranslatedText
                      style={styles.submissionSectionContentTitle}
                      language={language}
                      path="paymentAndSubmission.section.text.lastSubmissionDateAndTime"
                    />

                    <Text style={Theme.bodySecondary}>
                      {submission.values.lastSubmitDateTime}
                    </Text>
                  </View>
                </View>
              </View>
            ) : null}
            {submission.values.submitStatus !== "SUCCESS" &&
            submission.values.submitStatus !== "FAIL" ? (
              <View style={styles.contentView}>
                <View style={styles.submitMessageView}>
                  {this.getSubmissionMessage(language)}
                </View>
                <View
                  style={[styles.submitMessageView, styles.bottomMarginFix]}
                >
                  <EABButton
                    style={styles.submissionButton}
                    buttonType={RECTANGLE_BUTTON_1}
                    isDisable={
                      isDisableEappButton ||
                      // check network
                      !isNetworkConnected ||
                      // check supporting document
                      !application.isMandDocsAllUploaded ||
                      // check if payment completed
                      (["crCard", "eNets", "dbsCrCardIpp"].indexOf(
                        payment.values.initPayMethod
                      ) >= 0 &&
                        this.props.onlinePaymentStatus.trxStatusRemark !==
                          "Y") ||
                      // check if payment selected
                      (!payment.values.initPayMethod ||
                        payment.values.initPayMethod === "") ||
                      // check if payment fields are all filled
                      (payment.values.initPayMethod === "teleTransfter"
                        ? !payment.values.ttRemittingBank ||
                          !payment.values.ttDOR ||
                          !payment.values.ttRemarks
                        : false) ||
                      (payment.values.initPayMethod === "srs" ||
                      payment.values.initPayMethod === "cpfissa" ||
                      payment.values.initPayMethod === "cpfisoa"
                        ? _.some(error, "hasError")
                        : false) ||
                      (agent.role !== "04" && !agent.managerCode) ||
                      false
                    }
                    onPress={() => {
                      this.setState(
                        {
                          hideApplicationDialog,
                          showLoading: true,
                          lang: language
                        },
                        () => {
                          disableEappButton(true);
                          setApiResponsePage({ apiPage: "submission" });
                          this.checkTokenBeforeSubmission(language);
                        }
                      );
                    }}
                  >
                    <TranslatedText language={language} path="button.submit" />
                  </EABButton>
                </View>
              </View>
            ) : null}
          </EABCard>
        )}
      </ContextConsumer>
    );
  }

  doOnlineLogin() {
    const { submissionUrl } = this.state;
    const { disableEappButton } = this.props;
    disableEappButton(false);
    this.setState(
      {
        showLoading: false
      },
      () => {
        setTimeout(() => {
          SafariView.isAvailable()
            .then(
              SafariView.show({
                url: submissionUrl
              })
            )
            .catch(error => {
              // Fallback WebView code for iOS 8 and earlier
              setTimeout(() => {
                Alert.alert(`[Error]${error}`);
              }, 200);
            });
        }, 500);
      }
    );
  }

  checkTokenBeforeSubmission(language) {
    const {
      loginByOnlineAuthForPayment,
      textStore,
      checkTokenExpiryDate,
      disableEappButton
    } = this.props;
    const { isNetworkConnected } = this.state;
    if (!isNetworkConnected) {
      Alert.alert(
        EABi18n({
          language,
          textStore,
          path: "paymentAndSubmission.section.submission.networkDisconnected"
        })
      );
    } else {
      checkTokenExpiryDate(status => {
        // status = false if its need to do online auth
        if (!status) {
          const title = EABi18n({
            language,
            textStore,
            path: "paymentAndSubmission.section.submission.title"
          });
          const warningMessage = EABi18n({
            language,
            textStore,
            path: "paymentAndSubmission.section.payment.onlineAuthWarning"
          });
          loginByOnlineAuthForPayment();
          Alert.alert(
            title,
            warningMessage,
            [
              {
                text: "Continue",
                onPress: () => this.doOnlineLogin()
              },
              {
                text: "Cancel",
                onPress: () => {
                  disableEappButton(false);
                  this.setState({
                    showLoading: false
                  });
                },
                style: "cancel"
              }
            ],
            { cancelable: false }
          );
        } else if (!isNetworkConnected) {
          disableEappButton(false);
          this.setState(
            {
              showLoading: false
            },
            () => {
              Alert.alert(
                EABi18n({
                  language,
                  textStore,
                  path:
                    "paymentAndSubmission.section.submission.networkDisconnected"
                })
              );
            }
          );
        } else {
          this.checkIfDataSyncNeeded();
        }
      });
    }
  }

  /**
   * @description check if data sync needed before payment or submission
   * */
  checkIfDataSyncNeeded() {
    const {
      application,
      authToken,
      agentCode,
      dataSync,
      lastDataSyncTime,
      environment
    } = this.props;
    const { hideApplicationDialog } = this.state;
    checkOnlinePaymentAndSubmission({
      authToken,
      clientId: application.pCid,
      agentCode,
      lastDataSyncTime,
      environment,
      callback: () => {}
    })
      .then(response => {
        this.setState(
          {
            showLoading: false
          },
          () => {
            setTimeout(() => {
              if (response.hasError) {
                Alert.alert(`[Error] ${response.errorMsg}`, null, [
                  {
                    text: "OK",
                    onPress: null
                  }
                ]);
                return;
              }
              if (response.result.isWebDataConflict) {
                Alert.alert(
                  "This application has been refreshed as either you have completed online payment or submission. You will be redirected to application summary to proceed further.",
                  null,
                  [
                    {
                      text: "Continue",
                      onPress: () => {
                        this.setState(
                          {
                            workType: IS_ONLINE_CHECKING_CONFLICT,
                            hideApplicationDialog
                          },
                          dataSync
                        );
                      }
                    }
                  ]
                );
                return;
              }
              this.setState(
                {
                  workType: IS_BEFORE_SUBMIT
                },
                dataSync
              );
            }, 500);
          }
        );
      })
      .catch(response => {
        Alert.alert(
          `[Error] ${response.errorMsg || response.error || response}`,
          null,
          [
            {
              text: "OK",
              onPress: null
            }
          ]
        );
      });
  }

  loginCallBack(response) {
    const {
      setOnlineAuthCodeForPayment,
      apiPage,
      disableEappButton
    } = this.props;
    if (response.url && apiPage === "submission") {
      this.setState({
        showLoading: true
      });
      disableEappButton(true);
      const urlGet = response.url;
      SafariView.dismiss();
      const segment1 = urlGet.replace("easemobile://?", "");
      const variables = segment1.split("&");
      const codeString = variables.find(variable => variable.includes("code"));
      const [, authCode] = codeString.split("=");
      if (!this.props.isHandlingListener) {
        setOnlineAuthCodeForPayment({
          authCode,
          callback: callback => {
            if (callback.hasError) {
              disableEappButton(false);
              if (callback.errorMsg) {
                setTimeout(() => {
                  Alert.alert(`[Error]  ${callback.errorMsg}`);
                }, 200);
              } else {
                setTimeout(() => {
                  Alert.alert(
                    `error msg: Authorization for submission Failed (Error 001)`
                  );
                }, 200);
              }
            } else {
              const { isOnlineAuthCompleted } = this.props;
              if (isOnlineAuthCompleted) {
                this.checkIfDataSyncNeeded();
              }
            }
          }
        });
      }
    }
  }

  callSubmissionApi() {
    const {
      callApiSubmitApplication,
      application,
      dataSync,
      disableEappButton
    } = this.props;
    let { lang } = this.state;
    switch (lang) {
      case "ENGLISH":
        lang = "en";
        break;
      case "TRADITIONAL_CHINESE":
        lang = "zh-Hant";
        break;
      default:
        lang = "en";
        break;
    }
    callApiSubmitApplication({
      docId: application.id,
      lang,
      callback: submitResponse => {
        if (!submitResponse[0].hasError) {
          this.setState(
            {
              workType: IS_AFTER_SUBMIT,
              showLoading: false
            },
            () => {
              setTimeout(() => {
                dataSync();
              }, 200);
            }
          );
        } else {
          // Submission is not successfully
          Alert.alert(`[Error] ${submitResponse[0].errorMsg}`, null, [
            {
              text: "OK",
              onPress: () => {
                disableEappButton(false);
                this.setState({
                  showLoading: false
                });
              }
            }
          ]);
        }
      }
    });
  }

  handleConnectionChange(isConnected) {
    this.setState({ isNetworkConnected: isConnected });
  }

  dataSyncHandler() {
    const { workType, hideApplicationDialog } = this.state;
    const {
      setLastDataSyncTime,
      getApplicationsAfterSubmission,
      getApplicationList,
      application,
      disableEappButton
    } = this.props;
    // update LastDataSyncTime after data sync
    setLastDataSyncTime();
    switch (workType) {
      case IS_ONLINE_CHECKING_CONFLICT:
        disableEappButton(false);
        getApplicationList(
          setTimeout(() => {
            hideApplicationDialog(() => {
              setTimeout(() => {
                this.setState({
                  showLoading: false
                });
              }, 1000);
            });
          }, 500)
        );
        break;
      case IS_AFTER_SUBMIT:
        Alert.alert("Submission is completed", null, [
          {
            text: "OK",
            onPress: () => {
              disableEappButton(false);
              getApplicationsAfterSubmission({
                applicationId: application.id,
                callback: () => {
                  this.setState({
                    showLoading: false
                  });
                }
              });
            }
          }
        ]);
        break;
      case IS_BEFORE_SUBMIT:
        this.callSubmissionApi();
        break;
      default:
        this.setState({
          showLoading: false
        });
        break;
    }
  }

  render() {
    return (
      <View>
        <EABHUD isOpen={this.state.showLoading} />
        {this.getSubmissionSection()}
      </View>
    );
  }
}

Submission.propTypes = {
  getApplicationList: PropTypes.func.isRequired,
  getSubmission: PropTypes.func.isRequired,
  getApplicationsAfterSubmission: PropTypes.func.isRequired,
  isOnlineAuthCompleted: PropTypes.bool.isRequired,
  dataSync: PropTypes.func.isRequired,
  setLastDataSyncTime: PropTypes.func.isRequired,
  callApiSubmitApplication: PropTypes.func.isRequired,
  setApiResponsePage: PropTypes.func.isRequired,
  setOnlineAuthCodeForPayment: PropTypes.func.isRequired,
  isHandlingListener: PropTypes.bool.isRequired,
  loginByOnlineAuthForPayment: PropTypes.func.isRequired,
  checkTokenExpiryDate: PropTypes.func.isRequired,
  apiPage: PropTypes.string.isRequired,
  textStore: APP_REDUCER_TYPE_CHECK[TEXT_STORE].isRequired,
  error:
    REDUCER_TYPE_CHECK[REDUCER_TYPES.PAYMENT_AND_SUBMISSION].error.isRequired,
  application:
    REDUCER_TYPE_CHECK[REDUCER_TYPES.PRE_APPLICATION].ApplicationItem.value
      .isRequired,
  submission:
    REDUCER_TYPE_CHECK[REDUCER_TYPES.PAYMENT_AND_SUBMISSION].submission
      .isRequired,
  payment:
    REDUCER_TYPE_CHECK[REDUCER_TYPES.PAYMENT_AND_SUBMISSION].payment.isRequired,
  environment: PropTypes.string.isRequired,
  onlinePaymentStatus: PropTypes.oneOfType([PropTypes.object]).isRequired,
  agent: PropTypes.oneOfType([PropTypes.object]).isRequired,
  authToken: PropTypes.string.isRequired,
  agentCode: PropTypes.string.isRequired,
  lastDataSyncTime: PropTypes.string.isRequired,
  isDisableEappButton: PropTypes.bool.isRequired,
  disableEappButton: PropTypes.func.isRequired
};

export default Submission;
