import React, { PureComponent } from "react";
import {
  View,
  Text,
  Linking,
  Alert,
  NativeModules,
  NativeEventEmitter,
  NetInfo
} from "react-native";
import PropTypes from "prop-types";
import SafariView from "react-native-safari-view";
import * as _ from "lodash";
import moment from "moment";
import {
  REDUCER_TYPES,
  REDUCER_TYPE_CHECK,
  TIME_FORMAT,
  EAPP
} from "eab-web-api";
import APP_REDUCER_TYPE_CHECK from "../../../../constants/REDUCER_TYPE_CHECK";
import checkOnlinePaymentAndSubmission from "../../../../apis/checkOnlinePaymentAndSubmission";
import { TEXT_STORE } from "../../../../constants/REDUCER_TYPES";
import Theme from "../../../../theme";
import EABTable from "../../../../components/EABTable";
import EABTableSection from "../../../../components/EABTableSection";
import EABTableHeader from "../../../../components/EABTableHeader";
import getEABTextByLangType from "../../../../utilities/getEABTextByLangType";
import TranslatedText from "../../../../containers/TranslatedText";
import { RECTANGLE_BUTTON_1 } from "../../../../components/EABButton/constants";
import EABButton from "../../../../components/EABButton";
import { ContextConsumer } from "../../../../context";
import EABi18n from "../../../../utilities/EABi18n";
import getEnvUrl from "../../../../utilities/getEnvUrl";
import styles from "./styles";
import EABHUD from "../../../../containers/EABHUD";

// =============================================================================
// constants
// =============================================================================
const {
  IS_ONLINE_CHECKING_CONFLICT,
  IS_AFTER_SUBMIT,
  IS_BEFORE_SUBMIT
} = EAPP.dataSyncStep;

/**
 * SubmissionShield
 * @description SubmissionShield in Application Summary page.
 * @param {array} SubmissionShield - array of product item object
 * */

class SubmissionShield extends PureComponent {
  constructor(props) {
    super(props);
    const dataSync = new NativeEventEmitter(NativeModules.DataSyncEventManager);
    this.dataSyncListener = dataSync.addListener(
      "DataSyncEventHandler",
      data => {
        if (data === "SUCCESS") {
          this.setState({
            showLoading: true
          });
          setTimeout(() => {
            this.dataSyncHandler();
          }, 500);
        }
      }
    );

    this.state = {
      submissionUrl: getEnvUrl(this.props.environment, "GET_AUTH_CODE_FULL_URL")
        .url,
      isNetworkConnected: false,
      showLoading: false,
      workType: "",
      lang: ""
    };
    // listen to network connection, isConnected: ${bool}
    NetInfo.isConnected.addEventListener("connectionChange", isConnected =>
      this.setState({ isNetworkConnected: isConnected })
    );
    NetInfo.isConnected.fetch().done(isConnected => {
      this.setState({
        isNetworkConnected: isConnected
      });
    });
    this.loginCallback = this.loginCallback.bind(this);
  }

  componentDidMount() {
    const { application } = this.props;
    if (!_.isEmpty(application)) {
      Linking.addEventListener("url", this.loginCallback);
    }
  }

  componentWillUnmount() {
    Linking.removeEventListener("url", this.loginCallback);
    this.dataSyncListener.remove();
    this.dataSyncListener = null;
  }

  getNotSubmittedContent() {
    const {
      payment,
      application,
      setApiResponsePage,
      agent,
      disableEappButton
    } = this.props;
    const { isNetworkConnected } = this.state;
    let messagePath = "paymentAndSubmission.section.submission.canSubmit";
    if (!isNetworkConnected) {
      messagePath =
        "paymentAndSubmission.section.submission.networkDisconnected";
    } else if (!application.isMandDocsAllUploaded) {
      messagePath = "paymentAndSubmission.section.submission.cannotSubmit";
    } else if (application.submitStatus === "FAIL") {
      messagePath = "paymentAndSubmission.section.submission.submissionFail";
    } else if (payment.totCashPortion && payment.totCashPortion > 0) {
      if (!payment.initPayMethod || payment.initPayMethod === "") {
        messagePath =
          "paymentAndSubmission.section.submission.cannotSubmitNoPaymentSelected";
      } else if (
        ["crCard", "eNets", "dbsCrCardIpp"].indexOf(payment.initPayMethod) >= 0
          ? this.props.onlinePaymentStatus.trxStatusRemark !== "Y"
          : false
      ) {
        messagePath =
          "paymentAndSubmission.section.submission.cannotSubmitNoPaymentComplete";
      } else if (payment.initPayMethod === "teleTransfter") {
        if (!payment.ttRemittingBank || !payment.ttDOR || !payment.ttRemarks) {
          messagePath =
            "paymentAndSubmission.section.submission.cannotSubmitTeleTransfter";
        }
      }
    } else if (agent.role !== "04" && !agent.managerCode) {
      messagePath = "paymentAndSubmission.text.supervisorNotAvailable";
    } else if (
      application.agentChannelType === "N" &&
      application.isMandDocsAllUploaded &&
      application.submitStatus !== "SUCCESS" &&
      application.submitStatus !== "FAIL"
    ) {
      messagePath = "paymentAndSubmission.section.submission.canSubmit";
    }
    return (
      <ContextConsumer>
        {({ language, hideApplicationDialog }) => (
          <View>
            <View style={styles.sectionContentView}>
              <TranslatedText
                style={Theme.bodyPrimary}
                language={language}
                path={messagePath}
              />
            </View>
            <View style={styles.sectionContentView}>
              {application.submitStatus === "FAIL" ? (
                <View style={styles.sectionContentRowView}>
                  <TranslatedText
                    style={styles.submissionSectionContentTitle}
                    language={language}
                    path="paymentAndSubmission.section.text.lastSubmissionDateAndTime"
                  />

                  <Text style={Theme.bodySecondary}>
                    {moment(application.applicationSubmittedDate).format(
                      TIME_FORMAT.DATE_TIME_WITH_TIME
                    )}
                  </Text>
                </View>
              ) : null}
              <EABButton
                style={styles.submissionButton}
                buttonType={RECTANGLE_BUTTON_1}
                isDisable={this.checkIsSubmitButtonDisable()}
                onPress={() => {
                  this.setState(
                    {
                      hideApplicationDialog,
                      showLoading: true,
                      lang: language
                    },
                    () => {
                      disableEappButton(true);
                      setApiResponsePage({ apiPage: "shieldSubmission" });
                      this.checkTokenBeforeSubmission(language);
                    }
                  );
                }}
              >
                <TranslatedText language={language} path="button.submit" />
              </EABButton>
            </View>
          </View>
        )}
      </ContextConsumer>
    );
  }

  getSubmittedContent() {
    const { application } = this.props;
    return (
      <ContextConsumer>
        {({ language }) => (
          <View style={styles.sectionView}>
            <View style={styles.sectionContentView}>
              <TranslatedText
                style={Theme.bodyPrimary}
                language={language}
                path="paymentAndSubmission.section.payment.submitSuccessText2"
              />
            </View>
            <View style={styles.sectionContentView}>
              <View style={styles.sectionContentRowView}>
                <TranslatedText
                  style={styles.submissionSectionContentTitle}
                  language={language}
                  path="paymentAndSubmission.section.text.eSubmissionDateAndTime"
                />
                <Text style={styles.blueText}>
                  {moment(application.applicationSubmittedDate).format(
                    TIME_FORMAT.DATE_TIME_WITH_TIME
                  )}
                </Text>
              </View>
              <View style={styles.sectionContentRowView}>
                <TranslatedText
                  style={styles.submissionSectionContentTitle}
                  language={language}
                  path="paymentAndSubmission.section.text.proposerName"
                />

                <Text style={styles.blueText}>
                  {
                    application.applicationForm.values.proposer.personalInfo
                      .fullName
                  }
                </Text>
              </View>
            </View>
            <View style={styles.sectionContentView} />
            <EABTable
              tableConfig={[
                {
                  key: "policy_number",
                  style: [styles.tableSectionColumn1, styles.tableSection]
                },
                {
                  key: "life_assured",
                  style: [styles.tableSectionColumn2, styles.tableSection]
                },
                {
                  key: "plan_name",
                  style: [styles.tableSectionColumn3, styles.tableSection]
                }
              ]}
            >
              <EABTableHeader key="submissionTableHeader">
                <View>
                  <TranslatedText
                    style={Theme.tableColumnLabel}
                    language={language}
                    path="application.policy_number"
                  />
                </View>
                <View>
                  <TranslatedText
                    style={Theme.tableColumnLabel}
                    language={language}
                    path="application.life_assured"
                  />
                </View>
                <View>
                  <TranslatedText
                    style={Theme.tableColumnLabel}
                    language={language}
                    path="application.summary.item_header_planName"
                  />
                </View>
              </EABTableHeader>
              {application.payment.premiumDetails.map(value => (
                <EABTableSection>
                  <View>
                    <Text>{value.policyNumber}</Text>
                  </View>
                  <View>
                    <Text>{value.laName}</Text>
                  </View>
                  <View>
                    <Text>
                      {getEABTextByLangType({
                        data: value.covName,
                        language
                      })}
                    </Text>
                  </View>
                </EABTableSection>
              ))}
            </EABTable>
          </View>
        )}
      </ContextConsumer>
    );
  }

  getSubmissionSection() {
    const { payment, application } = this.props;
    if (_.isEmpty(application) || _.isEmpty(payment)) {
      return null;
    }
    let content = "";
    if (application.submitStatus && application.submitStatus === "SUCCESS") {
      content = this.getSubmittedContent();
    } else {
      content = this.getNotSubmittedContent();
    }
    return (
      <ContextConsumer>
        {({ language }) => (
          <View style={styles.sectionView}>
            <View style={styles.sectionHeaderView}>
              <TranslatedText
                style={Theme.captionBrand}
                language={language}
                path="paymentAndSubmission.section.submission.title"
              />
            </View>
            {content}
          </View>
        )}
      </ContextConsumer>
    );
  }

  dataSyncHandler() {
    const { workType, hideApplicationDialog } = this.state;
    const {
      setLastDataSyncTime,
      getApplicationList,
      continueApplicationFormShield,
      application,
      disableEappButton
    } = this.props;
    // update LastDataSyncTime after data sync
    setLastDataSyncTime();
    switch (workType) {
      case IS_ONLINE_CHECKING_CONFLICT:
        disableEappButton(false);
        getApplicationList(
          setTimeout(() => {
            hideApplicationDialog(() => {
              setTimeout(() => {
                this.setState({
                  showLoading: false
                });
              }, 1000);
            });
          }, 500)
        );
        break;
      case IS_AFTER_SUBMIT:
        Alert.alert("Submission is completed", null, [
          {
            text: "OK",
            onPress: () => {
              disableEappButton(false);
              continueApplicationFormShield({
                applicationId: application.id,
                callback: () => {
                  this.setState({
                    showLoading: false
                  });
                }
              });
            }
          }
        ]);
        break;
      case IS_BEFORE_SUBMIT:
        this.callSubmissionApi();
        break;
      default:
        this.setState({
          showLoading: false
        });
        break;
    }
  }

  doOnlineLogin() {
    const { submissionUrl } = this.state;
    const { disableEappButton } = this.props;
    disableEappButton(false);
    this.setState(
      {
        showLoading: false
      },
      () => {
        setTimeout(() => {
          SafariView.isAvailable()
            .then(
              SafariView.show({
                url: submissionUrl
              })
            )
            .catch(error => {
              // Fallback WebView code for iOS 8 and earlier
              setTimeout(() => {
                Alert.alert(`[Error]${error}`);
              }, 200);
            });
        }, 500);
      }
    );
  }

  checkTokenBeforeSubmission(language) {
    const {
      loginByOnlineAuthForPayment,
      textStore,
      checkTokenExpiryDate,
      disableEappButton
    } = this.props;
    const { isNetworkConnected } = this.state;
    if (!isNetworkConnected) {
      Alert.alert(
        EABi18n({
          language,
          textStore,
          path: "paymentAndSubmission.section.submission.networkDisconnected"
        })
      );
    } else {
      checkTokenExpiryDate(status => {
        // status = false if its need to do online auth
        if (!status) {
          const title = EABi18n({
            language,
            textStore,
            path: "paymentAndSubmission.section.submission.title"
          });
          const warningMessage = EABi18n({
            language,
            textStore,
            path: "paymentAndSubmission.section.payment.onlineAuthWarning"
          });
          loginByOnlineAuthForPayment();
          Alert.alert(
            title,
            warningMessage,
            [
              {
                text: "Continue",
                onPress: () => this.doOnlineLogin()
              },
              {
                text: "Cancel",
                onPress: () => {
                  disableEappButton(false);
                  this.setState({
                    showLoading: false
                  });
                },
                style: "cancel"
              }
            ],
            { cancelable: false }
          );
        } else if (!isNetworkConnected) {
          disableEappButton(false);
          this.setState(
            {
              showLoading: false
            },
            () => {
              Alert.alert(
                EABi18n({
                  language,
                  textStore,
                  path:
                    "paymentAndSubmission.section.submission.networkDisconnected"
                })
              );
            }
          );
        } else {
          this.checkIfDataSyncNeeded();
        }
      });
    }
  }

  /**
   * @description check if data sync needed before payment or submission
   * */
  checkIfDataSyncNeeded() {
    const {
      application,
      authToken,
      agentCode,
      dataSync,
      lastDataSyncTime,
      environment
    } = this.props;
    const { hideApplicationDialog } = this.state;
    checkOnlinePaymentAndSubmission({
      authToken,
      clientId: application.pCid,
      agentCode,
      lastDataSyncTime,
      environment,
      callback: () => {}
    })
      .then(response => {
        this.setState(
          {
            showLoading: false
          },
          () => {
            setTimeout(() => {
              if (response.hasError) {
                Alert.alert(`[Error] ${response.errorMsg}`, null, [
                  {
                    text: "OK",
                    onPress: null
                  }
                ]);
                return;
              }
              if (response.result.isWebDataConflict) {
                Alert.alert(
                  "This application has been refreshed as either you have completed online payment or submission. You will be redirected to application summary to proceed further.",
                  null,
                  [
                    {
                      text: "Continue",
                      onPress: () => {
                        this.setState(
                          {
                            workType: IS_ONLINE_CHECKING_CONFLICT,
                            hideApplicationDialog
                          },
                          dataSync
                        );
                      }
                    }
                  ]
                );
                return;
              }
              this.setState(
                {
                  workType: IS_BEFORE_SUBMIT
                },
                dataSync
              );
            }, 500);
          }
        );
      })
      .catch(response => {
        Alert.alert(
          `[Error] ${response.errorMsg || response.error || response}`,
          null,
          [
            {
              text: "OK",
              onPress: null
            }
          ]
        );
      });
  }

  loginCallback(response) {
    const {
      setOnlineAuthCodeForPayment,
      apiPage,
      disableEappButton
    } = this.props;
    this.setState({
      showLoading: true
    });
    disableEappButton(true);
    if (response.url && apiPage === "shieldSubmission") {
      const urlGet = response.url;
      SafariView.dismiss();
      const segment1 = urlGet.replace("easemobile://?", "");
      const variables = segment1.split("&");
      const codeString = variables.find(variable => variable.includes("code"));
      const [, authCode] = codeString.split("=");
      if (!this.props.isHandlingListener) {
        setOnlineAuthCodeForPayment({
          authCode,
          callback: callback => {
            if (callback.hasError) {
              disableEappButton(false);
              if (callback.errorMsg) {
                setTimeout(() => {
                  Alert.alert(`[Error] ${callback.errorMsg}`);
                }, 200);
              } else {
                setTimeout(() => {
                  Alert.alert(
                    `error msg: Authorization for submission Failed (Error 001)`
                  );
                }, 200);
              }
            } else {
              const { isOnlineAuthCompleted } = this.props;
              if (isOnlineAuthCompleted) {
                this.checkIfDataSyncNeeded();
              }
            }
          }
        });
      }
    }
  }

  callSubmissionApi() {
    const {
      callApiSubmitApplication,
      application,
      dataSync,
      disableEappButton
    } = this.props;
    let { lang } = this.state;
    switch (lang) {
      case "ENGLISH":
        lang = "en";
        break;
      case "TRADITIONAL_CHINESE":
        lang = "zh-Hant";
        break;
      default:
        lang = "en";
        break;
    }

    callApiSubmitApplication({
      docId: application.id,
      lang,
      callback: submitResponse => {
        if (!submitResponse[0].hasError) {
          this.setState(
            {
              workType: IS_AFTER_SUBMIT,
              showLoading: false
            },
            () => {
              setTimeout(() => {
                dataSync();
              }, 200);
            }
          );
        } else {
          // Submission is not successfully
          Alert.alert(`[Error] ${submitResponse[0].errorMsg}`, null, [
            {
              text: "OK",
              onPress: () => {
                disableEappButton(false);
                this.setState({
                  showLoading: false
                });
              }
            }
          ]);
        }
      }
    });
  }

  checkIsSubmitButtonDisable() {
    const { payment, application, agent, isDisableEappButton } = this.props;
    const { isNetworkConnected } = this.state;
    if (isDisableEappButton) {
      return true;
    } else if (!isNetworkConnected) {
      return true;
    } else if (!application.isMandDocsAllUploaded) {
      return true;
    } else if (payment.totCashPortion && payment.totCashPortion > 0) {
      if (
        ["crCard", "eNets", "dbsCrCardIpp"].indexOf(payment.initPayMethod) >=
          0 &&
        this.props.onlinePaymentStatus.trxStatusRemark !== "Y"
      ) {
        return true;
      } else if (!payment.initPayMethod || payment.initPayMethod === "") {
        return true;
      } else if (payment.initPayMethod === "teleTransfter") {
        if (!payment.ttRemittingBank || !payment.ttDOR || !payment.ttRemarks) {
          return true;
        }
      }
    } else if (agent.role !== "04" && !agent.managerCode) {
      return true;
    }
    return false;
  }

  render() {
    return (
      <View>
        <EABHUD isOpen={this.state.showLoading} />
        {this.getSubmissionSection()}
      </View>
    );
  }
}

SubmissionShield.propTypes = {
  getApplicationList: PropTypes.func.isRequired,
  setApiResponsePage: PropTypes.func.isRequired,
  continueApplicationFormShield: PropTypes.func.isRequired,
  isOnlineAuthCompleted: PropTypes.bool.isRequired,
  setLastDataSyncTime: PropTypes.func.isRequired,
  dataSync: PropTypes.func.isRequired,
  callApiSubmitApplication: PropTypes.func.isRequired,
  setOnlineAuthCodeForPayment: PropTypes.func.isRequired,
  isHandlingListener: PropTypes.bool.isRequired,
  checkTokenExpiryDate: PropTypes.func.isRequired,
  loginByOnlineAuthForPayment: PropTypes.func.isRequired,
  apiPage: PropTypes.string.isRequired,
  textStore: APP_REDUCER_TYPE_CHECK[TEXT_STORE].isRequired,
  application:
    REDUCER_TYPE_CHECK[REDUCER_TYPES.PRE_APPLICATION].ApplicationItem.value
      .isRequired,
  payment:
    REDUCER_TYPE_CHECK[REDUCER_TYPES.PAYMENT_AND_SUBMISSION].payment.isRequired,
  environment: PropTypes.string.isRequired,
  onlinePaymentStatus: PropTypes.oneOfType([PropTypes.object]).isRequired,
  agent: PropTypes.oneOfType([PropTypes.object]).isRequired,
  authToken: PropTypes.string.isRequired,
  agentCode: PropTypes.string.isRequired,
  lastDataSyncTime: PropTypes.string.isRequired,
  isDisableEappButton: PropTypes.bool.isRequired,
  disableEappButton: PropTypes.func.isRequired
};

export default SubmissionShield;
