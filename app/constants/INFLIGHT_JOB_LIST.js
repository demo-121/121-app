import moment from "moment";

export const INFLIGHT_JOB_LIST_FILE_NAME = "inflightJobList";
export const IMMEDIATE = "immediate";
export const SCHEDULED = "scheduled";

// this data will save into couchbase. so need to use object structure.
const list = [
  {
    jobName: "generalEndFundInvalidate",
    buildNumber: "0.0.0",
    versionNumber: "000",
    effectiveType: IMMEDIATE,
    executionTime: moment().valueOf(),
    isExecuted: false
  },
  {
    jobName: "generalInvalidation",
    buildNumber: "0.0.0",
    versionNumber: "000",
    effectiveType: IMMEDIATE,
    executionTime: moment().valueOf(),
    isExecuted: false
  }
];
const convertedJobList = {};
list.forEach(job => {
  convertedJobList[job.jobName] = job;
});
export default {
  data: convertedJobList,
  messages: []
};
