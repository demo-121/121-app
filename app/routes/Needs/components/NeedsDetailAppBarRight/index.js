import React, { Component } from "react";
import PropTypes from "prop-types";
import { View } from "react-native";
import EABButton from "../../../../components/EABButton";
import EABHUD from "../../../../containers/EABHUD";
import TranslatedText from "../../../../containers/TranslatedText";
import Theme from "../../../../theme";
import styles from "./styles";
import EABDialog from "../../../../components/EABDialog";
import * as DIALOG_TYPES from "../../../../components/EABDialog/constants";
import FNAReport from "../../containers/FNAReport";

/**
 * NeedsDetailAppBarRight
 * @description needs detail page app bar right.
 * */
export default class NeedsDetailAppBarRight extends Component {
  constructor() {
    super();

    this.state = {
      isLoadingReport: false,
      openPdf: false
    };
  }

  render() {
    // =========================================================================
    // variables
    // =========================================================================
    const { FNAReportInitialize, navigation } = this.props;
    const { isLoadingReport } = this.state;

    return (
      <View style={styles.container}>
        <EABDialog type={DIALOG_TYPES.SCREEN} isOpen={this.state.openPdf}>
          <FNAReport
            closePdf={() => {
              this.setState({
                openPdf: false
              });
            }}
          />
        </EABDialog>
        <EABButton
          style={styles.buttonFirst}
          onPress={async () => {
            await this.setState({ isLoadingReport: true });

            FNAReportInitialize(async () => {
              await this.setState({
                isLoadingReport: false,
                openPdf: true
              });
            });
          }}
        >
          <TranslatedText
            style={Theme.textButtonLabelNormalAccent}
            path="needs.view.fna.viewReport"
          />
        </EABButton>
        <EABButton
          onPress={() => {
            navigation.navigate("FNASummary");
          }}
        >
          <TranslatedText
            style={Theme.textButtonLabelNormalAccent}
            path="needs.view.fna.summary"
          />
        </EABButton>
        <EABHUD isOpen={isLoadingReport} />
      </View>
    );
  }
}

NeedsDetailAppBarRight.propTypes = {
  FNAReportInitialize: PropTypes.func.isRequired
};
