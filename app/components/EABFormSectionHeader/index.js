import PropTypes from "prop-types";
import React, { Component } from "react";
import { Text, View } from "react-native";
import getStyleArray from "../../utilities/getStyleArray";
import styles from "./styles";
import PopoverTooltip from "../../components/PopoverTooltip/PopoverTooltipIOS";

/**
 * <EABFormSectionHeader />
 * @description form section header component
 * @requires icInfoActive - assets/images/icons/icInfoActive.png
 * @requires icMandatoryArrow - assets/images/icons/icMandatoryArrow.png
 * @param {string} title - header title
 * @param {boolean} [isRequired=false] - indicates whether the section is required or not
 * @param {boolean} [shouldShowInfo=false] - indicates whether should show info icon or not
 * @param {number} [activeOpacity=.5] - the opacity value when the button is pressed
 * @param {object|number=} style - container style.
 * */
class EABFormSectionHeader extends Component {
  get styleArray() {
    const { style } = this.props;
    return getStyleArray(style);
  }

  get rootStyles() {
    const { container } = styles;
    return [container, this.styleArray];
  }

  render() {
    /**
     * functions
     * */
    const renderTitle = () => {
      const { title, isRequired, testID } = this.props;
      const { title: titleStyle } = styles;
      return (
        <View>
          <Text testID={testID} style={titleStyle}>
            {title}
            {isRequired ? <Text style={styles.requiredHint}> *</Text> : null}
          </Text>
        </View>
      );
    };

    const renderShowInfoIcon = () => {
      const { tooltipText, tooltipOptions } = this.props;
      if (tooltipText === "") return null;
      return (
        <PopoverTooltip popoverOptions={tooltipOptions} text={tooltipText} />
      );
    };

    /* TODO need tooltip component */
    return (
      <View style={this.rootStyles}>
        {renderTitle()}
        {renderShowInfoIcon()}
      </View>
    );
  }
}

EABFormSectionHeader.propTypes = {
  title: PropTypes.string.isRequired,
  isRequired: PropTypes.bool,
  tooltipText: PropTypes.string,
  tooltipOptions: PropTypes.string,
  style: PropTypes.oneOfType([
    PropTypes.number,
    PropTypes.object,
    PropTypes.array
  ]),
  testID: PropTypes.string
};

EABFormSectionHeader.defaultProps = {
  isRequired: false,
  tooltipText: "",
  tooltipOptions: "",
  style: {},
  testID: ""
};

export default EABFormSectionHeader;
