import PropTypes from "prop-types";
import React, { PureComponent } from "react";
import { Image } from "react-native";
import icWarning from "../../assets/images/icWarning.png";
import styles from "./styles";

/**
 * <FnaWarningIcon />
 * @description FnaWarningIcon
 * @param {boolean} isFnaInvalid - indicates whether the warning icon is show or not
 * @param {any} [imageSource='assets/images/icons/icWarning.png'] - image source
 * */
export default class FnaWarningIcon extends PureComponent {
  render() {
    const { isFnaInvalid } = this.props;
    return isFnaInvalid && <Image style={styles.image} source={icWarning} />;
  }
}

FnaWarningIcon.propTypes = {
  isFnaInvalid: PropTypes.bool
};

FnaWarningIcon.defaultProps = {
  isFnaInvalid: false
};
