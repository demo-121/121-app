import React, { PureComponent } from "react";
import { Alert } from "react-native";
import PropTypes from "prop-types";
import { TEXT_STORE } from "../../../../constants/REDUCER_TYPES";
import EABButton from "../../../../components/EABButton";
import TranslatedText from "../../../../containers/TranslatedText";
import { ContextConsumer } from "../../../../context";

import Theme from "../../../../theme";
import EABi18n from "../../../../utilities/EABi18n";
import APP_REDUCER_TYPE_CHECK from "../../../../constants/REDUCER_TYPE_CHECK";

export default class ApplicationDeleteButton extends PureComponent {
  render() {
    const {
      navigation,
      setEappLoading,
      textStore,
      deleteApplications
    } = this.props;
    const selectedIdCount = navigation.getParam("selectedIdCount");
    const containFullySignedCase = navigation.getParam(
      "containFullySignedCase"
    );

    return (
      <ContextConsumer>
        {({ language }) => (
          <EABButton
            style={{
              marginLeft: Theme.alignmentXL
            }}
            isDisable={selectedIdCount === 0 || containFullySignedCase}
            onPress={() => {
              Alert.alert(
                EABi18n({
                  path: "application.deleteApplication",
                  language,
                  textStore
                }),
                null,
                [
                  {
                    text: EABi18n({
                      path: "button.no",
                      language,
                      textStore
                    }),
                    onPress: () => false,
                    style: "cancel"
                  },
                  {
                    text: EABi18n({
                      path: "button.yes",
                      language,
                      textStore
                    }),
                    onPress: () => {
                      setEappLoading(true);
                      deleteApplications(
                        setEappLoading(false, () => {
                          setTimeout(() => {
                            navigation.setParams({ selectModeOn: false });
                          }, 300);
                        })
                      );
                    }
                  }
                ]
              );
            }}
          >
            <TranslatedText
              style={Theme.textButtonLabelNormalAccent}
              path="button.delete"
            />
          </EABButton>
        )}
      </ContextConsumer>
    );
  }
}

ApplicationDeleteButton.propTypes = {
  textStore: APP_REDUCER_TYPE_CHECK[TEXT_STORE].isRequired,
  setEappLoading: PropTypes.func.isRequired,
  deleteApplications: PropTypes.func.isRequired
};
