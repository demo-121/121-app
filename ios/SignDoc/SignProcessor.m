#include <string.h>
#include <errno.h>
#include "SignDocSDK-c.h"
#include "SignProcessor.h"
#import <React/RCTLog.h>
#include "SignDocEventEmitter.h"

static struct SIGNDOC_Exception *ex = NULL;
static struct SIGNDOC_DocumentHandler *handler;
static struct SIGNDOC_DocumentLoader *loader;
static struct SIGNDOC_Document *doc;
static const char *test_output_file;
static BOOL isSignProcessorInit = NO;

static void doThrow () {
  @throw [NSException exceptionWithName:@"SignDocException" reason:@"SignDoc error" userInfo:nil];
}

static NSString *signWord;
static NSString *signWordOrder;

static void handleException (struct SIGNDOC_Exception **aEx) {
  unsigned type = SIGNDOC_Exception_getType (*aEx);
  const char *text = SIGNDOC_Exception_getText (*aEx);
  if (text != NULL) {
    struct SIGNDOC_Exception *ex2 = NULL;
    char *t = SIGNDOC_recodeStringRelaxed (&ex2, SIGNDOC_ENCODING_UTF_8, text, SIGNDOC_ENCODING_NATIVE);
    if (ex2 == NULL) {
      printf ("SignDoc exception: %s\n", t);
      SIGNDOC_free (t);
    } else {
      NSLog (@"SignDoc exception: %u (%s)", SIGNDOC_Exception_getType (ex2), text);
      SIGNDOC_Exception_delete (ex2);
    }
  } else {
    printf ("SignDoc exception type: %u\n", type);
  }
  SIGNDOC_Exception_delete (*aEx);
  *aEx = NULL;
  doThrow ();
}

static void failAlloc (const char *fun) {
  NSLog (@"%s() failed", fun);
  doThrow ();
}


static void failLoader (struct SIGNDOC_DocumentLoader *loader, const char *fun) {
  const char *txt = SIGNDOC_DocumentLoader_getErrorMessage (&ex, loader, SIGNDOC_ENCODING_NATIVE);
  NSLog (@"%s() failed, %s", fun, txt);
  RCTLog (@"%s() failed, %s", fun, txt);
  doThrow ();
}

static void failDoc (struct SIGNDOC_Document *doc, int rc, const char *fun) {
  const char *txt = SIGNDOC_Document_getErrorMessage (&ex, doc, SIGNDOC_ENCODING_NATIVE);
  NSLog (@"%s() failed, %d, %s", fun, rc, txt);
  RCTLog (@"%s() failed, %d, %s", fun, rc, txt);
  doThrow ();
}

static void checkDocumentRC (struct SIGNDOC_Document *doc, int rc, const char *fun) {
  if (rc == SIGNDOC_SIGNATUREPARAMETERS_RETURNCODE_OK) return;
  failDoc(doc, rc, fun);
}

static void failVer (struct SIGNDOC_VerificationResult *ver, int rc, const char *fun) {
  const char *txt = SIGNDOC_VerificationResult_getErrorMessage (&ex, ver, SIGNDOC_ENCODING_NATIVE);
  NSLog (@"%s() failed, %d, %s", fun, rc, txt);
  doThrow ();
}

static void checkParam (struct SIGNDOC_SignatureParameters *params, int rc, const char *fun) {
  if (rc == SIGNDOC_SIGNATUREPARAMETERS_RETURNCODE_OK)
    return;
  const char *txt = SIGNDOC_SignatureParameters_getErrorMessage (&ex, params, SIGNDOC_ENCODING_NATIVE);
  NSLog (@"%s() failed, %d, %s", fun, rc, txt);
  RCTLog (@"%s() failed, %d, %s", fun, rc, txt);
  doThrow ();
}

unsigned char *readFile (const char *path, size_t *size) {
  FILE *f = fopen (path, "rb");
  if (f == NULL) {
    NSLog (@"%s: %s", path, strerror (errno));
    doThrow ();
  }
  fseek (f, 0, SEEK_END);
  long len = ftell (f);
  fseek (f, 0, SEEK_SET);
  *size = (size_t)len;
  unsigned char *p = (unsigned char *)malloc (*size);
  if (p == NULL) {
    NSLog (@"readFile(): out of memory");
    fclose (f);
    doThrow ();
  }
  size_t n = fread (p, 1, *size, f);
  if (ferror (f)) {
    NSLog (@"%s: %s", path, strerror (errno));
    fclose (f);
    doThrow ();
  }
  if (n != *size) {
    NSLog (@"readFile(): short read");
    fclose (f);
    doThrow ();
  }
  if (fclose (f) != 0) {
    NSLog (@"%s: %s", path, strerror (errno));
    doThrow ();
  }
  return p;
}

static void writeFile (const char *path, const void *ptr, size_t size) {
  FILE *f = fopen (path, "wb");
  if (f == NULL) {
    NSLog (@"%s: %s", path, strerror (errno));
    doThrow ();
  }
  fwrite (ptr, 1, size, f);
  if (ferror (f) || fflush (f) != 0) {
    NSLog (@"%s: %s", path, strerror (errno));
    fclose (f);
    doThrow ();
  }
  if (fclose (f) != 0) {
    NSLog (@"%s: %s", path, strerror (errno));
    doThrow ();
  }
}

BOOL initSignProcess (double signX, double signY, NSString *pdfFilePath, NSString *pdfFileName, NSString *word, NSString *order) {
  signWord = word;
  signWordOrder =  order;
  @try {
    docFilePath = pdfFilePath;
    docFileName = pdfFileName;
    //docSignFieldX = signX;
    //docSignFieldY = signY;
    NSLog(@"initSignProcess is called");
    NSLog(@"Input SingX: %g | SignY: %g", signX, signY);
    //    NSArray *doc_paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    //    doc_path = [doc_paths objectAtIndex:0];
    SIGNDOC_Exception_setHandler (handleException);
    
    // demo license
    //      const void* license = "h:SPLM2 4.10\ni:ID:9923379\ni:Product:SignDocSDK\ni:Manufacturer:SOFTPRO GmbH\ni:Customer:INTERNAL DEMO LICENSE - PRODUCTION USE PROHIBITED\ni:Version:6\ni:OS:all\na:product:unlimited\na:signware:unlimited\na:sign:2018-12-31\na:capture:unlimited\na:render:unlimited\ns:2f0f7fa819f40d0b491443f10ee502aac4d4880203f3dbc879b8601a3a736029\ns:74ed9fe6e023946a10e749130e4494d5f2473fd74cd8de18fd7f4415bc006d69\ns:7947b8f9b5a184edff4ccceffea6ad0ce9260281cb89168647644307b24a96de\ns:b652aedcc54636138ec7c6b9ce4cd89d8ca8901b423303209119726b2329d3e6\n";
    
    // EAB license
    //    const void* license = "h:SPLM2 4.10\ni:ID:13739\ni:Product:SignDocSDK\ni:Manufacturer:SOFTPRO GmbH\ni:Customer:EAB Systems (Hong Kong) Limited\ni:Version:99\ni:OS:all\na:product:unlimited\na:signware:unlimited\na:dynamic:unlimited\na:static:unlimited\na:sign:2018-08-17\na:capture:unlimited\na:render:unlimited\nps:CustomerCompany:EAB Systems (Hong Kong) Limited\nps:CustomerFirstName:EAB Systems (Hong Kong) Limited\nps:CustomerLastName:EAB Systems (Hong Kong) Limited\nps:CustomerMail:apjdealdesk@kofax.com, ranajit.chatterjee@kofax.com\ns:c09aec27b5e2765eac5ac5847cc5dd43dfd6a65f52804ccad78c5932db55f9a3\ns:573f843b0cdc6a21723f3ed092f0ecd65603de70276028a084fac5564235827c\ns:a8561dbae52b7439254006785df59be72499c71587b2074deebdeb7ec69cb78d\ns:3e40a18bc6a25c7d80dfe7a547dc3cde10531fe661bef953108a77672ac02840\n";
    NSBundle* bundle = [NSBundle mainBundle];
    NSError *error;
    NSString *initFilePath = [bundle pathForResource:@"SignDocSDK" ofType:@"key"];
    NSString *initFileContents = [NSString stringWithContentsOfFile:initFilePath encoding:NSUTF8StringEncoding error:&error];
//    NSLog(@"%@", initFileContents);
    
    
//    const void* license = "h:SPLM2 4.10\ni:ID:26685i:Product:SignDocSDK\ni:Manufacturer:SOFTPRO GmbH\ni:Customer:EAB Systems (Hong Kong) Limited\ni:Version:99\ni:OS:all\na:product:unlimited\na:signware:unlimited\na:dynamic:unlimited\na:static:unlimited\na:sign:2019-08-30\na:capture:unlimited\na:render:unlimited\nps:CustomerCompany:EAB Systems (Hong Kong) Limited\nps:CustomerFirstName:EAB Systems (Hong Kong) Limited\nps:CustomerLastName:EAB Systems (Hong Kong) Limited\nps:CustomerMail:SSR.APAC@kofax.com, wade.poh@kofax.com, Rachael.Soh@kofax.com;peddi@eabsystems.com\ns:262135c7955bd33a83d9eb606828a66cd56f136c36dc21a081a1dee3497c2bcc\ns:323af64a6484e5998db204eaf385b341764a36b281954de690e1c2490bf73302\ns:828c7b79ba097547ed9cdf30900b38a883fda3843e56c049a73132e7e898f877\ns:8f31aaf55dc22e75d7f60a5a5b5041b78ebd9b96460463de6d766c2335d1f502\n";
    
    const void* license = (void *)[initFileContents UTF8String];
    
    NSLog(@"Length of license: %lu", strlen(license));
    BOOL setResult = SIGNDOC_DocumentLoader_setLicenseKey(&ex, license, strlen(license), NULL, NULL, NULL, 0);
    if (!setResult) {
      NSLog (@"Cannot initialize license");
      doThrow ();
    }
    
    // Create loader
    
    loader = SIGNDOC_DocumentLoader_new (&ex);
    handler = SIGNDOC_PdfDocumentHandler_new (&ex);
    if (!SIGNDOC_DocumentLoader_registerDocumentHandler (&ex, loader, handler))
      failLoader (loader, "SIGNDOC_DocumentLoader_registerDocumentHandler");
    
    // Load document
    
    //  NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    //    NSString *documentsDirectory = [paths objectAtIndex:0];
    //    NSString *documentPath = [documentsDirectory stringByAppendingPathComponent:outputDocumentName];
    //    const char *input_path4 = [documentPath UTF8String];
    NSString *documentPath = [docFilePath stringByAppendingPathComponent:docFileName];
    const char *input_path4 = [documentPath UTF8String];
    NSLog (@"Input file: %s", input_path4);
    
    doc = SIGNDOC_DocumentLoader_loadFromFile (&ex, loader, SIGNDOC_ENCODING_NATIVE, input_path4, SIGNDOC_TRUE);
    if (doc == NULL) {
      failLoader (loader, "SIGNDOC_DocumentLoader_loadFromFile");
    }
    isSignProcessorInit = YES;
    
  } @catch (NSException *e) {
    NSLog(@"Exception: {name: %@, reason: %@}", [e name], [e reason]);
    isSignProcessorInit = NO;
  }
  return isSignProcessorInit;
}

struct SIGNDOC_ByteArray *renderTest(unsigned int height) {
  NSLog(@"renderTest is called");
  if(isSignProcessorInit) {
    struct SIGNDOC_ByteArray *blob;
    struct SIGNDOC_RenderParameters *rp;
    struct SIGNDOC_RenderOutput *ro;
    
    blob = SIGNDOC_ByteArray_new (&ex);
    if (blob == NULL)
      failAlloc ("SIGNDOC_ByteArray_new");
    rp = SIGNDOC_RenderParameters_new (&ex);
    ro = SIGNDOC_RenderOutput_new (&ex);
    
    SIGNDOC_RenderParameters_setFormat (&ex, rp, "png");
    SIGNDOC_RenderParameters_setPage (&ex, rp, 1);
    SIGNDOC_RenderParameters_fitHeight (&ex, rp, height);
    struct SIGNDOC_Rect *clip = NULL;
    
    int rc = SIGNDOC_Document_renderPageAsImage (&ex, doc, blob, ro, rp, clip);
    if (rc != SIGNDOC_DOCUMENT_RETURNCODE_OK) {
      failDoc (doc, rc, "SIGNDOC_Document_renderPageAsImage");
    }
    else {
      NSLog (@"renderTest");
      NSLog (@"Image width:  %d", SIGNDOC_RenderOutput_getWidth (ro));
      NSLog (@"Image height: %d", SIGNDOC_RenderOutput_getHeight (ro));
    }
    
    if (ro != NULL)
      SIGNDOC_RenderOutput_delete (ro);
    if (rp != NULL)
      SIGNDOC_RenderParameters_delete (rp);
    
    return blob;
  } else {
    NSLog (@"SignProcessor isn't init");
  }
  return nil;
}

UIImage *getPageImage(unsigned int height, unsigned int width, CGFloat x, CGFloat y) {
  NSLog(@"getPageImage is called");
  struct SIGNDOC_ByteArray *blob;
  struct SIGNDOC_RenderParameters *rp;
  struct SIGNDOC_RenderOutput *ro;
  
  if(isSignProcessorInit) {
    blob = SIGNDOC_ByteArray_new (&ex);
    if (blob == NULL)
      failAlloc ("SIGNDOC_ByteArray_new");
    rp = SIGNDOC_RenderParameters_new (&ex);
    ro = SIGNDOC_RenderOutput_new (&ex);
    
    SIGNDOC_RenderParameters_setFormat (&ex, rp, "png");
    SIGNDOC_RenderParameters_setPage (&ex, rp, 1);
    SIGNDOC_RenderParameters_fitHeight (&ex, rp, height);
    
    struct SIGNDOC_Rect *clip = SIGNDOC_Rect_newXY(&ex,
                                                   width * x,
                                                   height - (height * (y + height)),
                                                   width * (x + width),
                                                   height - (height * y));
    
    int rc = SIGNDOC_Document_renderPageAsImage (&ex, doc, blob, ro, rp, clip);
    if (rc != SIGNDOC_DOCUMENT_RETURNCODE_OK) {
      failDoc (doc, rc, "SIGNDOC_Document_renderPageAsImage");
    }
    else {
      NSLog (@"getPageImage");
      NSLog (@"Image width:  %d", SIGNDOC_RenderOutput_getWidth (ro));
      NSLog (@"Image height: %d", SIGNDOC_RenderOutput_getHeight (ro));
    }
    
    if (ro != NULL)
      SIGNDOC_RenderOutput_delete (ro);
    if (rp != NULL)
      SIGNDOC_RenderParameters_delete (rp);
    NSData *data = [NSData dataWithBytes:SIGNDOC_ByteArray_data(blob) length:SIGNDOC_ByteArray_count(blob)];
    UIImage *resultImage = [UIImage imageWithData:data];
    SIGNDOC_ByteArray_delete(blob);
    return resultImage;
  } else {
    NSLog (@"SignProcessor isn't init");
    return nil;
  }
}

UIImage * getSignatureImage(unsigned int height, CGRect frame) {
  NSLog(@"getSignatureImage is called");
  if(isSignProcessorInit) {
    struct SIGNDOC_ByteArray *blob;
    struct SIGNDOC_RenderParameters *rp;
    struct SIGNDOC_RenderOutput *ro;
    
    blob = SIGNDOC_ByteArray_new (&ex);
    if (blob == NULL)
      failAlloc ("SIGNDOC_ByteArray_new");
    rp = SIGNDOC_RenderParameters_new (&ex);
    ro = SIGNDOC_RenderOutput_new (&ex);
    
    SIGNDOC_RenderParameters_setFormat (&ex, rp, "png");
    SIGNDOC_RenderParameters_setPage (&ex, rp, 1);
    SIGNDOC_RenderParameters_fitHeight (&ex, rp, height);
    struct SIGNDOC_Rect *clipForRenderPageAsImage = SIGNDOC_Rect_newXY(&ex, frame.origin.x, frame.size.height, frame.origin.y, frame.size.width);
    
    int rc = SIGNDOC_Document_renderPageAsImage (&ex, doc, blob, ro, rp, clipForRenderPageAsImage);
    if (rc != SIGNDOC_DOCUMENT_RETURNCODE_OK) {
      failDoc (doc, rc, "SIGNDOC_Document_renderPageAsImage");
    } else {
      NSLog (@"getSignatureImage");
      NSLog (@"Image width:  %d", SIGNDOC_RenderOutput_getWidth (ro));
      NSLog (@"Image height: %d", SIGNDOC_RenderOutput_getHeight (ro));
    }
    NSData *data = [NSData dataWithBytes:SIGNDOC_ByteArray_data(blob) length:SIGNDOC_ByteArray_count(blob)];
    UIImage *signatureImage = [UIImage imageWithData:data];
    SIGNDOC_ByteArray_delete(blob);
    SIGNDOC_Rect_delete(clipForRenderPageAsImage);
    if (ro != NULL)
      SIGNDOC_RenderOutput_delete (ro);
    if (rp != NULL)
      SIGNDOC_RenderParameters_delete (rp);
    return signatureImage;
  } else {
    NSLog (@"SignProcessor isn't init");
    return nil;
  }
}

void saveDocument() {
  NSLog(@"processor saveDocument is called");
  if(isSignProcessorInit) {
    SIGNDOC_Document_saveToFile(&ex, doc, SIGNDOC_ENCODING_NATIVE, NULL, SIGNDOC_DOCUMENT_SAVEFLAGS_INCREMENTAL);
    SIGNDOC_Exception_delete(ex);
  } else {
    NSLog (@"SignProcessor isn't init");
  }
}

int *getSignDocPointPageByFindTextByWordAndOrder(const char *text, NSString *order) {
  struct SIGNDOC_FindTextPositionArray *pos = NULL;
  pos = SIGNDOC_FindTextPositionArray_new (&ex);
  if (pos == NULL) {
    failAlloc ("SIGNDOC_FindTextPositionArray_new");
  }
  int find_text_flags = 0;
  int rc = SIGNDOC_Document_findText (&ex, doc, SIGNDOC_ENCODING_NATIVE, 1, SIGNDOC_Document_getPageCount(&ex, doc), text, find_text_flags, pos);
  if (rc != SIGNDOC_DOCUMENT_RETURNCODE_OK) {
    failDoc (doc, rc, "SIGNDOC_Document_findText");
  }
  
  if (SIGNDOC_FindTextPositionArray_count (pos) >= 1) {
    struct SIGNDOC_FindTextPosition *pos2 = SIGNDOC_FindTextPositionArray_at (pos, [order integerValue]);
    struct SIGNDOC_CharacterPosition *pos3 = SIGNDOC_FindTextPosition_getLast (pos2);
    return SIGNDOC_CharacterPosition_getPage(pos3);
  }
  return nil;
}

struct SIGNDOC_Point *getSignDocPointByFindTextByWordAndOrder(const char *text, NSString *order) {
  struct SIGNDOC_FindTextPositionArray *pos = NULL;
  pos = SIGNDOC_FindTextPositionArray_new (&ex);
  if (pos == NULL) {
    failAlloc ("SIGNDOC_FindTextPositionArray_new");
  }
  int find_text_flags = 0;
  int rc = SIGNDOC_Document_findText (&ex, doc, SIGNDOC_ENCODING_NATIVE, 1, SIGNDOC_Document_getPageCount(&ex, doc), text, find_text_flags, pos);
  if (rc != SIGNDOC_DOCUMENT_RETURNCODE_OK) {
    failDoc (doc, rc, "SIGNDOC_Document_findText");
  }
  
  if (SIGNDOC_FindTextPositionArray_count (pos) >= 1) {
    struct SIGNDOC_FindTextPosition *pos2 = SIGNDOC_FindTextPositionArray_at (pos, [order integerValue]);
    struct SIGNDOC_CharacterPosition *pos3 = SIGNDOC_FindTextPosition_getLast (pos2);
    struct SIGNDOC_Point *pos4 = SIGNDOC_CharacterPosition_getRef (pos3);
    double x = SIGNDOC_Point_getX (pos4);
    double y = SIGNDOC_Point_getY (pos4);
    NSLog(@"x=%f",x);
    NSLog(@"y=%f", y);
    return pos4;
  }
  return nil;
}

int getSignDocPointByFindText(const char *text) {
  struct SIGNDOC_FindTextPositionArray *pos = NULL;
  pos = SIGNDOC_FindTextPositionArray_new (&ex);
  if (pos == NULL) {
    failAlloc ("SIGNDOC_FindTextPositionArray_new");
  }
  int find_text_flags = 0;
  int rc = SIGNDOC_Document_findText (&ex, doc, SIGNDOC_ENCODING_NATIVE, 1, SIGNDOC_Document_getPageCount(&ex, doc), text, find_text_flags, pos);
  if (rc != SIGNDOC_DOCUMENT_RETURNCODE_OK) {
    failDoc (doc, rc, "SIGNDOC_Document_findText");
  }
  
  NSLog (@"Text positions: %u", SIGNDOC_FindTextPositionArray_count (pos));
//  if (SIGNDOC_FindTextPositionArray_count (pos) >= 1) {
//    struct SIGNDOC_FindTextPosition *pos2 = SIGNDOC_FindTextPositionArray_at (pos, 0);
//    struct SIGNDOC_CharacterPosition *pos3 = SIGNDOC_FindTextPosition_getLast (pos2);
//    struct SIGNDOC_Point *pos4 = SIGNDOC_CharacterPosition_getRef (pos3);
//    double x = SIGNDOC_Point_getX (pos4);
//    double y = SIGNDOC_Point_getY (pos4);
//    return pos4;
//  }
  return SIGNDOC_FindTextPositionArray_count (pos);
//  int i = 0;
//  int x;
//  int y;
//  struct SIGNDOC_FindTextPosition *pos2;
//  for(;i < SIGNDOC_FindTextPositionArray_count(pos);i++){
//    pos2 = SIGNDOC_FindTextPositionArray_at (pos, i);
//    if(pos2 != nil) {
//      x = SIGNDOC_Point_getX (pos2);
//      y = SIGNDOC_Point_getY (pos2);
//    }
//    NSLog (@"inside x=%g, y=%g", x, y);
//
//  }
//  return nil;
}

NSMutableArray *startFindTexts(NSMutableArray *texts){
  NSMutableArray *textArr = [[NSMutableArray alloc] init];
  struct SIGNDOC_FindTextPositionArray *pos = SIGNDOC_FindTextPositionArray_new (&ex);
  int i = 0;
  int x;
  
  for(;i < [texts count]; i++){
    char *currentText = [[texts objectAtIndex:i][@"searchTag"] UTF8String];
    int positions = getSignDocPointByFindText(currentText);
    int j = 0;
    for(;j < positions; j++){
      [textArr addObject: @{
        @"key": [[NSString alloc] initWithUTF8String:currentText], 
        @"name":[texts objectAtIndex:i][@"name"],
        @"page":  [NSString stringWithFormat:@"%d",j],
        @"signX": [texts objectAtIndex:i][@"width"],
        @"signY": [texts objectAtIndex:i][@"height"],
        }];
    }
  }
  return textArr;
}

NSMutableArray *startConvertPdfToPng(NSString *path) {
  int height = 1024;
  int pageCount = SIGNDOC_Document_getPageCount(&ex, doc);
  
  UIImage *signatureImage;
  NSMutableArray *images = [[NSMutableArray alloc] init];
  for(int i = 1; i <= pageCount; i++){
    struct SIGNDOC_ByteArray *blob;
    struct SIGNDOC_RenderParameters *rp;
    struct SIGNDOC_RenderOutput *ro;
    blob = SIGNDOC_ByteArray_new (&ex);
    if (blob == NULL)
      RCTLog(@"SIGNDOC_ByteArray_new");
    rp = SIGNDOC_RenderParameters_new (&ex);
    ro = SIGNDOC_RenderOutput_new (&ex);
    SIGNDOC_RenderParameters_setFormat (&ex, rp, "jpg");
    SIGNDOC_RenderParameters_setPage(&ex, rp, i);
    SIGNDOC_RenderParameters_fitHeight (&ex, rp, height);
    //  struct SIGNDOC_Rect *clip = NULL;
    
    int rc = SIGNDOC_Document_renderPageAsImage (&ex, doc, blob, ro, rp, NULL);
    if (rc != SIGNDOC_DOCUMENT_RETURNCODE_OK) {
      RCTLog(@"Fail.....SIGNDOC_Document_renderPageAsImage");
    }
    else {
      RCTLog (@"renderTest");
      RCTLog (@"Image width:  %d", SIGNDOC_RenderOutput_getWidth (ro));
      RCTLog (@"Image height: %d", SIGNDOC_RenderOutput_getHeight (ro));
    }
    
    NSData *data = [NSData dataWithBytes:SIGNDOC_ByteArray_data(blob) length:SIGNDOC_ByteArray_count(blob)];
    //    NSString *base64Data = [data base64EncodedStringWithOptions:NSDataBase64EncodingEndLineWithLineFeed];
    signatureImage = [UIImage imageWithData:data];
    [images addObject: [UIImagePNGRepresentation(signatureImage) base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength]];
    SIGNDOC_ByteArray_delete(blob);
    //  SIGNDOC_Rect_delete(clipForRenderPageAsImage);
    
    if (ro != NULL)
      SIGNDOC_RenderOutput_delete (ro);
    if (rp != NULL)
      SIGNDOC_RenderParameters_delete (rp);
  }
  
  NSLog(@"Array length: %d", [images count]);
  return images;
}

int signProcess (const char *field_name_sig, unsigned char *signature_data, size_t signature_size, unsigned char *signature_img, size_t image_size, BOOL selfSigned, double signX, double signY) {
  @try{
    if(isSignProcessorInit) {
      int rc;
      struct SIGNDOC_SignatureParameters *sp = NULL;
      double x,y;
      // Insert signature field
      // x = docSignFieldX;
      // y = docSignFieldY;
      struct SIGNDOC_Point *pos = getSignDocPointByFindTextByWordAndOrder([signWord UTF8String], signWordOrder);
      if(pos != nil) {
        x = SIGNDOC_Point_getX (pos);
        y = SIGNDOC_Point_getY (pos);
        
        // Changed to reading parameter value
        x = x+signX;
        y = y+signY;
        //y = y + 80;
      // NSLog (@"field_name_sig =%@", @(field_name_sig) );
        
        NSLog (@"signProcess: Padding Info: X: %f |Y: %f", signX, signY);
        
        /**
        if ([signWordOrder isEqualToString :@"0"]) {
          if ([@(field_name_sig) isEqualToString:@"ADVISOR_SIGNATURE"] && x<110) {
            x = x - 60;
          } else if ([@(field_name_sig) isEqualToString:@"PROP_SIGNATURE"] && x<500 ) {
           x = x + 40;
          } else if ([@(field_name_sig) isEqualToString:@"TI_SIGNATURE"] && x>200 ) {
           x = x - 25;
          }
        } else {
          x = x - 30;
       }
         */
        
      }
        
      NSLog (@" x=%g, y=%g", x, y);

      NSLog(@">>>>>>>>>>>>> signWord=%@",signWord);
      NSLog(@">>>>>>>>>>>>>signWordOrder= %@",signWordOrder);
      struct SIGNDOC_Field *field = NULL;
      field = SIGNDOC_Field_new (&ex);
      int page = getSignDocPointPageByFindTextByWordAndOrder([signWord UTF8String], signWordOrder);
      SIGNDOC_Field_setName (&ex, field, SIGNDOC_ENCODING_NATIVE, [[NSString stringWithFormat:@"%@,%@", signWord, signWordOrder] UTF8String]);
      SIGNDOC_Field_setType (&ex, field, SIGNDOC_FIELD_TYPE_SIGNATURE_DIGSIG);
      SIGNDOC_Field_setPage (&ex, field, page);
      /**
      if(x<100){
        SIGNDOC_Field_setLeft (&ex, field, x );
        SIGNDOC_Field_setRight (&ex, field, x + 80);
      }else{
        SIGNDOC_Field_setLeft (&ex, field, x - 20);
        SIGNDOC_Field_setRight (&ex, field, x + 60);
      }
       */
      /*
      SIGNDOC_Field_setLeft (&ex, field, x -40 );
      SIGNDOC_Field_setLeft (&ex, field, x +40 );
      */
      SIGNDOC_Field_setLeft (&ex, field, x );
      SIGNDOC_Field_setRight (&ex, field, x + 80);
      /*
      SIGNDOC_Field_setBottom (&ex, field, y - 68);
      SIGNDOC_Field_setTop (&ex, field, y -20);
       */
      SIGNDOC_Field_setBottom (&ex, field, y - 24);
      SIGNDOC_Field_setTop (&ex, field, y +24);
      
      int set_field_flags = SIGNDOC_DOCUMENT_SETFIELDFLAGS_FONT_FAIL;
      rc = SIGNDOC_Document_addField (&ex, doc, field, set_field_flags);
      if (rc == SIGNDOC_DOCUMENT_RETURNCODE_OK){
        NSLog (@"Signature field added new");
      }
      
      rc = SIGNDOC_Document_createSignatureParameters (&ex, doc, SIGNDOC_ENCODING_NATIVE, [[NSString stringWithFormat:@"%@,%@", signWord, signWordOrder] UTF8String], "", &sp);
      if (rc != SIGNDOC_DOCUMENT_RETURNCODE_OK)
        failDoc (doc, rc, "SIGNDOC_Document_createSignatureParameters");
      
      rc = SIGNDOC_SignatureParameters_setInteger (&ex, sp, "Method", SIGNDOC_SIGNATUREPARAMETERS_METHOD_DIGSIG_PKCS7_DETACHED);
      checkParam (sp, rc, "SIGNDOC_SignatureParameters_setInteger");
      rc = SIGNDOC_SignatureParameters_setInteger (&ex, sp, "DetachedHashAlgorithm", SIGNDOC_SIGNATUREPARAMETERS_DETACHEDHASHALGORITHM_SHA256);
      checkParam (sp, rc, "SIGNDOC_SignatureParameters_setInteger");
      rc = SIGNDOC_SignatureParameters_setLength (&ex, sp, "FontSize", SIGNDOC_SIGNATUREPARAMETERS_VALUETYPE_ABS, 8);
      checkParam (sp, rc, "SIGNDOC_SignatureParameters_setLength");
      rc = SIGNDOC_SignatureParameters_setInteger (&ex, sp, "BiometricEncryption", SIGNDOC_SIGNATUREPARAMETERS_BIOMETRICENCRYPTION_RSA);
      checkParam (sp, rc, "SIGNDOC_SignatureParameters_setInteger");
      rc = SIGNDOC_SignatureParameters_setString(&ex, sp, SIGNDOC_ENCODING_UTF_8, "BiometricKeyPath", [[[[NSBundle mainBundle] bundlePath] stringByAppendingPathComponent:@"public.key"] UTF8String]);
      checkParam (sp, rc, "SIGNDOC_SignatureParameters_setString(bio key)");
      rc = SIGNDOC_SignatureParameters_setInteger (&ex, sp, "GenerateKeyPair", 1024);
      checkParam (sp, rc, "SIGNDOC_SignatureParameters_setInteger");
      rc = SIGNDOC_SignatureParameters_setString (&ex, sp, SIGNDOC_ENCODING_NATIVE, "CommonName", "self-signed");
      checkParam (sp, rc, "SIGNDOC_SignatureParameters_setString");
      if (selfSigned) {
        rc = SIGNDOC_SignatureParameters_setString (&ex, sp, SIGNDOC_ENCODING_NATIVE, "Signer", "self-signed");
        checkParam (sp, rc, "SIGNDOC_SignatureParameters_setString");
      }
      rc = SIGNDOC_SignatureParameters_setBlob (&ex, sp, "BiometricData", signature_data, signature_size);
      checkParam (sp, rc, "SIGNDOC_SignatureParameters_setBlob");
      rc = SIGNDOC_SignatureParameters_setBlob (&ex, sp, "Image", signature_img, image_size);
      checkParam (sp, rc, "SIGNDOC_SignatureParameters_setBlob");
      
      //NSDictionary *systemConfig = [NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"SystemConfig" ofType:@"plist"]];
      NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
     // formatter.dateFormat = [systemConfig objectForKey:@"displayDateTimeFormat"];
      formatter.timeZone = [NSTimeZone systemTimeZone];
      [formatter setDateFormat:@"dd/MM/yyyy HH:mm:ss"];
      NSString *dateInfo = [formatter stringFromDate:[NSDate date]];
      formatter.timeZone = [NSTimeZone systemTimeZone];
    //  NSLog(@">>>>>dateInfo>>>>>>>>>>>%@",dateInfo);

     // rc = SIGNDOC_SignatureParameters_setString(&ex, sp, SIGNDOC_ENCODING_UTF_8, "SignTime", [[formatter stringFromDate:[NSDate date]] UTF8String]);
       rc = SIGNDOC_SignatureParameters_setString(&ex, sp, SIGNDOC_ENCODING_UTF_8, "SignTime",[dateInfo cStringUsingEncoding:NSASCIIStringEncoding]);
      checkParam(sp, rc, "SIGNDOC_SignatureParameters_setString");
     
      
      rc = SIGNDOC_Document_addSignature (&ex, doc, sp);
      if (rc != SIGNDOC_DOCUMENT_RETURNCODE_OK)
        failDoc (doc, rc, "SIGNDOC_Document_addSignature");
      
      if (sp != NULL)
        SIGNDOC_SignatureParameters_delete (sp);
      SignDocEventEmitter *signEvent = [[SignDocEventEmitter alloc] init];
      
      
      NSString *filePath = NSTemporaryDirectory();
      NSString *filepath = [NSString stringWithFormat:@"%@%@%@", filePath, @"temp", @".pdf"];
      NSError *error;
      NSData *data = [NSData dataWithContentsOfFile:filepath];
      [data writeToFile:filepath options:(NSDataWritingOptions)0 error:&error];
      
      //int height = 1024;
      int height = getDocumentHeight();
      int pageCount = SIGNDOC_Document_getPageCount(&ex, doc);
      
      UIImage *signatureImage;
      NSMutableArray *images = [[NSMutableArray alloc] init];
      for(int i = 1; i <= pageCount; i++){
        struct SIGNDOC_ByteArray *blob;
        struct SIGNDOC_RenderParameters *rp;
        struct SIGNDOC_RenderOutput *ro;
        blob = SIGNDOC_ByteArray_new (&ex);
        if (blob == NULL)
          RCTLog(@"SIGNDOC_ByteArray_new");
        rp = SIGNDOC_RenderParameters_new (&ex);
        ro = SIGNDOC_RenderOutput_new (&ex);
        SIGNDOC_RenderParameters_setFormat (&ex, rp, "jpg");
        SIGNDOC_RenderParameters_setPage(&ex, rp, i);
        SIGNDOC_RenderParameters_fitHeight (&ex, rp, height);
        //  struct SIGNDOC_Rect *clip = NULL;
        
        int rc = SIGNDOC_Document_renderPageAsImage (&ex, doc, blob, ro, rp, NULL);
        if (rc != SIGNDOC_DOCUMENT_RETURNCODE_OK) {
          RCTLog(@"Fail.....SIGNDOC_Document_renderPageAsImage");
        }
        else {
          RCTLog (@"renderTest");
          RCTLog (@"Image width:  %d", SIGNDOC_RenderOutput_getWidth (ro));
          RCTLog (@"Image height: %d", SIGNDOC_RenderOutput_getHeight (ro));
        }
        
        NSData *data = [NSData dataWithBytes:SIGNDOC_ByteArray_data(blob) length:SIGNDOC_ByteArray_count(blob)];
        //    NSString *base64Data = [data base64EncodedStringWithOptions:NSDataBase64EncodingEndLineWithLineFeed];
        signatureImage = [UIImage imageWithData:data];
        [images addObject: [UIImagePNGRepresentation(signatureImage) base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength]];
        SIGNDOC_ByteArray_delete(blob);
        //  SIGNDOC_Rect_delete(clipForRenderPageAsImage);
        
        if (ro != NULL)
          SIGNDOC_RenderOutput_delete (ro);
        if (rp != NULL)
          SIGNDOC_RenderParameters_delete (rp);
        
      }
      
      NSLog(@"Array length: %d", [images count]);
      NSData *pdf = [[NSFileManager defaultManager] contentsAtPath:filepath];
      NSString *pdfBase64 = [pdf base64EncodedStringWithOptions:0];
      
      [signEvent tellJS: @{@"images": images, @"tag": signWord, @"page": signWordOrder, @"pdfBase64": pdfBase64}];
      
      
      
      // Need remove after converting to png image
      //      NSString *pdfBase64 = [data base64EncodedStringWithOptions:0];
      //      [signEvent tellJS: pdfBase64];
    } else {
      NSLog (@"SignProcessor isn't init");
    }
  } @catch (NSException *e)  {
    NSLog (@"Exception: %@", e);
  }
  return 0;
}

void clearSignatures() {
  int rc = SIGNDOC_Document_clearAllSignatures(&ex, doc);
  checkDocumentRC (doc, rc, "SIGNDOC_Document_clearAllSignatures");
}

struct SIGNDOC_FieldArray *getDocumentFields(struct SIGNDOC_Document *doc) {
  if(isSignProcessorInit) {
    struct SIGNDOC_FieldArray *fields = SIGNDOC_FieldArray_new(&ex);
    int rc = SIGNDOC_Document_getFields(&ex, doc, 0, fields);
    checkDocumentRC (doc, rc, "SIGNDOC_Document_getFields");
    return fields;
  } else {
    NSLog (@"SignProcessor isn't init");
  }
  return nil;
}

CGRect getFieldFrameRect(struct SIGNDOC_Field *field, int pageHeight) {
  int left = SIGNDOC_Field_getLeft(&ex, field);
  int right = SIGNDOC_Field_getRight(&ex, field);
  int top = SIGNDOC_Field_getTop(&ex, field);
  int bottom = SIGNDOC_Field_getBottom(&ex, field);
  return CGRectMake(left, pageHeight-top, right-left, top-bottom);
}

NSMutableArray* getSignatureRects () {
  NSLog(@"getSignatureRects is called");
  NSMutableArray *signatureFieldsRect = [[NSMutableArray alloc]init];
  struct SIGNDOC_FieldArray *fields = getDocumentFields(doc);
  if(fields != nil) {
    for (int i=0; i<SIGNDOC_FieldArray_count(fields); i++) {
      struct SIGNDOC_Field *currentField = SIGNDOC_FieldArray_at(fields,i);
      int type = SIGNDOC_Field_getType(&ex, currentField);
      if (type == SIGNDOC_FIELD_TYPE_SIGNATURE_SIGNDOC || type == SIGNDOC_FIELD_TYPE_SIGNATURE_DIGSIG) {
        [signatureFieldsRect addObject:[NSValue valueWithCGRect:getFieldFrameRect(currentField, getDocumentHeight())]];
      }
    }
  }
  SIGNDOC_FieldArray_delete(fields);
  return signatureFieldsRect;
}

NSMutableArray* getSignaturesSignDocCoordinates () {
  NSMutableArray *signatureFieldsRect = [[NSMutableArray alloc]init];
  struct SIGNDOC_FieldArray *fields = getDocumentFields(doc);
  for (int i=0; i<SIGNDOC_FieldArray_count(fields); i++) {
    struct SIGNDOC_Field *currentField = SIGNDOC_FieldArray_at(fields,i);
    int type = SIGNDOC_Field_getType(&ex, currentField);
    if (type == SIGNDOC_FIELD_TYPE_SIGNATURE_SIGNDOC || type == SIGNDOC_FIELD_TYPE_SIGNATURE_DIGSIG) {
      int left = SIGNDOC_Field_getLeft(&ex, currentField);
      int right = SIGNDOC_Field_getRight(&ex, currentField);
      int top = SIGNDOC_Field_getTop(&ex, currentField);
      int bottom = SIGNDOC_Field_getBottom(&ex, currentField);
      [signatureFieldsRect addObject:[NSValue valueWithCGRect:CGRectMake(left, right, top, bottom)]];
    }
  }
  SIGNDOC_FieldArray_delete(fields);
  return signatureFieldsRect;
}

NSString* getNameOfSignatureWithLocation(CGPoint location) {
  NSLog(@"getNameOfSignatureWithLocation is called");
  NSString *result = nil;
  struct SIGNDOC_FieldArray *fields = getDocumentFields(doc);
  for (int i=0; i<SIGNDOC_FieldArray_count(fields); i++) {
    struct SIGNDOC_Field *currentField = SIGNDOC_FieldArray_at(fields,i);
    int type = SIGNDOC_Field_getType(&ex, currentField);
    if (type == SIGNDOC_FIELD_TYPE_SIGNATURE_SIGNDOC || type == SIGNDOC_FIELD_TYPE_SIGNATURE_DIGSIG) {
      CGRect currentSignatureRect = getFieldFrameRect(currentField, getDocumentHeight());
      if (currentSignatureRect.origin.x < location.x && location.x < currentSignatureRect.origin.x + currentSignatureRect.size.width && currentSignatureRect.origin.y < location.y && location.y < currentSignatureRect.origin.y + currentSignatureRect.size.height) {
        result = [NSString stringWithUTF8String:SIGNDOC_Field_getNameUTF8(&ex, currentField)];
        SIGNDOC_FieldArray_delete(fields);
        return result;
      }
    }
  }
  SIGNDOC_FieldArray_delete(fields);
  return result;
}

double getDocumentHeight() {
  if(isSignProcessorInit) {
    double height, width;
    int rc = SIGNDOC_Document_getPageSize(&ex, doc, 1, &width, &height);
    checkDocumentRC (doc, rc, "SIGNDOC_Document_getPageSize");
    return height;
  } else {
    NSLog (@"SignProcessor isn't init");
  }
  return 0;
}

double getDocumentWidth() {
  if(isSignProcessorInit) {
    double height, width;
    int rc = SIGNDOC_Document_getPageSize(&ex, doc, 1, &width, &height);
    checkDocumentRC (doc, rc, "SIGNDOC_Document_getPageSize");
    return width;
  } else {
    NSLog (@"SignProcessor isn't init");
  }
  return 0;
}

void doTest () {
  if(isSignProcessorInit) {
    struct SIGNDOC_TextFieldAttributes *tfa = NULL;
    struct SIGNDOC_FindTextPositionArray *pos = NULL;
    struct SIGNDOC_Field *field = NULL;
    struct SIGNDOC_VerificationResult *ver = NULL;
    struct SIGNDOC_ByteArray *blob = NULL;
    const char *image_path = "output.png";
    const char *bio_path = "test.bio";
    const char *bmp_path = "test.bmp";
    const char *field_name_text = "text1";
    const char *field_name_sig = "sig1";
    
    NSLog (@"Test started");
    
    @try {
      // Initialize
      //        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
      //        NSString *documentsDirectory = [paths objectAtIndex:0];
      NSString *documentPath = [docFilePath stringByAppendingPathComponent:docFileName];
      test_output_file = [documentPath UTF8String];
      NSLog (@"Output file: %s", test_output_file);
      
      {
        // Set properties
        
        int rc = SIGNDOC_Document_setStringProperty (&ex, doc, SIGNDOC_ENCODING_NATIVE, "encrypted", "priv1", "one");
        if (rc != SIGNDOC_DOCUMENT_RETURNCODE_OK)
          failDoc (doc, rc, "SIGNDOC_Document_setStringProperty");
        
        rc = SIGNDOC_Document_setStringProperty (&ex, doc, SIGNDOC_ENCODING_NATIVE, "public", "pub2", "two");
        if (rc != SIGNDOC_DOCUMENT_RETURNCODE_OK)
          failDoc (doc, rc, "SIGNDOC_Document_setStringProperty");
      }
      
      {
        // Insert text field
        
        tfa = SIGNDOC_TextFieldAttributes_new (&ex);
        SIGNDOC_TextFieldAttributes_setFontName (&ex, tfa, SIGNDOC_ENCODING_NATIVE, "Helvetica");
        SIGNDOC_TextFieldAttributes_setFontSize (&ex, tfa, 12);
        
        field = SIGNDOC_Field_new (&ex);
        SIGNDOC_Field_setName (&ex, field, SIGNDOC_ENCODING_NATIVE, field_name_text);
        SIGNDOC_Field_setType (&ex, field, SIGNDOC_FIELD_TYPE_TEXT);
        SIGNDOC_Field_setPage (&ex, field, 1);
        SIGNDOC_Field_setLeft (&ex, field, 450);
        SIGNDOC_Field_setRight (&ex, field, 750);
        SIGNDOC_Field_setBottom (&ex, field, 520);
        SIGNDOC_Field_setTop (&ex, field, 750);
        SIGNDOC_Field_setTextFieldAttributes (&ex, field, tfa);
        SIGNDOC_Field_setJustification(&ex, field, SIGNDOC_FIELD_JUSTIFICATION_LEFT);
        
        int set_field_flags = SIGNDOC_DOCUMENT_SETFIELDFLAGS_FONT_FAIL;
        int rc = SIGNDOC_Document_addField (&ex, doc, field, set_field_flags);
        if (rc != SIGNDOC_DOCUMENT_RETURNCODE_OK)
          failDoc (doc, rc, "SIGNDOC_Document_addField");
        NSLog (@"Text field added");
        SIGNDOC_Field_delete (field);
        field = NULL;
      }
      
      {
        // Change text field
        
        field = SIGNDOC_Field_new (&ex);
        int rc = SIGNDOC_Document_getField (&ex, doc, SIGNDOC_ENCODING_NATIVE, field_name_text, field);
        if (rc != SIGNDOC_DOCUMENT_RETURNCODE_OK)
          failDoc (doc, rc, "SIGNDOC_Document_getField");
        
        SIGNDOC_Field_setValue (&ex, field, SIGNDOC_ENCODING_NATIVE, "Set by SDTest2");
        
        int set_field_flags = SIGNDOC_DOCUMENT_SETFIELDFLAGS_FONT_FAIL;
        rc = SIGNDOC_Document_setField (&ex, doc, field, set_field_flags);
        if (rc != SIGNDOC_DOCUMENT_RETURNCODE_OK)
          failDoc (doc, rc, "SIGNDOC_Document_setField");
        NSLog (@"Text field changed");
        SIGNDOC_Field_delete (field);
        field = NULL;
      }
      
      // Find text
      
      pos = SIGNDOC_FindTextPositionArray_new (&ex);
      if (pos == NULL)
        failAlloc ("SIGNDOC_FindTextPositionArray_new");
      int find_text_flags = 0;
      int rc = SIGNDOC_Document_findText (&ex, doc, SIGNDOC_ENCODING_NATIVE, 1, 0 , "ADVISOR_SIGNATURE", find_text_flags, pos);
      //        int rc = SIGNDOC_Document_findText (&ex, doc, SIGNDOC_ENCODING_NATIVE, 1, 1, "ADVISOR_SIGNATURE", find_text_flags, pos);
      if (rc != SIGNDOC_DOCUMENT_RETURNCODE_OK)
        failDoc (doc, rc, "SIGNDOC_Document_findText");
      NSLog (@"Text positions: %u", SIGNDOC_FindTextPositionArray_count (pos));
      
      int doc_signed = 0;
      if (SIGNDOC_FindTextPositionArray_count (pos) >= 1)
      {
        // Insert signature field
        
        struct SIGNDOC_FindTextPosition *pos2 = SIGNDOC_FindTextPositionArray_at (pos, 0);
        struct SIGNDOC_CharacterPosition *pos3 = SIGNDOC_FindTextPosition_getLast (pos2);
        struct SIGNDOC_Point *pos4 = SIGNDOC_CharacterPosition_getRef (pos3);
        double x = SIGNDOC_Point_getX (pos4);
        double y = SIGNDOC_Point_getY (pos4);
        NSLog (@"x=%g, y=%g", x, y);
        field = SIGNDOC_Field_new (&ex);
        SIGNDOC_Field_setName (&ex, field, SIGNDOC_ENCODING_NATIVE, field_name_sig);
        SIGNDOC_Field_setType (&ex, field, SIGNDOC_FIELD_TYPE_SIGNATURE_DIGSIG);
        SIGNDOC_Field_setPage (&ex, field, 1);
        SIGNDOC_Field_setLeft (&ex, field, x - 50);
        SIGNDOC_Field_setRight (&ex, field, x + 90);
        SIGNDOC_Field_setBottom (&ex, field, y - 100);
        SIGNDOC_Field_setTop (&ex, field, y -30);
        int set_field_flags = SIGNDOC_DOCUMENT_SETFIELDFLAGS_FONT_FAIL;
        rc = SIGNDOC_Document_addField (&ex, doc, field, set_field_flags);
        if (rc != SIGNDOC_DOCUMENT_RETURNCODE_OK)
          failDoc (doc, rc, "SIGNDOC_Document_addField");
        NSLog (@"Signature field added");
        
        // Sign
        
        NSString *bio_path2 = [NSString stringWithUTF8String:bio_path];
        NSString *bio_path3 = [[[NSBundle mainBundle] bundlePath] stringByAppendingPathComponent:bio_path2];
        const char *bio_path4 = [bio_path3 UTF8String];
        NSLog (@"bio file: %s", bio_path4);
        size_t bio_size = 0;
        unsigned char *bio_ptr = readFile (bio_path4, &bio_size);
        
        NSString *bmp_path2 = [NSString stringWithUTF8String:bmp_path];
        NSString *bmp_path3 = [[[NSBundle mainBundle] bundlePath] stringByAppendingPathComponent:bmp_path2];
        const char *bmp_path4 = [bmp_path3 UTF8String];
        NSLog (@"bmp file: %s", bmp_path4);
        size_t bmp_size = 0;
        unsigned char *bmp_ptr = readFile (bmp_path4, &bmp_size);
        
        signProcess(field_name_sig, bio_ptr, bio_size, bmp_ptr, bmp_size, TRUE, 0, 0);
        
        NSLog (@"Document signed");
        doc_signed = 1;
        free (bio_ptr);
        free (bmp_ptr);
      }
      
      if (doc_signed)
      {
        // Verify
        
        rc = SIGNDOC_Document_verifySignature (&ex, doc, SIGNDOC_ENCODING_NATIVE, field_name_sig, &ver);
        if (rc != SIGNDOC_DOCUMENT_RETURNCODE_OK)
          failDoc (doc, rc, "SIGNDOC_Document_verifySignature");
        int ss = -17;
        rc = SIGNDOC_VerificationResult_getState (&ex, ver, &ss);
        /** @todo SIGNDOC_VERIFICATIONRESULT_RETURNCODE_OK */
        if (rc != SIGNDOC_DOCUMENT_RETURNCODE_OK)
          failVer (ver, rc, "SIGNDOC_VerificationResult_getState");
        if (ss == SIGNDOC_VERIFICATIONRESULT_SIGNATURESTATE_UNMODIFIED)
          NSLog (@"Document unmodified");
        else
          NSLog (@"State = %d", ss);
      }
      
      // Render page as image
      
      NSString *image_path2 = [NSString stringWithUTF8String:image_path];
      NSString *image_path3 = [docFilePath stringByAppendingPathComponent:image_path2];
      const char *image_path4 = [image_path3 UTF8String];
      NSLog (@"Image file: %s", image_path4);
      remove (image_path4);
      
      blob = renderTest(1024);
      writeFile (image_path4, SIGNDOC_ByteArray_data (blob), SIGNDOC_ByteArray_count (blob));
      NSLog (@"Image saved");
      
      if (!doc_signed)
      {
        // Save document
        rc = SIGNDOC_Document_saveToFile(&ex, doc, SIGNDOC_ENCODING_NATIVE, NULL, SIGNDOC_DOCUMENT_SAVEFLAGS_INCREMENTAL);
        if (rc != SIGNDOC_DOCUMENT_RETURNCODE_OK)
          failDoc (doc, rc, "SIGNDOC_Document_saveToFile");
        NSLog (@"Document saved");
      }
      
      {
        // Check properties
        SIGNDOC_Document_delete (doc);
        doc = NULL;
        
        doc = SIGNDOC_DocumentLoader_loadFromFile (&ex, loader, SIGNDOC_ENCODING_NATIVE, test_output_file, SIGNDOC_TRUE);
        if (doc == NULL)
          failLoader (loader, "SIGNDOC_DocumentLoader_loadFromFile");
        
        char *v = NULL;
        int rc = SIGNDOC_Document_getStringProperty (&ex, doc, SIGNDOC_ENCODING_NATIVE, "encrypted", "priv1", &v);
        if (rc != SIGNDOC_DOCUMENT_RETURNCODE_OK)
          failDoc (doc, rc, "SIGNDOC_Document_getStringProperty");
        NSLog (@"priv1=%s", v);
        if (strcmp (v, "one") != 0)
          NSLog (@"Wrong value for priv1");
        SIGNDOC_free (v);
        
        v = NULL;
        rc = SIGNDOC_Document_getStringProperty (&ex, doc, SIGNDOC_ENCODING_NATIVE, "public", "pub2", &v);
        if (rc != SIGNDOC_DOCUMENT_RETURNCODE_OK)
          failDoc (doc, rc, "SIGNDOC_Document_getStringProperty");
        NSLog (@"pub2=%s", v);
        if (strcmp (v, "two") != 0)
          NSLog (@"Wrong value for pub2");
        SIGNDOC_free (v);
      }
    }
    @catch (NSException *e)  {
      NSLog (@"Exception: %@", e);
    }
    @finally {
      NSLog (@"Executing finally clause");
      if (blob != NULL)
        SIGNDOC_ByteArray_delete (blob);
      if (ver != NULL)
        SIGNDOC_VerificationResult_delete (ver);
      if (tfa != NULL)
        SIGNDOC_TextFieldAttributes_delete (tfa);
      if (field != NULL)
        SIGNDOC_Field_delete (field);
      if (pos != NULL)
        SIGNDOC_FindTextPositionArray_delete (pos);
    }
    NSLog (@"Test done");
  } else {
    NSLog (@"SignProcessor isn't init");
  }
}

