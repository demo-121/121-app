// emptyId is a default json responsed from server while you call the server with empty id
export const emptyId = {
  error: "not_found",
  reason: "missing"
};

export default {};
