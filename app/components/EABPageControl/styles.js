import { Dimensions, StyleSheet } from "react-native";
import Theme from "../../theme";

const dot = {
  borderRadius: Theme.radiusL,
  marginLeft: Theme.alignmentXS,
  width: 7,
  height: 7
};

export default StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "flex-end",
    position: "relative",
    width: "100%",
    overflow: "hidden"
  },
  pageContainer: {
    width: "100%",
    height: "100%"
  },
  pageContainerContent: {
    alignItems: "flex-start"
  },
  pageStyle: {
    width: Dimensions.get("window").width,
    height: "100%"
  },
  pageContentContainerStyle: {
    justifyContent: "center",
    alignItems: "center"
  },
  toolBar: {
    flexDirection: "row",
    flexBasis: "auto",
    alignItems: "center",
    justifyContent: "center",
    height: 48,
    backgroundColor: "rgba(248, 248, 248, 0.82)",
    borderTopWidth: 1,
    borderColor: Theme.lightGrey
  },
  dot,
  dotFirst: {
    ...dot,
    marginLeft: 0
  }
});
