import { StyleSheet } from "react-native";
import Theme from "../../../../theme";

export default StyleSheet.create({
  container: {
    width: "100%",
    height: "100%"
  },
  scrollView: {
    width: "100%",
    height: "100%"
  },
  scrollViewContent: {
    alignItems: "center",
    paddingVertical: Theme.alignmentM
  },
  quickQuotesWrapper: {
    flexDirection: "row",
    flexWrap: "wrap",
    justifyContent: "space-between",
    width: Theme.boxMaxWidth
  },
  quickQuote: {
    flex: 0,
    marginVertical: Theme.alignmentM,
    width: (Theme.boxMaxWidth - Theme.alignmentXL) / 2
  },
  quickQuoteCard: {
    flex: 0,
    padding: Theme.alignmentL
  },
  quickQuoteContent: {
    flexDirection: "row",
    alignItems: "flex-start"
  },
  mainWrapper: {
    flex: 1
  },
  productTitle: {
    flexDirection: "row",
    marginBottom: Theme.alignmentL
  },
  infoRow: {
    flexDirection: "row"
  },
  checkBoxRoot: {
    flexBasis: 24 + Theme.alignmentXS
  },
  checkBox: {
    paddingVertical: 0
  },
  quoteIcon: {
    flexBasis: "auto",
    width: 40,
    height: 40
  },
  laLists: {
    width: 360
  }
});
