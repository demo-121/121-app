import { StyleSheet } from "react-native";

export default StyleSheet.create({
  container: {
    alignItems: "center",
    justifyContent: "center",
    width: "100%",
    height: "100%"
  },
  scrollView: {
    height: "100%",
    width: "100%"
  },
  scrollViewContent: {
    alignItems: "center"
  }
});
