import { connect } from "react-redux";
import { REDUCER_TYPES } from "eab-web-api";
import EABHUD from "../components/EABHUD";

const { CONFIG } = REDUCER_TYPES;

const mapStateToProps = state => ({
  isLoading: state[CONFIG].isLoading
});

const mapDispatchToProps = () => ({});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(EABHUD);
