import { StyleSheet } from "react-native";
import Theme from "../../../../theme";
import Colors from "../../../../theme/colors";

/**
 * styles
 * */
export default StyleSheet.create({
  container: {
    width: 664,
    marginLeft: Theme.alignmentXL,
    marginRight: Theme.alignmentXL
  },
  header: {
    ...Theme.headerBrand,
    marginTop: Theme.alignmentXL,
    marginBottom: Theme.alignmentL
  },
  blockView: {
    justifyContent: "space-between",
    borderColor: Theme.lightGrey,
    borderBottomWidth: 1
  },
  blockViewBottom: {
    justifyContent: "space-between",
    borderColor: Theme.lightGrey,
    borderBottomWidth: 1,
    paddingBottom: Theme.alignmentXL,
    borderTopWidth: 1
  },
  blockViewNoBorder: {
    justifyContent: "space-between"
  },
  planDetailBlockView: {
    flexDirection: "row",
    justifyContent: "space-between"
  },
  planDetailContentView: {
    marginBottom: Theme.alignmentXL
  },
  planDetailContentViewRow: {
    flexDirection: "row",
    justifyContent: "flex-start",
    marginBottom: Theme.alignmentXXS / 2
  },
  title: {
    marginTop: Theme.alignmentXL
  },
  subTitle: {
    ...Theme.subheadSecondary,
    marginBottom: Theme.alignmentXL
  },
  planDetailTitle: {
    ...Theme.bodyPrimary,
    marginRight: Theme.alignmentXXS
  },
  planDetailContent: {
    ...Theme.bodySecondary,
    maxWidth: 330
  },
  textBox: {
    alignItems: "stretch",
    marginTop: Theme.alignmentL,
    // marginBottom: Theme.alignmentXL,
    backgroundColor: Theme.white
  },
  textSelection: {
    marginTop: 14,
    marginBottom: Theme.alignmentXL
  },
  warningStatementView: {
    flexDirection: "row",
    justifyContent: "flex-start"
  },
  warningStatementPoint: {
    ...Theme.bodyNegative,
    flexBasis: 18,
    marginRight: 6,
    marginBottom: Theme.alignmentXS
  },
  warningStatementHead: {
    ...Theme.bodyNegative,
    marginBottom: Theme.alignmentXS,
    fontWeight: "bold"
  },
  warningStatement: {
    ...Theme.bodyNegative,
    marginBottom: Theme.alignmentXS
  },
  table: {
    marginTop: Theme.alignmentXS,
    marginBottom: Theme.alignmentXL,
    borderColor: Theme.lightGrey,
    borderWidth: 1
  },
  tableColumnStart: {
    flex: 1,
    alignItems: "flex-start"
  },
  tableColumnEnd: {
    flex: 1,
    alignItems: "flex-end"
  },
  tableHeader: {
    backgroundColor: Theme.white
  },
  tableHeaderTitle: {
    ...Theme.tableColumnLabel
  },
  tableSectionHeaderTitle: {
    ...Theme.captionBrand
  },
  tableSection: {
    backgroundColor: Theme.white,
    paddingHorizontal: Theme.alignmentS
  },
  tableSectionTitle: {
    ...Theme.subheadPrimary,
    fontSize: Theme.fontSizeXS,
    textAlign: "center"
  },
  tableFooter: {
    backgroundColor: Theme.white
  },
  errorMsg: {
    ...Theme.fieldTextOrHelperTextNegative
  },
  inputFont: {
    fontSize: Theme.fontSizeS,
    lineHeight: Theme.fontSizeXXXM
  },
  AWICashUT: {
    fontStyle: "italic",
    color: Colors.grey,
    paddingBottom: Theme.alignmentS
  }
});
