import React from "react";
import { Image, Text, View } from "react-native";
import { createStackNavigator, createAppContainer } from "react-navigation";
import axaLogo from "../../assets/images/axaLogo.png";
import icAddPerson from "../../assets/images/icAddPerson.png";
import icMenu from "../../assets/images/icMenu.png";
import icSearch from "../../assets/images/icSearch.png";
import TranslatedText from "../../containers/TranslatedText";
import EABButton from "../../components/EABButton";
import { ContextConsumer } from "../../context";
import Theme from "../../theme";
import * as HOME_TYPES from "./constants/HOME_TYPES";
import ClientSearchBar from "./containers/ClientSearchBar";
import Home from "./containers/Home";
import Search from "./containers/Search";
import arrowIcon from "../../assets/images/back-icon.png";

const LandingNavigator = createStackNavigator(
  {
    Home: {
      screen: Home,
      navigationOptions: ({ navigation }) => {
        if (navigation.state.params) {
          const { params } = navigation.state;
          const renderHeaderRight = headerRightParams => {
            switch (headerRightParams.homeType) {
              case HOME_TYPES.CLIENT:
                return (
                  <View style={{ flexDirection: "row" }}>
                    <EABButton
                      testID="btnHomeSearchClients"
                      onPress={params.getContactList}
                    >
                      <Image
                        style={{
                          margin: Theme.alignmentXS,
                          width: 24,
                          height: 24
                        }}
                        source={icSearch}
                      />
                    </EABButton>
                    <EABButton
                      testID="btnHomeCreateClient"
                      onPress={headerRightParams.showCreateProfileDialog}
                    >
                      <Image
                        style={{
                          margin: Theme.alignmentXS,
                          width: 24,
                          height: 24
                        }}
                        source={icAddPerson}
                      />
                    </EABButton>
                  </View>
                );
              case HOME_TYPES.WORK_BENCH:
                return (
                  <EABButton onPress={headerRightParams.resetAllFilters}>
                    <Text style={Theme.textButtonLabelNormalAccent}>
                      Reset All Filters
                    </Text>
                  </EABButton>
                );
              case HOME_TYPES.HELP_CENTRE:
              case HOME_TYPES.SETTING:
              case HOME_TYPES.ABOUT_EASE:
              default:
                return null;
            }
          };

          const pageTitle = pageTitleParams => {
            switch (pageTitleParams.homeType) {
              case HOME_TYPES.CLIENT:
                return "Home";
              case HOME_TYPES.WORK_BENCH:
                return "My Workbench";
              case HOME_TYPES.HELP_CENTRE:
                return "HelpCentre";
              case HOME_TYPES.SETTING:
                return "Setting";
              case HOME_TYPES.ABOUT_EASE:
                return "About EAB";
              default:
                return "";
            }
          };

          return {
            ...(params.homeType === HOME_TYPES.CLIENT
              ? {
                  headerStyle: {
                    height: 80
                  }
                }
              : {}),
            title: pageTitle(params),
            headerLeft: (
              <View
                style={{
                  paddingTop: Theme.alignmentS - Theme.alignmentXS,
                  marginLeft: Theme.alignmentXL - Theme.alignmentXS,
                  height: "100%"
                }}
              >
                <EABButton onPress={params.showSideBar}>
                  <Image
                    style={{
                      margin: Theme.alignmentXS,
                      width: 24,
                      height: 24
                    }}
                    source={icMenu}
                  />
                </EABButton>
              </View>
            ),
            headerTitle:
              params.homeType === HOME_TYPES.CLIENT ? (
                <View
                  style={{
                    height: "100%",
                    justifyContent: "center"
                  }}
                >
                  <Image
                    style={{
                      height: 48,
                      width: 48
                    }}
                    source={axaLogo}
                  />
                </View>
              ) : (
                params.headerTitle
              ),
            headerRight: (
              <View
                style={{
                  paddingTop: Theme.alignmentS - Theme.alignmentXS,
                  marginRight: Theme.alignmentXL - Theme.alignmentXS,
                  height: "100%"
                }}
              >
                {renderHeaderRight(params)}
              </View>
            )
          };
        }
        return {};
      }
    },
    Search: {
      screen: Search,
      navigationOptions: ({ navigation }) => ({
        headerTitle: <ClientSearchBar />,
        headerLeft: (
          <ContextConsumer>
            {({ searchTextOnChange, language }) => (
              <View style={{ flexDirection: "row" }}>
                <Image
                  style={{
                    height: 20.5,
                    width: 12,
                    marginLeft: Theme.alignmentXS
                  }}
                  source={arrowIcon}
                />
                <EABButton
                  style={{
                    marginLeft: Theme.alignmentXS
                  }}
                  onPress={() => {
                    navigation.goBack();
                    searchTextOnChange("");
                  }}
                >
                  <TranslatedText
                    language={language}
                    style={Theme.textButtonLabelNormalAccent}
                    path="button.home"
                  />
                </EABButton>
              </View>
            )}
          </ContextConsumer>
        ),
        headerRight: (
          <ContextConsumer>
            {({ enableShowAll }) => (
              <EABButton
                style={{ marginRight: Theme.alignmentXL }}
                onPress={enableShowAll}
              >
                <Text style={Theme.textButtonLabelNormalAccent}>View All</Text>
              </EABButton>
            )}
          </ContextConsumer>
        )
      })
    }
  },
  {
    initialRouteName: "Home",
    defaultNavigationOptions: {
      headerTintColor: Theme.textButtonLabelNormalAccent.color
    }
  }
);

export default createAppContainer(LandingNavigator);
