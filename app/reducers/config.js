import { ACTION_TYPES, REDUCER_TYPES } from "eab-web-api";
import { combineReducers } from "redux";
import { Platform } from "react-native";
import callServerFunction from "../utilities/callServer";

const { CONFIG } = REDUCER_TYPES;

/**
 * callServer
 * */
const callServer = (state = callServerFunction) => state;

const platform = (state = Platform.OS) => state;

const isLoading = (state = false, { type }) => {
  switch (type) {
    case ACTION_TYPES[CONFIG].START_LOADING:
      return true;
    case ACTION_TYPES[CONFIG].FINISH_LOADING:
      return false;
    default:
      return state;
  }
};

export default combineReducers({
  callServer,
  platform,
  isLoading
});
