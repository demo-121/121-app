### Selector sample code snippets (To be placed in Playground.js)

```javascript
import React, { Component } from 'react';
import { Animated, StyleSheet, Text, Image, View, Dimensions, Platform} from 'react-native';
import SortableList from '../index';

const window = Dimensions.get('window');
const _ = require("lodash");

const data = {
  0: {
    image: 'https://placekitten.com/200/240',
    text: 'Chloe',
  },
  1: {
    image: 'https://placekitten.com/200/201',
    text: 'Jasper',
  },
  2: {
    image: 'https://placekitten.com/200/202',
    text: 'Pepper',
  },
  3: {
    image: 'https://placekitten.com/200/203',
    text: 'Oscar',
  }
};

export default class Basic extends Component {
  render() {
    return (
      <View style={styles.container}>
        <SortableList
          style={styles.list}
          contentContainerStyle={styles.contentContainer}
          data={data}
          renderRow={({data, active})=>{
            return (
              <Animated.View 
              style={[
                styles.row,
                styles.rowColor
              ]}
              >
                <Image source={{uri: data.image}} style={styles.image} />
                <Text style={styles.text}>{data.text}</Text>
              </Animated.View>
            );
          }}
          rowColor={styles.rowColor}
          rowCssBoxModel={{margin: 10}}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#eee',

    ...Platform.select({
      ios: {
        paddingTop: 20,
      },
    }),
  },

  title: {
    fontSize: 20,
    paddingVertical: 20,
    color: '#999999',
  },

  list: {
    flex: 1,
  },

  contentContainer: {
    width: window.width,
    paddingHorizontal: 30
  },

  row: {
    flexDirection: 'row',
    alignItems: 'center',
    height: 100,
    flex: 1,
  },

  rowColor: {
    backgroundColor: '#fff',
  },

  image: {
    width: 50,
    height: 50,
    marginRight: 30,
    borderRadius: 25,
  },

  text: {
    fontSize: 24,
    color: '#222222',
  },
});

```