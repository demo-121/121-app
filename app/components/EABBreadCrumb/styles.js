import { StyleSheet } from "react-native";
import Theme from "../../theme";

const stepContainer = {
  flexDirection: "row",
  justifyContent: "center",
  alignItems: "center",
  marginLeft: Theme.alignmentXS
};

export default StyleSheet.create({
  container: {
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    height: 36,
    width: "100%"
  },
  stepContainer,
  stepContainerFirst: {
    ...stepContainer,
    marginLeft: 0
  },
  image: {
    marginRight: Theme.alignmentXS,
    width: 24,
    height: 24
  },
  title: Theme.stepperLabelUnselected,
  titlePrevious: Theme.stepperLabelSelected
});
