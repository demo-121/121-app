import { StyleSheet } from "react-native";
import Theme from "../../../../theme";

/**
 * styles
 * */
export default StyleSheet.create({
  sectionHeaderView: {
    paddingBottom: 2,
    borderBottomWidth: 2,
    borderColor: Theme.brand
  },
  scrollView: {
    backgroundColor: Theme.paleGrey,
    width: "100%",
    height: "100%"
  },
  scrollViewContent: {
    alignItems: "center",
    paddingVertical: Theme.alignmentXL,
    paddingHorizontal: 72,
    paddingBottom: 150
  },
  giroCheckboxView: {
    paddingTop: 0,
    paddingBottom: 0
  },
  blueText: {
    ...Theme.textButtonLabelNormalAccent,
    fontWeight: "bold"
  },
  giroCheckbox: {
    height: 48
  },
  sectionView: {
    alignItems: "flex-start",
    width: 950,
    marginBottom: Theme.alignmentL
  },
  initalPaymentMethod: {
    flexDirection: "row",
    width: "100%"
  },
  paymentSelector: {
    width: "30%"
  },
  paymentButton: {
    marginTop: Theme.alignmentL,
    marginLeft: Theme.alignmentL
  },
  axsOnlineButton: {},
  samOnlineButton: {
    marginLeft: Theme.alignmentXL
  },
  axsSamButtonView: {
    flexDirection: "row",
    alignItems: "center"
  },
  tableSectionHeader: {
    justifyContent: "center"
  },
  tableSection: {
    flexDirection: "row",
    backgroundColor: Theme.white
  },
  tableSectionColumn1: {
    flex: 3,
    justifyContent: "flex-start"
  },
  tableSectionColumn2: {
    flex: 2,
    justifyContent: "flex-start"
  },
  tableSectionColumn3: {
    flex: 2,
    justifyContent: "flex-start"
  },
  tableSectionColumn4: {
    flex: 2,
    justifyContent: "flex-start"
  },
  bodyNegative: {
    ...Theme.bodyNegative,
    width: "100%",
    marginTop: Theme.alignmentXXS
  },
  cashPortionText: {
    ...Theme.bodySecondary,
    color: Theme.negative,
    width: "100%",
    marginTop: Theme.alignmentXXS
  },
  initialPaymentMethodSubtitle: {
    ...Theme.bodyPrimary,
    marginTop: Theme.alignmentL,
    marginBottom: Theme.alignmentL
  },
  initialPaymentMethodTableColumn1: {
    flex: 1,
    justifyContent: "flex-start"
  },
  initialPaymentMethodTableColumn2: {
    flex: 1,
    justifyContent: "flex-start"
  },
  sectionContentView: {
    marginTop: Theme.alignmentL,
    flexDirection: "column"
  },
  submissionButton: {
    marginTop: Theme.alignmentL
  },
  sectionContentRowView: {
    flexDirection: "row",
    width: "100%"
  },
  submissionSectionContentTitle: {
    width: 260,
    ...Theme.bodyPrimary
  },
  iconImage: {
    width: 24,
    height: 24
  },
  fieldFirst: {
    maxWidth: "100%"
  }
});
