import React, { PureComponent } from "react";
import { View, Text, Alert } from "react-native";
import PropTypes from "prop-types";
import * as _ from "lodash";
import EABSectionList from "../../../../components/EABSectionList";
import { TEXT_STORE } from "../../../../constants/REDUCER_TYPES";
import { ContextConsumer } from "../../../../context";
import EABi18n from "../../../../utilities/EABi18n";
import APP_REDUCER_TYPE_CHECK from "../../../../constants/REDUCER_TYPE_CHECK";
import styles from "./styles";
import ClientList from "../../../../components/ClientList";
import agentProfileAvatarUploadSelector from "../../../../utilities/agentProfileAvatarUploadSelector";
import { getAttachmentWithID } from "../../../../utilities/DataManager";

/**
 * Setting
 * @description Setting in Application Summary page.
 * @param {array} Setting - array of product item object
 * */

class Setting extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      agentProfileAvatar: {}
    };
  }

  componentDidMount() {
    const { agent } = this.props;
    if (!_.isEmpty(agent)) {
      this.getAgentProfileAvatar(`UX_${agent.profileId}`);
    }
  }

  getAgentProfileAvatar(docId) {
    getAttachmentWithID(docId, "agentProfilePic", response => {
      if (response && response.data) {
        this.setState({
          agentProfileAvatar: { uri: `data:image/png;base64,${response.data}` }
        });
      }
    });
  }

  render() {
    const { agent, textStore, updateAgentProfileAvatar } = this.props;
    const { agentProfileAvatar } = this.state;
    return (
      <ContextConsumer>
        {({ language }) => (
          <View style={styles.container}>
            <View style={styles.sectionListView}>
              <EABSectionList
                sections={[
                  {
                    key: "fnaReport",
                    data: [
                      {
                        key: "setting_key",
                        id: "setting_id",
                        title: EABi18n({
                          language,
                          textStore,
                          path: "agent.setting.profile.title"
                        }),
                        selected: true
                      }
                    ],
                    title: ""
                  }
                ]}
                onPress={() => {}}
              />
            </View>
            <View style={styles.contentView}>
              <Text style={styles.myProfileTitle}>
                {EABi18n({
                  language,
                  textStore,
                  path: "agent.setting.profile.title"
                })}
              </Text>
              <ClientList
                clientList={[
                  {
                    key: "setting",
                    photo: null,
                    name: agent.name,
                    position: agent.position,
                    lastName: "",
                    firstName: ""
                  }
                ]}
                avatarSrc={agentProfileAvatar}
                avatarOnPress={() => {
                  agentProfileAvatarUploadSelector({
                    language,
                    textStore,
                    onError: err => {
                      Alert.alert(err);
                    },
                    onChange: response => {
                      updateAgentProfileAvatar({
                        imageType: response.type,
                        base64: response.base64,
                        callback: () => {
                          this.getAgentProfileAvatar(`UX_${agent.profileId}`);
                        }
                      });
                    }
                  });
                }}
                clientOnPress={() => {}}
              />
            </View>
          </View>
        )}
      </ContextConsumer>
    );
  }
}

Setting.propTypes = {
  textStore: APP_REDUCER_TYPE_CHECK[TEXT_STORE].isRequired,
  updateAgentProfileAvatar: PropTypes.func.isRequired,
  agent: PropTypes.oneOfType([PropTypes.object]).isRequired
};

export default Setting;
