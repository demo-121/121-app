import {
  REDUCER_TYPE_CHECK as WEB_API_TYPES_CHECK,
  REDUCER_TYPES,
  utilities
} from "eab-web-api";
import PropTypes from "prop-types";
import React, { Component } from "react";
import {
  Image,
  Linking,
  ScrollView,
  Text,
  TouchableHighlight,
  View,
  Alert,
  KeyboardAvoidingView
} from "react-native";
import * as _ from "lodash";
import LinearGradient from "react-native-linear-gradient";
import moment from "moment";
import icEdit from "../../../../assets/images/icEdit.png";
import icAdd from "../../../../assets/images/icAdd.png";
import icCall from "../../../../assets/images/icCall.png";
import icDirections from "../../../../assets/images/icDirections.png";
import icEmail from "../../../../assets/images/icEmail.png";
import icLink from "../../../../assets/images/icLink.png";
import icError from "../../../../assets/images/icError.png";
import icWarning from "../../../../assets/images/icWarning.png";
import Avatar from "../../../../components/Avatar";
import EABButton from "../../../../components/EABButton";
import EABCard from "../../../../components/EABCard";
import EABDialog from "../../../../components/EABDialog";
import EABSegmentedControl from "../../../../components/EABSegmentedControl";
import {
  CLIENT_FORM,
  TRUSTED_INDIVIDUAL_FORM
} from "../../../../constants/FORM_TYPE";
import {
  ADDRESS,
  EMAIL,
  PHONENUMBER
} from "../../../../constants/PROFILE_DATA_TYPES";
import REDUCER_TYPE_CHECK from "../../../../constants/REDUCER_TYPE_CHECK";
import { TEXT_STORE } from "../../../../constants/REDUCER_TYPES";
import ClientFormContact from "../../../../containers/ClientFormContact";
import ClientFormProfile from "../../../../containers/ClientFormProfile";
import ClientFormTrustedIndividual from "../../../../containers/ClientFormTrustedIndividual";
import EABHUD from "../../../../containers/EABHUD";
import { ContextConsumer } from "../../../../context";
import Theme from "../../../../theme";
import EABi18n from "../../../../utilities/EABi18n";
import { languageTypeConvert } from "../../../../utilities/getEABTextByLangType";
import confirmationAlert from "../../../../utilities/confirmationAlert";
import LinkClientDialog from "../../../LinkClientDialog";
import styles from "./styles";

const { common, trigger, numberFormatter } = utilities;
const { getOptionTitle } = common;
const { CLIENT, OPTIONS_MAP } = REDUCER_TYPES;
const { optionCondition } = trigger;
const { numberToCurrency } = numberFormatter;
const { isClientFormHasError } = utilities.validation;

/**
 * ProfileDetail
 * @param {object} navigation - react navigation props
 * @param {function} cancelCreateClient - clean data and close dialog
 * @param {function} readyToAddFamilyMember - initialize client form for family member
 * @param {function} readyToEditFamilyMember - initialize client form for editing family member
 * @param {function} saveClient - save client data from client form
 * @param {function} readyToEditClient - initialize client form for subject client
 * @param {function} saveTrustedIndividual - save trusted individual data from
 *   client form
 * @param {function} loadClient - load profile data to client form
 * */
export default class ProfileDetail extends Component {
  constructor(props) {
    super(props);
    // =========================================================================
    // variables
    // =========================================================================
    this.segmentedControlArray = [
      {
        key: "A",
        title: "Profile",
        isShowingBadge: false,
        onPress: () => {
          this.setState({ showProfileTab: true });
        }
      },
      {
        key: "B",
        title: "Contact",
        isShowingBadge: false,
        onPress: () => {
          this.setState({ showProfileTab: false });
        }
      }
    ];
    // =========================================================================
    // functions
    // =========================================================================
    this.editTrustedIndividual = this.editTrustedIndividual.bind(this);

    this.state = {
      shouldShowTrustedIndividualForm: false,
      showProfileTab: true,
      isSavingClient: false
    };
  }
  // ===========================================================================
  // getters
  // ===========================================================================
  /**
   * @description indicate can save client or not
   * @return {boolean} errorStatus
   * */
  get isDisableSaveClient() {
    const { clientForm } = this.props;
    const { hasError } = isClientFormHasError(clientForm);
    return hasError;
  }
  // ===========================================================================
  // class methods
  // ===========================================================================
  /**
   * @description use to edit trusted individual
   * */
  editTrustedIndividual() {
    const { loadClient, readyToAddTrustedIndividual, profile } = this.props;
    loadClient(profile.trustedIndividuals || {});
    readyToAddTrustedIndividual(() => {
      this.setState({
        shouldShowTrustedIndividualForm: true
      });
    });
  }

  render() {
    // =========================================================================
    // variables
    // =========================================================================
    const { editTrustedIndividual } = this;
    const { textStore, profile, optionsMap, dependantProfiles } = this.props;
    const { isSavingClient, shouldShowTrustedIndividualForm } = this.state;
    // =========================================================================
    // render functions
    // =========================================================================
    /**
     * @description get UI address text form reducer data
     * @params {string} language - UI text language type
     * @return {string} returns a string that combines the reducer data
     * */
    const getAddressText = language => {
      const {
        addrBlock,
        addrEstate,
        unitNum,
        addrStreet,
        addrCountry,
        addrCity,
        residenceCountry,
        postalCode
      } = profile;

      const unitNumText = unitNum ? `${unitNum}, ` : "";
      const addrBlockText = addrBlock ? `${addrBlock}, ` : "";
      const addrEstateText = addrEstate ? `${addrEstate},` : "";
      const addrStreetText = addrStreet ? `${addrStreet},` : "";
      const addrPostalCodeText = postalCode ? `${postalCode},` : "";
      const firstLine = addrPostalCodeText ? `${addrPostalCodeText}\n` : "";
      const secondLine = addrBlockText ? `${addrBlockText}\n` : "";
      const thirdLine = addrStreetText ? `${addrStreetText}\n` : "";
      const fourthLine = unitNumText ? `${unitNumText}\n` : "";
      const fifthLine = addrEstateText ? `${addrEstateText}\n` : "";

      let cityData = "";
      if (addrCity) {
        cityData = optionCondition({
          value: residenceCountry,
          options: "city",
          optionsMap,
          showIfNoValue: false
        })
          ? `${getOptionTitle({
              value: addrCity,
              optionsMap: optionsMap.city,
              language: languageTypeConvert(language)
            })}, `
          : `${addrCity}, `;
      }
      const countryData =
        getOptionTitle({
          value: addrCountry,
          optionsMap: optionsMap.residency,
          language: languageTypeConvert(language)
        }) || "";
      let sixthLine = `${cityData}${countryData}\n`;
      if (sixthLine !== "") {
        sixthLine = `${sixthLine}`;
      }

      return (
        firstLine + secondLine + thirdLine + fourthLine + fifthLine + sixthLine
      );
    };
    /**
     * @description translate reducer data to UI structure
     * @params {string} language - UI text language type
     * @return {array} layout data
     * */
    const profileLayout = language => {
      /* get familyMembers section */
      const familyMembers = [];
      if (profile.dependants) {
        profile.dependants.forEach(member => {
          const memberProfile = dependantProfiles[member.cid];

          if (memberProfile) {
            familyMembers.push({
              key: memberProfile.cid,
              relationship: getOptionTitle({
                value: member.relationship,
                optionsMap: optionsMap.relationship,
                language: languageTypeConvert(language)
              }),
              name: memberProfile.fullName,
              photo: memberProfile.photo
            });
          }
        });
      }

      /* show Language Spoken */
      const displayLanguage = profile.language
        ? profile.language
            .split(",")
            .map(item =>
              getOptionTitle({
                value: item,
                optionsMap: optionsMap.language,
                language: languageTypeConvert(language)
              })
            )
            .join(",")
        : "";

      return {
        familyMembers,
        fullName: profile.fullName,
        photo: profile.photo,
        trustedIndividuals: profile.trustedIndividuals
          ? {
              photo: profile.trustedIndividuals.tiPhoto,
              fullName: profile.trustedIndividuals.fullName,
              idDocType:
                profile.trustedIndividuals.idDocType === "other" &&
                profile.trustedIndividuals.idDocTypeOther !== ""
                  ? profile.trustedIndividuals.idDocTypeOther
                  : getOptionTitle({
                      value: profile.trustedIndividuals.idDocType,
                      optionsMap: optionsMap.idDocType,
                      language: languageTypeConvert(language)
                    }),
              idCardNo: profile.trustedIndividuals.idCardNo,
              relationship:
                profile.trustedIndividuals.relationship === "OTH" &&
                profile.trustedIndividuals.relationshipOther !== ""
                  ? profile.trustedIndividuals.relationshipOther
                  : getOptionTitle({
                      value: profile.trustedIndividuals.relationship,
                      optionsMap: optionsMap.relationship,
                      language: languageTypeConvert(language)
                    })
            }
          : null,
        leftSection: [
          {
            id: "a",
            title: EABi18n({
              textStore,
              language,
              path: "clientProfile.personalInformation.title"
            }).toUpperCase(),
            items: [
              {
                id: "a",
                testID: "Gender",
                title: EABi18n({
                  textStore,
                  language,
                  path: "clientProfile.personalInformation.gender"
                }),
                value: getOptionTitle({
                  value: profile.gender,
                  optionsMap: {
                    type: "layout",
                    options: [
                      {
                        title: {
                          en: "Male"
                        },
                        value: "M"
                      },
                      {
                        title: {
                          en: "Female"
                        },
                        value: "F"
                      }
                    ]
                  },
                  language: languageTypeConvert(language)
                }),
                shouldShow: true
              },
              {
                id: "b",
                testID: "DateOfBirth",
                title: EABi18n({
                  textStore,
                  language,
                  path: "clientProfile.personalInformation.dateOfBirth"
                }),
                value: moment(profile.dob, "YYYY-MM-DD").isValid()
                  ? `${moment(profile.dob, "YYYY-MM-DD").format(
                      "DD/MM/YYYY"
                    )}(${profile.age})`
                  : "",
                shouldShow: true
              },
              {
                id: "c",
                testID: "Nationality",
                title: EABi18n({
                  textStore,
                  language,
                  path: "clientProfile.personalInformation.nationality"
                }),
                value: getOptionTitle({
                  value: profile.nationality,
                  optionsMap: optionsMap.nationality,
                  language: languageTypeConvert(language)
                }),
                shouldShow: true
              },
              {
                id: "j",
                testID: "PRStatus",
                title: EABi18n({
                  textStore,
                  language,
                  path: "clientProfile.personalInformation.PRStatus"
                }),
                value: getOptionTitle({
                  value: profile.prStatus,
                  optionsMap: {
                    type: "layout",
                    options: [
                      {
                        title: {
                          en: "Yes"
                        },
                        value: "Y"
                      },
                      {
                        title: {
                          en: "No"
                        },
                        value: "N"
                      }
                    ]
                  },
                  language: languageTypeConvert(language)
                }),
                shouldShow:
                  profile.nationality !== "" && profile.nationality !== "N1"
              },
              {
                id: "d",
                testID: "CountryOfResidence",
                title: EABi18n({
                  textStore,
                  language,
                  path: "clientProfile.personalInformation.countryOfResidence"
                }),
                value: getOptionTitle({
                  value: profile.residenceCountry,
                  optionsMap: optionsMap.residency,
                  language: languageTypeConvert(language)
                }),
                shouldShow: true
              },
              {
                id: "city state",
                testID: "CityState",
                title: EABi18n({
                  textStore,
                  language,
                  path: "clientProfile.personalInformation.cityState"
                }),
                value: getOptionTitle({
                  value: profile.residenceCity,
                  optionsMap: optionsMap.city,
                  language: languageTypeConvert(language)
                }),
                shouldShow: optionCondition({
                  value: profile.residenceCountry,
                  options: "city",
                  optionsMap,
                  showIfNoValue: false
                })
              },
              {
                id: "e",
                testID: "IdDocType",
                title: EABi18n({
                  textStore,
                  language,
                  path: "clientProfile.personalInformation.idDocType"
                }),
                value:
                  profile.idDocType === "other"
                    ? profile.idDocTypeOther
                    : getOptionTitle({
                        value: profile.idDocType,
                        optionsMap: optionsMap.idDocType,
                        language: languageTypeConvert(language)
                      }),
                shouldShow: true
              },
              {
                id: "f",
                testID: "IdCardNo",
                title: EABi18n({
                  textStore,
                  language,
                  path: "clientProfile.personalInformation.idCardNo"
                }),
                value: profile.idCardNo,
                shouldShow: true
              },
              {
                id: "g",
                testID: "SmokingHabit",
                title: EABi18n({
                  textStore,
                  language,
                  path: "clientProfile.personalInformation.smokingHabit"
                }),
                value: getOptionTitle({
                  value: profile.isSmoker,
                  optionsMap: {
                    type: "layout",
                    options: [
                      {
                        title: {
                          en: "Non-smoker"
                        },
                        value: "N"
                      },
                      {
                        title: {
                          en: "Smoker"
                        },
                        value: "Y"
                      }
                    ]
                  },
                  language: languageTypeConvert(language)
                }),
                shouldShow: true
              },
              {
                id: "i",
                testID: "MaritalStatus",
                title: EABi18n({
                  textStore,
                  language,
                  path: "clientProfile.personalInformation.maritalStatus"
                }),
                value: getOptionTitle({
                  value: profile.marital,
                  optionsMap: optionsMap.marital,
                  language: languageTypeConvert(language)
                }),
                shouldShow: true
              },
              {
                id: "h",
                testID: "Language",
                title: EABi18n({
                  textStore,
                  language,
                  path: "clientProfile.personalInformation.language"
                }),
                // value: profile.language ? profile.language : ""
                value: profile.languageOther
                  ? displayLanguage.replace("Others", profile.languageOther)
                  : displayLanguage,
                shouldShow: true
              }
            ]
          },
          {
            id: "b",
            title: EABi18n({
              textStore,
              language,
              path: "clientProfile.workEducation.title"
            }).toUpperCase(),
            items: [
              {
                id: "a",
                testID: "Education",
                title: EABi18n({
                  textStore,
                  language,
                  path: "clientProfile.workEducation.education"
                }),
                value: getOptionTitle({
                  value: profile.education,
                  optionsMap: optionsMap.education,
                  language: languageTypeConvert(language)
                }),
                shouldShow: true
              },
              {
                id: "b",
                testID: "EmployStatus",
                title: EABi18n({
                  textStore,
                  language,
                  path: "clientProfile.workEducation.employStatus"
                }),
                value:
                  profile.employStatus === "ot"
                    ? profile.employStatusOther
                    : getOptionTitle({
                        value: profile.employStatus,
                        optionsMap: optionsMap.employStatus,
                        language: languageTypeConvert(language)
                      }),
                shouldShow: true
              },
              {
                id: "c",
                testID: "Occupation",
                title: EABi18n({
                  textStore,
                  language,
                  path: "clientProfile.workEducation.occupation"
                }),
                value: getOptionTitle({
                  value: profile.occupation,
                  optionsMap: optionsMap.occupation,
                  language: languageTypeConvert(language)
                }),
                shouldShow: true
              },
              {
                id: "d",
                testID: "Industry",
                title: EABi18n({
                  textStore,
                  language,
                  path: "clientProfile.workEducation.industry"
                }),
                value: getOptionTitle({
                  value: profile.industry,
                  optionsMap: optionsMap.industry,
                  language: languageTypeConvert(language)
                }),
                shouldShow: true
              },
              {
                id: "e",
                testID: "EmployBusinessSchool",
                title: EABi18n({
                  textStore,
                  language,
                  path: "clientProfile.workEducation.nameOfEmployBusinessSchool"
                }),
                value: profile.organization,
                shouldShow: true
              },
              {
                id: "f",
                testID: "EmployBusinessSchool",
                title: EABi18n({
                  textStore,
                  language,
                  path:
                    "clientProfile.workEducation.countryOfEmployBusinessSchool"
                }),
                value:
                  profile.organizationCountry === "N/A"
                    ? "N/A"
                    : getOptionTitle({
                        value: profile.organizationCountry,
                        optionsMap: optionsMap.residency,
                        language: languageTypeConvert(language)
                      }),
                shouldShow: true
              },
              {
                id: "g",
                testID: "TypeOfPass",
                title: EABi18n({
                  textStore,
                  language,
                  path: "clientProfile.workEducation.typeOfPass"
                }),
                value: getOptionTitle({
                  value: profile.pass,
                  optionsMap: optionsMap.pass,
                  language: languageTypeConvert(language)
                }),
                shouldShow:
                  profile.nationality &&
                  profile.nationality !== "N1" &&
                  profile.nationality !== "N2" &&
                  profile.prStatus &&
                  profile.prStatus !== "Y"
              },
              {
                id: "h",
                testID: "PassExpiryDate",
                title: EABi18n({
                  textStore,
                  language,
                  path: "clientProfile.workEducation.passExpiryDate"
                }),
                value:
                  moment(new Date(profile.passExpDate)).isValid() &&
                  profile.passExpDate !== null
                    ? `${moment(
                        new Date(profile.passExpDate),
                        "YYYY-MM-DD"
                      ).format("DD/MM/YYYY")}`
                    : "",
                shouldShow:
                  profile.nationality &&
                  profile.nationality !== "N1" &&
                  profile.nationality !== "N2" &&
                  profile.prStatus &&
                  profile.prStatus !== "Y"
              },
              {
                id: "i",
                testID: "Income",
                title: EABi18n({
                  textStore,
                  language,
                  path: "clientProfile.workEducation.income"
                }),
                value:
                  profile.allowance >= 0
                    ? `$${numberToCurrency({
                        value: profile.allowance,
                        decimal: 0
                      })}`
                    : "",
                shouldShow: true
              }
            ]
          }
        ],
        rightSection: [
          {
            id: "a",
            title: EABi18n({
              textStore,
              language,
              path: "clientProfile.contact.title"
            }).toUpperCase(),
            items: [
              {
                id: "a",
                icon: icCall,
                testID: "Mobile",
                title: EABi18n({
                  textStore,
                  language,
                  path: "clientProfile.contact.mobile"
                }),
                value:
                  profile.mobileNo !== ""
                    ? `${profile.mobileCountryCode} ${profile.mobileNo}`
                    : "-",
                type: PHONENUMBER
              },
              {
                id: "b",
                icon: icCall,
                testID: "OtherMobile",
                title: EABi18n({
                  textStore,
                  language,
                  path: "clientProfile.contact.otherMobile"
                }),
                value:
                  profile.otherNo !== ""
                    ? `${profile.otherMobileCountryCode} ${profile.otherNo}`
                    : "-",
                type: PHONENUMBER
              },
              {
                id: "d",
                icon: icEmail,
                testID: "Email",
                title: EABi18n({
                  textStore,
                  language,
                  path: "clientProfile.contact.email"
                }),
                value: profile.email,
                type: EMAIL
              },
              {
                id: "e",
                icon: icDirections,
                testID: "HomeAddress",
                title: EABi18n({
                  textStore,
                  language,
                  path: "clientProfile.contact.homeAddress"
                }),
                value: getAddressText(language),
                type: ADDRESS
              }
            ]
          }
        ]
      };
    };

    /**
     * @description save client profile after clicking Yes in confirmation dialog
     * */
    const saveClientWithConfirm = callback => {
      this.setState({ isSavingClient: true }, () => {
        this.props.saveClient({
          confirm: true,
          callback: () => {
            this.setState({ isSavingClient: false }, callback);
          }
        });
      });
    };

    /**
     * @description save client profile after clicking Save button
     * */
    const saveClientWithoutConfirm = ({
      language,
      hideCreateProfileDialog
    }) => {
      this.setState({ isSavingClient: true }, () => {
        this.props.saveClient({
          confirm: false,
          callback: code => {
            this.setState({ isSavingClient: false }, () => {
              if (code) {
                setTimeout(() => {
                  confirmationAlert({
                    messagePath: `error.${code}`,
                    language,
                    textStore,
                    yesOnPress: () => {
                      saveClientWithConfirm(hideCreateProfileDialog);
                    }
                  });
                }, 200);
              } else {
                hideCreateProfileDialog();
              }
            });
          }
        });
      });
    };

    /**
     * @name renderClientForm
     * @return {element} edit client dialog component
     * */
    const renderClientForm = language => (
      <ContextConsumer>
        {({ isShowingCreateProfileDialog, hideCreateProfileDialog }) => (
          <EABDialog isOpen={isShowingCreateProfileDialog}>
            <View style={Theme.dialogHeader}>
              <View style={Theme.headerRow}>
                <EABButton
                  containerStyle={Theme.headerLeft}
                  onPress={() => {
                    hideCreateProfileDialog();
                    this.props.cancelCreateClient();
                  }}
                >
                  <Text style={Theme.textButtonLabelNormalAccent}>
                    {EABi18n({
                      textStore,
                      language,
                      path: "button.cancel"
                    })}
                  </Text>
                </EABButton>
                <EABSegmentedControl
                  testID="segmentedProfileCategory"
                  segments={this.segmentedControlArray}
                  activatedKey={this.state.showProfileTab ? "A" : "B"}
                />
                <EABButton
                  testID="btnProfileSave"
                  containerStyle={Theme.headerRight}
                  onPress={() => {
                    saveClientWithoutConfirm({
                      language,
                      hideCreateProfileDialog
                    });
                  }}
                  isDisable={this.isDisableSaveClient}
                >
                  <Text style={Theme.textButtonLabelNormalAccent}>
                    {EABi18n({
                      textStore,
                      language,
                      path: "button.save"
                    })}
                  </Text>
                </EABButton>
              </View>
              <EABHUD isOpen={isSavingClient} />
            </View>
            {this.state.showProfileTab ? (
              <ClientFormProfile />
            ) : (
              <ClientFormContact />
            )}
          </EABDialog>
        )}
      </ContextConsumer>
    );
    /**
     * @description render Trusted Individual Form
     * @return {element} Trusted Individual Form dialog component
     * */
    const renderTrustedIndividualForm = language => (
      <EABDialog isOpen={shouldShowTrustedIndividualForm}>
        <View style={Theme.dialogHeader}>
          <View style={Theme.headerRow}>
            <EABButton
              containerStyle={Theme.headerLeft}
              onPress={() => {
                this.setState(
                  {
                    shouldShowTrustedIndividualForm: false
                  },
                  this.props.cancelCreateClient
                );
              }}
            >
              <Text style={Theme.textButtonLabelNormalAccent}>
                {EABi18n({
                  textStore,
                  language,
                  path: "button.cancel"
                })}
              </Text>
            </EABButton>
            <Text style={Theme.title}>
              {EABi18n({
                textStore,
                language,
                path: "clientProfile.trustedIndividual.title"
              })}
            </Text>
            <EABButton
              containerStyle={Theme.headerRight}
              onPress={() => {
                // this save function flow is:
                // 1. call server save
                // 2. is some data will be reset?
                //    a1. yes, alert user to confirm
                //    a2. save the result (call the same function but parameter
                //        confirm is true)
                //    b. no, save the result
                this.setState({ isSavingClient: true }, () => {
                  const callback = () => {
                    // can not set state at the same time. multiple dialogs
                    // can not close at the same time.
                    this.setState({ isSavingClient: false }, () => {
                      setTimeout(() => {
                        this.setState({
                          shouldShowTrustedIndividualForm: false
                        });
                      }, 200);
                    });
                  };
                  const alert = code => {
                    Alert.alert(
                      null,
                      EABi18n({ path: `error.${code}`, language, textStore }),
                      [
                        {
                          text: EABi18n({
                            path: "button.ok",
                            textStore,
                            language
                          }),
                          onPress: () => {
                            this.props.saveTrustedIndividual({
                              confirm: true,
                              alert,
                              callback
                            });
                          }
                        }
                      ]
                    );
                  };

                  this.props.saveTrustedIndividual({
                    callback,
                    alert
                  });
                });
              }}
              isDisable={this.isDisableSaveClient}
            >
              <Text style={Theme.textButtonLabelNormalAccent}>
                {EABi18n({
                  textStore,
                  language,
                  path: "button.save"
                })}
              </Text>
            </EABButton>
          </View>
          <EABHUD isOpen={isSavingClient} />
        </View>
        <KeyboardAvoidingView behavior="padding" keyboardVerticalOffset={25}>
          <ScrollView
            style={styles.tiScrollView}
            contentContainerStyle={styles.tiScrollViewContainerStyle}
          >
            <ClientFormTrustedIndividual />
          </ScrollView>
        </KeyboardAvoidingView>
      </EABDialog>
    );
    /**
     * @name renderSearchClientForm
     * @return {element} search client dialog component
     * */
    const renderSearchClientForm = () => (
      <ContextConsumer>
        {({ isShowingLinkClientDialog }) => (
          <EABDialog isOpen={isShowingLinkClientDialog}>
            <LinkClientDialog />
          </EABDialog>
        )}
      </ContextConsumer>
    );

    return (
      <ContextConsumer>
        {({
          language,
          showCreateProfileDialog,
          showLinkClientDialog,
          setSearchClientDialogFormType
        }) => (
          <LinearGradient
            {...Theme.LinearGradientProps}
            style={styles.container}
          >
            <KeyboardAvoidingView>
              <ScrollView
                style={styles.scrollView}
                showsVerticalScrollIndicator={false}
                contentContainerStyle={styles.scrollViewContainerStyle}
              >
                <View style={styles.info}>
                  <EABCard style={styles.infoDetail}>
                    {profileLayout(language).leftSection.map(
                      (section, index) => (
                        <View key={section.id} style={styles.section}>
                          <View style={styles.sectionHeader}>
                            <Text style={Theme.captionBrand}>
                              {section.title}
                            </Text>
                          </View>
                          <View
                            style={
                              index === 0 ? styles.tableFirst : styles.table
                            }
                          >
                            {section.items.map(
                              item =>
                                item.shouldShow ? (
                                  <View key={item.id} style={styles.tableRow}>
                                    <Text style={styles.tableHeader}>
                                      {item.title}:
                                    </Text>
                                    <View style={styles.tableCellWrapper}>
                                      <Text
                                        testID={`ProfileDetail__lbl${
                                          item.testID
                                        }`}
                                        style={styles.tableCell}
                                      >
                                        {item.value || item.value === 0
                                          ? item.value
                                          : "-"}
                                      </Text>
                                    </View>
                                  </View>
                                ) : null
                            )}
                          </View>
                        </View>
                      )
                    )}
                  </EABCard>
                  <EABCard style={styles.family}>
                    <View style={styles.headerWrapper}>
                      <View style={styles.header}>
                        <Text style={Theme.captionBrand}>
                          {EABi18n({
                            textStore,
                            language,
                            path: "clientProfile.familyMembers.title"
                          }).toUpperCase()}
                        </Text>
                      </View>
                      <View style={styles.buttonWrapper}>
                        <EABButton
                          onPress={() => {
                            this.props.readyToAddFamilyMember(() => {
                              showCreateProfileDialog();
                              this.setState({ showProfileTab: true });
                            });
                          }}
                        >
                          <Image style={styles.icon} source={icAdd} />
                        </EABButton>
                        <EABButton
                          onPress={() => {
                            setSearchClientDialogFormType({
                              type: CLIENT_FORM,
                              callback: () => {
                                showLinkClientDialog();
                              }
                            });
                          }}
                        >
                          <Image style={styles.icon} source={icLink} />
                        </EABButton>
                      </View>
                    </View>
                    {profileLayout(language).familyMembers.map(member => (
                      <TouchableHighlight
                        key={member.key}
                        underlayColor={Theme.brandSuperTransparent}
                        onPress={() => {
                          this.props.loadClient(
                            this.props.dependantProfiles[member.key]
                          );
                          this.props.readyToEditFamilyMember(() => {
                            showCreateProfileDialog();
                          });
                        }}
                      >
                        <View style={styles.memberRow}>
                          <Avatar
                            style={styles.memberPhotoWrapper}
                            imageStyle={styles.memberPhoto}
                            source={member.photo}
                            avatarOnPress={() => {
                              this.props.loadClient(
                                this.props.dependantProfiles[member.key]
                              );
                              this.props.readyToEditFamilyMember(() => {
                                showCreateProfileDialog();
                              });
                            }}
                          />
                          <View style={styles.memberInfo}>
                            <Text style={Theme.bodyPrimary}>
                              {member.name}
                              {_.isEmpty(member.relationship) ? (
                                <Image
                                  style={styles.relationshipErrorIcon}
                                  source={icWarning}
                                />
                              ) : null}
                            </Text>

                            <Text style={Theme.subheadSecondary}>
                              {this.props.dependantProfiles[member.key]
                                .relationship === "OTH" &&
                              this.props.dependantProfiles[member.key]
                                .relationshipOther !== ""
                                ? this.props.dependantProfiles[member.key]
                                    .relationshipOther
                                : member.relationship}
                            </Text>
                          </View>
                          {this.props.dependantProfiles[member.key]
                            .relationship === "SPO" &&
                          profile.marital !== "M" ? (
                            <View style={styles.errorIconWrapper}>
                              <Image
                                style={styles.errorIcon}
                                source={icError}
                              />
                            </View>
                          ) : null}
                        </View>
                      </TouchableHighlight>
                    ))}
                  </EABCard>
                  <EABCard style={styles.individual}>
                    <View style={styles.headerWrapper}>
                      <View style={styles.header}>
                        <Text style={Theme.captionBrand}>
                          {EABi18n({
                            textStore,
                            language,
                            path: "clientProfile.trustedIndividual.title"
                          }).toUpperCase()}
                        </Text>
                      </View>
                      <View style={styles.buttonWrapper}>
                        <EABButton onPress={editTrustedIndividual}>
                          <Image style={styles.icon} source={icEdit} />
                        </EABButton>
                        <EABButton
                          onPress={() => {
                            setSearchClientDialogFormType({
                              type: TRUSTED_INDIVIDUAL_FORM,
                              callback: () => {
                                showLinkClientDialog();
                              }
                            });
                          }}
                        >
                          <Image style={styles.icon} source={icLink} />
                        </EABButton>
                      </View>
                    </View>
                    {profileLayout(language).trustedIndividuals !== null ? (
                      <TouchableHighlight
                        underlayColor={Theme.brandSuperTransparent}
                        onPress={editTrustedIndividual}
                      >
                        <View style={styles.memberRow}>
                          <Avatar
                            style={styles.memberPhotoWrapper}
                            imageStyle={styles.memberPhoto}
                            source={
                              profileLayout(language).trustedIndividuals.photo
                            }
                          />
                          <View style={styles.memberInfo}>
                            <Text style={Theme.bodyPrimary}>
                              {
                                profileLayout(language).trustedIndividuals
                                  .fullName
                              }
                            </Text>
                            <Text style={Theme.subheadSecondary}>
                              {`${profileLayout(language).trustedIndividuals
                                .relationship || ""}${
                                profileLayout(language).trustedIndividuals
                                  .relationship &&
                                (profileLayout(language).trustedIndividuals
                                  .idDocType ||
                                  profileLayout(language).trustedIndividuals
                                    .idCardNo)
                                  ? ", "
                                  : ""
                              }${profileLayout(language).trustedIndividuals
                                .idDocType || ""}${
                                profileLayout(language).trustedIndividuals
                                  .idDocType &&
                                profileLayout(language).trustedIndividuals
                                  .idCardNo
                                  ? ": "
                                  : ""
                              }${profileLayout(language).trustedIndividuals
                                .idCardNo || ""}`}
                            </Text>
                          </View>
                        </View>
                      </TouchableHighlight>
                    ) : null}
                  </EABCard>
                </View>
                <View style={styles.contact}>
                  {profileLayout(language).rightSection.map(section => (
                    <View>
                      <View style={styles.header}>
                        <Text style={Theme.captionBrand}>{section.title}</Text>
                      </View>
                      {section.items.map(item => (
                        <View key={item.id} style={styles.contactRow}>
                          <Image
                            style={styles.contactIcon}
                            source={item.icon}
                          />
                          <View style={styles.contactDetails}>
                            <Text style={styles.contactTitle}>
                              {item.title}
                            </Text>
                            <Text
                              testID={`ProfileDetail__lbl${item.testID}`}
                              style={Theme.bodyPrimary}
                              onPress={() => {
                                if (item.type === PHONENUMBER) {
                                  Linking.openURL(item.value);
                                } else if (item.type === EMAIL) {
                                  Linking.openURL(`mailto: ${item.value}`);
                                } else if (item.type === ADDRESS) {
                                  // TODO
                                }
                              }}
                            >
                              {item.value ? item.value : "-"}
                            </Text>
                          </View>
                        </View>
                      ))}
                    </View>
                  ))}
                </View>
              </ScrollView>
            </KeyboardAvoidingView>
            {renderClientForm(language)}
            {renderSearchClientForm()}
            {renderTrustedIndividualForm(language)}
          </LinearGradient>
        )}
      </ContextConsumer>
    );
  }
}

ProfileDetail.propTypes = {
  clientForm: PropTypes.oneOfType([PropTypes.object]).isRequired,
  readyToAddFamilyMember: PropTypes.func.isRequired,
  readyToEditFamilyMember: PropTypes.func.isRequired,
  saveClient: PropTypes.func.isRequired,
  profile: WEB_API_TYPES_CHECK[CLIENT].profile.isRequired,
  optionsMap: WEB_API_TYPES_CHECK[OPTIONS_MAP].isRequired,
  dependantProfiles: WEB_API_TYPES_CHECK[CLIENT].dependantProfiles.isRequired,
  cancelCreateClient: PropTypes.func.isRequired,
  textStore: REDUCER_TYPE_CHECK[TEXT_STORE].isRequired,
  reloadContactList: PropTypes.func.isRequired,
  readyToEditClient: PropTypes.func.isRequired,
  readyToAddTrustedIndividual: PropTypes.func.isRequired,
  saveTrustedIndividual: PropTypes.func.isRequired,
  loadClient: PropTypes.func.isRequired
};
