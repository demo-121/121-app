import PropTypes from "prop-types";
import React, { PureComponent } from "react";
import { TouchableOpacity, View } from "react-native";
import Theme from "../../theme";
import {
  disableOpacity,
  RECTANGLE_BUTTON_1,
  RECTANGLE_BUTTON_2,
  TEXT_BUTTON
} from "./constants";
import styles from "./styles";

/**
 * <EABButton />
 * @description button component. Only support these children's case:
 *   1. <Text /> component. the <Text /> component will automatically change the color
 *   according to the buttonType.
 *   2. <Image />. the Image will add a -4 margin according to its index
 * @requires disableOpacity
 * @requires RECTANGLE_BUTTON_1
 * @requires RECTANGLE_BUTTON_2
 * @requires TEXT_BUTTON
 * @param {function} onPress - the function that will be fire by the onPress event
 * @param {string} [buttonType=BUTTON_TYPES.TEXT_BUTTON] - the button layout type
 * @param {number} [activeOpacity=.5] - the opacity value when the button is pressed
 * @param {boolean} [isDisable=false] - indicates whether the button is enabled or not
 * @param {string} [buttonColor=Theme.brand] - the button default color
 * @param {object|number=} style - button style.
 * @param {object|number=} containerStyle - button container style.
 * */
export default class EABButton extends PureComponent {
  constructor() {
    super();

    this.onPress = this.onPress.bind(this);
    this.dimTimeout = null;

    this.state = {
      dimButton: false
    };
  }

  componentWillUnmount() {
    clearTimeout(this.dimTimeout);
  }

  onPress() {
    if (!this.state.dimButton) {
      this.setState(
        {
          dimButton: true
        },
        () => {
          if (!this.props.isDisable) {
            this.props.onPress();
          }
          this.dimTimeout = setTimeout(() => {
            this.setState({ dimButton: false });
          }, 1000);
        }
      );
    }
  }

  render() {
    const { onPress } = this;

    switch (this.props.buttonType) {
      case TEXT_BUTTON:
        return (
          <TouchableOpacity
            testID={this.props.testID}
            style={this.props.containerStyle}
            activeOpacity={this.props.isDisable ? 1 : this.props.activeOpacity}
            onPress={onPress}
          >
            <View
              style={[
                styles.buttonContainer,
                ...(Array.isArray(this.props.style)
                  ? this.props.style
                  : [this.props.style]),
                { opacity: this.props.isDisable ? disableOpacity : 1 }
              ]}
            >
              {this.props.children}
            </View>
          </TouchableOpacity>
        );
      case RECTANGLE_BUTTON_1:
        return (
          <TouchableOpacity
            testID={this.props.testID}
            style={this.props.containerStyle}
            activeOpacity={this.props.isDisable ? 1 : this.props.activeOpacity}
            onPress={onPress}
          >
            <View
              style={[
                styles.buttonContainer,
                styles.rectangleButton1,
                ...(Array.isArray(this.props.style)
                  ? this.props.style
                  : [this.props.style]),
                {
                  backgroundColor: this.props.buttonColor,
                  opacity: this.props.isDisable ? disableOpacity : 1
                }
              ]}
            >
              {/* children is more than one component? */
              this.props.children.map
                ? this.props.children.map((component, index) =>
                    React.cloneElement(component, {
                      ...component.props,
                      style: [
                        component.props.style,
                        /* is it a icon? */
                        component.props.source
                          ? {
                              /* add -4 margin to icon */
                              ...(index === 0 ? { marginLeft: -4 } : {}),
                              ...(index === this.props.children.length - 1
                                ? { marginRight: -4 }
                                : {})
                            }
                          : {
                              /* change text color to buttonColor */
                              color: Theme.white
                            }
                      ],
                      key: `${component.props.source}${
                        component.props.children
                      }`
                    })
                  )
                : React.cloneElement(this.props.children, {
                    /* change text color to buttonColor */
                    ...this.props.children.props,
                    key: Math.random(),
                    style: [
                      this.props.children.props.style,
                      /* is it a icon? */
                      this.props.children.props.source
                        ? {
                            /* add -4 margins on each side of the icon */
                            marginRight: -4,
                            marginLeft: -4
                          }
                        : Theme.rectangleButtonStyle1Label
                    ]
                  })}
            </View>
          </TouchableOpacity>
        );
      case RECTANGLE_BUTTON_2:
        return (
          <TouchableOpacity
            testID={this.props.testID}
            style={this.props.containerStyle}
            activeOpacity={this.props.isDisable ? 1 : this.props.activeOpacity}
            onPress={onPress}
          >
            <View
              style={[
                styles.buttonContainer,
                styles.rectangleButton2,
                ...(Array.isArray(this.props.style)
                  ? this.props.style
                  : [this.props.style]),
                {
                  borderColor: this.props.buttonColor,
                  opacity: this.props.isDisable ? disableOpacity : 1
                }
              ]}
            >
              {/* children is more than one component? */
              this.props.children.map
                ? this.props.children.map((component, index) =>
                    React.cloneElement(component, {
                      ...component.props,
                      style: [
                        component.props.style,
                        /* is it a icon? */
                        component.props.source
                          ? {
                              /* add -4 margin to icon */
                              ...(index === 0 ? { marginLeft: -4 } : {}),
                              ...(index === this.props.children.length - 1
                                ? { marginRight: -4 }
                                : {})
                            }
                          : {
                              /* change text color to buttonColor */
                              color: this.props.buttonColor
                            }
                      ],
                      key: `${component.props.source}${
                        component.props.children
                      }`
                    })
                  )
                : React.cloneElement(this.props.children, {
                    /* change text color to buttonColor */
                    ...this.props.children.props,
                    key: Math.random(),
                    style: [
                      this.props.children.props.style,
                      /* is it a icon? */
                      this.props.children.props.source
                        ? {
                            /* add -4 margins on each side of the icon */
                            marginRight: -4,
                            marginLeft: -4
                          }
                        : {
                            ...Theme.rectangleButtonStyle1Label,
                            color: this.props.buttonColor
                          }
                    ]
                  })}
            </View>
          </TouchableOpacity>
        );
      default:
        throw new Error(
          "The buttonType must come from the constant 'BUTTON_TYPE'"
        );
    }
  }
}

EABButton.propTypes = {
  testID: PropTypes.string,
  buttonType: PropTypes.oneOf([
    TEXT_BUTTON,
    RECTANGLE_BUTTON_2,
    RECTANGLE_BUTTON_1
  ]),
  onPress: PropTypes.func.isRequired,
  activeOpacity: PropTypes.number,
  isDisable: PropTypes.bool,
  buttonColor: PropTypes.string,
  style: PropTypes.oneOfType([
    PropTypes.number,
    PropTypes.object,
    PropTypes.array
  ]),
  containerStyle: PropTypes.oneOfType([
    PropTypes.number,
    PropTypes.object,
    PropTypes.array
  ]),
  children: PropTypes.node.isRequired
};

EABButton.defaultProps = {
  testID: "",
  buttonType: TEXT_BUTTON,
  activeOpacity: 0.5,
  isDisable: false,
  buttonColor: Theme.brand,
  style: {},
  containerStyle: {}
};
