import { Platform } from "react-native";

const white = "rgba(255,255,255,1)";

const colors = {
  paleGrey: "rgba(250,250,250,1)",

  snackbarBg: "rgba(95,95,95,1)",

  black: "rgba(0,0,0,1)",
  darkGrey: "rgba(0,0,0,0.87)",
  grey: "rgba(0,0,0,0.54)",
  mediumGrey: "rgba(0,0,0,0.38)",
  coolGrey: "rgba(0,0,0,0.26)",
  lightGrey: "rgba(0,0,0,0.12)",

  negative: "rgba(255,23,33,1)",

  positive: "rgba(28,197,78,1)",

  accent: "rgba(240,118,98,1)",
  accentHoverAndPressed: "rgba(236,77,51,1)",

  white,

  brand: "rgba(155, 43, 58, 1)",
  brandBackgroundTransparent: "rgba(0,0,143,0.004)",
  brandSuperTransparent: "rgba(0,0,143,0.04)",
  brandTransparent: "rgba(0,0,143,0.1)",
  brandTransparentThree: "rgba(0, 0, 143, 0.4)",
  brandHoverAndPressed: "rgba(0,0,91,1)",

  red: "rgba(240,40,73,1)",
  green: "rgba(0,189,13,1)",

  LinearGradientProps: {
    colors: [white, "rgb(235,235,235)"],
    locations: [0, 1]
  }
};

const platformColors = Platform.select({
  ios: {},
  android: {}
});

export default { ...platformColors, ...colors };
