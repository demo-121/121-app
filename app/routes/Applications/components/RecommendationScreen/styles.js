import { StyleSheet } from "react-native";
import Theme from "../../../../theme";

/**
 * styles
 * */
export default StyleSheet.create({
  container: {
    flex: 1
  },
  header: {
    flexBasis: "auto",
    paddingTop: Theme.statusBarHeight + Theme.alignmentS,
    alignItems: "center",
    backgroundColor: Theme.white,
    shadowColor: "rgba(0, 0, 0, 0.3)",
    shadowOffset: {
      width: 0,
      height: 0.5
    },
    shadowRadius: 0,
    shadowOpacity: 1
  },
  titleBar: {
    flexDirection: "row",
    justifyContent: "space-between",
    marginBottom: Theme.alignmentM,
    alignItems: "center",
    width: "100%"
  },
  titleBarViewLeft: {
    alignItems: "flex-start",
    flex: 1
  },
  titleBarViewRight: {
    alignItems: "flex-end",
    flex: 1
  },
  titleBarViewMiddle: {
    alignItems: "center",
    flex: 1
  },
  titleBarTitle: {
    ...Theme.title
  },
  titleBarButtonRight: {
    marginRight: Theme.alignmentXL
  },
  titleBarButtonTitle: {
    ...Theme.textButtonLabelNormalAccent
  },
  KeyboardAvoidingViewWrapper: {
    flex: 1,
    width: "100%",
    overflow: "hidden"
  },
  KeyboardAvoidingView: {
    width: "100%",
    height: "100%"
  },
  scrollView: {
    flex: 1,
    width: "100%"
  },
  scrollViewContent: {
    flexGrow: 1,
    alignItems: "center"
  },
  bottomSheetContainer: {
    width: "100%"
  },
  bottomSheetView: {
    flexDirection: "row",
    justifyContent: "center"
  },
  planDetailsControlView: {
    width: 880,
    flexDirection: "row",
    justifyContent: "flex-end"
  },
  planDetailsButton: {
    marginLeft: Theme.alignmentXS,
    marginTop: 26,
    marginBottom: 26
  },
  planDetailsButtonTitle: {
    ...Theme.textButtonLabelSmallAccent,
    marginLeft: Theme.alignmentXS
  },
  planDetailsView: {
    width: 880,
    marginHorizontal: -1,
    paddingHorizontal: 1,
    paddingTop: 1
  },
  bottomSheetStepView: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between"
  },
  stepButtonLeft: {
    marginLeft: Theme.alignmentXL
  },
  stepButtonRight: {
    marginRight: Theme.alignmentXL
  }
});
