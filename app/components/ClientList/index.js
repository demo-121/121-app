import PropTypes from "prop-types";
import React, { PureComponent } from "react";
import { Text, TouchableHighlight, View } from "react-native";
import EABCard from "../../components/EABCard";
import TranslatedText from "../../containers/TranslatedText";
import { ContextConsumer } from "../../context";
import Avatar from "../Avatar";
import Theme from "../../theme";
import styles from "./styles";

/**
 * ClientList
 * @param {array} clientList - list of client
 * @param {string} clientList[].key - component key of client row
 * @param {number|object|string} clientList[].photo - client photo
 * @param {string} clientList[].name - client name
 * @param {string} clientList[].phone - client phone number
 * @param {string} clientList[].email - client email address
 * @param {number} clientList[].policies - number of client policies
 * @param {function} clientOnPress - function will be fire by click client row.
 *   will pass the client object of pressed row to be the parameter
 * */
export default class ClientList extends PureComponent {
  renderClientArray(language) {
    const { clientList, avatarSrc } = this.props;
    let showIcon = false;
    if (avatarSrc && avatarSrc.uri) {
      showIcon = true;
    }
    return clientList.map((client, index) => (
      <TouchableHighlight
        testID="btnClientResult"
        key={client.key}
        underlayColor={Theme.brandSuperTransparent}
        onPress={() => {
          this.props.clientOnPress(client);
        }}
      >
        <View style={styles.clientRow}>
          <Avatar
            style={styles.clientPhotoWrapper}
            showIcon={showIcon}
            source={avatarSrc}
            iconTextArray={[client.lastName, client.firstName]}
            avatarOnPress={this.props.avatarOnPress}
          />

          <View
            style={index === 0 ? styles.clientInfoFirst : styles.clientInfo}
          >
            <View style={styles.clientMainInfo}>
              <Text style={Theme.headlinePrimary}>{client.name}</Text>
              <Text style={Theme.subheadSecondary}>{client.position}</Text>
              <Text style={Theme.subheadSecondary}>{client.idInfo}</Text>
              <Text style={Theme.subheadSecondary}>{client.phone}</Text>
              <Text style={Theme.subheadSecondary}>{client.email}</Text>
            </View>
            {client.policies !== undefined ? (
              <View style={styles.policiesWrapper}>
                <Text style={Theme.display1}>{client.policies}</Text>
                <TranslatedText
                  style={Theme.subheadSecondary}
                  path="search.applications"
                  language={language}
                />
              </View>
            ) : null}
          </View>
        </View>
      </TouchableHighlight>
    ));
  }

  render() {
    const { clientList } = this.props;
    return (
      <ContextConsumer>
        {({ language }) =>
          clientList.length !== 0 ? (
            <EABCard style={styles.clientTable}>
              {this.renderClientArray(language)}
            </EABCard>
          ) : null
        }
      </ContextConsumer>
    );
  }
}

ClientList.propTypes = {
  clientList: PropTypes.arrayOf(
    PropTypes.shape({
      key: PropTypes.string.isRequired,
      photo: PropTypes.oneOfType([
        PropTypes.number,
        PropTypes.object,
        PropTypes.string
      ]).isRequired,
      name: PropTypes.string.isRequired,
      phone: PropTypes.oneOfType([PropTypes.number, PropTypes.string])
        .isRequired,
      email: PropTypes.string.isRequired,
      policies: PropTypes.number.isRequired
    })
  ).isRequired,
  clientOnPress: PropTypes.func.isRequired,
  avatarSrc: PropTypes.shape({
    uri: PropTypes.string
  }),
  avatarOnPress: PropTypes.func
};

ClientList.defaultProps = {
  avatarSrc: PropTypes.shape({
    uri: PropTypes.string
  }),
  avatarOnPress: PropTypes.func
};
