import React, { PureComponent } from "react";
import { ScrollView, View, Image } from "react-native";
import LinearGradient from "react-native-linear-gradient";
import PropTypes from "prop-types";
import * as _ from "lodash";

import { FILTER_TYPES, REDUCER_TYPE_CHECK, REDUCER_TYPES } from "eab-web-api";

import Theme from "../../../../theme";
import styles from "./styles";

import APP_REDUCER_TYPE_CHECK from "../../../../constants/REDUCER_TYPE_CHECK";
import { TEXT_STORE } from "../../../../constants/REDUCER_TYPES";
import icWarning from "../../../../assets/images/icWarning.png";
import noRecords from "../../../../assets/images/noRecords.png";
import EABBottomSheet from "../../../../components/EABBottomSheet";
import EABDialog from "../../../../components/EABDialog";
import EABHUD from "../../../../containers/EABHUD";
import * as DIALOG_TYPES from "../../../../components/EABDialog/constants";
import { FLAT_LIST } from "../../../../components/Selector/constants";
import Selector from "../../../../components/Selector";
import TranslatedText from "../../../../containers/TranslatedText";
import ProductList from "../../containers/ProductList";
import ApplicationsListFilter from "../../containers/ApplicationsListFilter";
import ApplicationsList from "../../containers/ApplicationsList";
import RecommendationScreen from "../../containers/RecommendationScreen";
import Quotation from "../../../Quotation";
import ApplicationScreen from "../../containers/ApplicationScreen";
import MultiClientProfile from "../../containers/MultiClientProfile";
import EABButton from "../../../../components/EABButton";
import {
  RECTANGLE_BUTTON_1,
  RECTANGLE_BUTTON_2
} from "../../../../components/EABButton/constants";
import {
  BOTTOM_SHEET_COLLAPSE_HEIGHT,
  BOTTOM_SHEET_EXPAND_HEIGHT
} from "./constants";
import { ContextConsumer } from "../../../../context";
import SupportingDocument from "../../../SupportingDocument";
import EABi18n from "../../../../utilities/EABi18n";

const { CLIENT, PRE_APPLICATION } = REDUCER_TYPES;

/**
 * SummaryScreen
 * @description SummaryScreen page.
 * @param {object} navigation - react navigation props
 * */

class SummaryScreen extends PureComponent {
  static filterApplicationItems({ item, selectedFilter }) {
    const isApplication =
      _.get(item, "type") === "application" ||
      _.get(item, "type") === "masterApplication";
    const appStatus = _.get(item, "appStatus");

    switch (selectedFilter) {
      case FILTER_TYPES.APPLICATION_LIST.ALL:
        return true;
      case FILTER_TYPES.APPLICATION_LIST.PROPOSED:
        return !isApplication && appStatus !== "INVALIDATED";
      case FILTER_TYPES.APPLICATION_LIST.APPLYING:
        return (
          isApplication &&
          ["SUBMITTED", "INVALIDATED", "INVALIDATED_SIGNED"].indexOf(
            appStatus
          ) < 0
        );
      case FILTER_TYPES.APPLICATION_LIST.SUBMITTED:
        return isApplication && appStatus === "SUBMITTED";
      case FILTER_TYPES.APPLICATION_LIST.INVALIDATED:
        return appStatus === "INVALIDATED";
      case FILTER_TYPES.APPLICATION_LIST.INVALIDATED_SIGNED:
        return isApplication && appStatus === "INVALIDATED_SIGNED";
      default:
        return false;
    }
  }

  constructor(props) {
    super(props);

    this.prepareBundleSelectorItems = this.prepareBundleSelectorItems.bind(
      this
    );
    this.prepareClientSelectoritems = this.prepareClientSelectoritems.bind(
      this
    );
    this.navigationDidFocus = this.navigationDidFocus.bind(this);
    this.renderSelectors = this.renderSelectors.bind(this);
    this.renderApplicationsList = this.renderApplicationsList.bind(this);

    const { profile, navigation, applicationsList, selectedIdList } = props;

    navigation.setParams({
      showHeaderRight: applicationsList.length > 0
    });
    navigation.setParams({ selectedIdCount: selectedIdList.length });

    this.state = {
      selectedClientId: _.get(profile, "cid", ""),
      openRecommendationScreen: false,
      isLoading: false
    };
  }

  componentDidMount() {
    this.didFocusSubscription = this.props.navigation.addListener(
      "didFocus",
      this.navigationDidFocus
    );
  }

  componentDidUpdate(prevProps) {
    const { navigation, selectedIdList, applicationsList } = this.props;
    if (prevProps.selectedIdList.length !== selectedIdList.length) {
      navigation.setParams({ selectedIdCount: selectedIdList.length });
    }
    if (prevProps.applicationsList.length !== applicationsList.length) {
      navigation.setParams({ showHeaderRight: applicationsList.length > 0 });
    }
  }

  componentWillUnmount() {
    this.didFocusSubscription.remove();
  }

  navigationDidFocus() {
    const { getApplicationsList } = this.props;
    this.setState(
      {
        isLoading: true
      },
      () => {
        getApplicationsList({
          callback: () => {
            this.setState({
              isLoading: false
            });
          }
        });
      }
    );
  }

  prepareBundleSelectorItems(language) {
    const { textStore, profile } = this.props;
    const bundleList = _.map(_.get(profile, "bundle"), bundle => ({
      key: bundle.id,
      label: bundle.isValid
        ? EABi18n({
            textStore,
            language,
            path: "application.summary.selector_first_bundle"
          })
        : bundle.id
    }));

    return { bundleList: bundleList.reverse() };
  }

  prepareClientSelectoritems(language) {
    const { textStore, pdaMembers } = this.props;
    const clientList = _.map(pdaMembers, member => ({
      key: member.cid,
      label: _.replace(
        EABi18n({
          textStore,
          language,
          path: "application.summary.selector_proposed_client"
        }),
        "{1}",
        member.fullName
      )
    }));

    return { clientList };
  }

  renderSelectors() {
    const { selectedClientId } = this.state;
    const { selectedBundleId, getApplicationsList } = this.props;
    return (
      <ContextConsumer>
        {({ language }) => (
          <View style={styles.selectorView}>
            <View style={styles.selectorsWrapper}>
              <Selector
                style={styles.selector}
                selectorType={FLAT_LIST}
                flatListOptions={{
                  data: this.prepareBundleSelectorItems(language).bundleList,
                  selectedValue: selectedBundleId,
                  renderItemOnPress: (itemKey, item) => {
                    const bundleId = item.key;
                    getApplicationsList({ bundleId });
                  }
                }}
              />
              <Selector
                style={styles.selector}
                selectorType={FLAT_LIST}
                flatListOptions={{
                  data: this.prepareClientSelectoritems(language).clientList,
                  selectedValue: selectedClientId,
                  renderItemOnPress: (itemKey, item) => {
                    this.setState({ selectedClientId: item.key });
                  }
                }}
              />
            </View>
          </View>
        )}
      </ContextConsumer>
    );
  }

  /**
   * @name renderApplicationsList
   * @return {element} applications list section
   * */
  renderApplicationsList() {
    const { selectedClientId } = this.state;
    const { navigation, selectedAppListFilter, applicationsList } = this.props;

    return (
      <View
        style={
          selectedAppListFilter === FILTER_TYPES.APPLICATION_LIST.PROPOSED
            ? styles.appListViewWithBottomSheet
            : styles.appListViewWithoutBottomSheet
        }
      >
        {applicationsList.length > 0 ? (
          <ScrollView
            style={styles.scrollView}
            contentContainerStyle={styles.contentContainerScrollView}
            showsVerticalScrollIndicator={false}
            stickyHeaderIndices={[1]}
          >
            <View style={styles.scrollViewContentSubview}>
              <ScrollView style={styles.productListScrollView} horizontal>
                <ProductList cid={selectedClientId} />
              </ScrollView>
            </View>
            <View style={styles.scrollViewContentSubview} />
            <View style={styles.scrollViewContentSubview}>
              <View style={styles.midAppListFilterView}>
                <ApplicationsListFilter
                  filterApplicationItems={SummaryScreen.filterApplicationItems}
                />
              </View>
              <ApplicationsList
                navigation={navigation}
                filterApplicationItems={SummaryScreen.filterApplicationItems}
              />
            </View>
          </ScrollView>
        ) : (
          <View style={styles.noRecordsContainer}>
            <View style={styles.noRecordsView}>
              <Image source={noRecords} />
              <TranslatedText
                style={styles.noRecordsTitle}
                path="application.summary.no_records"
              />
            </View>
          </View>
        )}
      </View>
    );
  }

  render() {
    const { openRecommendationScreen, isLoading } = this.state;
    const {
      recommendCompleted,
      recommendChoiceList,
      selectedAppListFilter,
      applicationsList,
      getRecommendation
    } = this.props;

    const isAnyChoiceSelected =
      _.findIndex(recommendChoiceList, item => item.checked) >= 0;
    const selectedChoicesCount = _.filter(
      recommendChoiceList,
      item => item.checked
    ).length;

    // UI component for footer
    let recommendText = null;
    let recommendButton = null;

    if (isAnyChoiceSelected) {
      recommendText = recommendCompleted ? (
        <TranslatedText
          path="application.summary.recommendation_added_recommended"
          replace={[{ value: `${selectedChoicesCount}`, isPath: false }]}
        />
      ) : (
        <TranslatedText path="application.summary.recommendation_added" />
      );
      recommendButton = (
        <View style={styles.recommendContainer}>
          {!recommendCompleted ? (
            <Image style={styles.recommendIcon} source={icWarning} />
          ) : null}
          <EABButton
            testID="PreAppilcation__btnRecommandation"
            buttonType={
              recommendCompleted ? RECTANGLE_BUTTON_2 : RECTANGLE_BUTTON_1
            }
            onPress={() => {
              getRecommendation(() => {
                this.setState({ openRecommendationScreen: true });
              });
            }}
          >
            <TranslatedText
              path={
                recommendCompleted
                  ? "application.summary.button_edit_recommendation"
                  : "application.summary.button_recommend_choices"
              }
              funcOnText={recommendCompleted ? null : _.toUpper}
            />
          </EABButton>
        </View>
      );
    }

    return (
      <ContextConsumer>
        {({
          isEappLoading,
          isShowingQuotationNavigator,
          isShowingApplicationDialog,
          isShowingMultiClientProfileDialog,
          hideApplicationDialog,
          hideMultiClientProfileDialog,
          isShieldSupportingDocumentDialog,
          isShowingAppSummarySupportingDocumentDialog
        }) => (
          <LinearGradient
            {...Theme.LinearGradientProps}
            style={styles.container}
          >
            <EABHUD isOpen={isLoading || isEappLoading} />
            <View style={styles.contentView}>
              {this.renderSelectors()}
              {this.renderApplicationsList()}
              {applicationsList.length > 0 &&
              selectedAppListFilter ===
                FILTER_TYPES.APPLICATION_LIST.PROPOSED ? (
                <EABBottomSheet
                  containerStyle={styles.bottomSheetContainer}
                  style={styles.bottomSheetView}
                  isExpand={
                    selectedAppListFilter ===
                    FILTER_TYPES.APPLICATION_LIST.PROPOSED
                  }
                  expandHeight={BOTTOM_SHEET_EXPAND_HEIGHT}
                  collapseHeight={BOTTOM_SHEET_COLLAPSE_HEIGHT}
                >
                  <View style={styles.bottomSheetContentView}>
                    {recommendText}
                    {recommendButton}
                  </View>
                </EABBottomSheet>
              ) : null}
            </View>
            <EABDialog
              type={DIALOG_TYPES.SCREEN}
              isOpen={openRecommendationScreen}
            >
              <RecommendationScreen
                closeDialog={() => {
                  this.setState({ openRecommendationScreen: false });
                }}
              />
            </EABDialog>
            <EABDialog
              type={DIALOG_TYPES.SCREEN}
              isOpen={isShowingQuotationNavigator}
            >
              <Quotation />
            </EABDialog>
            <EABDialog
              type={DIALOG_TYPES.SCREEN}
              isOpen={isShowingApplicationDialog}
            >
              <ApplicationScreen
                closeDialog={() => {
                  hideApplicationDialog();
                }}
              />
            </EABDialog>
            <EABDialog
              type={DIALOG_TYPES.SCREEN}
              isOpen={isShowingMultiClientProfileDialog}
            >
              <MultiClientProfile closeDialog={hideMultiClientProfileDialog} />
            </EABDialog>
            <EABDialog isOpen={isShowingAppSummarySupportingDocumentDialog}>
              <SupportingDocument
                screenProps={{ isShield: isShieldSupportingDocumentDialog }}
              />
            </EABDialog>
          </LinearGradient>
        )}
      </ContextConsumer>
    );
  }
}

SummaryScreen.propTypes = {
  textStore: APP_REDUCER_TYPE_CHECK[TEXT_STORE].isRequired,
  profile: REDUCER_TYPE_CHECK[CLIENT].profile.isRequired,
  applicationsList: PropTypes.arrayOf(
    REDUCER_TYPE_CHECK[PRE_APPLICATION].ApplicationItem.value.isRequired
  ).isRequired,
  pdaMembers: PropTypes.oneOfType([PropTypes.array]).isRequired,
  selectedAppListFilter: PropTypes.string.isRequired,
  recommendCompleted: PropTypes.bool.isRequired,
  recommendChoiceList: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string,
      checked: PropTypes.bool
    })
  ).isRequired,
  selectedIdList: PropTypes.arrayOf(PropTypes.string).isRequired,
  selectedBundleId: PropTypes.string.isRequired,
  getApplicationsList: PropTypes.func.isRequired,
  getRecommendation: PropTypes.func.isRequired
};

export default SummaryScreen;
