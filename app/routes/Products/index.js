import React from "react";
import { View, Image } from "react-native";
import { createStackNavigator, createAppContainer } from "react-navigation";
import EABButton from "../../components/EABButton";
import TranslatedText from "../../containers/TranslatedText";
import { ContextConsumer } from "../../context";
import Theme from "../../theme";
import cleanClientData from "../../utilities/cleanClientData";
import NavigationService from "../../utilities/NavigationService";
import ProductsDetail from "./containers/ProductsDetail";
import QuickQuoteButtonGroup from "./containers/QuickQuoteButtonGroup";
import QuickQuoteHistory from "./containers/QuickQuoteHistory";
import arrowIcon from "../../assets/images/back-icon.png";

const ProductsNavigator = createStackNavigator(
  {
    ProductsDetail: {
      screen: ProductsDetail,
      navigationOptions: ({ navigation }) => ({
        title: "Products",
        headerLeft: (
          <ContextConsumer>
            {({ hideClientNavigator }) => (
              <EABButton
                style={{
                  marginLeft: Theme.alignmentXL
                }}
                onPress={() => {
                  hideClientNavigator();
                  cleanClientData(navigation.dispatch);
                  NavigationService.clearNavigationRef();
                }}
              >
                <TranslatedText
                  style={Theme.textButtonLabelNormalAccent}
                  path="button.close"
                />
              </EABButton>
            )}
          </ContextConsumer>
        ),
        headerRight: <QuickQuoteButtonGroup navigation={navigation} />
      })
    },
    QuickQuoteHistory: {
      screen: QuickQuoteHistory,
      navigationOptions: ({ navigation }) => {
        const selectModeOn = navigation.getParam("selectModeOn", false);
        const selectedIdList = navigation.getParam("selectedIdList", []);
        const deleteQuickQuote = navigation.getParam(
          "deleteQuickQuote",
          () => {}
        );

        return {
          headerTitle: selectModeOn ? (
            <TranslatedText
              style={Theme.title}
              path="proposal.title.num_selected"
              replace={[{ value: `${selectedIdList.length}`, isPath: false }]}
            />
          ) : (
            <TranslatedText
              style={Theme.title}
              path="quickQuote.quickQuoteHistory"
            />
          ),
          headerLeft: selectModeOn ? (
            <View style={{ flexDirection: "row" }}>
              <EABButton
                style={{
                  marginLeft: Theme.alignmentXL
                }}
                isDisable={selectedIdList.length === 0}
                onPress={() => {
                  const quotIds = selectedIdList.slice();
                  deleteQuickQuote(quotIds);
                  navigation.setParams({
                    selectedIdList: [],
                    selectModeOn: false
                  });
                }}
              >
                <TranslatedText
                  style={Theme.textButtonLabelNormalAccent}
                  path="button.delete"
                />
              </EABButton>
            </View>
          ) : (
            <View style={{ flexDirection: "row" }}>
              <Image
                style={{
                  height: 20.5,
                  width: 12,
                  marginLeft: Theme.alignmentXS
                }}
                source={arrowIcon}
              />
              <EABButton
                style={{
                  marginLeft: Theme.alignmentXS
                }}
                onPress={() => {
                  navigation.goBack();
                }}
              >
                <TranslatedText
                  style={Theme.textButtonLabelNormalAccent}
                  path="proposal.button.products"
                />
              </EABButton>
            </View>
          ),
          headerRight: selectModeOn ? (
            <EABButton
              style={{
                marginRight: Theme.alignmentXL
              }}
              onPress={() => {
                navigation.setParams({
                  selectModeOn: false,
                  selectedIdList: []
                });
              }}
            >
              <TranslatedText
                style={Theme.textButtonLabelNormalAccent}
                path="button.cancel"
              />
            </EABButton>
          ) : (
            <EABButton
              style={{
                marginRight: Theme.alignmentXL
              }}
              onPress={() => {
                navigation.setParams({ selectModeOn: true });
              }}
            >
              <TranslatedText
                style={Theme.textButtonLabelNormalAccent}
                path="button.select"
              />
            </EABButton>
          )
        };
      }
    }
  },
  {
    initialRouteName: "ProductsDetail",
    defaultNavigationOptions: {
      headerTitleStyle: Theme.title,
      headerTintColor: Theme.textButtonLabelNormalAccent.color
    }
  }
);

const Products = createAppContainer(ProductsNavigator);
export default class Product extends React.Component {
  render() {
    return (
      <Products
        ref={navigatorRef => {
          NavigationService.setNavigator({
            topLevelNavigator: "Products",
            ref: navigatorRef
          });
        }}
      />
    );
  }
}
