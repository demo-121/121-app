import CryptoJS from "crypto-js";
import uuidv4 from "uuid/v4";

export const hash = data => CryptoJS.SHA256(data).toString(CryptoJS.enc.Hex);

export const SecureRandom = size => {
  const min = 1;
  const max = 33;
  let random = "";
  let output = "";

  while (output.length < size) {
    random = uuidv4();
    for (let i = 0; i < 5; i += 1) {
      const pos = Math.random() * (max - min) + min;
      let tmpChar = random.substr(pos, 1);
      tmpChar = tmpChar.toUpperCase();
      random = random.substr(0, pos - 1) + tmpChar + random.substr(pos + 1);
    }
    output += random.replace(/[^a-zA-Z0-9]+/g, "");
  }
  return output.substr(0, size);
};

export const aesGen256Key = () => SecureRandom(32); // 1 char = 8 bits

export const aesEncrypt = (data, key) => {
  let ciphertext = null;

  try {
    if (data != null && key != null) {
      ciphertext = CryptoJS.AES.encrypt(data, key);
    } else {
      throw Error("empty data or key");
    }
  } catch (ex) {
    throw ex;
  }
  return ciphertext.toString();
};

export const aesDecrypt = (ciphertext, key) => {
  let decrypted = null;

  try {
    decrypted = CryptoJS.AES.decrypt(ciphertext, key);
  } catch (ex) {
    throw ex;
  }
  return decrypted.toString(CryptoJS.enc.Utf8);
};
