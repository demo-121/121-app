import { connect } from "react-redux";
import { REDUCER_TYPES, ACTION_TYPES } from "eab-web-api";
import FNAReportAppBarRight from "../components/FNAReportAppBarRight";
import APP_ACTION_TYPES from "../../../constants/ACTION_TYPES";
import { TEXT_STORE, LOGIN, PAYMENT } from "../../../constants/REDUCER_TYPES";

const { FNA, CLIENT } = REDUCER_TYPES;

/**
 * FNAReportAppBarRight
 * @requires FNAReportAppBarRight - FNAReportAppBarRight UI
 * */
const mapStateToProps = state => ({
  isOnlineAuthCompleted: state[PAYMENT].isOnlineAuthCompleted,
  profile: state[CLIENT].profile,
  environment: state[LOGIN].environment,
  isHandlingListener: state[LOGIN].isHandlingListener,
  FNAReportEmail: state[FNA].FNAReportEmail,
  tab: state[FNA].FNAReportEmail.tab,
  textStore: state[TEXT_STORE]
});

const mapDispatchToProps = dispatch => ({
  openEmailDialog: callback => {
    dispatch({
      type: ACTION_TYPES[FNA].OPEN_EMAIL_DIALOG,
      callback
    });
  },
  checkBoxOnPress: ({ tab, option }) => {
    dispatch({
      type: ACTION_TYPES[FNA].REPORT_ON_CHANGE,
      tab,
      report: option.key,
      isSelected: option.isSelected
    });
  },
  checkTokenExpiryDate: callback => {
    dispatch({
      type: APP_ACTION_TYPES[LOGIN].CHECK_TOKEN_EXPIRY_DATE,
      callback
    });
  },
  checkValidDevice: callback => {
    dispatch({
      type: APP_ACTION_TYPES[LOGIN].CHECK_VALID_DEVICE,
      callback
    });
  },
  loginByOnlineAuthForPayment: () => {
    dispatch({
      type: APP_ACTION_TYPES[PAYMENT].LOGIN_BY_ONLINE_AUTH_FOR_PAYMENT
    });
  },
  setOnlineAuthCodeForPayment: ({ authCode, callback }) => {
    dispatch({
      type: APP_ACTION_TYPES[PAYMENT].ONLINE_AUTH_CODE_FOR_PAYMENT,
      authCode,
      callback
    });
  },
  sendEmail: callback => {
    dispatch({
      type: ACTION_TYPES[FNA].SEND_EMAIL,
      callback
    });
  },
  tabOnPress: option => {
    dispatch({
      type: ACTION_TYPES[FNA].TAB_ON_CHANGE,
      option
    });
  },
  updateSkipEmailInReport: value => {
    dispatch({
      type: ACTION_TYPES[FNA].UPDATE_SKIP_EMAIL_IN_REPORT,
      value
    });
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(FNAReportAppBarRight);
