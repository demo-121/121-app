import React, { PureComponent } from "react";
import { View, ScrollView } from "react-native";
import PropTypes from "prop-types";
import moment from "moment";
import * as _ from "lodash";
import { REDUCER_TYPES, REDUCER_TYPE_CHECK } from "eab-web-api";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";

import APP_REDUCER_TYPE_CHECK from "../../../../constants/REDUCER_TYPE_CHECK";
import { TEXT_STORE } from "../../../../constants/REDUCER_TYPES";
import EABSectionList from "../../../../components/EABSectionList";
import EABi18n from "../../../../utilities/EABi18n";
import { ContextConsumer } from "../../../../context";

import RecommendationFormNormal from "../../containers/RecommendationFormNormal";
import RecommendationFormShield from "../../containers/RecommendationFormShield";

import styles from "./styles";

const { RECOMMENDATION } = REDUCER_TYPES;

/**
 * Recommendation
 * @description Recommendation page.
 * @param {object} textStore - reducer textStore state
 * @param {object} value - data for Recommendation page
 * @param {object} completed - indicate the completeness of each quotation
 * @param {string} selectedQuotId - indicate quotation id of selected quotation
 * @param {func} updateSelectedQuotation - update Recommendation state with inputted data
 * @param {func} validateRecommendation - validation Recommendation when component is ready
 * */

class Recommendation extends PureComponent {
  constructor(props) {
    super(props);

    this.prepareSectionListData = this.prepareSectionListData.bind(this);

    this.state = {};
  }

  componentDidMount() {
    const { value, validateRecommendation } = this.props;
    const { chosenList } = value;
    validateRecommendation(chosenList.length > 0);
  }

  componentDidUpdate(prevProps) {
    const { value, validateRecommendation } = this.props;
    const { chosenList } = value;

    if (
      _.get(prevProps, "value.chosenList.length") === 0 &&
      chosenList.length > 0
    ) {
      validateRecommendation(chosenList.length > 0);
    }
  }

  prepareSectionListData(language, quotId) {
    const { value, completed, selectedQuotId, textStore } = this.props;

    return {
      key: `${quotId}-menuItem`,
      id: quotId,
      testID: `Product-${quotId}`,
      title: _.get(value, `[${quotId}].extra.basicPlanName`, ""),
      subtitle: _.replace(
        EABi18n({
          path: "recommendation.recommendation.proposedOn",
          language,
          textStore
        }),
        "{1}",
        moment(_.get(value, `[${quotId}].extra.lastUpdateDate`, "")).format(
          "DD MMM YYYY HH:mm"
        )
      ),
      selected: quotId === selectedQuotId,
      completed: completed[quotId]
    };
  }

  render() {
    const {
      value,
      selectedQuotId,
      textStore,
      updateSelectedQuotation
    } = this.props;
    const { chosenList, notChosenList } = value;
    const fullList = _.concat(chosenList, notChosenList);
    const isShield = _.get(value, `[${selectedQuotId}].extra.isShield`);
    if (
      (!selectedQuotId || selectedQuotId === "") &&
      (chosenList.length || notChosenList.length)
    ) {
      const id = chosenList.length ? chosenList[0] : notChosenList[0];
      updateSelectedQuotation(id);
    }
    return (
      <ContextConsumer>
        {({ language }) => {
          const chosenData = _.map(chosenList, quotId =>
            this.prepareSectionListData(language, quotId)
          );
          const sections = [
            {
              key: "chosenSection",
              data: chosenData,
              title: _.toUpper(
                EABi18n({
                  path: "recommendation.recommendation.section.chosenProposals",
                  language,
                  textStore
                })
              )
            }
          ];

          if (notChosenList.length > 0) {
            const notChosenData = _.map(notChosenList, quotId =>
              this.prepareSectionListData(language, quotId)
            );
            sections.push({
              key: "notChosenSection",
              data: notChosenData,
              title: _.toUpper(
                EABi18n({
                  path:
                    "recommendation.recommendation.section.notChosenProposals",
                  language,
                  textStore
                })
              )
            });
          }

          return (
            <View style={styles.container}>
              <View style={styles.sectionListView}>
                <EABSectionList
                  testID="PreAppilcation__Recommendation__LeftPanel"
                  sections={sections}
                  showCompleteIcon
                  showSubtitle
                  showSectionHeader
                  onPress={id => {
                    updateSelectedQuotation(id);
                  }}
                />
              </View>
              <View style={styles.mainWrapper}>
                <KeyboardAwareScrollView extraScrollHeight={100}>
                  {fullList.map(quotId => {
                    if (quotId !== selectedQuotId) {
                      return null;
                    }
                    return (
                      <ScrollView
                        testID="PreAppilcation__Recommandation__RightPanel__PageScroll"
                        key={`${selectedQuotId}-recommendation`}
                        style={styles.scrollView}
                        contentContainerStyle={styles.scrollViewContent}
                      >
                        {isShield ? (
                          <RecommendationFormShield quotId={selectedQuotId} />
                        ) : (
                          <RecommendationFormNormal quotId={selectedQuotId} />
                        )}
                      </ScrollView>
                    );
                  })}
                </KeyboardAwareScrollView>
              </View>
            </View>
          );
        }}
      </ContextConsumer>
    );
  }
}

Recommendation.propTypes = {
  textStore: APP_REDUCER_TYPE_CHECK[TEXT_STORE].isRequired,
  value: REDUCER_TYPE_CHECK[RECOMMENDATION].recommendation.value.isRequired,
  completed: PropTypes.objectOf(
    (propValue, key, componentName, location, propFullName) => {
      if (!_.isBoolean(propValue[key])) {
        return new Error(
          `Invalid prop \`${propFullName}\` supplied to` +
            ` \`${componentName}\`. Validation failed.`
        );
      }
      return null;
    }
  ).isRequired,
  selectedQuotId: PropTypes.string.isRequired,
  updateSelectedQuotation: PropTypes.func.isRequired,
  validateRecommendation: PropTypes.func.isRequired
};

export default Recommendation;
