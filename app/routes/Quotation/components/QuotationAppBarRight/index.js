import {
  REDUCER_TYPE_CHECK as WEB_API_TYPES_CHECK,
  REDUCER_TYPES as WEB_API_REDUCER_TYPE,
  utilities
} from "eab-web-api";
import * as _ from "lodash";
import PropTypes from "prop-types";
import React, { Component } from "react";
import {
  View,
  Text,
  ScrollView,
  KeyboardAvoidingView,
  Alert
} from "react-native";
import { StackActions } from "react-navigation";
import EABButton from "../../../../components/EABButton";
import EABCheckBox from "../../../../components/EABCheckBox";
import EABDialog from "../../../../components/EABDialog/EABDialog.ios";
import EABHUD from "../../../../containers/EABHUD";
import EABTextField from "../../../../components/EABTextField";
import SelectorField from "../../../../components/SelectorField";
import { FLAT_LIST } from "../../../../components/SelectorField/constants";
import REDUCER_TYPE_CHECK from "../../../../constants/REDUCER_TYPE_CHECK";
import { TEXT_STORE } from "../../../../constants/REDUCER_TYPES";
import TranslatedText from "../../../../containers/TranslatedText";
import { ContextConsumer } from "../../../../context";
import Theme from "../../../../theme";
import EABi18n from "../../../../utilities/EABi18n";
import getEABTextByLangType from "../../../../utilities/getEABTextByLangType";
import policyOptionOnChange from "../../utilities/policyOptionOnChange";
import { SHIELD } from "../QuotationUI/constants";
import styles from "./styles";

const { QUOTATION, OPTIONS_MAP } = WEB_API_REDUCER_TYPE;
const { getCurrencySign, fillTextVariable } = utilities.common;
const { numberToCurrency, currencyToNumber } = utilities.numberFormatter;

/**
 * <QuotationAppBarRight />
 * @description navigation bar right component
 * @param {function} quote - calc quotation
 * @param {function} resetQuot - reset quotation
 * @param {function} updateQuotation - update quotation reducer state
 * @param {function} createProposal - use quotation to create a proposal
 * */
export default class QuotationAppBarRight extends Component {
  constructor() {
    super();

    this.state = {
      isCreatingProposal: false,
      shouldShowOptions: false
    };
  }

  // ===========================================================================
  // getters
  // ===========================================================================
  get canNotCreateProposal() {
    const { quotation, quotationErrors } = this.props;

    return (
      !_.every(quotationErrors, errors => _.isEmpty(errors)) ||
      !quotation.premium
    );
  }

  /**
   * @description get optional policy options
   * @return {array} array of optional policy option object.
   * */
  get getOptionalPolicyOptionGroups() {
    const { inputConfigs, quotation } = this.props;
    const policyOptionsGroups = {};
    const optionalOptionArray = [];

    if (quotation.quotType !== SHIELD) {
      Object.entries(inputConfigs).forEach(inputConfig => {
        inputConfig[1].policyOptions.forEach(policyOption => {
          if (!Array.isArray(policyOptionsGroups[policyOption.groupID])) {
            policyOptionsGroups[policyOption.groupID] = [];
          }

          if (policyOption.type !== "emptyBlock") {
            policyOptionsGroups[policyOption.groupID].push(policyOption);
          }
        });
      });

      Object.entries(policyOptionsGroups).forEach(policyOptionsGroup => {
        /* get group first field */
        const firstField = policyOptionsGroup[1][0];

        if (firstField.type === "checkbox") {
          optionalOptionArray.push(policyOptionsGroup);
        }
      });
    }

    return optionalOptionArray;
  }

  get sign() {
    const { quotation, optionsMap } = this.props;

    return getCurrencySign({
      compCode: quotation.compCode,
      ccy: quotation.ccy,
      optionsMap
    });
  }

  render() {
    // =========================================================================
    // variables
    // =========================================================================
    const { canNotCreateProposal, getOptionalPolicyOptionGroups, sign } = this;
    const {
      createProposal,
      textStore,
      quotation,
      quote,
      quotationErrors,
      resetQuot,
      updateQuotation
    } = this.props;
    const { shouldShowOptions, isCreatingProposal } = this.state;
    // =========================================================================
    // render functions
    // =========================================================================
    const renderOptionsButton = language =>
      getOptionalPolicyOptionGroups.map(optionGroup => {
        const groupName = optionGroup[0];
        const groupItem = optionGroup[1];
        const [checkBoxItem, ...groupFields] = groupItem;
        const { policyOptions } = quotation;
        const { withdrawalOption, numOfWD } = policyOptions;

        const getErrorMessage = policyOption => {
          if (quotationErrors) {
            const { mandatoryErrors, policyOptionErrors } = quotationErrors;
            if (mandatoryErrors && mandatoryErrors[policyOption.id]) {
              return EABi18n({
                path: "error.302",
                language,
                textStore
              });
            } else if (
              policyOptionErrors &&
              policyOptionErrors[policyOption.id]
            ) {
              const error = policyOptionErrors[policyOption.id];
              if (error.msg) {
                return error.msg;
              } else if (error.code) {
                const errorMessage = EABi18n({
                  language,
                  textStore,
                  path: `quotation.error.${error.code}`
                });
                const params =
                  error.msgPara &&
                  error.msgPara.map(variable => ({
                    value: getEABTextByLangType({ language, data: variable }),
                    isPath: false
                  }));

                if (errorMessage) {
                  return fillTextVariable({
                    text: errorMessage,
                    data: params,
                    EABi18n,
                    language,
                    textStore
                  });
                }
              }
            }
          }
          return "";
        };

        return {
          key: groupName,
          text: getEABTextByLangType({ data: checkBoxItem.title, language }),
          isSelected: !!quotation.policyOptions[checkBoxItem.id],
          policyOption: checkBoxItem,
          disable: checkBoxItem.disable === "Y",
          label: quotation.policyOptions[checkBoxItem.id] ? (
            <View style={styles.groupFields}>
              {groupFields.map(policyOption => {
                switch (policyOption.type) {
                  case "picker": {
                    const pickerOptions = [];
                    policyOption.options.forEach(option => {
                      pickerOptions.push({
                        label: getEABTextByLangType({
                          data: option.title,
                          language
                        }),
                        key: option.value,
                        value: option.value
                      });
                    });

                    /* get label */
                    /* TODO need refactor, hard code */
                    let value = "-";
                    if (pickerOptions.length) {
                      value =
                        quotation.policyOptions[policyOption.id] ||
                        EABi18n({ textStore, language, path: "button.select" });
                    }

                    return (
                      <SelectorField
                        key={policyOption.id}
                        style={styles.field}
                        value={value}
                        selectorType={FLAT_LIST}
                        placeholder={getEABTextByLangType({
                          data: policyOption.title,
                          language
                        })}
                        flatListOptions={{
                          data: pickerOptions,
                          renderItemOnPress: (key, item) => {
                            policyOptionOnChange({
                              quotation,
                              quote,
                              resetQuot,
                              textStore,
                              language,
                              policyOption,
                              newValue: item.value
                            });
                          },
                          selectedValue: value
                        }}
                        isError={
                          quotationErrors.mandatoryErrors &&
                          (quotationErrors.mandatoryErrors[policyOption.id] !==
                            undefined ||
                            quotationErrors.policyOptionErrors[
                              policyOption.id
                            ] !== undefined)
                        }
                        hintText={getErrorMessage(policyOption)}
                        editable={policyOption.disable !== "Y"}
                      />
                    );
                  }
                  case "note": {
                    const withdrawalIndex =
                      Number(policyOption.id[policyOption.id.length - 1]) || 0;

                    // Withdrawal Conditions
                    if (
                      (_.isEmpty(withdrawalOption) ||
                        withdrawalOption === "none" ||
                        _.isEmpty(numOfWD) ||
                        withdrawalIndex > Number(numOfWD)) &&
                      policyOption.subGroupID === "numOfWD"
                    ) {
                      return null;
                    }

                    return (
                      <View
                        key={policyOption.id}
                        style={styles.configFieldFullWidth}
                      >
                        <Text
                          style={[
                            Theme.bodyPrimary,
                            { paddingTop: Theme.alignmentXL }
                          ]}
                        >
                          {policyOption.note}
                        </Text>
                      </View>
                    );
                  }
                  case "linebreak":
                    return <View style={styles.configFieldFullWidth} />;

                  case "text": {
                    const withdrawalIndex =
                      Number(policyOption.id[policyOption.id.length - 1]) || 0;

                    // Withdrawal Conditions
                    if (
                      (_.isEmpty(withdrawalOption) ||
                        withdrawalOption === "none" ||
                        _.isEmpty(numOfWD) ||
                        withdrawalIndex > Number(numOfWD)) &&
                      policyOption.subGroupID === "numOfWD"
                    ) {
                      return null;
                    }

                    let maxLength = 20;
                    if (policyOption.subType === "currency") {
                      maxLength = sign.length;

                      if (policyOption.maxLength) {
                        maxLength +=
                          policyOption.maxLength +
                          Math.floor(policyOption.maxLength / 3);
                      } else {
                        maxLength += 10 + Math.floor(10 / 3);
                      }
                    } else if (policyOption.groupID === "withdrawal" || "wd1") {
                      maxLength = 2;
                    }

                    let quoteValue = `${
                      quotation.policyOptions[policyOption.id]
                    }`;
                    if (policyOption.subType === "currency") {
                      quoteValue = numberToCurrency({
                        value: quotation.policyOptions[policyOption.id],
                        decimal: 0
                      });
                    } else if (policyOption.subType === "number") {
                      quoteValue = numberToCurrency({
                        value: quotation.policyOptions[policyOption.id],
                        decimal: Number(policyOption.textDecimalPlace)
                      });
                    }

                    return (
                      <EABTextField
                        key={policyOption.id}
                        style={
                          policyOption.id === "invRateForWD"
                            ? styles.fullField
                            : styles.field
                        }
                        value={
                          quotation.policyOptions[policyOption.id] !== null
                            ? quoteValue
                            : ""
                        }
                        placeholder={getEABTextByLangType({
                          data: policyOption.title,
                          language
                        })}
                        onChange={value => {
                          if (
                            policyOption.max &&
                            Number(value) > policyOption.max
                          ) {
                            value = policyOption.max.toString();
                          }

                          let calValue = value;
                          if (policyOption.subType === "currency") {
                            calValue = currencyToNumber(value);
                          } else if (policyOption.subType === "number") {
                            calValue = numberToCurrency({
                              value: Number(value),
                              decimal: Number(policyOption.textDecimalPlace)
                            });
                          } else {
                            calValue = value.replace(/[^0-9]/g, "");
                          }

                          updateQuotation({
                            ...quotation,
                            policyOptions: {
                              ...quotation.policyOptions,
                              [policyOption.id]: calValue
                            }
                          });
                        }}
                        onBlur={value => {
                          quote({
                            ...quotation,
                            policyOptions: {
                              ...quotation.policyOptions,
                              [policyOption.id]: Number(
                                numberToCurrency({
                                  value: value.replace(/,/g, ""),
                                  decimal:
                                    policyOption.textDecimalPlace === ""
                                      ? 0
                                      : Number(policyOption.textDecimalPlace)
                                })
                              )
                            }
                          });
                        }}
                        prefix={policyOption.subType === "currency" ? sign : ""}
                        maxLength={policyOption.maxLength || maxLength}
                        editable={policyOption.disable !== "Y"}
                        isRequired={policyOption.mandatory === "Y"}
                        isError={
                          quotationErrors.mandatoryErrors &&
                          (quotationErrors.mandatoryErrors[policyOption.id] !==
                            undefined ||
                            quotationErrors.policyOptionErrors[
                              policyOption.id
                            ] !== undefined)
                        }
                        hintText={getErrorMessage(policyOption)}
                        toolTip={policyOption.hintMsg}
                        keyboardType="numeric"
                      />
                    );
                  }
                  default:
                    throw new Error(`unexpected type ${policyOption.type}`);
                }
              })}
            </View>
          ) : null
        };
      });

    return (
      <ContextConsumer>
        {({ language }) =>
          _.has(quotation, "type") ? (
            <View style={styles.container}>
              {getOptionalPolicyOptionGroups.length > 0 ? (
                <EABButton
                  testID="Quotation__btnOptions"
                  key="button"
                  containerStyle={styles.optionsButton}
                  onPress={() => {
                    this.setState({
                      shouldShowOptions: true
                    });
                  }}
                >
                  <Text style={Theme.textButtonLabelNormalAccent}>
                    {EABi18n({
                      textStore,
                      language,
                      path: "quotation.button.options"
                    })}
                  </Text>
                </EABButton>
              ) : null}
              <EABButton
                testID="Quotation__btnCreateProposal"
                onPress={() => {
                  this.setState({ isCreatingProposal: true }, () => {
                    const callback = () => {
                      this.setState({ isCreatingProposal: false });
                      this.props.navigation.dispatch(
                        StackActions.push({ routeName: "ProposalUI" })
                      );
                    };
                    createProposal({
                      callback,
                      warningCallback: text => {
                        Alert.alert(null, text, [
                          {
                            text: EABi18n({
                              language,
                              textStore,
                              path: "button.ok"
                            }),
                            onPress: callback
                          }
                        ]);
                      },
                      errorCallback: text => {
                        Alert.alert(
                          EABi18n({
                            textStore,
                            language,
                            path: "quotation.error.title"
                          }),
                          text,
                          [
                            {
                              text: EABi18n({
                                language,
                                textStore,
                                path: "button.ok"
                              }),
                              onPress: () => {
                                this.setState({ isCreatingProposal: false });
                              }
                            }
                          ]
                        );
                      }
                    });
                  });
                }}
                isDisable={canNotCreateProposal}
              >
                <TranslatedText
                  style={Theme.textButtonLabelNormalEmphasizedAccent}
                  path="quotation.button.createProposal"
                />
              </EABButton>
              <EABDialog key="dialog" isOpen={shouldShowOptions}>
                <View style={Theme.dialogHeader}>
                  <View style={styles.optionDialog} />
                  <View style={styles.optionDialogMiddle}>
                    <Text style={Theme.title}>
                      {EABi18n({
                        textStore,
                        language,
                        path: "quotation.button.options"
                      })}
                    </Text>
                  </View>
                  <View style={styles.optionDialogRight}>
                    <EABButton
                      testID="Quotation__btnDone"
                      onPress={() => {
                        this.setState({ shouldShowOptions: false });
                      }}
                      isDisable={this.isDisableSaveClient}
                    >
                      <Text style={Theme.textButtonLabelNormalAccent}>
                        {EABi18n({
                          textStore,
                          language,
                          path: "button.done"
                        })}
                      </Text>
                    </EABButton>
                  </View>
                </View>
                <View style={styles.dialogContentWrapper}>
                  <KeyboardAvoidingView
                    behavior="padding"
                    keyboardVerticalOffset={85}
                  >
                    <ScrollView style={styles.optionWrapper}>
                      <EABCheckBox
                        style={styles.policyOptionWrapper}
                        options={renderOptionsButton(language)}
                        onPress={option => {
                          if (option.disable) return;
                          policyOptionOnChange({
                            quotation,
                            quote,
                            resetQuot,
                            textStore,
                            language,
                            policyOption: option.policyOption,
                            newValue: !option.isSelected
                          });
                        }}
                      />
                    </ScrollView>
                  </KeyboardAvoidingView>
                </View>
              </EABDialog>
              <EABHUD isOpen={isCreatingProposal} />
            </View>
          ) : null
        }
      </ContextConsumer>
    );
  }
}

QuotationAppBarRight.propTypes = {
  quotation: WEB_API_TYPES_CHECK[QUOTATION].quotation.isRequired,
  inputConfigs: WEB_API_TYPES_CHECK[QUOTATION].inputConfigs.isRequired,
  optionsMap: WEB_API_TYPES_CHECK[OPTIONS_MAP].isRequired,
  quotationErrors: WEB_API_TYPES_CHECK[QUOTATION].quotationErrors.isRequired,
  textStore: REDUCER_TYPE_CHECK[TEXT_STORE].isRequired,
  quote: PropTypes.func.isRequired,
  resetQuot: PropTypes.func.isRequired,
  updateQuotation: PropTypes.func.isRequired,
  createProposal: PropTypes.func.isRequired
};
