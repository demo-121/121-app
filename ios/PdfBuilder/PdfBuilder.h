//
//  PdfBuilder.h
//  EASE
//
//  Created by Osric Wong on 10/5/2018.
//  Copyright © 2018 Facebook. All rights reserved.
//

#define kPageSizeA4 CGSizeMake(595.2, 841.8)  // paper size for general use
#define kPageSizeA4_eapp CGSizeMake(635.2, 841.8)  // page size for eapp form use, due to different width
//
//typedef void (^PdfBuilderSuccessBlock)(NSData *pdfData, NSNumber *pageCount);
//typedef void (^PdfBuilderFailureBlock)(NSError *error);
@class UIWebView;
@class UIView;

@interface PdfBuilder : NSObject

+ (id)createPdfWithHtml:(UIView *)view webView:(UIWebView *)webView html:(NSString *)html;

+ (id)createPdfWithUrl:(UIView *)view webView:(UIWebView *)webView url:(NSURL *)url;

@end
