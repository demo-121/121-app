import PropTypes from "prop-types";
import React, { PureComponent } from "react";
import { View } from "react-native";
import styles from "./styles";

/**
 * <EABBottomSheet />
 * @description bottom sheet component. please place this component to the page
 *   bottom.
 * @param {number} expandHeight - height of expand mode bottom sheet
 * @param {number} collapseHeight - height of collapse mode bottom sheet
 * @param {boolean} isExpand - indicates whether the bottom sheet is open or
 *   not
 * @param {object|number=} style - custom style of bottom sheet
 * @param {object|number=} containerStyle - custom container style of bottom
 *   sheet
 * */
export default class EABBottomSheet extends PureComponent {
  render() {
    return (
      <View
        style={[
          styles.placeholder,
          ...(Array.isArray(this.props.containerStyle)
            ? this.props.containerStyle
            : [this.props.containerStyle]),
          { height: this.props.collapseHeight }
        ]}
      >
        <View
          style={[
            styles.container,
            ...(Array.isArray(this.props.style)
              ? this.props.style
              : [this.props.style]),
            {
              height: this.props.isExpand
                ? this.props.expandHeight
                : this.props.collapseHeight
            }
          ]}
        >
          {this.props.children}
        </View>
      </View>
    );
  }
}

EABBottomSheet.propTypes = {
  expandHeight: PropTypes.number.isRequired,
  collapseHeight: PropTypes.number.isRequired,
  isExpand: PropTypes.bool.isRequired,
  style: PropTypes.oneOfType([
    PropTypes.number,
    PropTypes.object,
    PropTypes.array
  ]),
  containerStyle: PropTypes.oneOfType([
    PropTypes.number,
    PropTypes.object,
    PropTypes.array
  ]),
  children: PropTypes.node.isRequired
};

EABBottomSheet.defaultProps = {
  style: {},
  containerStyle: {}
};
