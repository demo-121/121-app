import { connect } from "react-redux";
import { REDUCER_TYPES } from "eab-web-api";
import SelectorField from "../../components/DynamicViews/SelectorField";

const { OPTIONS_MAP } = REDUCER_TYPES;
/**
 * SelectorField
 * @description SelectorField in DynamicViews
 * */
const mapStateToProps = state => ({
  optionsMap: state[OPTIONS_MAP]
});

export default connect(mapStateToProps)(SelectorField);
