import * as _ from "lodash";
import { REDUCER_TYPE_CHECK, REDUCER_TYPES, utilities } from "eab-web-api";
import PropTypes from "prop-types";
import React, { PureComponent } from "react";
import {
  Image,
  ScrollView,
  Text,
  View,
  TouchableHighlight
} from "react-native";
import moment from "moment";
import LinearGradient from "react-native-linear-gradient";
import { StackActions } from "react-navigation";
import quote from "../../../../assets/images/quote.png";
import EABCard from "../../../../components/EABCard";
import EABCheckBox from "../../../../components/EABCheckBox";
import EABDialog from "../../../../components/EABDialog";
import { SCREEN } from "../../../../components/EABDialog/constants";
import APP_REDUCER_TYPES_CHECK from "../../../../constants/REDUCER_TYPE_CHECK";
import { TEXT_STORE } from "../../../../constants/REDUCER_TYPES";
import EABHUD from "../../../../containers/EABHUD";
import TranslatedText from "../../../../containers/TranslatedText";
import { ContextConsumer } from "../../../../context";
import Quotation from "../../../Quotation";
import Theme from "../../../../theme";
import getEABTextByLangType, {
  languageTypeConvert
} from "../../../../utilities/getEABTextByLangType";
import styles from "./styles";

const { common } = utilities;
const { getOptionTitle } = common;
const { CLIENT, OPTIONS_MAP, QUOTATION } = REDUCER_TYPES;

/**
 * <QuickQuoteHistory />
 * @param textStore - reducer textStore state
 * @param quickQuotes - quick quote list
 * @param {function} quickQuoteOnChange - change quickQuote state
 * @param {function} openProposal - saga action, open Proposal page
 * @param {function} queryQuickQuote - get quick quote list
 * */
export default class QuickQuoteHistory extends PureComponent {
  constructor(props) {
    super(props);

    this.quickQuoteItemOnPress = this.quickQuoteItemOnPress.bind(this);

    this.state = {
      isLoading: true
    };
  }
  // ===========================================================================
  // life circles
  // ===========================================================================
  componentDidMount() {
    const { deleteQuickQuote, navigation } = this.props;
    navigation.setParams({
      deleteQuickQuote
    });
    this.props.queryQuickQuote(() => {
      this.setState({
        isLoading: false
      });
    });
  }

  quickQuoteItemOnPress({ quickQuote, showQuotationNavigator }) {
    const { navigation, openProposal } = this.props;
    const selectModeOn = navigation.getParam("selectModeOn", false);
    const quotationId = quickQuote.key;

    if (selectModeOn) {
      let selectedIdList = navigation.getParam("selectedIdList", []);

      if (selectedIdList.indexOf(quotationId) < 0) {
        selectedIdList.push(quotationId);
      } else {
        selectedIdList = _.filter(selectedIdList, id => id !== quotationId);
      }

      navigation.setParams({ selectedIdList });
    } else {
      this.setState(
        {
          isLoading: true
        },
        () => {
          openProposal({
            quotationId,
            callback: () => {
              this.setState({ isLoading: false }, () => {
                this.props.navigation.dispatch(
                  StackActions.push({ routeName: "ProposalUI" })
                );
                showQuotationNavigator();
              });
            }
          });
        }
      );
    }
  }

  render() {
    // =========================================================================
    // variables
    // =========================================================================
    const { navigation, quickQuotes, setAvoidNavigate } = this.props;
    const { isLoading } = this.state;
    // =========================================================================
    // render functions
    // =========================================================================
    /**
     * @description map reducer data to UI structure
     * @return {array} quick quotes data
     * */
    const quickQuotesLayout = language => {
      const returnArray = [];

      quickQuotes.forEach(quickQuote => {
        returnArray.push({
          key: quickQuote.id,
          productName: getEABTextByLangType({
            data: quickQuote.covName,
            language
          }),
          proposer: quickQuote.pFullName,
          lifeAssured: quickQuote.iFullName,
          lastUpdateDate: quickQuote.lastUpdateDate
        });
      });

      return returnArray;
    };

    return (
      <ContextConsumer>
        {({
          language,
          isShowingQuotationNavigator,
          showQuotationNavigator
        }) => (
          <LinearGradient
            {...Theme.LinearGradientProps}
            style={styles.container}
          >
            <EABHUD isOpen={isLoading} />
            <ScrollView
              style={styles.scrollView}
              contentContainerStyle={styles.scrollViewContent}
            >
              <View style={styles.quickQuotesWrapper}>
                {quickQuotesLayout(language).map(quickQuote => (
                  <View key={quickQuote.key} style={styles.quickQuote}>
                    <TouchableHighlight
                      underlayColor={Theme.brandSuperTransparent}
                      onPress={() => {
                        setAvoidNavigate(true);
                        this.quickQuoteItemOnPress({
                          quickQuote,
                          showQuotationNavigator
                        });
                      }}
                    >
                      <EABCard
                        style={styles.quickQuoteCard}
                        contentStyle={styles.quickQuoteContent}
                      >
                        <View style={styles.mainWrapper}>
                          <View style={styles.productTitle}>
                            <Text style={Theme.headlinePrimary}>
                              {quickQuote.productName}
                            </Text>
                          </View>
                          <View style={styles.infoRow}>
                            <TranslatedText
                              style={Theme.bodyPrimary}
                              path="quickQuote.PINumber"
                              funcOnText={text => `${text}: `}
                            />
                            <Text style={Theme.bodySecondary}>
                              {quickQuote.key}
                            </Text>
                          </View>
                          <View style={styles.infoRow}>
                            <View style={styles.laTitle}>
                              <TranslatedText
                                style={Theme.bodyPrimary}
                                path="quickQuote.lifeAssured"
                                funcOnText={text => `${text}: `}
                              />
                            </View>
                            <View style={styles.laLists}>
                              <Text style={Theme.bodySecondary}>
                                {quickQuote.lifeAssured}
                              </Text>
                            </View>
                          </View>
                          <View style={styles.infoRow}>
                            <TranslatedText
                              style={Theme.bodyPrimary}
                              path="quickQuote.proposer"
                              funcOnText={text => `${text}: `}
                            />
                            <Text style={Theme.bodySecondary}>
                              {quickQuote.proposer}
                            </Text>
                          </View>
                          <View style={styles.infoRow}>
                            <Text style={Theme.bodyPrimary}>
                              {this.props.profile.idDocType === "other"
                                ? this.props.profile.idDocTypeOther
                                : getOptionTitle({
                                    value: this.props.profile.idDocType,
                                    optionsMap: this.props.optionsMap.idDocType,
                                    language: languageTypeConvert(language)
                                  })}:{" "}
                            </Text>

                            <Text style={Theme.bodySecondary}>
                              {this.props.profile.idCardNo}
                            </Text>
                          </View>
                          <View style={styles.infoRow}>
                            <Text style={Theme.bodySecondary}>
                              {moment(
                                new Date(quickQuote.lastUpdateDate)
                              ).format("DD/MM/YYYY HH:mm:ss")}
                            </Text>
                          </View>
                        </View>
                        {navigation.getParam("selectModeOn", false) ? (
                          <EABCheckBox
                            style={styles.checkBoxRoot}
                            checkBoxStyle={styles.checkBox}
                            options={[
                              {
                                key: quickQuote.key,
                                isSelected:
                                  navigation
                                    .getParam("selectedIdList", [])
                                    .indexOf(quickQuote.key) >= 0
                              }
                            ]}
                            onPress={() => {
                              this.quickQuoteItemOnPress({
                                quickQuote,
                                showQuotationNavigator
                              });
                            }}
                          />
                        ) : (
                          <Image style={styles.quoteIcon} source={quote} />
                        )}
                      </EABCard>
                    </TouchableHighlight>
                  </View>
                ))}
              </View>
            </ScrollView>
            <EABDialog type={SCREEN} isOpen={isShowingQuotationNavigator}>
              <Quotation />
            </EABDialog>
          </LinearGradient>
        )}
      </ContextConsumer>
    );
  }
}

QuickQuoteHistory.propTypes = {
  textStore: APP_REDUCER_TYPES_CHECK[TEXT_STORE].isRequired,
  quickQuotes: REDUCER_TYPE_CHECK[QUOTATION].quickQuotes.isRequired,
  openProposal: PropTypes.func.isRequired,
  queryQuickQuote: PropTypes.func.isRequired,
  deleteQuickQuote: PropTypes.func.isRequired,
  setAvoidNavigate: PropTypes.func.isRequired,
  optionsMap: REDUCER_TYPE_CHECK[OPTIONS_MAP].isRequired,
  profile: REDUCER_TYPE_CHECK[CLIENT].profile.isRequired
};
