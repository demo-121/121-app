import { connect } from "react-redux";
import { REDUCER_TYPES, ACTION_TYPES } from "eab-web-api";
import { TEXT_STORE } from "../../../constants/REDUCER_TYPES";
import FinancialEvaluation from "../components/FinancialEvaluation";

const { FNA, CLIENT } = REDUCER_TYPES;

/**
 * FinancialEvaluation
 * @requires FinancialEvaluation - FinancialEvaluation UI
 * */
const mapStateToProps = state => ({
  textStore: state[TEXT_STORE],
  profile: state[CLIENT].profile,
  dependantProfiles: state[CLIENT].dependantProfiles,
  fe: state[FNA].fe,
  feErrors: state[FNA].feErrors,
  pda: state[FNA].pda
});

const mapDispatchToProps = dispatch => ({
  updateFE({ feData, profileData, pdaData, dependantProfilesData }) {
    dispatch({
      type: ACTION_TYPES[FNA].UPDATE_FE,
      feData,
      profileData,
      dependantProfilesData,
      pdaData
    });
  },
  saveFE({ confirm, callback }) {
    dispatch({
      type: ACTION_TYPES[FNA].SAVE_FE,
      confirm,
      callback
    });
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(FinancialEvaluation);
