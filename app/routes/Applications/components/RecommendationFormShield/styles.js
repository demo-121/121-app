import { StyleSheet } from "react-native";
import Theme from "../../../../theme";

/**
 * styles
 * */
export default StyleSheet.create({
  container: {
    width: 664,
    marginLeft: Theme.alignmentXL,
    marginRight: Theme.alignmentXL
  },
  header: {
    ...Theme.headerBrand,
    marginTop: Theme.alignmentXL,
    marginBottom: Theme.alignmentL
  },
  plansHeader: {
    ...Theme.captionBrand,
    marginTop: Theme.alignmentL,
    marginBottom: Theme.alignmentL,
    textDecorationLine: "underline"
  },
  blockView: {
    justifyContent: "space-between",
    borderColor: Theme.lightGrey,
    borderBottomWidth: 1
  },
  blockViewBottom: {
    justifyContent: "space-between",
    borderColor: Theme.lightGrey,
    borderBottomWidth: 1,
    paddingBottom: Theme.alignmentXL
  },
  blockViewNoBorder: {
    justifyContent: "space-between"
  },
  planDetailBlockView: {
    flexDirection: "row",
    justifyContent: "space-between",
    borderColor: Theme.lightGrey,
    borderBottomWidth: 1
  },
  planDetailContentView: {
    maxWidth: 541,
    marginBottom: Theme.alignmentXL
  },
  planDetailContentViewRow: {
    flexDirection: "row",
    justifyContent: "flex-start"
  },
  planDetailContentViewRowShield: {
    justifyContent: "flex-start"
  },
  planDetailButtonView: {
    marginTop: Theme.alignmentXL
  },
  title: {
    marginTop: Theme.alignmentXL
  },
  subTitle: {
    ...Theme.subheadSecondary,
    marginBottom: Theme.alignmentXL
  },
  planDetailTitle: {
    ...Theme.bodyPrimary,
    marginRight: Theme.alignmentXXS
  },
  planDetailTitleShield: {
    ...Theme.bodyPrimary,
    width: 416,
    marginRight: Theme.alignmentXXS
  },
  planDetailContent: {
    ...Theme.bodySecondary,
    maxWidth: 420
  },
  planDetailContentShield: {
    ...Theme.bodySecondary,
    width: 664
  },
  textBox: {
    alignItems: "stretch",
    marginTop: Theme.alignmentL,
    // marginBottom: Theme.alignmentXL,
    backgroundColor: Theme.white
  },
  textSelectionTitle: {
    ...Theme.bodyPrimary,
    marginBottom: Theme.alignmentXXS / 2
  },
  textSelection: {
    marginTop: 14,
    marginBottom: Theme.alignmentXL
  },
  warningView: {
    marginBottom: Theme.alignmentXL
  },
  warningStatementView: {
    flexDirection: "row",
    justifyContent: "flex-start"
  },
  warningStatementPoint: {
    ...Theme.bodyNegative,
    flexBasis: 18,
    marginRight: 6,
    marginBottom: Theme.alignmentXS
  },
  warningStatement: {
    ...Theme.bodyNegative,
    marginBottom: Theme.alignmentXS
  },
  errorMsg: {
    ...Theme.fieldTextOrHelperTextNegative
  }
});
