module.exports = {
  "extends": ["airbnb", "prettier", "prettier/react"],
  "plugins": ["prettier"],
  "rules": {
    "prettier/prettier": "error",
    /**
     * ignore react navigation, redux
     * */
    "react/prop-types": [2, { "ignore": ["navigation", "dispatch"] }],
    /**
     * make js support jsx
     * */
    "react/jsx-filename-extension": [1, { "extensions": [".js", ".jsx"] }],
    /*TODO for reducer reassign to state. remove or edit it when eslint update*/
    "no-param-reassign": [0],
    "no-eval": 0,
    /**
     * ignore string
     * */
    "max-len": [
      "error",
      {
        "code": 80,
        "ignoreRegExpLiterals": true,
        "ignoreTemplateLiterals": true,
        "ignoreStrings": true,
        "ignoreTrailingComments": true,
        "ignoreComments": true
      }
    ],
    /**
     * for couch base
     * */
    "no-underscore-dangle": 0
  }
};
