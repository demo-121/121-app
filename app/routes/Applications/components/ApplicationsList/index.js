import React, { PureComponent } from "react";
import { View } from "react-native";
import PropTypes from "prop-types";
import * as _ from "lodash";

import { REDUCER_TYPES, REDUCER_TYPE_CHECK } from "eab-web-api";

import ApplicationItemHeader from "../../containers/ApplicationItemHeader";
import ApplicationItemDetails from "../../containers/ApplicationItemDetails";
import ApplicationItemShieldDetails from "../../containers/ApplicationItemShieldDetails";
import styles from "./styles";

const { PRE_APPLICATION } = REDUCER_TYPES;

/**
 * ApplicationsList
 * @description ApplicationsList in Application Summary page.
 * @param {object} navigation - react navigation props
 * @param {array} applicationsList - array of application / quotation item object
 * @param {string} selectedAppListFilter - indicate which filter is selected
 * @param {func} filterApplicationItems - condition to filter application list by selected filter
 * */

class ApplicationsList extends PureComponent {
  componentDidUpdate(prevProps) {
    const { navigation, selectedIdList, applicationsList } = this.props;
    if (prevProps.selectedIdList.length !== selectedIdList.length) {
      const containFullySignedCase =
        _.findIndex(applicationsList, app => app.isFullySigned) >= 0;

      navigation.setParams({
        containFullySignedCase
      });
    }
  }

  render() {
    const {
      applicationsList,
      selectedAppListFilter,
      filterApplicationItems,
      navigation
    } = this.props;

    const sortedApplicationsList = applicationsList.sort((a, b) => {
      const aTime = a.lastUpdateDate;
      const bTime = b.lastUpdateDate;
      if (aTime < bTime) return 1;
      if (aTime > bTime) return -1;
      return 0;
    });

    return (
      <View style={styles.container}>
        {applicationsList.length > 0
          ? sortedApplicationsList.map((item, index) => {
              if (
                filterApplicationItems({
                  item,
                  selectedFilter: selectedAppListFilter
                })
              ) {
                if (item.quotType === "SHIELD") {
                  return (
                    <View key={`${item.id}`}>
                      <ApplicationItemHeader
                        value={item}
                        navigation={navigation}
                        rowIndex={index}
                      />
                      <ApplicationItemShieldDetails value={item} />
                    </View>
                  );
                }
                return (
                  <View key={`${item.id}`}>
                    <ApplicationItemHeader
                      value={item}
                      navigation={navigation}
                      rowIndex={index}
                    />
                    <ApplicationItemDetails value={item} />
                  </View>
                );
              }
              return null;
            })
          : null}
      </View>
    );
  }
}

ApplicationsList.propTypes = {
  applicationsList: PropTypes.arrayOf(
    REDUCER_TYPE_CHECK[PRE_APPLICATION].ApplicationItem.value.isRequired
  ).isRequired,
  selectedAppListFilter: PropTypes.string.isRequired,
  selectedIdList: PropTypes.arrayOf(PropTypes.string).isRequired,
  filterApplicationItems: PropTypes.func.isRequired
};

export default ApplicationsList;
