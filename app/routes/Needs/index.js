import React from "react";
import { Text } from "react-native";
import { createStackNavigator, createAppContainer } from "react-navigation";
import EABButton from "../../components/EABButton";
import { ContextConsumer } from "../../context";
import Theme from "../../theme";
import cleanClientData from "../../utilities/cleanClientData";
import LearnMore from "./containers/LearnMore";
import NeedsDetail from "./containers/NeedsDetail";
import NeedsDetailAppBarRight from "./containers/NeedsDetailAppBarRight";
import FNASummary from "./containers/FNASummary";
import NavigationService from "../../utilities/NavigationService";

const NeedsNavigator = createStackNavigator(
  {
    NeedsDetail: {
      screen: NeedsDetail,
      navigationOptions: ({ navigation }) => ({
        title: "Needs",
        headerLeft: (
          <ContextConsumer>
            {({ hideClientNavigator }) => (
              <EABButton
                style={{
                  marginLeft: Theme.alignmentXL
                }}
                onPress={() => {
                  hideClientNavigator();
                  cleanClientData(navigation.dispatch);
                  NavigationService.clearNavigationRef();
                }}
              >
                <Text style={Theme.textButtonLabelNormalAccent}>Close</Text>
              </EABButton>
            )}
          </ContextConsumer>
        ),
        headerRight: <NeedsDetailAppBarRight navigation={navigation} />
      })
    },
    LearnMore: {
      screen: LearnMore,
      navigationOptions: {
        title: "Learn More"
      }
    },
    FNASummary: {
      screen: FNASummary,
      navigationOptions: {
        title: "FNA Summary"
      }
    }
  },
  {
    initialRouteName: "NeedsDetail",
    defaultNavigationOptions: {
      headerTitleStyle: Theme.title,
      headerTintColor: Theme.textButtonLabelNormalAccent.color
    }
  }
);

export default createAppContainer(NeedsNavigator);
