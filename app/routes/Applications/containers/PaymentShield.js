import { connect } from "react-redux";
import { REDUCER_TYPES, ACTION_TYPES } from "eab-web-api";
import APP_ACTION_TYPES from "../../../constants/ACTION_TYPES";
import { LOGIN, TEXT_STORE } from "../../../constants/REDUCER_TYPES";
import PaymentShield from "../components/PaymentAndSubmissionShield/paymentShield";
import { setLastDataSyncTime } from "../../../actions/login";
/**
 * PaymentShield
 * @description PaymentShield in Application Summary page.
 * @requires PaymentShield - PaymentShield UI
 * */
const mapStateToProps = state => ({
  optionsMap: state[REDUCER_TYPES.OPTIONS_MAP],
  application: state[REDUCER_TYPES.APPLICATION].application,
  payment: state[REDUCER_TYPES.PAYMENT_AND_SUBMISSION].payment,
  template: state[REDUCER_TYPES.PAYMENT_AND_SUBMISSION].template,
  error: state[REDUCER_TYPES.PAYMENT_AND_SUBMISSION].error,
  isDisableEappButton:
    state[REDUCER_TYPES.APPLICATION].component.isDisableEappButton,
  textStore: state[TEXT_STORE]
});

const mapDispatchToProps = dispatch => ({
  getPayment: docId => {
    dispatch({
      type: ACTION_TYPES[REDUCER_TYPES.PAYMENT_AND_SUBMISSION].SAGA_GET_PAYMENT,
      docId
    });
  },
  updatePayment: ({ path, newValue, validateObj, isShield, callback }) => {
    dispatch({
      type:
        ACTION_TYPES[REDUCER_TYPES.PAYMENT_AND_SUBMISSION]
          .SAGA_UPDATE_PAYMENT_AND_SUBMISSION,
      path,
      newValue,
      isShield,
      validateObj,
      callback
    });
  },
  dataSync() {
    dispatch({
      type: APP_ACTION_TYPES[LOGIN].PRE_CEHCK_ON_DATA_SYNC
    });
  },
  setLastDataSyncTime() {
    setLastDataSyncTime({ dispatch });
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PaymentShield);
