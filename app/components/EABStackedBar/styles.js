import { StyleSheet } from "react-native";
import Theme from "../../theme";

const bar = {
  height: 24,
  marginTop: 8,
  marginBottom: 8
};

export default StyleSheet.create({
  container: {
    width: Theme.boxMaxWidth,
    height: 40
  },
  indicator: {
    backgroundColor: Theme.accent,
    height: 40,
    width: 4,
    position: "absolute"
  },
  topBar: {
    ...bar,
    backgroundColor: Theme.brand,
    position: "absolute"
  },
  bottomBarBrand: {
    ...bar,
    backgroundColor: Theme.brand,
    position: "absolute"
  },
  bottomBarBrandTransparent: {
    ...bar,
    backgroundColor: Theme.brandTransparent,
    position: "absolute"
  }
});
