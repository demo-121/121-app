import axios from "axios";
import querystring from "querystring";
import Config from "react-native-config";
import { RSA } from "react-native-rsa-native";
import { BUILD_NUMBER, VERSION_NUMBER } from "../constants/BUILD_CONFIG";
import { aesGen256Key, aesEncrypt, aesDecrypt } from "../utilities/SecureUtils";

/**
 * getPublicKey
 * @description getPublicKey
 * @param {string} authToken - authToken from AXA api provide
 * @param {string} environment - environment
 * */
export const getPublicKey = (authToken, environment) => {
  let url = "";
  let baseURL = "";
  let getPublicKeyResult = {};
  // const errorMsg = "Authorization Failed (Error 008)";
  if (Config.ENV_NAME === "PROD") {
    url = Config.GET_PUBLIC_KEY;
    baseURL = Config.WEB_SERVICE_BASIC_URL;
  } else if (environment && environment.length > 0) {
    url = Config[`${environment || "SIT3_EAB"}_GET_PUBLIC_KEY`];
    baseURL = Config[`${environment || "SIT3_EAB"}_WEB_SERVICE_BASIC_URL`];
  } else {
    url = Config.SIT3_EAB_GET_PUBLIC_KEY;
    baseURL = Config.SIT3_EAB_WEB_SERVICE_BASIC_URL;
  }
  return new Promise(resolve => {
    const AuthStr = `Bearer ${authToken}`;
    const options = {
      method: "get",
      headers: { Authorization: AuthStr },
      baseURL,
      url
    };
    axios
      .request(options)
      .then(response => {
        if (response.status === 401) {
          getPublicKeyResult = {
            hasError: true,
            errorMsg: "Get Public Key Error 401"
          };
          resolve(getPublicKeyResult);
        } else if (response.data && response.data.status === "success") {
          getPublicKeyResult = {
            hasError: false,
            result: response.data.result
          };
          resolve(getPublicKeyResult);
        } else {
          getPublicKeyResult = {
            hasError: true,
            errorMsg: "[getPublicKeyError]"
          };
          resolve(getPublicKeyResult);
        }
      })
      .catch(error => {
        getPublicKeyResult = {
          hasError: true,
          errorMsg: `[getPublicKeyError]${error}`
        };
        resolve(getPublicKeyResult);
      });
  });
};
export const encryption = (dataInput, authToken, environment) =>
  new Promise(resolve => {
    let encryptionResult = {};
    const getPublicKeyResult = getPublicKey(authToken, environment);
    getPublicKeyResult.then(callback => {
      if (!callback.hasError) {
        if (callback.result && callback.result.publicKey) {
          const publicKeyGen = callback.result.publicKey;
          if (publicKeyGen) {
            // gen aesEncryptedKey
            const aesKey = aesGen256Key();
            let d0 = {};
            if (dataInput) {
              const data = JSON.stringify(dataInput);
              d0 = aesEncrypt(data, aesKey);
            } else {
              d0 = {};
            }
            RSA.encrypt(aesKey, publicKeyGen).then(encodedMessage => {
              const dk = encodedMessage;
              encryptionResult = {
                hasError: false,
                result: {
                  aesKey,
                  d0,
                  dk
                }
              };
              resolve(encryptionResult);
            });
          } else {
            // no key is find
            encryptionResult = {
              hasError: true,
              errorMsg: callback.errorMsg
                ? callback.errorMsg
                : "Authorization Failed (Error 008)"
            };
            resolve(encryptionResult);
          }
        } else {
          // error if callback.result.publicyKey is undefined
          encryptionResult = {
            hasError: true,
            errorMsg: callback.errorMsg
              ? callback.errorMsg
              : "Authorization Failed (Error 008)"
          };
          resolve(encryptionResult);
        }
      } else {
        // error if callback.result.publicyKey is undefined
        encryptionResult = {
          hasError: true,
          errorMsg: callback.errorMsg
            ? callback.errorMsg
            : "Authorization Failed (Error 008)"
        };
        resolve(encryptionResult);
      }
    });
  }).catch(error => {
    const encryptionResult = {
      hasError: true,
      errorMsg: `${error}Authorization Failed (Error 008)`
    };
    return encryptionResult;
  });

export const decryption = (encodedMessage, aesKey) =>
  new Promise(resolve => {
    const data = aesDecrypt(encodedMessage, aesKey);
    resolve(data);
  }).catch(() => {
    // need error handling
  });

/**
 * checkVersionAndDevice
 * @description checkVersionAndDevice
 * @param {string} authToken - authToken from AXA api provide
 * @param {string} installationId - installationId
 * @param {string} environment - environment
 * */
export const checkVersionAndDevice = (
  authToken,
  installationId,
  environment
) => {
  let url = "";
  let baseURL = "";
  let isValidVersionAndDevice = true;
  let checkVersionAndDeviceResult = {};
  let errorMsg = "";
  let hasError = false;
  if (Config.ENV_NAME === "PROD") {
    url = Config.CHECK_VERSION_AND_DEVICE;
    baseURL = Config.WEB_SERVICE_BASIC_URL;
  } else if (environment && environment.length > 0) {
    url = Config[`${environment || "SIT3_EAB"}_CHECK_VERSION_AND_DEVICE`];
    baseURL = Config[`${environment || "SIT3_EAB"}_WEB_SERVICE_BASIC_URL`];
  } else {
    url = Config.SIT3_EAB_CHECK_VERSION_AND_DEVICE;
    baseURL = Config.SIT3_EAB_WEB_SERVICE_BASIC_URL;
  }
  return new Promise(resolve => {
    if (authToken && installationId) {
      const AuthStr = `Bearer ${authToken}`;
      const options = {
        method: "post",
        headers: { Authorization: AuthStr },
        baseURL,
        url,
        data: {
          versionNumber: VERSION_NUMBER,
          buildNumber: BUILD_NUMBER,
          installationId
        }
      };
      axios
        .request(options)
        .then(response => {
          if (response.data.status === "success") {
            if (!response.data.result.isNeedUpgrade) {
              // no need to upgrade
              isValidVersionAndDevice = true;
              if (!response.data.result.isDeactiveDevice) {
                // is active device
                hasError = false;
                isValidVersionAndDevice = true; // will amend
              } else {
                // no longer an active device
                hasError = false;
                isValidVersionAndDevice = false;
                errorMsg =
                  "This device is no longer registered to use this app. If you wish to use the app on this device, please delete and re-install the app.";
              }
            } else {
              // need to upgrade
              hasError = false;
              isValidVersionAndDevice = false;
              errorMsg =
                "The version of the app you are currently using is no longer supported. Please update the app to proceed further.";
            }
          } else {
            hasError = true;
            isValidVersionAndDevice = false;
            errorMsg = "Authorization Failed (Error 007)";
          }
          // Final result
          checkVersionAndDeviceResult = {
            hasError,
            isValidVersionAndDevice,
            errorMsg
          };

          resolve(checkVersionAndDeviceResult);
        })
        .catch(error => {
          if (error.code === "ECONNABORTED") {
            // Timeout error
            errorMsg = "Authorization Failed (Error 009)";
          } else {
            errorMsg = "Authorization Failed (Error 007)";
          }
          isValidVersionAndDevice = false;
          checkVersionAndDeviceResult = {
            hasError: true,
            isValidVersionAndDevice,
            errorMsg
          };
          resolve(checkVersionAndDeviceResult);
        });
    } else {
      isValidVersionAndDevice = false;
      errorMsg = "Authorization Failed (Error 007)";
      checkVersionAndDeviceResult = {
        hasError: true,
        isValidVersionAndDevice,
        errorMsg
      };
      resolve(checkVersionAndDeviceResult);
    }
  });
};

/**
 * fetchAuthToken
 * @description call AXA api to get authToekn by authCode
 * @param {string} authCode - authCode from AXA api provide
 * @param {string} environment - app environment
 * */
export const fetchAuthToken = (authCode, environment) => {
  let authCodeCheck = "";
  if (authCode && authCode.length > 0) {
    authCodeCheck = authCode;
  } else {
    authCodeCheck = null;
  }

  return new Promise((resolve, reject) => {
    if (
      authCode &&
      authCode.length > 0 &&
      authCodeCheck &&
      authCodeCheck.length > 0
    ) {
      const options = {
        method: "POST",
        headers: {
          "cache-control": "no-cache",
          "content-type": "application/x-www-form-urlencoded"
        },
        data: querystring.stringify({
          grant_type: "authorization_code",
          code: authCode,
          redirect_uri: "easemobile://"
        })
      };
      if (Config.ENV_NAME === "PROD") {
        options.url = Config.GET_TOKEN_URL;
        options.baseURL = Config.GET_TOKEN_BASIC_URL;
        options.headers.authorization = Config.MAAM_AUTH;
      } else if (environment && environment.length > 0) {
        options.url = Config[`${environment || "SIT3_EAB"}_GET_TOKEN_URL`];
        options.baseURL =
          Config[`${environment || "SIT3_EAB"}_GET_TOKEN_BASIC_URL`];
        options.headers.authorization =
          Config[`${environment || "SIT3_EAB"}_MAAM_AUTH`];
      } else {
        options.url = Config.SIT3_EAB_GET_TOKEN_URL;
        options.baseURL = Config.SIT3_EAB_GET_TOKEN_BASIC_URL;
        options.headers.authorization = Config.SIT3_EAB_MAAM_AUTH;
      }

      axios
        .request(options)
        .then(response => {
          if (response.status === 200) {
            const result = { ...response.data };
            if (result.access_token && result.access_token.length > 0) {
              resolve(response);
            } else {
              reject(new Error("[fetchAuthToken]-[Error]:  can not get token"));
            }
          } else {
            reject(
              new Error("[fetchAuthToken]-[Error]:Fail to retrieve authToken")
            );
          }
        })
        .catch(error =>
          reject(new Error(`[fetchAuthToken]-[Error]: ${error}`))
        );
    } else {
      reject(new Error("[fetchAuthToken]-[Error]:  No authCode"));
    }
  });
};

/**
 * fetchSessionCookie
 * @description call AXA api to get authToekn by authCode
 * @param {string} idToken - authCode from AXA api provide
 * @param {string} authToken - authToken from AXA api provide
 * @param {string} environment - app environment
 * */
export const fetchSessionCookie = (idToken, authToken, environment) =>
  new Promise(resolve => {
    if (idToken && idToken.length > 0) {
      let clientId = "";
      let clientPw = "";
      let baseURL = "";
      let url = "";
      if (Config.ENV_NAME === "PROD") {
        clientId = Config.CLIENT_ID;
        clientPw = Config.CLIENT_PW;
        url = `${Config.SYNC_GATEWAY_BASIC_URL}:${Config.SYNC_GATEWAY_PORT}/${
          Config.SYNC_GATEWAY_DB
        }/_session`;
        baseURL = Config.WEB_SERVICE_BASIC_URL;
      } else if (environment && environment.length > 0) {
        clientId = Config[`${environment || "SIT3_EAB"}_CLIENT_ID`];
        clientPw = Config[`${environment || "SIT3_EAB"}_CLIENT_PW`];
        url = `${
          Config[`${environment || "SIT3_EAB"}_SYNC_GATEWAY_BASIC_URL`]
        }:${Config[`${environment || "SIT3_EAB"}_SYNC_GATEWAY_PORT`]}/${
          Config[`${environment || "SIT3_EAB"}_SYNC_GATEWAY_DB`]
        }/_session`;
        baseURL = Config[`${environment || "SIT3_EAB"}_WEB_SERVICE_BASIC_URL`];
      } else {
        clientId = Config.CLIENT_ID;
        clientPw = Config.SIT3_EAB_CLIENT_PW;
        url = `${Config.SIT3_EAB_SYNC_GATEWAY_BASIC_URL}:${
          Config.SIT3_EAB_SYNC_GATEWAY_PORT
        }/${Config.SIT3_EAB_SYNC_GATEWAY_DB}/_session`;
        baseURL = Config.SIT3_EAB_WEB_SERVICE_BASIC_URL;
      }
      const AuthStr = `Bearer ${idToken}`;
      const options = {
        method: "post",
        headers: { Authorization: AuthStr },
        baseURL,
        url,
        data: {
          SessionBody: {
            name: clientId,
            password: clientPw
          }
        }
      };
      // --> CR 197, disable encryption temporarily and then roll back
      // let aes256Key = "";
      // const encryptResult = encryption(options.data, authToken, environment);
      // encryptResult
      //   .then(callback => {
      //     if (callback.hasError) {
      //       const responseResult = {
      //         hasError: true,
      //         error: callback.errorMsg
      //           ? callback.errorMsg
      //           : "Cannot get Public Key"
      //       };
      //       resolve(responseResult);
      //     } else {
      //       aes256Key = callback.result.aesKey;
      //       options.data.d0 = callback.result.d0;
      //       options.data.dk = callback.result.dk;
      //     }
      //   })
      //   .then(() => {
      //     axios
      //       .request(options)
      //       .then(response => {
      //         const decryptionResult = decryption(response.data, aes256Key);
      //         decryptionResult.then(decryptedResponseBody => {
      //           response.data = JSON.parse(decryptedResponseBody);
      //           if (response.status === 200) {
      //             const responseResult = {
      //               hasError: false,
      //               result: response
      //             };
      //             resolve(responseResult);
      //           } else {
      //             const responseResult = {
      //               hasError: true,
      //               error:
      //                 "[fetchSessionCookie]-[Error]:Fail to fetch Session Cookie"
      //             };
      //             resolve(responseResult);
      //           }
      //         });
      //       })
      //       .catch(error => {
      //         const responseResult = {
      //           hasError: true,
      //           error
      //         };
      //         resolve(responseResult);
      //       });
      //   });

      axios
        .request(options)
        .then(response => {
          if (response.status === 200) {
            const responseResult = {
              hasError: false,
              result: response
            };
            resolve(responseResult);
          } else {
            const responseResult = {
              hasError: true,
              error: "[fetchSessionCookie]-[Error]:Fail to fetch Session Cookie"
            };
            resolve(responseResult);
          }
        })
        .catch(error => {
          const responseResult = {
            hasError: true,
            error
          };
          resolve(responseResult);
        });
    } else {
      const responseResult = {
        hasError: true,
        error: "[fetchSessionCookie]-[Error]:No idToken / environment"
      };
      resolve(responseResult);
    }
  });

/**
 * verifyUserInfo
 * @description call 121Online API to verify User and Mobile Device
 * @param {string} authToken - authToken from AXA api provide
 * @param {string} deviceInfo.uuid - device uuid
 * @param {string} deviceInfo.deviceModel - device model
 * @param {string} versionNumber - ease version number
 * @param {string} buildNumber - ease build number
 * @param {string} environment - app environment
 * @param {string} deviceInfo.deviceCountry - the device country based on the locale information
 * @param {string} deviceInfo.brand - device brand
 * @param {string} deviceInfo.carrier - device carrier (support iOS and Android, not support Windows)
 * @param {string} deviceInfo.deviceLocale - device locale
 * @param {string} deviceInfo.deviceName - device name
 * @param {string} deviceInfo.firstInstallTime - the time at which the app was first installed, in milliseconds (support Android and Windows, not support iOS)
 * @param {string} deviceInfo.fontScale - the device font scale. The font scale is the ratio of the current system font to the "normal" font size, so if normal text is 10pt and the system font is currently 15pt, the font scale would be 1.5 This can be used to determine if accessability settings has been changed for the device; you may want to re-layout certain views if the font scale is significantly larger ( > 2.0 ) (support iOS and Android, not support Windows)
 * @param {string} deviceInfo.freeDiskStorage - available storage size, in bytes (support iOS and Android, not support Windows)
 * @param {string} deviceInfo.ipAddress - the device current IP address
 * @param {string} deviceInfo.installReferrer - the referrer string upon application installation (only support Android)
 * @param {string} deviceInfo.instanceID - the application instance ID (only support Android)
 * @param {string} deviceInfo.macAddress - the network adapter MAC address (support iOS and Android, not support Windows)
 * @param {string} deviceInfo.manufacturer - device manufacturer
 * @param {number} deviceInfo.maxMemory - the maximum amount of memory that the VM will attempt to use, in bytes (support Android and Windows, not support iOS)
 * @param {string} deviceInfo.phoneNumber - the device phone number (only support Android)
 * @param {string} deviceInfo.serialNumber - the device serial number(only support Android)
 * @param {string} deviceInfo.systemName - device OS name
 * @param {string} deviceInfo.systemVersion - the device OS version
 * @param {string} deviceInfo.timezone - the device default timezone
 * @param {number} deviceInfo.storageSize - full disk storage size, in bytes (support iOS and Android, not support Windows)
 * @param {number} deviceInfo.totalMemory - the device total memory, in bytes (support iOS and Android, not support Windows)
 * @param {string} deviceInfo.userAgent - device User Agent (support iOS and Android, not support Windows)
 * @param {boolean} deviceInfo.is24Hour - Tells if the user preference is set to 24-hour format
 * @param {boolean} deviceInfo.isEmulator - Tells if the application is running in an emulator
 * @param {boolean} deviceInfo.isPinOrFingerprintSet - Tells if a PIN number or a fingerprint was set for the device
 * @param {boolean} deviceInfo.isTablet - Tells if the device is a tablet
 * @param {boolean} deviceInfo.hasNotch - Tells if the device has a notch
 * @param {boolean} deviceInfo.isLandscape - Tells if the device is currently in landscape mode
 * @param {boolean} deviceInfo.deviceType - Returns the device's type as a string, which will be one of: Handset, Tablet, Tv and Unknown (support iOS and Android, not support Windows)
 * */
export const verifyUserInfo = (
  authToken,
  idToken,
  versionNumber,
  buildNumber,
  environment,
  deviceInfo
) => {
  let url = "";
  let baseURL = "";
  // TODO: to be removed
  if (Config.ENV_NAME === "PROD") {
    url = Config.ACTIVATE_USER_INFO_URL;
    baseURL = Config.WEB_SERVICE_BASIC_URL;
  } else if (environment && environment.length > 0) {
    url = Config[`${environment || "SIT3_EAB"}_ACTIVATE_USER_INFO_URL`];
    baseURL = Config[`${environment || "SIT3_EAB"}_WEB_SERVICE_BASIC_URL`];
  } else {
    url = Config.DEV_ACTIVATE_USER_INFO_URL;
    baseURL = Config.DEV_WEB_SERVICE_BASIC_URL;
  }

  return new Promise((resolve, reject) => {
    if (
      authToken &&
      authToken.length > 0 &&
      deviceInfo.uuid &&
      deviceInfo.uuid.length > 0 &&
      deviceInfo.deviceModel &&
      deviceInfo.deviceModel.length > 0
    ) {
      const AuthStr = `Bearer ${authToken}`;
      const options = {
        method: "post",
        headers: { Authorization: AuthStr },
        baseURL,
        url,
        data: {
          device_model: deviceInfo.deviceModel,
          device_id: deviceInfo.uuid,
          versionNumber,
          buildNumber,
          id_token: idToken,
          deviceCountry: deviceInfo.deviceCountry,
          brand: deviceInfo.brand,
          carrier: deviceInfo.carrier,
          deviceLocale: deviceInfo.deviceLocale,
          deviceName: deviceInfo.deviceName,
          firstInstallTime: deviceInfo.firstInstallTime,
          fontScale: deviceInfo.fontScale,
          freeDiskStorage: deviceInfo.freeDiskStorage,
          ipAddress: deviceInfo.ipAddress,
          installReferrer: deviceInfo.installReferrer,
          instanceID: deviceInfo.instanceID,
          macAddress: deviceInfo.macAddress,
          manufacturer: deviceInfo.manufacturer,
          maxMemory: deviceInfo.maxMemory,
          phoneNumber: deviceInfo.phoneNumber,
          serialNumber: deviceInfo.serialNumber,
          systemVersion: deviceInfo.systemVersion,
          timezone: deviceInfo.timezone,
          storageSize: deviceInfo.storageSize,
          totalMemory: deviceInfo.totalMemory,
          userAgent: deviceInfo.userAgent,
          is24Hour: deviceInfo.is24Hour,
          isEmulator: deviceInfo.isEmulator,
          isPinOrFingerprintSet: deviceInfo.isPinOrFingerprintSet,
          isTablet: deviceInfo.isTablet,
          hasNotch: deviceInfo.hasNotch,
          isLandscape: deviceInfo.isLandscape,
          deviceType: deviceInfo.deviceType,
          systemName: deviceInfo.systemName
        }
      };
      // --> CR 197, disable encryption temporarily and then roll back
      // let aes256Key = "";
      // const encryptResult = encryption(options.data, authToken, environment);
      // encryptResult
      //   .then(callback => {
      //     if (callback.hasError) {
      //       const responseResult = {
      //         hasError: true,
      //         multipleMsgKey: callback.errorMsg
      //           ? callback.errorMsg
      //           : "Cannot get Public Key"
      //       };
      //       resolve(responseResult);
      //     } else {
      //       aes256Key = callback.result.aesKey;
      //       options.data.d0 = callback.result.d0;
      //       options.data.dk = callback.result.dk;
      //     }
      //   })
      //   .then(() => {
      //     axios
      //       .request(options)
      //       .then(response => {
      //         const decryptionResult = decryption(response.data, aes256Key);
      //         decryptionResult.then(decryptedResponseBody => {
      //           response.data = JSON.parse(decryptedResponseBody);
      //           if (response.status === 200) {
      //             const responseData = { ...response.data };
      //             if (
      //               responseData.result.installation_id &&
      //               responseData.result.installation_id.length > 0 &&
      //               responseData.result.user_id &&
      //               responseData.result.user_id.length > 0
      //             ) {
      //               const responseResult = {
      //                 hasError: false,
      //                 result: response.data
      //               };
      //               resolve(responseResult);
      //             } else if (
      //               responseData.result.error &&
      //               responseData.result.error.length > 0
      //             ) {
      //               const responseResult = {
      //                 hasError: true,
      //                 multipleMsgKey: responseData.result.error
      //               };
      //               resolve(responseResult);
      //             } else {
      //               reject(
      //                 new Error("[verifyUserInfo]-[Error]:Fail to Verify User")
      //               );
      //             }
      //           } else {
      //             reject(
      //               new Error("[verifyUserInfo]-[Error]:Fail to Verify User")
      //             );
      //           }
      //         });
      //       })
      //       .catch(error => {
      //         const responseResult = {
      //           hasError: true,
      //           error
      //         };
      //         reject(responseResult.error);
      //       });
      //   });

      axios
        .request(options)
        .then(response => {
          if (response.status === 200) {
            const responseData = { ...response.data };
            if (
              responseData.result.installation_id &&
              responseData.result.installation_id.length > 0 &&
              responseData.result.user_id &&
              responseData.result.user_id.length > 0
            ) {
              const responseResult = {
                hasError: false,
                result: response.data
              };
              resolve(responseResult);
            } else if (
              responseData.result.error &&
              responseData.result.error.length > 0
            ) {
              const responseResult = {
                hasError: true,
                multipleMsgKey: responseData.result.error
              };
              resolve(responseResult);
            } else {
              reject(new Error("[verifyUserInfo]-[Error]:Fail to Verify User"));
            }
          } else {
            reject(new Error("[verifyUserInfo]-[Error]:Fail to Verify User"));
          }
        })
        .catch(error => {
          const responseResult = {
            hasError: true,
            error
          };
          reject(responseResult.error);
        });
    } else {
      reject(
        new Error(
          "[verifyUserInfo]-[Error]:No authToken / masterKey / deviceModel"
        )
      );
    }
  });
};

/**
 * updateMasterKeyToServer
 * @description call 121Online API to keep MasterKey as backup in OnlineDB
 * @param {string} authToken - authToken from AXA api provide
 * @param {string} masterKey - non-encrypted masterKey
 * @param {string} installationID - installationID from AXA
 * @param {string} environment - app environment
 * */
export const updateMasterKeyToServer = (
  authToken,
  masterKey,
  installationID,
  environment
) => {
  let url = "";
  let baseURL = "";
  if (Config.ENV_NAME === "PROD") {
    url = Config.STORE_KEYS_URL;
    baseURL = Config.WEB_SERVICE_BASIC_URL;
  } else {
    url = Config[`${environment || "SIT3_EAB"}_STORE_KEYS_URL`];
    baseURL = Config[`${environment || "SIT3_EAB"}_WEB_SERVICE_BASIC_URL`];
  }

  return new Promise((resolve, reject) => {
    if (
      authToken &&
      authToken.length > 0 &&
      masterKey &&
      masterKey.length > 0 &&
      installationID &&
      installationID.length > 0
    ) {
      const AuthStr = `Bearer ${authToken}`;
      const options = {
        method: "put",
        headers: { Authorization: AuthStr },
        baseURL,
        url,
        data: {
          master_key: masterKey,
          installation_id: installationID
        }
      };
      // --> CR 197, disable encryption temporarily and then roll back
      // let aes256Key = "";
      // const encryptResult = encryption(options.data, authToken, environment);
      // encryptResult
      //   .then(callback => {
      //     if (callback.hasError) {
      //       reject(
      //         new Error(
      //           callback.errorMsg ? callback.errorMsg : "Cannot get Public Key"
      //         )
      //       );
      //     } else {
      //       aes256Key = callback.result.aesKey;
      //       options.data.d0 = callback.result.d0;
      //       options.data.dk = callback.result.dk;
      //     }
      //   })
      //   .then(() => {
      //     axios
      //       .request(options)
      //       .then(response => {
      //         const decryptionResult = decryption(response.data, aes256Key);
      //         decryptionResult.then(decryptedResponseBody => {
      //           response.data = JSON.parse(decryptedResponseBody);
      //           if (response.status === 200) {
      //             const responseResult = { ...response.data };
      //             if (responseResult.status === "success") {
      //               resolve(response);
      //             } else {
      //               reject(
      //                 new Error(
      //                   "[updateMasterKeyToServer]-[Error]: Fail to update Keys to 121"
      //                 )
      //               );
      //             }
      //           } else {
      //             reject(
      //               new Error(
      //                 "[updateMasterKeyToServer]-[Error]: Fail to update Keys to 121"
      //               )
      //             );
      //           }
      //         });
      //       })
      //       .catch(error => {
      //         reject(
      //           new Error(
      //             `[updateMasterKeyToServer]-[Response_Error]: ${error}`
      //           )
      //         );
      //       });
      //   });

      axios
        .request(options)
        .then(response => {
          if (response.status === 200) {
            const responseResult = { ...response.data };
            if (responseResult.status === "success") {
              resolve(response);
            } else {
              reject(
                new Error(
                  "[updateMasterKeyToServer]-[Error]: Fail to update Keys to 121"
                )
              );
            }
          } else {
            reject(
              new Error(
                "[updateMasterKeyToServer]-[Error]: Fail to update Keys to 121"
              )
            );
          }
        })
        .catch(error => {
          reject(
            new Error(`[updateMasterKeyToServer]-[Response_Error]: ${error}`)
          );
        });
    } else {
      reject(
        new Error(
          `[updateMasterKeyToServer]-No authToken / masterKey / installationID`
        )
      );
    }
  });
};

/**
 * verifyUser121Online
 * @description Authenticates the user by access token and user ID in online mode
 * @param {string} authToken - authToken from AXA api provide
 * @param {string} userID - userID
 * @param {string} installationID - installationID
 * @param {string} environment - app environment
 * @param {string} deviceInfo.uuid - device uuid
 * @param {string} deviceInfo.deviceModel - device model
 * @param {string} deviceInfo.deviceCountry - the device country based on the locale information
 * @param {string} deviceInfo.brand - device brand
 * @param {string} deviceInfo.carrier - device carrier (support iOS and Android, not support Windows)
 * @param {string} deviceInfo.deviceLocale - device locale
 * @param {string} deviceInfo.deviceName - device name
 * @param {string} deviceInfo.firstInstallTime - the time at which the app was first installed, in milliseconds (support Android and Windows, not support iOS)
 * @param {string} deviceInfo.fontScale - the device font scale. The font scale is the ratio of the current system font to the "normal" font size, so if normal text is 10pt and the system font is currently 15pt, the font scale would be 1.5 This can be used to determine if accessability settings has been changed for the device; you may want to re-layout certain views if the font scale is significantly larger ( > 2.0 ) (support iOS and Android, not support Windows)
 * @param {string} deviceInfo.freeDiskStorage - available storage size, in bytes (support iOS and Android, not support Windows)
 * @param {string} deviceInfo.ipAddress - the device current IP address
 * @param {string} deviceInfo.installReferrer - the referrer string upon application installation (only support Android)
 * @param {string} deviceInfo.instanceID - the application instance ID (only support Android)
 * @param {string} deviceInfo.macAddress - the network adapter MAC address (support iOS and Android, not support Windows)
 * @param {string} deviceInfo.manufacturer - device manufacturer
 * @param {number} deviceInfo.maxMemory - the maximum amount of memory that the VM will attempt to use, in bytes (support Android and Windows, not support iOS)
 * @param {string} deviceInfo.phoneNumber - the device phone number (only support Android)
 * @param {string} deviceInfo.serialNumber - the device serial number(only support Android)
 * @param {string} deviceInfo.systemName - device OS name
 * @param {string} deviceInfo.systemVersion - the device OS version
 * @param {string} deviceInfo.timezone - the device default timezone
 * @param {number} deviceInfo.storageSize - full disk storage size, in bytes (support iOS and Android, not support Windows)
 * @param {number} deviceInfo.totalMemory - the device total memory, in bytes (support iOS and Android, not support Windows)
 * @param {string} deviceInfo.userAgent - device User Agent (support iOS and Android, not support Windows)
 * @param {boolean} deviceInfo.is24Hour - Tells if the user preference is set to 24-hour format
 * @param {boolean} deviceInfo.isEmulator - Tells if the application is running in an emulator
 * @param {boolean} deviceInfo.isPinOrFingerprintSet - Tells if a PIN number or a fingerprint was set for the device
 * @param {boolean} deviceInfo.isTablet - Tells if the device is a tablet
 * @param {boolean} deviceInfo.hasNotch - Tells if the device has a notch
 * @param {boolean} deviceInfo.isLandscape - Tells if the device is currently in landscape mode
 * @param {boolean} deviceInfo.deviceType - Returns the device's type as a string, which will be one of: Handset, Tablet, Tv and Unknown (support iOS and Android, not support Windows)
 * */
export const verifyUser121Online = (
  authToken,
  idToken,
  installationID,
  environment,
  deviceInfo
) => {
  let url = "";
  let baseURL = "";
  if (Config.ENV_NAME === "PROD") {
    url = Config.VERIFY_USER_URL;
    baseURL = Config.WEB_SERVICE_BASIC_URL;
  } else if (environment && environment.length > 0) {
    url = Config[`${environment || "SIT3_EAB"}_VERIFY_USER_URL`];
    baseURL = Config[`${environment || "SIT3_EAB"}_WEB_SERVICE_BASIC_URL`];
  } else {
    url = Config.DEV_VERIFY_USER_URL;
    baseURL = Config.DEV_WEB_SERVICE_BASIC_URL;
  }

  return new Promise((resolve, reject) => {
    if (
      authToken &&
      authToken.length > 0 &&
      installationID &&
      installationID.length > 0
    ) {
      const AuthStr = `Bearer ${authToken}`;
      const options = {
        method: "post",
        headers: { Authorization: AuthStr },
        baseURL,
        url,
        data: {
          installation_id: installationID,
          id_token: idToken,
          uuid: deviceInfo.uuid,
          deviceModel: deviceInfo.deviceModel,
          versionNumber: VERSION_NUMBER,
          buildNumber: BUILD_NUMBER,
          deviceCountry: deviceInfo.deviceCountry,
          brand: deviceInfo.brand,
          carrier: deviceInfo.carrier,
          deviceLocale: deviceInfo.deviceLocale,
          deviceName: deviceInfo.deviceName,
          firstInstallTime: deviceInfo.firstInstallTime,
          fontScale: deviceInfo.fontScale,
          freeDiskStorage: deviceInfo.freeDiskStorage,
          ipAddress: deviceInfo.ipAddress,
          installReferrer: deviceInfo.installReferrer,
          instanceID: deviceInfo.instanceID,
          macAddress: deviceInfo.macAddress,
          manufacturer: deviceInfo.manufacturer,
          maxMemory: deviceInfo.maxMemory,
          phoneNumber: deviceInfo.phoneNumber,
          serialNumber: deviceInfo.serialNumber,
          systemName: deviceInfo.systemName,
          systemVersion: deviceInfo.systemVersion,
          timezone: deviceInfo.timezone,
          storageSize: deviceInfo.storageSize,
          totalMemory: deviceInfo.totalMemory,
          userAgent: deviceInfo.userAgent,
          is24Hour: deviceInfo.is24Hour,
          isEmulator: deviceInfo.isEmulator,
          isPinOrFingerprintSet: deviceInfo.isPinOrFingerprintSet,
          isTablet: deviceInfo.isTablet,
          hasNotch: deviceInfo.hasNotch,
          isLandscape: deviceInfo.isLandscape,
          deviceType: deviceInfo.deviceType
        }
      };
      // --> CR 197, disable encryption temporarily and then roll back
      // let aes256Key = "";
      // const encryptResult = encryption(options.data, authToken, environment);
      // encryptResult
      //   .then(callback => {
      //     if (callback.hasError) {
      //       reject(
      //         new Error(
      //           callback.errorMsg ? callback.errorMsg : "Cannot get public key"
      //         )
      //       );
      //     } else {
      //       aes256Key = callback.result.aesKey;
      //       options.data.d0 = callback.result.d0;
      //       options.data.dk = callback.result.dk;
      //     }
      //   })
      //   .then(() => {
      //     axios
      //       .request(options)
      //       .then(response => {
      //         const decryptionResult = decryption(response.data, aes256Key);
      //         decryptionResult.then(decryptedResponseBody => {
      //           response.data = JSON.parse(decryptedResponseBody);
      //           if (response.status === 200) {
      //             const responseResult = { ...response.data };
      //             if (responseResult.status === "success") {
      //               resolve(response);
      //             } else if (responseResult.message) {
      //               resolve(response);
      //             } else {
      //               reject(
      //                 new Error(
      //                   "Fail to verify user by access token and user ID in online mode"
      //                 )
      //               );
      //             }
      //           } else {
      //             reject(
      //               new Error(
      //                 "Fail to verify user by access token and user ID in online mode"
      //               )
      //             );
      //           }
      //         });
      //       })
      //       .catch(error => {
      //         reject(
      //           new Error(`[verifyUser121Online]-[Response_Error]: ${error}`)
      //         );
      //       });
      //   });
      axios
        .request(options)
        .then(response => {
          if (response.status === 200) {
            const responseResult = { ...response.data };
            if (responseResult.status === "success") {
              resolve(response);
            } else if (responseResult.message) {
              resolve(response);
            } else {
              reject(
                new Error(
                  "Fail to verify user by access token and user ID in online mode"
                )
              );
            }
          } else {
            reject(
              new Error(
                "Fail to verify user by access token and user ID in online mode"
              )
            );
          }
        })
        .catch(error => {
          reject(new Error(`[verifyUser121Online]-[Response_Error]: ${error}`));
        });
    } else {
      reject(
        new Error(
          `[verifyUser121Online]-[No authToken / userID / installationID`
        )
      );
    }
  });
};
