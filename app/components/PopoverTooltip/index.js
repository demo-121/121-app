import { Platform } from "react-native";
import PopoverTooltipIOS from "./PopoverTooltipIOS";

export default Platform.select({
  ios: PopoverTooltipIOS,
  android: {}
});
