import { StyleSheet } from "react-native";
import Theme from "../../theme";

export default StyleSheet.create({
  buttonContainer: {
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center"
  },
  rectangleButton1: {
    paddingVertical: Theme.alignmentXS,
    paddingHorizontal: Theme.alignmentL,
    borderRadius: Theme.radiusXS,
    minWidth: 64
  },
  rectangleButton2: {
    paddingVertical: Theme.alignmentXS,
    paddingHorizontal: Theme.alignmentL,
    borderWidth: 2,
    borderRadius: Theme.radiusXS,
    minWidth: 64
  }
});
