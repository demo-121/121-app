### PopoverDatePicker sample code snippets

```javascript
class Sample extends Component {
  constructor(props, context) {
    super(props, context)
    this.state = {

    }
  }

  render() {
    /*
     * preferredContentSize: [width, height]
     */
    return (
      <View>
        <PopoverTooltip
          popoverOptions={
            preferredContentSize:[100,100]
          }
          text=""
        />
      </View>
    )
  }
}
```