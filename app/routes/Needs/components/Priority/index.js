import React, { Component } from "react";
import PropTypes from "prop-types";
import * as _ from "lodash";
import {
  REDUCER_TYPE_CHECK as WEB_API_REDUCER_TYPE_CHECK,
  REDUCER_TYPES,
  PROGRESS_TABS,
  NEEDS,
  utilities
} from "eab-web-api";
import { View, Text, Animated, ScrollView, Image } from "react-native";
import SortableList from "../../../../components/SortableList";
import TranslatedText from "../../../../containers/TranslatedText";
import { TEXT_STORE } from "../../../../constants/REDUCER_TYPES";
import { ContextConsumer } from "../../../../context";
import APP_REDUCER_TYPES_CHECK from "../../../../constants/REDUCER_TYPE_CHECK";
import EABi18n from "../../../../utilities/EABi18n";
import styles from "./styles";
import diProtectionImg from "../../../../assets/images/diProtectionImg.png";
import ciProtectionImg from "../../../../assets/images/ciProtectionImg.png";
import ePlanningImg from "../../../../assets/images/ePlanningImg.png";
import fiProtectionImg from "../../../../assets/images/fiProtectionImg.png";
import hcProtectionImg from "../../../../assets/images/hcProtectionImg.png";
import otherAspectImg from "../../../../assets/images/otherAspectImg.png";
import paProtectionImg from "../../../../assets/images/paProtectionImg.png";
import pcHeadstartImg from "../../../../assets/images/pcHeadstartImg.png";
import psGoalsImg from "../../../../assets/images/psGoalsImg.png";
import rPlanningImg from "../../../../assets/images/rPlanningImg.png";

const { CLIENT, FNA } = REDUCER_TYPES;
const {
  FIPROTECTION,
  CIPROTECTION,
  DIPROTECTION,
  PAPROTECTION,
  PCHEADSTART,
  HCPROTECTION,
  PSGOALS,
  EPLANNING,
  RPLANNING,
  OTHER
} = NEEDS;
const { needsUtil, numberFormatter } = utilities;
const { numberToCurrency } = numberFormatter;

/**
 * <Priority />
 * @param {object} na - needs analysis data
 * @param {func} setScrollEnabled - change parent scroll view enabled
 * */

export default class Priority extends Component {
  constructor(props) {
    super(props);

    const { na, profile, dependantProfiles, pda } = props;
    const shortfallOptions = needsUtil.getPriorityOptions({
      subType: "shortfall",
      fnaValue: na,
      profile,
      dependantProfiles,
      pda
    });
    const surplusOptions = needsUtil.getPriorityOptions({
      subType: "surplus",
      fnaValue: na,
      profile,
      dependantProfiles,
      pda
    });

    this.state = {
      shortfallData: shortfallOptions[0],
      surplusData: surplusOptions[0],
      scrollEnabled: true
    };

    this.prepareTitle = this.prepareTitle.bind(this);
    this.renderRow = this.renderRow.bind(this);
    this.setScrollEnabled = this.setScrollEnabled.bind(this);
  }

  componentDidMount() {
    this.props.initCompletedStep(PROGRESS_TABS[FNA].PRIORITY);
    this.props.initPriorityNAValue({
      spAspects: this.state.surplusData,
      sfAspects: this.state.shortfallData
    });
  }

  /**
   * setScrollEnabled - update scroll view enabled
   * callback function passed into children
   * */
  setScrollEnabled(enabled) {
    this.setState({
      scrollEnabled: enabled
    });
  }

  prepareTitle({ option, language }) {
    const { textStore } = this.props;

    if (_.isEmpty(option.title)) {
      switch (option.aid) {
        case FIPROTECTION:
          option.title = EABi18n({
            path: "na.needs.fiProtection",
            textStore,
            language
          });
          return option.title;
        case CIPROTECTION:
          option.title = EABi18n({
            path: "na.needs.ciProtection",
            textStore,
            language
          });
          return option.title;
        case DIPROTECTION:
          option.title = EABi18n({
            path: "na.priority.diProtection",
            textStore,
            language
          });
          return option.title;
        case PAPROTECTION:
          option.title = EABi18n({
            path: "na.needs.paProtection",
            textStore,
            language
          });
          return option.title;
        case PCHEADSTART:
          option.title = EABi18n({
            path: "na.needs.pcHeadstart",
            textStore,
            language
          });
          return option.title;
        case HCPROTECTION:
          option.title = EABi18n({
            path: "na.needs.hcProtection",
            textStore,
            language
          });
          return option.title;
        case PSGOALS:
          option.title = EABi18n({
            path: "na.needs.psGoals",
            textStore,
            language
          });
          return option.title;
        case EPLANNING:
          option.title = EABi18n({
            path: "na.needs.ePlanning",
            textStore,
            language
          });
          return option.title;
        case RPLANNING:
          option.title = EABi18n({
            path: "na.needs.rPlanning",
            textStore,
            language
          });
          return option.title;
        case OTHER:
          option.title = EABi18n({
            path: "na.needs.other",
            textStore,
            language
          });
          return option.title;
        default:
          return "";
      }
    }
    return option.title;
  }

  renderRow({ data, index, language }) {
    const { textStore } = this.props;
    const getImage = aid => {
      switch (aid) {
        case FIPROTECTION:
          return fiProtectionImg;
        case CIPROTECTION:
          return ciProtectionImg;
        case DIPROTECTION:
          return diProtectionImg;
        case PAPROTECTION:
          return paProtectionImg;
        case PCHEADSTART:
          return pcHeadstartImg;
        case HCPROTECTION:
          return hcProtectionImg;
        case PSGOALS:
          return psGoalsImg;
        case EPLANNING:
          return ePlanningImg;
        case RPLANNING:
          return rPlanningImg;
        case OTHER:
          return otherAspectImg;
        default:
          return null;
      }
    };

    if (data.aid === "psGoals" || data.aid === "other") {
      data.index = Number(data.id.charAt(data.id.length - 1)) + 1;
    } else {
      data.index = 1;
    }

    return (
      <Animated.View style={styles.row}>
        <View style={styles.rowIndex}>
          <Text style={styles.rowIndexTitle}>{`${index + 1}.`}</Text>
        </View>
        <Image source={getImage(data.aid)} style={styles.priorityImage} />
        <View style={styles.rowCard}>
          <View style={styles.rowDataRow}>
            <TranslatedText
              style={styles.rowDataRowTitle}
              path="na.priority.list.forPerson"
              replace={[
                {
                  value: data.name,
                  isPath: false
                }
              ]}
            />
            {_.isNumber(data.timeHorizon) ? (
              <View style={styles.rowData}>
                <Text style={styles.rowDataTitle}>
                  {EABi18n({
                    path: "na.priority.list.timeHorizon",
                    language,
                    textStore
                  })}:
                </Text>
                <TranslatedText
                  style={styles.rowValueNormal}
                  path="na.priority.list.timeHorizonYears"
                  replace={[
                    {
                      value: data.timeHorizon,
                      isPath: false
                    }
                  ]}
                />
              </View>
            ) : null}
          </View>
          <View style={styles.rowDataRow}>
            <Text style={styles.rowDataRowSubTitle}>
              {this.prepareTitle({ option: data, language })}
            </Text>
            {_.has(data, "totShortfall") ? (
              <View style={styles.rowData}>
                <Text style={styles.rowDataTitle}>
                  {EABi18n({
                    path:
                      data.totShortfall < 0
                        ? "na.priority.list.shortfall"
                        : "na.priority.list.surplus",
                    language,
                    textStore
                  })}:
                </Text>
                <Text
                  style={
                    data.totShortfall < 0
                      ? styles.rowValueNegative
                      : styles.rowValuePositive
                  }
                >
                  {numberToCurrency({
                    value: Math.abs(data.totShortfall),
                    ccy: "SGD",
                    maxValue: 999999999999999
                  })}
                </Text>
              </View>
            ) : null}
          </View>
          <View style={styles.rowDataRow}>
            <Text style={styles.rowDataRowSubTitle} />
            {_.has(data, "totShortfall") &&
            data.totShortfall < 0 &&
            data.aid === "rPlanning" ? (
              <View style={styles.rowData}>
                <Text style={styles.rowDataTitle}>
                  {EABi18n({
                    path: "na.needs.common.annualRetirementIncome",
                    language,
                    textStore
                  })}:
                </Text>
                <Text
                  style={
                    data.totShortfall < 0
                      ? styles.rowValueNegative
                      : styles.rowValuePositive
                  }
                >
                  {numberToCurrency({
                    value: Math.abs(data.annRetireIncome),
                    ccy: "SGD",
                    maxValue: 999999999999999
                  })}
                </Text>
              </View>
            ) : null}
          </View>
        </View>
      </Animated.View>
    );
  }

  render() {
    const { updateNAValue } = this.props;
    const { setScrollEnabled } = this;
    const { shortfallData, surplusData, scrollEnabled } = this.state;
    return (
      <ContextConsumer>
        {({ language }) => (
          <ScrollView
            contentContainerStyle={styles.scrollViewContent}
            scrollEnabled={scrollEnabled}
          >
            <View style={styles.container}>
              {surplusData.length > 0 ? (
                <View style={styles.headerContainer}>
                  <TranslatedText
                    style={styles.warningStatement}
                    path="na.priority.statement.rankStatement"
                  />
                </View>
              ) : null}
              <View style={styles.titleContainer}>
                <TranslatedText
                  style={styles.statement}
                  language={language}
                  path="na.priority.statement.dragAndDrop"
                />
                <Text style={styles.required}>*</Text>
              </View>
              <View style={styles.listContainer}>
                {shortfallData.length > 0 ? (
                  <SortableList
                    style={styles.list}
                    contentContainerStyle={styles.listContentContainer}
                    data={shortfallData}
                    fullRow
                    autoscrollEnable={false}
                    onActivateRow={() => {
                      setScrollEnabled(false);
                    }}
                    onReleaseRow={rowOrder => {
                      const newSfAspects = rowOrder.map(
                        order => this.props.na.sfAspects[order]
                      );
                      updateNAValue({
                        target: PROGRESS_TABS[FNA].PRIORITY,
                        value: { sfAspects: newSfAspects }
                      });
                      setScrollEnabled(true);
                    }}
                    renderHeader={() => (
                      <View style={styles.rowHeaderContainer}>
                        <TranslatedText
                          style={styles.rowHeaderTitle}
                          path="na.priority.list.shortfall"
                          language={language}
                        />
                      </View>
                    )}
                    renderRow={({ data, index }) =>
                      this.renderRow({ data, index, language })
                    }
                    rowColor={styles.rowColor}
                  />
                ) : null}
                {surplusData.length > 0 ? (
                  <SortableList
                    style={styles.list}
                    contentContainerStyle={styles.listContentContainer}
                    data={surplusData}
                    fullRow
                    autoscrollEnable={false}
                    onActivateRow={() => {
                      setScrollEnabled(false);
                    }}
                    onReleaseRow={rowOrder => {
                      const newSpAspects = [];
                      _.forEach(rowOrder, order => {
                        newSpAspects.push(this.props.na.spAspects[order]);
                      });

                      updateNAValue({
                        target: PROGRESS_TABS[FNA].PRIORITY,
                        value: {
                          spAspects: newSpAspects,
                          sfAspects: this.props.na.sfAspects
                        }
                      });
                      setScrollEnabled(true);
                    }}
                    renderHeader={() => (
                      <View style={styles.rowHeaderContainer}>
                        <TranslatedText
                          style={styles.rowHeaderTitle}
                          path="na.priority.list.surplus"
                          language={language}
                        />
                      </View>
                    )}
                    renderRow={({ data, index }) =>
                      this.renderRow({
                        data,
                        index: shortfallData.length + index,
                        language
                      })
                    }
                    rowColor={styles.rowColor}
                  />
                ) : null}
              </View>
              <View style={styles.footerContainer}>
                <TranslatedText
                  style={styles.warningStatement}
                  path="na.priority.statement.canRank"
                />
              </View>
            </View>
          </ScrollView>
        )}
      </ContextConsumer>
    );
  }
}

Priority.propTypes = {
  initCompletedStep: PropTypes.func.isRequired,
  textStore: APP_REDUCER_TYPES_CHECK[TEXT_STORE].isRequired,
  na: WEB_API_REDUCER_TYPE_CHECK[FNA].na.isRequired,
  profile: WEB_API_REDUCER_TYPE_CHECK[CLIENT].profile.isRequired,
  dependantProfiles:
    WEB_API_REDUCER_TYPE_CHECK[CLIENT].dependantProfiles.isRequired,
  pda: WEB_API_REDUCER_TYPE_CHECK[FNA].pda.isRequired,
  initPriorityNAValue: PropTypes.func.isRequired,
  updateNAValue: PropTypes.func.isRequired
};
