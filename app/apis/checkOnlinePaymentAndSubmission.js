// TODO: Config and axios Will be used
import axios from "axios";
import * as _ from "lodash";
import getEnvUrl from "../utilities/getEnvUrl";
// import { encryption, decryption } from "../apis/login";

/**
 ** checkOnlinePaymentAndSubmission
 * @description check if data sync needed
 * @param {string} authToken - authToken from AXA api provide
 * @param {string} clientId - client id ex:CP00000123
 * @param {string} agentCode -  agent code
 * @param {string} lastDataSyncTime -  unix time
 * @param {string} environment -  application object
 * @param {function} callback
 * */
export default ({
  authToken,
  clientId,
  agentCode,
  lastDataSyncTime,
  environment,
  callback
}) => {
  const env = getEnvUrl(environment, "CHECK_PAYMENT_AND_SUBMISSION_URL");
  const cb = _.isFunction(callback) ? callback : () => {};
  return new Promise((resolve, reject) => {
    if (!authToken) {
      reject(Error("No authToken").toString());
    } else {
      const options = {
        method: "post",
        headers: { Authorization: `Bearer ${authToken}` },
        baseURL: env.baseURL,
        url: env.url || "/dataSync/checkOnlinePaymentAndSubmission",
        data: {
          agentCode,
          clientId,
          lastSyncTime: lastDataSyncTime
        }
      };
      // --> CR 197, disable encryption temporarily and then roll back
      // encryption(options.data, authToken, environment)
      //   .then(encryptBody => {
      //     options.data.d0 = encryptBody.result.d0;
      //     options.data.dk = encryptBody.result.dk;
      //     axios
      //       .request(options)
      //       .then(responseBody => {
      //         decryption(responseBody.data, encryptBody.result.aesKey)
      //           .then(decryptedResponseBody => {
      //             if (responseBody.status === 200) {
      //               const responseResult = JSON.parse(decryptedResponseBody);
      //               if (responseResult.status === "success") {
      //                 resolve(responseResult.result);
      //               }
      //             } else {
      //               reject(new Error("Fail to check"));
      //             }
      //           })
      //           .catch(() => {
      //             reject(new Error("decryption error"));
      //           });
      //       })
      //       .catch(error => {
      //         reject(new Error(`request error: ${error}`));
      //       });
      //   })
      //   .catch(() => {
      //     reject(new Error("encryption error"));
      //   });

      axios
        .request(options)
        .then(responseBody => {
          if (responseBody.status === 200) {
            const responseResult = responseBody.data;
            if (responseResult.status === "success") {
              resolve(responseResult.result);
            }
          } else {
            reject(new Error("Fail to check"));
          }
        })
        .catch(error => {
          reject(new Error(`request error: ${error}`));
        });
    }
  })
    .then(result => {
      cb();
      return {
        hasError: false,
        result
      };
    })
    .catch(error => {
      cb();
      return {
        hasError: true,
        errorMsg: error
      };
    });
};
