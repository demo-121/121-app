//
//  PdfGenerator.h
//  EASE
//
//  Created by Osric Wong on 18/5/2018.
//  Copyright © 2018 Facebook. All rights reserved.
//

#import <React/RCTBridgeModule.h>
#import <Foundation/Foundation.h>
#import <React/RCTView.h>
#import "UIPrintPageRenderer+PDF.h"

@interface PdfGenerator : RCTView <RCTBridgeModule, UIWebViewDelegate>

@end
