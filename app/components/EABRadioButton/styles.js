import { StyleSheet } from "react-native";
import Theme from "../../theme";

const row = {
  flexDirection: "row",
  paddingVertical: Theme.alignmentM,
  maxWidth: "100%"
};

export default StyleSheet.create({
  container: {
    width: "100%"
  },
  row,
  rowFirst: {
    ...row,
    marginTop: 0
  },
  button: {
    alignItems: "center",
    justifyContent: "center",
    margin: 2,
    marginRight: Theme.alignmentL + 2,
    borderRadius: Theme.radiusL,
    borderWidth: 2,
    width: 20,
    height: 20
  },
  point: {
    borderRadius: Theme.radiusL,
    width: 10,
    height: 10
  },
  title: {
    ...Theme.bodyPrimary,
    marginVertical: 1
  },
  replacedTitle: {
    fontSize: Theme.fontSizeXXM,
    marginBottom: Theme.alignmentXL,
    textDecorationLine: "underline"
  }
});
