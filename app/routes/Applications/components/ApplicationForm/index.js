import React, { PureComponent } from "react";
import { View, KeyboardAvoidingView, Alert } from "react-native";
import PropTypes from "prop-types";
import * as _ from "lodash";
import {
  SECTION_KEYS,
  REDUCER_TYPES,
  REDUCER_TYPE_CHECK,
  utilities,
  EAPP
} from "eab-web-api";
import APP_REDUCER_TYPE_CHECK from "../../../../constants/REDUCER_TYPE_CHECK";
import { TEXT_STORE } from "../../../../constants/REDUCER_TYPES";
import EABSectionList from "../../../../components/EABSectionList";
import EABi18n from "../../../../utilities/EABi18n";
import { ContextConsumer } from "../../../../context";
import {
  FAIL_GET_POLICY_NO_DOC,
  FAIL_CREATE_POLICY_NO_DOC,
  FAIL_TO_UPDATE_POLICY_NO_DOC,
  POLICY_NO_EXHAUSTED,
  FAIL_TO_GET_PN_FROM_API,
  NO_POLICY_NUMBER_AVAILABLE
} from "../../../../constants/ERROR_MSG";
import { POLICY_NUMBER_ERROR } from "../../../../constants/ERROR_TYPE";
import ApplicationFormPersonalDetails from "../../containers/ApplicationFormPersonalDetails";
import ApplicationFormDynamic from "../../containers/ApplicationFormDynamic";
import styles from "./styles";

const { APPLICATION } = REDUCER_TYPES;
const { PERSONAL_DETAILS, RESIDENCY, INSURABILITY, DECLARATION } = SECTION_KEYS[
  APPLICATION
];
const {
  checkApplicationFormSectionHasError,
  checkHasPolicyNumber,
  checkHasInsurability
} = utilities.application;

// TODO don't use index, use SECTION_KEYS
const RESIDENCY_MENUSECTION_INDEX = 0;
const RESIDENCY_MENUITEM_INDEX = 1;
const INSURABILITY_MENUSECTION_INDEX = 0;
const INSURABILITY_MENUITEM_INDEX = 2;
const DECLARATION_MENUSECTION_INDEX = 2;
const DECLARATION_MENUITEM_INDEX = 0;

/**
 * ApplicationForm
 * @description ApplicationForm page.
 * @param {object} textStore - reducer textStore state
 * @param {string} selectedSectionKey - indicate key of selected section item
 * @param {func} updateUiSelectedSectionKey - action to update key of selected section item
 * */

class ApplicationForm extends PureComponent {
  constructor(props) {
    super(props);

    this.prepareSectionListOptions = this.prepareSectionListOptions.bind(this);
  }

  componentDidMount() {
    // TODO update selected section based on completeness of form
    this.props.updateUiSelectedSectionKey(PERSONAL_DETAILS);
  }

  hasInsurability() {
    const { application, template } = this.props;
    return checkHasInsurability({ application, template });
  }

  checkSectionIsCompleted({ sectionKey, hasInsurability }) {
    const { application, error } = this.props;
    let hasPolicyNumber = true;
    if (!hasInsurability && sectionKey === DECLARATION) {
      hasPolicyNumber = checkHasPolicyNumber({ application });
    } else if (hasInsurability && sectionKey === INSURABILITY) {
      hasPolicyNumber = checkHasPolicyNumber({ application });
    }

    const hasError = checkApplicationFormSectionHasError({
      sectionKey,
      error
    });

    // return !hasError && hasPolicyNumber;

    const checkedMenuArr = _.get(
      application,
      "applicationForm.values.checkedMenu",
      []
    );
    const hasChecked = _.includes(checkedMenuArr, sectionKey);

    return !hasError && hasPolicyNumber && hasChecked;
  }

  prepareSectionListOptions({ language, isShield }) {
    const { textStore, selectedSectionKey } = this.props;
    const hasInsurability = this.hasInsurability();

    const personalOption = {
      key: `appform-personal`,
      id: PERSONAL_DETAILS,
      testID: "PersonalDetails",
      title: EABi18n({
        path: "application.form.section.personal",
        language,
        textStore
      }),
      selected: selectedSectionKey === PERSONAL_DETAILS,
      completed: this.checkSectionIsCompleted({
        sectionKey: PERSONAL_DETAILS,
        hasInsurability
      })
    };

    const residencyOption = {
      key: `appform-residency`,
      id: RESIDENCY,
      testID: "ResidencyInsurance",
      title: EABi18n({
        path: isShield
          ? "application.form.section.residency.shield"
          : "application.form.section.residency",
        language,
        textStore
      }),
      selected: selectedSectionKey === RESIDENCY,
      completed: this.checkSectionIsCompleted({
        sectionKey: RESIDENCY,
        hasInsurability
      })
    };

    const insurabilityOption = {
      key: `appform-insurability`,
      id: INSURABILITY,
      testID: "Insurability",
      title: EABi18n({
        path: "application.form.section.insurability",
        language,
        textStore
      }),
      selected: selectedSectionKey === INSURABILITY,
      completed: this.checkSectionIsCompleted({
        sectionKey: INSURABILITY,
        hasInsurability
      })
    };

    const declarationOption = {
      key: `appform-declaration`,
      id: DECLARATION,
      testID: "Declaration",
      title: EABi18n({
        path: "application.form.section.declaration",
        language,
        textStore
      }),
      selected: selectedSectionKey === DECLARATION,
      completed: this.checkSectionIsCompleted({
        sectionKey: DECLARATION,
        hasInsurability
      })
    };

    const options = [personalOption, residencyOption].concat(
      hasInsurability
        ? [insurabilityOption, declarationOption]
        : [declarationOption]
    );

    return [
      {
        key: "section",
        data: options,
        title: ""
      }
    ];
  }

  checkIsShieldPolicyNumberExist() {
    const { application } = this.props;
    let policyNumberLength = 0;
    const policyNumber = [];
    Object.values(application.iCidMapping).forEach(la => {
      la.forEach(value => {
        policyNumberLength += 1;
        if (value.policyNumber && value.policyNumber !== "") {
          policyNumber.push(value.policyNumber);
        }
      });
    });
    if (policyNumberLength !== policyNumber.length) {
      return false;
    }
    return true;
  }

  render() {
    const {
      selectedSectionKey,
      updateUiSelectedSectionKey,
      saveForm,
      application,
      genPolicyNumber,
      updatePolicyNumber,
      checkAndRefillPolicyNumber,
      saveFormShield,
      submitPolicyNumber
    } = this.props;

    const isShield = application.quotation.quotType === "SHIELD";

    const hasInsurability = this.hasInsurability();

    let form = null;
    switch (selectedSectionKey) {
      case PERSONAL_DETAILS:
        form = <ApplicationFormPersonalDetails />;
        break;
      case RESIDENCY:
        form = (
          <ApplicationFormDynamic
            menuSection={RESIDENCY_MENUSECTION_INDEX}
            menuItem={RESIDENCY_MENUITEM_INDEX}
            pageId="residency"
          />
        );
        break;
      case INSURABILITY:
        form = (
          <ApplicationFormDynamic
            menuSection={INSURABILITY_MENUSECTION_INDEX}
            menuItem={INSURABILITY_MENUITEM_INDEX}
            pageId="insurability"
          />
        );
        break;
      case DECLARATION:
        form = (
          <ApplicationFormDynamic
            menuSection={DECLARATION_MENUSECTION_INDEX}
            menuItem={DECLARATION_MENUITEM_INDEX}
            pageId="declaration"
          />
        );
        break;
      default:
        form = null;
        break;
    }

    return (
      <ContextConsumer>
        {({ language }) => (
          <View style={styles.container}>
            <View style={styles.sectionListView}>
              <EABSectionList
                testID="Application__AppForm"
                sections={this.prepareSectionListOptions({
                  language,
                  isShield
                })}
                showCompleteIcon
                onPress={id => {
                  if (isShield) {
                    if (
                      !this.checkIsShieldPolicyNumberExist() &&
                      ((hasInsurability && id === INSURABILITY) ||
                        (!hasInsurability && id === DECLARATION))
                    ) {
                      genPolicyNumber({
                        isShield: true,
                        numOfShield: application.childIds.length,
                        numOfNonShield: 0,
                        callback: response => {
                          if (!response.hasError) {
                            submitPolicyNumber({
                              isShield: true,
                              policyNumber: response.result.shieldPolicyNum,
                              callback: submitResponse => {
                                if (submitResponse.hasError) {
                                  Alert.alert(
                                    `Submit policy number error: ${
                                      submitResponse.errorMsg
                                    }`
                                  );
                                } else {
                                  // if shield, update and save application into couchbase
                                  updatePolicyNumber({
                                    policyNumber:
                                      response.result.shieldPolicyNum,
                                    isShield: true
                                  });
                                  checkAndRefillPolicyNumber(result => {
                                    if (result.hasError) {
                                      if (
                                        result.errorType ===
                                          POLICY_NUMBER_ERROR &&
                                        result.errorMsg === POLICY_NO_EXHAUSTED
                                      ) {
                                        Alert.alert(
                                          `Error Message: ${POLICY_NO_EXHAUSTED}`
                                        );
                                      } else {
                                        // no need to throw
                                      }
                                    }
                                  });
                                }
                              }
                            });
                          } else if (response.errorMsg) {
                            Alert.alert(`Error Message: ${response.errorMsg}`);
                          } else if (response) {
                            Alert.alert(`Error: ${response}`);
                          }
                          updateUiSelectedSectionKey(id);
                        }
                      });
                    } else {
                      saveFormShield({
                        actionType: EAPP.action.SWITCHMENU,
                        callback: () => {
                          updateUiSelectedSectionKey(id);
                        }
                      });
                    }
                  } else if (
                    !application.policyNumber &&
                    ((hasInsurability && id === INSURABILITY) ||
                      (!hasInsurability && id === DECLARATION))
                  ) {
                    genPolicyNumber({
                      isShield: false,
                      numOfShield: 0,
                      numOfNonShield: 1,
                      callback: response => {
                        if (!response.hasError) {
                          submitPolicyNumber({
                            isShield: false,
                            policyNumber: response.result.nonShieldPolicyNum,
                            callback: submitResponse => {
                              if (submitResponse.hasError) {
                                Alert.alert(
                                  `Submit policy number error: ${
                                    submitResponse.errorMsg
                                  }`
                                );
                              } else {
                                updatePolicyNumber({
                                  policyNumber:
                                    response.result.nonShieldPolicyNum,
                                  isShield: false
                                });
                                checkAndRefillPolicyNumber(result => {
                                  if (result.hasError) {
                                    if (
                                      result.errorType ===
                                        POLICY_NUMBER_ERROR &&
                                      result.errorMsg === POLICY_NO_EXHAUSTED
                                    ) {
                                      Alert.alert(
                                        `Error Message: ${POLICY_NO_EXHAUSTED}`
                                      );
                                    } else {
                                      // no need to throw
                                    }
                                  }
                                });
                              }
                            }
                          });
                        } else if (response.errorMsg) {
                          if (
                            response.errorMsg === FAIL_GET_POLICY_NO_DOC ||
                            response.errorMsg === FAIL_CREATE_POLICY_NO_DOC ||
                            response.errorMsg ===
                              FAIL_TO_UPDATE_POLICY_NO_DOC ||
                            response.errorMsg === POLICY_NO_EXHAUSTED ||
                            response.errorMsg === FAIL_TO_GET_PN_FROM_API ||
                            response.errorMsg === NO_POLICY_NUMBER_AVAILABLE
                          ) {
                            Alert.alert(`Error Message: ${response.errorMsg}`);
                          } else {
                            Alert.alert(
                              `Error Message: Gen Policy Number Error: ${
                                response.errorMsg
                              }`
                            );
                          }
                        }
                        saveForm({
                          genPDF: false,
                          actionType: EAPP.action.SWITCHMENU,
                          callback: () => {
                            updateUiSelectedSectionKey(id);
                          }
                        });
                      }
                    });
                  } else {
                    saveForm({
                      genPDF: false,
                      actionType: EAPP.action.SWITCHMENU,
                      callback: () => {
                        updateUiSelectedSectionKey(id);
                      }
                    });
                  }
                }}
              />
            </View>
            <KeyboardAvoidingView
              behavior="padding"
              keyboardVerticalOffset={100}
              style={styles.detailView}
            >
              {form}
            </KeyboardAvoidingView>
          </View>
        )}
      </ContextConsumer>
    );
  }
}

ApplicationForm.propTypes = {
  template: REDUCER_TYPE_CHECK[REDUCER_TYPES.APPLICATION].template.isRequired,
  textStore: APP_REDUCER_TYPE_CHECK[TEXT_STORE].isRequired,
  selectedSectionKey: PropTypes.string.isRequired,
  updateUiSelectedSectionKey: PropTypes.func.isRequired,
  updatePolicyNumber: PropTypes.func.isRequired,
  submitPolicyNumber: PropTypes.func.isRequired,
  checkAndRefillPolicyNumber: PropTypes.func.isRequired,
  saveForm: PropTypes.func.isRequired,
  saveFormShield: PropTypes.func.isRequired,
  genPolicyNumber: PropTypes.func.isRequired,
  error: REDUCER_TYPE_CHECK[APPLICATION].error.isRequired,
  application:
    REDUCER_TYPE_CHECK[REDUCER_TYPES.PRE_APPLICATION].ApplicationItem.value
      .isRequired
};
export default ApplicationForm;
