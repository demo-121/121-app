import React, { Component } from "react";
import { View, Text, Image, ScrollView } from "react-native";
import PropTypes from "prop-types";
import * as _ from "lodash";
import {
  REDUCER_TYPE_CHECK as WEB_API_REDUCER_TYPE_CHECK,
  REDUCER_TYPES,
  PRODUCT_TYPES,
  NEEDS,
  PROGRESS_TABS
} from "eab-web-api";
import REDUCER_TYPE_CHECK from "../../../../constants/REDUCER_TYPE_CHECK";
import EABFloatingCard from "../../../../components/EABFloatingCard";
import ciIcon from "../../../../assets/images/ciIcon.png";
import accidentIcon from "../../../../assets/images/accidentIcon.png";
import wholeLifeIcon from "../../../../assets/images/wholeLifeIcon.png";
import investmentIcon from "../../../../assets/images/investmentIcon.png";
import EABi18n from "../../../../utilities/EABi18n";
import { TEXT_STORE } from "../../../../constants/REDUCER_TYPES";
import { ContextConsumer } from "../../../../context";
import styles from "./styles";
import Theme from "../../../../theme";
import PopoverTooltip from "../../../../components/PopoverTooltip/PopoverTooltipIOS";

const { FNA } = REDUCER_TYPES;
const {
  INVESTLINKEDPLANS,
  PARTICIPATINGPLANS,
  NONPARTICIPATINGPLAN,
  ACCIDENTHEALTHPLANS
} = PRODUCT_TYPES;

export default class ProductTypes extends Component {
  constructor(props) {
    super(props);
    this.state = {};

    const { CIPROTECTION, PAPROTECTION, PCHEADSTART, HCPROTECTION } = NEEDS;

    const { na } = props;
    const needsAspect = na.aspects.split(",");

    this.ilpDisable = false;
    this.ppDisable = false;
    this.nppDisable = false;
    this.ahpDisable = false;

    // if ciProtection or pcHeadStart selected
    if (
      needsAspect.indexOf(CIPROTECTION) > -1 ||
      needsAspect.indexOf(PCHEADSTART) > -1
    ) {
      this.nppDisable = true;
    }

    // if (only paProtection) or (only hcProtection) or (both paProtection and hcProtection) selected
    if (
      na.aspects === PAPROTECTION ||
      na.aspects === HCPROTECTION ||
      _.isEqual(needsAspect.sort(), [PAPROTECTION, HCPROTECTION].sort())
    ) {
      this.ilpDisable = true;
      this.ppDisable = true;
      this.nppDisable = true;
      this.ahpDisable = true;
    } else if (
      // if (paProtection with other needs) or (hcProtection with other needs)
      needsAspect.indexOf(PAPROTECTION) > -1 ||
      needsAspect.indexOf(HCPROTECTION) > -1
    ) {
      this.ahpDisable = true;
    }
  }

  componentDidMount() {
    this.props.initProductTypes();
  }

  render() {
    const { na, textStore } = this.props;
    const selectedProductTypes = na.productType.prodType.split(",");

    const renderProductTypes = language => {
      const productTypes = [
        {
          key: INVESTLINKEDPLANS,
          testID: "InvestmentLinkedPlan",
          title: EABi18n({
            path: "na.productType.investmentPlan",
            language,
            textStore
          }),
          icon: investmentIcon
        },
        {
          key: PARTICIPATINGPLANS,
          testID: "WholeLife/EndowmentPlan",
          title: EABi18n({
            path: "na.productType.wholeLifePlan",
            language,
            textStore
          }),
          icon: wholeLifeIcon
        },
        {
          key: NONPARTICIPATINGPLAN,
          testID: "CriticalIllness/TermPlan",
          title: EABi18n({
            path: "na.productType.criticalPlan",
            language,
            textStore
          }),
          icon: ciIcon
        },
        {
          key: ACCIDENTHEALTHPLANS,
          testID: "Accident/MedicalPlan",
          title: EABi18n({
            path: "na.productType.accidentPlan",
            language,
            textStore
          }),
          icon: accidentIcon
        }
      ];

      return productTypes.map(productTypesObj => {
        let disable;
        const isSelected =
          selectedProductTypes.indexOf(productTypesObj.key) > -1;

        switch (productTypesObj.key) {
          case INVESTLINKEDPLANS: {
            disable = this.ilpDisable;
            break;
          }
          case PARTICIPATINGPLANS: {
            disable = this.ppDisable;
            break;
          }
          case NONPARTICIPATINGPLAN: {
            disable = this.nppDisable;
            break;
          }
          case ACCIDENTHEALTHPLANS: {
            disable = this.ahpDisable;
            break;
          }
          default:
            break;
        }

        return (
          <EABFloatingCard
            testID={`NeedsAnalysis__PT__${productTypesObj.testID}`}
            isFullBoxImageBackground
            style={styles.floatingCard}
            hasBorder
            disable={disable}
            isSelected={isSelected}
            onPress={selected => {
              if (selected) {
                // Add Item
                selectedProductTypes.push(productTypesObj.key);
              } else {
                // Remove Item
                const index = selectedProductTypes.indexOf(productTypesObj.key);
                if (index > -1) {
                  selectedProductTypes.splice(index, 1);
                }
              }
              this.props.updateNAValue({
                target: PROGRESS_TABS[FNA].PRODUCT_TYPES,
                value: selectedProductTypes.join()
              });
            }}
          >
            <View style={styles.floatingCardContent}>
              <Image
                source={productTypesObj.icon}
                style={styles.floatingCardImage}
              />
              <Text style={styles.floatingCardText}>
                {productTypesObj.title}
              </Text>
            </View>
          </EABFloatingCard>
        );
      });
    };

    return (
      <ContextConsumer>
        {({ language }) => (
          <ScrollView
            style={styles.scrollView}
            contentContainerStyle={styles.scrollViewContent}
          >
            <View style={styles.container}>
              <View style={styles.wrapper}>
                <Text style={styles.statement}>
                  Please select the types of products you would like to
                  consider.
                  <Text style={Theme.fieldTextOrHelperTextNegative}> * </Text>
                </Text>

                {na.aspects === NEEDS.PAPROTECTION && (
                  <View style={{ paddingTop: Theme.alignmentL }}>
                    <PopoverTooltip
                      popoverOptions={{ preferredContentSize: [300, 100] }}
                      text={EABi18n({
                        textStore,
                        language,
                        path: "na.productType.accidentPlan.tooltip"
                      })}
                    />
                  </View>
                )}
              </View>
              {_.isEmpty(this.props.na.productType.prodType) ? (
                <Text style={Theme.fieldTextOrHelperTextNegative}>
                  {EABi18n({
                    language,
                    textStore,
                    path: "error.302"
                  })}
                </Text>
              ) : null}
              <View style={styles.prodTypeWrap}>
                {renderProductTypes(language)}
              </View>
            </View>
          </ScrollView>
        )}
      </ContextConsumer>
    );
  }
}

ProductTypes.propTypes = {
  na: WEB_API_REDUCER_TYPE_CHECK[FNA].na.isRequired,
  initProductTypes: PropTypes.func.isRequired,
  updateNAValue: PropTypes.func.isRequired,
  textStore: REDUCER_TYPE_CHECK[TEXT_STORE].isRequired
};
