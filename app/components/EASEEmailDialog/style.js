import { StyleSheet } from "react-native";
import Colors from "../../theme/colors";
import Theme from "../../theme";

/**
 * styles
 * */
export default StyleSheet.create({
  container: {
    flexDirection: "row",
    marginRight: Theme.alignmentXL
  },
  buttonFirst: {
    marginRight: Theme.alignmentXL
  },
  dialogHeader: {
    flexDirection: "column",
    borderBottomColor: Colors.coolGrey,
    borderBottomWidth: 1,
    paddingTop: 16,
    paddingLeft: 24,
    paddingRight: 24,
    paddingBottom: 16
  },
  dialogButtonWrapper: {
    flexDirection: "row",
    justifyContent: "space-between"
  },
  headerButton: {
    ...Theme.textButtonLabelNormalAccent
  },
  headerMiddleText: {
    fontSize: 17
  }
});
