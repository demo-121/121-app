import { StyleSheet } from "react-native";
import Theme from "../../../../theme";

const selectorField = {
  marginBottom: Theme.alignmentXS,
  backgroundColor: Theme.white,
  height: 40,
  width: "100%"
};

export default StyleSheet.create({
  container: {
    flexDirection: "row",
    paddingHorizontal: Theme.alignmentXL,
    width: "100%",
    height: "100%"
  },
  scrollViewFilter: {
    flexBasis: "auto",
    marginRight: Theme.alignmentXL,
    maxWidth: 308,
    height: "100%"
  },
  scrollViewCase: {
    flex: 1,
    width: "100%",
    height: "100%"
  },
  BoardContainer: {
    marginVertical: Theme.alignmentXL,
    width: "100%"
  },
  sumbitButton: Theme.rectangleButtonStyle1Label,
  findCaseBoard: {
    alignItems: "flex-start",
    marginBottom: Theme.alignmentL,
    padding: Theme.alignmentL,
    backgroundColor: Theme.brandSuperTransparent,
    width: "100%"
  },
  searchField: {
    marginBottom: Theme.alignmentL
  },
  filterBoard: {
    alignItems: "flex-start",
    paddingHorizontal: Theme.alignmentL,
    paddingBottom: Theme.alignmentL,
    backgroundColor: Theme.brandSuperTransparent,
    width: "100%"
  },
  filterBoardHeader: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    height: 48,
    width: "100%"
  },
  filterBoardHeaderTitle: Theme.headerBrand,
  selectAllText: Theme.textButtonLabelSmallAccent,
  checkBoxGroup: {
    marginBottom: Theme.alignmentXS
  },
  sectionHeader: {
    justifyContent: "center",
    alignItems: "flex-start",
    height: 40
  },
  sectionHeaderTitle: Theme.headerBrand,
  selectorField,
  selectorFieldLast: {
    ...selectorField,
    marginBottom: Theme.alignmentXL
  }
});
