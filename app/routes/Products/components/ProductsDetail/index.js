import { REDUCER_TYPE_CHECK, REDUCER_TYPES, utilities } from "eab-web-api";
import * as _ from "lodash";
import PropTypes from "prop-types";
import React, { Component } from "react";
import {
  Image,
  ScrollView,
  Text,
  TouchableWithoutFeedback,
  View,
  Alert
} from "react-native";
import LinearGradient from "react-native-linear-gradient";
import { NavigationActions } from "react-navigation";
import EABButton from "../../../../components/EABButton";
import { RECTANGLE_BUTTON_1 } from "../../../../components/EABButton/constants";
import TranslatedText from "../../../../containers/TranslatedText";
import noRecords from "../../../../assets/images/noRecords.png";
import EABDialog from "../../../../components/EABDialog";
import { SCREEN } from "../../../../components/EABDialog/constants";
import EABHUD from "../../../../containers/EABHUD";
import { ContextConsumer } from "../../../../context";
import Theme from "../../../../theme";
import {
  getAttachmentWithID,
  getDocumentWithID
} from "../../../../utilities/DataManager";
import getEABTextByLangType from "../../../../utilities/getEABTextByLangType";
import Quotation from "../../../Quotation";
import styles from "./styles";
import Selector from "../../../../components/Selector";
import { FLAT_LIST } from "../../../../components/Selector/constants";
import EABCard from "../../../../components/EABCard";
import EABi18n from "../../../../utilities/EABi18n";
import confirmationAlert from "../../../../utilities/confirmationAlert";
import EABSegmentedControl from "../../../../components/EABSegmentedControl";
import ClientFormContact from "../../../../containers/ClientFormContact";
import APP_REDUCER_TYPE_CHECK from "../../../../constants/REDUCER_TYPE_CHECK";
import ClientFormProfile from "../../../../containers/ClientFormProfile";
import { TEXT_STORE } from "../../../../constants/REDUCER_TYPES";

const { PRODUCTS, OPTIONS_MAP, CLIENT } = REDUCER_TYPES;
const { getProductId } = utilities;
const { isClientFormHasError } = utilities.validation;

/**
 * ProductsDetail
 * @description ProductsDetail page.
 * @param {object} navigation - react navigation props
 * @param currency - reducer products.currency state
 * @param insuredCid - reducer products.insuredCid state
 * @param dependants - reducer products.dependants state
 * @param productList - reducer products.productList state
 * @param {function} getQuotation - redux action. trigger saga to get quotation
 * */
export default class ProductsDetail extends Component {
  constructor(props) {
    super(props);
    // =========================================================================
    // variables
    // =========================================================================
    this.segmentedControlArray = [
      {
        key: "A",
        title: "Profile",
        isShowingBadge: false,
        onPress: () => {
          this.setState({ showProfileTab: true });
        }
      },
      {
        key: "B",
        title: "Contact",
        isShowingBadge: false,
        onPress: () => {
          this.setState({ showProfileTab: false });
        }
      }
    ];
    this.closeDialogFunction = () => {
      this.setState({ isOpenQuotationDialog: false });
    };
    this.setDialogFunction = ({
      setCloseQuotationDialog,
      closeQuotationDialog
    }) => {
      if (closeQuotationDialog !== this.closeDialogFunction) {
        setCloseQuotationDialog(this.closeDialogFunction);
      }
    };
    this.getContentView = this.getContentView.bind(this);
    this.loadImageFunction = this.loadImageFunction.bind(this);
    this.getErrorView = this.getErrorView.bind(this);
    this.getCurrencyList = this.getCurrencyList.bind(this);
    this.getWarningAlert = this.getWarningAlert.bind(this);
    this.queryQuotationWithoutConfirm = this.queryQuotationWithoutConfirm.bind(
      this
    );
    this.queryQuotationWithConfirm = this.queryQuotationWithConfirm.bind(this);

    this.state = {
      isOpenQuotationDialog: false,
      // DO NOT SET THIS STATE TO TRUE!!!! this state will auto update when
      // productList change! any set this state will crush this page behavior
      isSavingClient: false,
      isLoadingProducts: true,
      isLoadingQuotation: false,
      sectionIndex: 0,
      productIndex: 0,
      productsLayout: [],
      isShowingCreateProfileDialog: false,
      showProfileTab: "A",
      productRowWidth: 0
    };
  }
  // ===========================================================================
  // life circles
  // ===========================================================================

  componentDidMount() {
    this.loadImageFunction();
    this.didFocusSubscription = this.props.navigation.addListener(
      "didFocus",
      this.props.toProductPage
    );
  }

  componentWillReceiveProps(nextProps) {
    const { reloadProductPage } = this.props;
    const { reload } = nextProps;
    if (reload) {
      reloadProductPage(false);
      setTimeout(() => {
        this.setState({
          sectionIndex: 0,
          productIndex: 0,
          productsLayout: [],
          isLoadingProducts: true,
          isShowingCreateProfileDialog: false
        });
      }, 300);
    }
  }

  componentDidUpdate() {
    this.loadImageFunction();
  }

  componentWillUnmount() {
    this.didFocusSubscription.remove();
  }
  // ===========================================================================
  // getters
  // ===========================================================================
  /**
   * @description indicate can save client or not
   * @return {boolean} errorStatus
   * */
  get isDisableSaveClient() {
    const { clientForm } = this.props;
    const { hasError } = isClientFormHasError(clientForm);
    return hasError;
  }

  get dependantItems() {
    const { dependants } = this.props;

    return [
      { key: "myself", value: null, label: "For you" },
      ...dependants.map(dependant => ({
        key: dependant.cid,
        label: dependant.fullName,
        value: dependant.cid
      }))
    ];
  }
  /**
   * @description product alignment variable
   * @return {number}
   * */
  static get productAlignmentBase() {
    return Theme.alignmentXL;
  }
  /**
   * @description product size
   * @return {object}
   * */
  get productSize() {
    const { productAlignmentBase } = ProductsDetail;
    const { productRowWidth } = this.state;

    // show 7.5 products
    const productsNumber = 7.5;
    const marginNumber = 7;

    const width =
      productRowWidth !== 0
        ? (productRowWidth -
            productAlignmentBase -
            productAlignmentBase * marginNumber) /
          productsNumber
        : 0;

    return {
      shouldShowProducts: productRowWidth !== 0,
      width,
      height: width / 0.7
    };
  }
  // ===========================================================================
  // class methods
  // ===========================================================================
  getCurrencyList(language) {
    const { optionsMap } = this.props;
    return optionsMap.ccy.currencies[0].options.map(option => ({
      key: option.value,
      label: getEABTextByLangType({ data: option.title, language }),
      value: option.value
    }));
  }

  getContentView(language) {
    const { productAlignmentBase } = ProductsDetail;
    const {
      dependantItems,
      getCurrencyList,
      queryQuotationWithoutConfirm,
      productSize
    } = this;
    const { productsLayout, isLoadingProducts } = this.state;
    const {
      currencyOnChange,
      currency,
      insuredCid,
      insuredCidOnChange
    } = this.props;
    const { shouldShowProducts, width, height } = productSize;
    return (
      <View
        style={styles.container}
        onLayout={event => {
          this.setState({
            productRowWidth: event.nativeEvent.layout.width
          });
        }}
      >
        <View style={styles.optionsWrapper}>
          <Selector
            style={styles.selectorSizeFirst}
            selectorType={FLAT_LIST}
            flatListOptions={{
              renderItemOnPress: (itemKey, item) => {
                insuredCidOnChange(item.value);
              },
              selectedValue: insuredCid,
              data: dependantItems
            }}
          />
          <Selector
            style={styles.selectorSize}
            selectorType={FLAT_LIST}
            flatListOptions={{
              renderItemOnPress: (itemKey, item) => {
                currencyOnChange(item.value);
              },
              selectedValue: currency,
              data: getCurrencyList(language)
            }}
          />
        </View>
        <EABHUD isOpen={isLoadingProducts}>
          {this.getErrorView() || (
            <ScrollView
              testID="ProductsDetail__ScrollPage"
              style={styles.scrollView}
              showsVerticalScrollIndicator={false}
              contentContainerStyle={styles.scrollViewContent}
            >
              {productsLayout.map(section => (
                <View key={section.key} style={styles.section}>
                  <View style={styles.sectionHeader}>
                    <Text style={Theme.tagLine1Primary}>
                      {getEABTextByLangType({ data: section.title, language })}
                    </Text>
                    {/* <EABButton onPress={() => {}}>
                       <Text style={styles.seeAllButtonText}>See All</Text>
                       <Image
                       style={styles.seeAllButtonIcon}
                       source={icArrowActive}
                       />
                       </EABButton> */}
                  </View>
                  <ScrollView
                    style={styles.productScrollView}
                    contentContainerStyle={styles.productScrollViewContent}
                    horizontal
                    showsHorizontalScrollIndicator={false}
                  >
                    {shouldShowProducts
                      ? section.products.map((product, index) => (
                          <TouchableWithoutFeedback
                            testID={`ProductsDetail__btn${product.key}`}
                            key={product.key}
                            onPress={() => {
                              queryQuotationWithoutConfirm({
                                language,
                                product
                              });
                            }}
                          >
                            <View
                              style={{
                                marginLeft:
                                  index === 0 ? 0 : productAlignmentBase,
                                width
                              }}
                            >
                              <EABCard
                                style={[styles.productImage, { width, height }]}
                              >
                                <Image
                                  style={styles.productSize}
                                  source={{ uri: product.image }}
                                />
                              </EABCard>
                              <Text style={styles.productText}>
                                {getEABTextByLangType({
                                  data: product.title,
                                  language
                                })}
                              </Text>
                            </View>
                          </TouchableWithoutFeedback>
                        ))
                      : null}
                  </ScrollView>
                </View>
              ))}
            </ScrollView>
          )}
        </EABHUD>
      </View>
    );
  }

  getErrorView() {
    const {
      errorMsg,
      profile,
      insuredCid,
      getProfileData,
      readyToEditProfile,
      loadClient,
      navigation
    } = this.props;
    let missingDataTitle = "";
    let missingDataHint = "";
    let goTo = "";
    if (!errorMsg) {
      return null;
    }
    switch (errorMsg) {
      case "missingProposerInfo":
      case "missingInsuredInfo":
        missingDataTitle = "products.text.missingDataProfileTitle";
        missingDataHint = "products.text.missingDataProfileHint";
        goTo = "products.button.goToProfile";
        break;
      case "incompleteFNA":
        missingDataTitle = "products.text.missingDataFNATitle";
        missingDataHint = "products.text.missingDataFNAHint";
        goTo = "products.button.goToFNA";
        break;
      case "noData":
        missingDataTitle = "products.text.noEligibleProductTitle";
        missingDataHint = "products.text.noEligibleProductHint";
        goTo = "products.button.goToProfile";
        break;
      default:
        throw new Error(`unexpected error message: "${errorMsg}"`);
    }

    return (
      <View style={styles.noRecordsContainer}>
        <View style={styles.noRecordsView}>
          <Image source={noRecords} />
          <TranslatedText
            style={styles.missingDataTitle}
            path={missingDataTitle}
          />
          <TranslatedText
            style={styles.missingDataHint}
            path={missingDataHint}
          />
          <EABButton
            buttonType={RECTANGLE_BUTTON_1}
            onPress={() => {
              switch (errorMsg) {
                case "missingInsuredInfo": {
                  loadClient(this.props.dependantProfiles[insuredCid]);
                  readyToEditProfile({
                    isFamilyMember: true,
                    callback: () => {
                      this.setState({
                        isShowingCreateProfileDialog: true
                      });
                    }
                  });
                  break;
                }
                case "missingProposerInfo": {
                  getProfileData({
                    cid: profile.cid,
                    callback: () => {
                      readyToEditProfile({
                        isFamilyMember: false,
                        callback: () => {
                          this.setState({
                            isShowingCreateProfileDialog: true
                          });
                        }
                      });
                    }
                  });
                  break;
                }
                case "incompleteFNA": {
                  this.props.getFNA(() => {
                    navigation.dispatch(
                      NavigationActions.navigate({
                        routeName: "Needs"
                      })
                    );
                  });
                  break;
                }
                case "noData": {
                  const targetCid = insuredCid || profile.cid;
                  // as web design, they use null to indicate client instead
                  // client.cid
                  const isFamilyMember = insuredCid !== null;
                  if (isFamilyMember) {
                    loadClient(this.props.dependantProfiles[targetCid]);
                    readyToEditProfile({
                      isFamilyMember,
                      callback: () => {
                        this.setState({
                          isShowingCreateProfileDialog: true
                        });
                      }
                    });
                  } else {
                    getProfileData({
                      cid: targetCid,
                      callback: () => {
                        readyToEditProfile({
                          isFamilyMember,
                          callback: () => {
                            this.setState({
                              isShowingCreateProfileDialog: true
                            });
                          }
                        });
                      }
                    });
                  }
                  break;
                }
                default:
                  throw new Error(`unexpected errorMsg: ${errorMsg}`);
              }
            }}
          >
            <TranslatedText path={goTo} />
            {/* <Image
              style={styles.seeAllButtonIcon}
              source={icArrowActive}
            /> */}
          </EABButton>
        </View>
      </View>
    );
  }

  getWarningAlert({ message, language }) {
    const { textStore } = this.props;
    Alert.alert(
      EABi18n({
        textStore,
        language,
        path: "alert.warning"
      }),
      message,
      [
        {
          text: EABi18n({
            textStore,
            language,
            path: "button.ok"
          }),
          onPress: () => {}
        }
      ]
    );
  }
  /**
   * @description function for query quotation with confirmation
   * @param {object} product - product data
   * @param {string} product.key - product id
   * @param {string} language
   * */
  queryQuotationWithConfirm({ product, language }) {
    const { textStore, getQuotation, closeSummaryProposal } = this.props;
    closeSummaryProposal();
    const { queryQuotationWithConfirm } = this;
    this.setState(
      {
        isLoadingQuotation: true
      },
      () => {
        getQuotation({
          productID: product.key,
          alert: message => {
            this.setState(
              {
                isLoadingQuotation: false
              },
              () => {
                setTimeout(() => {
                  this.getWarningAlert({ message, language });
                }, 200);
              }
            );
          },
          confirmRequest: messagePath => {
            this.setState(
              {
                isLoadingQuotation: false
              },
              () => {
                setTimeout(() => {
                  Alert.alert(
                    EABi18n({
                      textStore,
                      language,
                      path: "alert.confirm"
                    }),
                    EABi18n({
                      textStore,
                      language,
                      path: messagePath
                    }),
                    [
                      {
                        text: EABi18n({
                          textStore,
                          language,
                          path: "button.no"
                        }),
                        onPress: () => {}
                      },
                      {
                        text: EABi18n({
                          textStore,
                          language,
                          path: "button.yes"
                        }),
                        onPress: queryQuotationWithConfirm({
                          confirm: true,
                          product,
                          language
                        })
                      }
                    ]
                  );
                }, 200);
              }
            );
          },
          openQuotationFunction: () => {
            this.setState({
              isOpenQuotationDialog: true,
              isLoadingQuotation: false
            });
          },
          confirm: true
        });
      }
    );
  }
  /**
   * @description function for query quotation without confirmation
   * @param {object} product - product data
   * @param {string} product.key - product id
   * @param {string} language
   * */
  queryQuotationWithoutConfirm({ product, language }) {
    const { textStore, getQuotation, closeSummaryProposal } = this.props;
    closeSummaryProposal();
    this.setState(
      {
        isLoadingQuotation: true
      },
      () => {
        getQuotation({
          productID: product.key,
          alert: message => {
            this.setState(
              {
                isLoadingQuotation: false
              },
              () => {
                setTimeout(() => {
                  this.getWarningAlert({ message, language });
                }, 200);
              }
            );
          },
          confirmRequest: messagePath => {
            this.setState(
              {
                isLoadingQuotation: false
              },
              () => {
                setTimeout(() => {
                  confirmationAlert({
                    titlePath: "alert.confirm",
                    messagePath,
                    language,
                    textStore,
                    yesOnPress: () => {
                      this.setState(
                        {
                          isLoadingQuotation: true
                        },
                        () => {
                          this.queryQuotationWithConfirm({
                            product,
                            language
                          });
                        }
                      );
                    }
                  });
                }, 200);
              }
            );
          },
          openQuotationFunction: () => {
            this.setState({
              isOpenQuotationDialog: true,
              isLoadingQuotation: false
            });
          },
          confirm: false
        });
      }
    );
  }

  loadImageFunction() {
    const { productList, errorMsg } = this.props;
    const {
      productsLayout,
      sectionIndex,
      productIndex,
      isLoadingProducts
    } = this.state;
    if (errorMsg && isLoadingProducts) {
      this.setState({
        isLoadingProducts: false
      });
    }
    const newProductsLayout = _.cloneDeep(productsLayout);
    if (sectionIndex < productList.length) {
      if (productIndex === 0) {
        newProductsLayout[sectionIndex] = {
          key: getProductId(productList[sectionIndex].products[0]),
          title: productList[sectionIndex].title,
          products: []
        };
      }

      if (productIndex < productList[sectionIndex].products.length) {
        const targetProduct = productList[sectionIndex].products[productIndex];
        const productID = getProductId(targetProduct);

        getDocumentWithID(productID, dataA => {
          const targetProducts = newProductsLayout[sectionIndex].products;
          targetProducts[productIndex] = {
            key: productID,
            title: targetProduct.covName
          };

          if (dataA._attachments) {
            const getDataType = dataA._attachments.thumbnail3.content_type;
            const imageType = `data:${getDataType}`;

            getAttachmentWithID(productID, "thumbnail3", dataB => {
              targetProducts[productIndex].image = `${imageType};base64,${
                dataB.data
              }`;
              this.setState({
                productsLayout: newProductsLayout,
                sectionIndex,
                productIndex: productIndex + 1
              });
            });
          } else {
            this.setState({
              productsLayout: newProductsLayout,
              productIndex: productIndex + 1
            });
          }
        });
      } else {
        this.setState({
          sectionIndex: sectionIndex + 1,
          productIndex: 0
        });
      }
    }
    if (
      productList.length &&
      sectionIndex === productList.length &&
      productIndex === 0 &&
      isLoadingProducts
    ) {
      this.setState({
        isLoadingProducts: false
      });
    }
  }

  /**
   * @name renderClientForm
   * @return {element} edit client dialog component
   * */
  renderClientForm(language) {
    const { textStore } = this.props;
    const { isSavingClient, isShowingCreateProfileDialog } = this.state;
    return (
      <EABDialog isOpen={isShowingCreateProfileDialog}>
        <View style={Theme.dialogHeader}>
          <View style={styles.headerRow}>
            <EABButton
              onPress={() => {
                this.setState({ isShowingCreateProfileDialog: false });
                this.props.cancelCreateClient();
              }}
            >
              <Text style={Theme.textButtonLabelNormalAccent}>
                {EABi18n({
                  textStore,
                  language,
                  path: "button.cancel"
                })}
              </Text>
            </EABButton>
            <EABSegmentedControl
              segments={this.segmentedControlArray}
              activatedKey={this.state.showProfileTab ? "A" : "B"}
            />
            <EABButton
              onPress={() => {
                this.setState(
                  {
                    isSavingClient: true
                  },
                  () => {
                    this.props.saveClient(() => {
                      this.props.getProductList(() => {
                        // can not set state at the same time. multiple dialogs
                        // can not close at the same time.
                        this.setState({
                          isSavingClient: false
                        });
                      });
                    });
                  }
                );
              }}
              isDisable={this.isDisableSaveClient}
            >
              <Text style={Theme.textButtonLabelNormalAccent}>
                {EABi18n({
                  textStore,
                  language,
                  path: "button.done"
                })}
              </Text>
            </EABButton>
          </View>
          <EABHUD isOpen={isSavingClient} />
        </View>
        {this.state.showProfileTab ? (
          <ClientFormProfile />
        ) : (
          <ClientFormContact />
        )}
      </EABDialog>
    );
  }

  render() {
    // =========================================================================
    // variables
    // =========================================================================
    const { setDialogFunction } = this;
    const { isLoadingProducts, isLoadingQuotation } = this.state;

    return (
      <ContextConsumer>
        {({ language, setCloseQuotationDialog, closeQuotationDialog }) => (
          <LinearGradient
            {...Theme.LinearGradientProps}
            style={styles.container}
          >
            {this.getContentView(language)}
            {isLoadingProducts || isLoadingQuotation ? <EABHUD isOpen /> : null}
            <EABDialog type={SCREEN} isOpen={this.state.isOpenQuotationDialog}>
              <Quotation />
            </EABDialog>
            {/* apply close quotation dialog function */}
            {setDialogFunction({
              setCloseQuotationDialog,
              closeQuotationDialog
            })}
            {this.renderClientForm(language)}
          </LinearGradient>
        )}
      </ContextConsumer>
    );
  }
}

ProductsDetail.propTypes = {
  currency: REDUCER_TYPE_CHECK[PRODUCTS].currency.isRequired,
  insuredCid: REDUCER_TYPE_CHECK[PRODUCTS].insuredCid.isRequired,
  dependants: REDUCER_TYPE_CHECK[PRODUCTS].dependants.isRequired,
  productList: REDUCER_TYPE_CHECK[PRODUCTS].productList.isRequired,
  getQuotation: PropTypes.func.isRequired,
  getProductList: PropTypes.func.isRequired,
  optionsMap: REDUCER_TYPE_CHECK[OPTIONS_MAP].isRequired,
  currencyOnChange: PropTypes.func.isRequired,
  insuredCidOnChange: PropTypes.func.isRequired,
  errorMsg: PropTypes.string.isRequired,
  getErrorMsg: PropTypes.func.isRequired,
  cancelCreateClient: PropTypes.func.isRequired,
  textStore: APP_REDUCER_TYPE_CHECK[TEXT_STORE].isRequired,
  saveClient: PropTypes.func.isRequired,
  readyToEditProfile: PropTypes.func.isRequired,
  getProfileData: PropTypes.func.isRequired,
  loadClient: PropTypes.func.isRequired,
  dependantProfiles: REDUCER_TYPE_CHECK[CLIENT].dependantProfiles.isRequired,
  profile: REDUCER_TYPE_CHECK[CLIENT].profile.isRequired,
  clientForm: PropTypes.oneOfType([PropTypes.object]).isRequired,
  getFNA: PropTypes.func.isRequired,
  toProductPage: PropTypes.func.isRequired,
  closeSummaryProposal: PropTypes.func.isRequired,
  reloadProductPage: PropTypes.func.isRequired,
  reload: PropTypes.bool.isRequired
};
