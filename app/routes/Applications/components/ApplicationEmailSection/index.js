import {
  REDUCER_TYPE_CHECK as WEB_API_REDUCER_TYPE_CHECK,
  REDUCER_TYPES
} from "eab-web-api";
import PropTypes from "prop-types";
import SafariView from "react-native-safari-view";
import React, { PureComponent } from "react";
import Config from "react-native-config";
import { View, Alert, Linking, NetInfo } from "react-native";
import * as _ from "lodash";
import EABi18n from "../../../../utilities/EABi18n";
import EABHUD from "../../../../components/EABHUD";
import APP_REDUCER_TYPE_CHECK from "../../../../constants/REDUCER_TYPE_CHECK";
import { TEXT_STORE } from "../../../../constants/REDUCER_TYPES";
import EABButton from "../../../../components/EABButton";
import TranslatedText from "../../../../containers/TranslatedText";
import { ContextConsumer } from "../../../../context";
import styles from "./styles";
import EASEEmailContentDialog from "../../../../containers/EASEEmailContentDialog";

const { CLIENT, APPLICATION, PRE_APPLICATION } = REDUCER_TYPES;

/**
 * <ApplicationEmailSection />
 * @description Application Email Button in APP bar
 * @param {boolean} canEmail - boolean whether enable or disable the Email button
 * */
export default class ApplicationEmailSection extends PureComponent {
  constructor() {
    super();
    this.getAuthCodeCallback = this.getAuthCodeCallback.bind(this);
    this.setfunction = this.setfunction.bind(this);
    this.state = {
      isNetworkConnected: false,
      function: null,
      setOnlineDone: false,
      isLoading: false
    };
    this.handleConnectionChange = this.handleConnectionChange.bind(this);
    NetInfo.isConnected.fetch().done(this.handleConnectionChange);
  }

  componentDidMount() {
    Linking.addEventListener("url", this.getAuthCodeCallback);
    NetInfo.isConnected.addEventListener(
      "connectionChange",
      this.handleConnectionChange
    );
  }

  componentWillUnmount() {
    Linking.removeEventListener("url", this.getAuthCodeCallback);
    NetInfo.isConnected.removeEventListener(
      "connectionChange",
      this.handleConnectionChange
    );
  }

  getAuthCodeCallback(response) {
    const { setOnlineAuthCodeForPayment } = this.props;
    if (response.url) {
      const urlGet = response.url;
      SafariView.dismiss();
      this.setState(
        {
          isLoading: true
        },
        () => {
          const segment1 = urlGet.replace("easemobile://?", "");
          const variables = segment1.split("&");
          const codeString = variables.find(variable =>
            variable.includes("code")
          );
          const [, authCode] = codeString.split("=");
          if (!this.props.isHandlingListener) {
            setOnlineAuthCodeForPayment({
              authCode,
              callback: callback => {
                if (callback.hasError) {
                  if (callback.errorMsg) {
                    setTimeout(() => {
                      Alert.alert(`[Error] ${callback.errorMsg}`);
                    }, 200);
                  } else {
                    setTimeout(() => {
                      Alert.alert(
                        `error msg: Authorization for submission Failed (Error 001)`
                      );
                    }, 200);
                  }
                } else {
                  this.setState({
                    setOnlineDone: true
                  });
                }
              }
            });
          }
        }
      );
    }
  }

  setfunction(inputFunction) {
    if (this.state.setOnlineDone) {
      this.setState({
        function: inputFunction,
        isLoading: false
      });
    }
  }
  handleConnectionChange(isConnected) {
    this.setState({ isNetworkConnected: isConnected });
  }
  pressHandler() {
    SafariView.isAvailable()
      .then(
        SafariView.show({
          url: this.path
          // url: "eabtest://"
        })
      )
      .catch(error => {
        // Fallback WebView code for iOS 8 and earlier
        setTimeout(() => {
          Alert.alert(`[Error]${error}`);
        }, 200);
      });
  }
  render() {
    const {
      profile,
      environment,
      preApplicationEmail,
      openEmailDialog,
      textStore,
      checkTokenExpiryDate,
      checkValidDevice,
      loginByOnlineAuthForPayment,
      sendEmail,
      tabOnPress,
      tab,
      checkBoxOnPress,
      canEmail
    } = this.props;
    const { isNetworkConnected } = this.state;
    if (Config.ENV_NAME === "PROD") {
      this.path = Config.GET_AUTH_CODE_FULL_URL;
    } else if (environment === "DEV") {
      this.path = Config.DEV_GET_AUTH_CODE_FULL_URL;
    } else if (environment === "SIT1_EAB") {
      this.path = Config.SIT1_EAB_GET_AUTH_CODE_FULL_URL;
    } else if (environment === "SIT2_EAB") {
      this.path = Config.SIT2_EAB_GET_AUTH_CODE_FULL_URL;
    } else if (environment === "SIT3_EAB") {
      this.path = Config.SIT3_EAB_GET_AUTH_CODE_FULL_URL;
    } else if (environment === "SIT_AXA") {
      this.path = Config.SIT_AXA_GET_AUTH_CODE_FULL_URL;
    } else if (environment === "UAT") {
      this.path = Config.UAT_GET_AUTH_CODE_FULL_URL;
    } else if (environment === "PRE_PROD") {
      this.path = Config.PRE_PROD_GET_AUTH_CODE_FULL_URL;
    } else {
      this.path = Config.SIT3_EAB_GET_AUTH_CODE_FULL_URL;
    }

    const { options } = preApplicationEmail;
    if (this.state.function !== null) {
      this.state.function();
    }
    return (
      <ContextConsumer>
        {({ showEmailContentDialog, language }) => (
          <View>
            {this.setfunction(() => {
              if (this.state.function !== null && this.state.setOnlineDone) {
                this.setState(
                  {
                    setOnlineDone: false
                  },
                  () => {
                    showEmailContentDialog(APPLICATION);
                  }
                );
              }
            })}
            <EABButton
              style={styles.container}
              onPress={() => {
                if (this.state.isNetworkConnected) {
                  openEmailDialog(() => {
                    checkTokenExpiryDate(status => {
                      // status = false if its need to do online auth
                      if (!status) {
                        const title = EABi18n({
                          language,
                          textStore,
                          path: "needs.text.email.title"
                        });
                        const warningMessage = EABi18n({
                          language,
                          textStore,
                          path: "needs.text.onlineAuthWarning"
                        });
                        const openSafari = EABi18n({
                          language,
                          textStore,
                          path: "needs.text.openSafari"
                        });
                        const cancel = EABi18n({
                          language,
                          textStore,
                          path: "needs.text.cancel"
                        });
                        loginByOnlineAuthForPayment();
                        Alert.alert(
                          title,
                          warningMessage,
                          [
                            {
                              text: openSafari,
                              onPress: () => this.pressHandler()
                            },
                            {
                              text: cancel,
                              onPress: () => {},
                              style: "cancel"
                            }
                          ],
                          { cancelable: false }
                        );
                      } else {
                        checkValidDevice(checkValidDeviceResult => {
                          if (!checkValidDeviceResult.hasError) {
                            showEmailContentDialog(APPLICATION);
                          } else {
                            setTimeout(() => {
                              Alert.alert(
                                `[Error] Authorization Failed (Error 007)`
                              );
                            }, 200);
                          }
                        });
                      }
                    });
                  });
                } else {
                  setTimeout(() => {
                    Alert.alert(
                      "Warning",
                      `Please connect to the internet to proceed further`,
                      [
                        {
                          text: "OK",
                          onPress: () => {},
                          style: "cancel"
                        }
                      ],
                      { cancelable: false }
                    );
                  }, 200);
                }
              }}
              isDisable={canEmail}
            >
              <TranslatedText style={styles.text} path="button.email" />
            </EABButton>
            <EASEEmailContentDialog
              isNetworkConnected={isNetworkConnected}
              checkBoxOptions={options}
              checkBoxOnPress={option => {
                const newOptions = _.cloneDeep(options);
                const checkboxIndex = newOptions[option.index].findIndex(
                  checkboxOption => checkboxOption.key === option.key
                );
                if (checkboxIndex >= 0) {
                  const { isSelected } = newOptions[option.index][
                    checkboxIndex
                  ];
                  newOptions[option.index][
                    checkboxIndex
                  ].isSelected = !isSelected;
                }

                checkBoxOnPress(newOptions);
              }}
              tabOnPress={option => {
                tabOnPress(option);
              }}
              sendEmail={sendEmail}
              reportEmail={preApplicationEmail}
              tab={tab}
              disableClientTab={!profile.email}
              reportType={APPLICATION}
            />
            <EABHUD isOpen={this.state.isLoading} />
          </View>
        )}
      </ContextConsumer>
    );
  }
}

ApplicationEmailSection.propTypes = {
  openEmailDialog: PropTypes.func.isRequired,
  textStore: APP_REDUCER_TYPE_CHECK[TEXT_STORE].isRequired,
  environment: PropTypes.string.isRequired,
  profile: WEB_API_REDUCER_TYPE_CHECK[CLIENT].profile.isRequired,
  checkTokenExpiryDate: PropTypes.func.isRequired,
  checkValidDevice: PropTypes.func.isRequired,
  setOnlineAuthCodeForPayment: PropTypes.func.isRequired,
  isHandlingListener: PropTypes.bool.isRequired,
  loginByOnlineAuthForPayment: PropTypes.func.isRequired,
  sendEmail: PropTypes.func.isRequired,
  checkBoxOnPress: PropTypes.func.isRequired,
  tab: PropTypes.bool.isRequired,
  tabOnPress: PropTypes.func.isRequired,
  canEmail: PropTypes.bool.isRequired,
  preApplicationEmail:
    WEB_API_REDUCER_TYPE_CHECK[PRE_APPLICATION].preApplicationEmail.isRequired
};
