import { StyleSheet } from "react-native";
import Theme from "../../theme";

const input = {
  paddingBottom: Theme.alignmentXS - 1,
  marginTop: Theme.alignmentXL,
  marginBottom: Theme.alignmentXXS,
  borderBottomWidth: 1,
  borderBottomColor: Theme.lightGrey,
  height: Theme.bodyPrimary.lineHeight + (Theme.alignmentXS - 1)
};

const labelText = Theme.bodySecondary;
const lifeStyle01aLabelText = Theme.specialBodySecondary;
const lifeStyle01aLabelOnFocusText = Theme.BodyTertiary;
const labelFloat = {
  bottom: 20
};

export default StyleSheet.create({
  wrapper: {
    position: "relative",
    height: 80,
    width: "100%"
  },
  label: {
    position: "absolute",
    left: 0,
    flexDirection: "row"
  },
  labelText,
  labelTextFloat: {
    ...labelText,
    ...labelFloat
  },
  lifeStyle01aLabelText,
  lifeStyle01aLabelOnFocusText,
  labelNonEditable: {
    ...labelText,
    color: Theme.mediumGrey
  },
  labelNonEditableFloat: {
    ...labelText,
    color: Theme.mediumGrey,
    ...labelFloat
  },
  labelTextFocus: {
    ...labelText,
    color: Theme.brand
  },
  labelTextFocusFloat: {
    ...labelText,
    color: Theme.brand,
    ...labelFloat
  },
  specialLabelTextFocus: {
    ...lifeStyle01aLabelOnFocusText,
    color: Theme.brand
  },
  requiredHint: {
    ...labelText,
    paddingLeft: Theme.alignmentXXS,
    color: Theme.negative
  },
  requiredHintNoPlaceholder: {
    ...labelText,
    color: Theme.negative,
    alignSelf: "flex-end",
    marginTop: -22,
    marginRight: 12
  },
  specialRequiredHint: {
    ...labelText,
    right: 25,
    color: Theme.negative
  },
  input,
  inputNonEditable: input,
  /* minus 1 padding bottom for the border width plus one */
  inputFocus: {
    ...input,
    marginBottom: Theme.alignmentXXS - 1,
    borderBottomWidth: input.borderBottomWidth + 1,
    borderBottomColor: Theme.brand,
    height: Theme.bodyPrimary.lineHeight + Theme.alignmentXS
  },
  inputError: {
    ...input,
    marginBottom: Theme.alignmentXXS - 1,
    borderBottomWidth: input.borderBottomWidth + 1,
    borderBottomColor: Theme.negative,
    height: Theme.bodyPrimary.lineHeight + Theme.alignmentXS
  },
  dropdownImage: {
    width: 8,
    height: 24,
    alignSelf: "flex-end"
  },
  dropdownImageAlign: {
    width: 8,
    height: 24,
    alignSelf: "flex-end",
    marginTop: -22
  }
});
