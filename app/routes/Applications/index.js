import React from "react";
import { View } from "react-native";
import { createStackNavigator, createAppContainer } from "react-navigation";
import EABButton from "../../components/EABButton";
import TranslatedText from "../../containers/TranslatedText";
import { ContextConsumer } from "../../context";

import Theme from "../../theme";
import cleanClientData from "../../utilities/cleanClientData";
import SummaryScreen from "./containers/SummaryScreen";
import ApplicationEmailSection from "./containers/ApplicationEmailSection";
import ApplicationDeleteButton from "./containers/ApplicationDeleteButton";
import ApplicationSelectButton from "./containers/ApplicationSelectButton";
import NavigationService from "../../utilities/NavigationService";

/**
 * <Applications />
 * @reducer navigation.applications
 * @requires Applications - Applications index page
 * */

export const ApplicationNavigator = createStackNavigator(
  {
    Summary: {
      screen: SummaryScreen,
      navigationOptions: ({ navigation }) => {
        const renderHeaderLeft = () => {
          const selectModeOn = navigation.getParam("selectModeOn", false);
          const selectedIdCount = navigation.getParam("selectedIdCount");

          return (
            <ContextConsumer>
              {({ hideClientNavigator, setEappLoading }) =>
                selectModeOn ? (
                  <View style={{ flexDirection: "row" }}>
                    <ApplicationEmailSection canEmail={selectedIdCount === 0} />
                    <ApplicationDeleteButton
                      navigation={navigation}
                      setEappLoading={setEappLoading}
                    />
                  </View>
                ) : (
                  <EABButton
                    style={{
                      marginLeft: Theme.alignmentXL
                    }}
                    onPress={() => {
                      hideClientNavigator();
                      cleanClientData(navigation.dispatch);
                      NavigationService.clearNavigationRef();
                    }}
                  >
                    <TranslatedText
                      style={Theme.textButtonLabelNormalAccent}
                      path="button.close"
                    />
                  </EABButton>
                )
              }
            </ContextConsumer>
          );
        };

        return {
          headerTitle: () => {
            const selectModeOn = navigation.getParam("selectModeOn", false);
            const selectedIdCount = navigation.getParam("selectedIdCount");
            return selectModeOn ? (
              <TranslatedText
                style={Theme.title}
                path="application.summary.num_selected"
                replace={[{ value: selectedIdCount, isPath: false }]}
              />
            ) : (
              <TranslatedText
                style={Theme.title}
                path="tab.applications.title"
              />
            );
          },
          headerLeft: renderHeaderLeft(),
          headerRight: <ApplicationSelectButton navigation={navigation} />
        };
      }
    }
  },
  {
    initialRouteName: "Summary",
    defaultNavigationOptions: {
      headerTitleStyle: Theme.title,
      headerTintColor: Theme.textButtonLabelNormalAccent.color
    }
  }
);

export default createAppContainer(ApplicationNavigator);
