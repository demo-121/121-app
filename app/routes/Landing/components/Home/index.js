import moment from "moment";
import * as _ from "lodash";
import _getOr from "lodash/fp/getOr";
import PropTypes from "prop-types";
import SafariView from "react-native-safari-view";
import React, { Component } from "react";
import {
  Image,
  Text,
  View,
  Alert,
  Linking,
  NetInfo,
  NativeModules,
  NativeEventEmitter
} from "react-native";
import Config from "react-native-config";
import { LANGUAGE_TYPES, utilities } from "eab-web-api";
import avatar from "../../../../assets/images/avatar.png";
// TODO: Maybe HELP CENTRE will appear in the future
// import icInfo from "../../../../assets/images/icInfo.png";
// import icInfoActive from "../../../../assets/images/icInfoActive.png";
import icLogOut from "../../../../assets/images/icLogOut.png";
import icLogOutActive from "../../../../assets/images/icLogOutActive.png";
import icMyClients from "../../../../assets/images/icMyClients.png";
import icMyClientsActive from "../../../../assets/images/icMyClientsActive.png";
// import icMyWorkbench from "../../../../assets/images/icMyWorkbench.png";
// import icMyWorkbenchActive from "../../../../assets/images/icMyWorkbenchActive.png";
import icSettings from "../../../../assets/images/icSettings.png";
import icSettingsActive from "../../../../assets/images/icSettingsActive.png";
import icAboutEASE from "../../../../assets/images/icInfo.png";
import icAboutEASEActive from "../../../../assets/images/icInfoActive.png";
import icDataSync from "../../../../assets/images/icDataSync.png";
import icDataSyncActive from "../../../../assets/images/icHistoryActive.png";
import icResetAppPasswordUnselected from "../../../../assets/images/icResetAppPasswordUnselected.png";
import EABButton from "../../../../components/EABButton";
import EABDialog from "../../../../components/EABDialog";
import EABSegmentedControl from "../../../../components/EABSegmentedControl";
import EABSideBar from "../../../../components/EABSideBar";
import ClientFormContact from "../../../../containers/ClientFormContact";
import ClientFormProfile from "../../../../containers/ClientFormProfile";
import EABHUD from "../../../../containers/EABHUD";
import { ContextConsumer } from "../../../../context";
import Theme from "../../../../theme";
import Agent from "../../containers/Agent";
import WorkBench from "../../components/WorkBench";
import Setting from "../../containers/Setting";
import AboutEASE from "../../components/AboutEASE";
import * as HOME_TYPES from "../../constants/HOME_TYPES";
import EABi18n from "../../../../utilities/EABi18n";
import REDUCER_TYPES_CHECK from "../../../../constants/REDUCER_TYPE_CHECK";
import * as REDUCER_TYPES from "../../../../constants/REDUCER_TYPES";
import styles from "./styles";
import {
  VERSION_NUMBER,
  BUILD_NUMBER
} from "../../../../constants/BUILD_CONFIG";
import {
  dataSync as dataSyncImport,
  getAttachmentWithID
} from "../../../../utilities/DataManager";

const { TEXT_STORE } = REDUCER_TYPES;
const { isClientFormHasError } = utilities.validation;

/**
 * Home
 * @description Home page of Landing navigator. this page have multiple layout.
 * @param {object} navigation - react navigation props
 * @param {function} saveClient - redux action. trigger saga to save client
 * @param {function} getProfile - redux action. trigger saga to get profile
 * */
export default class Home extends Component {
  constructor(props) {
    super(props);
    const dataSync = new NativeEventEmitter(NativeModules.DataSyncEventManager);
    this.dataSyncListener = dataSync.addListener(
      "DataSyncEventHandler",
      data => {
        if (data === "SUCCESS") {
          this.props.setLastDataSyncTime();
          if (this.props.login.triggerRunView) {
            this.props.runView();
            this.props.disRunView();
          }
          if (this.state.logoutButtonPressed) {
            this.pressHandler2();
          }
        }
      }
    );
    const { environment } = this.props;
    if (Config.ENV_NAME === "PROD") {
      this.path = Config.GET_AUTH_CODE_FULL_URL;
      this.path2 = Config.LOGOUT_URL;
    } else if (environment === "DEV") {
      this.path = Config.DEV_GET_AUTH_CODE_FULL_URL;
      this.path2 = Config.DEV_LOGOUT_URL;
    } else if (environment === "SIT1_EAB") {
      this.path = Config.SIT1_EAB_GET_AUTH_CODE_FULL_URL;
      this.path2 = Config.SIT1_EAB_LOGOUT_URL;
    } else if (environment === "SIT2_EAB") {
      this.path = Config.SIT2_EAB_GET_AUTH_CODE_FULL_URL;
      this.path2 = Config.SIT2_EAB_LOGOUT_URL;
    } else if (environment === "SIT3_EAB") {
      this.path = Config.SIT3_EAB_GET_AUTH_CODE_FULL_URL;
      this.path2 = Config.SIT3_EAB_LOGOUT_URL;
    } else if (environment === "SIT_AXA") {
      this.path = Config.SIT_AXA_GET_AUTH_CODE_FULL_URL;
      this.path2 = Config.SIT_AXA_LOGOUT_URL;
    } else if (environment === "UAT") {
      this.path = Config.UAT_GET_AUTH_CODE_FULL_URL;
      this.path2 = Config.UAT_LOGOUT_URL;
    } else if (environment === "PRE_PROD") {
      this.path = Config.PRE_PROD_GET_AUTH_CODE_FULL_URL;
      this.path2 = Config.PRE_PROD_LOGOUT_URL;
    } else {
      this.path = Config.SIT3_EAB_GET_AUTH_CODE_FULL_URL;
      this.path2 = Config.SIT3_EAB_LOGOUT_URL;
    }

    this.pageWillBeChangeTo = "";
    this.sideBarOption = [
      {
        type: HOME_TYPES.CLIENT,
        unselectedIcon: icMyClients,
        selectedIcon: icMyClientsActive,
        title: "sideNav.myClients",
        onPress: () => {
          this.props.assignEligProductsToAgent(() => {
            this.sideBar.hideSideBar();
            if (
              this.props.navigation.state.params &&
              this.props.navigation.state.params.homeType &&
              this.props.navigation.state.params.homeType === HOME_TYPES.CLIENT
            ) {
              setTimeout(() => {
                this.props.navigation.navigate("Search");
              }, 200);
            } else {
              this.props.navigation.navigate("Search");
              this.pageWillBeChangeTo = HOME_TYPES.CLIENT;
            }
          });
        }
      },
      // TODO: Maybe work bench will appear in the future
      // {
      //   type: HOME_TYPES.WORK_BENCH,
      //   unselectedIcon: icMyWorkbench,
      //   selectedIcon: icMyWorkbenchActive,
      //   title: "sideNav.myWorkBench",
      //   onPress: () => {
      //     this.pageWillBeChangeTo = HOME_TYPES.WORK_BENCH;
      //     this.sideBar.hideSideBar();
      //   }
      // },
      {
        type: HOME_TYPES.DATA_SYNC,
        unselectedIcon: icDataSync,
        selectedIcon: icDataSyncActive,
        title: "sideNav.dataSync",
        onPress: async () => {
          await this.sideBar.hideSideBar();

          const title = "Data Sync";
          const warningMessage = "Online authentication is required.";
          this.props.checkTokenExpiryDate(status => {
            this.setState({ onlineAuthType: "dataSync" });
            // status = false if its need to do online auth
            if (!status) {
              this.props.loginByOnlineAuthForPayment();
              setTimeout(() => {
                Alert.alert(
                  title,
                  warningMessage,
                  [
                    {
                      text: "Continue",
                      onPress: () => this.pressHandler()
                    },
                    {
                      text: "Cancel",
                      onPress: () => {},
                      style: "cancel"
                    }
                  ],
                  { cancelable: false }
                );
              }, 200);
            } else {
              dataSyncImport(
                this.props.login.environment,
                this.props.agent.agentCode,
                this.props.login.profileId,
                `Bearer ${this.props.login.authToken}`,
                true,
                false,
                this.props.login.sessionCookieSyncGatewaySession,
                this.props.login.sessionCookieExpiryDate,
                this.props.login.sessionCookiePath,
                this.props.login.syncDocumentIds,
                () => {}
              );
            }
          });
        }
      },
      // TODO: Maybe HELP CENTRE will appear in the future
      // {
      //   type: HOME_TYPES.HELP_CENTRE,
      //   unselectedIcon: icInfo,
      //   selectedIcon: icInfoActive,
      //   title: "Help Centre",
      //   onPress: () => {
      //     this.pageWillBeChangeTo = HOME_TYPES.HELP_CENTRE;
      //     this.sideBar.hideSideBar();
      //   }
      // },
      {
        type: "resetAppPasswordManually",
        unselectedIcon: icResetAppPasswordUnselected,
        selectedIcon: icResetAppPasswordUnselected,
        title: "sideNav.resetAppPassword",
        onPress: () =>
          this.props.checkTokenExpiryDate(status => {
            this.setState({ onlineAuthType: "resetAppPassword" });
            const title = "Reset App Password";
            const warningMessage =
              "EAB Mobile wants you to perform Online Authentication";
            // status = false if its need to do online auth
            if (!status) {
              this.props.loginByOnlineAuthForPayment();
              setTimeout(() => {
                Alert.alert(
                  title,
                  warningMessage,
                  [
                    {
                      text: "Continue",
                      onPress: () => this.pressHandler()
                    },
                    {
                      text: "Cancel",
                      onPress: () => {},
                      style: "cancel"
                    }
                  ],
                  { cancelable: false }
                );
              }, 200);
            } else {
              this.props.resetAppPasswordManually();
            }
          })
      },
      {
        type: HOME_TYPES.SETTING,
        unselectedIcon: icSettings,
        selectedIcon: icSettingsActive,
        title: "sideNav.settings",
        onPress: () => {
          this.pageWillBeChangeTo = HOME_TYPES.SETTING;
          this.sideBar.hideSideBar();
        }
      },
      {
        type: HOME_TYPES.ABOUT_EASE,
        unselectedIcon: icAboutEASE,
        selectedIcon: icAboutEASEActive,
        title: "sideNav.aboutEASE",
        onPress: () => {
          this.pageWillBeChangeTo = HOME_TYPES.ABOUT_EASE;
          this.sideBar.hideSideBar();
        }
      },
      {
        type: "logOut",
        unselectedIcon: icLogOut,
        selectedIcon: icLogOutActive,
        title: "sideNav.logOut",
        onPress: () => {
          this.setState({ isLogouting: true }, this.sideBar.hideSideBar);
        }
      }
    ];
    this.segmentedControlArray = [
      {
        key: "A",
        title: "Profile",
        isShowingBadge: false,
        onPress: () => {
          this.setState({ showProfileTab: true });
        }
      },
      {
        key: "B",
        title: "Contact",
        isShowingBadge: false,
        onPress: () => {
          this.setState({ showProfileTab: false });
        }
      }
    ];
    this.hideSideBarCallBack = this.hideSideBarCallBack.bind(this);
    this.onlineAuthCallback = this.onlineAuthCallback.bind(this);
    this.handleConnectionChange = this.handleConnectionChange.bind(this);
    this.state = {
      countOfShieldPolicyNumber: 0,
      countOfNonShieldPolicyNumber: 0,
      lastDataSyncT: "",
      isShowingCreateProfileDialog: false,
      showProfileTab: true,
      isSavingClient: false,
      status: false,
      logoutButtonPressed: false,
      onlineAuthType: null, // dataSync, resetAppPassword
      isLogouting: false,
      agentProfileAvatar: ""
    };
  }

  // ===========================================================================
  // life circles
  // ===========================================================================
  componentDidMount() {
    const { agent } = this.props;
    if (!_.isEmpty(agent)) {
      this.getAgentProfileAvatar(`UX_${agent.profileId}`);
    }
    this.props.updateAgentProfile();
    Linking.addEventListener("url", this.onlineAuthCallback);
    this.props.navigation.setParams({
      showSideBar: () => {
        const { showPolicyNumber, checkLastDataSyncTime } = this.props;
        showPolicyNumber(callback => {
          if (!callback.errorMsg) {
            if (callback.result) {
              this.setState({
                countOfShieldPolicyNumber:
                  callback.result.countOfShieldPolicyNumber,
                countOfNonShieldPolicyNumber:
                  callback.result.countOfNonShieldPolicyNumber
              });
            }
          }
        });
        checkLastDataSyncTime(callback => {
          if (callback)
            this.setState({
              lastDataSyncT: callback
            });
        });
        this.sideBar.showSideBar();
      },
      hideSideBar: () => {
        this.sideBar.hideSideBar();
      },
      showCreateProfileDialog: () => {
        this.props.assignEligProductsToAgent(() => {
          this.props.readyToAddClient(() => {
            this.setState({ isShowingCreateProfileDialog: true });
          });
        });
      },
      getContactList: () => {
        this.props.navigation.navigate("Search", { searchText: "" });
      },
      homeType: HOME_TYPES.CLIENT
    });
    NetInfo.isConnected.addEventListener(
      "connectionChange",
      this.handleConnectionChange
    );

    NetInfo.isConnected.fetch().done(isConnected => {
      this.setState({
        status: isConnected
      });
    });
  }
  componentWillUnmount() {
    Linking.removeEventListener("url", this.onlineAuthCallback);
    NetInfo.isConnected.removeEventListener(
      "connectionChange",
      this.handleConnectionChange
    );
    this.dataSyncListener.remove();
    this.dataSyncListener = null;
  }

  // ===========================================================================
  // getters
  // ===========================================================================
  onlineAuthCallback(response) {
    const { setOnlineAuthCodeForPayment } = this.props;
    if (response.url) {
      const urlGet = response.url;
      SafariView.dismiss();
      const segment1 = urlGet.replace("easemobile://?", "");
      const variables = segment1.split("&");
      const codeString = variables.find(variable => variable.includes("code"));
      const [, authCode] = codeString.split("=");
      if (!this.props.isHandlingListener) {
        setOnlineAuthCodeForPayment({
          authCode,
          callback: callback => {
            if (callback.hasError) {
              if (callback.errorMsg) {
                setTimeout(() => {
                  Alert.alert(`[Error] ${callback.errorMsg}`);
                }, 200);
              } else {
                setTimeout(() => {
                  Alert.alert(
                    `error msg: Authorization for submission Failed (Error 001)`
                  );
                }, 200);
              }
            } else {
              const { isOnlineAuthCompleted } = this.props;
              if (isOnlineAuthCompleted) {
                if (this.state.onlineAuthType === "resetAppPassword") {
                  this.props.resetAppPasswordManually();
                } else {
                  dataSyncImport(
                    this.props.login.environment,
                    this.props.agent.agentCode,
                    this.props.login.profileId,
                    `Bearer ${this.props.login.authToken}`,
                    false,
                    false,
                    this.props.login.sessionCookieSyncGatewaySession,
                    this.props.login.sessionCookieExpiryDate,
                    this.props.login.sessionCookiePath,
                    this.props.login.syncDocumentIds,
                    () => {}
                  );
                }
              }
            }
          }
        });
      }
    }
  }
  get isDisableSaveClient() {
    const { clientForm } = this.props;
    const { hasError } = isClientFormHasError(clientForm);
    return hasError;
  }

  // ===========================================================================
  // functions
  // ===========================================================================

  /**
   * getAgentProfileAvatar
   * @description this easiest way to generate image preview
   * @return {obj}
   * */
  getAgentProfileAvatar(docId) {
    const { agentProfileAvatar } = this.state;
    getAttachmentWithID(docId, "agentProfilePic", response => {
      if (response && response.data && response.data !== agentProfileAvatar) {
        this.setState({
          agentProfileAvatar: response.data
        });
      }
    });
  }

  hideSideBarCallBack() {
    if (this.state.isLogouting) {
      this.setState({ isLogouting: false }, () => {
        setTimeout(() => {
          Alert.alert(
            null,
            EABi18n({
              path: "sideNav.confirmLogOut",
              language: LANGUAGE_TYPES.ENGLISH,
              textStore: this.props.textStore
            }),
            [
              {
                text: EABi18n({
                  language: LANGUAGE_TYPES.ENGLISH,
                  textStore: this.props.textStore,
                  path: "button.no"
                }),
                onPress: () => {}
              },
              {
                text: EABi18n({
                  language: LANGUAGE_TYPES.ENGLISH,
                  textStore: this.props.textStore,
                  path: "button.yes"
                }),
                onPress: () => {
                  this.setState({ logoutButtonPressed: true });
                  this.props.checkTokenExpiryDate(status => {
                    this.setState({ onlineAuthType: "dataSync" });
                    if (this.state.status) {
                      if (!status) {
                        this.props.logout();
                      } else {
                        dataSyncImport(
                          this.props.login.environment,
                          this.props.agent.agentCode,
                          this.props.login.profileId,
                          `Bearer ${this.props.login.authToken}`,
                          false,
                          false,
                          this.props.login.sessionCookieSyncGatewaySession,
                          this.props.login.sessionCookieExpiryDate,
                          this.props.login.sessionCookiePath,
                          this.props.login.syncDocumentIds,
                          () => {}
                        );
                      }
                    } else {
                      this.props.logout();
                    }
                  });
                }
              }
            ]
          );
        }, 200);
      });
    } else {
      switch (this.pageWillBeChangeTo) {
        case HOME_TYPES.CLIENT:
          this.props.navigation.setParams({
            homeType: HOME_TYPES.CLIENT
          });
          this.pageWillBeChangeTo = "";
          break;
        case HOME_TYPES.WORK_BENCH:
          this.props.navigation.setParams({
            resetAllFilters: () => {},
            homeType: HOME_TYPES.WORK_BENCH,
            headerTitle: "My Workbench"
          });
          this.pageWillBeChangeTo = "";
          break;
        case HOME_TYPES.HELP_CENTRE:
          this.props.navigation.setParams({
            homeType: HOME_TYPES.HELP_CENTRE
          });
          this.pageWillBeChangeTo = "";
          break;
        case HOME_TYPES.SETTING:
          this.props.navigation.setParams({
            homeType: HOME_TYPES.SETTING,
            headerTitle: "Setting"
          });
          this.pageWillBeChangeTo = "";
          break;
        case HOME_TYPES.ABOUT_EASE:
          this.props.navigation.setParams({
            homeType: HOME_TYPES.ABOUT_EASE,
            headerTitle: "About EAB"
          });
          this.pageWillBeChangeTo = "";
          break;
        default:
          break;
      }
    }
  }
  pressHandler() {
    SafariView.isAvailable()
      .then(
        SafariView.show({
          url: this.path
        })
      )
      .catch(error => {
        // Fallback WebView code for iOS 8 and earlier
        setTimeout(() => {
          Alert.alert(`[Error]${error}`);
        }, 200);
      });
  }
  pressHandler2() {
    this.sideBar.hideSideBar();
    SafariView.isAvailable()
      .then(
        SafariView.show({
          url: this.path2
        })
      )
      .catch(error => {
        // Fallback WebView code for iOS 8 and earlier
        setTimeout(() => {
          Alert.alert(`[Error]${error}`);
        }, 200);
      });
    this.props.logout();
    this.setState({ logoutButtonPressed: false });
  }

  handleConnectionChange(isConnected) {
    this.setState({ status: isConnected });
  }

  render() {
    // =========================================================================
    // variables
    // =========================================================================
    const { isSavingClient } = this.state;
    // =========================================================================
    // render functions
    // =========================================================================
    const renderPage = () => {
      if (
        this.props.navigation.state.params &&
        this.props.navigation.state.params.homeType
      ) {
        switch (this.props.navigation.state.params.homeType) {
          case HOME_TYPES.CLIENT:
            return <Agent />;
          case HOME_TYPES.WORK_BENCH:
            return <WorkBench />;
          // case HOME_TYPES.HELP_CENTRE:
          case HOME_TYPES.SETTING:
            return <Setting />;
          case HOME_TYPES.ABOUT_EASE:
            return <AboutEASE />;
          default:
            break;
        }
      }
      return null;
    };
    const renderCreateClientForm = () =>
      this.props.navigation.state.params &&
      this.props.navigation.state.params.homeType === HOME_TYPES.CLIENT ? (
        <ContextConsumer>
          {({ showClientNavigator }) => (
            <EABDialog isOpen={this.state.isShowingCreateProfileDialog}>
              <View style={Theme.dialogHeader}>
                <EABButton
                  testID="btnClientCancel"
                  onPress={() => {
                    this.setState({
                      isShowingCreateProfileDialog: false,
                      showProfileTab: true
                    });
                    this.props.cancelCreateClient();
                  }}
                >
                  <Text style={Theme.textButtonLabelNormalAccent}>Cancel</Text>
                </EABButton>
                <EABSegmentedControl
                  testID="segmentedProfileCategory"
                  segments={this.segmentedControlArray}
                  activatedKey={this.state.showProfileTab ? "A" : "B"}
                />
                <EABButton
                  testID="btnClientSave"
                  onPress={() => {
                    this.setState({ isSavingClient: true }, () => {
                      this.props.saveClient(() => {
                        // can not set state at the same time. multiple dialogs
                        // can not close at the same time.
                        this.setState({ isSavingClient: false }, () => {
                          this.setState(
                            {
                              isShowingCreateProfileDialog: false
                            },
                            showClientNavigator
                          );
                          this.props.navigateToNeeds();
                        });
                      });
                    });
                  }}
                  isDisable={this.isDisableSaveClient}
                >
                  <Text style={Theme.textButtonLabelNormalAccent}>Save</Text>
                </EABButton>
                <EABHUD isOpen={isSavingClient} />
              </View>
              {this.state.showProfileTab ? (
                <ClientFormProfile />
              ) : (
                <ClientFormContact />
              )}
            </EABDialog>
          )}
        </ContextConsumer>
      ) : null;

    const { agent, textStore } = this.props;
    const { agentProfileAvatar } = this.state;
    if (!_.isEmpty(agent)) {
      this.getAgentProfileAvatar(`UX_${agent.profileId}`);
    }
    return (
      <ContextConsumer>
        {({ language }) => (
          <EABSideBar
            ref={ref => {
              this.sideBar = ref;
            }}
            content={[
              {
                key: "a",
                component: (
                  <View style={styles.header}>
                    <View style={styles.avatarWrapper}>
                      <Image
                        style={styles.avatar}
                        source={
                          agentProfileAvatar
                            ? {
                                uri: `data:image/png;base64,${agentProfileAvatar}`
                              }
                            : avatar
                        }
                      />
                    </View>
                    <Text style={styles.userName}>{agent.name}</Text>
                  </View>
                )
              },
              {
                key: "b",
                component: (
                  <View style={styles.optionGroup}>
                    {this.sideBarOption.map(option => (
                      <EABButton
                        key={option.type}
                        style={styles.optionButton}
                        onPress={option.onPress}
                      >
                        <Image
                          style={styles.optionIcon}
                          source={
                            option.type ===
                            _getOr(
                              "",
                              "homeType",
                              this.props.navigation.state.params
                            )
                              ? option.selectedIcon
                              : option.unselectedIcon
                          }
                        />
                        <Text
                          style={
                            option.type ===
                            _getOr(
                              "",
                              "homeType",
                              this.props.navigation.state.params
                            )
                              ? styles.selectedOptionText
                              : styles.unselectedOptionText
                          }
                        >
                          {EABi18n({
                            language,
                            textStore,
                            path: option.title
                          })}
                        </Text>
                      </EABButton>
                    ))}
                  </View>
                )
              },
              {
                key: "c",
                component:
                  process.env.NODE_ENV === "production" ? (
                    <View style={styles.informationWrapper}>
                      <Text style={styles.version}>
                        v{VERSION_NUMBER}-b{BUILD_NUMBER}
                      </Text>
                      <Text style={styles.lastSyncTime}>
                        {`${EABi18n({
                          language,
                          textStore,
                          path: "sideNav.lastDataSync"
                        })}: ${moment(Number(this.state.lastDataSyncT)).format(
                          "YYYY-MM-DD HH:mm:ss"
                        )}`}
                      </Text>
                      <Text style={styles.version}>
                        {EABi18n({
                          language,
                          textStore,
                          path: "sideNav.environment"
                        })}: {this.props.login.environment}
                      </Text>
                      <Text style={styles.version}>
                        {EABi18n({
                          language,
                          textStore,
                          path: "sideNav.shield"
                        })}: {this.state.countOfShieldPolicyNumber}
                      </Text>
                      <Text style={styles.version}>
                        {EABi18n({
                          language,
                          textStore,
                          path: "sideNav.nonShield"
                        })}: {this.state.countOfNonShieldPolicyNumber}
                      </Text>
                    </View>
                  ) : (
                    <ContextConsumer>
                      {({ showPlayGround }) => (
                        <EABButton
                          containerStyle={{ alignItems: "flex-start" }}
                          onPress={showPlayGround}
                        >
                          <View style={styles.informationWrapper}>
                            <Text style={styles.version}>
                              v{VERSION_NUMBER}-b{BUILD_NUMBER}
                            </Text>
                            <Text style={styles.lastSyncTime}>
                              {EABi18n({
                                language,
                                textStore,
                                path: "sideNav.lastDataSync"
                              })}:{" "}
                              {moment(Number(this.state.lastDataSyncT)).format(
                                "YYYY-MM-DD HH:mm"
                              )}
                            </Text>
                            <Text style={styles.version}>
                              {EABi18n({
                                language,
                                textStore,
                                path: "sideNav.environment"
                              })}: {this.props.login.environment}
                            </Text>
                            <Text style={styles.version}>
                              {EABi18n({
                                language,
                                textStore,
                                path: "sideNav.shield"
                              })}: {this.state.countOfShieldPolicyNumber}
                            </Text>
                            <Text style={styles.version}>
                              {EABi18n({
                                language,
                                textStore,
                                path: "sideNav.nonShield"
                              })}: {this.state.countOfNonShieldPolicyNumber}
                            </Text>
                          </View>
                        </EABButton>
                      )}
                    </ContextConsumer>
                  )
              }
            ]}
            hideCallBack={this.hideSideBarCallBack}
          >
            {renderPage()}
            {renderCreateClientForm()}
          </EABSideBar>
        )}
      </ContextConsumer>
    );
  }
}

Home.propTypes = {
  disRunView: PropTypes.func.isRequired,
  runView: PropTypes.func.isRequired,
  updateAgentProfile: PropTypes.func.isRequired,
  setLastDataSyncTime: PropTypes.func.isRequired,
  checkLastDataSyncTime: PropTypes.func.isRequired,
  showPolicyNumber: PropTypes.func.isRequired,
  setOnlineAuthCodeForPayment: PropTypes.func.isRequired,
  loginByOnlineAuthForPayment: PropTypes.func.isRequired,
  checkTokenExpiryDate: PropTypes.func.isRequired,
  isOnlineAuthCompleted: PropTypes.bool.isRequired,
  login: PropTypes.oneOfType([PropTypes.object]).isRequired,
  agent: PropTypes.oneOfType([PropTypes.object]).isRequired,
  clientForm: PropTypes.oneOfType([PropTypes.object]).isRequired,
  readyToAddClient: PropTypes.func.isRequired,
  saveClient: PropTypes.func.isRequired,
  logout: PropTypes.func.isRequired,
  environment: PropTypes.string.isRequired,
  resetAppPasswordManually: PropTypes.func.isRequired,
  cancelCreateClient: PropTypes.func.isRequired,
  navigateToNeeds: PropTypes.func.isRequired,
  isHandlingListener: PropTypes.bool.isRequired,
  textStore: REDUCER_TYPES_CHECK[TEXT_STORE].isRequired,
  assignEligProductsToAgent: PropTypes.func.isRequired
};
