//
//  NSObject+Logger.h
//  EASE
//
//  Created by Alex Tang on 30/7/2018.
//  Copyright © 2018 Facebook. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Logger.h"

@implementation Logger

RCT_EXPORT_MODULE()

RCT_EXPORT_METHOD(log:(NSString *) format) {
  NSLog(@"%@", format);
}

RCT_EXPORT_METHOD(addDebugLog:(NSString *)message) {
  NSLog(@"addLogs... %@",message);
}

@end

