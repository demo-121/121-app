import Config from "react-native-config";

export default (environment, url) => {
  if (Config.ENV_NAME === "PROD") {
    return {
      url: Config[url],
      baseURL: Config.WEB_SERVICE_BASIC_URL
    };
  }

  switch (environment) {
    case "DEV":
      return {
        url: Config[url],
        baseURL: Config.DEV_WEB_SERVICE_BASIC_URL
      };
    case "SIT1_EAB":
      return {
        url: Config[`SIT1_EAB_${url}`],
        baseURL: Config.SIT1_EAB_WEB_SERVICE_BASIC_URL
      };
    case "SIT2_EAB":
      return {
        url: Config[`SIT2_EAB_${url}`],
        baseURL: Config.SIT2_EAB_WEB_SERVICE_BASIC_URL
      };
    case "SIT3_EAB":
      return {
        url: Config[`SIT3_EAB_${url}`],
        baseURL: Config.SIT3_EAB_WEB_SERVICE_BASIC_URL
      };
    case "SIT_AXA":
      return {
        url: Config[`SIT_AXA_${url}`],
        baseURL: Config.SIT_AXA_WEB_SERVICE_BASIC_URL
      };
    case "UAT":
      return {
        url: Config[`UAT_${url}`],
        baseURL: Config.UAT_WEB_SERVICE_BASIC_URL
      };
    case "PRE_PROD":
      return {
        url: Config[`PRE_PROD_${url}`],
        baseURL: Config.PRE_PROD_WEB_SERVICE_BASIC_URL
      };
    default:
      return {
        url: Config[`SIT3_EAB_${url}`],
        baseURL: Config.SIT3_EAB_WEB_SERVICE_BASIC_URL
      };
  }
};
