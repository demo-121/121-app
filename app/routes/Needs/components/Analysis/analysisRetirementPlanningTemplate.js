import PropTypes from "prop-types";
import React, { Component } from "react";
import { Text, View } from "react-native";
import EABTextField from "../../../../components/EABTextField";
import EABTextBox from "../../../../components/EABTextBox";
import EABStepper from "../../../../components/EABStepper";
import { ContextConsumer } from "../../../../context";
import styles from "./styles";
import { getCurrency } from "./analysisUtil";

// TODO: THIS FILE WILL BE REMOVED AND BE REPLACED BY A NEW FILE IN TEMPLATES FOLDER
/**
 * AnalysiRetirementPlanningTemplate
 * @description Analysis - Retirement Planning - Template
 * */
export default class AnalysiRetirementPlanningTemplate extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  checkDependant() {
    const { selectedProfile, data } = this.props;
    if (selectedProfile !== "owner" && selectedProfile !== "spouse") {
      if (data.dependants && data.dependants.length) {
        return data.dependants.find(val => val.cid === selectedProfile);
      }
    } else {
      return data[selectedProfile];
    }
    return "";
  }

  initValidate() {
    const { selectedProfile, data } = this.props;
    let targetProfile = {};
    if (selectedProfile !== "spouse" && selectedProfile !== "owner") {
      targetProfile = data.dependants.find(val => val.cid === selectedProfile);
    } else {
      targetProfile = data[selectedProfile];
    }
    if (!targetProfile.validate) {
      return {};
    }
    return Object.keys(targetProfile.validate).reduce((preValue, currIndex) => {
      preValue[currIndex] = targetProfile.validate[currIndex];
      return preValue;
    }, {});
  }

  render() {
    const {
      dispatch,
      selectedProfile,
      rule,
      profile,
      assetsSelector
    } = this.props;
    const {
      retireAgeOnChange,
      inReqRetirementOnChange,
      unMatchPmtReasonOnChange,
      othRegIncomeOnChange
    } = dispatch;
    const validate = this.initValidate();
    const data = this.checkDependant();

    return (
      <ContextConsumer>
        {({ language }) => (
          <View key={123} style={styles.analysisProductContent}>
            <Text style={styles.analysisProductPointTitle}>
              1. Retirement age
            </Text>
            <EABStepper
              value={data ? data.retireAge : null}
              step={1}
              min={30}
              max={99}
              onChange={value =>
                retireAgeOnChange({
                  selectedProfile,
                  profile,
                  value,
                  analysisData: data
                })
              }
            />
            <View style={styles.analysisSectionBorderLine} />

            {/* TODO: Add minimum and maximum, 
            numeric restrictions on this field */}
            <Text style={styles.analysisProductPointTitle}>
              2. Income Required at retirement (Monthly)(PV){" "}
              <Text style={styles.requiredHint}>*</Text>
            </Text>
            <EABTextField
              style={styles.analysisTextInputShort}
              prefix="$"
              value={data ? data.inReqRetirement : null}
              isError={
                validate.inReqRetirement
                  ? validate.inReqRetirement.hasError
                  : false
              }
              hintText={
                validate.inReqRetirement
                  ? validate.inReqRetirement.message[language]
                  : ""
              }
              onChange={value =>
                inReqRetirementOnChange(
                  Object.assign(
                    {},
                    { selectedProfile, value, analysisData: data },
                    rule.inReqRetirement
                      ? { rule: rule.inReqRetirement }
                      : { rule: null }
                  )
                )
              }
            />
            <Text style={styles.analysisProductPointText}>
              Annual Income Required at Retirement
            </Text>
            <Text style={styles.analysisProductPointContent}>
              {getCurrency(data.annualInReqRetirement, "$", 0)}
            </Text>
            {profile.allowance && profile.allowance < data.inReqRetirement ? (
              <View>
                <Text style={styles.analysisProductPointText}>
                  Income required at retirement is more than your current
                  income, please provide reason(s).{" "}
                  <Text style={styles.requiredHint}>*</Text>
                </Text>
                <EABTextBox
                  style={styles.analysisProductTextBox}
                  numberOfLines={5}
                  isError={
                    validate.unMatchPmtReason
                      ? validate.unMatchPmtReason.hasError
                      : false
                  }
                  hintText={
                    validate.unMatchPmtReason
                      ? validate.unMatchPmtReason.message[language]
                      : ""
                  }
                  value={data ? data.unMatchPmtReason : ""}
                  onChange={value => {
                    unMatchPmtReasonOnChange(
                      Object.assign(
                        {},
                        { selectedProfile, value },
                        rule.unMatchPmtReason
                          ? { rule: rule.unMatchPmtReason }
                          : { rule: null }
                      )
                    );
                  }}
                />
              </View>
            ) : null}
            <View style={styles.analysisSectionBorderLine} />

            {/* TODO: Add Tooltip */}
            <Text style={styles.analysisProductPointTitle}>
              3. Years to retirement (n)
            </Text>
            <Text style={styles.analysisProductPointContent}>
              {data.timeHorizon} years
            </Text>
            <View style={styles.analysisSectionBorderLine} />

            <Text style={styles.analysisProductPointTitle}>
              4. Average inflation rate (i %)
            </Text>
            <Text style={styles.analysisProductPointContent}>
              {data.avgInflatRate}%
            </Text>
            <View style={styles.analysisSectionBorderLine} />

            <Text style={styles.analysisProductPointTitle}>
              5. Future value (COMP FV)
            </Text>
            <Text style={styles.analysisProductPointContent}>
              {getCurrency(data.compFv, "$", 2)}
            </Text>
            <View style={styles.analysisSectionBorderLine} />

            <Text style={styles.analysisProductPointTitle}>
              6. Other Regular Income
            </Text>
            <EABTextField
              style={styles.analysisTextInputShort}
              prefix="$"
              value={data ? data.othRegIncome : 0}
              onChange={value =>
                othRegIncomeOnChange(
                  Object.assign(
                    {},
                    { selectedProfile, value, analysisData: data }
                  )
                )
              }
            />
            <View style={styles.analysisSectionBorderLine} />

            <Text style={styles.analysisProductPointTitle}>
              7. First year income required (PMT)
            </Text>
            <Text style={styles.analysisProductPointContent}>
              {getCurrency(data.firstYrPMT, "$", 2)}
            </Text>
            <View style={styles.analysisSectionBorderLine} />

            <Text style={styles.analysisProductPointTitle}>
              8. Inflation adjusted rate of return (i %)
            </Text>
            <Text style={styles.analysisProductPointContent}>
              {data.iarRate2}%
            </Text>
            <View style={styles.analysisSectionBorderLine} />

            <Text style={styles.analysisProductPointTitle}>
              9. Duration of retirement (n)
            </Text>
            <Text style={styles.analysisProductPointContent}>
              {data.retireDuration} years
            </Text>
            <View style={styles.analysisSectionBorderLine} />

            <Text style={styles.analysisProductPointTitle}>
              10. Total amount required (COMP PV)
            </Text>
            <Text style={styles.analysisProductPointContent}>
              {getCurrency(data.compPv, "$", 0)}
            </Text>
            <View style={styles.analysisSectionBorderLine} />

            {assetsSelector !== null ? (
              <View>
                <Text style={styles.analysisProductPointTitle}>
                  11. Value of Assets/Regular Savings/Investments to be used
                  (COMP FV) (Less):
                </Text>
                {assetsSelector}
                <View style={styles.analysisSectionBorderLine} />
              </View>
            ) : null}

            <Text style={styles.analysisProductPointTitle}>
              12. Estimated maturity value of insurance plan(s) at retirement
              (Less)
            </Text>
            <Text style={styles.analysisProductPointContent}>
              {getCurrency(data.maturityValue, "$", 0)}
            </Text>
            <View style={styles.analysisSectionBorderLine} />
          </View>
        )}
      </ContextConsumer>
    );
  }
}

AnalysiRetirementPlanningTemplate.propTypes = {
  selectedProfile: PropTypes.string.isRequired,
  data: PropTypes.oneOfType([PropTypes.object]).isRequired,
  rule: PropTypes.oneOfType([PropTypes.object]).isRequired,
  assetsSelector: PropTypes.oneOfType([PropTypes.object]).isRequired,
  profile: PropTypes.oneOfType([PropTypes.object]).isRequired
};
