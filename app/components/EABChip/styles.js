import { StyleSheet } from "react-native";
import Theme from "../../theme";

const chipContainer = {
  flexDirection: "row",
  alignItems: "center",
  justifyContent: "center",
  paddingHorizontal: Theme.alignmentL,
  borderRadius: Theme.radiusL,
  height: 32
};

export default StyleSheet.create({
  chipContainer,
  chipContainerIcon: {
    ...chipContainer,
    paddingRight: Theme.alignmentXXS
  },
  textIcon: {
    ...Theme.chipLabelBrand,
    marginRight: Theme.alignmentXXS
  },
  image: {
    width: 24,
    height: 24
  }
});
