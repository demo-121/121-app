import React, { PureComponent } from "react";
import { View } from "react-native";
import PropTypes from "prop-types";
import * as _ from "lodash";

import { FILTER_TYPES, REDUCER_TYPES, REDUCER_TYPE_CHECK } from "eab-web-api";

import APP_REDUCER_TYPE_CHECK from "../../../../constants/REDUCER_TYPE_CHECK";
import { TEXT_STORE } from "../../../../constants/REDUCER_TYPES";
import { ContextConsumer } from "../../../../context";
import EABi18n from "../../../../utilities/EABi18n";
import EABChip from "../../../../components/EABChip";
import styles from "./styles";

const { PRE_APPLICATION } = REDUCER_TYPES;

/**
 * ApplicationsListFilter
 * @description filter for ApplicationsList in Application Summary page.
 * @param {object} textStore - reducer textStore state
 * @param {array} applicationsList - array of application / quotation item object
 * @param {string} selectedAppListFilter - indicate which filter is selected
 * @param {func} updateUiAppListFilter - update the selected filter to reducer
 * @param {func} filterApplicationItems - condition to filter application list by selected filter
 * */

class ApplicationsListFilter extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {};

    this.countApplicationItems = this.countApplicationItems.bind(this);
  }

  countApplicationItems(selectedFilter) {
    const { applicationsList, filterApplicationItems } = this.props;
    let count = 0;

    _.forEach(applicationsList, item => {
      if (filterApplicationItems({ item, selectedFilter })) {
        count += 1;
      }
    });
    return count;
  }

  render() {
    const {
      textStore,
      selectedAppListFilter,
      updateUiAppListFilter
    } = this.props;
    const filters = [
      {
        key: FILTER_TYPES.APPLICATION_LIST.ALL,
        testID: "All",
        path: "application.summary.filter_all"
      },
      {
        key: FILTER_TYPES.APPLICATION_LIST.PROPOSED,
        testID: "Proposed",
        path: "application.summary.filter_proposed"
      },
      {
        key: FILTER_TYPES.APPLICATION_LIST.APPLYING,
        testID: "Applying",
        path: "application.summary.filter_applying"
      },
      {
        key: FILTER_TYPES.APPLICATION_LIST.SUBMITTED,
        testID: "Submitted",
        path: "application.summary.filter_submitted"
      },
      {
        key: FILTER_TYPES.APPLICATION_LIST.INVALIDATED_SIGNED,
        testID: "InvalidatedSigned",
        path: "application.summary.filter_invalidated_signed"
      },
      {
        key: FILTER_TYPES.APPLICATION_LIST.INVALIDATED,
        testID: "Invalidated",
        path: "application.summary.filter_invalidated"
      }
    ];

    return (
      <ContextConsumer>
        {({ language }) => (
          <View style={styles.container}>
            <View style={styles.filterWrapper}>
              {filters.map(titleObj => (
                <View
                  key={`applist_filter-${titleObj.key}`}
                  style={styles.filterView}
                >
                  <EABChip
                    testID={`PreAppilcation__Filter__chp${titleObj.testID}`}
                    title={`${_.toUpper(
                      EABi18n({
                        path: titleObj.path,
                        language,
                        textStore
                      })
                    )} (${this.countApplicationItems(titleObj.key)})`}
                    isSelected={selectedAppListFilter === titleObj.key}
                    onPress={() => {
                      updateUiAppListFilter(titleObj.key);
                    }}
                  />
                </View>
              ))}
            </View>
          </View>
        )}
      </ContextConsumer>
    );
  }
}

ApplicationsListFilter.propTypes = {
  textStore: APP_REDUCER_TYPE_CHECK[TEXT_STORE].isRequired,
  applicationsList: PropTypes.arrayOf(
    REDUCER_TYPE_CHECK[PRE_APPLICATION].ApplicationItem.value.isRequired
  ).isRequired,
  selectedAppListFilter: PropTypes.string.isRequired,
  updateUiAppListFilter: PropTypes.func.isRequired,
  filterApplicationItems: PropTypes.func.isRequired
};

export default ApplicationsListFilter;
