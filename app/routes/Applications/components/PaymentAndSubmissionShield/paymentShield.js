import React, { PureComponent } from "react";
import {
  View,
  Text,
  Linking,
  Image,
  NativeEventEmitter,
  NativeModules
} from "react-native";
import PropTypes from "prop-types";
import * as _ from "lodash";
import moment from "moment";
import {
  REDUCER_TYPES,
  REDUCER_TYPE_CHECK,
  utilities,
  EAPP
} from "eab-web-api";
import EABTable from "../../../../components/EABTable";
import EABTableSection from "../../../../components/EABTableSection";
import EABTableSectionHeader from "../../../../components/EABTableSectionHeader";
import EABTableHeader from "../../../../components/EABTableHeader";
import APP_REDUCER_TYPE_CHECK from "../../../../constants/REDUCER_TYPE_CHECK";
import { TEXT_STORE } from "../../../../constants/REDUCER_TYPES";
import Theme from "../../../../theme";
import EABi18n from "../../../../utilities/EABi18n";
import icNested from "../../../../assets/images/icNested.png";

import TranslatedText from "../../../../containers/TranslatedText";
import EABCheckBox from "../../../../components/EABCheckBox";
import { RECTANGLE_BUTTON_1 } from "../../../../components/EABButton/constants";
import EABButton from "../../../../components/EABButton";
import SelectorField from "../../../../components/SelectorField";
import getEABTextByLangType from "../../../../utilities/getEABTextByLangType";
import {
  DATE_PICKER,
  FLAT_LIST
} from "../../../../components/SelectorField/constants";
import { ContextConsumer } from "../../../../context";
import EABTextField from "../../../../components/EABTextField";
import EABTextBox from "../../../../components/EABTextBox";
import PaymentButton from "../../containers/PaymentButton";
import PaymentStatus from "../../containers/PaymentStatus";
import styles from "./styles";

const {
  INITPAYMETHOD,
  TELETRANSFTER,
  TTREMITTINGBANK,
  TTDOR,
  TTREMARKS,
  SUBSEQPAYMETHOD
} = EAPP.fieldId[REDUCER_TYPES.PAYMENT];

const { AXASAM, DBSCRCARDIPP } = EAPP.fieldValue[REDUCER_TYPES.PAYMENT];

const {
  BASIC_CARE_A,
  GENERAL_CARE_A,
  HOME_CARE,
  BASIC_CARE_B,
  GENERAL_CARE_B,
  BASIC_CARE_STANDARD
} = EAPP.riderName;

const { getCurrency, getCurrencySign } = utilities.common;
/**
 * PaymentShield
 * @description PaymentShield in Application Summary page.
 * @param {array} PaymentShield - array of product item object
 * */

class PaymentShield extends PureComponent {
  constructor(props) {
    super(props);

    this.dataSyncEvent = new NativeEventEmitter(
      NativeModules.DataSyncEventManager
    );
    this.axsPath = "http://www.axs.com.sg/";
    this.samPath = "https://www.mysam.sg/index.jsp";
    this.dataSyncCallbackHandler = this.dataSyncCallbackHandler.bind(this);

    this.state = {
      onDataSync: false,
      dataSyncCallback: () => {}
    };
  }

  componentDidMount() {
    const { dataSyncCallbackHandler } = this;

    this.dataSyncEventListener = this.dataSyncEvent.addListener(
      "DataSyncEventHandler",
      dataSyncCallbackHandler
    );
  }
  componentWillUnmount() {
    this.dataSyncEventListener.remove();
    this.dataSyncEventListener = null;
  }

  /**
   * getPremiumDetailsTableFooter
   * @description return the table of plan details rider footer
   * @param {string} targetProfile the target profile name
   * @return {object} table rider footer
   * */
  getPremiumDetailsTableFooter(language) {
    const { payment, optionsMap } = this.props;
    const { totCashPortion, totMedisave } = payment;
    return (
      <EABTableSection key={totCashPortion + totMedisave}>
        <View style={styles.tableFooterValueView} />
        <View style={styles.tableFooterValueView}>
          <TranslatedText
            style={Theme.subheadSecondary}
            language={language}
            path="paymentAndSubmission.text.total"
          />
        </View>
        <View style={styles.tableFooterValueView}>
          <Text style={Theme.subheadPrimary}>
            {` ${getCurrency({
              value: totMedisave,
              sign: getCurrencySign({
                compCode: "01",
                ccy: payment.policyCcy,
                optionsMap
              }),
              decimals: 2
            })}`}
          </Text>
        </View>
        <View style={styles.tableFooterValueView}>
          <Text style={Theme.subheadPrimary}>
            {` ${getCurrency({
              value: totCashPortion,
              sign: getCurrencySign({
                compCode: "01",
                ccy: payment.policyCcy,
                optionsMap
              }),
              decimals: 2
            })}`}
          </Text>
        </View>
        <View style={styles.tableFooterValueView} />
      </EABTableSection>
    );
  }

  /**
   * getPremiumDetailsTableContent
   * @description return the table of plan details content
   * @return {object} table content
   * */
  getPremiumDetailsTableContent(language) {
    const {
      payment,
      optionsMap,
      updatePayment,
      isDisableEappButton
    } = this.props;
    const shieldTableContent = [];

    const isRiderPlans = covName => {
      switch (covName) {
        case BASIC_CARE_A:
        case GENERAL_CARE_A:
        case HOME_CARE:
        case BASIC_CARE_B:
        case GENERAL_CARE_B:
        case BASIC_CARE_STANDARD:
          return true;
        default:
          return false;
      }
    };

    const groupArr = this.groupByCid();
    groupArr.forEach((value, index) => {
      shieldTableContent.push(
        <EABTableSectionHeader
          key={`${value[0].policyNumber + index}`}
          tableConfig={[{ key: "name", style: styles.tableSectionHeader }]}
        >
          <View>
            <Text style={Theme.captionBrand}>
              {value[0].laName.toUpperCase()}
            </Text>
          </View>
        </EABTableSectionHeader>
      );
      value.forEach(subValue => {
        const isRiderPlan = isRiderPlans(
          getEABTextByLangType({
            data: subValue.covName,
            language
          })
        );
        shieldTableContent.push(
          <EABTableSection key={subValue.policyNumber}>
            <View>
              {isRiderPlan ? (
                <Image style={styles.iconImage} source={icNested} />
              ) : null}
              <Text style={styles.tableSectionTitle}>
                {getEABTextByLangType({
                  data: subValue.covName,
                  language
                })}
              </Text>
            </View>
            <View>
              <Text style={Theme.subheadPrimary}>{subValue.policyNumber}</Text>
            </View>
            <View>
              <Text style={styles.tableSectionTitle}>
                {!subValue.medisave || subValue.medisave === 0
                  ? "-"
                  : getCurrency({
                      value: subValue.medisave,
                      sign: getCurrencySign({
                        compCode: "01",
                        ccy: payment.policyCcy,
                        optionsMap
                      }),
                      decimals: 2
                    })}
              </Text>
            </View>
            <View>
              <Text style={styles.tableSectionTitle}>
                {!subValue.cashPortion || subValue.cashPortion === 0
                  ? "-"
                  : getCurrency({
                      value: subValue.cashPortion,
                      sign: getCurrencySign({
                        compCode: "01",
                        ccy: payment.policyCcy,
                        optionsMap
                      }),
                      decimals: 2
                    })}
              </Text>
            </View>
            {subValue.cashPortion > 0 ? (
              <View style={styles.giroCheckboxView}>
                <EABCheckBox
                  labelStyle={{ fontWeight: "800", fontSize: 22 }}
                  disable={subValue.payFrequency === "M" || isDisableEappButton}
                  style={styles.giroCheckbox}
                  options={[
                    {
                      key: `GIRO_${index}`,
                      text: "GIRO",
                      isSelected: subValue[SUBSEQPAYMETHOD] === "Y" || false
                    }
                  ]}
                  onPress={newValue => {
                    updatePayment({
                      path: `premiumDetails[${payment.premiumDetails.findIndex(
                        premValue =>
                          premValue.policyNumber === subValue.policyNumber
                      )}].${SUBSEQPAYMETHOD}`,
                      newValue: newValue.isSelected ? "N" : "Y",
                      isShield: true
                    });
                  }}
                />
              </View>
            ) : (
              <View>
                <Text style={styles.tableSectionTitle}>-</Text>
              </View>
            )}
          </EABTableSection>
        );
      });
    });
    return shieldTableContent;
  }

  getPremiumDetails(language) {
    const { payment } = this.props;
    if (_.isEmpty(payment)) {
      return null;
    }
    return (
      <View style={styles.sectionView}>
        <View style={styles.sectionHeaderView}>
          <TranslatedText
            style={Theme.captionBrand}
            language={language}
            path="paymentAndSubmission.section.payment.premiumDetails"
          />
        </View>
        <EABTable
          tableConfig={[
            {
              key: "plan_name",
              style: [styles.tableSectionColumn1, styles.tableSection]
            },
            {
              key: "proposal_number",
              style: [styles.tableSectionColumn2, styles.tableSection]
            },
            {
              key: "cpf_portion",
              style: [styles.tableSectionColumn3, styles.tableSection]
            },
            {
              key: "cash_portion",
              style: [styles.tableSectionColumn4, styles.tableSection]
            },
            {
              key: "subsequent_premium",
              style: [styles.tableSectionColumn4, styles.tableSection]
            }
          ]}
        >
          <EABTableHeader key="fundSelectionTableHeader">
            <View>
              <TranslatedText
                style={Theme.tableColumnLabel}
                language={language}
                path="paymentAndSubmission.item_header.planName"
              />
            </View>
            <View>
              <TranslatedText
                style={Theme.tableColumnLabel}
                language={language}
                path="paymentAndSubmission.item_header.proposalNumber"
              />
            </View>
            <View>
              <TranslatedText
                style={Theme.tableColumnLabel}
                language={language}
                path="paymentAndSubmission.item_header.cpfPortion"
              />
            </View>
            <View>
              <TranslatedText
                style={Theme.tableColumnLabel}
                language={language}
                path="paymentAndSubmission.item_header.cashPortion"
              />
            </View>
            <View>
              <TranslatedText
                style={Theme.tableColumnLabel}
                language={language}
                path="paymentAndSubmission.item_header.subsequentPremium"
              />
            </View>
          </EABTableHeader>
          {this.getPremiumDetailsTableContent(language)}
          {this.getPremiumDetailsTableFooter(language)}
        </EABTable>
        <TranslatedText
          style={styles.bodyNegative}
          language={language}
          path="paymentAndSubmission.text.premiumDetailsText1"
        />
        <TranslatedText
          style={styles.bodyNegative}
          language={language}
          path="paymentAndSubmission.text.premiumDetailsText2"
        />
      </View>
    );
  }

  getPaymentSelector(language) {
    const {
      application,
      updatePayment,
      template,
      payment,
      textStore,
      onlinePaymentStatus,
      error,
      isDisableEappButton
    } = this.props;
    let targetOptions = "";
    let editable = true;
    const { trxStatus, trxStatusRemark, trxMethod } = onlinePaymentStatus;
    const initPayMethod = payment[INITPAYMETHOD];
    if (_.isEmpty(template)) {
      return null;
    }
    if (
      !trxStatus ||
      trxStatus === "" ||
      trxStatus === "Y" ||
      trxStatus === "N" ||
      trxStatus === "C"
    ) {
      targetOptions = template.items[3].items[3].items[0].items[0].options;
      targetOptions = targetOptions.filter(value => value && value.value);
    } else if (trxStatus === "I" || trxStatus === "O") {
      targetOptions = template.items[3].items[3].items[0].items[1].options;
      targetOptions = targetOptions.filter(
        value =>
          value &&
          value.value &&
          ((trxMethod === "crCard" &&
            (value.value === "crCard" || value.value === "axasam")) ||
            (trxMethod === "eNets" &&
              (value.value === "eNets" || value.value === "axasam")) ||
            (trxMethod === "dbsCrCardIpp" &&
              (value.value === "dbsCrCardIpp" || value.value === "axasam")))
      );
      if (trxStatusRemark === "I") {
        editable = false;
      }
    }
    if (
      trxStatus === "Y" ||
      application.isSubmittedStatus ||
      isDisableEappButton
    ) {
      editable = false;
    }
    return (
      <SelectorField
        style={styles.paymentSelector}
        placeholder={EABi18n({
          language,
          textStore,
          path: "paymentAndSubmission.section.payment.initialPaymentPlaceHolder"
        })}
        isRequired
        value={initPayMethod}
        editable={editable}
        isError={!initPayMethod}
        hintText={_.get(error, `initPayMethod.message[${language}]`, "")}
        selectorType={FLAT_LIST}
        flatListOptions={{
          renderItemOnPress: (itemKey, item) => {
            if (item) {
              updatePayment({
                path: INITPAYMETHOD,
                newValue: item.value,
                isShield: true,
                validateObj: {
                  mandatory: true,
                  field: "initPayMethod",
                  value: item.value
                }
              });
            }
          },
          data: targetOptions.map((value, index) => ({
            label: value.title,
            value: value.value,
            key: index
          }))
        }}
      />
    );
  }

  getInitialPaymentMethod(language) {
    const {
      payment,
      textStore,
      error,
      updatePayment,
      isDisableEappButton
    } = this.props;
    if (_.isEmpty(payment)) {
      return null;
    }

    const initPayMethod = payment[INITPAYMETHOD] || "";
    const ttRemittingBank = payment[TTREMITTINGBANK] || "";
    const ttDOR = payment[TTDOR] || "";
    const ttRemarks = payment[TTREMARKS] || "";

    return (
      <View style={styles.sectionView}>
        <View style={styles.sectionHeaderView}>
          <TranslatedText
            style={Theme.captionBrand}
            language={language}
            path="paymentAndSubmission.section.payment.initialPaymentMethod"
          />
        </View>
        <TranslatedText
          style={styles.initialPaymentMethodSubtitle}
          language={language}
          path="paymentAndSubmission.initialPayment.cpfPortion"
        />
        <EABTable
          tableConfig={[
            {
              key: "cpf_account_holder_name",
              style: [
                styles.initialPaymentMethodTableColumn1,
                styles.tableSection
              ]
            },
            {
              key: "cpf_account_no",
              style: [
                styles.initialPaymentMethodTableColumn2,
                styles.tableSection
              ]
            }
          ]}
        >
          <EABTableHeader key="fundSelectionTableHeader">
            <View>
              <TranslatedText
                style={Theme.tableColumnLabel}
                language={language}
                path="paymentAndSubmission.initialPayment.cpfAccountHolderName"
              />
            </View>
            <View>
              <TranslatedText
                style={Theme.tableColumnLabel}
                language={language}
                path="paymentAndSubmission.initialPayment.cpfAccountNo"
              />
            </View>
          </EABTableHeader>
          <EABTableSection
            key={payment.cpfMedisaveAccDetails[0].cpfAccHolderName}
          >
            <View>
              <Text style={Theme.subheadPrimary}>
                {`${
                  payment.cpfMedisaveAccDetails[0].cpfAccHolderName
                } ${EABi18n({
                  language,
                  textStore,
                  path: "paymentAndSubmission.initialPayment.proposer"
                })}`}
              </Text>
            </View>
            <View>
              <Text style={Theme.subheadPrimary}>
                {payment.cpfMedisaveAccDetails[0].cpfAccNo}
              </Text>
            </View>
          </EABTableSection>
        </EABTable>
        {payment.totCashPortion > 0 ? (
          <View>
            <TranslatedText
              style={styles.initialPaymentMethodSubtitle}
              language={language}
              path="paymentAndSubmission.initialPayment.cashPortion"
            />
            <View style={styles.initalPaymentMethod}>
              {this.getPaymentSelector(language)}
              <PaymentButton />
            </View>
            <PaymentStatus />
            {initPayMethod === DBSCRCARDIPP ? (
              <View style={{ marginTop: Theme.alignmentXS }}>
                <TranslatedText
                  style={Theme.fieldTextOrHelperTextNegative}
                  language={language}
                  path="paymentAndSubmission.section.payment.dbsWarning"
                />
              </View>
            ) : null}
            {initPayMethod === AXASAM ? (
              <View style={styles.axsSamButtonView}>
                <EABButton
                  isDisable={isDisableEappButton}
                  style={styles.axsOnlineButton}
                  buttonType={RECTANGLE_BUTTON_1}
                  onPress={() => {
                    Linking.canOpenURL(this.axsPath).then(supported => {
                      if (!supported) {
                        return null;
                      }
                      return Linking.openURL(this.axsPath);
                    });
                  }}
                >
                  <TranslatedText language={language} path="button.axsOnline" />
                </EABButton>
                <EABButton
                  isDisable={isDisableEappButton}
                  style={styles.samOnlineButton}
                  buttonType={RECTANGLE_BUTTON_1}
                  onPress={() => {
                    Linking.canOpenURL(this.samPath).then(supported => {
                      if (!supported) {
                        return null;
                      }
                      return Linking.openURL(this.samPath);
                    });
                  }}
                >
                  <TranslatedText language={language} path="button.samOnline" />
                </EABButton>
              </View>
            ) : null}
            {initPayMethod === TELETRANSFTER ? (
              <View style={styles.teleTransfter}>
                <EABTextField
                  editable={!isDisableEappButton}
                  style={styles.fieldFirst}
                  value={ttRemittingBank || ""}
                  placeholder={EABi18n({
                    language,
                    textStore,
                    path: "placeholder.remittingBank"
                  })}
                  isError={_.get(error, "ttRemittingBank.hasError", false)}
                  hintText={_.get(
                    error,
                    `ttRemittingBank.message[${language}]`,
                    ""
                  )}
                  onChange={newValue => {
                    updatePayment({
                      path: TTREMITTINGBANK,
                      isShield: true,
                      newValue,
                      validateObj: {
                        mandatory: true,
                        field: "ttRemittingBank",
                        value: newValue
                      }
                    });
                  }}
                  isRequired
                  maxLength={30}
                />
                <SelectorField
                  editable={!isDisableEappButton}
                  style={styles.fieldFirst}
                  value={ttDOR}
                  placeholder={EABi18n({
                    language,
                    textStore,
                    path:
                      "paymentAndSubmission.section.payment.dateOfRemittance"
                  })}
                  selectorType={DATE_PICKER}
                  datePickerIOSOptions={{
                    mode: "date",
                    date: ttDOR === "" ? new Date() : new Date(ttDOR),
                    onDateChange: value => {
                      const newValue = moment(value).valueOf();
                      updatePayment({
                        path: TTDOR,
                        isShield: true,
                        newValue,
                        validateObj: {
                          mandatory: true,
                          field: "ttDOR",
                          value: newValue
                        }
                      });
                    },
                    maximumDate: new Date(moment().add(+3, "years")),
                    minimumDate: new Date(moment().add(-3, "years"))
                  }}
                  isRequired
                  isError={_.get(error, "ttDOR.hasError", false)}
                  hintText={_.get(error, `ttDOR.message[${language}]`, "")}
                />
                <EABTextBox
                  editable={!isDisableEappButton}
                  style={
                    _.get(error, "ttRemarks.hasError", false)
                      ? { marginTop: 20, borderColor: Theme.red }
                      : { marginTop: 20 }
                  }
                  placeholder={EABi18n({
                    textStore,
                    language,
                    path: "placeholder.remarks"
                  })}
                  value={ttRemarks || ""}
                  numberOfLines={2}
                  onChange={newValue => {
                    updatePayment({
                      path: TTREMARKS,
                      isShield: true,
                      newValue,
                      validateObj: {
                        mandatory: true,
                        field: "ttRemarks",
                        value: newValue
                      }
                    });
                  }}
                  maxLength={100}
                />
                {_.get(error, "ttRemarks.hasError", false) ? (
                  <View style={styles.errorRow}>
                    <TranslatedText
                      style={Theme.fieldTextOrHelperTextNegative}
                      path="error.302"
                    />
                  </View>
                ) : null}
              </View>
            ) : null}
          </View>
        ) : null}
        {payment.premiumDetails.find(
          value => value[SUBSEQPAYMETHOD] === "Y"
        ) ? (
          <TranslatedText
            style={styles.cashPortionText}
            language={language}
            path="paymentAndSubmission.initialPayment.cashPortionText"
          />
        ) : null}
      </View>
    );
  }

  groupByCid() {
    const { payment } = this.props;
    const newGroups = {};
    payment.premiumDetails.forEach(value => {
      const group = JSON.stringify(value.cid);
      newGroups[group] = newGroups[group] || [];
      newGroups[group].push(value);
    });
    return Object.keys(newGroups).map(group => newGroups[group]);
  }
  /**
   * @description data sync callback function
   * @param {string} status - data sync result
   * */
  dataSyncCallbackHandler(status) {
    const { onDataSync, dataSyncCallback } = this.state;

    if (status === "SUCCESS" && onDataSync) {
      this.props.setLastDataSyncTime();
      this.setState(
        {
          onDataSync: false
        },
        dataSyncCallback
      );
    }
  }

  render() {
    return (
      <ContextConsumer>
        {({ language }) => (
          <View>
            {this.getPremiumDetails(language)}
            {this.getInitialPaymentMethod(language)}
          </View>
        )}
      </ContextConsumer>
    );
  }
}

PaymentShield.propTypes = {
  updatePayment: PropTypes.func.isRequired,
  textStore: APP_REDUCER_TYPE_CHECK[TEXT_STORE].isRequired,
  setLastDataSyncTime: PropTypes.func.isRequired,
  error:
    REDUCER_TYPE_CHECK[REDUCER_TYPES.PAYMENT_AND_SUBMISSION].error.isRequired,
  application:
    REDUCER_TYPE_CHECK[REDUCER_TYPES.PRE_APPLICATION].ApplicationItem.value
      .isRequired,
  payment:
    REDUCER_TYPE_CHECK[REDUCER_TYPES.PAYMENT_AND_SUBMISSION].paymentShield
      .isRequired,
  template:
    REDUCER_TYPE_CHECK[REDUCER_TYPES.PAYMENT_AND_SUBMISSION].template
      .isRequired,
  optionsMap: REDUCER_TYPE_CHECK[REDUCER_TYPES.OPTIONS_MAP].isRequired,
  onlinePaymentStatus: PropTypes.oneOfType([PropTypes.object]).isRequired,
  isDisableEappButton: PropTypes.bool.isRequired
};

export default PaymentShield;
