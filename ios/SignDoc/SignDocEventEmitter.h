//
//  SignDocEventEmitter.h
//  EASE
//
//  Created by Kevin Liang on 22/9/2018.
//  Copyright © 2018 Facebook. All rights reserved.
//

#import <React/RCTEventEmitter.h>
#import <React/RCTBridgeModule.h>

@interface SignDocEventEmitter : RCTEventEmitter <RCTBridgeModule>

- (void)tellJS:(NSString *) pdfBase64;

@end
