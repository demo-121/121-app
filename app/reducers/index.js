import { REDUCER_TYPES, reducers } from "eab-web-api";
import { combineReducers } from "redux";
import {
  LOGIN,
  TEXT_STORE,
  PAYMENT,
  SUBMISSION,
  POLICYNUMBER
} from "../constants/REDUCER_TYPES";
import config from "./config";
import login from "./login";
import payment from "./payment";
import submission from "./submission";
import textStore from "./textStore";
import policyNumber from "./policyNumber";

const rootReducer = combineReducers({
  ...reducers,
  /**
   * although it from web-api, but it is platform specific and required for
   * web-api
   *  */
  [REDUCER_TYPES.CONFIG]: config,
  [TEXT_STORE]: textStore,
  [PAYMENT]: payment,
  [LOGIN]: login,
  [SUBMISSION]: submission,
  [POLICYNUMBER]: policyNumber
});

export default rootReducer;
