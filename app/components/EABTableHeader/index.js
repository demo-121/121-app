import PropTypes from "prop-types";
import React, { Component } from "react";
import { View } from "react-native";
import Theme from "../../theme";
import getStyleArray from "../../utilities/getStyleArray";

import styles from "./styles";

/**
 * <EABTableHeader />
 * @description table header component. please use it with <Table />. <Table />
 *   will apply tableConfig to this component. if you wanna cover it, just pass
 *   a tableConfig props to this component.
 * @see <EABTable /> - components/EABTable
 * @param {object|number=} style - the table header container custom style
 * @param {array=} tableConfig - array of table cell config object
 * @param {string} tableConfig[].key - table cell key
 * @param {object} tableConfig[].style - table cell style
 * @param {boolean} [isInitialized=false] - PLEASE DO NOT SET THIS PROP! it
 *   should be done in the <Table />
 * */
export default class EABTableHeader extends Component {
  constructor(props) {
    super(props);
    this.setUpProps = this.setUpProps.bind(this);
    this.renderContent = this.renderContent.bind(this);
  }

  get tableChildren() {
    const { children } = this.props;

    const removeNullChildren = [];

    if (Array.isArray(children)) {
      children.forEach(child => {
        if (child !== null) {
          removeNullChildren.push(child);
        }
      });
    } else if (children !== null) {
      removeNullChildren.push(children);
    }

    return removeNullChildren;
  }

  setUpProps({ child, index }) {
    // all check should be done in <Table />, do not need any validation in
    // this function
    const { isInitialized } = this.props;
    if (isInitialized) {
      const { tableConfig } = this.props;
      const style = [
        {
          padding: Theme.alignmentL
        },
        ...getStyleArray(tableConfig[index].style),
        ...getStyleArray(child.props.style)
      ];
      return {
        ...child.props,
        key: tableConfig[index].key,
        style
      };
    }
    return {};
  }

  renderContent() {
    return this.tableChildren.map((child, index) => {
      if (child) {
        return React.cloneElement(
          child,
          this.setUpProps({
            child,
            index
          })
        );
      }
      return null;
    });
  }

  render() {
    const { isInitialized, style } = this.props;
    const { container } = styles;
    return isInitialized ? (
      <View style={[container, getStyleArray(style)]}>
        {this.renderContent()}
      </View>
    ) : null;
  }
}

EABTableHeader.propTypes = {
  style: PropTypes.oneOfType([
    PropTypes.number,
    PropTypes.object,
    PropTypes.array
  ]),
  tableConfig: PropTypes.arrayOf(
    PropTypes.shape({
      key: PropTypes.string.isRequired,
      style: PropTypes.oneOfType([
        PropTypes.number,
        PropTypes.object,
        PropTypes.array
      ])
    })
  ),
  children: PropTypes.node.isRequired,
  isInitialized: PropTypes.bool
};

EABTableHeader.defaultProps = {
  style: [],
  tableConfig: [],
  isInitialized: false
};
