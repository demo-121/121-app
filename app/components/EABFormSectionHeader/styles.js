import { StyleSheet } from "react-native";
import Theme from "../../theme";

const image = {
  width: 24,
  height: 24
};

export default StyleSheet.create({
  container: {
    flexDirection: "row",
    width: "100%"
  },
  image,
  title: Theme.tagLine1Primary,
  info: {
    ...image,
    marginLeft: Theme.alignmentXS
  },
  requiredHint: {
    paddingLeft: Theme.alignmentXXS,
    color: Theme.negative
  }
});
