import moment from "moment";
import SQLite from "./sqlcipher";
import { SQLITE_DB_NAME } from "../constants/ENCRYPTION_FIELD_NAMES";

/**
 * initSqliteDB
 * @description init sqliteDB (encrypt database and create tables)
 * @param {string} config.appPassword - user key-in a app Password
 * @param {string} config.encryptedMasterKey - for encrypt couchbase lite database
 * @param {string} config.sqliteEncryptionKey - for encrypt sqlite database
 * @param {string} config.userID - provide from AXA login page
 * @param {string} config.syncDocumentIds - provide syncDocumentIds
 * @param {string} config.lastLoginTime - last login time
 * @param {string} config.timebombExpiryDate - timebomb expiry Date
 * @param {string} config.environment - environment
 * */
export const initSqliteDB = (config, resolve, reject) => {
  const {
    appPassword,
    encryptedMasterKey,
    sqliteEncryptionKey,
    userID,
    syncDocumentIds,
    lastLoginTime,
    timebombExpiryDate,
    environment
  } = config;
  const db = SQLite.openDatabase(
    { name: SQLITE_DB_NAME, key: sqliteEncryptionKey },
    () => {},
    err => {
      reject(err);
    }
  );
  const createConfigTable =
    "CREATE TABLE IF NOT EXISTS config (\n" +
    "	id INTEGER PRIMARY KEY AUTOINCREMENT,\n" +
    "	bz_view_version INTEGER NOT NULL\n" +
    ");" +
    " DELETE FROM config; " +
    " VACUUM; ";

  const createLoginTable =
    "CREATE TABLE IF NOT EXISTS login (\n" +
    "	id INTEGER PRIMARY KEY AUTOINCREMENT,\n" +
    "	encrypted_master_key text NOT NULL,\n" +
    "	user_id text NOT NULL,\n" +
    "	sync_document_ids text NOT NULL,\n" +
    "	hashed_app_password text NOT NULL,\n" +
    "	last_login_time INTEGER NOT NULL,\n" +
    "	login_fail_count INTEGER NOT NULL,\n" +
    "	timebomb_expiry_date INTEGER NOT NULL,\n" +
    "	environment text NOT NULL,\n" +
    "	create_dt INTEGER NOT NULL,\n" +
    "	modified_dt INTEGER NOT NULL\n" +
    "); " +
    " DELETE FROM login; " +
    " VACUUM; ";

  const createPasswordHistoryTable =
    "CREATE TABLE IF NOT EXISTS password_history (\n" +
    "	id INTEGER PRIMARY KEY AUTOINCREMENT,\n" +
    "	hashed_app_password text NOT NULL,\n" +
    "	create_dt INTEGER NOT NULL,\n" +
    "	modified_dt INTEGER NOT NULL\n" +
    ");" +
    " DELETE FROM password_history; " +
    " VACUUM; ";
  const nowTime = moment.utc().valueOf();

  // Create Config Table, select to check the len, if len ==0, insert one
  db.transaction(tx => {
    tx.executeSql(
      createConfigTable,
      [],
      () => {
        tx.executeSql(
          "select * from config",
          [],
          selectResult => {
            const len = selectResult.rows.length;
            if (len === 0) {
              tx.executeSql(
                "insert into config(bz_view_version) values (0) ",
                [],
                () => {},
                () => {}
              );
            }
          },
          () => {}
        );
      },
      () => {}
    );

    // Create Login Table, insert one and select to check sucessfully or not
    tx.executeSql(
      createLoginTable,
      [],
      () => {
        tx.executeSql(
          "insert into login(encrypted_master_key, user_id, sync_document_ids, hashed_app_password, last_login_time, " +
            "login_fail_count, timebomb_expiry_date, environment, create_dt, modified_dt )" +
            " values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?) ",
          [
            encryptedMasterKey,
            userID,
            syncDocumentIds,
            appPassword,
            lastLoginTime,
            0,
            timebombExpiryDate,
            environment,
            nowTime,
            nowTime
          ],
          () => {
            tx.executeSql("SELECT * FROM login", [], () => {}, () => {});
          },
          () => {}
        );
      },
      () => {}
    );
    tx.executeSql(
      createPasswordHistoryTable,
      [],
      () => {
        tx.executeSql(
          "insert into password_history(hashed_app_password, create_dt, modified_dt) " +
            " values (?, ?, ?) ",
          [appPassword, nowTime, nowTime],
          () => {},
          () => {}
        );
      },
      () => {}
    );
    resolve();
  });
};

/**
 * resetSqliteDB
 * @description reset sqliteDB (delete database because user might not succeed to decrypt database)
 * @param {string} config.appPassword - user key-in a app Password
 * @param {string} config.encryptedMasterKey - for encrypt couchbase lite database
 * @param {string} config.sqliteEncryptionKey - for encrypt sqlite database
 * @param {string} config.userID - provide from AXA login page
 * @param {string} config.lastLoginTime - last login time
 * @param {string} config.actiivationTime - activation time
 * @param {string} config.timebombExpiryDate - timebomb expiry Date
 * @param {string} config.environment - timebomb expiry Date
 * */
export const resetSqliteDB = config =>
  new Promise((resolve, reject) => {
    SQLite.deleteDatabase(
      { name: SQLITE_DB_NAME },
      () => initSqliteDB(config, resolve, reject),
      () => initSqliteDB(config, resolve, reject)
    );
  });

/**
 * deleteSqliteDB
 * @description delete sqliteDB (delete database because of time bomb)
 * */
export const deleteSqliteDB = () =>
  new Promise((resolve, reject) => {
    SQLite.deleteDatabase(
      { name: SQLITE_DB_NAME },
      result => resolve(result === "DB deleted"),
      reject
    );
  });
/**
 * getEnvironment
 * @description get couchbase view version
 * @param {string} environment - sqlite Encryption Key
 * */
export const getEnvironment = sqliteEncryptionKey =>
  new Promise((resolve, reject) => {
    const db = SQLite.openDatabase(
      { name: SQLITE_DB_NAME, key: sqliteEncryptionKey },
      () => {},
      err => {
        reject(err);
      }
    );

    db.transaction(tx => {
      tx.executeSql(
        "select * from login",
        [],
        (tx1, results) => {
          const len = results.rows.length;
          // resolve(results.rows.item(0).environment);
          if (len > 0) {
            resolve(results.rows.item(0).environment);
          } else {
            resolve(0);
          }
        },
        () => {
          resolve(0);
        }
      );
    });
  });

/**
 * getLoginRecord
 * @description get couchbase view version
 * @param {string} sqliteEncryptionKey - sqlite Encryption Key
 * */
export const getLoginRecord = sqliteEncryptionKey =>
  new Promise((resolve, reject) => {
    const statement = "select * from login";
    const db = SQLite.openDatabase(
      { name: SQLITE_DB_NAME, key: sqliteEncryptionKey },
      () => {},
      err => {
        reject(err);
      }
    );
    db.transaction(tx => {
      tx.executeSql(
        statement,
        [],
        (tx1, results) => {
          const len = results.rows.length;
          if (len > 0) {
            resolve(results.rows.item(0));
          } else {
            resolve(0);
          }
        },
        () => {
          resolve(0);
        }
      );
    });
  });
/**
 * update appPassword, encryptedMasterKey in the login table
 * @description get couchbase view version
 * @param {string} sqliteEncryptionKey - sqlite Encryption Key
 //  * */
//   UPDATE table_name
//   SET column1 = value1, column2 = value2, ...
//   WHERE condition;
export const updateAfterResetPassword = (
  sqliteEncryptionKey,
  encryptedMasterKey,
  appPassword,
  userID
) =>
  new Promise((resolve, reject) => {
    const statement = `update login set encrypted_master_key='${encryptedMasterKey}',hashed_app_password ='${appPassword}'where user_id='${userID}'`;
    const db = SQLite.openDatabase(
      { name: SQLITE_DB_NAME, key: sqliteEncryptionKey },
      () => {},
      err => {
        reject(err);
      }
    );
    db.transaction(tx => {
      tx.executeSql(
        statement,
        [],
        (tx1, results) => {
          const len = results.rows.length;
          if (len > 0) {
            resolve(results);
          } else {
            resolve(results);
          }
        },
        () => {
          resolve(0);
        }
      );
    });
  });

/**
 * update syncDocumentIds in the login table
 * @description get couchbase view version
 * @param {string} sqliteEncryptionKey - sqlite Encryption Key
 * */
export const updateSyncDocumentIds = (
  sqliteEncryptionKey,
  syncDocumentIds,
  userID
) =>
  new Promise((resolve, reject) => {
    const statement = `update login set sync_document_ids ='${syncDocumentIds}' where user_id ='${userID}'`;
    const db = SQLite.openDatabase(
      { name: SQLITE_DB_NAME, key: sqliteEncryptionKey },
      () => {},
      err => {
        reject(err);
      }
    );
    db.transaction(tx => {
      tx.executeSql(
        statement,
        [],
        (tx1, results) => {
          const len = results.rows.length;
          if (len > 0) {
            resolve(results);
          } else {
            resolve(results);
          }
        },
        () => {
          resolve(0);
        }
      );
    });
  });
/**
 * getSyncDocumentIds
 * @description get couchbase view version
 * @param {string} sqliteEncryptionKey - sqlite Encryption Key
 * */
export const getSyncDocumentIds = sqliteEncryptionKey =>
  new Promise((resolve, reject) => {
    const db = SQLite.openDatabase(
      { name: SQLITE_DB_NAME, key: sqliteEncryptionKey },
      () => {},
      err => {
        reject(err);
      }
    );

    db.transaction(tx => {
      tx.executeSql(
        "select * from login",
        [],
        (tx1, results) => {
          const len = results.rows.length;
          if (len > 0) {
            resolve(results.rows.item(0).sync_document_ids);
          } else {
            resolve(0);
          }
        },
        () => {
          resolve(0);
        }
      );
    });
  });
/**
 * update login table
 * @description get couchbase view version
 * @param {string} sqliteEncryptionKey - sqlite Encryption Key
 //  * */
//   UPDATE table_name
//   SET column1 = value1, column2 = value2, ...
//   WHERE condition;
export const updateFailAttempt = (
  sqliteEncryptionKey,
  failLoginAttemps,
  userID
) =>
  new Promise((resolve, reject) => {
    const statement = `update login set login_fail_count =${failLoginAttemps} where user_id ='${userID}'`;
    const db = SQLite.openDatabase(
      { name: SQLITE_DB_NAME, key: sqliteEncryptionKey },
      () => {},
      err => {
        reject(err);
      }
    );
    db.transaction(tx => {
      tx.executeSql(
        statement,
        [],
        (tx1, results) => {
          const len = results.rows.length;
          if (len > 0) {
            resolve(results);
          } else {
            resolve(results);
          }
        },
        () => {
          resolve(0);
        }
      );
    });
  });
/**
 * getFailAttempt
 * @description get couchbase view version
 * @param {string} sqliteEncryptionKey - sqlite Encryption Key
 * */

export const getFailAttempt = sqliteEncryptionKey =>
  new Promise((resolve, reject) => {
    const db = SQLite.openDatabase(
      { name: SQLITE_DB_NAME, key: sqliteEncryptionKey },
      () => {},
      err => {
        reject(err);
      }
    );

    db.transaction(tx => {
      tx.executeSql(
        "select * from login",
        [],
        (tx1, results) => {
          const len = results.rows.length;
          if (len > 0) {
            resolve(results.rows.item(0).login_fail_count);
          } else {
            resolve(0);
          }
        },
        () => {
          resolve(0);
        }
      );
    });
  });

/**
 * getUserName
 * @description get couchbase view version
 * @param {string} sqliteEncryptionKey - sqlite Encryption Key
 * */

export const getUserName = sqliteEncryptionKey =>
  new Promise((resolve, reject) => {
    const db = SQLite.openDatabase(
      { name: SQLITE_DB_NAME, key: sqliteEncryptionKey },
      () => {},
      err => {
        reject(err);
      }
    );

    db.transaction(tx => {
      tx.executeSql(
        "select * from login",
        [],
        (tx1, results) => {
          const len = results.rows.length;
          if (len > 0) {
            resolve(results.rows.item(0).user_id);
          } else {
            resolve(0);
          }
        },
        () => {
          resolve(0);
        }
      );
    });
  });
/**
 * tryLoginbyAppPassword
 * @description try login by app password
 * @param {string} sqliteEncryptionKey - sqlite Encryption Key
 * @param {string} userID - user ID
 * @param {string} hashedAppPassword - hashed App Password
 * */
export const tryLoginbyAppPassword = (
  sqliteEncryptionKey,
  userID,
  hashedAppPassword
) =>
  new Promise((resolve, reject) => {
    const db = SQLite.openDatabase(
      { name: SQLITE_DB_NAME, key: sqliteEncryptionKey },
      () => {},
      err => {
        reject(err);
      }
    );
    db.transaction(tx => {
      tx.executeSql(
        "select encrypted_master_key from login where user_id = ? and hashed_app_password = ?",
        [userID, hashedAppPassword],
        (tx1, results) => {
          const len = results.rows.length;
          if (len > 0) {
            const data = results.rows.item(0);
            if (data.encrypted_master_key) {
              resolve(data.encrypted_master_key);
            } else {
              resolve(false);
            }
          } else {
            resolve(false);
          }
        },
        () => {
          resolve(false);
        }
      );
    });
  });

/**
 * tryLoginbyOnlineAuth
 * @description try login by app password
 * @param {string} sqliteEncryptionKey - sqlite Encryption Key
 * */
export const tryLoginbyOnlineAuth = sqliteEncryptionKey =>
  new Promise((resolve, reject) => {
    const db = SQLite.openDatabase(
      { name: SQLITE_DB_NAME, key: sqliteEncryptionKey },
      () => {},
      err => {
        reject(err);
      }
    );

    db.transaction(tx => {
      tx.executeSql(
        "select encrypted_master_key from login",
        [],
        (tx1, results) => {
          const len = results.rows.length;
          if (len > 0) {
            resolve(true);
          } else {
            resolve(false);
          }
        },
        () => {
          resolve(false);
        }
      );
    });
  });

/**
 * isMobileActivated
 * @description check if it is mobile activated
 * @param {string} sqliteEncryptionKey - sqlite Encryption Key
 * */
export const isMobileActivated = sqliteEncryptionKey =>
  new Promise((resolve, reject) => {
    const db = SQLite.openDatabase(
      { name: SQLITE_DB_NAME, key: sqliteEncryptionKey },
      () => {},
      err => {
        reject(err);
      }
    );
    db.transaction(tx => {
      tx.executeSql(
        "select count('x') as user_count from login",
        [],
        (tx1, selectResult) => {
          const len = selectResult.rows.length;
          if (tx1 && len > 0) {
            resolve(selectResult.rows.item(0).user_count > 0);
          } else {
            resolve(false);
          }
          resolve(true);
        },
        () => {
          resolve(false);
        }
      );
    });
  });

/**
 * getCBViewVersion
 * @description get couchbase view version
 * @param {string} sqliteEncryptionKey - sqlite Encryption Key
 * */
export const getCBViewVersion = sqliteEncryptionKey =>
  new Promise((resolve, reject) => {
    const db = SQLite.openDatabase(
      { name: SQLITE_DB_NAME, key: sqliteEncryptionKey },
      () => {},
      err => {
        reject(err);
      }
    );

    db.transaction(tx => {
      tx.executeSql(
        "select bz_view_version from config",
        [],
        (tx1, results) => {
          const len = results.rows.length;
          if (len > 0) {
            resolve(results.rows.item(0).bz_view_version);
          } else {
            resolve(0);
          }
        },
        () => {
          resolve(0);
        }
      );
    });
  });

/**
 * saveCBViewVersion
 * @description store couchbase view version
 * @param {string} sqliteEncryptionKey - sqlite Encryption Key
 * */
export const saveCBViewVersion = (sqliteEncryptionKey, versionNum) =>
  new Promise((resolve, reject) => {
    const db = SQLite.openDatabase(
      { name: SQLITE_DB_NAME, key: sqliteEncryptionKey },
      () => {},
      err => {
        reject(err);
      }
    );

    db.transaction(tx => {
      tx.executeSql(
        "update config set bz_view_version = ?",
        [versionNum],
        () => {
          resolve(true);
        },
        () => {
          resolve(true);
        }
      );
    });
  });

/**
 * getTimeBombExpiryDate
 * @description get time bomb expiry date for user
 * @param {string} sqliteEncryptionKey - sqlite Encryption Key
 * @param {string} userID - user ID
 * */
export const getTimeBombExpiryDate = (sqliteEncryptionKey, userID) =>
  new Promise((resolve, reject) => {
    const db = SQLite.openDatabase(
      { name: SQLITE_DB_NAME, key: sqliteEncryptionKey },
      () => {},
      err => {
        reject(err);
      }
    );

    db.transaction(tx => {
      tx.executeSql(
        `select timebomb_expiry_date from login where user_id = ?`,
        [userID],
        (tx1, results) => {
          const len = results.rows.length;
          if (len > 0) {
            resolve(results.rows.item(0));
          } else {
            resolve(0);
          }
        },
        () => {
          resolve(0);
        }
      );
    });
  });

/**
 * updateTimeBombExpiryDate
 * @description update time bomb expiry date for user
 * @param {string} sqliteEncryptionKey - sqlite Encryption Key
 * @param {string} userID - user ID
 * @param {number} timebombExpiryDate - timestamp for time bomb expiry date
 * */
export const updateTimeBombExpiryDate = (
  sqliteEncryptionKey,
  userID,
  timebombExpiryDate
) =>
  new Promise((resolve, reject) => {
    const db = SQLite.openDatabase(
      { name: SQLITE_DB_NAME, key: sqliteEncryptionKey },
      () => {},
      err => {
        reject(err);
      }
    );

    db.transaction(tx => {
      tx.executeSql(
        `update login set timebomb_expiry_date = ? where user_id = ?`,
        [timebombExpiryDate, userID],
        (tx1, results) => {
          const len = results.rows.length;
          if (len > 0) {
            resolve(results);
          } else {
            resolve(results);
          }
        },
        () => {
          resolve(0);
        }
      );
    });
  });
