import PropTypes from "prop-types";
import Config from "react-native-config";
import SafariView from "react-native-safari-view";
import React, { PureComponent } from "react";
import LinearGradient from "react-native-linear-gradient";
import {
  View,
  Image,
  KeyboardAvoidingView,
  Linking,
  Alert,
  TouchableWithoutFeedback
} from "react-native";
import axaLogo from "../../../../assets/images/axaLogo.png";
import EABButton from "../../../../components/EABButton";
import { RECTANGLE_BUTTON_1 } from "../../../../components/EABButton/constants";
import { ContextConsumer } from "../../../../context";
import Theme from "../../../../theme";
import styles from "./styles";
import TranslatedText from "../../../../containers/TranslatedText";
import { TYPE_TIME_BOMB } from "../../../../constants/TIME_BOMB";
/**
 * <OnlineAuth />
 * @param {function} loginByOnlineAuth - action after getting the authCode
 * @param {function} setOnlineAuthCode - action after getting the authCode
 * */
export default class OnlineAuth extends PureComponent {
  constructor(props) {
    super(props);
    const { environment } = this.props;
    if (Config.ENV_NAME === "PROD") {
      this.path = Config.GET_AUTH_CODE_FULL_URL;
    } else {
      this.path = Config[`${environment || "SIT3_EAB"}_GET_AUTH_CODE_FULL_URL`];
    }

    this.pressHandler = this.pressHandler.bind(this);
    this.handAuthCode = this.handAuthCode.bind(this);
  }
  componentDidMount() {
    const tilteTimeBomb = "Time Bomb";
    const warningMessage2 =
      "The app has been locked as you have not synced your data recently. Please uninstall and install the app again.";
    Linking.addEventListener("url", this.handAuthCode);
    this.props.checkAppLocked(isLocked => {
      if (isLocked) {
        setTimeout(() => {
          Alert.alert(tilteTimeBomb, warningMessage2);
        }, 500);
      } else {
        this.props.checkTimeBomb(showAlert => {
          if (showAlert) {
            if (!showAlert.hasError) {
              if (showAlert.result === TYPE_TIME_BOMB) {
                setTimeout(() => {
                  Alert.alert(
                    `The app has been locked as you have not synced your data recently. Please uninstall and install the app again.`
                  );
                }, 500);
              } else {
                this.props.loginByOnlineAuth();
              }
            } else {
              this.props.loginByOnlineAuth();
            }
          } else {
            this.props.loginByOnlineAuth();
          }
        });
      }
    });
  }
  componentWillUnmount() {
    Linking.removeEventListener("url", this.handAuthCode);
  }

  pressHandler() {
    SafariView.isAvailable()
      .then(
        SafariView.show({
          url: this.path
          // url: "eabtest://"
        })
      )
      .catch(() => {
        // Fallback WebView code for iOS 8 and earlier
      });
  }

  handAuthCode(event) {
    const urlGet = event.url;
    SafariView.dismiss();
    const segment1 = urlGet.replace("easemobile://?", "");
    const variables = segment1.split("&");
    const codeString = variables.find(variable => variable.includes("code"));
    const [, authCode] = codeString.split("=");
    if (!this.props.isHandlingListener) {
      this.props.onlineAuthCode(authCode, errorResult => {
        if (errorResult.hasError) {
          if (errorResult.errorMsg) {
            setTimeout(() => {
              Alert.alert(`[Error] ${errorResult.errorMsg}`);
            }, 200);
          } else {
            setTimeout(() => {
              Alert.alert(`[Error] Authorization Failed (Error 001)`);
            }, 200);
          }
        }
      });
    }
  }
  loginByOnlineAuth() {
    if (this.props.loginByOnlineAuth != null) {
      this.props.loginByOnlineAuth(errorResult => {
        if (errorResult.errorMsg) {
          setTimeout(() => {
            Alert.alert(`[Error] ${errorResult.errorMsg}`);
          }, 200);
        } else {
          setTimeout(() => {
            Alert.alert(`[Error] Authorization Failed (Error 001)`);
          }, 200);
        }
      });
    }
  }
  header() {
    if (this.props.isExceedFailLoginAttemps) {
      return (
        <TranslatedText
          style={styles.welcome}
          path="login.onlineAuthFailAttempt"
        />
      );
    } else if (this.props.isAppPasswordExpired) {
      return (
        <TranslatedText
          style={styles.welcome}
          path="login.onlineAuthAppPasswordExpiry"
        />
      );
    }
    return <TranslatedText style={styles.welcome} path="login.onlineAuth" />;
  }

  render() {
    const headerString = this.header();
    if (this.props.isAppLocked) {
      return null;
    }
    return (
      <ContextConsumer>
        {({ showPlayGround }) => (
          <View style={styles.box}>
            <LinearGradient
              {...Theme.LinearGradientProps}
              style={styles.container}
            >
              <KeyboardAvoidingView behavior="padding">
                <View style={styles.loginForm}>
                  <View style={styles.logoWrapper}>
                    <TouchableWithoutFeedback
                      onPress={
                        process.env.NODE_ENV !== "production"
                          ? showPlayGround
                          : () => {}
                      }
                    >
                      <View>
                        <Image style={styles.logo} source={axaLogo} />
                      </View>
                    </TouchableWithoutFeedback>
                  </View>
                  {headerString}
                  <EABButton
                    style={styles.button}
                    buttonType={RECTANGLE_BUTTON_1}
                    onPress={this.pressHandler}
                  >
                    <TranslatedText path="login.openSafari" />
                  </EABButton>
                </View>
              </KeyboardAvoidingView>
            </LinearGradient>
          </View>
        )}
      </ContextConsumer>
    );
  }
}

OnlineAuth.propTypes = {
  isAppLocked: PropTypes.bool,
  environment: PropTypes.string.isRequired,
  onlineAuthCode: PropTypes.func.isRequired,
  isHandlingListener: PropTypes.bool.isRequired,
  loginByOnlineAuth: PropTypes.func.isRequired,
  checkTimeBomb: PropTypes.func.isRequired,
  isExceedFailLoginAttemps: PropTypes.bool.isRequired,
  isAppPasswordExpired: PropTypes.bool.isRequired,
  checkAppLocked: PropTypes.func.isRequired
};
OnlineAuth.defaultProps = {
  isAppLocked: false
};
