/**
 * ignore redux logger
 * */
/* eslint-disable import/no-extraneous-dependencies */
import { createLogger } from "redux-logger";
import { applyMiddleware, createStore } from "redux";
import createSagaMiddleware from "redux-saga";
import ReduxThunk from "redux-thunk";
import reducer from "../../app/reducers";

export const sagaMiddleware = createSagaMiddleware();

const middleware = [sagaMiddleware];

if (process.env.NODE_ENV !== "production") {
  middleware.push(
    createLogger({
      collapsed: true,
      diff: true
    })
  );
}

export const appStore = createStore(
  reducer,
  applyMiddleware(...middleware, ReduxThunk)
);
