import { LANGUAGE_TYPES } from "eab-web-api";
import React from "react";
import * as _ from "lodash";
import { CLIENT_FORM } from "../constants/FORM_TYPE";

const { Provider, Consumer } = React.createContext();

export const ContextProvider = Provider;
export const ContextConsumer = Consumer;

export const initialContextData = self => ({
  /**
   * @section language
   * @description handling application language data
   * */
  language: LANGUAGE_TYPES.ENGLISH,
  changeLanguage: newLanguage => {
    self.setState({
      language: newLanguage
    });
  },
  /**
   * @section client navigatorloginByAppPassword
   * @description handling showing client navigator data
   * */
  isShowingClientNavigator: false,
  showClientNavigator: (callback = () => {}) => {
    self.setState(
      {
        isShowingClientNavigator: true
      },
      callback
    );
  },
  hideClientNavigator: (callback = () => {}) => {
    self.setState(
      {
        isShowingClientNavigator: false
      },
      callback
    );
  },
  /**
   * @section playground page
   * @description handling showing playground data
   * */
  shouldShowPlayGround: false,
  showPlayGround: () => {
    self.setState({
      shouldShowPlayGround: true
    });
  },
  /**
   * @section search client page
   * @description handling filter
   * */
  searchText: "",
  isViewAll: false,
  enableShowAll: () => {
    self.setState({
      isViewAll: true,
      searchText: ""
    });
  },
  searchTextOnChange: searchText => {
    self.setState({
      searchText,
      isViewAll: false
    });
  },
  resetSearch: () => {
    self.setState({
      isViewAll: false,
      searchText: ""
    });
  },
  /**
   * @section link client dialog navigator
   * @description handling showing playground data
   * */
  isShowingLinkClientDialog: false,
  showLinkClientDialog: () => {
    self.setState({
      isShowingLinkClientDialog: true,
      isViewAll: false,
      searchText: ""
    });
  },
  hideLinkClientDialog: callback => {
    self.setState(
      {
        isShowingLinkClientDialog: false,
        isViewAll: false,
        searchText: ""
      },
      callback
    );
  },
  /**
   * @section quotation navigator
   * @description handling showing quotation dialog
   * */
  closeQuotationDialog: () => {},
  setCloseQuotationDialog: closeFunction => {
    self.setState({ closeQuotationDialog: closeFunction });
  },
  /**
   * @section quotation navigator for applications
   * @description handling showing client navigator data
   * */
  isShowingQuotationNavigator: false,
  showQuotationNavigator: () => {
    self.setState({
      isShowingQuotationNavigator: true
    });
  },
  hideQuotationNavigator: () => {
    self.setState({
      isShowingQuotationNavigator: false
    });
  },
  /**
   * @section client profile page
   * @description handling showing client form
   * */
  isShowingCreateProfileDialog: false,
  showCreateProfileDialog: (callback = () => {}) => {
    self.setState(
      {
        isShowingCreateProfileDialog: true
      },
      callback
    );
  },
  hideCreateProfileDialog: (callback = () => {}) => {
    self.setState(
      {
        isShowingCreateProfileDialog: false
      },
      callback
    );
  },
  /**
   * @section application dialog for applications
   * @description handling showing application form dialog
   * */
  isShowingApplicationDialog: false,
  showApplicationDialog: () => {
    self.setState({
      isShowingApplicationDialog: true
    });
  },
  hideApplicationDialog: callback => {
    self.setState({
      isShowingApplicationDialog: false
    });
    if (_.isFunction(callback)) {
      callback();
    }
  },
  /**
   * @section multiple client profile dialog for applying applications
   * @description handling showing multiple client profile dialog
   * */
  isShowingMultiClientProfileDialog: false,
  dialogQuotationId: null,
  dialogIsShield: null,
  showMultiClientProfileDialog: ({ quotationId, isShield }) => {
    self.setState({
      isShowingMultiClientProfileDialog: true,
      dialogQuotationId: quotationId,
      dialogIsShield: isShield
    });
  },
  hideMultiClientProfileDialog: () => {
    self.setState({
      isShowingMultiClientProfileDialog: false
    });
  },
  /**
   * searchClientDialogFormType
   * @description use to indicate the search client dialog behavior
   * */
  searchClientDialogFormType: CLIENT_FORM,
  setSearchClientDialogFormType: ({ type, callback }) => {
    self.setState(
      {
        searchClientDialogFormType: type
      },
      callback
    );
  },
  /**
   * @section application supporting document
   * @description handling showing application supporting document form dialog
   * */
  isShowingApplicationSupportingDocumentDialog: false,
  showApplicationSupportingDocumentDialog: () => {
    self.setState({
      isShowingApplicationSupportingDocumentDialog: true
    });
  },
  hideSupportingDocumentDialog: () => {
    self.setState({
      isShowingApplicationSupportingDocumentDialog: false,
      isShowingAppSummarySupportingDocumentDialog: false
    });
  },
  isShowingAppSummarySupportingDocumentDialog: false,
  showAppSummarySupportingDocumentDialog: isShield => {
    self.setState({
      isShowingAppSummarySupportingDocumentDialog: true,
      isShieldSupportingDocumentDialog: isShield
    });
  },
  hideAppSummarySupportingDocumentDialog: () => {
    self.setState({
      isShowingAppSummarySupportingDocumentDialog: false,
      isShieldSupportingDocumentDialog: false
    });
  },
  isEappLoading: false,
  setEappLoading: (bool, callback = () => {}) => {
    self.setState(
      {
        isEappLoading: bool
      },
      callback
    );
  },
  /**
   * @section PDFViewer
   * @description handle the isOpen of EABHUD inside the PDFViewer compnent
   * */
  isPDFLoading: false,
  showPDFLoading: callback => {
    self.setState({
      isPDFLoading: true
    });
    if (_.isFunction(callback)) {
      callback();
    }
  },
  hidePDFLoading: () => {
    self.setState({
      isPDFLoading: false
    });
  },
  /**
   * @section PDFViewer
   * @description set the image source of the image
   * */
  PDFSource: "",
  setPDFViewerSource: (source, callback) => {
    self.setState({
      // keep
      // PDFViewerSource: `data:image/jpeg;base64,${source}`
      PDFSource: source
    });
    if (_.isFunction(callback)) {
      callback();
    }
  },
  /**
   * @section ImageViewer
   * @description handle the isOpen of EABHUD inside the ImageViewer compnent
   * */
  isImageLoading: false,
  showImageLoading: callback => {
    self.setState({
      isImageLoading: true
    });
    if (_.isFunction(callback)) {
      callback();
    }
  },
  hideImageLoading: () => {
    self.setState({
      isImageLoading: false
    });
  },
  /**
   * @section ImageViewer
   * @description set the image source of the image
   * */
  imageSource: "",
  setImageViewerSource: (source, callback) => {
    self.setState({
      imageSource: source
    });
    if (_.isFunction(callback)) {
      callback();
    }
  },

  signDocImageSource: "",
  setSignDocImageSource: (source, callback) => {
    self.setState({
      signDocImageSource: source
    });
    if (_.isFunction(callback)) {
      callback();
    }
  },

  isShowingFNAEmailDialog: false,
  showEmailDialog: () => {
    self.setState({
      isShowingFNAEmailDialog: true
    });
  },
  hideEmailDialog: () => {
    self.setState({
      isShowingFNAEmailDialog: false
    });
  },

  emailContentDialogType: "",
  showEmailContentDialog: value => {
    self.setState({
      emailContentDialogType: value
    });
  },
  hideEmailContentDialog: () => {
    self.setState({
      emailContentDialogType: ""
    });
  },

  isBottomTabNavigatorLoading: false,
  setBottomTabNavigatorLoading: bool => {
    setTimeout(() => {
      self.setState({
        isBottomTabNavigatorLoading: bool
      });
    }, 500);
  }
});
