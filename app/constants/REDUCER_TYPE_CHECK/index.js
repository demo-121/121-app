import { TEXT_STORE } from "../REDUCER_TYPES";
import textStore from "./textStore";

export default {
  [TEXT_STORE]: textStore
};
