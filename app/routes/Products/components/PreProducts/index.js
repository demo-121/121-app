import PropTypes from "prop-types";
import React, { PureComponent } from "react";

import Products from "../../index";

/**
 * <PreProducts />
 * @param textStore - reducer textStore state
 * @param quickQuotes - quick quote list
 * @param {function} quickQuoteOnChange - change quickQuote state
 * @param {function} openProposal - saga action, open Proposal page
 * @param {function} queryQuickQuote - get quick quote list
 * */
export default class PreProducts extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {
    const { navigation, toProductPage } = this.props;
    navigation.setParams({
      toProductPage
    });
  }

  render() {
    return <Products />;
  }
}

PreProducts.propTypes = {
  toProductPage: PropTypes.func.isRequired
};
