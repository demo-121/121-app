export const PDF_ID_STANDALONE_BI = "standaloneBi";
export const PDF_ID_BI = "bi";
export const PDF_ID_PRODUCT_SUMMARY = "prodSummary";
export const PDF_ID_SHIELD_PRODUCT_SUMMARY = "shield_product_summary";
export const PDF_ID_FPX_PRODUCT_SUMMARY = "FPXProdSummary";
export const PDF_ID_RODUCT_HIGHLIGHT_SHEET = "phs";
export const PDF_ID_FUND_INFO_BOOKLET = "fib";
export const PDF_ID_SHIELD_PRODUCT_SUMMARY_1 = "shield_product_summary_1";
