import PropTypes from "prop-types";
import React, { Component } from "react";
import { Image, Text, TouchableOpacity, View } from "react-native";

import Theme from "../../theme";
import icStepperMinus from "../../assets/images/icStepperMinus.png";
import icStepperPlus from "../../assets/images/icStepperPlus.png";
import styles from "./styles";
import EABTextField from "../EABTextField";

/**
 * <EABStepper />
 * @description stepper component
 * @requires icStepperMinus - assets/images/icons/icStepperMinus.png
 * @requires icStepperPlus - assets/images/icons/icStepperPlus.png
 * @param {boolean} [supportErrorMessage=true] - switch the error function
 * @param {number} value - stepper value
 * @param {number} step - specifies the legal number intervals for the stepper
 * @param {string} hintText - error message
 * @param {function} onChange - the function that will be fire when the value
 *   change. the function parameter is the new value
 * @param {number} min - the minimum value
 * @param {number} max - the maximum value
 * */
export default class EABStepper extends Component {
  constructor(props) {
    super(props);

    this.handleChange = this.handleChange.bind(this);
    this.handleMinusPress = this.handleMinusPress.bind(this);
    this.handlePlusPress = this.handlePlusPress.bind(this);
    this.onBlurHandleChange = this.onBlurHandleChange.bind(this);
    this.valueHandler = this.valueHandler.bind(this);

    this.state = {
      isFocus: false,
      temporaryValue: this.props.value
    };
  }

  // ===========================================================================
  // life circles
  // ===========================================================================
  static getDerivedStateFromProps(nextProps, prevState) {
    if (nextProps.value === prevState.temporaryValue) {
      return null;
    }
    return {
      temporaryValue: prevState.temporaryValue
    };
  }

  // ===========================================================================
  // ===========================================================================

  onBlurHandleChange(value) {
    const { onChange, min, max, defaultValue } = this.props;

    if (value < min || value > max || value === "") {
      onChange(defaultValue);
    } else {
      onChange(value);
    }
  }

  // ===========================================================================
  // getters
  // ===========================================================================
  get displayValue() {
    const { value } = this.props;
    const { temporaryValue, isFocus } = this.state;

    if (isFocus) {
      return `${temporaryValue}`;
    }
    return typeof value === "number" ? `${value}` : "-";
  }

  // ===========================================================================
  // class methods
  // ===========================================================================
  valueHandler(value) {
    const { step, value: propValue } = this.props;

    let newValue = step < 1 ? parseFloat(value) : parseInt(value, 10);
    if (Number.isNaN(newValue)) {
      newValue = propValue;
    }

    if (value === "") {
      newValue = "";
    }

    return newValue;
  }

  handleChange(value) {
    const { onChange, min, max } = this.props;

    if (value < min) {
      onChange(min);
    } else if (value > max) {
      onChange(max);
    } else {
      onChange(value);
    }
  }

  handlePlusPress() {
    const { handleChange } = this;
    const { value, step } = this.props;

    const newValue = parseFloat(value);

    handleChange((Number.isNaN(newValue) ? 0 : newValue) + step);
  }

  handleMinusPress() {
    const { handleChange } = this;
    const { value, step } = this.props;

    const newValue = parseFloat(value);

    handleChange((Number.isNaN(newValue) ? 0 : newValue) - step);
  }

  render() {
    const {
      onBlurHandleChange,
      handlePlusPress,
      handleMinusPress,
      displayValue,
      valueHandler
    } = this;
    const { temporaryValue, isFocus } = this.state;
    /**
     * functions
     * */
    const displayHintText = () => (
      <View style={styles.hitTextWrapper}>
        <Text style={Theme.fieldTextOrHelperTextNegative}>
          {this.props.hintText}
        </Text>
      </View>
    );

    const renderMinusButton = () => {
      const { button, buttonImage } = styles;
      const { testID } = this.props;
      return (
        <TouchableOpacity
          testID={`${testID}__stpDown`}
          activeOpacity={0.5}
          onPress={handleMinusPress}
        >
          <View style={button}>
            <Image style={buttonImage} source={icStepperMinus} />
          </View>
        </TouchableOpacity>
      );
    };

    const renderValueTextLabel = () => {
      const { value: valueStyle, valueText } = styles;
      const { blockZero, testID, value } = this.props;

      return (
        <View style={valueStyle}>
          <EABTextField
            testID={`${testID}__stpTextField`}
            inputStyle={valueText}
            value={blockZero && Number(displayValue) === 0 ? "" : displayValue}
            onChange={input => {
              this.setState({
                temporaryValue: isFocus ? input : valueHandler(input)
              });
            }}
            onFocus={() => {
              this.setState({ isFocus: true, temporaryValue: value });
            }}
            onBlur={() => {
              const newValue = valueHandler(temporaryValue);

              this.setState(
                { isFocus: false, temporaryValue: newValue },
                () => {
                  onBlurHandleChange(newValue);
                }
              );
            }}
          />
        </View>
      );
    };

    const renderPlusButton = () => {
      const { button, buttonImage } = styles;
      const { testID } = this.props;
      return (
        <TouchableOpacity
          testID={`${testID}__stpUp`}
          activeOpacity={0.5}
          onPress={handlePlusPress}
        >
          <View style={button}>
            <Image style={buttonImage} source={icStepperPlus} />
          </View>
        </TouchableOpacity>
      );
    };

    return (
      <View>
        {displayHintText()}
        <View style={styles.container}>
          {renderMinusButton()}
          {renderValueTextLabel()}
          {renderPlusButton()}
        </View>
      </View>
    );
  }
}

EABStepper.propTypes = {
  value: PropTypes.oneOfType([PropTypes.number, PropTypes.string]).isRequired,
  step: PropTypes.number.isRequired,
  onChange: PropTypes.func.isRequired,
  min: PropTypes.number.isRequired,
  max: PropTypes.number.isRequired,
  defaultValue: PropTypes.number,
  hintText: PropTypes.string,
  blockZero: PropTypes.bool,
  testID: PropTypes.string
};
EABStepper.defaultProps = {
  hintText: "",
  defaultValue: 0,
  blockZero: false,
  testID: ""
};
