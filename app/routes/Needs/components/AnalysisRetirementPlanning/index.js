import PropTypes from "prop-types";
import React, { Component } from "react";
import { Text, View } from "react-native";
import * as _ from "lodash";
import {
  utilities,
  REDUCER_TYPE_CHECK as WEB_API_REDUCER_TYPE_CHECK,
  REDUCER_TYPES,
  NEEDS
} from "eab-web-api";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import EABTextField from "../../../../components/EABTextField";
import EABTextBox from "../../../../components/EABTextBox";
import TranslatedText from "../../../../containers/TranslatedText";
import APP_REDUCER_TYPE_CHECK from "../../../../constants/REDUCER_TYPE_CHECK";
import { TEXT_STORE } from "../../../../constants/REDUCER_TYPES";
import Theme from "../../../../theme";
import EABi18n from "../../../../utilities/EABi18n";
import EABStepper from "../../../../components/EABStepper";
import { ContextConsumer } from "../../../../context";
import styles from "../Analysis/styles";
import PopoverTooltip from "../../../../components/PopoverTooltip/PopoverTooltipIOS";
import EASEAssets from "../../../../containers/EASEAssets";

const { FNA, CLIENT } = REDUCER_TYPES;
const { numberToCurrency, currencyToNumber } = utilities.numberFormatter;
const { AVGINFLATRATE } = NEEDS;
const { calOtherAsset } = utilities.naAnalysis;

/**
 * AnalysisRetirementPlanning
 * @description Analysis - Retirement Planning - Template
 * */
export default class AnalysisRetirementPlanning extends Component {
  constructor(props) {
    super(props);
    this.state = {};

    this.onChange = this.onChange.bind(this);
  }

  componentDidMount() {
    this.props.initNaAnalysis();
  }

  componentDidUpdate(prevProps) {
    if (prevProps.selectedProfile !== this.props.selectedProfile) {
      this.props.initNaAnalysis();
    }
  }

  onChange({ value, key }) {
    const {
      selectedProfile,
      updateNA,
      na,
      pda,
      profile,
      dependantProfiles,
      fe
    } = this.props;

    if (selectedProfile === "owner" || selectedProfile === "spouse") {
      updateNA({
        naData: {
          ...na,
          rPlanning: {
            ...na.rPlanning,
            [selectedProfile]: {
              ...na.rPlanning[selectedProfile],
              [key]: value,
              init: false
            }
          }
        },
        pdaData: pda,
        profileData: profile,
        dependantProfilesData: dependantProfiles,
        feData: fe
      });
    } else {
      const index = na.rPlanning.dependants.findIndex(
        dependant => dependant.cid === selectedProfile
      );
      const dependants = _.cloneDeep(na.rPlanning.dependants);
      dependants[index] = {
        ...dependants[index],
        [key]: value,
        init: false
      };

      updateNA({
        naData: {
          ...na,
          rPlanning: {
            ...na.rPlanning,
            dependants
          }
        },
        pdaData: pda,
        profileData: profile,
        dependantProfilesData: dependantProfiles,
        feData: fe
      });
    }
  }

  checkDependant() {
    const { selectedProfile, data } = this.props;
    if (!data || _.isEmpty(data)) {
      return {};
    }
    if (selectedProfile !== "owner" && selectedProfile !== "spouse") {
      if (data.dependants && data.dependants.length) {
        return data.dependants.find(val => val.cid === selectedProfile);
      }
    } else {
      return data[selectedProfile];
    }
    return "";
  }

  shortfallUI() {
    const data = this.checkDependant();
    const { totShortfall } = data;

    return {
      totShortfallPath:
        totShortfall >= 0
          ? "na.needs.common.surplus"
          : "na.needs.common.shortfall",
      totShortfallStyle:
        totShortfall >= 0 ? styles.surplusContent : styles.shortfallContent,
      totShortfallInfo:
        totShortfall >= 0
          ? "na.needs.common.surplusContent"
          : "na.needs.common.shortfallContent"
    };
  }

  render() {
    const {
      textStore,
      profile,
      dependantProfiles,
      selectedProfile,
      updateNA,
      na,
      fe,
      pda,
      isRPlanningError
    } = this.props;

    const data = this.checkDependant();

    const {
      totShortfallPath,
      totShortfallStyle,
      totShortfallInfo
    } = this.shortfallUI();

    const spouseKey = Object.keys(dependantProfiles).find(
      key => dependantProfiles[key].relationship === "SPO"
    );
    const allowance =
      selectedProfile === "owner"
        ? profile.allowance
        : dependantProfiles[spouseKey].allowance;

    const selectedAssets = na.rPlanning[selectedProfile].assets;
    const { onChange } = this;

    return (
      <ContextConsumer>
        {({ language }) => (
          <View>
            <KeyboardAwareScrollView
              extraScrollHeight={100}
              style={{ marginBottom: Theme.alignmentXXXXL }}
            >
              <View
                key={123}
                style={[
                  styles.analysisProductContent,
                  { marginBottom: Theme.alignmentXXXXL }
                ]}
              >
                <Text style={styles.title}>
                  {EABi18n({
                    language,
                    textStore,
                    path: "na.needs.rPlanning"
                  })}
                </Text>
                <Text style={styles.analysisProductPointTitle}>
                  {`1. ${EABi18n({
                    language,
                    textStore,
                    path: "na.needs.rPlanning.retireAge"
                  })}`}
                </Text>
                <EABStepper
                  testID="NeedsAnalysis__RP"
                  value={data.retireAge || 65}
                  step={1}
                  min={profile.age}
                  max={99}
                  defaultValue={65}
                  onChange={value =>
                    updateNA({
                      naData: {
                        ...na,
                        rPlanning: {
                          ...na.rPlanning,
                          [selectedProfile]: {
                            ...na.rPlanning[selectedProfile],
                            retireAge: value,
                            init: false
                          }
                        },
                        completedStep: 1
                      },
                      pdaData: pda,
                      profileData: profile,
                      dependantProfilesData: dependantProfiles,
                      feData: fe
                    })
                  }
                  blockZero
                />
                <View style={styles.analysisSectionBorderLine} />

                {/* TODO: Add minimum and maximum,
                numeric restrictions on this field */}
                <Text style={styles.analysisProductPointTitle}>
                  {`2. ${EABi18n({
                    language,
                    textStore,
                    path: "na.needs.rPlanning.inReqRetirement"
                  })}`}
                  <Text style={styles.requiredHint}> *</Text>
                </Text>

                <EABTextField
                  testID="NeedsAnalysis__RP__txtIncomeRequired"
                  style={styles.analysisTextInputShort}
                  prefix="$"
                  value={numberToCurrency({
                    value: data.inReqRetirement || 0,
                    decimal: 0
                  })}
                  isError={
                    isRPlanningError[selectedProfile].inReqRetirement.hasError
                  }
                  hintText={EABi18n({
                    path:
                      isRPlanningError[selectedProfile].inReqRetirement
                        .messagePath,
                    language,
                    textStore
                  })}
                  onChange={value =>
                    updateNA({
                      naData: {
                        ...na,
                        rPlanning: {
                          ...na.rPlanning,
                          [selectedProfile]: {
                            ...na.rPlanning[selectedProfile],
                            inReqRetirement:
                              value !== "0" ? currencyToNumber(value) : "",
                            init: false
                          }
                        },
                        completedStep: 1
                      },
                      pdaData: pda,
                      profileData: profile,
                      dependantProfilesData: dependantProfiles,
                      feData: fe
                    })
                  }
                  maxLength={
                    11 +
                    numberToCurrency({
                      value: data.inReqRetirement || 0,
                      decimal: 0
                    }).replace(/[^,]/g, "").length
                  }
                />

                <Text style={styles.analysisProductPointText}>
                  {`${EABi18n({
                    language,
                    textStore,
                    path: "na.needs.rPlanning.annualInReqRetirement"
                  })}`}
                </Text>
                <Text style={styles.analysisProductPointContent}>
                  {`$${numberToCurrency({
                    value: data.annualInReqRetirement || 0,
                    decimal: 0,
                    maxValue: 999999999999999
                  })}`}
                </Text>

                {/* unMatchPmtReason */}
                {allowance < data.inReqRetirement ? (
                  <View>
                    <Text style={styles.analysisProductPointText}>
                      {`${EABi18n({
                        language,
                        textStore,
                        path: "na.needs.rPlanning.unMatchPmtReason"
                      })}`}
                      <Text style={styles.requiredHint}> *</Text>
                    </Text>
                    <EABTextBox
                      style={styles.analysisProductTextBox}
                      numberOfLines={5}
                      maxLength={200}
                      value={data.unMatchPmtReason || ""}
                      onChange={value => {
                        updateNA({
                          naData: {
                            ...na,
                            rPlanning: {
                              ...na.rPlanning,
                              [selectedProfile]: {
                                ...na.rPlanning[selectedProfile],
                                unMatchPmtReason: value,
                                init: false
                              }
                            },
                            completedStep: 1
                          },
                          pdaData: pda,
                          profileData: profile,
                          dependantProfilesData: dependantProfiles,
                          feData: fe
                        });
                      }}
                    />
                    {isRPlanningError[selectedProfile].unMatchPmtReason
                      .hasError ? (
                      <View>
                        <Text style={Theme.fieldTextOrHelperTextNegative}>
                          {EABi18n({
                            textStore,
                            language,
                            path:
                              isRPlanningError[selectedProfile].unMatchPmtReason
                                .messagePath
                          })}
                        </Text>
                      </View>
                    ) : null}
                  </View>
                ) : null}
                <View style={styles.analysisSectionBorderLine} />
                <View style={styles.titleRow}>
                  <Text style={styles.analysisProductPointTitle}>
                    {`3. ${`${EABi18n({
                      language,
                      textStore,
                      path: "na.needs.rPlanning.timeHorizon"
                    })} `}`}
                  </Text>
                  <PopoverTooltip
                    popoverOptions={{ preferredContentSize: [300, 90] }}
                    text={EABi18n({
                      textStore,
                      language,
                      path: "na.needs.rPlanning.timeHorizon.tooltip"
                    })}
                  />
                </View>
                <TranslatedText
                  style={styles.analysisProductPointContent}
                  path="na.needs.common.years"
                  replace={[
                    {
                      value: data.timeHorizon || 0,
                      isPath: false
                    }
                  ]}
                />
                <View style={styles.analysisSectionBorderLine} />

                <Text style={styles.analysisProductPointTitle}>
                  {`4. ${EABi18n({
                    language,
                    textStore,
                    path: "na.needs.rPlanning.avgInflatRate"
                  })}`}
                </Text>
                <Text style={styles.analysisProductPointContent}>
                  {`${data.avgInflatRate || AVGINFLATRATE}%`}
                </Text>
                <View style={styles.analysisSectionBorderLine} />

                <Text style={styles.analysisProductPointTitle}>
                  {`5. ${EABi18n({
                    language,
                    textStore,
                    path: "na.needs.rPlanning.compFv"
                  })}`}
                </Text>
                <Text style={styles.analysisProductPointContent}>
                  {`$${numberToCurrency({
                    value: data.compFv || 0,
                    decimal: 2,
                    maxValue: 999999999999999
                  })}`}
                </Text>
                <View style={styles.analysisSectionBorderLine} />

                <Text style={styles.analysisProductPointTitle}>
                  {`6. ${EABi18n({
                    language,
                    textStore,
                    path: "na.needs.rPlanning.othRegIncome"
                  })}`}
                </Text>
                <EABTextField
                  testID="NeedsAnalysis__RP__txtOtherIncome"
                  style={styles.analysisTextInputShort}
                  prefix="$"
                  value={numberToCurrency({
                    value: data.othRegIncome || 0,
                    decimal: 0
                  })}
                  onChange={value =>
                    updateNA({
                      naData: {
                        ...na,
                        rPlanning: {
                          ...na.rPlanning,
                          [selectedProfile]: {
                            ...na.rPlanning[selectedProfile],
                            othRegIncome:
                              value !== "0" ? currencyToNumber(value) : "",
                            init: false
                          }
                        },
                        completedStep: 1
                      },
                      pdaData: pda,
                      profileData: profile,
                      dependantProfilesData: dependantProfiles,
                      feData: fe
                    })
                  }
                />
                <View style={styles.analysisSectionBorderLine} />

                <Text style={styles.analysisProductPointTitle}>
                  {`7. ${EABi18n({
                    language,
                    textStore,
                    path: "na.needs.rPlanning.firstYrPMT"
                  })}`}
                </Text>
                <Text style={styles.analysisProductPointContent}>
                  {`$${numberToCurrency({
                    value: data.firstYrPMT || 0,
                    decimal: 2,
                    maxValue: 999999999999999
                  })}`}
                </Text>
                <View style={styles.analysisSectionBorderLine} />

                <Text style={styles.analysisProductPointTitle}>
                  {`8. ${EABi18n({
                    language,
                    textStore,
                    path: "na.needs.rPlanning.iarRate2"
                  })}`}
                </Text>
                <Text style={styles.analysisProductPointContent}>
                  {data.iarRate2 || na.iarRate}%
                </Text>
                <View style={styles.analysisSectionBorderLine} />

                <View style={styles.titleRow}>
                  <Text style={styles.analysisProductPointTitle}>
                    {`9. ${`${EABi18n({
                      language,
                      textStore,
                      path: "na.needs.rPlanning.retireDuration"
                    })}`}`}
                  </Text>
                  <Text style={styles.requiredHint}>*</Text>
                </View>
                <EABTextField
                  style={styles.analysisTextInputShort}
                  value={numberToCurrency({
                    value: data.retireDuration || 0,
                    decimal: 0
                  })}
                  isError={
                    isRPlanningError[selectedProfile].retireDuration.hasError
                  }
                  hintText={EABi18n({
                    path:
                      isRPlanningError[selectedProfile].retireDuration
                        .messagePath,
                    language,
                    textStore
                  })}
                  onChange={value => {
                    updateNA({
                      naData: {
                        ...na,
                        rPlanning: {
                          ...na.rPlanning,
                          [selectedProfile]: {
                            ...na.rPlanning[selectedProfile],
                            retireDuration:
                              value !== "0" ? currencyToNumber(value) : "",
                            init: false
                          }
                        },
                        completedStep: 1
                      },
                      pdaData: pda,
                      profileData: profile,
                      dependantProfilesData: dependantProfiles,
                      feData: fe
                    });
                  }}
                  maxLength={2}
                />
                <View style={styles.analysisSectionBorderLine} />

                <Text style={styles.analysisProductPointTitle}>
                  {`10. ${EABi18n({
                    language,
                    textStore,
                    path: "na.needs.rPlanning.compPv"
                  })}`}
                </Text>
                <Text style={styles.analysisProductPointContent}>
                  {`$${numberToCurrency({
                    value: data.compPv || 0,
                    decimal: 2,
                    maxValue: 999999999999999
                  })}`}
                </Text>
                <View style={styles.analysisSectionBorderLine} />
                <View style={styles.titleRow}>
                  <Text style={styles.analysisProductPointTitle}>
                    {`11. ${EABi18n({
                      language,
                      textStore,
                      path: "na.needs.rPlanning.assets"
                    })}`}
                  </Text>
                  <PopoverTooltip
                    popoverOptions={{ preferredContentSize: [365, 65] }}
                    text={EABi18n({
                      textStore,
                      language,
                      path: "na.needs.assets.toolTip"
                    })}
                  />
                </View>
                <EASEAssets
                  testID="NeedsAnalysis__RP__ValueOfAssets"
                  style={styles.analysisAssetsWrapper}
                  selectedProduct="rPlanning"
                  selectedProfile={selectedProfile}
                  selectedAssets={selectedAssets}
                  addAssetsOnPress={() => {
                    let cloneAssets = _.cloneDeep(selectedAssets);
                    if (!cloneAssets) {
                      cloneAssets = [];
                    }
                    cloneAssets.push({
                      calAsset: 0,
                      key: "all",
                      usedAsset: 0
                    });
                    onChange({ value: cloneAssets, key: "assets" });
                  }}
                  removeAssetsOnPress={i => {
                    const cloneAssets = _.cloneDeep(selectedAssets);
                    cloneAssets.forEach((assetsData, index) => {
                      if (i === index) {
                        cloneAssets.splice(index, 1);
                        onChange({ value: cloneAssets, key: "assets" });
                      }
                    });
                  }}
                  assetTextOnChange={(value, index) => {
                    const cloneAssets = _.cloneDeep(selectedAssets);
                    if (selectedAssets[index].return) {
                      cloneAssets[index] = {
                        key: selectedAssets[index].key,
                        usedAsset: currencyToNumber(value),
                        return: selectedAssets[index].return
                      };
                    } else {
                      cloneAssets[index] = {
                        key: selectedAssets[index].key,
                        usedAsset: currencyToNumber(value)
                      };
                    }
                    onChange({ value: cloneAssets, key: "assets" });
                  }}
                  assetSelectorOnChange={(itemKey, item) => {
                    if (
                      !selectedAssets.find(select => select.key === item.value)
                    ) {
                      let cloneAssets = _.cloneDeep(selectedAssets);
                      if (selectedAssets[item.prevData.index]) {
                        cloneAssets[item.prevData.index] = {
                          calAsset: 0,
                          key: item.value,
                          usedAsset: 0
                        };
                      } else {
                        cloneAssets = selectedAssets.concat({
                          calAsset: 0,
                          key: item.value,
                          usedAsset: 0
                        });
                      }
                      onChange({ value: cloneAssets, key: "assets" });
                    }
                  }}
                  rateOfReturn
                  returnOnChange={(value, index) => {
                    const cloneAssets = _.cloneDeep(selectedAssets);
                    cloneAssets[index] = {
                      key: selectedAssets[index].key,
                      usedAsset: selectedAssets[index].usedAsset,
                      return: value
                    };
                    onChange({ value: cloneAssets, key: "assets" });
                  }}
                  isProtectionError={isRPlanningError[selectedProfile]}
                  calOtherAsset={({ key, selectedProduct }) =>
                    calOtherAsset({
                      key,
                      selectedProduct,
                      selectedProfile,
                      na
                    })
                  }
                />
                <View style={styles.analysisSectionBorderLine} />
                <Text style={styles.analysisProductPointTitle}>
                  {`12. ${EABi18n({
                    language,
                    textStore,
                    path: "na.needs.rPlanning.maturityValue"
                  })}`}
                </Text>
                <Text style={styles.analysisProductPointContent}>
                  {`$${numberToCurrency({
                    value: fe[selectedProfile].retirePol || 0,
                    decimal: 0
                  })}`}
                </Text>
              </View>
            </KeyboardAwareScrollView>
            {data.totShortfall < 0 && (
              <View style={styles.shortfallView}>
                <View style={styles.shortfallWrapper}>
                  <View style={styles.subShortfallView}>
                    <Text style={Theme.fieldTextOrHelperTextNormal}>
                      {`${EABi18n({
                        language,
                        textStore,
                        path: "na.needs.common.annualRetirementIncome"
                      })}: `}
                      <Text style={totShortfallStyle}>
                        {`$${numberToCurrency({
                          value: Math.abs(data.annRetireIncome) || 0,
                          decimal: 2,
                          maxValue: 999999999999999
                        })}`}
                      </Text>
                    </Text>
                  </View>
                </View>
              </View>
            )}
            <View
              style={
                data.totShortfall < 0
                  ? [styles.shortfallView, { bottom: 100 }]
                  : styles.shortfallView
              }
            >
              <View style={styles.shortfallWrapper}>
                <PopoverTooltip
                  popoverOptions={{ preferredContentSize: [350, 100] }}
                  text={EABi18n({
                    textStore,
                    language,
                    path: totShortfallInfo
                  })}
                />
                <View style={styles.subShortfallView}>
                  <Text style={Theme.fieldTextOrHelperTextNormal}>
                    {`${EABi18n({
                      language,
                      textStore,
                      path: totShortfallPath
                    })}: `}
                    <Text style={totShortfallStyle}>
                      {`$${numberToCurrency({
                        value: Math.abs(data.totShortfall || 0),
                        decimal: 2,
                        maxValue: 999999999999999
                      })}`}
                    </Text>
                  </Text>
                </View>
              </View>
            </View>
          </View>
        )}
      </ContextConsumer>
    );
  }
}

AnalysisRetirementPlanning.propTypes = {
  na: WEB_API_REDUCER_TYPE_CHECK[FNA].na.isRequired,
  fe: WEB_API_REDUCER_TYPE_CHECK[FNA].fe.isRequired,
  pda: WEB_API_REDUCER_TYPE_CHECK[FNA].pda.isRequired,
  selectedProfile: PropTypes.string.isRequired,
  textStore: APP_REDUCER_TYPE_CHECK[TEXT_STORE].isRequired,
  data: WEB_API_REDUCER_TYPE_CHECK[FNA].analysisData.isRequired,
  initNaAnalysis: PropTypes.func.isRequired,
  updateNA: PropTypes.func.isRequired,
  // TODO
  isRPlanningError: PropTypes.shape({}).isRequired,
  profile: WEB_API_REDUCER_TYPE_CHECK[CLIENT].profile.isRequired,
  dependantProfiles:
    WEB_API_REDUCER_TYPE_CHECK[CLIENT].dependantProfiles.isRequired
};
