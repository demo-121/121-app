import React, { Component, cloneElement } from "react";
import PropTypes from "prop-types";
import { Animated, Easing, PanResponder, Image, View } from "react-native";
import { shallowEqual } from "./utils";
import styles from "./styles";
import icReorder from "../../assets/images/icReorder.png";

export default class Row extends Component {
  constructor(props) {
    super(props);

    this._animatedLocation = new Animated.ValueXY(props.location);
    this._location = props.location;

    this._animatedLocation.addListener(this._onChangeLocation);

    this._animated = new Animated.Value(0);

    this._onChangeLocation = this._onChangeLocation.bind(this);
    this._isTouchInsideElement = this._isTouchInsideElement.bind(this);
    this._isDisabled = this._isDisabled.bind(this);
    this._mapGestureToMove = this._mapGestureToMove.bind(this);
    this._toggleActive = this._toggleActive.bind(this);
    this._relocate = this._relocate.bind(this);
    this._cancelLongPress = this._cancelLongPress.bind(this);
    this.moveBy = this.moveBy.bind(this);

    this._onLayout = this._onLayout.bind(this);

    this._style = {
      /**
       * Use transform for animation when row is on tapping
       */
      // transform: [{
      //   scale: this._animated.interpolate({
      //     inputRange: [0, 1],
      //     outputRange: [1, 1.1],
      //   }),
      // }],
      shadowRadius: this._animated.interpolate({
        inputRange: [0, 1],
        outputRange: [2, 20]
      })
    };

    this._panResponder = PanResponder.create({
      onStartShouldSetPanResponder: () => !this._isDisabled(),

      onMoveShouldSetPanResponder: (e, gestureState) => {
        if (this._isDisabled()) return false;

        const vy = Math.abs(gestureState.vy);
        const vx = Math.abs(gestureState.vx);

        return this._active && (this.props.horizontal ? vx > vy : vy > vx);
      },

      onShouldBlockNativeResponder: () => false,

      onPanResponderGrant: (e, gestureState) => {
        e.persist();

        this._target = e.nativeEvent.target;
        this._prevGestureState = {
          ...gestureState,
          moveX: gestureState.x0,
          moveY: gestureState.y0
        };

        if (this.props.manuallyActivateRows) return;

        this._longPressTimer = setTimeout(() => {
          if (this._active) return;

          this._toggleActive(e, gestureState);
        }, this.props.activationTime);
      },

      onPanResponderMove: (e, gestureState) => {
        if (
          !this._active ||
          gestureState.numberActiveTouches > 1 ||
          e.nativeEvent.target !== this._target
        ) {
          if (!this._isTouchInsideElement(e)) {
            this._cancelLongPress();
          }

          return;
        }

        const elementMove = this._mapGestureToMove(
          this._prevGestureState,
          gestureState
        );
        this.moveBy(elementMove);
        this._prevGestureState = { ...gestureState };

        if (this.props.onMove) {
          this.props.onMove(e, gestureState, this._nextLocation);
        }
      },

      onPanResponderRelease: (e, gestureState) => {
        if (this._active) {
          this._toggleActive(e, gestureState);
        } else {
          this._cancelLongPress();

          if (this._isTouchInsideElement(e) && this.props.onPress) {
            this.props.onPress();
          }
        }
      },

      onPanResponderTerminationRequest: () => {
        if (this._active) {
          return false;
        }

        this._cancelLongPress();

        return true;
      },

      onPanResponderTerminate: (e, gestureState) => {
        this._cancelLongPress();

        // If responder terminated while dragging,
        // deactivate the element and move to the initial location.
        if (this._active) {
          this._toggleActive(e, gestureState);

          if (shallowEqual(this.props.location, this._location)) {
            this._relocate(this.props.location);
          }
        }
      }
    });
  }

  componentWillReceiveProps(nextProps) {
    if (!this._active && !shallowEqual(this._location, nextProps.location)) {
      const animated = !this._active && nextProps.animated;
      this._relocate(nextProps.location, animated);
    }

    if (this.props.active !== nextProps.active) {
      Animated.timing(this._animated, {
        duration: 300,
        easing: Easing.bounce,
        toValue: Number(nextProps.active)
      }).start();
    }
  }

  shouldComponentUpdate(nextProps) {
    return (
      this.props.disabled !== nextProps.disabled ||
      this.props.children !== nextProps.children ||
      !shallowEqual(this.props.style, nextProps.style)
    );
  }

  moveBy({ dx = 0, dy = 0, animated = false }) {
    this._nextLocation = {
      x: this._location.x + dx,
      y: this._location.y + dy
    };
    this._relocate(this._nextLocation, animated);
  }

  _cancelLongPress() {
    clearTimeout(this._longPressTimer);
  }

  _relocate(nextLocation, animated) {
    this._location = nextLocation;

    if (animated) {
      this._isAnimationRunning = true;
      Animated.timing(this._animatedLocation, {
        toValue: nextLocation,
        duration: 300
      }).start(() => {
        this._isAnimationRunning = false;
      });
    } else {
      this._animatedLocation.setValue(nextLocation);
    }
  }

  _toggleActive(e, gestureState) {
    const callback = this._active
      ? this.props.onRelease
      : this.props.onActivate;

    this._active = !this._active;

    if (callback) {
      callback(e, gestureState, this._location);
    }
  }

  _mapGestureToMove(prevGestureState, gestureState) {
    return this.props.horizontal
      ? { dx: gestureState.moveX - prevGestureState.moveX }
      : { dy: gestureState.moveY - prevGestureState.moveY };
  }

  _isDisabled() {
    return this.props.disabled || this._isAnimationRunning;
  }

  _isTouchInsideElement({ nativeEvent }) {
    return (
      this._layout &&
      nativeEvent.locationX >= 0 &&
      nativeEvent.locationX <= this._layout.width &&
      nativeEvent.locationY >= 0 &&
      nativeEvent.locationY <= this._layout.height
    );
  }

  _onChangeLocation(value) {
    this._location = value;
  }

  _onLayout(e) {
    this._layout = e.nativeEvent.layout;

    if (this.props.onLayout) {
      this.props.onLayout(e);
    }
  }

  render() {
    const { children, style, fullRow } = this.props;
    const rowStyle = [
      style,
      styles.container,
      this._animatedLocation.getLayout(),
      this._style
    ];
    const icReorderStyle = [styles.icReorder, this.props.rowColor];
    const rowAttr = fullRow ? { ...this._panResponder.panHandlers } : {};
    const cellAttr = fullRow ? {} : { ...this._panResponder.panHandlers };

    return (
      <View style={this.props.rowCssBoxModel}>
        <Animated.View style={rowStyle} onLayout={this._onLayout} {...rowAttr}>
          {this.props.manuallyActivateRows && children
            ? cloneElement(children, {
                toggleRowActive: this._toggleActive
              })
            : children}
          {this.props.disabled ? (
            <View style={[styles.icPlaceholder, this.props.rowColor]} />
          ) : (
            <Animated.View style={icReorderStyle} {...cellAttr}>
              <Image source={icReorder} />
            </Animated.View>
          )}
        </Animated.View>
      </View>
    );
  }
}

Row.propTypes = {
  children: PropTypes.node.isRequired,
  location: PropTypes.shape({
    x: PropTypes.number,
    y: PropTypes.number
  }),
  activationTime: PropTypes.number,
  animated: PropTypes.bool,
  disabled: PropTypes.bool,
  horizontal: PropTypes.bool,
  style: Animated.View.propTypes.style,
  manuallyActivateRows: PropTypes.bool,
  fullRow: PropTypes.bool,

  // Called on long press.
  onActivate: PropTypes.func,
  onLayout: PropTypes.func,
  onPress: PropTypes.func,

  // Called, when user (directly) move the view.
  onMove: PropTypes.func,

  // Called, when user release the view.
  onRelease: PropTypes.func,
  active: PropTypes.bool,
  rowColor: PropTypes.shape({
    backgroundColor: PropTypes.string
  }),
  rowCssBoxModel: PropTypes.oneOfType([PropTypes.object])
};

Row.defaultProps = {
  location: { x: 0, y: 0 },
  activationTime: 200,
  animated: false,
  disabled: false,
  horizontal: true,
  style: {},
  manuallyActivateRows: false,
  onActivate: () => {},
  onLayout: () => {},
  onPress: () => {},
  onMove: () => {},
  onRelease: () => {},
  active: false,
  rowColor: {},
  rowCssBoxModel: {},
  fullRow: false
};
