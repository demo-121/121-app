/* eslint-disable react/no-did-update-set-state */
import PropTypes from "prop-types";
import React, { Component } from "react";
import { Animated, Text, View } from "react-native";
import * as _ from "lodash";
import TextInputBugFix from "../../containers/TextInputBugFix";
import Theme from "../../theme";
import getStyleArray from "../../utilities/getStyleArray";
import styles from "./styles";

/**
 * <EABTextField />
 * @description text input component. base on
 *   {@link https://facebook.github.io/react-native/docs/textinput.html}
 * @param {string} value - the input field value
 * @param {function} onChange - value on change call back with "value" parameter
 * @param {function} onBlur - field on blur call back with "value" parameter
 * @param {function} onFocus - field on focus call back with "value" parameter
 * @param {boolean} [supportLabelPlaceholder=true] - switch the placeholder function
 * @param {string=} placeholder - input field label
 * @param {string} [keyboardType='default'] - determines which keyboard to
 *   open. check
 *   https://facebook.github.io/react-native/docs/textinput.html#keyboardtype
 *   to get more information
 * @param {boolean} [secureTextEntry=false] - obscures the text entered so that
 *   sensitive text like passwords stay secure
 * @param {boolean} [editable=true] - if false, text is not editable
 * @param {boolean} [isRequired=false] - indicates whether the text field show
 *   required dot or not
 * @param {string=} hintText - error message
 * @param {string} [toolTip=""] - tool tip message. only show when <EABTextField />
 *   is focus.
 * @param {string=} prefix - string before the value
 * @param {boolean} [supportErrorMessage=true] - switch the error function
 * @param {boolean=} isError - indicates whether the text field is error or not
 * @param {boolean=} isAutoFocus - focus the input field when launch page
 * @param {number} maxLength - the maximum number of characters in the input field
 * @param {object|number=} style - text field container style.
 * @param {object|number=} inputStyle - text field container style.
 * */
export default class EABTextField extends Component {
  // TODO: Add minimum and maximum, numeric restrictions on this class
  constructor(props) {
    super(props);

    this.state = {
      isFocus: false,
      labelAnimateStatus: new Animated.Value(this.valueLength === 0 ? 0 : 1),
      labelHeight: 0,
      labelWidth: 0
    };
  }

  // ===========================================================================
  // life circles
  // ===========================================================================
  componentDidUpdate(prevProps) {
    let prevValueLength = 0;
    if (prevProps.value !== "") {
      prevValueLength = prevProps.prefix.length + prevProps.value.length;
    }

    if (prevProps.value !== this.props.value) {
      if (prevValueLength === 0 && this.valueLength !== 0) {
        this.switchToLabel();
      }
      if (
        prevValueLength !== 0 &&
        this.valueLength === 0 &&
        !this.state.isFocus
      ) {
        this.switchToPlaceholder();
      }
    }
  }

  // ===========================================================================
  // getters
  // ===========================================================================
  get valueLength() {
    let newValueLength = 0;

    if (this.props.value !== "") {
      newValueLength = this.props.prefix.length + this.props.value.length;
    }

    return newValueLength;
  }
  /**
   * @description get the input style
   * @return {array} input style config
   * */
  get inputStyle() {
    const {
      supportLabelPlaceholder,
      supportErrorMessage,
      editable,
      isError
    } = this.props;
    const { isFocus } = this.state;
    const style = [styles.input];

    if (supportLabelPlaceholder) {
      style.push(styles.inputPlaceholderPlugin);

      if (isFocus) {
        style.push(styles.inputPlaceholderFocus);
      } else if (isError) {
        style.push(styles.inputPlaceholderError);
      }
    }

    if (supportErrorMessage) {
      style.push(styles.inputErrorPlugin);
    }

    if (!editable) {
      style.push(styles.inputNonEditable);
    }

    return style;
  }

  // ===========================================================================
  // class methods
  // ===========================================================================
  switchToLabel() {
    Animated.timing(this.state.labelAnimateStatus, {
      toValue: 1,
      duration: 125,
      useNativeDriver: true
    }).start();
  }

  switchToPlaceholder() {
    Animated.timing(this.state.labelAnimateStatus, {
      toValue: 0,
      duration: 125,
      useNativeDriver: true
    }).start();
  }

  removePrefix(newValue) {
    const { prefix, value } = this.props;

    // new value have prefix?
    if (!_.startsWith(newValue, prefix)) {
      // old value have prefix? if yes, clean data
      return !_.startsWith(value, prefix) ? newValue : "";
    }

    return newValue.slice(prefix.length);
  }

  render() {
    // =========================================================================
    // variables
    // =========================================================================
    const { inputStyle } = this;
    const reduceSize = 0.75;
    let placeholderStyle;
    const {
      supportLabelPlaceholder,
      supportErrorMessage,
      isError,
      toolTip
    } = this.props;
    const { isFocus } = this.state;
    // =========================================================================
    // render functions
    // =========================================================================
    const displayHintText = () => {
      switch (true) {
        case toolTip !== "" && isFocus:
          return (
            <Text numberOfLines={2} style={Theme.fieldTextOrHelperTextBrand}>
              {toolTip}
            </Text>
          );
        case (supportErrorMessage && isError && toolTip === "") ||
          (supportErrorMessage && isError && toolTip !== "" && !isFocus):
          return (
            <Text style={Theme.fieldTextOrHelperTextNegative}>
              {this.props.hintText}
            </Text>
          );
        default:
          return null;
      }
    };

    if (this.props.editable) {
      if (this.state.isFocus && this.props.value.length !== 0) {
        placeholderStyle = styles.labelTextFocus;
      } else {
        placeholderStyle = styles.labelText;
      }
    } else {
      placeholderStyle = styles.labelNonEditable;
    }

    return (
      <View style={[styles.wrapper, ...getStyleArray(this.props.style)]}>
        {supportLabelPlaceholder ? (
          <Animated.View
            style={[
              styles.label,
              {
                transform: [
                  {
                    translateY: this.state.labelAnimateStatus.interpolate({
                      inputRange: [0, 1],
                      outputRange: [
                        24,
                        -(this.state.labelHeight * ((1 - reduceSize) / 2))
                      ]
                    })
                  },
                  {
                    translateX: this.state.labelAnimateStatus.interpolate({
                      inputRange: [0, 1],
                      outputRange: [
                        0,
                        -(this.state.labelWidth * ((1 - reduceSize) / 2))
                      ]
                    })
                  },
                  {
                    scale: this.state.labelAnimateStatus.interpolate({
                      inputRange: [0, 1],
                      outputRange: [1, reduceSize]
                    })
                  }
                ]
              }
            ]}
            onLayout={event => {
              this.setState({
                labelWidth: event.nativeEvent.layout.width,
                labelHeight: event.nativeEvent.layout.height
              });
            }}
          >
            <Text style={placeholderStyle}>{this.props.placeholder}</Text>
            {this.props.isRequired ? (
              <Text style={styles.requiredHint}>*</Text>
            ) : null}
          </Animated.View>
        ) : null}
        <TextInputBugFix
          testID={this.props.testID}
          onRef={ref => {
            this.input = ref;
          }}
          style={[
            ...getStyleArray(inputStyle),
            ...getStyleArray(this.props.inputStyle)
          ]}
          placeholder={
            supportLabelPlaceholder === false ? this.props.placeholder : ""
          }
          placeholderTextColor={
            this.props.editable ? Theme.darkGrey : Theme.mediumGrey
          }
          secureTextEntry={this.props.secureTextEntry}
          maxLength={this.props.maxLength}
          editable={this.props.editable}
          value={
            this.props.value === ""
              ? this.props.value
              : this.props.prefix + this.props.value
          }
          keyboardType={this.props.keyboardType}
          autoFocus={this.props.isAutoFocus}
          onFocus={() => {
            this.switchToLabel();
            this.props.onFocus(
              this.props.prefix
                ? this.removePrefix(this.props.value)
                : this.props.value
            );
            if (this.props.editable) {
              this.setState({ isFocus: true });
            }
          }}
          onBlur={event => {
            event.persist();
            setTimeout(() => {
              if (this.props.value.length === 0) {
                this.switchToPlaceholder();
              }
              this.props.onBlur(
                this.props.prefix
                  ? this.removePrefix(this.props.value)
                  : event.nativeEvent.text || this.props.value
              );
              this.setState({ isFocus: false });
            }, this.props.onChangeTimeOut + 50);
          }}
          onChangeText={value => {
            /* check if textField value without dollar sign and value */
            let returnValue = "";
            if (this.props.returnWithPrefix) {
              if (
                value.indexOf(this.props.prefix) === -1 &&
                this.props.value === ""
              ) {
                returnValue = this.props.prefix + value;
              } else {
                returnValue = value;
              }
            } else {
              returnValue =
                this.props.prefix !== "" ? this.removePrefix(value) : value;
            }
            this.props.onChange(returnValue);
          }}
          onChangeTimeOut={this.props.onChangeTimeOut}
        />
        {supportErrorMessage ? (
          <View style={styles.hitTextWrapper}>{displayHintText()}</View>
        ) : null}
      </View>
    );
  }
}

EABTextField.propTypes = {
  secureTextEntry: PropTypes.bool,
  editable: PropTypes.bool,
  value: PropTypes.string.isRequired,
  supportLabelPlaceholder: PropTypes.bool,
  placeholder: PropTypes.string,
  hintText: PropTypes.string,
  toolTip: PropTypes.string,
  isRequired: PropTypes.bool,
  supportErrorMessage: PropTypes.bool,
  isError: PropTypes.bool,
  maxLength: PropTypes.number,
  prefix: PropTypes.string,
  returnWithPrefix: PropTypes.bool,
  onChange: PropTypes.func.isRequired,
  onBlur: PropTypes.func,
  onFocus: PropTypes.func,
  keyboardType: PropTypes.string,
  isAutoFocus: PropTypes.bool,
  style: PropTypes.oneOfType([
    PropTypes.number,
    PropTypes.object,
    PropTypes.array
  ]),
  inputStyle: PropTypes.oneOfType([
    PropTypes.number,
    PropTypes.object,
    PropTypes.array
  ]),
  onChangeTimeOut: PropTypes.number,
  testID: PropTypes.string
};
EABTextField.defaultProps = {
  supportLabelPlaceholder: true,
  placeholder: "",
  secureTextEntry: false,
  editable: true,
  isRequired: false,
  hintText: "",
  toolTip: "",
  keyboardType: "default",
  onBlur: () => {},
  onFocus: () => {},
  supportErrorMessage: true,
  isError: false,
  prefix: "",
  isAutoFocus: false,
  maxLength: 9999,
  style: {},
  inputStyle: {},
  returnWithPrefix: false,
  onChangeTimeOut: 600,
  testID: ""
};
