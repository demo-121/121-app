import * as _ from "lodash";
import PropTypes from "prop-types";
import React, { PureComponent } from "react";
import { Image, View, Text, TouchableHighlight } from "react-native";
import avatar64X64Dp from "../../assets/images/avatar64X64Dp.png";
import getStyleArray from "../../utilities/getStyleArray";
import styles from "./styles";

/**
 * <Avatar />
 * @description Avatar component. if source is ""/null/undefined, will auto
 *   change to use defautl photo
 * @param {string|number|object} [source=avatar64X64Dp] - avatar photo
 * @param {number|object|array} [style={}] - container custom style
 * @param {number|object|array} [imageStyle={}] - image custom style
 * @param {number} [width=40] - avatar width
 * @param {number} [height=40] - avatar height
 * */
export default class Avatar extends PureComponent {
  get getPhoto() {
    const { source } = this.props;

    if (source) {
      return source;
    }

    return avatar64X64Dp;
  }

  render() {
    /**
     * variables
     * */
    const { getPhoto } = this;
    const {
      style,
      imageStyle,
      width,
      height,
      showIcon,
      iconTextArray
    } = this.props;
    let iconText = "";
    iconTextArray.forEach(text => {
      iconText += (text[0] || "").toUpperCase();
    });

    return (
      <View
        style={[
          styles.clientPhotoWrapper,
          ...getStyleArray(style),
          { width, height }
        ]}
      >
        <TouchableHighlight key="avatar" onPress={this.props.avatarOnPress}>
          {showIcon ? (
            <Image
              style={[...getStyleArray(imageStyle), { width, height }]}
              source={!_.isEmpty(getPhoto) ? getPhoto : avatar64X64Dp}
            />
          ) : (
            <View style={styles.clientIconInner}>
              <Text style={styles.clientIconText}>{iconText}</Text>
            </View>
          )}
        </TouchableHighlight>
      </View>
    );
  }
}

Avatar.propTypes = {
  avatarOnPress: PropTypes.func,
  width: PropTypes.number,
  height: PropTypes.number,
  showIcon: PropTypes.bool,
  iconTextArray: PropTypes.oneOfType([PropTypes.array]),
  style: PropTypes.oneOfType([
    PropTypes.number,
    PropTypes.object,
    PropTypes.array
  ]),
  imageStyle: PropTypes.oneOfType([
    PropTypes.number,
    PropTypes.object,
    PropTypes.array
  ]),
  source: PropTypes.oneOfType([
    PropTypes.object,
    PropTypes.number,
    PropTypes.string
  ])
};

Avatar.defaultProps = {
  avatarOnPress: () => {},
  width: 40,
  height: 40,
  style: {},
  imageStyle: {},
  showIcon: true,
  iconTextArray: [],
  source: PropTypes.oneOfType([
    PropTypes.object,
    PropTypes.number,
    PropTypes.string,
    avatar64X64Dp
  ])
};
