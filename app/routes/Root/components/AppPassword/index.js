import PropTypes from "prop-types";
import React, { PureComponent } from "react";
import { Image, KeyboardAvoidingView, View, Alert, Text } from "react-native";
import LinearGradient from "react-native-linear-gradient";
import axaLogo from "../../../../assets/images/axaLogo.png";
import EABButton from "../../../../components/EABButton";
import { RECTANGLE_BUTTON_1 } from "../../../../components/EABButton/constants";
import EABTextField from "../../../../components/EABTextField";
import Theme from "../../../../theme";
import styles from "./styles";
import { POLICY_NUMBER_ERROR } from "../../../../constants/ERROR_TYPE";
import EABHUD from "../../../../containers/EABHUD";
import TranslatedText from "../../../../containers/TranslatedText";
/**
 * <AppPassword />
 * @param {function} saveNewAppPassword - method for save new app password
 * @param {object} text - UI text store object
 * @param {string} text.passwordDoNotMeetPasswordPolicy - password do not meet password policy text
 * */
export default class AppPassword extends PureComponent {
  constructor(props) {
    super(props);
    this.saveNewAppPassword = this.saveNewAppPassword.bind(this);
    this.navigateToLandingPage = this.navigateToLandingPage.bind(this);
    this.state = {
      password: "",
      password2: "",
      isDisableButton: false
    };
  }

  setNewPasswordRender() {
    return (
      <View style={styles.loginForm}>
        <View style={styles.logoWrapper}>
          <Image style={styles.logo} source={axaLogo} />
        </View>
        <EABTextField
          placeholder="App Password"
          secureTextEntry
          value={this.state.password}
          onChange={pw => {
            this.setState({
              password: pw
            });
          }}
        />
        <EABTextField
          placeholder="App Confirm Password"
          secureTextEntry
          value={this.state.password2}
          onChange={pw2 => {
            this.setState({
              password2: pw2
            });
          }}
        />
        <EABButton
          style={styles.button}
          buttonType={RECTANGLE_BUTTON_1}
          isDisable={this.state.isDisableButton}
          onPress={this.saveNewAppPassword}
        >
          <TranslatedText path="login.LogIn" />
        </EABButton>
      </View>
    );
  }
  resetNewAppPasswordRender() {
    return (
      <View style={styles.loginForm}>
        <View style={styles.logoWrapper}>
          <Image style={styles.logo} source={axaLogo} />
        </View>
        <TranslatedText path="login.ResetAppPassword" />
        <EABTextField
          placeholder="Reset App Password"
          secureTextEntry
          value={this.state.password}
          onChange={pw => {
            this.setState({
              password: pw
            });
          }}
        />
        <EABTextField
          placeholder="App Confirm Password"
          secureTextEntry
          value={this.state.password2}
          onChange={pw2 => {
            this.setState({
              password2: pw2
            });
          }}
        />
        <EABButton
          style={styles.button}
          buttonType={RECTANGLE_BUTTON_1}
          isDisable={this.state.isDisableButton}
          onPress={this.saveNewAppPassword}
        >
          <TranslatedText path="login.LogIn" />
        </EABButton>
      </View>
    );
  }
  resetNewAppPasswordManuallyRender() {
    return (
      <View style={styles.loginForm}>
        <View style={styles.logoWrapper}>
          <Image style={styles.logo} source={axaLogo} />
        </View>
        <EABTextField
          placeholder="Reset App Password"
          secureTextEntry
          value={this.state.password}
          onChange={pw => {
            this.setState({
              password: pw
            });
          }}
        />
        <EABTextField
          placeholder="Reset App Confirm Password"
          secureTextEntry
          value={this.state.password2}
          onChange={pw2 => {
            this.setState({
              password2: pw2
            });
          }}
        />
        <View style={styles.buttonWrapper}>
          <EABButton
            style={styles.button}
            buttonType={RECTANGLE_BUTTON_1}
            isDisable={this.state.isDisableButton}
            onPress={this.saveNewAppPassword}
          >
            <Text>Reset Password</Text>
          </EABButton>
          <EABButton
            style={styles.button}
            buttonType={RECTANGLE_BUTTON_1}
            onPress={this.navigateToLandingPage}
          >
            <Text>Cancel</Text>
          </EABButton>
        </View>
      </View>
    );
  }

  saveNewAppPassword() {
    this.setState(
      {
        isDisableButton: true
      },
      () => {
        let isValidPwPolicy = true;
        let warningMessage = "";
        if (this.state.password && this.state.password2) {
          if (this.state.password !== this.state.password2) {
            // password legth issue
            isValidPwPolicy = false;
            warningMessage =
              "App password and App confirm password does not match";
          } else if (
            this.state.password.length >= 8 &&
            this.state.password.length <= 16
          ) {
            const r = /^(?:(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[^a-zA-Z\d\s:]).*)$/g;
            if (r.test(this.state.password)) {
              const p = this.state.password.toUpperCase();
              const AlphanumericA = [
                "0",
                "1",
                "2",
                "3",
                "4",
                "5",
                "6",
                "7",
                "8",
                "9"
              ];
              const AlphanumericB = [
                "A",
                "B",
                "C",
                "D",
                "E",
                "F",
                "G",
                "H",
                "I",
                "J",
                "K",
                "L",
                "M",
                "N",
                "O",
                "P",
                "Q",
                "R",
                "S",
                "T",
                "U",
                "V",
                "W",
                "X",
                "Y",
                "Z"
              ];
              for (let i = 0; i < p.length; i += 1) {
                const indexA = AlphanumericA.findIndex(
                  varchar => varchar === p[i]
                );
                const indexB = AlphanumericB.findIndex(
                  varchar => varchar === p[i]
                );

                if (
                  (indexA !== -1 && p[i + 1] && p[i + 2]) ||
                  (indexB !== -1 && p[i + 1] && p[i + 2])
                ) {
                  if (
                    AlphanumericB[indexB] === p[i + 1] &&
                    AlphanumericB[indexB] === p[i + 2]
                  ) {
                    // Check AAA
                    isValidPwPolicy = false;
                    warningMessage =
                      "Alphanumeric patterns like AAA, AaA, bbb and 888 are invalid (case sensitive)";
                  } else if (
                    AlphanumericB[indexB + 1] === p[i + 1] &&
                    AlphanumericB[indexB + 2] === p[i + 2]
                  ) {
                    // Check ABC
                    isValidPwPolicy = false;
                    warningMessage =
                      "Alphanumeric patterns like 123, Abc, EFG and xyz are invalid (case sensitive)";
                  } else if (
                    AlphanumericA[indexA] === p[i + 1] &&
                    AlphanumericA[indexA] === p[i + 2]
                  ) {
                    // Check 111
                    isValidPwPolicy = false;
                    warningMessage =
                      "Alphanumeric patterns like AAA, AaA, bbb and 888 are invalid (case sensitive)";
                  } else if (
                    AlphanumericA[indexA + 1] === p[i + 1] &&
                    AlphanumericA[indexA + 2] === p[i + 2]
                  ) {
                    // Check 123
                    isValidPwPolicy = false;
                    warningMessage =
                      "Alphanumeric patterns like 123, Abc, EFG and xyz are invalid (case sensitive)";
                  } else {
                    // Do nothing
                  }
                }
              }
            } else {
              isValidPwPolicy = false;
              warningMessage = "Password doesn’t meet the security guidelines.";
            }
          } else {
            // password legth issue
            isValidPwPolicy = false;
            warningMessage = "Minimum length =8 and Maximum length=16";
          }
        } else {
          // no password enter
          isValidPwPolicy = false;
          warningMessage =
            "Please Enter Reset App Password and Reset App Confirm Password";
        }
        if (!isValidPwPolicy) {
          if (warningMessage && warningMessage.length > 0) {
            Alert.alert(`[Error] ${warningMessage}`);
          } else {
            Alert.alert(
              `[Error] ${this.props.text.passwordDoNotMeetPasswordPolicy}`
            );
          }
        } else if (
          (isValidPwPolicy && this.props.showResetAppPasswordManuallyScene) ||
          (isValidPwPolicy && this.props.showResetAppPasswordScene)
        ) {
          this.props.resetAppPasswordManually(
            this.state.password,
            errorResult => {
              if (errorResult.errorType === POLICY_NUMBER_ERROR) {
                setTimeout(() => {
                  Alert.alert(
                    `[Error] Policy number exhausted, please connect your device to online mode and login again.`
                  );
                }, 200);
              } else if (errorResult.error) {
                setTimeout(() => {
                  Alert.alert(`[Error] ${errorResult.error}`);
                }, 200);
              } else if (errorResult.errorMsg) {
                setTimeout(() => {
                  Alert.alert(`[Error] ${errorResult.errorMsg}`);
                }, 200);
              } else {
                setTimeout(() => {
                  Alert.alert(`[Error] Authorization Failed (Error 005)`);
                }, 200);
              }
            }
          );
        } else {
          this.props.saveNewAppPassword(this.state.password, errorResult => {
            if (errorResult.errorType === POLICY_NUMBER_ERROR) {
              setTimeout(() => {
                Alert.alert(
                  `[Error] Policy number exhausted, please connect your device to online mode and login again.`
                );
              }, 200);
            } else if (errorResult.error) {
              setTimeout(() => {
                Alert.alert(`[Error] ${errorResult.error}`);
              }, 200);
            } else if (errorResult.errorMsg) {
              setTimeout(() => {
                Alert.alert(`[Error]${errorResult.errorMsg}`);
              }, 200);
            } else {
              setTimeout(() => {
                Alert.alert(`[Error] Authorization Failed (Error 005)`);
              }, 200);
            }
          });
        }
        this.setState(
          {
            isDisableButton: false
          },
          () => {}
        );
      }
    );
  }
  navigateToLandingPage() {
    if (this.props.navigateToLandingPage != null) {
      this.props.navigateToLandingPage();
    }
  }

  render() {
    let renderCase = null;
    if (
      this.props.showResetAppPasswordScene ||
      this.props.isAppPasswordExpired
    ) {
      renderCase = this.resetNewAppPasswordRender();
    } else if (this.props.showResetAppPasswordManuallyScene) {
      renderCase = this.resetNewAppPasswordManuallyRender();
    } else {
      renderCase = this.setNewPasswordRender();
    }
    return (
      <LinearGradient {...Theme.LinearGradientProps} style={styles.container}>
        <KeyboardAvoidingView behavior="padding">
          {renderCase}
        </KeyboardAvoidingView>
        <EABHUD />
      </LinearGradient>
    );
  }
}

AppPassword.propTypes = {
  saveNewAppPassword: PropTypes.func.isRequired,
  navigateToLandingPage: PropTypes.func.isRequired,
  resetAppPasswordManually: PropTypes.func.isRequired,
  isAppPasswordExpired: PropTypes.bool,
  showResetAppPasswordManuallyScene: PropTypes.bool,
  showResetAppPasswordScene: PropTypes.bool,
  text: PropTypes.shape({
    passwordDoNotMeetPasswordPolicy: PropTypes.string.isRequired
  }).isRequired
};

AppPassword.defaultProps = {
  showResetAppPasswordManuallyScene: false,
  isAppPasswordExpired: false,
  showResetAppPasswordScene: false
};
