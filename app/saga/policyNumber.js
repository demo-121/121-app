import { put, call, select } from "redux-saga/effects";
import * as _ from "lodash";
import SInfo from "react-native-sensitive-info";
import { REDUCER_TYPES, ACTION_TYPES } from "eab-web-api";
import {
  createDocument,
  getDocumentWithID,
  updateDocumentWithID
} from "../utilities/DataManager";
import { POLICYNUMBER } from "../constants/REDUCER_TYPES";
import APP_ACTION_TYPES from "../constants/ACTION_TYPES";
import { getPolicyNumber } from "../apis/policyNumber";
import { checkVersionAndDevice } from "../apis/login";
import {
  MAX_OF_SHIELD_POLICY_NUM,
  MAX_OF_NON_SHIELD_POLICY_NUM,
  POLICY_NUM_DOCUMENT,
  POLICY_DOCUMENT
} from "../constants/POLICY_NUMBER";
import {
  INSTALLATION_ID
  // LAST_DATA_SYNC_STATUS
} from "../constants/ENCRYPTION_FIELD_NAMES";
import {
  FAIL_GET_POLICY_NO_DOC,
  FAIL_CREATE_POLICY_NO_DOC,
  FAIL_TO_UPDATE_POLICY_NO_DOC,
  POLICY_NO_EXHAUSTED,
  FAIL_TO_GET_PN_FROM_API,
  NO_POLICY_NUMBER_AVAILABLE
} from "../../app/constants/ERROR_MSG";
import { POLICY_NUMBER_ERROR } from "../constants/ERROR_TYPE";

const { CONFIG } = REDUCER_TYPES;

/**
 * initPolicyNumber
 * @description 1. Get policy number from api 2. Create policy number document in CB
 * @param callback: callback
 * */
export function* initPolicyNumber(callback) {
  try {
    yield put({
      type: ACTION_TYPES[CONFIG].START_LOADING
    });
    let checkVersionAndDeviceResult = {};
    let errorMsg = "";
    let getDocumentResult = {};
    let initPolicyNumberResult = {};
    let policyNumberData = {};
    let getPolicyNumberFromServiceResult = {};
    let createDocumentResult = {};
    const policyNumberID = POLICY_NUM_DOCUMENT;
    const shieldPolicyNumArray = [];
    const nonShieldPolicyNumArray = [];
    // 1. Get policy number from api
    const numOfShield = MAX_OF_SHIELD_POLICY_NUM;
    const numOfNonShield = MAX_OF_NON_SHIELD_POLICY_NUM;
    const authToken = yield select(state => state.login.authToken);
    const environment = yield select(state => state.login.environment);
    const installationID = yield call(SInfo.getItem, INSTALLATION_ID, {});
    // need to refill the pool of policy number
    checkVersionAndDeviceResult = yield call(
      checkVersionAndDevice,
      authToken,
      installationID,
      environment
    );
    if (!checkVersionAndDeviceResult.hasError) {
      if (checkVersionAndDeviceResult.isValidVersionAndDevice) {
        // isValidVersionAndDevice = true --> ok
        // OK la
      } else {
        // isValidVersionAndDevice = false --> throw errorMsg
        errorMsg = checkVersionAndDeviceResult.errorMsg
          ? checkVersionAndDeviceResult.errorMsg
          : "Authorization Failed (Error 007)";
        throw errorMsg;
      }
    } else {
      errorMsg = checkVersionAndDeviceResult.errorMsg
        ? checkVersionAndDeviceResult.errorMsg
        : "Authorization Failed (Error 007)";
      throw errorMsg;
    }

    getPolicyNumberFromServiceResult = yield call(
      getPolicyNumber,
      authToken,
      numOfShield,
      numOfNonShield,
      environment
    );
    if (!getPolicyNumberFromServiceResult.hasError) {
      if (
        getPolicyNumberFromServiceResult.result.data &&
        getPolicyNumberFromServiceResult.result.data.status === "success"
      ) {
        // create shieldPolicyNumber array in policyNumberPool

        if (
          getPolicyNumberFromServiceResult.result.data.result.shieldPolicyNum
        ) {
          const array =
            getPolicyNumberFromServiceResult.result.data.result.shieldPolicyNum;
          for (let i = 0; i < array.length; i += 1) {
            const o = {
              policyNum: array[i],
              isUsed: false,
              isSubmitted: false
            };
            shieldPolicyNumArray.push(o);
          }
        } else {
          // error in get shieldPolicyNum
          errorMsg = FAIL_TO_GET_PN_FROM_API;
          throw errorMsg;
        }

        // create isNotShieldPolicyNumber array in policyNumberPool
        if (
          getPolicyNumberFromServiceResult.result.data &&
          getPolicyNumberFromServiceResult.result.data.result.nonShieldPolicyNum
        ) {
          const array =
            getPolicyNumberFromServiceResult.result.data.result
              .nonShieldPolicyNum;
          for (let j = 0; j < array.length; j += 1) {
            const p = {
              policyNum: array[j],
              isUsed: false,
              isSubmitted: false
            };
            nonShieldPolicyNumArray.push(p);
          }
        } else {
          // error in get nonShieldPolicyNum
          errorMsg = FAIL_TO_GET_PN_FROM_API;
          throw errorMsg;
        }

        // create policy number document with fix id
        policyNumberData = {
          type: "policyNumber",
          shieldPolicyNumber: shieldPolicyNumArray,
          nonShieldPolicyNumber: nonShieldPolicyNumArray
        };
      } else {
        // do nothing
      }
    } else if (getPolicyNumberFromServiceResult.errorMsg === 500) {
      const er = POLICY_NO_EXHAUSTED;
      throw er;
    } else {
      // Fail to call api to refill 20/20 policy numbers
      errorMsg = FAIL_TO_GET_PN_FROM_API;
      throw errorMsg;
    }
    // 2. Create policy number document
    const createPolicyNumberDocument = (docID, docData) =>
      new Promise(resolve => {
        createDocument(docID, docData, () => {
          createDocumentResult = {
            hasError: false,
            result: policyNumberData
          };
          resolve(createDocumentResult);
        });
      });
    const getPolicyNumberDocumentWithID = docID =>
      new Promise(resolve => {
        getDocumentWithID(docID, cbResult => {
          if (cbResult.error && cbResult.error.length > 0) {
            getDocumentResult = {
              hasError: true,
              errorMsg: cbResult.error
            };
          } else {
            getDocumentResult = {
              hasError: false,
              result: cbResult
            };
          }
          resolve(getDocumentResult);
        });
      });
    yield call(createPolicyNumberDocument, policyNumberID, policyNumberData);
    if (!createDocumentResult.hasError) {
      yield call(getPolicyNumberDocumentWithID, policyNumberID);
      initPolicyNumberResult = {
        hasError: false,
        errorType: POLICY_NUMBER_ERROR,
        result: getDocumentResult.result
      };
      callback(initPolicyNumberResult);
    } else {
      errorMsg = FAIL_CREATE_POLICY_NO_DOC;

      throw errorMsg;
    }
    yield put({
      type: ACTION_TYPES[CONFIG].FINISH_LOADING
    });
  } catch (e) {
    const initPolicyNumberResult = {
      hasError: true,
      errorType: POLICY_NUMBER_ERROR,
      errorMsg: e
    };
    yield put({
      type: ACTION_TYPES[CONFIG].FINISH_LOADING
    });
    callback(initPolicyNumberResult);
  }
}

/**
 * getPolicyNumberFromService
 * @description 1. Get policy number from api 2. Create policy number document in CB
 * @param callback: callback
 * */
export function* getPolicyNumberFromService(
  numOfShield,
  numOfNonShield,
  callback
) {
  try {
    yield put({
      type: ACTION_TYPES[CONFIG].START_LOADING
    });
    let getPolicyNumberFromServiceResult = {};
    let errorMsg = "";
    if (numOfShield > 0 || numOfNonShield > 0) {
      const authToken = yield select(state => state.login.authToken);
      const environment = yield select(state => state.login.environment);
      // need to refill the pool of policy number
      const getPolicyNumberResult = yield call(
        getPolicyNumber,
        authToken,
        numOfShield,
        numOfNonShield,
        environment
      );
      if (!getPolicyNumberResult.hasError) {
        const shieldPolicyNumArray =
          getPolicyNumberResult.result.shieldPolicyNum;
        const nonShieldPolicyNumArray =
          getPolicyNumberResult.result.isNotShieldPolicyNumber;
        getPolicyNumberFromServiceResult = {
          hasError: true,
          errorType: POLICY_NUMBER_ERROR,
          result: {
            shieldPolicyNum: shieldPolicyNumArray,
            nonShieldPolicyNum: nonShieldPolicyNumArray
          }
        };
        yield put({
          type: ACTION_TYPES[CONFIG].FINISH_LOADING
        });
        callback(getPolicyNumberFromServiceResult);
      } else if (getPolicyNumberResult.hasError) {
        if (getPolicyNumberResult.errorMsg === 500) {
          errorMsg = POLICY_NO_EXHAUSTED;
          throw errorMsg;
        }
      } else {
        // fail at calling api to refill policy number
        errorMsg = FAIL_TO_GET_PN_FROM_API;
        throw errorMsg;
      }
    } else {
      errorMsg =
        "numOfShieldPolicyNumberRequired / numOfNotShieldPolicyNumberRequired must be larger than zero";
      throw errorMsg;
    }
  } catch (e) {
    const getPolicyNumberFromServiceResult = {
      hasError: true,
      errorType: POLICY_NUMBER_ERROR,
      errorMsg: e
    };
    yield put({
      type: ACTION_TYPES[CONFIG].FINISH_LOADING
    });
    callback(getPolicyNumberFromServiceResult);
  }
}

export function* checkPolicyNumber(action) {
  try {
    // yield put({
    //   type: ACTION_TYPES[CONFIG].START_LOADING
    // });
    let errorMsg = "";
    let checkVersionAndDeviceResult = {};
    let checkPolicyNumberResult = {};
    let getDocumentResult = {};
    let getPolicyNumberResult = {};
    let updateDocumentResult = {};
    const policyNumberID = POLICY_NUM_DOCUMENT;
    let numOfShield = 0;
    let numOfNonShield = 0;
    let findIndex1 = 0;
    let findIndex2 = 0;
    // 1. Get Document from CB DB given the name of policy numnber json
    const getPolicyNumberDocumentWithID = docID =>
      new Promise(resolve => {
        getDocumentWithID(docID, callback => {
          if (callback.error && callback.error.length > 0) {
            getDocumentResult = {
              hasError: true,
              errorMsg: callback.error
            };
          } else {
            getDocumentResult = {
              hasError: false,
              result: callback
            };
          }
          resolve(getDocumentResult);
        });
      });
    yield call(getPolicyNumberDocumentWithID, policyNumberID);
    if (!getDocumentResult.hasError) {
      // Can get the doucment
      // get the content and save into the state
      yield put({
        type: APP_ACTION_TYPES[POLICYNUMBER].POLICY_NUMBER_DOC,
        policyNumberData: getDocumentResult.result
      });
    } else {
      yield call(initPolicyNumber, callbackResult => {
        if (!callbackResult.hasError) {
          getDocumentResult = {
            hasError: false,
            result: callbackResult
          };
          getDocumentResult = getDocumentResult.result;
        } else {
          throw callbackResult.errorMsg
            ? callbackResult.errorMsg
            : "Fail to create policy number document";
        }
      });
      // set the reducer state
      yield put({
        type: APP_ACTION_TYPES[POLICYNUMBER].POLICY_NUMBER_DOC,
        policyNumberData: getDocumentResult.result
      });
    }
    // For now we have getPolicyNumberResult
    if (getDocumentResult && getDocumentResult.result) {
      const policyNumberDocument = getDocumentResult.result;
      if (policyNumberDocument && policyNumberDocument.shieldPolicyNumber) {
        findIndex1 = policyNumberDocument.shieldPolicyNumber.findIndex(
          element => element.isUsed === false
        );
        if (findIndex1 === -1) {
          // cannot find true
          numOfShield = MAX_OF_SHIELD_POLICY_NUM;
          yield put({
            type:
              APP_ACTION_TYPES[POLICYNUMBER].NO_POLICY_NUMBER_AVAILABLE_SHIELD
          });
        } else if (findIndex1 === 0) {
          numOfShield = 0;
        } else if (
          policyNumberDocument.shieldPolicyNumber.length - findIndex1 >=
          MAX_OF_SHIELD_POLICY_NUM
        ) {
          numOfShield = 0;
        } else {
          numOfShield =
            MAX_OF_SHIELD_POLICY_NUM -
            (policyNumberDocument.shieldPolicyNumber.length - findIndex1);
        }
      }
      if (policyNumberDocument && policyNumberDocument.nonShieldPolicyNumber) {
        findIndex2 = policyNumberDocument.nonShieldPolicyNumber.findIndex(
          element => element.isUsed === false
        );
        if (findIndex2 === -1) {
          // cannot find true
          numOfNonShield = MAX_OF_NON_SHIELD_POLICY_NUM;
          yield put({
            type:
              APP_ACTION_TYPES[POLICYNUMBER]
                .NO_POLICY_NUMBER_AVAILABLE_NON_SHIELD
          });
        } else if (findIndex2 === 0) {
          numOfNonShield = 0;
        } else if (
          policyNumberDocument.nonShieldPolicyNumber.length - findIndex2 >=
          MAX_OF_NON_SHIELD_POLICY_NUM
        ) {
          numOfNonShield = 0;
        } else {
          numOfNonShield =
            MAX_OF_NON_SHIELD_POLICY_NUM -
            (policyNumberDocument.nonShieldPolicyNumber.length - findIndex2);
        }
      }
    } else {
      // cannot get the result
      errorMsg = FAIL_GET_POLICY_NO_DOC;
      throw errorMsg;
    }
    if (numOfShield > 0 || numOfNonShield > 0) {
      const authToken = yield select(state => state.login.authToken);
      const environment = yield select(state => state.login.environment);
      const installationID = yield call(SInfo.getItem, INSTALLATION_ID, {});
      checkVersionAndDeviceResult = yield call(
        checkVersionAndDevice,
        authToken,
        installationID,
        environment
      );
      if (!checkVersionAndDeviceResult.hasError) {
        if (checkVersionAndDeviceResult.isValidVersionAndDevice) {
          // isValidVersionAndDevice = true --> ok
          // OK la
        } else {
          // isValidVersionAndDevice = false --> throw errorMsg
          errorMsg = checkVersionAndDeviceResult.errorMsg
            ? checkVersionAndDeviceResult.errorMsg
            : "Authorization Failed (Error 007)";
          // throw errorMsg;
        }
      } else {
        // throw 007
        errorMsg = checkVersionAndDeviceResult.errorMsg
          ? checkVersionAndDeviceResult.errorMsg
          : "Authorization Failed (Error 007)";
        // throw errorMsg;
      }
      // need to refill the pool of policy number
      getPolicyNumberResult = yield call(
        getPolicyNumber,
        authToken,
        numOfShield,
        numOfNonShield,
        environment
      );
      if (!getPolicyNumberResult.hasError) {
        if (
          getPolicyNumberResult.result.data &&
          getPolicyNumberResult.result.data.status === "success"
        ) {
          const document = getDocumentResult.result;
          // update isShieldPolicyNumber array in policyNumberPool
          if (document.shieldPolicyNumber) {
            if (getPolicyNumberResult.result.data.result.shieldPolicyNum) {
              const array =
                getPolicyNumberResult.result.data.result.shieldPolicyNum;
              for (let i = 0; i < array.length; i += 1) {
                const o = {
                  policyNum: array[i],
                  isUsed: false,
                  isSubmitted: false
                };
                document.shieldPolicyNumber.push(o);
              }
            }
          }
          // update nonShieldPolicyNumber array in policyNumberPool
          if (document.nonShieldPolicyNumber) {
            if (getPolicyNumberResult.result.data.result.nonShieldPolicyNum) {
              const array =
                getPolicyNumberResult.result.data.result.nonShieldPolicyNum;
              for (let j = 0; j < array.length; j += 1) {
                const p = {
                  policyNum: array[j],
                  isUsed: false,
                  isSubmitted: false
                };
                document.nonShieldPolicyNumber.push(p);
              }
            }
          }
          // document._rev = null;
          const documetId = document._id ? document._id : POLICY_NUM_DOCUMENT;

          // Call utilities to update policyNumberPool
          const updatePolicyNumberDocument = (docID, docData) =>
            new Promise(resolve => {
              updateDocumentWithID(docID, docData, callback => {
                if (callback.error && callback.error.length > 0) {
                  updateDocumentResult = {
                    hasError: true,
                    errorMsg: callback.error
                  };
                  resolve(updateDocumentResult);
                } else {
                  updateDocumentResult = {
                    hasError: false,
                    result: callback
                  };
                  resolve(updateDocumentResult);
                }
              });
            });
          yield call(updatePolicyNumberDocument, documetId, document);
          if (!updateDocumentResult.hasError) {
            // OK to update Policy Number Document
            yield put({
              type: APP_ACTION_TYPES[POLICYNUMBER].POLICY_NUMBER_DOC,
              policyNumberData: checkPolicyNumberResult.result
            });
          } else {
            // fail to update Policy Number Document
            const er = FAIL_TO_UPDATE_POLICY_NO_DOC;
            throw er;
          }
        } else {
          // do nothing
        }
        checkPolicyNumberResult = {
          hasError: false,
          result: null
        };
      } else if (getPolicyNumberResult.errorMsg === 500) {
        const er = POLICY_NO_EXHAUSTED;
        throw er;
      } else {
        // has error
        const er = FAIL_TO_GET_PN_FROM_API;
        throw er;
      }
    } else {
      // No need to call api to retrieve more policy number
      // Do Nothing
      checkPolicyNumberResult = {
        hasError: false,
        errorType: POLICY_NUMBER_ERROR,
        result: null
      };
    }
    // yield put({
    //   type: ACTION_TYPES[CONFIG].FINISH_LOADING
    // });
    action.callback(checkPolicyNumberResult);
  } catch (e) {
    const checkPolicyNumberResult = {
      hasError: true,
      errorType: POLICY_NUMBER_ERROR,
      errorMsg: e
    };
    yield put({
      type: ACTION_TYPES[CONFIG].FINISH_LOADING
    });
    action.callback(checkPolicyNumberResult);
  }
}

export function* showPolicyNumber(action) {
  let showPolicyNumberResult = {};
  const { callback } = action;
  try {
    yield put({
      type: ACTION_TYPES[CONFIG].START_LOADING
    });
    let getPolicyNumberDocumentResult = {};
    let policyNumberDocument = null;
    const jsonName = POLICY_NUM_DOCUMENT;
    const getPolicyNumberDocumentWithID = docID =>
      new Promise(resolve => {
        getDocumentWithID(docID, callbackResult => {
          if (callbackResult.error && callbackResult.error.length > 0) {
            getPolicyNumberDocumentResult = {
              hasError: true,
              errorMsg: callbackResult.error
            };
          } else {
            getPolicyNumberDocumentResult = {
              hasError: false,
              result: callbackResult
            };
          }
          policyNumberDocument = getPolicyNumberDocumentResult.result;
          resolve(getPolicyNumberDocumentResult);
        });
      });
    // mandy fix
    yield call(getPolicyNumberDocumentWithID, jsonName);
    if (
      policyNumberDocument &&
      policyNumberDocument.shieldPolicyNumber &&
      policyNumberDocument.nonShieldPolicyNumber
    ) {
      const lengthOfshieldPolicyNumber =
        policyNumberDocument.shieldPolicyNumber.length;
      const lengthOfNonShieldPolicyNumber =
        policyNumberDocument.nonShieldPolicyNumber.length;
      let countOfShieldPolicyNumber = 0;
      let countOfNonShieldPolicyNumber = 0;
      for (let i = 0; i < lengthOfshieldPolicyNumber; i += 1) {
        const element = policyNumberDocument.shieldPolicyNumber[i];
        if (!element.isUsed) {
          countOfShieldPolicyNumber += 1;
        }
      }
      for (let j = 0; j < lengthOfNonShieldPolicyNumber; j += 1) {
        const element = policyNumberDocument.nonShieldPolicyNumber[j];
        if (!element.isUsed) {
          countOfNonShieldPolicyNumber += 1;
        }
      }
      showPolicyNumberResult = {
        hasError: false,
        result: {
          countOfShieldPolicyNumber,
          countOfNonShieldPolicyNumber
        }
      };
    } else {
      showPolicyNumberResult = {
        hasError: true,
        errorType: POLICY_NUMBER_ERROR,
        errorMsg: "Cannot Get the Policy Number Document"
      };
    }
    yield put({
      type: ACTION_TYPES[CONFIG].FINISH_LOADING
    });
    callback(showPolicyNumberResult);
  } catch (e) {
    showPolicyNumberResult = {
      hasError: true,
      errorType: POLICY_NUMBER_ERROR,
      errorMsg: e
    };
    yield put({
      type: ACTION_TYPES[CONFIG].FINISH_LOADING
    });
    callback(showPolicyNumberResult);
  }
}

export function* applyPolicyNumber(action) {
  try {
    let applyPolicyNumberResult = {};
    const { isShield } = action;
    const { numOfShield } = action;
    let policyDocumentFound = false;
    let policyNumberPool = yield select(
      state => state.policynumber.policyNumber
    );

    const getDocumentWithIDPromise = (type, docId) =>
      new Promise(resolve => {
        getDocumentWithID(docId, callback => {
          if (callback.error && callback.error.length > 0) {
            // not found
            if (type === POLICY_NUM_DOCUMENT) {
              policyNumberPool = null;
            } else if (type === POLICY_DOCUMENT) {
              policyDocumentFound = policyDocumentFound || false;
            }
            resolve();
          } else if (type === POLICY_NUM_DOCUMENT) {
            policyNumberPool = callback;
            resolve();
          } else if (type === POLICY_DOCUMENT) {
            policyDocumentFound = true;
          }
          resolve();
        });
      });
    yield call(
      getDocumentWithIDPromise,
      POLICY_NUM_DOCUMENT,
      POLICY_NUM_DOCUMENT
    );
    if (
      !policyNumberPool ||
      _.isEmpty(policyNumberPool) ||
      (isShield &&
        (!policyNumberPool.shieldPolicyNumber ||
          _.isEmpty(policyNumberPool.shieldPolicyNumber))) ||
      (!isShield &&
        (!policyNumberPool.nonShieldPolicyNumber ||
          _.isEmpty(policyNumberPool.nonShieldPolicyNumber)))
    ) {
      applyPolicyNumberResult = {
        hasError: true,
        errorType: POLICY_NUMBER_ERROR,
        errorMsg: NO_POLICY_NUMBER_AVAILABLE
      };
    }

    if (!isShield) {
      const getPolicyNumberAndMarkUsed = () => {
        const { nonShieldPolicyNumber } = policyNumberPool;
        const policyNumberIndex = nonShieldPolicyNumber.findIndex(
          policyNumber => policyNumber.isUsed === false
        );
        if (policyNumberIndex !== -1) {
          policyNumberPool.nonShieldPolicyNumber[
            policyNumberIndex
          ].isUsed = true;
          return {
            hasError: false,
            result: {
              nonShieldPolicyNum: [
                policyNumberPool.nonShieldPolicyNumber[policyNumberIndex]
                  .policyNum
              ],
              shieldPolicyNum: []
            }
          };
        }
        return {
          hasError: true,
          errorType: POLICY_NUMBER_ERROR,
          errorMsg: NO_POLICY_NUMBER_AVAILABLE
        };
      };
      let count = 0;
      while (1) {
        applyPolicyNumberResult = getPolicyNumberAndMarkUsed();
        if (!applyPolicyNumberResult.hasError) {
          // set policyDocumentFound to false or true
          yield call(
            getDocumentWithIDPromise,
            POLICY_DOCUMENT,
            applyPolicyNumberResult.result.nonShieldPolicyNum[0]
          );
          if (!policyDocumentFound) {
            break;
          }
          if (policyDocumentFound && count === 1) {
            applyPolicyNumberResult = {
              hasError: true,
              errorType: POLICY_NUMBER_ERROR,
              errorMsg: NO_POLICY_NUMBER_AVAILABLE
            };
            break;
          }
          count += 1;
        } else {
          break;
        }
      }
    } else {
      const getPolicyNumberAndMarkUsedForShield = () => {
        const poolIndex = [];
        const shieldPNResult = [];
        for (
          let i = 0;
          i < policyNumberPool.shieldPolicyNumber.length;
          i += 1
        ) {
          if (!policyNumberPool.shieldPolicyNumber[i].isUsed) {
            poolIndex.push(i);
          }
        }
        if (_.isEmpty(poolIndex)) {
          return {
            hasError: true,
            errorType: POLICY_NUMBER_ERROR,
            errorMsg: NO_POLICY_NUMBER_AVAILABLE
          };
        } else if (poolIndex.length < numOfShield) {
          // not enough shield policy number
          return {
            hasError: true,
            errorType: POLICY_NUMBER_ERROR,
            errorMsg: NO_POLICY_NUMBER_AVAILABLE
          };
        }
        poolIndex.some((value, index) => {
          if (index === numOfShield) {
            return true;
          }
          policyNumberPool.shieldPolicyNumber[value].isUsed = true;
          shieldPNResult.push(
            policyNumberPool.shieldPolicyNumber[value].policyNum
          );
          return false;
        });

        if (shieldPNResult.length) {
          policyDocumentFound = false;
          return {
            hasError: false,
            result: {
              nonShieldPolicyNum: [],
              shieldPolicyNum: shieldPNResult
            }
          };
        }

        return {
          hasError: true,
          errorType: POLICY_NUMBER_ERROR,
          errorMsg: NO_POLICY_NUMBER_AVAILABLE
        };
      };

      let count = 0;
      while (1) {
        applyPolicyNumberResult = getPolicyNumberAndMarkUsedForShield();
        if (!applyPolicyNumberResult.hasError) {
          for (
            let i = 0;
            i < applyPolicyNumberResult.result.shieldPolicyNum.length;
            i += 1
          ) {
            yield call(
              getDocumentWithIDPromise,
              POLICY_DOCUMENT,
              applyPolicyNumberResult.result.shieldPolicyNum[i]
            );
          }
          // set policyDocumentFound to false or true
          if (!policyDocumentFound) {
            break;
          }
          if (policyDocumentFound && count === 1) {
            applyPolicyNumberResult = {
              hasError: true,
              errorType: POLICY_NUMBER_ERROR,
              errorMsg: NO_POLICY_NUMBER_AVAILABLE
            };
            break;
          }
          count += 1;
        } else {
          break;
        }
      }
    }
    // Call utilities to update policyNumberPool
    const updatePolicyNumberDocument = docData =>
      new Promise(resolve => {
        updateDocumentWithID(POLICY_NUM_DOCUMENT, docData, callback => {
          if (callback.ok) {
            // update Document ok
            resolve();
          } else {
            applyPolicyNumberResult = {
              hasError: true,
              errorMsg: FAIL_TO_UPDATE_POLICY_NO_DOC
            };
            resolve();
          }
        });
      });
    yield call(updatePolicyNumberDocument, policyNumberPool);
    action.callback(applyPolicyNumberResult);
  } catch (e) {
    const applyPolicyNumberResult = {
      hasError: true,
      errorType: POLICY_NUMBER_ERROR,
      errorMsg: e
    };
    action.callback(applyPolicyNumberResult);
  }
}

// old version of applyPolicyNumber()
// export function* applyPolicyNumber(action) {
//   try {
//     const shieldPNResult = [];
//     const shieldPNOutput = [];
//     const nonShieldPNResult = [];
//     const nonShieldPNOutput = [];
//     let getPolicyNumberDocumentResult = {};
//     let policyNumberDocument = null;
//     let applyPolicyNumberResult = {};
//     let updatePolicyNumberDocumenbtResult = {};
//     const { isShield, numOfShield, numOfNonShield } = action;
//     const jsonName = POLICY_NUM_DOCUMENT;
//     policyNumberDocument = yield select(
//       state => state.policynumber.policyNumber
//     );
//     const getPolicyNumberDocumentWithID = docID =>
//       new Promise(resolve => {
//         getDocumentWithID(docID, callback => {
//           if (callback.error && callback.error.length > 0) {
//             getPolicyNumberDocumentResult = {
//               hasError: true,
//               errorMsg: callback.error
//             };
//           } else {
//             getPolicyNumberDocumentResult = {
//               hasError: false,
//               result: callback
//             };
//           }
//           policyNumberDocument = getPolicyNumberDocumentResult.result;
//           resolve(getPolicyNumberDocumentResult);
//         });
//       });
//     // mandy fix
//     yield call(getPolicyNumberDocumentWithID, jsonName);
//     if (
//       policyNumberDocument &&
//       policyNumberDocument.shieldPolicyNumber &&
//       policyNumberDocument.nonShieldPolicyNumber
//     ) {
//       // Get Document from CB DB given the name of policy numnber json
//       // mandy fix
//       // yield call(getPolicyNumberDocumentWithID, jsonName);
//     } else {
//       applyPolicyNumberResult = {
//         hasError: true,
//         errorMsg: NO_POLICY_NUMBER_AVAILABLE
//       };
//     }
//     if (
//       policyNumberDocument &&
//       policyNumberDocument.shieldPolicyNumber &&
//       policyNumberDocument.nonShieldPolicyNumber
//     ) {
//       if (!isShield) {
//         // not shield application
//         for (let i = 0; i < numOfNonShield; i += 1) {
//           let tmp = policyNumberDocument.nonShieldPolicyNumber;
//           let findIndex1 = tmp.findIndex(element => element.isUsed === false);
//           nonShieldPNResult.push(findIndex1);
//           if (findIndex1 === -1) {
//             // do nothing
//           } else {
//             // update the document
//             let element =
//               policyNumberDocument.nonShieldPolicyNumber[findIndex1];
//             element.isUsed = true;

//             // reset values
//             element = null;
//           }
//           // reset values
//           findIndex1 = 0; // reset to zero
//           tmp = null;
//         }
//         const findIndex2 = nonShieldPNResult.findIndex(
//           element => element === -1
//         );
//         if (findIndex2 === -1) {
//           // pass the check
//           for (let j = 0; j < nonShieldPNResult.length; j += 1) {
//             const element =
//               policyNumberDocument.nonShieldPolicyNumber[nonShieldPNResult[j]];
//             const num = element.policyNum;
//             nonShieldPNOutput.push(num);
//           }
//           applyPolicyNumberResult = {
//             hasError: false,
//             errorType: POLICY_NUMBER_ERROR,
//             result: {
//               nonShieldPolicyNum: nonShieldPNOutput,
//               shieldPolicyNum: shieldPNOutput
//             }
//           };
//         } else {
//           // checkPolicyNumber
//           applyPolicyNumberResult = {
//             hasError: true,
//             errorType: POLICY_NUMBER_ERROR,
//             errorMsg: NO_POLICY_NUMBER_AVAILABLE
//           };
//         }
//       } else {
//         // shield application
//         for (let i = 0; i < numOfShield; i += 1) {
//           let findIndex1 = policyNumberDocument.shieldPolicyNumber.findIndex(
//             element => element.isUsed === false
//           );
//           shieldPNResult.push(findIndex1);
//           if (findIndex1 === -1) {
//             // do nothing
//           } else {
//             // update the document
//             let element = policyNumberDocument.shieldPolicyNumber[findIndex1];
//             element.isUsed = true;

//             // reset values
//             element = null;
//           }
//           // reset values
//           findIndex1 = 0;
//         }
//         const findIndex2 = shieldPNResult.findIndex(element => element === -1);
//         if (findIndex2 === -1) {
//           // pass the check
//           for (let j = 0; j < shieldPNResult.length; j += 1) {
//             const element =
//               policyNumberDocument.shieldPolicyNumber[shieldPNResult[j]];
//             const num = element.policyNum;
//             shieldPNOutput.push(num);
//           }
//           applyPolicyNumberResult = {
//             hasError: false,
//             errorType: POLICY_NUMBER_ERROR,
//             result: {
//               nonShieldPolicyNum: nonShieldPNOutput,
//               shieldPolicyNum: shieldPNOutput
//             }
//           };
//         } else {
//           // checkPolicyNumber
//           applyPolicyNumberResult = {
//             hasError: true,
//             errorType: POLICY_NUMBER_ERROR,
//             errorMsg: NO_POLICY_NUMBER_AVAILABLE
//           };
//         }
//       }
//       // Call utilities to update policyNumberPool
//       const updatePolicyNumberDocument = (docID, docData) =>
//         new Promise(resolve => {
//           updateDocumentWithID(docID, docData, callback => {
//             if (callback.ok) {
//               // update Document ok
//               updatePolicyNumberDocumenbtResult = {
//                 hasError: false,
//                 result: callback
//               };
//               resolve(updatePolicyNumberDocumenbtResult);
//             } else {
//               updatePolicyNumberDocumenbtResult = {
//                 hasError: true,
//                 error: FAIL_TO_UPDATE_POLICY_NO_DOC
//               };
//               resolve(updatePolicyNumberDocumenbtResult);
//             }
//           });
//         });
//       yield call(updatePolicyNumberDocument, jsonName, policyNumberDocument);
//       if (updatePolicyNumberDocumenbtResult.hasError) {
//         // update Document not ok
//         const er = FAIL_TO_UPDATE_POLICY_NO_DOC;
//         throw er;
//       }
//     } else {
//       // fail to get policy number document
//       const er = FAIL_GET_POLICY_NO_DOC;
//       throw er;
//     }
//     action.callback(applyPolicyNumberResult);
//   } catch (e) {
//     const applyPolicyNumberResult = {
//       hasError: true,
//       errorType: POLICY_NUMBER_ERROR,
//       errorMsg: e
//     };
//     action.callback(applyPolicyNumberResult);
//   }
// }

export function* counterCheckPolicyNumber(action) {
  try {
    yield put({
      type: ACTION_TYPES[CONFIG].START_LOADING
    });
    let updateDocumentResult = {};
    let getPolicyNumberDocumentResult = {};
    let policyNumberDocument = null;
    const { isShield, shieldPolicyNum, nonShieldPolicyNum } = action;
    const jsonName = POLICY_NUM_DOCUMENT;
    const getPolicyNumberDocumentWithID = docID =>
      new Promise(resolve => {
        getDocumentWithID(docID, callback => {
          if (callback.error && callback.error.length > 0) {
            getPolicyNumberDocumentResult = {
              hasError: true,
              errorMsg: callback.error
            };
          } else {
            getPolicyNumberDocumentResult = {
              hasError: false,
              result: callback
            };
          }
          policyNumberDocument = getPolicyNumberDocumentResult.result;
          resolve(getPolicyNumberDocumentResult);
        });
      });
    // mandy fix
    yield call(getPolicyNumberDocumentWithID, jsonName);
    if (
      policyNumberDocument &&
      policyNumberDocument.shieldPolicyNumber &&
      policyNumberDocument.nonShieldPolicyNumber
    ) {
      if (!isShield) {
        // not shield application
        const nonShieldArray = policyNumberDocument.nonShieldPolicyNumber;
        nonShieldPolicyNum.forEach(targetElment => {
          nonShieldArray.forEach(element => {
            if (element.policyNum === targetElment) {
              element.isUsed = true;
            }
          });
        });
      } else {
        // shield application
        const shieldArray = policyNumberDocument.shieldPolicyNumber;
        shieldPolicyNum.forEach(targetElment => {
          shieldArray.forEach(element => {
            if (element.policyNum === targetElment) {
              element.isUsed = true;
            }
          });
        });
      }
      // Call utilities to update policyNumberPool
      const updatePolicyNumberDocument = (docID, docData) =>
        new Promise(resolve => {
          updateDocumentWithID(docID, docData, callback => {
            if (callback.error && callback.error.length > 0) {
              updateDocumentResult = {
                hasError: true,
                errorMsg: callback.error
              };
              resolve(updateDocumentResult);
            } else {
              updateDocumentResult = {
                hasError: false,
                result: callback
              };
              resolve(updateDocumentResult);
            }
          });
        });
      yield call(updatePolicyNumberDocument, jsonName, policyNumberDocument);
      if (updatePolicyNumberDocument.hasError) {
        const er = FAIL_TO_UPDATE_POLICY_NO_DOC;
        throw er;
      }
    } else {
      // fail to get policy number document
      const er = FAIL_GET_POLICY_NO_DOC;
      throw er;
    }
    yield put({
      type: ACTION_TYPES[CONFIG].FINISH_LOADING
    });
  } catch (e) {
    yield put({
      type: ACTION_TYPES[CONFIG].FINISH_LOADING
    });
  }
}

export function* submitPolicyNumber(action) {
  try {
    yield put({
      type: ACTION_TYPES[CONFIG].START_LOADING
    });
    let policyNumberDocument = null;
    let getPolicyNumberDocumentResult = null;
    let submitPolicyNumberResult = {};
    let updateDocumentResult = {};
    const { isShield, shieldPNIntput, nonShieldPNIntput } = action;
    const jsonName = POLICY_NUM_DOCUMENT;
    policyNumberDocument = yield select(
      state => state.policynumber.policyNumber
    );
    if (!policyNumberDocument) {
      // Get Document from CB DB given the name of policy numnber json
      const getPolicyNumberDocumentWithID = docID =>
        new Promise(resolve => {
          getDocumentWithID(docID, callback => {
            if (callback.error && callback.error.length > 0) {
              getPolicyNumberDocumentResult = {
                hasError: true,
                errorMsg: callback.error
              };
            } else {
              getPolicyNumberDocumentResult = {
                hasError: false,
                result: callback
              };
            }
            resolve(policyNumberDocument);
          });
        });
      yield call(getPolicyNumberDocumentWithID, jsonName);
      policyNumberDocument = getPolicyNumberDocumentResult.result;
    }
    if (
      policyNumberDocument.shieldPolicyNumber &&
      policyNumberDocument.nonShieldPolicyNumber
    ) {
      if (!isShield) {
        // not shield application
        for (let i = 0; i < nonShieldPNIntput.length; i += 1) {
          const tmp = policyNumberDocument.nonShieldPolicyNumber;
          const findIndex1 = tmp.findIndex(
            element => element.policyNum === nonShieldPNIntput[i]
          );
          const element =
            policyNumberDocument.nonShieldPolicyNumber[findIndex1];
          if (findIndex1 === -1) {
            // do something to handle
            policyNumberDocument.nonShieldPolicyNumber.push({
              policyNum: nonShieldPNIntput[i],
              isUsed: true,
              isSubmitted: true
            });
          } else {
            element.isUsed = true;
            element.isSubmitted = true;
          }
        }
      } else {
        // shield application
        for (let i = 0; i < shieldPNIntput.length; i += 1) {
          const findIndex1 = policyNumberDocument.shieldPolicyNumber.findIndex(
            element => element.policyNum === shieldPNIntput[i]
          );
          const element = policyNumberDocument.shieldPolicyNumber[findIndex1];
          if (findIndex1 === -1) {
            // do something to handle
            policyNumberDocument.shieldPolicyNumber.push({
              policyNum: shieldPNIntput[i],
              isUsed: true,
              isSubmitted: true
            });
          } else {
            element.isUsed = true;
            element.isSubmitted = true;
          }
        }
      }
      // Call utilities to update policyNumberPool
      const updatePolicyNumberDocument = (docID, docData) =>
        new Promise(resolve => {
          updateDocumentWithID(docID, docData, callback => {
            if (callback.error && callback.error.length > 0) {
              updateDocumentResult = {
                hasError: true,
                errorMsg: callback.error
              };
              resolve(updateDocumentResult);
            } else {
              updateDocumentResult = {
                hasError: false,
                result: callback
              };
              resolve(updateDocumentResult);
            }
          });
        });
      yield call(updatePolicyNumberDocument, jsonName, policyNumberDocument);
      if (updatePolicyNumberDocument.hasError) {
        throw FAIL_TO_UPDATE_POLICY_NO_DOC;
      }
    } else {
      // fail to get policy number document
      throw FAIL_GET_POLICY_NO_DOC;
    }
    submitPolicyNumberResult = {
      hasError: false,
      errorType: POLICY_NUMBER_ERROR,
      result: {
        nonShieldPolicyNum: nonShieldPNIntput,
        shieldPolicyNum: shieldPNIntput
      }
    };
    yield put({
      type: ACTION_TYPES[CONFIG].FINISH_LOADING
    });
    action.callback(submitPolicyNumberResult);
  } catch (e) {
    const submitPolicyNumberResult = {
      hasError: true,
      errorType: POLICY_NUMBER_ERROR,
      errorMsg: e
    };
    yield put({
      type: ACTION_TYPES[CONFIG].FINISH_LOADING
    });
    action.callback(submitPolicyNumberResult);
  }
}

export default {
  initPolicyNumber,
  checkPolicyNumber,
  showPolicyNumber,
  applyPolicyNumber,
  submitPolicyNumber
};
