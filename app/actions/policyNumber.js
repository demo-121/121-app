// import { ACTION_TYPES, REDUCER_TYPES } from "eab-web-api";
import APP_ACTION_TYPES from "../constants/ACTION_TYPES";
import { POLICYNUMBER as APP_POLICYNUMBER } from "../constants/REDUCER_TYPES";
/**
 * checkPolicyNumber
 * @description check the Policy Numebr, refill number of policy number
 * @param {function} dispatch - redux dispatch function
 * @param {boolean} isShield - isShield Application
 * @param {function} callback - callback when the check is done.
 * */
export const initPolicyNumber = ({ dispatch, isShield, callback }) => {
  dispatch({
    type: APP_ACTION_TYPES[APP_POLICYNUMBER].INIT_POLICY_NUMBER,
    isShield,
    callback
  });
};

/**
 * checkPolicyNumber
 * @description check the Policy Numebr, refill number of policy number
 * @param {function} dispatch - redux dispatch function
 * @param {boolean} isShield - isShield Application
 * @param {function} callback - callback when the check is done.
 * */
export const checkPolicyNumber = ({ dispatch, callback }) => {
  dispatch({
    type: APP_ACTION_TYPES[APP_POLICYNUMBER].CHECK_POLICY_NUMBER,
    callback
  });
};

export const showPolicyNumber = ({ dispatch, callback }) => {
  dispatch({
    type: APP_ACTION_TYPES[APP_POLICYNUMBER].SHOW_POLICY_NUMBER,
    callback
  });
};

export const getPolicyNumber = ({
  dispatch,
  docId,
  applicationResult,
  callback
}) => {
  dispatch({
    type: APP_ACTION_TYPES[APP_POLICYNUMBER].GET_POLICY_NUMBER,
    docId,
    applicationResult,
    callback
  });
};

export const applyPolicyNumber = ({
  dispatch,
  isShield,
  numOfShield,
  numOfNonShield,
  callback
}) => {
  dispatch({
    type: APP_ACTION_TYPES[APP_POLICYNUMBER].APPLY_POLICY_NUMBER,
    isShield,
    numOfShield,
    numOfNonShield,
    callback
  });
};

export const submitPolicyNumber = ({
  dispatch,
  isShield,
  shieldPNIntput,
  nonShieldPNIntput,
  callback
}) => {
  dispatch({
    type: APP_ACTION_TYPES[APP_POLICYNUMBER].SUBMIT_POLICY_NUMBER,
    isShield,
    shieldPNIntput,
    nonShieldPNIntput,
    callback
  });
};
