import axios from "axios";
import DeviceInfo from "react-native-device-info";
import Config from "react-native-config";
import moment from "moment";
import SInfo from "react-native-sensitive-info";
import { Platform, NativeModules, AsyncStorage } from "react-native";
import { put, call, select } from "redux-saga/effects";
import { CouchbaseMgr } from "eab-core-api";
import { REDUCER_TYPES, ACTION_TYPES } from "eab-web-api";
import {
  fetchAuthToken,
  // fetchSessionCookie,
  verifyUserInfo, // yes
  updateMasterKeyToServer,
  verifyUser121Online,
  checkVersionAndDevice
} from "../apis/login";
import APP_ACTION_TYPES from "../constants/ACTION_TYPES";
import INFLIGHT_JOB_LIST, {
  INFLIGHT_JOB_LIST_FILE_NAME
} from "../constants/INFLIGHT_JOB_LIST";
import {
  INFLIGHT_JOB,
  LOGIN,
  PAYMENT,
  POLICYNUMBER
} from "../constants/REDUCER_TYPES";
import { aesGen256Key, aesEncrypt, aesDecrypt } from "../utilities/SecureUtils";
import {
  APP_PASSWORD_KEY,
  APP_PASSWORD_EXPIRY_DATE,
  SQLITE_ENCRYPT_KEY,
  INSTALLATION_ID,
  USER_ID,
  TIME_BOMB_EXPIRY_DATE,
  TOKEN_EXPIRY_DATE,
  LAST_DATA_SYNC_TIME,
  KC_VERSION_NUMBER,
  KC_BUILD_NUMBER,
  IS_APP_LOCKED
} from "../constants/ENCRYPTION_FIELD_NAMES";
import { POLICY_NUMBER_ERROR } from "../constants/ERROR_TYPE";
import { VERSION_NUMBER, BUILD_NUMBER } from "../constants/BUILD_CONFIG";
import { POLICY_NO_EXHAUSTED } from "../constants/ERROR_MSG";
import {
  FIRST_WARNING,
  SECOND_WARNING,
  TIME_BOMB,
  TYPE_FIRST_WARNING,
  TYPE_SECOND_WARNING,
  TYPE_TIME_BOMB,
  TYPE_SAFE,
  DURATION_FOR_APP_PASSWORD
} from "../constants/TIME_BOMB";
import {
  setEncryptionKey,
  genTestData,
  queryByExistingView,
  createDocument,
  getDocumentWithID,
  updateDocumentWithID,
  deleteDatabase,
  dataSync
} from "../utilities/DataManager";
import {
  resetSqliteDB,
  updateAfterResetPassword,
  tryLoginbyAppPassword,
  tryLoginbyOnlineAuth,
  isMobileActivated,
  saveCBViewVersion,
  getCBViewVersion,
  getEnvironment,
  getUserName,
  getLoginRecord,
  updateFailAttempt,
  updateSyncDocumentIds,
  getSyncDocumentIds,
  getFailAttempt
} from "../utilities/SQLiteUtils";

const { CONFIG } = REDUCER_TYPES;

const getTimeBombTriggerDays = environment => {
  let days = 0;
  days = Config[`${environment || "SIT3_EAB"}_TIME_BOMB_TRIGGER_DAYS`];
  return days;
};

/**
 * checkBuildInfo
 * @description check build number change.
 * */
export function* checkBuildInfo(callback) {
  let isBuildNoChanged = false;
  let isVersionNoChanged = false;
  let isImportRequired = false;
  try {
    yield put({
      type: ACTION_TYPES[CONFIG].START_LOADING
    });

    // get build number and version number from keychain
    const buildNumberKC = yield call(SInfo.getItem, KC_BUILD_NUMBER, {});
    const versionNumberKC = yield call(SInfo.getItem, KC_VERSION_NUMBER, {});

    // get build number and version number from build file
    const buildNumberBF = BUILD_NUMBER;
    const versionNumberBF = VERSION_NUMBER;

    if (buildNumberKC === buildNumberBF) {
      isBuildNoChanged = false;
    } else {
      isBuildNoChanged = true;
      // update KC
      // SInfo.setItem(KC_BUILD_NUMBER, buildNumberBF, {});
    }

    if (versionNumberKC === versionNumberBF) {
      isVersionNoChanged = false;
    } else {
      isVersionNoChanged = true;
      // update KC
      // SInfo.setItem(KC_VERSION_NUMBER, versionNumberBF, {});
    }

    if (isBuildNoChanged || isVersionNoChanged) {
      isImportRequired = true;
    } else {
      isImportRequired = false;
    }

    yield put({
      type: APP_ACTION_TYPES[LOGIN].IS_IMPORT_REQUIRED,
      isImportRequired
    });
    callback(isImportRequired);

    yield put({
      type: ACTION_TYPES[CONFIG].FINISH_LOADING
    });
  } catch (e) {
    isImportRequired = true;
    yield put({
      type: ACTION_TYPES[CONFIG].FINISH_LOADING
    });
    callback(isImportRequired);
  }
}

export function* checkTimezone() {
  let isTimezoneValid = true;
  try {
    let getTimeZone = DeviceInfo.getTimezone();
    const checkTimeZone = () =>
      new Promise(resolve => {
        const checkTimeZonefromNative = NativeModules.SignDocDialogManager;
        checkTimeZonefromNative.getTimezone(timezone => {
          getTimeZone = timezone.value;

          resolve(getTimeZone);
        });
      });
    yield call(checkTimeZone);
    if (
      getTimeZone.indexOf("Singapore") === -1 &&
      getTimeZone.indexOf("Hong_Kong") === -1 &&
      getTimeZone.indexOf("Shanghai") === -1
    ) {
      // block but not logout
      isTimezoneValid = false;
    } else {
      // passs
      isTimezoneValid = true;
    }
    yield put({
      type: APP_ACTION_TYPES[LOGIN].IS_TIMEZONE_VALID,
      isValid: isTimezoneValid
    });
  } catch (e) {
    isTimezoneValid = true;
    yield put({
      type: APP_ACTION_TYPES[LOGIN].IS_TIMEZONE_VALID,
      isValid: isTimezoneValid
    });
  }
}

/**
 * setAppLocked
 * @description set appLocked when activa
 * */
export function* setAppLocked(action) {
  try {
    yield put({
      type: ACTION_TYPES[CONFIG].START_LOADING
    });
    const storeDataTrue = async () => {
      try {
        await AsyncStorage.setItem(IS_APP_LOCKED, "true");
      } catch (error) {
        // Error retrieving data
      }
    };
    const storeDataFalse = async () => {
      try {
        await AsyncStorage.setItem(IS_APP_LOCKED, "false");
      } catch (error) {
        // Error retrieving data
      }
    };
    if (!action.isLocked) {
      yield call(storeDataFalse);
    } else {
      yield call(storeDataTrue);
    }

    yield put({
      type: ACTION_TYPES[CONFIG].FINISH_LOADING
    });
  } catch (e) {
    const storeDataFalse = async () => {
      try {
        await AsyncStorage.setItem(IS_APP_LOCKED, "false");
      } catch (error) {
        // Error saving data
      }
    };
    yield call(storeDataFalse);
    yield put({
      type: ACTION_TYPES[CONFIG].START_LOADING
    });
  }
}
/**
 * checkAppLocked
 * @description check if app is locked
 * */
export function* checkAppLocked(action) {
  const isAppActivationDone = yield select(
    state => state.login.isAppActivationDone
  );
  if (isAppActivationDone) {
    let isLocked = false;
    let isAppLockedString = null;
    try {
      yield put({
        type: ACTION_TYPES[CONFIG].START_LOADING
      });
      // const isAppLockedString = yield call(SInfo.getItem, IS_APP_LOCKED, {});
      const retrieveData = async () => {
        try {
          isAppLockedString = await AsyncStorage.getItem(IS_APP_LOCKED);
        } catch (error) {
          // Error retrieving data
        }
      };
      yield call(retrieveData);
      if (isAppLockedString && isAppLockedString === "true") {
        isLocked = true;
        yield put({
          type: APP_ACTION_TYPES[LOGIN].IS_APP_LOCKED,
          isLocked
        });
        yield put({
          type: ACTION_TYPES[CONFIG].FINISH_LOADING
        });
        action.callback(isLocked);
      } else {
        isLocked = false;
        yield put({
          type: APP_ACTION_TYPES[LOGIN].IS_APP_LOCKED,
          isLocked
        });
        yield put({
          type: ACTION_TYPES[CONFIG].FINISH_LOADING
        });
        action.callback(isLocked);
      }
    } catch (e) {
      isLocked = false;
      yield put({
        type: APP_ACTION_TYPES[LOGIN].IS_APP_LOCKED,
        isLocked
      });
      yield put({
        type: ACTION_TYPES[CONFIG].FINISH_LOADING
      });
      action.callback(isLocked);
    }
  } else {
    const isLocked = false;
    yield put({
      type: APP_ACTION_TYPES[LOGIN].IS_APP_LOCKED,
      isLocked
    });
    action.callback(isLocked);
  }
}
/**
 * checkTimeBomb
 * @description check if time bomb is triggered
 * */
export function* checkTimeBomb(action) {
  const isAppActivationDone = yield select(
    state => state.login.isAppActivationDone
  );
  if (isAppActivationDone) {
    let checkTimeBombResult = {};
    try {
      yield put({
        type: ACTION_TYPES[CONFIG].START_LOADING
      });
      const m = moment();
      const nowToMs = m.valueOf();
      const dataSyncTime = yield call(SInfo.getItem, LAST_DATA_SYNC_TIME, {});
      const dataSyncTimeToMS = Number(dataSyncTime);
      const w1 = moment(dataSyncTimeToMS);
      const w2 = moment(dataSyncTimeToMS);
      const w3 = moment(dataSyncTimeToMS);
      if (typeof dataSyncTimeToMS === "number") {
        const afterXDaysDateSync = w1.add(FIRST_WARNING, "day").valueOf();
        const afterYDaysDateSync = w2.add(SECOND_WARNING, "day").valueOf();
        const afterZDaysDateSync = w3.add(TIME_BOMB, "day").valueOf();
        const dayLeftForTimeBomb = Math.floor(
          (afterZDaysDateSync - nowToMs) / 86400000
        );
        if (Math.floor((afterXDaysDateSync - dataSyncTimeToMS) / 86400000))
          if (afterZDaysDateSync < nowToMs) {
            checkTimeBombResult = {
              hasError: false,
              result: TYPE_TIME_BOMB,
              daysLeft: 0
            };
            yield put({
              type: APP_ACTION_TYPES[LOGIN].IS_APP_LOCKED,
              isLocked: true
            });
            yield put({
              type: APP_ACTION_TYPES[LOGIN].SET_APP_LOCKED,
              isLocked: true
            });
            yield put({
              type: ACTION_TYPES[CONFIG].FINISH_LOADING
            });

            const timeBombTriggers = () =>
              new Promise(resolve => {
                deleteDatabase(result => {
                  resolve(result);
                });
              });

            yield call(timeBombTriggers);
            action.callback(checkTimeBombResult);
            // Also set this to keychain and reducer
          } else if (afterYDaysDateSync < nowToMs) {
            checkTimeBombResult = {
              hasError: false,
              result: TYPE_SECOND_WARNING,
              daysLeft: dayLeftForTimeBomb
            };
          } else if (afterXDaysDateSync < nowToMs) {
            checkTimeBombResult = {
              hasError: false,
              result: TYPE_FIRST_WARNING,
              daysLeft: dayLeftForTimeBomb
            };
          } else {
            checkTimeBombResult = {
              hasError: false,
              result: TYPE_SAFE,
              daysLeft: dayLeftForTimeBomb
            };
          }
      } else {
        checkTimeBombResult = {
          hasError: false,
          result: TYPE_SAFE,
          daysLeft: null
        };
      }
      yield put({
        type: ACTION_TYPES[CONFIG].FINISH_LOADING
      });
      action.callback(checkTimeBombResult);
    } catch (e) {
      checkTimeBombResult = {
        hasError: true,
        daysLeft: null,
        error: e,
        errorMsg: "Time Bomb Error"
      };
      yield put({
        type: ACTION_TYPES[CONFIG].FINISH_LOADING
      });
      action.callback(checkTimeBombResult);
    }
  } else {
    const checkTimeBombResult = {
      hasError: false,
      result: TYPE_SAFE,
      daysLeft: null
    };
    action.callback(checkTimeBombResult);
  }
}

export function* runView() {
  try {
    const agentCode = yield select(state => state.login.agentCode);
    const updateView = (emitViewNamed, params) =>
      new Promise(resolve => {
        queryByExistingView(emitViewNamed, params, result => {
          resolve(result);
        });
      });
    yield call(updateView, "contacts", null);
    yield call(updateView, "validbundleApplicationsByAgent", null);

    yield call(updateView, "validbundleApplicationsByAgent", {
      startKey: ["01", agentCode],
      endKey: ["01", agentCode]
    });
    yield call(updateView, "contacts", {
      startKey: ["01", agentCode],
      endKey: ["01", agentCode]
    });
    // yield call(updateView, "agentDetails", {
    //   startKey: null,
    //   keys: '["undefined","agentCode","undefined"]',
    //   endKey: null,
    //   ccy: "ALL"
    // });
    // yield call(updateView, "products", {
    //   startKey: '["01", "B", "0"]',
    //   endKey: '["01", "B", "ZZZ"]',
    //   ccy: "ALL"
    // });
    // yield call(updateView, "bundleApplications", {
    //   startKey: '["undefined", "application", "SUBMITTED", "undefined"]',
    //   endKey: '["undefined", "application", "SUBMITTED", "undefined"]'
    // });
  } catch (e) {
    // need error handling
  }
}

/**
 * initCallServerConfig
 * @description init config & create login session after user login successfully and couchbase lite replication completed
 * @param loginUserID: userID from loginByPassword or saveNewPassword
 * */
export function* initCallServerConfig(loginUserID, callback) {
  let initCallServerConfigResult = {};
  try {
    // yield put({
    //   type: ACTION_TYPES[CONFIG].START_LOADING
    // });
    let checkVersionAndDeviceResult = {};
    let error = "";
    let agentData = null;
    let agent = null;
    let userID = "";
    let profile = null;
    let getAgentDataResult = {};
    let getAgentDataUXResult = {};
    let getAgentDocumentResult = {};
    const authToken = yield select(state => state.login.authToken);
    const installationID = yield call(SInfo.getItem, INSTALLATION_ID, {});
    if (loginUserID != null && typeof loginUserID === "string") {
      userID = loginUserID;
    } else {
      userID = yield select(state => state.login.userID);
    }
    // Get the sqliteEncryptionKey from keychain
    const sqliteEncryptionKey = yield call(
      SInfo.getItem,
      SQLITE_ENCRYPT_KEY,
      {}
    );
    // check timezone
    yield call(checkTimezone);
    // Get environment from SQLiteDB
    let environmentGet = "";
    if (Config.ENV_NAME === "PROD") {
      environmentGet = "PROD";
    } else {
      environmentGet = yield call(getEnvironment, sqliteEncryptionKey);
    }
    let url = "";
    let baseURL = "";
    const loginFlowCheck = yield select(state => state.login.loginFlowCheck);
    if (Config.ENV_NAME === "PROD") {
      url = Config.GET_AGENT_URL;
      baseURL = Config.WEB_SERVICE_BASIC_URL;
    } else if (environmentGet && environmentGet.length > 0) {
      if (environmentGet === "DEV") {
        url = Config.DEV_GET_AGENT_URL;
        baseURL = Config.DEV_WEB_SERVICE_BASIC_URL;
      } else if (environmentGet === "SIT1_EAB") {
        url = Config.SIT1_EAB_GET_AGENT_URL;
        baseURL = Config.SIT1_EAB_WEB_SERVICE_BASIC_URL;
      } else if (environmentGet === "SIT2_EAB") {
        url = Config.SIT2_EAB_GET_AGENT_URL;
        baseURL = Config.SIT2_EAB_WEB_SERVICE_BASIC_URL;
      } else if (environmentGet === "SIT3_EAB") {
        url = Config.SIT3_EAB_GET_AGENT_URL;
        baseURL = Config.SIT3_EAB_WEB_SERVICE_BASIC_URL;
      } else if (environmentGet === "SIT_AXA") {
        url = Config.SIT_AXA_GET_AGENT_URL;
        baseURL = Config.SIT_AXA_WEB_SERVICE_BASIC_URL;
      } else if (environmentGet === "UAT") {
        url = Config.UAT_GET_AGENT_URL;
        baseURL = Config.UAT_WEB_SERVICE_BASIC_URL;
      } else if (environmentGet === "PRE_PROD") {
        url = Config.PRE_PROD_GET_AGENT_URL;
        baseURL = Config.PRE_PROD_WEB_SERVICE_BASIC_URL;
      } else {
        url = Config.SIT3_EAB_GET_AGENT_URL;
        baseURL = Config.SIT3_EAB_WEB_SERVICE_BASIC_URL;
      }
    } else {
      url = Config.SIT3_EAB_GET_AGENT_URL;
      baseURL = Config.SIT3_EAB_WEB_SERVICE_BASIC_URL;
    }

    const AuthStr = `Bearer ${authToken}`;
    // let aes256Key = "";
    const options = {
      method: "get",
      headers: { Authorization: AuthStr },
      baseURL,
      url
    };
    const getAgentData = () =>
      new Promise(resolve => {
        axios
          .request(options)
          .then(response => {
            agentData = response.data.result;
            agent = agentData.agentCode;
            profile = agentData.profileId;
            if (response.data.result) {
              getAgentDataResult = {
                hasError: false,
                result: response.data.result
              };
              resolve(response.data.result);
            } else {
              getAgentDataResult = {
                hasError: true,
                errorMsg: "Fail to get agent profile"
              };
            }
            resolve(getAgentDataResult);
          })
          .catch(e => {
            getAgentDataResult = {
              hasError: true,
              errorMsg: e
            };
            resolve(getAgentDataResult);
          });
      });

    const options2 = {
      method: "get",
      headers: { Authorization: AuthStr },
      baseURL,
      url: `${url}?type=agentUX`
    };
    const getAgentUXData = () =>
      new Promise(resolve => {
        axios
          .request(options2)
          .then(response => {
            if (response.data.result) {
              getAgentDataUXResult = {
                hasError: false,
                result: response.data.result
              };
            } else {
              getAgentDataUXResult = {
                hasError: true,
                errorMsg: "Fail to get agent UX profile"
              };
            }
            resolve(getAgentDataUXResult);
          })
          .catch(e => {
            getAgentDataUXResult = {
              hasError: true,
              errorMsg: e
            };
            resolve(getAgentDataUXResult);
          });
      });
    const agentDocumentId = `U_${userID}`;
    const getAgentDocument = docID =>
      new Promise(resolve => {
        getDocumentWithID(docID, result => {
          if (result.error && result.error.length > 0) {
            getAgentDocumentResult = {
              hasError: true,
              errorMsg: result.error
            };
          } else {
            getAgentDocumentResult = {
              hasError: false,
              result
            };
          }
          resolve(getAgentDocumentResult);
        });
      });
    if (
      environmentGet === "SIT1_EAB" ||
      environmentGet === "SIT2_EAB" ||
      environmentGet === "SIT3_EAB" ||
      environmentGet === "SIT_AXA" ||
      environmentGet === "UAT" ||
      environmentGet === "PRE_PROD" ||
      environmentGet === "PROD"
    ) {
      if (loginFlowCheck === "local_auth") {
        yield call(getAgentDocument, agentDocumentId);
        if (!getAgentDocumentResult.hasError) {
          agentData = getAgentDocumentResult.result;
          agent = agentData.agentCode;
          profile = agentData.profileId;
        } else {
          // To be handle if cannot find document
        }
      } else {
        checkVersionAndDeviceResult = yield call(
          checkVersionAndDevice,
          authToken,
          installationID,
          environmentGet
        );
        if (!checkVersionAndDeviceResult.hasError) {
          if (checkVersionAndDeviceResult.isValidVersionAndDevice) {
            // isValidVersionAndDevice = true --> ok
            // OK la
          } else {
            // isValidVersionAndDevice = false --> throw errorMsg
            error = checkVersionAndDeviceResult.errorMsg
              ? checkVersionAndDeviceResult.errorMsg
              : "Authorization Failed (Error 007)";
            throw error;
          }
        } else {
          // throw 007
          error = checkVersionAndDeviceResult.errorMsg
            ? checkVersionAndDeviceResult.errorMsg
            : "Authorization Failed (Error 007)";
          throw error;
        }
        yield call(getAgentData);
        yield call(getAgentUXData);
        if (!getAgentDataUXResult.hasError) {
          yield put({
            type: ACTION_TYPES[REDUCER_TYPES.AGENTUX].UPDATE_UX_AGENT_DATA,
            data: getAgentDataUXResult.result
          });
        }
      }
      yield put({
        type: APP_ACTION_TYPES[LOGIN].AGENTCODE,
        agent
      });
      yield put({
        type: APP_ACTION_TYPES[LOGIN].AGENTDATA,
        agentData
      });
      yield put({
        type: APP_ACTION_TYPES[LOGIN].PROFILEID,
        profile
      });
    } else {
      // DEV
      yield call(getAgentDocument, agentDocumentId);
      console.log("getAgentDocumentResult");
      console.log(getAgentDocumentResult);
      if (!getAgentDocumentResult.hasError) {
        agentData = getAgentDocumentResult.result;
        console.log("agentData");
        console.log(agentData);
        agent = agentData.agentCode;
        profile = agentData.profileId;
        yield put({
          type: APP_ACTION_TYPES[LOGIN].AGENTCODE,
          agent
        });
        yield put({
          type: APP_ACTION_TYPES[LOGIN].AGENTDATA,
          agentData
        });
        yield put({
          type: APP_ACTION_TYPES[LOGIN].PROFILEID,
          profile
        });
      }
    }
    if (sqliteEncryptionKey != null && sqliteEncryptionKey.trim().length > 0) {
      const cbViewVersion = yield call(getCBViewVersion, sqliteEncryptionKey);

      if (cbViewVersion !== CouchbaseMgr.viewVersionNum) {
        global.viewVersionNum = CouchbaseMgr.viewVersionNum || cbViewVersion;
        yield call(
          saveCBViewVersion,
          sqliteEncryptionKey,
          global.viewVersionNum
        );
      }
    }
    // init config from couchbase lite (update view)
    const initConfig = () =>
      new Promise(resolve => {
        CouchbaseMgr.init(() => {
          resolve();
        });
      });
    yield call(initConfig);
    const createDocSeq = (docID, docData) =>
      new Promise(resolve => {
        createDocument(docID, docData, () => {
          resolve();
        });
      });
    // create document seq
    const documentSeq = yield select(state => state[LOGIN].documentSeq);
    const docSeqData = {
      map: {
        [agent]: documentSeq
      },
      nextSeq: documentSeq
    };
    if (loginFlowCheck === "local_auth" || environmentGet === "DEV") {
      // no need to create
    } else {
      if (agentData._rev) {
        agentData._rev = null;
      }
      if (agentData._id) {
        const id = agentData._id ? agentData._id : `U_${userID}`;
        agentData._id = null;
        yield call(createDocSeq, id, agentData);
      }
      yield call(createDocSeq, "agentNumberMap", docSeqData);
      yield call(createDocSeq, INFLIGHT_JOB_LIST_FILE_NAME, INFLIGHT_JOB_LIST);
    }

    // get the system document and update it
    let getOriginalDocResult = {};
    let getNewDocResult = {};
    let updateSystemParamsDocResult = {};
    const getSystemParamsDoc = docID =>
      new Promise(resolve => {
        getDocumentWithID(docID, result => {
          if (result.error && result.error.length > 0) {
            getOriginalDocResult = {
              hasError: true,
              errorMsg: result.error
            };
          } else {
            getOriginalDocResult = {
              hasError: false,
              result
            };
          }
          resolve(getOriginalDocResult);
        });
      });
    const getSystemParamsDoc2 = docID =>
      new Promise(resolve => {
        getDocumentWithID(docID, result => {
          if (result.error && result.error.length > 0) {
            getNewDocResult = {
              hasError: true,
              errorMsg: result.error
            };
          } else {
            getNewDocResult = {
              hasError: false,
              result
            };
          }
          resolve(getNewDocResult);
        });
      });
    const updateSystemParamsDoc = (docID, docData) =>
      new Promise(resolve => {
        updateDocumentWithID(docID, docData, result => {
          if (result.error && result.error.length > 0) {
            updateSystemParamsDocResult = {
              hasError: true,
              errorMsg: result.error
            };
          } else {
            updateSystemParamsDocResult = {
              hasError: false,
              result
            };
          }
          resolve(updateSystemParamsDocResult);
        });
      });
    // add update sysParameter.json
    yield call(getSystemParamsDoc, "sysParameter");
    if (environmentGet === "PROD") {
      yield call(getSystemParamsDoc2, "sysParameter");
    } else if (environmentGet === "SIT1_EAB") {
      yield call(getSystemParamsDoc2, "sysParameter_SIT1_EAB");
    } else if (environmentGet === "SIT2_EAB") {
      yield call(getSystemParamsDoc2, "sysParameter_SIT2_EAB");
    } else if (environmentGet === "SIT3_EAB" || environmentGet === "DEV") {
      yield call(getSystemParamsDoc2, "sysParameter_SIT3_EAB");
    } else if (environmentGet === "SIT_AXA") {
      yield call(getSystemParamsDoc2, "sysParameter_SIT_AXA");
    } else if (environmentGet === "UAT") {
      yield call(getSystemParamsDoc2, "sysParameter_UAT");
    } else if (environmentGet === "PRE_PROD") {
      yield call(getSystemParamsDoc2, "sysParameter_PRE_PROD");
    }
    if (
      !getOriginalDocResult.hasError &&
      !getNewDocResult.hasError &&
      getOriginalDocResult.result &&
      getNewDocResult.result
    ) {
      const originalData = getOriginalDocResult.result;
      console.log("originalData");
      console.log(originalData);
      const newData = getNewDocResult.result;
      if (originalData._rev) {
        const rev = originalData._rev;
        const id = originalData._id;
        newData._rev = rev;
        newData._id = id;
      }
      yield call(updateSystemParamsDoc, "sysParameter", newData);
    }
    if (loginFlowCheck !== "local_auth") {
      yield put({
        type: APP_ACTION_TYPES[POLICYNUMBER].CHECK_POLICY_NUMBER,
        callback: result => {
          if (result.hasError) {
            if (
              result.errorType === POLICY_NUMBER_ERROR &&
              result.errorMsg === POLICY_NO_EXHAUSTED
            ) {
              initCallServerConfigResult = {
                hasError: true,
                errorType: POLICY_NUMBER_ERROR,
                errorMsg: POLICY_NO_EXHAUSTED
              };
              // callback(initCallServerConfigResult);
            } else {
              initCallServerConfigResult = {
                hasError: true,
                errorMsg: result.errorMsg
                  ? result.errorMsg
                  : "initCallServerConfig fails"
              };
              // callback(initCallServerConfigResult);
            }
          }
        }
      });
    }

    if (initCallServerConfigResult.hasError) {
      // Do nothing
    } else {
      initCallServerConfigResult = {
        hasError: false,
        result: null
      };
    }
    // yield put({
    //   type: ACTION_TYPES[CONFIG].FINISH_LOADING
    // });
    callback(initCallServerConfigResult);
  } catch (e) {
    initCallServerConfigResult = {
      hasError: true,
      errorMsg: e
    };
    yield put({ type: APP_ACTION_TYPES[LOGIN].LOGIN_FAIL });
    yield put({
      type: ACTION_TYPES[CONFIG].FINISH_LOADING
    });
    callback(initCallServerConfigResult);
  }
}

export function* initCallServerConfig2(callback) {
  let initCallServerConfig2Result = {};
  let agentData = {};
  try {
    const profileId = yield select(state => state.login.profileId);
    const callServer = yield select(state => state.config.callServer);
    global.session = { platform: Platform.OS };

    // Get the sqliteEncryptionKey from keychain
    const sqliteEncryptionKey = yield call(
      SInfo.getItem,
      SQLITE_ENCRYPT_KEY,
      {}
    );
    // Get environment from SQLiteDB
    let environmentGet = "";
    if (Config.ENV_NAME === "PROD") {
      environmentGet = "PROD";
    } else {
      environmentGet = yield call(getEnvironment, sqliteEncryptionKey);
    }
    if (environmentGet === "DEV") {
      const authResponse = yield call(callServer, {
        url: "auth",
        data: {
          action: "login",
          userName: "alex.tang"
        }
      });
      yield put({
        type: ACTION_TYPES[REDUCER_TYPES.AGENT].UPDATE_AGENT_DATA,
        data: authResponse.agent
      });
    } else if (
      environmentGet === "PROD" ||
      environmentGet === "SIT1_EAB" ||
      environmentGet === "SIT2_EAB" ||
      environmentGet === "SIT3_EAB" ||
      environmentGet === "SIT_AXA" ||
      environmentGet === "UAT" ||
      environmentGet === "PRE_PROD"
    ) {
      const authResponse = yield call(callServer, {
        url: "auth",
        data: {
          action: "login",
          userName: profileId
        }
      });
      if (authResponse.agent) {
        yield put({
          type: ACTION_TYPES[REDUCER_TYPES.AGENT].UPDATE_AGENT_DATA,
          data: authResponse.agent
        });
      } else {
        // get agent data from the state
        agentData = yield select(state => state.login.agentData);
        yield put({
          type: ACTION_TYPES[REDUCER_TYPES.AGENT].UPDATE_AGENT_DATA,
          data: agentData
        });
      }
    } else {
      agentData = yield select(state => state.login.agentData);
      yield put({
        type: ACTION_TYPES[REDUCER_TYPES.AGENT].UPDATE_AGENT_DATA,
        data: agentData
      });
    }
    initCallServerConfig2Result = {
      hasError: false,
      result: null
    };
    callback(initCallServerConfig2Result);
  } catch (e) {
    initCallServerConfig2Result = {
      hasError: true,
      errorMsg: e
    };
    yield put({ type: APP_ACTION_TYPES[LOGIN].LOGIN_FAIL });

    yield put({
      type: ACTION_TYPES[CONFIG].FINISH_LOADING
    });
    callback(initCallServerConfig2Result);
  }
}

export function* checkTokenExpiryDate(action) {
  try {
    // Get the appPasswordExpiryDate from keychain
    const tokenExpiryDate = yield call(SInfo.getItem, TOKEN_EXPIRY_DATE, {});
    const tokenExpiryDateToMS = Number(tokenExpiryDate);
    const today = moment(); // Get Today
    const todayToMS = today.valueOf(); // Get Today in terms of ms
    const loginFlowCheck = yield select(state => state.login.loginFlowCheck);
    const isTokenValidPre = yield select(state => state.login.isTokenValid);
    if (loginFlowCheck === "local_auth" && !isTokenValidPre) {
      yield put({
        type: APP_ACTION_TYPES[LOGIN].IS_TOKEN_VALID,
        isValid: false
      });
      action.callback(false);
    } else if (tokenExpiryDateToMS) {
      // compared with appPasswordExpiryDate and todayToMS in terms of ms
      if (todayToMS < tokenExpiryDateToMS) {
        // do
        yield put({
          type: APP_ACTION_TYPES[LOGIN].IS_TOKEN_VALID,
          isValid: true
        });
        action.callback(true);
      } else {
        yield put({
          type: APP_ACTION_TYPES[LOGIN].IS_TOKEN_VALID,
          isValid: false
        });
        action.callback(false);
      }
    } else {
      // if cannot get from keychain, bypass the checking on checkAppPasswordExpiryDate;
      // Do Nothing
      yield put({
        type: APP_ACTION_TYPES[LOGIN].IS_TOKEN_VALID,
        isValid: false
      });
      action.callback(false);
    }
    yield put({
      type: ACTION_TYPES[CONFIG].FINISH_LOADING
    });
  } catch (e) {
    yield put({
      type: ACTION_TYPES[CONFIG].FINISH_LOADING
    });
    yield put({
      type: APP_ACTION_TYPES[LOGIN].IS_TOKEN_VALID,
      isValid: false
    });
    action.callback(false);
  }
}

const getDeviceModelName = deviceId => {
  // This mapping should update after new device submit, especially ipad
  const deviceNamesByCode = {
    "iPod1,1": "iPod Touch",
    "iPod2,1": "iPod Touch (Second Generation)",
    "iPod3,1": "iPod Touch (Third Generation)",
    "iPod4,1": "iPod Touch (Fourth Generation)",
    "iPod5,1": "iPod Touch (Fifth Generation)",
    "iPod7,1": "iPod Touch (Sixth Generation)",
    "iPhone1,1": "iPhone",
    "iPhone1,2": "iPhone 3G",
    "iPhone2,1": "iPhone 3GS",
    "iPad1,1": "iPad",
    "iPad2,1": "iPad 2",
    "iPad2,2": "iPad 2",
    "iPad2,3": "iPad 2",
    "iPad2,4": "iPad 2",
    "iPad3,1": "iPad (3rd Generation)",
    "iPad3,2": "iPad (3rd Generation)",
    "iPad3,3": "iPad (3rd Generation)",
    "iPhone3,1": "iPhone 4 (GSM)",
    "iPhone3,2": "iPhone 4",
    "iPhone3,3": "iPhone 4 (CDMA/Verizon/Sprint)",
    "iPhone4,1": "iPhone 4S",
    "iPhone5,1": "iPhone 5 (model A1428, AT&T/Canada)",
    "iPhone5,2": "iPhone 5 (model A1429, everything else)",
    "iPad3,4": "iPad (4th Generation)",
    "iPad3,5": "iPad (4th Generation)",
    "iPad3,6": "iPad (4th Generation)",
    "iPad2,5": "iPad Mini",
    "iPad2,6": "iPad Mini",
    "iPad2,7": "iPad Mini",
    "iPhone5,3": "iPhone 5c (model A1456, A1532 | GSM)",
    "iPhone5,4":
      "iPhone 5c (model A1507, A1516, A1526 (China), A1529 | Global)",
    "iPhone6,1": "iPhone 5s (model A1433, A1533 | GSM)",
    "iPhone6,2":
      "iPhone 5s (model A1457, A1518, A1528 (China), A1530 | Global)",
    "iPhone7,1": "iPhone 6 Plus",
    "iPhone7,2": "iPhone 6",
    "iPhone8,1": "iPhone 6s",
    "iPhone8,2": "iPhone 6s Plus",
    "iPhone8,4": "iPhone SE",
    "iPhone9,1": "iPhone 7 (model A1660 | CDMA)",
    "iPhone9,3": "iPhone 7 (model A1778 | Global)",
    "iPhone9,2": "iPhone 7 Plus (model A1661 | CDMA)",
    "iPhone9,4": "iPhone 7 Plus (model A1784 | Global)",
    "iPhone10,3": "iPhone X (model A1865, A1902)",
    "iPhone10,6": "iPhone X (model A1901)",
    "iPhone10,1": "iPhone 8 (model A1863, A1906, A1907)",
    "iPhone10,4": "iPhone 8 (model A1905)",
    "iPhone10,2": "iPhone 8 Plus (model A1864, A1898, A1899)",
    "iPhone10,5": "iPhone 8 Plus (model A1897)",
    "iPhone11,2": "iPhone XS (model A2097, A2098)",
    "iPhone11,4": "iPhone XS Max (model A1921, A2103)",
    "iPhone11,6": "iPhone XS Max (model A2104)",
    "iPhone11,8": "iPhone XR (model A1882, A1719, A2105)",
    "iPad4,1": "5th Generation iPad (iPad Air) - Wifi",
    "iPad4,2": "5th Generation iPad (iPad Air) - Cellular",
    "iPad4,3": "5th Generation iPad (iPad Air)",
    "iPad4,4": "2nd Generation iPad Mini - Wifi",
    "iPad4,5": "2nd Generation iPad Mini - Cellular",
    "iPad4,6": "2nd Generation iPad Mini",
    "iPad4,7": "3rd Generation iPad Mini",
    "iPad4,8": "3rd Generation iPad Mini",
    "iPad4,9": "3rd Generation iPad Mini",
    "iPad5,1": "4th Generation iPad Mini",
    "iPad5,2": "4th Generation iPad Mini",
    "iPad5,3": "6th Generation iPad (iPad Air 2)",
    "iPad5,4": "6th Generation iPad (iPad Air 2)",
    "iPad6,3": "iPad Pro 9.7-inch",
    "iPad6,4": "iPad Pro 9.7-inch",
    "iPad6,7": "iPad Pro 12.9-inch",
    "iPad6,8": "iPad Pro 12.9-inch",
    "iPad7,1": "2nd Generation iPad Pro 12.5-inch - Wifi",
    "iPad7,2": "2nd Generation iPad Pro 12.5-inch - Cellular",
    "iPad7,3": "iPad Pro 10.5-inch - Wifi",
    "iPad7,4": "iPad Pro 10.5-inch - Cellular",
    "iPad7,5": "iPad (6th generation) - Wifi",
    "iPad7,6": "iPad (6th generation) - Cellular",
    "AppleTV2,1": "Apple TV (2nd Generation)",
    "AppleTV3,1": "Apple TV (3rd Generation)",
    "AppleTV3,2": "Apple TV (3rd Generation - Rev A)",
    "AppleTV5,3": "Apple TV (4th Generation)",
    "AppleTV6,2": "Apple TV 4K"
  };
  const value = deviceNamesByCode[deviceId];
  return value || deviceId;
};

const getDeviceInfo = () => {
  let ipAddress = "";
  let macAddress = "";
  let isPinOrFingerprintSet = "";
  DeviceInfo.getIPAddress().then(ip => {
    ipAddress = ip;
  });
  DeviceInfo.getMACAddress().then(mac => {
    macAddress = mac;
  });
  DeviceInfo.isPinOrFingerprintSet()(isPinOrFingerprintSetResult => {
    isPinOrFingerprintSet = isPinOrFingerprintSetResult;
  });
  const uuid = DeviceInfo.getUniqueID();
  const deviceModel = getDeviceModelName(DeviceInfo.getDeviceId());
  const deviceCountry = DeviceInfo.getDeviceCountry();
  const brand = DeviceInfo.getBrand();
  const carrier = DeviceInfo.getCarrier();
  const deviceLocale = DeviceInfo.getDeviceLocale();
  const deviceName = DeviceInfo.getDeviceName();
  const firstInstallTime = DeviceInfo.getFirstInstallTime();
  const fontScale = DeviceInfo.getFontScale();
  const freeDiskStorage = DeviceInfo.getFreeDiskStorage();
  const installReferrer = DeviceInfo.getInstallReferrer();
  const instanceID = DeviceInfo.getInstanceID();
  const manufacturer = DeviceInfo.getManufacturer();
  const maxMemory = DeviceInfo.getMaxMemory();
  const phoneNumber = DeviceInfo.getPhoneNumber();
  const serialNumber = DeviceInfo.getSerialNumber();
  const systemName = DeviceInfo.getSystemName();
  const systemVersion = DeviceInfo.getSystemVersion();
  const timezone = DeviceInfo.getTimezone();
  const storageSize = DeviceInfo.getTotalDiskCapacity();
  const totalMemory = DeviceInfo.getTotalMemory();
  const userAgent = DeviceInfo.getUserAgent();
  const is24Hour = DeviceInfo.is24Hour();
  const isEmulator = DeviceInfo.isEmulator();
  const isTablet = DeviceInfo.isTablet();
  const hasNotch = DeviceInfo.hasNotch();
  const isLandscape = DeviceInfo.isLandscape();
  const deviceType = DeviceInfo.getDeviceType();
  return {
    uuid,
    deviceModel,
    deviceCountry,
    brand,
    carrier,
    deviceLocale,
    deviceName,
    firstInstallTime,
    fontScale,
    freeDiskStorage,
    ipAddress,
    installReferrer,
    instanceID,
    macAddress,
    manufacturer,
    maxMemory,
    phoneNumber,
    serialNumber,
    systemName,
    systemVersion,
    timezone,
    storageSize,
    totalMemory,
    userAgent,
    is24Hour,
    isEmulator,
    isPinOrFingerprintSet,
    isTablet,
    hasNotch,
    isLandscape,
    deviceType
  };
};

/**
 * onlineAuthCode
 * @description call AXA API to get authToken by authCode
 * @param {object} action - reducer object
 * @param {string} action.authCode - authCode is provided from AXA
 * */
export function* onlineAuthCode(action) {
  try {
    let error = "";
    let errorResult = {};
    yield put({
      type: ACTION_TYPES[CONFIG].START_LOADING
    });
    const environment = yield select(state => state.login.environment);
    const isAppActivationDone = yield select(
      state => state.login.isAppActivationDone
    );
    const response = yield call(fetchAuthToken, action.authCode, environment);
    const result = { ...response.data };
    const loginFlow = yield select(state => state.login.loginFlow);

    if (loginFlow === "activation" && !isAppActivationDone) {
      // Some screen handling
      yield put({
        type: APP_ACTION_TYPES[LOGIN].DISABLE_APP_ACTIVATION_PAGE
      });
    }
    if (result.access_token && result.access_token.length > 0) {
      const authToken = result.access_token;
      const idToken = result.id_token;
      yield put({
        type: APP_ACTION_TYPES[LOGIN].AUTH_TOKEN,
        authToken
      });
      // couchbase sync gateway openid
      // yield put({
      //   type: APP_ACTION_TYPES[LOGIN].ID_TOKEN,
      //   idToken
      // });
      const m = moment();
      const add60Mins = m
        .add(60, "minutes")
        .valueOf()
        .toString();
      SInfo.setItem(TOKEN_EXPIRY_DATE, add60Mins, {});

      // Set IS_TOKEN_VALID to true
      yield put({
        type: APP_ACTION_TYPES[LOGIN].IS_TOKEN_VALID,
        isValid: true
      });

      if (loginFlow === "activation" && !isAppActivationDone) {
        const responseResult = yield call(
          verifyUserInfo,
          authToken,
          idToken,
          VERSION_NUMBER,
          BUILD_NUMBER,
          environment,
          getDeviceInfo()
        );
        if (!responseResult.hasError) {
          if (responseResult.result.result.isChangeDevice) {
            error =
              "By activating the app on this device, any un-synced data on a previously registered device will be lost. Please make sure you have synced the data on your other device before proceeding";
            throw error;
          }
          yield put({
            type: APP_ACTION_TYPES[LOGIN].UPDATE_DOCUMENT_SEQ,
            documentSeqData: responseResult.result.result.doc_seq
          });

          const sessionCookie = responseResult.result.result.cookie;
          const syncDocumentId = responseResult.result.result.syncDocumentIds;
          if (sessionCookie) {
            const variables = sessionCookie.split(";");
            // const expiresString = variables.find(variable =>
            //   variable.includes("Expires")
            // );
            const syncGatewaySessionString = variables.find(variable =>
              variable.includes("SyncGatewaySession")
            );
            const pathString = variables.find(variable =>
              variable.includes("Path")
            );
            // const [, sessionCookieExpiryDateString] = expiresString.split("=");
            const [
              ,
              sessionCookieSyncGatewaySession
            ] = syncGatewaySessionString.split("=");
            const [, sessionCookiePath] = pathString.split("=");

            // const sessionCookieExpiryDate = moment(
            //   sessionCookieExpiryDateString
            // ).valueOf();

            const sessionCookieExpiryDate = m.valueOf();

            yield put({
              type: APP_ACTION_TYPES[LOGIN].FETECH_SESSION_COOKIE,
              sessionCookie
            });
            yield put({
              type:
                APP_ACTION_TYPES[LOGIN]
                  .FETECH_SESSION_COOKIE_SYNC_GATEWAY_SESSION,
              sessionCookieSyncGatewaySession
            });
            yield put({
              type: APP_ACTION_TYPES[LOGIN].FETECH_SESSION_COOKIE_EXPIRY_DATE,
              sessionCookieExpiryDate
            });
            yield put({
              type: APP_ACTION_TYPES[LOGIN].FETECH_SESSION_COOKIE_PATH,
              sessionCookiePath
            });
          } else {
            error = "Cannot session Cookie";
            // throw error;
          }

          if (syncDocumentId) {
            yield put({
              type: APP_ACTION_TYPES[LOGIN].SYNC_DOCUMENT_IDS,
              syncDocumentId
            });
          } else {
            error = "Cannot sync document Ids";
            // throw error;
          }

          const userID = responseResult.result.result.user_id;
          const installationID = responseResult.result.result.installation_id;
          SInfo.setItem(INSTALLATION_ID, installationID, {});
          const todayMoment = moment()
            .hours(0)
            .minutes(0)
            .seconds(0)
            .milliseconds(0);
          const expiryDateVal = todayMoment
            .add(getTimeBombTriggerDays(environment), "days")
            .hours(0)
            .minutes(0)
            .seconds(0)
            .milliseconds(0)
            .utc()
            .valueOf();
          yield put({
            type: APP_ACTION_TYPES[LOGIN].VERIFY_DEVICE_SUCCESS,
            userID,
            lastLoginTime: moment.utc().valueOf(),
            timebombExpiryDate: expiryDateVal,
            authToken,
            installationID,
            idToken
          });
          action.callback(errorResult);
        } else if (responseResult.hasError && responseResult.error) {
          error = responseResult.error
            ? responseResult.error
            : "Fail to Verify User";
          throw error;
        }
      } else {
        const userID = yield call(SInfo.getItem, USER_ID, {});
        const installationID = yield call(SInfo.getItem, INSTALLATION_ID, {});
        const todayMoment = moment()
          .hours(0)
          .minutes(0)
          .seconds(0)
          .milliseconds(0);
        const expiryDateVal = todayMoment
          .add(getTimeBombTriggerDays(environment), "days")
          .hours(0)
          .minutes(0)
          .seconds(0)
          .milliseconds(0)
          .utc()
          .valueOf();
        yield put({
          type: APP_ACTION_TYPES[LOGIN].VERIFY_DEVICE_SUCCESS,
          userID,
          lastLoginTime: moment.utc().valueOf(),
          timebombExpiryDate: expiryDateVal,
          authToken,
          installationID,
          idToken
        });
        errorResult = {
          hasError: false,
          result: null
        };
        action.callback(errorResult);
      }
    } else {
      error = "Cannot get Auth Token";
      throw error;
    }
    yield put({
      type: ACTION_TYPES[CONFIG].FINISH_LOADING
    });
  } catch (e) {
    const errorResult = {
      hasError: true,
      error: e,
      errorMsg: "Authorization Failed (Error 001)"
    };
    yield put({ type: APP_ACTION_TYPES[LOGIN].LOGIN_FAIL });

    yield put({
      type: ACTION_TYPES[CONFIG].FINISH_LOADING
    });
    action.callback(errorResult);
  }
}

/**
 * onlineAuthCode2
 * @description call AXA API to get authToken by authCode
 * @param {object} action - reducer object
 * @param {string} action.authCode - authCode is provided from AXA
 * */
export function* onlineAuthCode2(action) {
  try {
    let error = "";
    let errorResult = {};
    yield put({
      type: ACTION_TYPES[CONFIG].START_LOADING
    });
    const environment = yield select(state => state.login.environment);
    const authToken = yield select(state => state.login.authToken);
    const idToken = yield select(state => state.login.idToken);
    const isAppActivationDone = yield select(
      state => state.login.isAppActivationDone
    );
    const loginFlow = yield select(state => state.login.loginFlow);

    if (loginFlow === "activation" && !isAppActivationDone) {
      // Some screen handling
      yield put({
        type: APP_ACTION_TYPES[LOGIN].DISABLE_APP_ACTIVATION_PAGE
      });
    }
    const m = moment();
    const add60Mins = m
      .add(60, "minutes")
      .valueOf()
      .toString();
    SInfo.setItem(TOKEN_EXPIRY_DATE, add60Mins, {});

    // Set IS_TOKEN_VALID to true
    yield put({
      type: APP_ACTION_TYPES[LOGIN].IS_TOKEN_VALID,
      isValid: true
    });

    if (loginFlow === "activation" && !isAppActivationDone) {
      const responseResult = yield call(
        verifyUserInfo,
        authToken,
        idToken,
        VERSION_NUMBER,
        BUILD_NUMBER,
        environment,
        getDeviceInfo()
      );
      if (!responseResult.hasError) {
        if (responseResult.result.result.isChangeDevice) {
          // do nothing
        }
        yield put({
          type: APP_ACTION_TYPES[LOGIN].UPDATE_DOCUMENT_SEQ,
          documentSeqData: responseResult.result.result.doc_seq
        });

        const sessionCookie = responseResult.result.result.cookie;
        const syncDocumentId = responseResult.result.result.syncDocumentIds;
        if (sessionCookie) {
          const variables = sessionCookie.split(";");
          // const expiresString = variables.find(variable =>
          //   variable.includes("Expires")
          // );
          const syncGatewaySessionString = variables.find(variable =>
            variable.includes("SyncGatewaySession")
          );
          const pathString = variables.find(variable =>
            variable.includes("Path")
          );
          // const [, sessionCookieExpiryDateString] = expiresString.split("=");
          const [
            ,
            sessionCookieSyncGatewaySession
          ] = syncGatewaySessionString.split("=");
          const [, sessionCookiePath] = pathString.split("=");
          // const sessionCookieExpiryDate = moment(
          //   sessionCookieExpiryDateString
          // ).valueOf();
          const sessionCookieExpiryDate = m.valueOf();

          yield put({
            type: APP_ACTION_TYPES[LOGIN].FETECH_SESSION_COOKIE,
            sessionCookie
          });
          yield put({
            type:
              APP_ACTION_TYPES[LOGIN]
                .FETECH_SESSION_COOKIE_SYNC_GATEWAY_SESSION,
            sessionCookieSyncGatewaySession
          });
          yield put({
            type: APP_ACTION_TYPES[LOGIN].FETECH_SESSION_COOKIE_EXPIRY_DATE,
            sessionCookieExpiryDate
          });
          yield put({
            type: APP_ACTION_TYPES[LOGIN].FETECH_SESSION_COOKIE_PATH,
            sessionCookiePath
          });
        } else {
          error = "Cannot session Cookie";
          // throw error;
        }

        if (syncDocumentId) {
          yield put({
            type: APP_ACTION_TYPES[LOGIN].SYNC_DOCUMENT_IDS,
            syncDocumentId
          });
        } else {
          error = "Cannot sync document Ids";
          // throw error;
        }

        const userID = responseResult.result.result.user_id;
        const installationID = responseResult.result.result.installation_id;
        SInfo.setItem(INSTALLATION_ID, installationID, {});
        const todayMoment = moment()
          .hours(0)
          .minutes(0)
          .seconds(0)
          .milliseconds(0);
        const expiryDateVal = todayMoment
          .add(getTimeBombTriggerDays(environment), "days")
          .hours(0)
          .minutes(0)
          .seconds(0)
          .milliseconds(0)
          .utc()
          .valueOf();
        yield put({
          type: APP_ACTION_TYPES[LOGIN].VERIFY_DEVICE_SUCCESS,
          userID,
          lastLoginTime: moment.utc().valueOf(),
          timebombExpiryDate: expiryDateVal,
          authToken,
          installationID,
          idToken
        });
        yield put({
          type: ACTION_TYPES[CONFIG].FINISH_LOADING
        });
        action.callback(errorResult);
      } else {
        error = "Fail to Verify User";
        throw error;
      }
    } else {
      const userID = yield call(SInfo.getItem, USER_ID, {});
      const installationID = yield call(SInfo.getItem, INSTALLATION_ID, {});
      const todayMoment = moment()
        .hours(0)
        .minutes(0)
        .seconds(0)
        .milliseconds(0);
      const expiryDateVal = todayMoment
        .add(getTimeBombTriggerDays(environment), "days")
        .hours(0)
        .minutes(0)
        .seconds(0)
        .milliseconds(0)
        .utc()
        .valueOf();
      yield put({
        type: APP_ACTION_TYPES[LOGIN].VERIFY_DEVICE_SUCCESS,
        userID,
        lastLoginTime: moment.utc().valueOf(),
        timebombExpiryDate: expiryDateVal,
        authToken,
        installationID,
        idToken
      });
      errorResult = {
        hasError: false,
        result: null
      };
      yield put({
        type: ACTION_TYPES[CONFIG].FINISH_LOADING
      });
      action.callback(errorResult);
    }
    yield put({
      type: ACTION_TYPES[CONFIG].FINISH_LOADING
    });
  } catch (e) {
    const errorResult = {
      hasError: true,
      error: e,
      errorMsg: "Authorization Failed (Error 001)"
    };
    yield put({ type: APP_ACTION_TYPES[LOGIN].LOGIN_FAIL });

    yield put({
      type: ACTION_TYPES[CONFIG].FINISH_LOADING
    });
    action.callback(errorResult);
  }
}

/**
 * checkValidDevice
 * @description store key encryption key
 * @param {string} action.appPassword - user key-in a app Password
 * */
export function* checkValidDevice(action) {
  let error = {};
  let checkVersionAndDeviceResult = {};
  let checkValidDeviceResult = {};
  let isValidDevice = true;
  try {
    yield put({
      type: ACTION_TYPES[CONFIG].START_LOADING
    });
    const installationID = yield call(SInfo.getItem, INSTALLATION_ID, {});
    const environment = yield select(state => state.login.environment);
    const authToken = yield select(state => state.login.authToken);
    checkVersionAndDeviceResult = yield call(
      checkVersionAndDevice,
      authToken,
      installationID,
      environment
    );
    if (!checkVersionAndDeviceResult.hasError) {
      if (checkVersionAndDeviceResult.isValidVersionAndDevice) {
        // isValidVersionAndDevice = true --> ok
        // OK la
        isValidDevice = true;
        checkValidDeviceResult = {
          hasError: false,
          isValidDevice,
          result: null
        };
      } else {
        // isValidVersionAndDevice = false --> throw errorMsg
        isValidDevice = false;
        checkValidDeviceResult = {
          hasError: true,
          isValidDevice,
          errorMsg: checkVersionAndDeviceResult.errorMsg
            ? checkVersionAndDeviceResult.errorMsg
            : "Authorization Failed (Error 007)"
        };
      }
    } else {
      error = checkVersionAndDeviceResult.errorMsg
        ? checkVersionAndDeviceResult.errorMsg
        : "Authorization Failed (Error 007)";
      throw error;
    }
    yield put({
      type: ACTION_TYPES[CONFIG].FINISH_LOADING
    });
    action.callback(checkValidDeviceResult);
  } catch (e) {
    isValidDevice = false;
    checkValidDeviceResult = {
      hasError: true,
      isValidDevice,
      errorMsg: e
    };
    yield put({
      type: ACTION_TYPES[CONFIG].FINISH_LOADING
    });
    action.callback(checkValidDeviceResult);
  }
}
/**
 * saveNewAppPassword
 * @description store key encryption key
 * @param {string} action.appPassword - user key-in a app Password
 * */
export function* saveNewAppPassword(action) {
  let errorResult = {};
  let error = {};
  let checkVersionAndDeviceResult = {};
  try {
    yield put({
      type: ACTION_TYPES[CONFIG].START_LOADING
    });
    const userID = yield select(state => state.login.userID);
    const lastLoginTime = yield select(state => state.login.lastLoginTime);
    let syncDocumentIds = yield select(state => state.login.syncDocumentIds);
    const installationID = yield call(SInfo.getItem, INSTALLATION_ID, {});
    const loginFlow = yield select(state => state.login.loginFlow);
    const environment = yield select(state => state.login.environment);
    const timebombExpiryDate = yield select(
      state => state.login.timebombExpiryDate
    );
    const authToken = yield select(state => state.login.authToken);
    const { appPassword } = action;
    if (loginFlow !== "activation") {
      const oldAppPassword = yield call(SInfo.getItem, APP_PASSWORD_KEY, {});
      if (oldAppPassword === appPassword) {
        error = "New password cannot be the same as user’s current password";
        throw error;
      }
    }
    checkVersionAndDeviceResult = yield call(
      checkVersionAndDevice,
      authToken,
      installationID,
      environment
    );
    if (!checkVersionAndDeviceResult.hasError) {
      if (checkVersionAndDeviceResult.isValidVersionAndDevice) {
        // isValidVersionAndDevice = true --> ok
        // OK la
      } else {
        // isValidVersionAndDevice = false --> throw errorMsg
        error = checkVersionAndDeviceResult.errorMsg
          ? checkVersionAndDeviceResult.errorMsg
          : "Authorization Failed (Error 007)";
        throw error;
      }
    } else {
      error = checkVersionAndDeviceResult.errorMsg
        ? checkVersionAndDeviceResult.errorMsg
        : "Authorization Failed (Error 007)";
      throw error;
    }
    const sqliteEncryptionKey = aesGen256Key();
    const m = moment();
    const add60Days = m
      .add(DURATION_FOR_APP_PASSWORD, "day")
      .valueOf()
      .toString();
    // store into keychain - app Password, SQLite encryption key
    SInfo.setItem(APP_PASSWORD_KEY, appPassword, {});
    SInfo.setItem(APP_PASSWORD_EXPIRY_DATE, add60Days, {});
    SInfo.setItem(SQLITE_ENCRYPT_KEY, sqliteEncryptionKey, {});
    SInfo.setItem(INSTALLATION_ID, installationID, {});
    SInfo.setItem(USER_ID, userID, {});
    SInfo.setItem(
      TIME_BOMB_EXPIRY_DATE,
      moment(timebombExpiryDate).toISOString(),
      {}
    );
    const masterKey = aesGen256Key();
    const encryptedMasterKey = aesEncrypt(masterKey, appPassword);
    // reset sqlite DB
    yield call(resetSqliteDB, {
      appPassword,
      encryptedMasterKey,
      sqliteEncryptionKey,
      userID,
      syncDocumentIds,
      lastLoginTime,
      timebombExpiryDate,
      installationID,
      environment
    });

    // store syncDocumentIds
    yield call(
      updateSyncDocumentIds,
      sqliteEncryptionKey,
      syncDocumentIds,
      userID
    );
    // Get UserID and failAttempt
    syncDocumentIds = yield call(getSyncDocumentIds, sqliteEncryptionKey);
    // create and encrypt couchbaseDB
    const initCouchbaseDB = encryptionKey =>
      new Promise(resolve => {
        setEncryptionKey(encryptionKey, true, () => {
          resolve();
        });
      });
    yield call(initCouchbaseDB, masterKey);
    const sleep = () => new Promise(resolve => setTimeout(resolve, 2000));
    yield call(sleep);

    const isBackuped = yield call(
      updateMasterKeyToServer,
      authToken,
      masterKey,
      installationID,
      environment
    );

    let agentData;

    if (isBackuped) {
      const testingData = () =>
        new Promise(resolve => {
          genTestData(() => {
            const buildNumberBF = BUILD_NUMBER;
            const versionNumberBF = VERSION_NUMBER;
            SInfo.setItem(KC_BUILD_NUMBER, buildNumberBF, {});
            SInfo.setItem(KC_VERSION_NUMBER, versionNumberBF, {});

            resolve();
          });
        });
      const isImportRequired = yield select(
        state => state.login.isImportRequired
      );
      if (isImportRequired || loginFlow === "activation") {
        yield call(testingData);
      }
      yield call(initCallServerConfig, userID, result => {
        if (result.hasError) {
          if (
            result.errorType === POLICY_NUMBER_ERROR &&
            result.errorMsg === POLICY_NO_EXHAUSTED
          ) {
            error = POLICY_NO_EXHAUSTED;
            errorResult = {
              hasError: true,
              errorType: POLICY_NUMBER_ERROR,
              errorMsg: error
            };
          } else {
            error = result.errorMsg
              ? result.errorMsg
              : "Fail in initCallServerConfig";
            throw error;
          }
        }
      });

      const profileId = yield select(state => state.login.profileId);
      agentData = yield select(state => state.login.agentData);
      const callServer = yield select(state => state.config.callServer);
      global.session = { platform: Platform.OS };
      yield call(callServer, {
        url: "auth",
        data: {
          action: "login",
          userName: profileId
        }
      });
      yield put({
        type: ACTION_TYPES[REDUCER_TYPES.AGENT].UPDATE_AGENT_DATA,
        data: agentData
      });
      yield put({
        type: APP_ACTION_TYPES[LOGIN].LOGIN_SUCCESS,
        userID,
        lastLoginTime: moment.utc().valueOf()
      });
    } else {
      error = "Update Master Key Server";
      throw error;
    }
    // Also setAppLocked = false when performing app activation.
    yield put({
      type: APP_ACTION_TYPES[LOGIN].SET_APP_LOCKED,
      isLocked: false
    });
    const agentCode = yield select(state => state.login.agentCode);
    const profileId = yield select(state => state.login.profileId);
    const sessionCookieSyncGatewaySession = yield select(
      state => state.login.sessionCookieSyncGatewaySession
    );
    const sessionCookieExpiryDate = yield select(
      state => state.login.sessionCookieExpiryDate
    );
    const sessionCookiePath = yield select(
      state => state.login.sessionCookiePath
    );

    if (errorResult.errorType) {
      yield put({ type: APP_ACTION_TYPES[LOGIN].LOGIN_FAIL });

      yield put({
        type: ACTION_TYPES[CONFIG].FINISH_LOADING
      });
      action.callback(errorResult);
    } else {
      yield put({
        type: APP_ACTION_TYPES[LOGIN].ENABLE_RUN_VIEW
      });
      yield put({
        type: APP_ACTION_TYPES[LOGIN].APP_ACTIVATION_DONE
      });
      yield put({
        type: ACTION_TYPES[CONFIG].FINISH_LOADING
      });

      const syncDocumentId = yield select(state => state.login.syncDocumentIds);
      setTimeout(
        dataSync,
        500,
        environment,
        agentCode,
        profileId,
        `Bearer ${authToken}`,
        false,
        false,
        sessionCookieSyncGatewaySession,
        sessionCookieExpiryDate,
        sessionCookiePath,
        syncDocumentId,
        () => {}
      );
    }
  } catch (e) {
    errorResult = {
      hasError: true,
      error: e,
      errorMsg: "Authorization Failed (Error 005)"
    };
    yield put({ type: APP_ACTION_TYPES[LOGIN].LOGIN_FAIL });
    yield put({
      type: ACTION_TYPES[CONFIG].FINISH_LOADING
    });
    action.callback(errorResult);
  }
}

export function* saveNewAppPasswordDEV(action) {
  let errorResult = {};
  let error = {};
  try {
    yield put({
      type: ACTION_TYPES[CONFIG].START_LOADING
    });
    const { appPassword } = action;
    const userID = "alex.tang";
    const lastLoginTime = moment().valueOf();
    const syncDocumentIds = [];
    const installationID = "123456789";
    const loginFlow = yield select(state => state.login.loginFlow);
    let environment = yield select(state => state.login.environment);
    if (environment && environment.length > 0) {
      // Do nothing
    } else {
      environment = "DEV";
    }
    const todayMoment = moment()
      .hours(0)
      .minutes(0)
      .seconds(0)
      .milliseconds(0);
    const expiryDateMoment = todayMoment
      .add(getTimeBombTriggerDays(environment), "days")
      .hours(0)
      .minutes(0)
      .seconds(0)
      .milliseconds(0)
      .utc();
    const timebombExpiryDate = expiryDateMoment.valueOf();
    const sqliteEncryptionKey = aesGen256Key();
    const m = moment();
    const add60Days = m
      .add(DURATION_FOR_APP_PASSWORD, "day")
      .valueOf()
      .toString();

    // store into keychain - app Password, SQLite encryption key
    SInfo.setItem(APP_PASSWORD_KEY, appPassword, {});
    SInfo.setItem(APP_PASSWORD_EXPIRY_DATE, add60Days, {});
    SInfo.setItem(SQLITE_ENCRYPT_KEY, sqliteEncryptionKey, {});
    SInfo.setItem(INSTALLATION_ID, installationID, {});
    SInfo.setItem(USER_ID, userID, {});
    SInfo.setItem(
      TIME_BOMB_EXPIRY_DATE,
      moment(timebombExpiryDate).toISOString(),
      {}
    );
    const masterKey = aesGen256Key();
    const encryptedMasterKey = aesEncrypt(masterKey, appPassword);

    yield call(resetSqliteDB, {
      appPassword,
      encryptedMasterKey,
      sqliteEncryptionKey,
      userID,
      syncDocumentIds,
      lastLoginTime,
      timebombExpiryDate,
      installationID,
      environment
    });
    // store syncDocumentIds
    yield call(
      updateSyncDocumentIds,
      sqliteEncryptionKey,
      syncDocumentIds,
      userID
    );

    // create and encrypt couchbaseDB
    const initCouchbaseDB = encryptionKey =>
      new Promise(resolve => {
        setEncryptionKey(encryptionKey, true, () => {
          resolve();
        });
      });
    yield call(initCouchbaseDB, masterKey);
    const sleep = () => new Promise(resolve => setTimeout(resolve, 2000));
    yield call(sleep);

    // Import Testing Data
    const testingData = () =>
      new Promise(resolve => {
        genTestData(() => {
          const buildNumberBF = BUILD_NUMBER;
          const versionNumberBF = VERSION_NUMBER;
          SInfo.setItem(KC_BUILD_NUMBER, buildNumberBF, {});
          SInfo.setItem(KC_VERSION_NUMBER, versionNumberBF, {});

          resolve();
        });
      });
    const isImportRequired = yield select(
      state => state.login.isImportRequired
    );

    if (isImportRequired || loginFlow === "activation") {
      yield call(testingData);
    }

    yield call(initCallServerConfig, userID, result => {
      if (result.hasError) {
        if (
          result.errorType === POLICY_NUMBER_ERROR &&
          result.errorMsg === POLICY_NO_EXHAUSTED
        ) {
          error = POLICY_NO_EXHAUSTED;
          errorResult = {
            hasError: true,
            errorType: POLICY_NUMBER_ERROR,
            errorMsg: error
          };
        } else {
          error = result.errorMsg
            ? result.errorMsg
            : "Fail in initCallServerConfig";
          throw error;
        }
      }
    });

    let agentData = {};
    const profileId = "alex.tang";
    agentData = yield select(state => state.login.agentData);
    const callServer = yield select(state => state.config.callServer);
    global.session = { platform: Platform.OS };
    yield call(callServer, {
      url: "auth",
      data: {
        action: "login",
        userName: profileId
      }
    });
    yield put({
      type: ACTION_TYPES[REDUCER_TYPES.AGENT].UPDATE_AGENT_DATA,
      data: agentData
    });
    yield put({
      type: APP_ACTION_TYPES[LOGIN].LOGIN_SUCCESS,
      userID,
      lastLoginTime: moment.utc().valueOf()
    });

    yield put({
      type: APP_ACTION_TYPES[LOGIN].SET_APP_LOCKED,
      isLocked: false
    });
    if (errorResult.errorType) {
      yield put({ type: APP_ACTION_TYPES[LOGIN].LOGIN_FAIL });

      yield put({
        type: ACTION_TYPES[CONFIG].FINISH_LOADING
      });
      action.callback(errorResult);
    } else {
      yield put({
        type: APP_ACTION_TYPES[LOGIN].ENABLE_RUN_VIEW
      });
      yield put({
        type: APP_ACTION_TYPES[LOGIN].APP_ACTIVATION_DONE
      });
      yield put({
        type: ACTION_TYPES[CONFIG].FINISH_LOADING
      });
    }
  } catch (e) {
    errorResult = {
      hasError: true,
      error: e,
      errorMsg: "Authorization Failed (Error 005)"
    };
    yield put({ type: APP_ACTION_TYPES[LOGIN].LOGIN_FAIL });
    yield put({
      type: ACTION_TYPES[CONFIG].FINISH_LOADING
    });
    action.callback(errorResult);
  }
}
/**
 * updateAgentProfile
 * @description store key encryption key
 * @param {string} action.appPassword - user key-in a app Password
 * */
export function* updateAgentProfile() {
  let getAgentDocumentResult = {};
  let getAgentUXDocumentResult = {};
  let updateAgentData = null;
  let updateAgentUXData = {};
  try {
    const userID = yield select(state => state.login.userID);
    const agentDocumentId = `U_${userID}`;
    const getAgentDocument = docID =>
      new Promise(resolve => {
        getDocumentWithID(docID, result => {
          if (result.error && result.error.length > 0) {
            getAgentDocumentResult = {
              hasError: true,
              errorMsg: result.error
            };
          } else {
            getAgentDocumentResult = {
              hasError: false,
              result
            };
          }
          resolve(getAgentDocumentResult);
        });
      });
    const agentUXDocumentId = `UX_${userID}`;
    const getAgentUXDocument = docID =>
      new Promise(resolve => {
        getDocumentWithID(docID, result => {
          if (result.error && result.error.length > 0) {
            getAgentUXDocumentResult = {
              hasError: true,
              errorMsg: result.error
            };
          } else {
            getAgentUXDocumentResult = {
              hasError: false,
              result
            };
          }
          resolve(getAgentUXDocumentResult);
        });
      });
    yield call(getAgentDocument, agentDocumentId);
    if (!getAgentDocumentResult.hasError) {
      updateAgentData = getAgentDocumentResult.result;
    }
    yield call(getAgentUXDocument, agentUXDocumentId);
    if (!getAgentUXDocumentResult.hasError) {
      updateAgentUXData = getAgentUXDocumentResult.result;
      yield put({
        type: ACTION_TYPES[REDUCER_TYPES.AGENTUX].UPDATE_UX_AGENT_DATA,
        data: updateAgentUXData
      });
    }
    if (updateAgentData !== null) {
      yield put({
        type: ACTION_TYPES[REDUCER_TYPES.AGENT].UPDATE_AGENT_DATA,
        data: updateAgentData
      });
    }
    yield put({
      type: APP_ACTION_TYPES[LOGIN].AGENTDATA,
      updateAgentData
    });
    // update profile again [End]
  } catch (e) {
    // errorResult = {
    //   hasError: true,
    //   error: e,
    //   errorMsg: "Authorization Failed (Error 007)"
    // };
  }
}
/**
 * resetNewAppPassword
 * @description store key encryption key
 * @param {string} action.appPassword - user key-in a app Password
 * */
export function* resetAppPasswordManually(action) {
  let errorResult = {};
  let error = "";
  let checkVersionAndDeviceResult = {};
  try {
    yield put({
      type: ACTION_TYPES[CONFIG].START_LOADING
    });
    const userID = yield select(state => state.login.userID);
    const installationID = yield call(SInfo.getItem, INSTALLATION_ID, {});
    const environment = yield select(state => state.login.environment);
    const authToken = yield select(state => state.login.authToken);
    const idToken = yield select(state => state.login.idToken);
    const { appPassword } = action;
    const oldAppPassword = yield call(SInfo.getItem, APP_PASSWORD_KEY, {});
    if (oldAppPassword === appPassword) {
      error = "New password cannot be the same as user’s current password";
      throw error;
    }
    const m = moment();
    const add60Days = m
      .add(DURATION_FOR_APP_PASSWORD, "day")
      .valueOf()
      .toString();
    // store into keychain - app Password, SQLite encryption key
    SInfo.setItem(APP_PASSWORD_KEY, appPassword, {});
    SInfo.setItem(APP_PASSWORD_EXPIRY_DATE, add60Days, {});
    // 1. get sqliteEncryptionKey from Key Chain
    const sqliteEncryptionKey = yield call(
      SInfo.getItem,
      SQLITE_ENCRYPT_KEY,
      {}
    );

    // checkVersionAndDeviceResult
    checkVersionAndDeviceResult = yield call(
      checkVersionAndDevice,
      authToken,
      installationID,
      environment
    );
    if (!checkVersionAndDeviceResult.hasError) {
      if (checkVersionAndDeviceResult.isValidVersionAndDevice) {
        // isValidVersionAndDevice = true --> ok
        // OK la
      } else {
        // isValidVersionAndDevice = false --> throw errorMsg
        error = checkVersionAndDeviceResult.errorMsg
          ? checkVersionAndDeviceResult.errorMsg
          : "Authorization Failed (Error 007)";
        throw error;
      }
    } else {
      error = checkVersionAndDeviceResult.errorMsg
        ? checkVersionAndDeviceResult.errorMsg
        : "Authorization Failed (Error 007)";
      throw error;
    }

    // 2. get masterKey by calling the service
    const verify121OnlineAuthResult = yield call(
      verifyUser121Online,
      authToken,
      idToken,
      installationID,
      environment,
      getDeviceInfo()
    );
    const finalResult = verify121OnlineAuthResult.data;
    if (finalResult.status === "success") {
      const masterKey = finalResult.result.master_key;
      const sessionCookie = finalResult.result.cookie;

      if (sessionCookie) {
        const variables = sessionCookie.split(";");
        // const expiresString = variables.find(variable =>
        //   variable.includes("Expires")
        // );
        const syncGatewaySessionString = variables.find(variable =>
          variable.includes("SyncGatewaySession")
        );
        const pathString = variables.find(variable =>
          variable.includes("Path")
        );
        // const [, sessionCookieExpiryDateString] = expiresString.split("=");
        const [
          ,
          sessionCookieSyncGatewaySession
        ] = syncGatewaySessionString.split("=");
        const [, sessionCookiePath] = pathString.split("=");
        // const sessionCookieExpiryDate = moment(
        //   sessionCookieExpiryDateString
        // ).valueOf();

        const sessionCookieExpiryDate = m.valueOf();

        yield put({
          type: APP_ACTION_TYPES[LOGIN].FETECH_SESSION_COOKIE,
          sessionCookie
        });
        yield put({
          type:
            APP_ACTION_TYPES[LOGIN].FETECH_SESSION_COOKIE_SYNC_GATEWAY_SESSION,
          sessionCookieSyncGatewaySession
        });
        yield put({
          type: APP_ACTION_TYPES[LOGIN].FETECH_SESSION_COOKIE_EXPIRY_DATE,
          sessionCookieExpiryDate
        });
        yield put({
          type: APP_ACTION_TYPES[LOGIN].FETECH_SESSION_COOKIE_PATH,
          sessionCookiePath
        });
      } else {
        error = "Cannot session Cookie";
        // throw error;
      }

      // create and encrypt couchbaseDB
      const initCouchbaseDB = encryptionKey =>
        new Promise(resolve => {
          setEncryptionKey(encryptionKey, false, () => {
            resolve();
          });
        });
      yield call(initCouchbaseDB, masterKey);

      // 3. Encrypt to gen new encryptedMasterKey using old master key and new appPassword
      const encryptedMasterKey = aesEncrypt(masterKey, appPassword);
      yield call(
        updateAfterResetPassword,
        sqliteEncryptionKey,
        encryptedMasterKey,
        appPassword,
        userID
      );
      yield put({
        type: APP_ACTION_TYPES[LOGIN].RESET_APP_PASSWORD_MANUALLY_SUCCESS
      });
      yield call(updateFailAttempt, sqliteEncryptionKey, 0, userID);

      yield put({
        type: ACTION_TYPES[CONFIG].FINISH_LOADING
      });
    } else if (
      finalResult.message === "userId and installationId are not match."
    ) {
      error = "userId and installationId are not match.";
      throw error;
    } else {
      error = "Cannot update new App Password";
      throw error;
    }
  } catch (e) {
    errorResult = {
      hasError: true,
      error: e,
      errorMsg: "Authorization Failed (Error 006)"
    };
    yield put({
      type: APP_ACTION_TYPES[LOGIN].RESET_APP_PASSWORD_MANUALLY_FAIL
    });

    yield put({
      type: ACTION_TYPES[CONFIG].FINISH_LOADING
    });
    action.callback(errorResult);
  }
}

/**
 * verify121OnlineAuth
 * @description Authenticates the user by access token and user ID in online mode
 * */
export function* verify121OnlineAuth(action) {
  const userID = yield select(state => state.login.userID);
  const authToken = yield select(state => state.login.authToken);
  const idToken = yield select(state => state.login.idToken);
  const installationID = yield call(SInfo.getItem, INSTALLATION_ID, {});
  const loginFlow = yield select(state => state.login.loginFlow);
  const environment = yield select(state => state.login.environment);
  let error = "";
  let errorResult = {};
  let checkVersionAndDeviceResult = {};
  try {
    yield put({
      type: ACTION_TYPES[CONFIG].START_LOADING
    });
    checkVersionAndDeviceResult = yield call(
      checkVersionAndDevice,
      authToken,
      installationID,
      environment
    );
    if (!checkVersionAndDeviceResult.hasError) {
      if (checkVersionAndDeviceResult.isValidVersionAndDevice) {
        // isValidVersionAndDevice = true --> ok
        // OK la
      } else {
        // isValidVersionAndDevice = false --> throw errorMsg
        error = checkVersionAndDeviceResult.errorMsg
          ? checkVersionAndDeviceResult.errorMsg
          : "Authorization Failed (Error 007)";
        throw error;
      }
    } else {
      // throw 007
      error = checkVersionAndDeviceResult.errorMsg
        ? checkVersionAndDeviceResult.errorMsg
        : "Authorization Failed (Error 007)";
      throw error;
    }
    const verify121OnlineAuthResult = yield call(
      verifyUser121Online,
      authToken,
      idToken,
      installationID,
      environment,
      getDeviceInfo()
    );
    // success
    const finalResult = verify121OnlineAuthResult.data;
    if (finalResult.status === "success") {
      const masterKey = finalResult.result.master_key;
      const sessionCookie = finalResult.result.cookie;
      if (sessionCookie) {
        const variables = sessionCookie.split(";");
        // const expiresString = variables.find(variable =>
        //   variable.includes("Expires")
        // );
        const syncGatewaySessionString = variables.find(variable =>
          variable.includes("SyncGatewaySession")
        );
        const pathString = variables.find(variable =>
          variable.includes("Path")
        );
        // const [, sessionCookieExpiryDateString] = expiresString.split("=");
        const [
          ,
          sessionCookieSyncGatewaySession
        ] = syncGatewaySessionString.split("=");
        const [, sessionCookiePath] = pathString.split("=");
        // const sessionCookieExpiryDate = moment(
        //   sessionCookieExpiryDateString
        // ).valueOf();
        const m = moment();
        const sessionCookieExpiryDate = m.valueOf();

        yield put({
          type: APP_ACTION_TYPES[LOGIN].FETECH_SESSION_COOKIE,
          sessionCookie
        });
        yield put({
          type:
            APP_ACTION_TYPES[LOGIN].FETECH_SESSION_COOKIE_SYNC_GATEWAY_SESSION,
          sessionCookieSyncGatewaySession
        });
        yield put({
          type: APP_ACTION_TYPES[LOGIN].FETECH_SESSION_COOKIE_EXPIRY_DATE,
          sessionCookieExpiryDate
        });
        yield put({
          type: APP_ACTION_TYPES[LOGIN].FETECH_SESSION_COOKIE_PATH,
          sessionCookiePath
        });
      } else {
        error = "Cannot session Cookie";
        // throw error;
      }
      // create and encrypt couchbaseDB
      const initCouchbaseDB = encryptionKey =>
        new Promise(resolve => {
          setEncryptionKey(encryptionKey, false, () => {
            resolve();
          });
        });
      yield call(initCouchbaseDB, masterKey);

      const testingData = () =>
        new Promise(resolve => {
          genTestData(() => {
            const buildNumberBF = BUILD_NUMBER;
            const versionNumberBF = VERSION_NUMBER;
            SInfo.setItem(KC_BUILD_NUMBER, buildNumberBF, {});
            SInfo.setItem(KC_VERSION_NUMBER, versionNumberBF, {});
            resolve();
          });
        });
      const isImportRequired = yield select(
        state => state.login.isImportRequired
      );
      if (isImportRequired || loginFlow === "activation") {
        yield call(testingData);
      }
      yield call(initCallServerConfig, userID, result => {
        if (result.hasError) {
          if (
            result.errorType === POLICY_NUMBER_ERROR &&
            result.errorMsg === POLICY_NO_EXHAUSTED
          ) {
            error = POLICY_NO_EXHAUSTED;
            errorResult = {
              hasError: true,
              errorType: POLICY_NUMBER_ERROR,
              errorMsg: error
            };
            // action.callback(errorResult);
          } else {
            error = result.errorMsg
              ? result.errorMsg
              : "Fail in initCallServerConfig";
            throw error;
          }
        }
      });
      yield call(initCallServerConfig2, result => {
        if (result.hasError) {
          error = result.errorMsg
            ? result.errorMsg
            : "Fail in initCallServerConfig2";
          throw error;
        }
      });
      yield put({
        type: APP_ACTION_TYPES[LOGIN].LOGIN_SUCCESS,
        userID,
        lastLoginTime: moment.utc().valueOf()
      });
      if (errorResult.hasError) {
        // Do nothing
      } else {
        errorResult = {
          hasError: false,
          result: null
        };
      }
    } else if (
      finalResult.message === "userId and installationId are not match."
    ) {
      error = "userId and installationId are not match.";
      throw error;
    } else {
      error = "Cannot verify 121 Online Auth";
      throw error;
    }
    yield put({
      type: ACTION_TYPES[CONFIG].FINISH_LOADING
    });

    // start the inflight jobs
    yield put({
      type: APP_ACTION_TYPES[INFLIGHT_JOB].RUN_INFLIGHT_JOBS,
      callback: () => {
        action.callback(errorResult);
      }
    });
  } catch (e) {
    errorResult = {
      hasError: true,
      error: e,
      errorMsg: "Authorization Failed (Error 004)"
    };
    yield put({ type: APP_ACTION_TYPES[LOGIN].LOGIN_FAIL });
    yield put({
      type: ACTION_TYPES[CONFIG].FINISH_LOADING
    });
    action.callback(errorResult);
  }
}

export function* verify121OnlineAuthForPayment() {
  // const userID = yield select(state => state.login.userID);
  const authToken = yield select(state => state.login.authToken);
  const idToken = yield select(state => state.login.idToken);
  const installationID = yield call(SInfo.getItem, INSTALLATION_ID, {});
  const environment = yield select(state => state.login.environment);
  let checkVersionAndDeviceResult = {};
  try {
    yield put({
      type: ACTION_TYPES[CONFIG].START_LOADING
    });
    checkVersionAndDeviceResult = yield call(
      checkVersionAndDevice,
      authToken,
      installationID,
      environment
    );
    if (!checkVersionAndDeviceResult.hasError) {
      if (checkVersionAndDeviceResult.isValidVersionAndDevice) {
        // isValidVersionAndDevice = true --> ok
        // OK la
      } else {
        // isValidVersionAndDevice = false --> throw errorMsg
        throw new Error(
          checkVersionAndDeviceResult.errorMsg
            ? checkVersionAndDeviceResult.errorMsg
            : "Authorization Failed (Error 007)"
        );
      }
    } else {
      throw new Error(
        checkVersionAndDeviceResult.errorMsg
          ? checkVersionAndDeviceResult.errorMsg
          : "Authorization Failed (Error 007)"
      );
    }
    const verify121OnlineAuthResult = yield call(
      verifyUser121Online,
      authToken,
      idToken,
      installationID,
      environment,
      getDeviceInfo()
    );
    const finalResult = verify121OnlineAuthResult.data;
    if (finalResult.status === "success") {
      const sessionCookie = finalResult.result.cookie;
      if (sessionCookie) {
        const variables = sessionCookie.split(";");
        // const expiresString = variables.find(variable =>
        //   variable.includes("Expires")
        // );
        const syncGatewaySessionString = variables.find(variable =>
          variable.includes("SyncGatewaySession")
        );
        const pathString = variables.find(variable =>
          variable.includes("Path")
        );
        // const [, sessionCookieExpiryDateString] = expiresString.split("=");
        const [
          ,
          sessionCookieSyncGatewaySession
        ] = syncGatewaySessionString.split("=");
        const [, sessionCookiePath] = pathString.split("=");
        // const sessionCookieExpiryDate = moment(
        //   sessionCookieExpiryDateString
        // ).valueOf();
        const m = moment();
        const sessionCookieExpiryDate = m.valueOf();

        yield put({
          type: APP_ACTION_TYPES[LOGIN].FETECH_SESSION_COOKIE,
          sessionCookie
        });
        yield put({
          type:
            APP_ACTION_TYPES[LOGIN].FETECH_SESSION_COOKIE_SYNC_GATEWAY_SESSION,
          sessionCookieSyncGatewaySession
        });
        yield put({
          type: APP_ACTION_TYPES[LOGIN].FETECH_SESSION_COOKIE_EXPIRY_DATE,
          sessionCookieExpiryDate
        });
        yield put({
          type: APP_ACTION_TYPES[LOGIN].FETECH_SESSION_COOKIE_PATH,
          sessionCookiePath
        });
      } else {
        throw new Error("Cannot session Cookie");
      }
      yield put({
        type: APP_ACTION_TYPES[PAYMENT].ONLINE_AUTH_SUCCESS
      });
    } else {
      yield put({ type: APP_ACTION_TYPES[PAYMENT].ONLINE_AUTH_FAIL });
    }
  } catch (e) {
    yield put({ type: APP_ACTION_TYPES[PAYMENT].ONLINE_AUTH_FAIL });

    yield put({
      type: ACTION_TYPES[CONFIG].FINISH_LOADING
    });
  }
}

/**
 * checkAppPasswordExpiryDate
 * check App Password Status
 * @description callback true if expiry; else false
 * */
export function* checkAppPasswordExpiryDate(callback) {
  const isAppActivationDone = yield select(
    state => state.login.isAppActivationDone
  );
  if (isAppActivationDone) {
    let isAppPasswordExpired = false;
    try {
      // Get the appPasswordExpiryDate from keychain
      const appPasswordExpiryDate = yield call(
        SInfo.getItem,
        APP_PASSWORD_EXPIRY_DATE,
        {}
      );
      const appPasswordExpiryDateToMs = Number(appPasswordExpiryDate);
      const today = moment(); // Get Today
      const todayToMS = today.valueOf(); // Get Today in terms of ms
      if (appPasswordExpiryDateToMs) {
        // compared with appPasswordExpiryDate and todayToMS in terms of ms
        if (todayToMS < appPasswordExpiryDateToMs) {
          // NOT expired
          // DO Nothing
          isAppPasswordExpired = false;
        } else {
          // expired
          yield put({
            type: APP_ACTION_TYPES[LOGIN].APP_PASSWORD_EXPIRY_DATE
          });
          isAppPasswordExpired = true;
        }
      } else {
        // if cannot get from keychain, bypass the checking on checkAppPasswordExpiryDate;
        // Do Nothing
        isAppPasswordExpired = false;
      }
      yield put({
        type: ACTION_TYPES[CONFIG].FINISH_LOADING
      });
      callback(isAppPasswordExpired);
    } catch (e) {
      isAppPasswordExpired = false;
      yield put({
        type: ACTION_TYPES[CONFIG].FINISH_LOADING
      });
      callback(isAppPasswordExpired);
    }
  } else {
    callback(false);
  }
}

/**
 * check Environment Status
 * @description Environment Status
 * */
export function* checkEnvironment() {
  try {
    // Get the sqliteEncryptionKey from keychain
    const sqliteEncryptionKey = yield call(
      SInfo.getItem,
      SQLITE_ENCRYPT_KEY,
      {}
    );

    // Get environment from SQLiteDB
    let environment = "";
    if (Config.ENV_NAME === "PROD") {
      environment = "PROD";
    } else {
      environment = yield call(getEnvironment, sqliteEncryptionKey);
    }
    let webServiceUrl;
    if (environment && environment.length > 0) {
      yield put({
        type: APP_ACTION_TYPES[LOGIN].ENVIRONMENT,
        environment
      });
      if (environment === "PROD") {
        webServiceUrl = Config.WEB_SERVICE_BASIC_URL;
      } else if (environment === "DEV") {
        webServiceUrl = Config.DEV_WEB_SERVICE_BASIC_URL;
      } else if (environment === "SIT1_EAB") {
        webServiceUrl = Config.SIT1_EAB_WEB_SERVICE_BASIC_URL;
      } else if (environment === "SIT2_EAB") {
        webServiceUrl = Config.SIT2_EAB_WEB_SERVICE_BASIC_URL;
      } else if (environment === "SIT3_EAB") {
        webServiceUrl = Config.SIT3_EAB_WEB_SERVICE_BASIC_URL;
      } else if (environment === "SIT_AXA") {
        webServiceUrl = Config.SIT_AXA_WEB_SERVICE_BASIC_URL;
      } else if (environment === "UAT") {
        webServiceUrl = Config.UAT_WEB_SERVICE_BASIC_URL;
      } else if (environment === "PRE_PROD") {
        webServiceUrl = Config.PRE_PROD_WEB_SERVICE_BASIC_URL;
      }
      yield put({
        type: APP_ACTION_TYPES[LOGIN].WEB_SERVICE_URL,
        webServiceUrl
      });
    } else {
      //  TODO: Do nothing now
    }
  } catch (e) {
    yield put({ type: APP_ACTION_TYPES[LOGIN].LOGIN_FAIL });
    yield put({
      type: ACTION_TYPES[CONFIG].FINISH_LOADING
    });
  }
}
/**
 * check checkLastDataSyncTime Status
 * @description checkLastDataSyncTime Status
 * */
export function* checkLastDataSyncTime(action) {
  const { callback } = action;
  try {
    yield put({
      type: ACTION_TYPES[CONFIG].FINISH_LOADING
    });
    const lastDataSyncTime = yield call(SInfo.getItem, LAST_DATA_SYNC_TIME, {});
    yield put({
      type: APP_ACTION_TYPES[LOGIN].LAST_DATA_SYNC_TIME,
      lastDataSyncTime: lastDataSyncTime || ""
    });
    callback(lastDataSyncTime);
  } catch (e) {
    yield put({ type: APP_ACTION_TYPES[LOGIN].LOGIN_FAIL });
    yield put({
      type: ACTION_TYPES[CONFIG].FINISH_LOADING
    });
    callback(null);
  }
}
/**
 * setLastDataSyncTime Status
 * @description setLastDataSyncTime Status
 * */
export function* setLastDataSyncTime() {
  try {
    yield put({
      type: ACTION_TYPES[CONFIG].FINISH_LOADING
    });
    const lastDataSyncTime = yield call(SInfo.getItem, LAST_DATA_SYNC_TIME, {});
    yield put({
      type: APP_ACTION_TYPES[LOGIN].LAST_DATA_SYNC_TIME,
      lastDataSyncTime: lastDataSyncTime || ""
    });
  } catch (e) {
    yield put({
      type: ACTION_TYPES[CONFIG].FINISH_LOADING
    });
  }
}

/**
 * check checkAppPasswordFailAttempt Status
 * @description checkAppPasswordFailAttempt Status
 * */
export function* checkAppPasswordFailAttempt() {
  try {
    // Get the sqliteEncryptionKey from keychain
    const sqliteEncryptionKey = yield call(
      SInfo.getItem,
      SQLITE_ENCRYPT_KEY,
      {}
    );

    // Get UserID and failAttempt
    const failAttempt = yield call(getFailAttempt, sqliteEncryptionKey);
    if (typeof failAttempt === "number" && failAttempt === 3) {
      yield put({
        type: APP_ACTION_TYPES[LOGIN].EXCEED_FAIL_LOGIN_ATTEMPS
      });
    }
  } catch (e) {
    yield put({ type: APP_ACTION_TYPES[LOGIN].LOGIN_FAIL });
  }
}

/**
 * checkLoginStatus
 * @reducer login.loginFlow
 * @description check login status
 * */
export function* checkLoginStatus() {
  try {
    yield call(checkEnvironment);
    yield call(checkAppPasswordExpiryDate, () => {});
    yield call(checkBuildInfo, () => {});
    const lastDataSyncTime = yield call(SInfo.getItem, LAST_DATA_SYNC_TIME, {});
    yield put({
      type: APP_ACTION_TYPES[LOGIN].LAST_DATA_SYNC_TIME,
      lastDataSyncTime: lastDataSyncTime || ""
    });
    const sqliteEncryptionKey = yield call(
      SInfo.getItem,
      SQLITE_ENCRYPT_KEY,
      {}
    );
    const userID = yield call(getUserName, sqliteEncryptionKey);
    const loginRecord = yield call(getLoginRecord, sqliteEncryptionKey);
    if (loginRecord) {
      const encryptedMasterKey = loginRecord.encrypted_master_key;
      const hashedAppPassword = loginRecord.hashed_app_password;
      aesDecrypt(encryptedMasterKey, hashedAppPassword);
    }

    // get syncSystemIds from SQLlite and store in redurer
    const syncDocumentIdGet = yield call(
      getSyncDocumentIds,
      sqliteEncryptionKey
    );

    const syncDocumentId = syncDocumentIdGet.split(",");
    if (syncDocumentId) {
      yield put({
        type: APP_ACTION_TYPES[LOGIN].SYNC_DOCUMENT_IDS,
        syncDocumentId
      });
    } else {
      // throw error;
    }

    // If we can obtain username, the app has been activated
    if (userID && userID.length > 0) {
      yield put({ type: APP_ACTION_TYPES[LOGIN].SET_USER_ID, userID });
    }

    if (sqliteEncryptionKey != null && sqliteEncryptionKey.trim().length > 0) {
      const isActivated = yield call(isMobileActivated, sqliteEncryptionKey);
      if (isActivated) {
        const isOnline = true;
        if (isOnline) {
          yield put({ type: APP_ACTION_TYPES[LOGIN].USE_ONLINE_AUTH });
        } else {
          yield put({ type: APP_ACTION_TYPES[LOGIN].USE_LOCAL_AUTH });
        }
      } else {
        const environment = yield select(state => state.login.environment);
        if (environment && environment.length > 0 && environment === "DEV") {
          yield put({ type: APP_ACTION_TYPES[LOGIN].USE_LOCAL_AUTH });
        } else {
          yield put({ type: APP_ACTION_TYPES[LOGIN].USE_ACTIVATION });
        }
      }
    } else {
      yield put({ type: APP_ACTION_TYPES[LOGIN].USE_ACTIVATION });
    }
  } catch (e) {
    yield put({ type: APP_ACTION_TYPES[LOGIN].USE_ACTIVATION });
  }
}
export function* setEnvironment(action) {
  const { environment } = action;
  try {
    if (environment != null && environment.trim().length > 0) {
      yield put({
        type: APP_ACTION_TYPES[LOGIN].ENVIRONMENT,
        environment
      });
      let webServiceUrl;
      if (environment === "PROD") {
        webServiceUrl = Config.WEB_SERVICE_BASIC_URL;
      } else if (environment === "DEV") {
        webServiceUrl = Config.DEV_WEB_SERVICE_BASIC_URL;
      } else if (environment === "SIT1_EAB") {
        webServiceUrl = Config.SIT1_EAB_WEB_SERVICE_BASIC_URL;
      } else if (environment === "SIT2_EAB") {
        webServiceUrl = Config.SIT2_EAB_WEB_SERVICE_BASIC_URL;
      } else if (environment === "SIT3_EAB") {
        webServiceUrl = Config.SIT3_EAB_WEB_SERVICE_BASIC_URL;
      } else if (environment === "SIT_AXA") {
        webServiceUrl = Config.SIT_AXA_WEB_SERVICE_BASIC_URL;
      } else if (environment === "UAT") {
        webServiceUrl = Config.UAT_WEB_SERVICE_BASIC_URL;
      } else if (environment === "PRE_PROD") {
        webServiceUrl = Config.PRE_PROD_WEB_SERVICE_BASIC_URL;
      }
      yield put({
        type: APP_ACTION_TYPES[LOGIN].WEB_SERVICE_URL,
        webServiceUrl
      });
    } else {
      yield put({ type: APP_ACTION_TYPES[LOGIN].LOGIN_FAIL });
      yield put({
        type: ACTION_TYPES[CONFIG].FINISH_LOADING
      });
    }
  } catch (e) {
    yield put({ type: APP_ACTION_TYPES[LOGIN].LOGIN_FAIL });
    yield put({
      type: ACTION_TYPES[CONFIG].FINISH_LOADING
    });
  }
}
/**
 * checkFailAttempt
 * @description login by app password
 * @param {string} action.userID - user id
 * @param {string} action.hashedAppPassword - hashed app password
 * */
export function* checkFailAttempt(action) {
  try {
    yield put({
      type: ACTION_TYPES[CONFIG].START_LOADING
    });

    const sqliteEncryptionKey = yield call(
      SInfo.getItem,
      SQLITE_ENCRYPT_KEY,
      {}
    );
    // Get UserID and failAttempt
    const failAttempt = yield call(getFailAttempt, sqliteEncryptionKey);
    const failAttemptTime = Number(failAttempt);
    yield put({
      type: ACTION_TYPES[CONFIG].FINISH_LOADING
    });
    action.callback(failAttemptTime);
  } catch (e) {
    yield put({
      type: ACTION_TYPES[CONFIG].FINISH_LOADING
    });
    action.callback(0);
  }
}

/**
 * loginByAppPassword
 * @description login by app password
 * @param {string} action.userID - user id
 * @param {string} action.hashedAppPassword - hashed app password
 * */
export function* loginByAppPassword(action) {
  const { userID, hashedAppPassword } = action;
  const environment = yield select(state => state.login.environment);
  let error = {};
  try {
    yield put({
      type: ACTION_TYPES[CONFIG].START_LOADING
    });
    const sqliteEncryptionKey = yield call(
      SInfo.getItem,
      SQLITE_ENCRYPT_KEY,
      {}
    );
    let encryptedMasterKey = false;
    if (sqliteEncryptionKey != null && sqliteEncryptionKey.trim().length > 0) {
      encryptedMasterKey = yield call(
        tryLoginbyAppPassword,
        sqliteEncryptionKey,
        userID,
        hashedAppPassword
      );
    }
    // Get UserID and failAttempt
    const failAttempt = yield call(getFailAttempt, sqliteEncryptionKey);
    const failAttemptAfterAdjustment = Number(failAttempt) + 1;

    if (encryptedMasterKey && encryptedMasterKey.length > 0) {
      // Do nothing
      // For each time login success, set failAttempt to zero
      const setToZero = yield call(
        updateFailAttempt,
        sqliteEncryptionKey,
        0,
        userID
      );
      if (setToZero.rowsAffected > 0) {
        // update successfully
        // Do nothing
      } else {
        // TO DO: Handling if update SQLite fail
      }
    } else if (environment && environment.length > 0 && environment === "DEV") {
      encryptedMasterKey = "123";
    } else {
      // App Password Fail case
      let updateFailAttemptResult2 = null;
      if (
        typeof failAttemptAfterAdjustment === "number" &&
        failAttemptAfterAdjustment === 1
      ) {
        updateFailAttemptResult2 = yield call(
          updateFailAttempt,
          sqliteEncryptionKey,
          failAttemptAfterAdjustment,
          userID
        );
        if (updateFailAttemptResult2.rowsAffected > 0) {
          // update successfully
          // do nothing
        } else {
          // TO DO: error handling if updating SQLite DB is not successfully.
        }
        error = "Login failed. Please enter correct credentials";
        throw error;
      } else if (
        typeof failAttemptAfterAdjustment === "number" &&
        failAttemptAfterAdjustment > 4
      ) {
        updateFailAttemptResult2 = yield call(
          updateFailAttempt,
          sqliteEncryptionKey,
          failAttemptAfterAdjustment,
          userID
        );
        if (updateFailAttemptResult2.rowsAffected > 0) {
          // update successfully
          // do nothing
        } else {
          // TO DO: error handling if updating SQLite DB is not successfully.
        }
        // yield put({
        //   type: APP_ACTION_TYPES[LOGIN].EXCEED_FAIL_LOGIN_ATTEMPS
        // });
        error = "Your account is locked. Please unlock through Reset Password";
        throw error;
      } else {
        const remainingCount = 5 - failAttemptAfterAdjustment;
        updateFailAttemptResult2 = yield call(
          updateFailAttempt,
          sqliteEncryptionKey,
          failAttemptAfterAdjustment,
          userID
        );
        if (updateFailAttemptResult2.rowsAffected > 0) {
          // update successfully
          // do nothing
        } else {
          // TO DO: error handling if updating SQLite DB is not successfully.
        }
        error = `You have entered an incorrect password ${failAttemptAfterAdjustment} times. After ${remainingCount} unsuccessful attempts your account would be locked.`;
        throw error;
      }
    }
    if (encryptedMasterKey && encryptedMasterKey.length > 0) {
      const masterKey = aesDecrypt(encryptedMasterKey, hashedAppPassword);
      const loginFlow = yield select(state => state.login.loginFlow);
      if (
        environment &&
        environment.length > 0 &&
        environment === "DEV" &&
        loginFlow &&
        loginFlow.length > 0 &&
        loginFlow === "activation"
      ) {
        // yield call(resetSqliteDBForDev, environment);
      }
      // create and encrypt couchbaseDB
      const initCouchbaseDB = encryptionKey =>
        new Promise(resolve => {
          setEncryptionKey(encryptionKey, false, () => {
            resolve();
          });
        });
      yield call(initCouchbaseDB, masterKey);

      const testingData = () =>
        new Promise(resolve => {
          genTestData(() => {
            const buildNumberBF = BUILD_NUMBER;
            const versionNumberBF = VERSION_NUMBER;
            SInfo.setItem(KC_BUILD_NUMBER, buildNumberBF, {});
            SInfo.setItem(KC_VERSION_NUMBER, versionNumberBF, {});
            resolve();
          });
        });
      const isImportRequired = yield select(
        state => state.login.isImportRequired
      );

      // yield call(testingData);
      yield call(initCallServerConfig, userID, result => {
        if (result.hasError) {
          if (
            result.errorType === POLICY_NUMBER_ERROR &&
            result.errorMsg === POLICY_NO_EXHAUSTED
          ) {
            error = POLICY_NO_EXHAUSTED;
            const errorResult = {
              hasError: true,
              errorType: POLICY_NUMBER_ERROR,
              errorMsg: error
            };
            action.callback(errorResult);
          } else {
            error = result.errorMsg
              ? result.errorMsg
              : "Fail in initCallServerConfig";
            throw error;
          }
        }
      });
      const sleep = () => new Promise(resolve => setTimeout(resolve, 200));
      yield call(sleep);
      if (isImportRequired || loginFlow === "activation") {
        yield call(testingData);
      }
      yield call(initCallServerConfig2, result => {
        if (result.hasError) {
          error = result.errorMsg
            ? result.errorMsg
            : "Fail in initCallServerConfig2";
          throw error;
        }
      });
      yield put({
        type: APP_ACTION_TYPES[LOGIN].LOGIN_SUCCESS,
        userID: action.userID,
        lastLoginTime: moment.utc().valueOf()
      });
      const errorResult = {
        hasError: false,
        result: null
      };
      yield put({
        type: ACTION_TYPES[CONFIG].FINISH_LOADING
      });
      action.callback(errorResult);
    } else {
      const sleep = () => new Promise(resolve => setTimeout(resolve, 200));
      yield call(sleep);
      error =
        "Wrong Password to decode encryptedMasterKey and then unlock the database";
      throw error;
    }
    yield put({
      type: ACTION_TYPES[CONFIG].FINISH_LOADING
    });

    // start the inflight jobs
    yield put({
      type: APP_ACTION_TYPES[INFLIGHT_JOB].RUN_INFLIGHT_JOBS
    });
  } catch (e) {
    const errorResult = {
      hasError: true,
      error: e,
      errorMsg: "Authorization Failed (Error 002)"
    };
    yield put({ type: APP_ACTION_TYPES[LOGIN].LOGIN_FAIL });

    yield put({
      type: ACTION_TYPES[CONFIG].FINISH_LOADING
    });
    action.callback(errorResult);
  }
}

export function* loginByAppPasswordDEV(action) {
  try {
    yield put({
      type: ACTION_TYPES[CONFIG].START_LOADING
    });
    const { userID, hashedAppPassword } = action;
    let error = {};
    const sqliteEncryptionKey = yield call(
      SInfo.getItem,
      SQLITE_ENCRYPT_KEY,
      {}
    );
    let encryptedMasterKey = false;
    if (sqliteEncryptionKey != null && sqliteEncryptionKey.trim().length > 0) {
      encryptedMasterKey = yield call(
        tryLoginbyAppPassword,
        sqliteEncryptionKey,
        userID,
        hashedAppPassword
      );
    }
    if (encryptedMasterKey && encryptedMasterKey.length > 0) {
      const masterKey = aesDecrypt(encryptedMasterKey, hashedAppPassword);
      const loginFlow = yield select(state => state.login.loginFlow);
      // create and encrypt couchbaseDB
      const initCouchbaseDB = encryptionKey =>
        new Promise(resolve => {
          setEncryptionKey(encryptionKey, false, () => {
            resolve();
          });
        });
      yield call(initCouchbaseDB, masterKey);

      const testingData = () =>
        new Promise(resolve => {
          genTestData(() => {
            const buildNumberBF = BUILD_NUMBER;
            const versionNumberBF = VERSION_NUMBER;
            SInfo.setItem(KC_BUILD_NUMBER, buildNumberBF, {});
            SInfo.setItem(KC_VERSION_NUMBER, versionNumberBF, {});
            resolve();
          });
        });
      const isImportRequired = yield select(
        state => state.login.isImportRequired
      );
      yield call(initCallServerConfig, userID, result => {
        if (result.hasError) {
          if (
            result.errorType === POLICY_NUMBER_ERROR &&
            result.errorMsg === POLICY_NO_EXHAUSTED
          ) {
            error = POLICY_NO_EXHAUSTED;
            const errorResult = {
              hasError: true,
              errorType: POLICY_NUMBER_ERROR,
              errorMsg: error
            };
            action.callback(errorResult);
          } else {
            error = result.errorMsg
              ? result.errorMsg
              : "Fail in initCallServerConfig";
            throw error;
          }
        }
      });
      const sleep = () => new Promise(resolve => setTimeout(resolve, 200));
      yield call(sleep);
      if (isImportRequired || loginFlow === "activation") {
        yield call(testingData);
      }
      yield call(initCallServerConfig2, result => {
        if (result.hasError) {
          error = result.errorMsg
            ? result.errorMsg
            : "Fail in initCallServerConfig2";
          throw error;
        }
      });
      yield put({
        type: APP_ACTION_TYPES[LOGIN].LOGIN_SUCCESS,
        userID: action.userID,
        lastLoginTime: moment.utc().valueOf()
      });
      const errorResult = {
        hasError: false,
        result: null
      };
      yield put({
        type: ACTION_TYPES[CONFIG].FINISH_LOADING
      });
      action.callback(errorResult);
    } else {
      const sleep = () => new Promise(resolve => setTimeout(resolve, 200));
      yield call(sleep);
      error =
        "Wrong Password to decode encryptedMasterKey and then unlock the database";
      throw error;
    }
    yield put({
      type: ACTION_TYPES[CONFIG].FINISH_LOADING
    });
  } catch (e) {
    const errorResult = {
      hasError: true,
      error: e,
      errorMsg: "Authorization Failed (Error 002)"
    };
    yield put({ type: APP_ACTION_TYPES[LOGIN].LOGIN_FAIL });

    yield put({
      type: ACTION_TYPES[CONFIG].FINISH_LOADING
    });
    action.callback(errorResult);
  }
}
/**
 * loginByOnlineAuth
 * @description login by online authentication
 * */
export function* loginByOnlineAuth(action) {
  try {
    let error = "";
    yield put({
      type: ACTION_TYPES[CONFIG].START_LOADING
    });
    const sqliteEncryptionKey = yield call(
      SInfo.getItem,
      SQLITE_ENCRYPT_KEY,
      {}
    );
    let encryptedMasterKey = false;
    if (sqliteEncryptionKey != null && sqliteEncryptionKey.trim().length > 0) {
      encryptedMasterKey = yield call(
        tryLoginbyOnlineAuth,
        sqliteEncryptionKey
      );
    } else {
      error = "Cannot get sqliteEncryptionKey from keychain";
      throw error;
    }
    if (encryptedMasterKey) {
      yield put({ type: APP_ACTION_TYPES[LOGIN].ACCESSSQLITESUCCESS });
    } else {
      yield put({ type: APP_ACTION_TYPES[LOGIN].ACCESSSQLITEFAIL });
      error = "Cannot have encryptedMasterKey";
      throw error;
    }
    yield put({
      type: ACTION_TYPES[CONFIG].FINISH_LOADING
    });
  } catch (e) {
    const errorResult = {
      hasError: true,
      error: e,
      errorMsg: "Authorization Failed (Error 003)"
    };
    yield put({ type: APP_ACTION_TYPES[LOGIN].LOGIN_FAIL });

    yield put({
      type: ACTION_TYPES[CONFIG].FINISH_LOADING
    });
    action.callback(errorResult);
  }
}

export function* onlineAuthCodeForPayment(action) {
  let error = {};
  let output = {};
  try {
    yield put({
      type: ACTION_TYPES[CONFIG].START_LOADING
    });
    const environment = yield select(state => state.login.environment);
    let installationID = yield call(SInfo.getItem, INSTALLATION_ID, {});
    let userID = yield select(state => state.login.userID);
    const response = yield call(fetchAuthToken, action.authCode, environment);
    const result = { ...response.data };

    // 1
    if (result.access_token && result.access_token.length > 0) {
      const authToken = result.access_token;
      const idToken = result.id_token;
      yield put({
        type: APP_ACTION_TYPES[LOGIN].AUTH_TOKEN,
        authToken
      });

      // couchbase sync gateway openid
      // yield put({
      //   type: APP_ACTION_TYPES[LOGIN].ID_TOKEN,
      //   idToken
      // });
      const m = moment();
      const add60Mins = m
        .add(60, "minutes")
        .valueOf()
        .toString();
      SInfo.setItem(TOKEN_EXPIRY_DATE, add60Mins, {});

      // Set IS_TOKEN_VALID to true
      yield put({
        type: APP_ACTION_TYPES[LOGIN].IS_TOKEN_VALID,
        isValid: true
      });
      const verify121OnlineAuthResult = yield call(
        verifyUser121Online,
        authToken,
        idToken,
        installationID,
        environment,
        getDeviceInfo()
      );
      // success
      const finalResult = verify121OnlineAuthResult.data;
      if (finalResult.status === "success") {
        // const masterKey = finalResult.result.master_key;
        const sessionCookie = finalResult.result.cookie;
        if (sessionCookie) {
          const variables = sessionCookie.split(";");
          // const expiresString = variables.find(variable =>
          //   variable.includes("Expires")
          // );
          const syncGatewaySessionString = variables.find(variable =>
            variable.includes("SyncGatewaySession")
          );
          const pathString = variables.find(variable =>
            variable.includes("Path")
          );
          // const [, sessionCookieExpiryDateString] = expiresString.split("=");
          const [
            ,
            sessionCookieSyncGatewaySession
          ] = syncGatewaySessionString.split("=");
          const [, sessionCookiePath] = pathString.split("=");
          // const sessionCookieExpiryDate = moment(
          //   sessionCookieExpiryDateString
          // ).valueOf();

          const sessionCookieExpiryDate = m.valueOf();

          yield put({
            type: APP_ACTION_TYPES[LOGIN].FETECH_SESSION_COOKIE,
            sessionCookie
          });
          yield put({
            type:
              APP_ACTION_TYPES[LOGIN]
                .FETECH_SESSION_COOKIE_SYNC_GATEWAY_SESSION,
            sessionCookieSyncGatewaySession
          });
          yield put({
            type: APP_ACTION_TYPES[LOGIN].FETECH_SESSION_COOKIE_EXPIRY_DATE,
            sessionCookieExpiryDate
          });
          yield put({
            type: APP_ACTION_TYPES[LOGIN].FETECH_SESSION_COOKIE_PATH,
            sessionCookiePath
          });
        } else {
          error = "Cannot session Cookie";
          // throw error;
        }
      } else if (
        finalResult.message === "userId and installationId are not match."
      ) {
        error = "userId and installationId are not match.";
        throw error;
      } else {
        error = "Cannot verify 121 Online Auth";
        throw error;
      }
      userID = yield call(SInfo.getItem, USER_ID, {});
      installationID = yield call(SInfo.getItem, INSTALLATION_ID, {});
      const todayMoment = moment()
        .hours(0)
        .minutes(0)
        .seconds(0)
        .milliseconds(0);
      const expiryDateVal = todayMoment
        .add(Config.TIME_BOMB_TRIGGER_DAYS, "days")
        .hours(0)
        .minutes(0)
        .seconds(0)
        .milliseconds(0)
        .utc()
        .valueOf();
      yield put({
        type: APP_ACTION_TYPES[LOGIN].VERIFY_DEVICE_SUCCESS,
        userID,
        lastLoginTime: moment.utc().valueOf(),
        timebombExpiryDate: expiryDateVal,
        authToken,
        installationID,
        idToken
      });
      yield put({ type: APP_ACTION_TYPES[PAYMENT].ONLINE_AUTH_SUCCESS });
      output = {
        hasError: false,
        result: null
      };
    } else {
      error = "Cannot get Auth Token";
      throw error;
    }
    yield put({
      type: ACTION_TYPES[CONFIG].FINISH_LOADING
    });
    action.callback(output);
  } catch (e) {
    output = {
      hasError: true,
      error: e,
      errorMsg: e
    };
    yield put({ type: APP_ACTION_TYPES[PAYMENT].ONLINE_AUTH_FAIL });
    yield put({
      type: ACTION_TYPES[CONFIG].FINISH_LOADING
    });
    action.callback(output);
  }
}

export default {
  onlineAuthCode,
  saveNewAppPassword,
  resetAppPasswordManually,
  verify121OnlineAuth,
  checkLoginStatus,
  checkEnvironment,
  setEnvironment,
  initCallServerConfig,
  // Payment
  onlineAuthCodeForPayment,
  verify121OnlineAuthForPayment,
  checkBuildInfo
};
