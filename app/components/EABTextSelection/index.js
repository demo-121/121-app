import React, { PureComponent } from "react";
import { View, Text, TouchableOpacity } from "react-native";
import PropTypes from "prop-types";
import * as _ from "lodash";
import Theme from "../../theme";
import getStyleArray from "../../utilities/getStyleArray";

import EABCard from "../EABCard";
import styles from "./styles";

/**
 * <EABTextSelection />
 * @description EABTextSelection UI.
 * @requires EABCard - components/EABCard
 * @param {boolean} [isVertical=false]
 * @param {array} options - array of options's object.
 * @param {string} options[].key - text selection key
 * @param {string|element} options[].title - text selection text
 * @param {boolean} options[].isSelected - indicates whether the card is selected or not
 * @param {boolean} [editable=true] - indicates whether the text box is editable or not
 * @param {function} onPress - the function that will be fire by the text select onPress event.
 *   it will return the option's object to be the function parameter.
 * @param {object|array|number} [style={}] - custom container style
 * */

class EABTextSelection extends PureComponent {
  constructor(props) {
    super(props);

    this.state = {};
  }

  render() {
    const {
      options,
      editable,
      onPress,
      activeOpacity,
      style,
      testID
    } = this.props;
    return (
      <View style={[styles.container, ...getStyleArray(style)]}>
        {_.map(options, (option, index) => (
          <TouchableOpacity
            key={option.key}
            testID={`${testID}__btn${option.testID}`}
            activeOpacity={editable ? activeOpacity : 1}
            style={[
              index === 0
                ? styles.textSelectionViewFirst
                : styles.textSelectionView
            ]}
            onPress={() => {
              if (editable) {
                onPress(option);
              }
            }}
          >
            <EABCard
              isPlainText
              isSelected={option.isSelected}
              style={styles.textSelection}
              contentStyle={[
                styles.textSelectionContent,
                {
                  opacity: editable ? 1 : activeOpacity
                }
              ]}
            >
              <Text style={option.isSelected ? Theme.headlineBrand : {}}>
                {option.title}
              </Text>
            </EABCard>
          </TouchableOpacity>
        ))}
      </View>
    );
  }
}

EABTextSelection.propTypes = {
  options: PropTypes.arrayOf(
    PropTypes.shape({
      key: PropTypes.string.isRequired,
      title: PropTypes.oneOfType([PropTypes.string, PropTypes.node]).isRequired,
      isSelected: PropTypes.bool.isRequired
    })
  ).isRequired,
  editable: PropTypes.bool,
  activeOpacity: PropTypes.number,
  onPress: PropTypes.func.isRequired,
  style: PropTypes.oneOfType([
    PropTypes.number,
    PropTypes.object,
    PropTypes.array
  ]),
  testID: PropTypes.string
};

EABTextSelection.defaultProps = {
  editable: true,
  activeOpacity: 0.5,
  style: {},
  testID: ""
};

export default EABTextSelection;
