import { REDUCER_TYPES, ACTION_TYPES } from "eab-web-api";
import { connect } from "react-redux";
import { TEXT_STORE } from "../../../constants/REDUCER_TYPES";
import QuickQuoteButtonGroup from "../components/QuickQuoteButtonGroup";

const { QUOTATION } = REDUCER_TYPES;

/**
 * <QuickQuoteButtonGroup />
 * @description productDetails page app bar quick quote.
 * @requires QuickQuoteButtonGroup - QuickQuoteButtonGroup UI
 * */
const mapStateToProps = state => ({
  textStore: state[TEXT_STORE],
  isQuickQuote: state[QUOTATION].isQuickQuote
});

const mapDispatchToProps = dispatch => ({
  quickQuoteOnChange({ isQuickQuoteData, callback }) {
    dispatch({
      type: ACTION_TYPES[QUOTATION].UPDATE_IS_QUICK_QUOTE,
      isQuickQuoteData,
      callback
    });
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(QuickQuoteButtonGroup);
