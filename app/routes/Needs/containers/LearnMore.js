import { connect } from "react-redux";
import { logout } from "../../../actions/login";
import LearnMore from "../components/LearnMore";

/**
 * LearnMore
 * @requires LearnMore - LearnMore UI
 * */
const mapStateToProps = () => ({});

const mapDispatchToProps = dispatch => ({
  logout() {
    logout({ dispatch });
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LearnMore);
