import React, { PureComponent } from "react";
import { View, Text, TouchableOpacity } from "react-native";
import PropTypes from "prop-types";
import { REDUCER_TYPE_CHECK, REDUCER_TYPES } from "eab-web-api";
import Pdf from "react-native-pdf";
import TranslatedText from "../../../../containers/TranslatedText";
import FNAReportAppBarRight from "../../containers/FNAReportAppBarRight";
import styles from "./styles";
import Theme from "../../../../theme";

const { FNA } = REDUCER_TYPES;

/**
 * FNAReport
 * @description needs detail page app bar right.
 * */
export default class FNAReport extends PureComponent {
  render() {
    const { FNAReport: data, closePdf } = this.props;
    return (
      <View style={{ width: "100%", height: "100%" }}>
        <View
          style={{
            height: 64,
            width: "100%",
            borderBottomColor: Theme.coolGrey,
            borderBottomWidth: 1,
            flexDirection: "row"
          }}
          contentContainerStyle={styles.container}
        >
          <TouchableOpacity
            style={{
              marginLeft: Theme.alignmentXL,
              marginTop: Theme.alignmentXL + Theme.alignmentXS,
              flex: 1
            }}
            onPress={() => {
              closePdf();
            }}
          >
            <Text style={Theme.textButtonLabelNormalAccent}>Close</Text>
          </TouchableOpacity>
          <View
            style={{
              marginTop: Theme.alignmentXL + Theme.alignmentXS,
              marginRight: Theme.alignmentXXXXL,
              flex: 1
            }}
          >
            <TranslatedText style={Theme.title} path="needs.view.fna.report" />
          </View>
          <View
            contentContainerStyle={{
              alignItems: "flex-end"
            }}
            style={{
              marginTop: Theme.alignmentXL + Theme.alignmentXS
            }}
          >
            <FNAReportAppBarRight />
          </View>
        </View>
        <Pdf style={styles.pdf} source={{ uri: data }} />
      </View>
    );
  }
}

FNAReport.propTypes = {
  closePdf: PropTypes.func.isRequired,
  FNAReport: REDUCER_TYPE_CHECK[FNA].FNAReport.isRequired
};
