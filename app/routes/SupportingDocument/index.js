import React, { PureComponent } from "react";
import { View, ScrollView } from "react-native";
import PropTypes from "prop-types";
import { createStackNavigator, createAppContainer } from "react-navigation";
import UploadButtonAfterSubmitted from "./containers/UploadButtonAfterSubmitted";
import EABButton from "../../components/EABButton";
import EABSegmentedControl from "../../components/EABSegmentedControl";
import TranslatedText from "../../containers/TranslatedText";
import { ContextConsumer } from "../../context";
import Theme from "../../theme";
import { addListener } from "../../utilities/reactNavigationReduxHelper";
import PDFViewer from "../../containers/PDFViewer";
import ImageViewer from "../../containers/ImageViewer";
import styles from "./styles";
import screenRedirect from "./screenRedirect";
/**
 * <SupportingDocument />
 * @reducer navigation.SupportingDocument
 * @requires PDFViewer - PDFViewer component
 * @requires ImageViewer - ImageViewer component
 * @requires SupportingDocumentDetails - SupportingDocumentDetails page
 * */
class SupportingDocument extends PureComponent {
  render() {
    const { isShield } = this.props;
    return (
      <SupportingDocumentNavigator
        navigation={{
          dispatch: this.props.dispatch,
          state: this.props.navigation,
          addListener: addListener("SupportingDocument")
        }}
        screenProps={{ isShield }}
      />
    );
  }
}

const SupportingDocumentNavigator = createStackNavigator(
  {
    PDFViewer: {
      screen: PDFViewer,
      navigationOptions: ({ navigation }) => {
        const { params } = navigation.state;
        if (params) {
          return {
            title: "PDF Viewer",
            headerTitle: params.titleDisplay
          };
        }
        return {};
      }
    },
    ImageViewer: {
      screen: ImageViewer,
      navigationOptions: ({ navigation }) => {
        const { params } = navigation.state;
        if (params) {
          return {
            title: "Image Viewer",
            headerTitle: params.titleDisplay
          };
        }
        return {};
      }
    },
    screenRedirect: {
      screen: screenRedirect,
      navigationOptions: ({ navigation }) => {
        const { params } = navigation.state;
        if (params) {
          return {
            title: "Supporting Document",
            headerStyle: {
              height: 75
            },
            headerLeft: <UploadButtonAfterSubmitted navigation={navigation} />,
            headerTitle: (
              <View>
                <View style={styles.titleBar}>
                  <TranslatedText
                    style={Theme.title}
                    path="application.form.supportingDocument.title"
                  />
                </View>
                <ScrollView horizontal showsHorizontalScrollIndicator={false}>
                  <EABSegmentedControl
                    style={{ marginBottom: Theme.alignmentXS }}
                    segments={params.segmentedControlArray}
                    activatedKey={params.showProfileTab}
                  />
                </ScrollView>
              </View>
            ),
            headerRight: (
              <ContextConsumer>
                {({ hideSupportingDocumentDialog }) => (
                  <EABButton
                    isDisable={!params.canDone}
                    containerStyle={styles.doneButton}
                    onPress={() => {
                      hideSupportingDocumentDialog();
                      params.saveSupportingDocument({
                        applicationId: params.docId
                      });
                      if (params.isSubmitted) {
                        params.clearPendingList();
                        params.getApplicationsList();
                      }
                    }}
                  >
                    <TranslatedText
                      style={Theme.textButtonLabelNormalEmphasizedAccent}
                      path="button.done"
                    />
                  </EABButton>
                )}
              </ContextConsumer>
            )
          };
        }
        return {};
      }
    }
  },
  {
    initialRouteName: "screenRedirect"
  }
);

export default createAppContainer(SupportingDocumentNavigator);

SupportingDocument.propTypes = {
  isShield: PropTypes.string.isRequired
};
