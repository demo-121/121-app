import PropTypes from "prop-types";
import React, { Component } from "react";
import { Text, View } from "react-native";
import * as _ from "lodash";
import {
  utilities,
  REDUCER_TYPE_CHECK as WEB_API_REDUCER_TYPE_CHECK,
  REDUCER_TYPES
} from "eab-web-api";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import EABTextField from "../../../../components/EABTextField";
import APP_REDUCER_TYPE_CHECK from "../../../../constants/REDUCER_TYPE_CHECK";
import { TEXT_STORE } from "../../../../constants/REDUCER_TYPES";
import EABi18n from "../../../../utilities/EABi18n";
import EABStepper from "../../../../components/EABStepper";
import { ContextConsumer } from "../../../../context";
import styles from "../Analysis/styles";
import Theme from "../../../../theme";
import PopoverTooltip from "../../../../components/PopoverTooltip/PopoverTooltipIOS";
import EASEAssets from "../../../../containers/EASEAssets";

const { FNA, CLIENT } = REDUCER_TYPES;
const { fillTextVariable } = utilities.common;
const { numberToCurrency, currencyToNumber } = utilities.numberFormatter;
const { calOtherAsset } = utilities.naAnalysis;

/**
 * AnalysisPlanningForSpecificGoals
 * @description Analysis - Planning For Specific Goals - Template
 * */
export default class AnalysisPlanningForSpecificGoals extends Component {
  constructor(props) {
    super(props);

    this.onChange = this.onChange.bind(this);
    this.renderGoals = this.renderGoals.bind(this);
    this.renderShortfall = this.renderShortfall.bind(this);
  }

  componentDidMount() {
    this.props.initNaAnalysis();
  }

  componentDidUpdate(prevProps) {
    if (prevProps.selectedProfile !== this.props.selectedProfile) {
      this.props.initNaAnalysis();
    }
  }

  onChange({ value, key, goalIndex = -1 }) {
    const {
      selectedProfile,
      updateNA,
      na,
      pda,
      profile,
      dependantProfiles,
      fe
    } = this.props;
    const naData = _.cloneDeep(na);

    if (goalIndex !== -1) {
      naData.psGoals[selectedProfile].goals[goalIndex][key] = value;
      naData.psGoals[selectedProfile].init = false;

      updateNA({
        naData,
        pdaData: pda,
        profileData: profile,
        dependantProfilesData: dependantProfiles,
        feData: fe,
        completedStep: 1
      });
    } else {
      naData.psGoals[selectedProfile][key] = value;
      naData.psGoals[selectedProfile].init = false;

      updateNA({
        naData,
        pdaData: pda,
        profileData: profile,
        dependantProfilesData: dependantProfiles,
        feData: fe,
        completedStep: 1
      });
    }
  }

  get selectedGoals() {
    const { data } = this;
    const newGoals = [];

    for (let goalIndex = 0; goalIndex < data.goalNo; goalIndex += 1) {
      newGoals.push(data.goals[goalIndex]);
    }

    return newGoals;
  }

  get data() {
    const { selectedProfile, na } = this.props;

    return na.psGoals[selectedProfile];
  }

  renderGoals(language) {
    const { selectedGoals, onChange } = this;
    const { textStore, isPsGoalsError, selectedProfile, na } = this.props;
    const selectedAssets = na.psGoals[selectedProfile].goals;

    return selectedGoals.map((goal, goalIndex) => (
      <View>
        <View style={styles.goalsWrapper}>
          <Text style={styles.analysisProductPointTitle}>
            {`1. ${EABi18n({
              language,
              textStore,
              path: "na.needs.psGoals.goalNameHeader"
            })}`}
            <Text style={styles.requiredHint}> *</Text>
          </Text>
          <View style={styles.analysisProductPointContent}>
            <EABTextField
              testID={`NeedsAnalysis__PSG__txtLifestyleGoal____gp${goalIndex}`}
              style={styles.analysisTextInputShort}
              value={goal.goalName || ""}
              placeholder={EABi18n({
                language,
                textStore,
                path: "na.needs.psGoals.goalNamePlaceholder"
              })}
              isError={
                isPsGoalsError[selectedProfile].goals[goalIndex].goalName
                  .hasError
              }
              hintText={EABi18n({
                textStore,
                language,
                path:
                  isPsGoalsError[selectedProfile].goals[goalIndex].goalName
                    .messagePath
              })}
              onChange={value => {
                onChange({ key: "goalName", value, goalIndex });
              }}
            />
          </View>

          <Text style={styles.analysisProductPointTitle}>
            {`2. ${EABi18n({
              language,
              textStore,
              path: "na.needs.psGoals.timeHorizon"
            })}`}
            <Text style={styles.requiredHint}> *</Text>
          </Text>
          {isPsGoalsError[selectedProfile].goals[goalIndex].timeHorizon
            .hasError && (
            <Text style={styles.errorMessage}>
              {EABi18n({
                textStore,
                language,
                path:
                  isPsGoalsError[selectedProfile].goals[goalIndex].timeHorizon
                    .messagePath
              })}
            </Text>
          )}
          <EABStepper
            testID={`NeedsAnalysis__PSG__GoalPeriod__gp${goalIndex}`}
            value={goal.timeHorizon ? Number(goal.timeHorizon) : ""}
            step={1}
            min={0}
            max={99}
            onChange={value => {
              onChange({
                key: "timeHorizon",
                value: value.toString(),
                goalIndex
              });
            }}
          />

          <Text style={styles.analysisProductPointTitle}>
            {`3. ${EABi18n({
              language,
              textStore,
              path: "na.needs.psGoals.compFv"
            })}`}
            <Text style={styles.requiredHint}> *</Text>
          </Text>
          <View style={styles.analysisProductPointContent}>
            <EABTextField
              testID={`NeedsAnalysis__PSG__txtTotalAmount__gp${goalIndex}`}
              style={styles.analysisTextInputShort}
              prefix="$"
              value={numberToCurrency({
                value: goal.compFv || 0,
                decimal: 0
              })}
              isError={
                isPsGoalsError[selectedProfile].goals[goalIndex].compFv.hasError
              }
              hintText={EABi18n({
                textStore,
                language,
                path:
                  isPsGoalsError[selectedProfile].goals[goalIndex].compFv
                    .messagePath
              })}
              onChange={value => {
                onChange({
                  key: "compFv",
                  value:
                    value !== "0" || value !== ""
                      ? currencyToNumber(value)
                      : "",
                  goalIndex
                });
              }}
            />
          </View>

          <View>
            <View style={styles.titleRow}>
              <Text style={styles.analysisProductPointTitle}>
                {`4. ${EABi18n({
                  language,
                  textStore,
                  path: "na.needs.psGoals.assets"
                })} `}
              </Text>
              <PopoverTooltip
                popoverOptions={{ preferredContentSize: [365, 65] }}
                text={EABi18n({
                  textStore,
                  language,
                  path: "na.needs.assets.toolTip"
                })}
              />
            </View>
            <EASEAssets
              testID={`NeedsAnalysis__PSG__ValueOfAssets__gp${goalIndex}`}
              style={styles.analysisAssetsWrapper}
              selectedProduct="psGoals"
              selectedProfile={selectedProfile}
              selectedAssets={selectedAssets[goalIndex].assets}
              addAssetsOnPress={() => {
                let cloneAssets = _.cloneDeep(selectedAssets[goalIndex].assets);
                if (!cloneAssets) {
                  cloneAssets = [];
                }
                cloneAssets.push({
                  calAsset: 0,
                  key: "all",
                  usedAsset: 0
                });
                onChange({ value: cloneAssets, key: "assets", goalIndex });
              }}
              removeAssetsOnPress={i => {
                const cloneAssets = _.cloneDeep(
                  selectedAssets[goalIndex].assets
                );
                cloneAssets.forEach((assetsData, index) => {
                  if (i === index) {
                    cloneAssets.splice(index, 1);
                    onChange({ value: cloneAssets, key: "assets", goalIndex });
                  }
                });
              }}
              assetTextOnChange={(value, index) => {
                const cloneAssets = _.cloneDeep(
                  selectedAssets[goalIndex].assets
                );
                if (selectedAssets[goalIndex].assets[index].return) {
                  cloneAssets[index] = {
                    key: selectedAssets[goalIndex].assets[index].key,
                    usedAsset: currencyToNumber(value),
                    return: selectedAssets[goalIndex].assets[index].return
                  };
                } else {
                  cloneAssets[index] = {
                    key: selectedAssets[goalIndex].assets[index].key,
                    usedAsset: currencyToNumber(value)
                  };
                }
                onChange({ value: cloneAssets, key: "assets", goalIndex });
              }}
              assetSelectorOnChange={(itemKey, item) => {
                if (
                  !selectedAssets[goalIndex].assets.find(
                    select => select.key === item.value
                  )
                ) {
                  let cloneAssets = _.cloneDeep(
                    selectedAssets[goalIndex].assets
                  );
                  if (selectedAssets[goalIndex].assets[item.prevData.index]) {
                    cloneAssets[item.prevData.index] = {
                      calAsset: 0,
                      key: item.value,
                      usedAsset: 0
                    };
                  } else {
                    cloneAssets = selectedAssets[goalIndex].assets.concat({
                      calAsset: 0,
                      key: item.value,
                      usedAsset: 0
                    });
                  }
                  onChange({ value: cloneAssets, key: "assets", goalIndex });
                }
              }}
              rateOfReturn
              returnOnChange={(value, index) => {
                const cloneAssets = _.cloneDeep(
                  selectedAssets[goalIndex].assets
                );
                cloneAssets[index] = {
                  key: selectedAssets[goalIndex].assets[index].key,
                  usedAsset: selectedAssets[goalIndex].assets[index].usedAsset,
                  return: value
                };
                onChange({ value: cloneAssets, key: "assets", goalIndex });
              }}
              isProtectionError={
                isPsGoalsError[selectedProfile].goals[goalIndex]
              }
              calOtherAsset={({ key, selectedProduct }) =>
                calOtherAsset({
                  key,
                  selectedProduct,
                  selectedProfile,
                  na,
                  goalIndex
                })
              }
            />
          </View>

          <Text style={styles.analysisProductPointTitle}>
            {`5. ${EABi18n({
              language,
              textStore,
              path: "na.needs.psGoals.projMaturity"
            })}`}
          </Text>
          <View style={styles.analysisProductPointContent}>
            <EABTextField
              testID={`NeedsAnalysis__PSG__txtMaturityValue__gp${goalIndex}`}
              style={styles.analysisTextInputShort}
              prefix="$"
              value={numberToCurrency({
                value: goal.projMaturity || 0,
                decimal: 0
              })}
              onChange={value => {
                onChange({
                  key: "projMaturity",
                  value:
                    value !== "0" || value !== ""
                      ? currencyToNumber(value)
                      : "",
                  goalIndex
                });
              }}
            />
          </View>
        </View>
        <View style={styles.analysisSectionBorderLine} />
      </View>
    ));
  }

  renderShortfall(language) {
    const { selectedGoals } = this;
    const { textStore } = this.props;

    return selectedGoals.map((goal, index) => {
      const isTotShortfallNegative = goal.totShortfall < 0;

      return (
        <View
          style={[
            styles.shortfallView,
            {
              marginBottom: 0,
              bottom: index === 0 ? 55 : 55 * (index + 1)
            }
          ]}
        >
          <PopoverTooltip
            popoverOptions={{ preferredContentSize: [350, 100] }}
            text={EABi18n({
              textStore,
              language,
              path: isTotShortfallNegative
                ? "na.needs.common.shortfallContent"
                : "na.needs.common.surplusContent"
            })}
          />

          <Text
            style={
              isTotShortfallNegative
                ? Theme.headlineNegative
                : Theme.headlinePositive
            }
          >
            {` ${fillTextVariable({
              text: EABi18n({
                path: isTotShortfallNegative
                  ? "na.needs.psGoals.shortfall"
                  : "na.needs.psGoals.surplus",
                language,
                textStore
              }),
              data: [
                {
                  value: index + 1,
                  isPath: false
                }
              ],
              EABi18n,
              language,
              textStore
            })} $${numberToCurrency({
              value: Math.abs(goal.totShortfall || 0),
              decimal: 2
            })}`}
          </Text>
        </View>
      );
    });
  }

  render() {
    const { onChange, data } = this;
    const { textStore, selectedProfile, isPsGoalsError } = this.props;

    return (
      <ContextConsumer>
        {({ language }) => (
          <View>
            <KeyboardAwareScrollView extraScrollHeight={100}>
              <View key={123} style={styles.analysisProductContent}>
                <Text style={styles.title}>
                  {EABi18n({
                    language,
                    textStore,
                    path: "na.needs.psGoals"
                  })}
                </Text>
                <Text style={styles.analysisProductPointTitle}>
                  {EABi18n({
                    language,
                    textStore,
                    path: "na.needs.psGoals.goalNo"
                  })}
                  <Text style={styles.requiredHint}> *</Text>
                </Text>
                {isPsGoalsError[selectedProfile].goalNo.hasError ? (
                  <View>
                    <Text style={Theme.fieldTextOrHelperTextNegative}>
                      {EABi18n({
                        textStore,
                        language,
                        path: isPsGoalsError[selectedProfile].goalNo.messagePath
                      })}
                    </Text>
                  </View>
                ) : null}
                <EABStepper
                  testID="NeedsAnalysis__PSG"
                  value={data.goalNo || 1}
                  step={1}
                  defaultValue={1}
                  min={1}
                  max={3}
                  onChange={value => {
                    onChange({ key: "goalNo", value });
                  }}
                />
                <View style={styles.analysisSectionBorderLine} />
              </View>
              {this.renderGoals(language)}
              <View style={{ marginBottom: Theme.alignmentXXXXXL }} />
            </KeyboardAwareScrollView>
            {this.renderShortfall(language)}
          </View>
        )}
      </ContextConsumer>
    );
  }
}

AnalysisPlanningForSpecificGoals.propTypes = {
  na: WEB_API_REDUCER_TYPE_CHECK[FNA].na.isRequired,
  selectedProfile: PropTypes.string.isRequired,
  textStore: APP_REDUCER_TYPE_CHECK[TEXT_STORE].isRequired,
  initNaAnalysis: PropTypes.func.isRequired,
  updateNA: PropTypes.func.isRequired,
  fe: WEB_API_REDUCER_TYPE_CHECK[FNA].fe.isRequired,
  pda: WEB_API_REDUCER_TYPE_CHECK[FNA].pda.isRequired,
  profile: PropTypes.oneOfType([PropTypes.object]).isRequired,
  dependantProfiles:
    WEB_API_REDUCER_TYPE_CHECK[CLIENT].dependantProfiles.isRequired,
  // TODO
  isPsGoalsError: PropTypes.oneOfType([PropTypes.object]).isRequired
};
