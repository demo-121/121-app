import {
  REDUCER_TYPE_CHECK,
  REDUCER_TYPES,
  SYSTEM,
  PRODUCT_TYPES,
  utilities
} from "eab-web-api";
import moment from "moment";
import React, { Component } from "react";
import {
  Dimensions,
  Image,
  ScrollView,
  Text,
  View,
  Alert,
  TouchableOpacity
} from "react-native";
import PropTypes from "prop-types";
import * as _ from "lodash";
import LinearGradient from "react-native-linear-gradient";
import ckaImage from "../../../../assets/images/cka.jpg";
import feImage from "../../../../assets/images/fe.png";
import naImage from "../../../../assets/images/na.png";
import pdaImage from "../../../../assets/images/pda.png";
import raImage from "../../../../assets/images/ra.jpg";
/* TODO: Enable in future version.
import icArrowActive from "../../../../assets/images/icArrowActive.png"; */
import { RECTANGLE_BUTTON_1 } from "../../../../components/EABButton/constants";
import TranslatedText from "../../../../containers/TranslatedText";
import { ContextConsumer } from "../../../../context";
import noRecords from "../../../../assets/images/noRecords.png";
import EABBadge from "../../../../components/EABBadge";
import EABButton from "../../../../components/EABButton";
import EABCard from "../../../../components/EABCard";
import EABDialog from "../../../../components/EABDialog";
import EABi18n from "../../../../utilities/EABi18n";
import EABSegmentedControl from "../../../../components/EABSegmentedControl";
import * as DIALOG_TYPES from "../../../../components/EABDialog/constants";
import Theme from "../../../../theme";
import ApplicantsAndDependents from "../../containers/ApplicantsAndDependants";
import FinancialEvaluation from "../../containers/FinancialEvaluation";
import ClientFormProfile from "../../../../containers/ClientFormProfile";
import ClientFormContact from "../../../../containers/ClientFormContact";
import APP_REDUCER_TYPE_CHECK from "../../../../constants/REDUCER_TYPE_CHECK";
import { TEXT_STORE } from "../../../../constants/REDUCER_TYPES";
import CustomerKnowledgeAssessment from "../../containers/CustomerKnowledgeAssessment";
import NeedsAnalysis from "../../containers/NeedsAnalysis";
import RiskAssessment from "../../containers/RiskAssessment";
import styles from "./styles";
import EABHUD from "../../../../containers/EABHUD";

const { CLIENT, FNA } = REDUCER_TYPES;
const { FEATURES } = SYSTEM;
const { INVESTLINKEDPLANS, PARTICIPATINGPLANS } = PRODUCT_TYPES;
const { genProfileMandatory } = utilities;
const { isClientFormHasError } = utilities.validation;

/**
 * NeedsDetail
 * @description NeedsDetail page.
 * @param {object} navigation - react navigation props
 * */
export default class NeedsDetail extends Component {
  constructor(props) {
    super(props);

    this.state = {
      applicantsAndDependentsDialogIsOpen: false,
      financialEvaluationDialogIsOpen: false,
      isShowingCreateProfileDialog: false,
      showProfileTab: "A",
      needsAnalysisDialogIsOpen: false,
      customerKnowledgeAssessmentDialogIsOpen: false,
      riskAssessmentDialogIsOpen: false,
      isLoading: false
    };

    this.segmentedControlArray = [
      {
        key: "A",
        title: "Profile",
        isShowingBadge: false,
        onPress: () => {
          this.setState({ showProfileTab: true });
        }
      },
      {
        key: "B",
        title: "Contact",
        isShowingBadge: false,
        onPress: () => {
          this.setState({ showProfileTab: false });
        }
      }
    ];

    this.getFNA = this.getFNAWrapper.bind(this);
    this.getErrorView = this.getErrorView.bind(this);
    this.isValidFNA = this.isValidFNA.bind(this);
    this.navigateToNA = this.navigateToNA.bind(this);
    this.navigateToFE = this.navigateToFE.bind(this);
  }

  componentDidMount() {
    this.didFocusSubscription = this.props.navigation.addListener(
      "didFocus",
      () => {
        this.getFNAWrapper();
      }
    );
  }

  componentWillUnmount() {
    this.didFocusSubscription.remove();
  }

  // ===========================================================================
  // getters
  // ===========================================================================
  /**
   * @description indicate can save client or not
   * @return {boolean} errorStatus
   * */
  get isDisableSaveClient() {
    const { clientForm } = this.props;
    const { hasError } = isClientFormHasError(clientForm);
    return hasError;
  }

  getFNAWrapper() {
    const { getFNA } = this.props;

    this.setState(
      {
        isLoading: true
      },
      () => {
        getFNA(() => {
          this.setState({
            isLoading: false
          });
        });
      }
    );
  }

  getCompletedView(language) {
    const { navigateToNA, navigateToFE } = this;
    const { pda, textStore, na } = this.props;
    const naCompletedRule =
      na.completedStep === 4 &&
      this.props.pda.isCompleted &&
      this.props.fe.isCompleted &&
      !_.isEmpty(na.productType.prodType);

    this.itemNumbersInOneRow = Dimensions.get("window").width >= 1352 ? 4 : 3;
    this.stepRowArray = [];

    this.stepArray = [
      {
        key: "a",
        image: pdaImage,
        testID: "PersonalDataAcknowlegdement",
        title: "needs.steps.pda.title",
        description: "needs.steps.pda.description",
        isCompleted: this.props.pda.isCompleted,
        notCompleteAlertTextPath: "needs.alert.notCompletedPDA",
        onPress: () => {
          if (pda.applicant === "joint" && this.relationShipHasError()) {
            Alert.alert(
              null,
              EABi18n({
                textStore,
                language,
                path: "needs.alert.reviewSpouseProfile"
              }),
              [
                {
                  text: EABi18n({
                    textStore,
                    language,
                    path: "button.ok"
                  }),
                  onPress: () => {}
                }
              ],
              { cancelable: false }
            );
          } else {
            this.setState({ applicantsAndDependentsDialogIsOpen: true });
          }
        }
      },
      {
        key: "b",
        image: feImage,
        testID: "FinancialEvaluation",
        title: "needs.steps.fe.title",
        description: "needs.steps.fe.description",
        isCompleted: this.props.fe.isCompleted,
        notCompleteAlertTextPath: "needs.alert.notCompletedFE",
        onPress: navigateToFE
      },
      {
        key: "c",
        image: naImage,
        testID: "NeedsAnalysis",
        title: "needs.steps.na.title",
        description: "needs.steps.na.description",
        isCompleted: naCompletedRule, // can't use this.props.na.isCompleted since the behavior is not match to web
        notCompleteAlertTextPath: "",
        onPress: navigateToNA
      }
    ];

    if (
      naCompletedRule &&
      na.productType &&
      na.productType.prodType.split(",").indexOf(INVESTLINKEDPLANS) > -1
    ) {
      this.stepArray.push({
        key: "cka",
        image: ckaImage,
        testID: "CustomerKnowledgeAssessment",
        title: "needs.steps.cka.title",
        description: "needs.steps.cka.description",
        isCompleted:
          na.ckaSection && na.ckaSection.isValid !== undefined
            ? na.ckaSection.isValid
            : false,
        notCompleteAlertTextPath: "",
        onPress: () => {
          this.setState({ customerKnowledgeAssessmentDialogIsOpen: true });
        }
      });
    }

    if (
      naCompletedRule &&
      na.productType &&
      (na.productType.prodType.split(",").indexOf(INVESTLINKEDPLANS) > -1 ||
        na.productType.prodType.split(",").indexOf(PARTICIPATINGPLANS) > -1)
    ) {
      this.stepArray.push({
        key: "ra",
        image: raImage,
        testID: "RiskProfileAssessment",
        title: "needs.steps.ra.title",
        description: "needs.steps.ra.description",
        isCompleted:
          na.raSection && na.raSection.isValid !== undefined
            ? na.raSection.isValid
            : false,
        notCompleteAlertTextPath: "",
        onPress: () => {
          this.setState({ riskAssessmentDialogIsOpen: true });
        }
      });
    }

    for (
      let i = 0;
      i <= this.stepArray.length / this.itemNumbersInOneRow;
      i += 1
    ) {
      this.stepRowArray.push({ key: i });
    }

    return true;
  }

  getErrorView(language) {
    const missingDataTitle = "needs.text.missingDataTitle";
    const missingDataHint = "needs.text.missingDataHint";
    const goTo = "needs.button.goToProfile";

    return (
      <View style={styles.noRecordsContainer}>
        <View style={styles.noRecordsView}>
          <Image source={noRecords} />
          <TranslatedText
            style={styles.missingDataTitle}
            language={language}
            path={missingDataTitle}
          />
          <TranslatedText
            style={styles.missingDataHint}
            language={language}
            path={missingDataHint}
          />
          <EABButton
            testID="btnGoToProfile"
            buttonType={RECTANGLE_BUTTON_1}
            onPress={() => {
              this.props.getProfileData(this.props.profile.cid, () => {
                this.props.readyToEditProfile(() => {
                  this.setState({
                    isShowingCreateProfileDialog: true
                  });
                });
              });
            }}
          >
            <TranslatedText language={language} path={goTo} />
            {/* TODO: Enable in future version. */}
            {/* <Image
              style={styles.seeAllButtonIcon}
              source={icArrowActive}
            /> */}
          </EABButton>
        </View>
      </View>
    );
  }

  relationShipHasError() {
    const dependants = Object.values(this.props.dependantProfiles);
    let hasError = false;

    for (let i = 0; i < dependants.length; i += 1) {
      if (_.isEmpty(dependants[i].relationship)) {
        hasError = true;
      }
    }

    return hasError;
  }

  isValidFNA() {
    const conditions = genProfileMandatory(FEATURES.FNA, true, false, false);
    for (let i = 0; i < conditions.length; i += 1) {
      if (this.props.profile[conditions[i]] === "") return false;
    }
    return true;
  }

  navigateToNA() {
    const {
      initialValidateNA,
      profile,
      dependantProfiles,
      na,
      fe
    } = this.props;

    initialValidateNA({
      naData: na,
      profileData: profile,
      dependantProfilesData: dependantProfiles,
      feData: fe
    });
    this.setState({ needsAnalysisDialogIsOpen: true });
  }

  navigateToFE() {
    const {
      initialValidateFE,
      fe,
      profile,
      dependantProfiles,
      pda
    } = this.props;

    if (pda.isCompleted) {
      initialValidateFE({
        feData: fe,
        pdaData: pda,
        profileData: profile,
        dependantProfilesData: dependantProfiles
      });
      this.setState({ financialEvaluationDialogIsOpen: true });
    }
  }

  /**
   * @name renderClientForm
   * @return {element} edit client dialog component
   * */
  renderClientForm(language) {
    const { textStore } = this.props;
    return (
      <EABDialog isOpen={this.state.isShowingCreateProfileDialog}>
        <View style={Theme.dialogHeader}>
          <View style={styles.headerRow}>
            <EABButton
              testID="btnClientFormClose"
              onPress={() => {
                this.setState({ isShowingCreateProfileDialog: false });
                this.props.cancelCreateClient();
              }}
            >
              <Text style={Theme.textButtonLabelNormalAccent}>
                {EABi18n({
                  textStore,
                  language,
                  path: "button.cancel"
                })}
              </Text>
            </EABButton>
            <EABSegmentedControl
              segments={this.segmentedControlArray}
              activatedKey={this.state.showProfileTab ? "A" : "B"}
            />
            <EABButton
              testID="btnClientFormDone"
              onPress={() => {
                this.props.saveClient(() => {
                  this.setState(
                    {
                      isShowingCreateProfileDialog: false
                    },
                    () => {}
                  );
                });
              }}
              isDisable={this.isDisableSaveClient}
            >
              <Text style={Theme.textButtonLabelNormalAccent}>
                {EABi18n({
                  textStore,
                  language,
                  path: "button.done"
                })}
              </Text>
            </EABButton>
          </View>
        </View>
        {this.state.showProfileTab ? (
          <ClientFormProfile />
        ) : (
          <ClientFormContact />
        )}
      </EABDialog>
    );
  }

  render() {
    /**
     * render functions
     * */
    const textButtonLabelStyle = ({ step, index }) => {
      if (index === 0 || step.isCompleted || step.key === "e") {
        return Theme.textButtonLabelSmallAccent;
      } else if (this.stepArray[index - 1].isCompleted) {
        return Theme.textButtonLabelSmallAccent;
      }
      return Theme.textButtonLabelDisabled;
    };

    /**
     * render text functions
     * */
    const getIncompleteOrLockText = (step, index) => {
      if (index === 0 || step.isCompleted || step.key === "ra") {
        return "needs.text.incomplete";
      } else if (this.stepArray[index - 1].isCompleted) {
        return "needs.text.incomplete";
      }
      return "needs.text.lock";
    };

    const { navigateToNA, navigateToFE } = this;
    const { textStore } = this.props;

    const renderStep = language => {
      this.getCompletedView(language);

      return this.stepRowArray.map((stepRow, rowIndex) => (
        <View
          key={stepRow.key}
          style={[
            styles.stepRow,
            { marginTop: rowIndex === 0 ? 0 : Theme.alignmentXL }
          ]}
        >
          {this.stepArray.map(
            (step, index) =>
              index >= rowIndex * this.itemNumbersInOneRow &&
              index < (rowIndex + 1) * this.itemNumbersInOneRow ? (
                <TouchableOpacity
                  testID={`Needs__btn${step.testID}`}
                  activeOpacity={1}
                  onPress={() => {
                    let alertPath = "";
                    const previousStepIsIncomplete = this.stepArray.some(
                      (checkStep, checkIndex) => {
                        alertPath = checkStep.notCompleteAlertTextPath;
                        return (
                          checkStep.key !== "cka" &&
                          checkStep.key !== "ra" &&
                          checkIndex < index &&
                          !checkStep.isCompleted
                        );
                      }
                    );

                    if (previousStepIsIncomplete) {
                      Alert.alert(
                        null,
                        EABi18n({
                          path: alertPath,
                          textStore,
                          language
                        }),
                        [
                          {
                            text: EABi18n({
                              path: "button.ok",
                              textStore,
                              language
                            }),
                            onPress: () => {}
                          }
                        ],
                        { cancelable: false }
                      );
                    } else {
                      step.onPress();
                    }
                  }}
                >
                  <EABBadge
                    key={step.key}
                    isShow={step.isCompleted}
                    style={{
                      flex: 1,
                      alignItems: "flex-start",
                      marginLeft:
                        index === rowIndex * this.itemNumbersInOneRow
                          ? 0
                          : Theme.alignmentXL
                    }}
                  >
                    <EABCard
                      style={styles.stepContainer}
                      contentStyle={styles.step}
                    >
                      <Image style={styles.stepImage} source={step.image} />
                      <View style={styles.stepContent}>
                        <Text style={styles.stepNumber}>{`${index + 1}.`}</Text>
                        <View style={styles.stepMainContent}>
                          <View style={styles.stepTextWrapper}>
                            <Text style={styles.stepContentTitle}>
                              {EABi18n({
                                path: step.title,
                                textStore,
                                language
                              })}
                            </Text>
                            <Text style={Theme.subheadSecondary}>
                              {step.isCompleted
                                ? `${EABi18n({
                                    path: "needs.text.lastCompleted",
                                    textStore,
                                    language
                                  })} ${moment(this.props.pda.lastUpd).format(
                                    "DD MMM YYYY"
                                  )}`
                                : EABi18n({
                                    path: step.description,
                                    textStore,
                                    language
                                  })}
                            </Text>
                          </View>
                          <EABButton
                            isDisable={
                              step.key === "a" ||
                              step.key === "ra" ||
                              step.isCompleted
                                ? false
                                : !this.stepArray[index - 1].isCompleted
                            }
                            onPress={step.onPress}
                          >
                            <Text
                              style={textButtonLabelStyle({
                                step,
                                index
                              })}
                            >
                              {step.isCompleted ? (
                                EABi18n({
                                  language,
                                  textStore,
                                  path: "needs.text.edit"
                                }).toUpperCase()
                              ) : (
                                <Text style={styles.incompleteText}>
                                  {EABi18n({
                                    language,
                                    textStore,
                                    path: getIncompleteOrLockText(step, index)
                                  }).toUpperCase()}
                                </Text>
                              )}
                            </Text>
                          </EABButton>
                        </View>
                      </View>
                    </EABCard>
                  </EABBadge>
                </TouchableOpacity>
              ) : null
          )}
        </View>
      ));
    };

    return (
      <ContextConsumer>
        {({ language }) => (
          <LinearGradient
            {...Theme.LinearGradientProps}
            style={styles.container}
          >
            <ScrollView
              style={styles.scrollView}
              showsVerticalScrollIndicator={false}
              contentContainerStyle={styles.scrollViewContent}
            >
              {this.isValidFNA() ? (
                <View style={styles.stepPage}>
                  <View style={styles.header}>
                    <Text style={styles.headerText}>
                      Protect yourself and your loved ones.
                    </Text>
                    {/* TODO: Enable in future version. */}
                    {/* <EABButton
                  onPress={() => {
                    this.props.navigation.navigate({ routeName: "LearnMore" });
                  }}
                >
                  <Text style={Theme.textButtonLabelSmallAccent}>Learn More</Text>
                  <Image
                    style={styles.learnMoreButtonIcon}
                    source={icArrowActive}
                  />
                </EABButton> */}
                  </View>
                  {renderStep(language)}
                </View>
              ) : (
                this.getErrorView(language)
              )}
            </ScrollView>
            <EABDialog
              type={DIALOG_TYPES.SCREEN}
              isOpen={this.state.applicantsAndDependentsDialogIsOpen}
            >
              <ApplicantsAndDependents
                closeDialog={() => {
                  this.setState({ applicantsAndDependentsDialogIsOpen: false });
                }}
                switchToFE={() => {
                  this.setState(
                    { applicantsAndDependentsDialogIsOpen: false },
                    () => {
                      setTimeout(navigateToFE, 200);
                    }
                  );
                }}
              />
            </EABDialog>
            <EABDialog
              type={DIALOG_TYPES.SCREEN}
              isOpen={
                this.props.fnaPage === "fe" ||
                this.state.financialEvaluationDialogIsOpen
              }
            >
              <FinancialEvaluation
                closeDialog={() => {
                  this.props.clearSelectedPage();
                  this.setState({ financialEvaluationDialogIsOpen: false });
                }}
                switchToPDA={() => {
                  this.props.clearSelectedPage();
                  this.setState(
                    { financialEvaluationDialogIsOpen: false },
                    () => {
                      setTimeout(() => {
                        this.setState({
                          applicantsAndDependentsDialogIsOpen: true
                        });
                      }, 200);
                    }
                  );
                }}
                switchToNA={() => {
                  this.props.clearSelectedPage();
                  this.setState(
                    { financialEvaluationDialogIsOpen: false },
                    () => {
                      setTimeout(navigateToNA, 200);
                    }
                  );
                }}
              />
            </EABDialog>
            <EABDialog
              type={DIALOG_TYPES.SCREEN}
              isOpen={this.state.needsAnalysisDialogIsOpen}
            >
              <NeedsAnalysis
                closeDialog={() => {
                  this.setState({ needsAnalysisDialogIsOpen: false });
                }}
                openPreviousDialog={() => {
                  this.setState({ needsAnalysisDialogIsOpen: false }, () => {
                    setTimeout(navigateToFE, 200);
                  });
                }}
                openCKADialog={() => {
                  this.setState({
                    needsAnalysisDialogIsOpen: false,
                    customerKnowledgeAssessmentDialogIsOpen: true
                  });
                }}
                openRADialog={() => {
                  this.setState({
                    needsAnalysisDialogIsOpen: false,
                    riskAssessmentDialogIsOpen: true
                  });
                }}
                toProductPage={() => {
                  this.setState(
                    {
                      needsAnalysisDialogIsOpen: false
                    },
                    () => {
                      this.props.toProductPage(
                        this.props.navigation.navigate({
                          routeName: "Products"
                        })
                      );
                    }
                  );
                }}
              />
            </EABDialog>
            <EABDialog
              type={DIALOG_TYPES.SCREEN}
              isOpen={this.state.customerKnowledgeAssessmentDialogIsOpen}
            >
              <CustomerKnowledgeAssessment
                closeDialog={() => {
                  this.setState({
                    customerKnowledgeAssessmentDialogIsOpen: false
                  });
                }}
                openNADialog={() => {
                  this.setState({
                    customerKnowledgeAssessmentDialogIsOpen: false,
                    needsAnalysisDialogIsOpen: true
                  });
                }}
                openRADialog={() => {
                  this.setState({
                    customerKnowledgeAssessmentDialogIsOpen: false,
                    riskAssessmentDialogIsOpen: true
                  });
                }}
              />
            </EABDialog>
            <EABDialog
              type={DIALOG_TYPES.SCREEN}
              isOpen={this.state.riskAssessmentDialogIsOpen}
            >
              <RiskAssessment
                closeDialog={() => {
                  this.setState({
                    riskAssessmentDialogIsOpen: false
                  });
                }}
                openNADialog={() => {
                  this.setState({
                    riskAssessmentDialogIsOpen: false,
                    needsAnalysisDialogIsOpen: true
                  });
                }}
                openCKADialog={() => {
                  this.setState({
                    riskAssessmentDialogIsOpen: false,
                    customerKnowledgeAssessmentDialogIsOpen: true
                  });
                }}
              />
            </EABDialog>
            {this.renderClientForm(language)}
            <EABHUD isOpen={this.state.isLoading} />
          </LinearGradient>
        )}
      </ContextConsumer>
    );
  }
}

NeedsDetail.propTypes = {
  fnaPage: PropTypes.string.isRequired,
  clearSelectedPage: PropTypes.func.isRequired,
  getProfileData: PropTypes.func.isRequired,
  getFNA: PropTypes.func.isRequired,
  profile: REDUCER_TYPE_CHECK[CLIENT].profile.isRequired,
  readyToEditProfile: PropTypes.func.isRequired,
  textStore: APP_REDUCER_TYPE_CHECK[TEXT_STORE].isRequired,
  saveClient: PropTypes.func.isRequired,
  initialValidateFE: PropTypes.func.isRequired,
  initialValidateNA: PropTypes.func.isRequired,
  cancelCreateClient: PropTypes.func.isRequired,
  toProductPage: PropTypes.func.isRequired,
  na: REDUCER_TYPE_CHECK[FNA].na.isRequired,
  clientForm: PropTypes.oneOfType([PropTypes.object]).isRequired,
  pda: REDUCER_TYPE_CHECK[FNA].pda.isRequired,
  fe: REDUCER_TYPE_CHECK[FNA].fe.isRequired,
  dependantProfiles: REDUCER_TYPE_CHECK[CLIENT].dependantProfiles.isRequired
};
