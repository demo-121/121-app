import { StyleSheet } from "react-native";
import Theme from "../../theme";

export default StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "flex-start",
    alignItems: "center"
  },
  image: {
    width: 500,
    height: 500
  },
  signDocImage: {
    width: 750, // if use %, the program will keep loading.
    height: 1000,
    resizeMode: "contain",
    marginBottom: Theme.alignmentXL
  }
});
