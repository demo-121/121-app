import { StyleSheet } from "react-native";

export default StyleSheet.create({
  container: {
    width: "100%",
    height: "100%"
  },
  indicatorContainer: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "space-around",
    alignItems: "center",
    padding: 10
  },
  onlineAuthWrap: {
    flex: 1
  },
  appPasswordWrap: {
    flex: 1
  },
  appPasswordLoginWrap: {
    flex: 1
  },
  coverPage: {
    flex: 1
  }
});
