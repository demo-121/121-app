import PropTypes from "prop-types";
import React, { PureComponent } from "react";
import { Alert, Image, View } from "react-native";
import arrowIcon from "../../../../assets/images/back-icon.png";
import EABButton from "../../../../components/EABButton";
import REDUCER_TYPE_CHECK from "../../../../constants/REDUCER_TYPE_CHECK";
import { TEXT_STORE } from "../../../../constants/REDUCER_TYPES";
import EABHUD from "../../../../containers/EABHUD";
import TranslatedText from "../../../../containers/TranslatedText";
import { ContextConsumer } from "../../../../context";
import Theme from "../../../../theme";
import EABi18n from "../../../../utilities/EABi18n";
import { CLONE, RE_QUOTE } from "./constants";
import styles from "./styles";

/**
 * <ProposalHeaderLeft />
 * @description navigation bar left component
 * */
export default class ProposalHeaderLeft extends PureComponent {
  constructor() {
    super();

    this.state = {
      isLoading: false
    };
  }
  render() {
    // =========================================================================
    // variables
    // =========================================================================
    const {
      clone,
      reQuote,
      quotationId,
      textStore,
      type,
      back,
      closeSummaryProposal
    } = this.props;
    const { isLoading } = this.state;

    return (
      <ContextConsumer>
        {({ language }) => (
          <View style={styles.row}>
            <Image style={styles.arrow} source={arrowIcon} />
            <EABButton
              testID="Proposal__btnRequote"
              style={styles.button}
              onPress={() => {
                this.setState(
                  {
                    isLoading: true
                  },
                  () => {
                    closeSummaryProposal();
                    switch (type) {
                      case CLONE:
                        clone({
                          quotationId,
                          callback: () => {
                            this.setState(
                              {
                                isLoading: false
                              },
                              () => {
                                setTimeout(back, 200);
                              }
                            );
                          },
                          cloneAlert: () => {
                            this.setState(
                              {
                                isLoading: false
                              },
                              () => {
                                setTimeout(() => {
                                  Alert.alert(
                                    null,
                                    EABi18n({
                                      language,
                                      textStore,
                                      path: "proposal.error.clone"
                                    }),
                                    [
                                      {
                                        text: EABi18n({
                                          language,
                                          textStore,
                                          path: "button.ok"
                                        }),
                                        onPress: () => {}
                                      }
                                    ]
                                  );
                                }, 200);
                              }
                            );
                          }
                        });
                        break;
                      case RE_QUOTE:
                        reQuote({
                          quotationId,
                          callback: () => {
                            this.setState(
                              {
                                isLoading: false
                              },
                              () => {
                                setTimeout(back, 200);
                              }
                            );
                          }
                        });
                        break;
                      default:
                        throw new Error(
                          `unexpected ProposalHeaderLeft type: ${type}`
                        );
                    }
                  }
                );
              }}
            >
              <TranslatedText
                style={Theme.textButtonLabelNormalAccent}
                path="proposal.button.requote"
              />
            </EABButton>
            <EABHUD isOpen={isLoading} />
          </View>
        )}
      </ContextConsumer>
    );
  }
}

ProposalHeaderLeft.propTypes = {
  type: PropTypes.oneOf([CLONE, RE_QUOTE]).isRequired,
  textStore: REDUCER_TYPE_CHECK[TEXT_STORE].isRequired,
  quotationId: PropTypes.string.isRequired,
  reQuote: PropTypes.func.isRequired,
  clone: PropTypes.func.isRequired,
  back: PropTypes.func.isRequired,
  closeSummaryProposal: PropTypes.func.isRequired
};
