import React, { PureComponent } from "react";
import {
  View,
  Text,
  TouchableWithoutFeedback,
  Alert,
  Image
} from "react-native";
import PropTypes from "prop-types";
import * as _ from "lodash";
import {
  FILTER_TYPES,
  REDUCER_TYPES,
  REDUCER_TYPE_CHECK,
  DIALOG_TYPES,
  DOCUMENT_TYPES,
  EAPP
} from "eab-web-api";
import APP_REDUCER_TYPE_CHECK from "../../../../constants/REDUCER_TYPE_CHECK";
import { TEXT_STORE } from "../../../../constants/REDUCER_TYPES";
import { ContextConsumer } from "../../../../context";
import EABCheckBox from "../../../../components/EABCheckBox";
import EABButton from "../../../../components/EABButton";
import EABHUD from "../../../../components/EABHUD";
import TranslatedText from "../../../../containers/TranslatedText";
import styles from "./styles";
import EABi18n from "../../../../utilities/EABi18n";
import getEABTextByLangType from "../../../../utilities/getEABTextByLangType";
import confirmationAlert from "../../../../utilities/confirmationAlert";
import icWarning from "../../../../assets/images/icWarning.png";
import icInfoActive from "../../../../assets/images/icInfoActive.png";
import theme from "../../../../theme";

const { CLIENT, PRE_APPLICATION } = REDUCER_TYPES;
const { ERROR, CONFIRMATION, OTHER, INVALID_DEFAULT } = DIALOG_TYPES;
const { APPLICATION_NORMAL, APPLICATION_MASTER } = DOCUMENT_TYPES;

/**
 * ApplicationItemHeader
 * @description header of application item in ApplicationsList inside Application Summary page.
 * @param {object} value - value of application / quotation item object
 * @param {bool} selectModeOn - control UI if select is clicked in Navigation bar
 * @param {array} selectedIdList - store selected quotation ids
 * @param {string} selectedAppListFilter - indicate which filter is selected
 * @param {bool} recommendCompleted - indicate if recommendation is completed
 * @param {array} recommendChoiceList - store chosen list and not chosen list for recommendation
 * @param {string} recommendChoiceList.id - quotation id
 * @param {string} recommendChoiceList.checked - indicate if the quotation is chosen
 * @param {bool} isFaChannel - indicate if current channel is FA or not
 * @param {func} updateUiSelectedList - saga action, update selected list in Summary screen
 * @param {func} updateRecommendChoiceList - saga action, update recommendation choice list in Summary screen
 * @param {func} openProposal - saga action, open Proposal Dialog after clicking View Proposal button
 * */

class ApplicationItemHeader extends PureComponent {
  constructor(props) {
    super(props);

    this.checkboxOnClick = this.checkboxOnClick.bind(this);
    this.requoteWithConfirm = this.requoteWithConfirm.bind(this);
    this.requoteWithoutConfirm = this.requoteWithoutConfirm.bind(this);
    this.invalidateReasonAlert = this.invalidateReasonAlert.bind(this);

    this.state = {
      isLoading: false
    };
  }

  get isApplication() {
    const { value } = this.props;

    return (
      _.get(value, "type") === APPLICATION_NORMAL ||
      _.get(value, "type") === APPLICATION_MASTER
    );
  }

  get isInvalidated() {
    const { value } = this.props;
    return _.get(value, "appStatus") === EAPP.appStatus.INVALIDATED;
  }

  get isInvalidatedSigned() {
    const { value } = this.props;
    return _.get(value, "appStatus") === EAPP.appStatus.INVALIDATED_SIGNED;
  }

  get isSubmitted() {
    const { value } = this.props;
    return _.get(value, "appStatus") === EAPP.appStatus.SUBMITTED;
  }

  invalidateReasonAlert({ language }) {
    const { value, textStore } = this.props;
    const invalidateReason = _.get(value, "invalidateReason");
    const contentPath = `application.summary.item_header_invalidateReason_${invalidateReason}`;

    Alert.alert(
      EABi18n({
        language,
        textStore,
        path: "alert.warning"
      }),
      EABi18n({
        language,
        textStore,
        path: contentPath
      }),
      [
        {
          text: EABi18n({
            language,
            textStore,
            path: "button.ok"
          }),
          onPress: () => {}
        }
      ]
    );
  }

  isBundleValid(bundleId) {
    const { profile } = this.props;
    const bundleObject = profile.bundle.find(bundle => bundle.id === bundleId);
    return bundleObject ? bundleObject.isValid : false;
  }

  checkboxOnClick() {
    const { value, selectedIdList, updateUiSelectedList } = this.props;
    const { id } = value;
    const isSelected = selectedIdList.indexOf(id) >= 0;
    const nextIsSelected = !isSelected;

    updateUiSelectedList({ id, isSelected: nextIsSelected });
  }

  requoteWithConfirm(callback) {
    const { requoteInvalid, value } = this.props;
    const { id, quotation } = value;
    const quotationId = this.isApplication ? _.get(quotation, "id") : id;

    requoteInvalid({
      quotationId,
      confirm: true,
      callback: () => {
        this.setState({ isLoading: false }, () => {
          callback();
        });
      }
    });
  }

  requoteWithoutConfirm({ language, callback }) {
    const { textStore, requoteInvalid, value } = this.props;
    const { id, quotation } = value;

    const quotationId = this.isApplication ? _.get(quotation, "id") : id;

    requoteInvalid({
      quotationId,
      confirm: false,
      callback: dialogType => {
        this.setState(
          {
            isLoading: false
          },
          () => {
            setTimeout(() => {
              if (dialogType === CONFIRMATION) {
                confirmationAlert({
                  messagePath: "products.confirm.invalidateApplications",
                  language,
                  textStore,
                  yesOnPress: () => {
                    this.setState(
                      {
                        isLoading: true
                      },
                      () => {
                        this.requoteWithConfirm(callback);
                      }
                    );
                  }
                });
              } else if (dialogType === ERROR) {
                Alert.alert(
                  EABi18n({
                    language,
                    textStore,
                    path: "alert.warning"
                  }),
                  EABi18n({
                    language,
                    textStore,
                    path: "proposal.error.requoteInvalid"
                  }),
                  [
                    {
                      text: EABi18n({
                        language,
                        textStore,
                        path: "button.ok"
                      }),
                      onPress: () => {}
                    }
                  ]
                );
              } else if (dialogType === INVALID_DEFAULT) {
                Alert.alert(
                  EABi18n({
                    language,
                    textStore,
                    path: "alert.warning"
                  }),
                  EABi18n({
                    language,
                    textStore,
                    path: "proposal.error.requoteInvalidDefaultMsg"
                  }),
                  [
                    {
                      text: EABi18n({
                        language,
                        textStore,
                        path: "button.ok"
                      }),
                      onPress: () => {}
                    }
                  ]
                );
              } else {
                callback();
              }
            }, 200);
          }
        );
      }
    });
  }

  render() {
    const {
      textStore,
      value,
      selectedIdList,
      selectedAppListFilter,
      recommendCompleted,
      recommendChoiceList,
      isFaChannel,
      updateRecommendChoiceList,
      openProposal,
      routeToProposalUI,
      applyApplicationForm,
      continueApplicationForm,
      continueApplicationFormShield,
      checkBeforeApply,
      getSupportingDocument,
      comfirmApplicationPaid,
      updateApplicationStep,
      navigation,
      rowIndex
    } = this.props;
    const { isLoading } = this.state;
    const selectModeOn = navigation.getParam("selectModeOn", false);
    const {
      id,
      policyNumber,
      quotation,
      quotType,
      baseProductName,
      pName,
      iName,
      paymentMethod,
      pCid,
      iCid,
      iCids,
      isInitialPaymentCompleted,
      approvalStatus,
      isMandDocsAllUploaded,
      bundleId
    } = value;
    const isSelected = selectedIdList.indexOf(id) >= 0;
    const quotationId = this.isApplication ? _.get(quotation, "id") : id;
    const applicationId = this.isApplication ? id : "";
    const choiceChecked = _.find(
      recommendChoiceList,
      item => item.id === quotationId
    );
    const isChoiceChecked = !_.isEmpty(choiceChecked)
      ? choiceChecked.checked
      : false;
    const isShield = quotType === "SHIELD";

    const buttonRequote =
      this.isInvalidated &&
      [
        FILTER_TYPES.APPLICATION_LIST.ALL,
        FILTER_TYPES.APPLICATION_LIST.INVALIDATED
      ].indexOf(selectedAppListFilter) >= 0 ? (
        <ContextConsumer>
          {({ language, showQuotationNavigator }) => (
            <EABButton
              onPress={() => {
                this.setState(
                  {
                    isLoading: true
                  },
                  () => {
                    this.requoteWithoutConfirm({
                      language,
                      callback: () => {
                        showQuotationNavigator();
                      }
                    });
                  }
                );
              }}
            >
              <TranslatedText
                style={styles.buttonTitle}
                path="application.summary.item_requote"
                funcOnText={_.toUpper}
              />
            </EABButton>
          )}
        </ContextConsumer>
      ) : null;

    const buttonViewProposal =
      !this.isSubmitted &&
      this.isBundleValid(bundleId) &&
      (!this.isApplication ||
        selectedAppListFilter === FILTER_TYPES.APPLICATION_LIST.APPLYING ||
        (selectedAppListFilter === FILTER_TYPES.APPLICATION_LIST.ALL &&
          !this.isInvalidated)) ? (
        <ContextConsumer>
          {({ showQuotationNavigator }) => (
            <EABButton
              testID="PreAppilcation__ProductList__btnViewProposal"
              onPress={() => {
                this.setState(
                  {
                    isLoading: true
                  },
                  () => {
                    openProposal({
                      quotationId,
                      callback: () => {
                        this.setState({
                          isLoading: false
                        });
                        routeToProposalUI();
                        showQuotationNavigator();
                      }
                    });
                  }
                );
              }}
            >
              <TranslatedText
                style={styles.buttonTitle}
                path="application.summary.item_view_proposal"
                funcOnText={_.toUpper}
              />
            </EABButton>
          )}
        </ContextConsumer>
      ) : null;
    const buttonChoice =
      selectedAppListFilter === FILTER_TYPES.APPLICATION_LIST.PROPOSED ? (
        <View style={styles.checkBox}>
          <EABCheckBox
            testID="PreAppilcation__ProductList"
            options={[
              {
                key: "a",
                testID: `Choice-${id}`,
                text: (
                  <TranslatedText
                    style={styles.buttonTitle}
                    path="application.summary.item_choice"
                    funcOnText={_.toUpper}
                  />
                ),
                isSelected: isChoiceChecked
              }
            ]}
            onPress={() => {
              updateRecommendChoiceList({
                id,
                checked: !isChoiceChecked
              });
            }}
          />
        </View>
      ) : null;

    const buttonApply =
      !this.isInvalidated &&
      !this.isInvalidatedSigned &&
      !this.isSubmitted &&
      !this.isApplication &&
      [
        FILTER_TYPES.APPLICATION_LIST.ALL,
        FILTER_TYPES.APPLICATION_LIST.PROPOSED
      ].indexOf(selectedAppListFilter) >= 0 ? (
        <ContextConsumer>
          {({
            language,
            showApplicationDialog,
            setEappLoading,
            showMultiClientProfileDialog
          }) => (
            <EABButton
              isDisable={
                !isFaChannel && !(isChoiceChecked && recommendCompleted)
              }
              onPress={() => {
                const checkICids = isShield ? iCids : [iCid];
                setEappLoading(true);
                checkBeforeApply({
                  pCid,
                  iCids: checkICids,
                  callback: showDialog => {
                    if (showDialog === OTHER) {
                      setEappLoading(false, () => {
                        setTimeout(() => {
                          showMultiClientProfileDialog({
                            quotationId,
                            isShield
                          });
                        }, 300);
                      });
                    } else if (showDialog === ERROR) {
                      setEappLoading(false, () => {
                        setTimeout(() => {
                          Alert.alert(
                            null,
                            EABi18n({
                              language,
                              textStore,
                              path: "application.summary.error.invalidFna"
                            }),
                            [
                              {
                                text: EABi18n({
                                  language,
                                  textStore,
                                  path: "button.ok"
                                }),
                                onPress: () => {}
                              }
                            ]
                          );
                        }, 300);
                      });
                    } else {
                      applyApplicationForm({
                        quotationId,
                        isShield,
                        callback: () => {
                          setEappLoading(false, () => {
                            setTimeout(() => {
                              showApplicationDialog();
                            }, 300);
                          });
                        }
                      });
                    }
                  }
                });
              }}
            >
              <TranslatedText
                style={styles.buttonTitle}
                path="application.summary.item_apply"
                funcOnText={_.toUpper}
              />
            </EABButton>
          )}
        </ContextConsumer>
      ) : null;

    const buttonContinue =
      !this.isInvalidated &&
      !this.isInvalidatedSigned &&
      !this.isSubmitted &&
      this.isApplication &&
      [
        FILTER_TYPES.APPLICATION_LIST.ALL,
        FILTER_TYPES.APPLICATION_LIST.APPLYING
      ].indexOf(selectedAppListFilter) >= 0 ? (
        <ContextConsumer>
          {({ showApplicationDialog, setEappLoading, language }) => (
            <EABButton
              isDisable={!isFaChannel && !recommendCompleted}
              onPress={() => {
                if (this.props.isFnaInvalid) {
                  Alert.alert(
                    null,
                    EABi18n({
                      language,
                      textStore,
                      path: "application.summary.error.invalidFna"
                    }),
                    [
                      {
                        text: EABi18n({
                          language,
                          textStore,
                          path: "button.ok"
                        }),
                        onPress: () => {}
                      }
                    ]
                  );
                  return;
                }
                setEappLoading(true);
                if (quotType === "SHIELD") {
                  continueApplicationFormShield({
                    applicationId,
                    callback: ({ currentStep }) => {
                      setEappLoading(false, () => {
                        setTimeout(() => {
                          updateApplicationStep({
                            nextStep: currentStep,
                            isShield,
                            callback: () => {
                              showApplicationDialog();
                            }
                          });
                        }, 300);
                      });
                    }
                  });
                } else {
                  continueApplicationForm({
                    applicationId,
                    callback: ({ currentStep }) => {
                      setEappLoading(false, () => {
                        setTimeout(() => {
                          updateApplicationStep({
                            nextStep: currentStep,
                            isShield,
                            callback: () => {
                              showApplicationDialog();
                            }
                          });
                        }, 300);
                      });
                    }
                  });
                }
              }}
            >
              <TranslatedText
                style={styles.buttonTitle}
                path="application.summary.item_continue"
                funcOnText={_.toUpper}
              />
            </EABButton>
          )}
        </ContextConsumer>
      ) : null;

    const buttonPaid =
      this.isApplication &&
      [FILTER_TYPES.APPLICATION_LIST.INVALIDATED_SIGNED].indexOf(
        selectedAppListFilter
      ) >= 0 ? (
        <ContextConsumer>
          {({ language }) => (
            <View style={styles.checkBox}>
              <EABCheckBox
                options={[
                  {
                    key: "p",
                    text: (
                      <TranslatedText
                        style={styles.buttonTitle}
                        path="application.summary.item_paid"
                        funcOnText={_.toUpper}
                      />
                    ),
                    isSelected: isInitialPaymentCompleted
                  }
                ]}
                disable={isInitialPaymentCompleted}
                onPress={() => {
                  confirmationAlert({
                    messagePath: "application.summary.paid_confirm",
                    language,
                    textStore,
                    yesOnPress: () => {
                      comfirmApplicationPaid({ applicationId, isShield });
                    }
                  });
                }}
              />
            </View>
          )}
        </ContextConsumer>
      ) : null;

    const buttonSupportingDoc =
      this.isApplication &&
      (this.isInvalidatedSigned || this.isInvalidated || this.isSubmitted) &&
      [
        FILTER_TYPES.APPLICATION_LIST.ALL,
        FILTER_TYPES.APPLICATION_LIST.SUBMITTED,
        FILTER_TYPES.APPLICATION_LIST.INVALIDATED_SIGNED,
        FILTER_TYPES.APPLICATION_LIST.INVALIDATED
      ].indexOf(selectedAppListFilter) >= 0 ? (
        <ContextConsumer>
          {({ showAppSummarySupportingDocumentDialog }) => (
            <EABButton
              isDisable={false}
              onPress={() => {
                getSupportingDocument({
                  isShield,
                  appId: applicationId,
                  appStatus: EAPP.appStatus.SUBMITTED,
                  callback: () => {
                    showAppSummarySupportingDocumentDialog(isShield);
                  }
                });
              }}
            >
              {!isMandDocsAllUploaded && this.isSubmitted ? (
                <Image style={styles.titleBarIcon} source={icWarning} />
              ) : null}
              <TranslatedText
                style={styles.buttonTitle}
                path="application.summary.item_supportingDocuments"
                funcOnText={_.toUpper}
              />
            </EABButton>
          )}
        </ContextConsumer>
      ) : null;

    const invalidateReasonIcon = ({ language }) => {
      const invalidateReason = _.get(value, "invalidateReason");
      if (invalidateReason) {
        return (
          <EABButton
            key="invalidatedReasonIcon"
            onPress={() => {
              this.invalidateReasonAlert({ language });
            }}
          >
            <Image style={styles.invalidateReasonIcon} source={icInfoActive} />
          </EABButton>
        );
      }
      return null;
    };

    return (
      <ContextConsumer>
        {({ language }) => (
          <View style={styles.header}>
            <View style={styles.headerLeft}>
              {selectModeOn ? (
                <View>
                  <EABCheckBox
                    checkBoxStyle={styles.checkBox}
                    options={[{ key: `${id}`, text: "", isSelected }]}
                    onPress={this.checkboxOnClick}
                  />
                </View>
              ) : null}
              <TouchableWithoutFeedback
                testID={`PreAppilcation__ProductList__lblProductName__${rowIndex}`}
                onPress={() => {
                  if (selectModeOn) {
                    this.checkboxOnClick();
                  }
                }}
              >
                <View>
                  <Text style={theme.headerPrimary}>
                    {`${getEABTextByLangType({
                      data: baseProductName,
                      language
                    })} (${paymentMethod}) (${
                      policyNumber && !isShield ? policyNumber : id
                    })`}
                    {`${approvalStatus ? ` -- ${approvalStatus}` : ""}`}
                    {invalidateReasonIcon({ language })}
                  </Text>
                  <Text>
                    {pName}
                    {!_.isEmpty(iCid) && iCid !== pCid
                      ? `, ${iName.split(",")}`
                      : null}
                  </Text>
                </View>
              </TouchableWithoutFeedback>
            </View>
            <View style={styles.headerRight}>
              {buttonRequote}
              {buttonChoice}
              {buttonViewProposal}
              {buttonApply}
              {buttonContinue}
              {buttonPaid}
              {buttonSupportingDoc}
            </View>
            <EABHUD isOpen={isLoading} />
          </View>
        )}
      </ContextConsumer>
    );
  }
}

ApplicationItemHeader.propTypes = {
  profile: REDUCER_TYPE_CHECK[CLIENT].profile.isRequired,
  textStore: APP_REDUCER_TYPE_CHECK[TEXT_STORE].isRequired,
  value: REDUCER_TYPE_CHECK[PRE_APPLICATION].ApplicationItem.value.isRequired,
  updateApplicationStep: PropTypes.func.isRequired,
  selectedIdList: PropTypes.arrayOf(PropTypes.string).isRequired,
  selectedAppListFilter: PropTypes.string.isRequired,
  recommendCompleted: PropTypes.bool.isRequired,
  recommendChoiceList: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string,
      checked: PropTypes.bool
    })
  ).isRequired,
  isFaChannel: PropTypes.bool.isRequired,
  updateUiSelectedList: PropTypes.func.isRequired,
  updateRecommendChoiceList: PropTypes.func.isRequired,
  openProposal: PropTypes.func.isRequired,
  requoteInvalid: PropTypes.func.isRequired,
  routeToProposalUI: PropTypes.func.isRequired,
  applyApplicationForm: PropTypes.func.isRequired,
  getSupportingDocument: PropTypes.func.isRequired,
  continueApplicationForm: PropTypes.func.isRequired,
  continueApplicationFormShield: PropTypes.func.isRequired,
  comfirmApplicationPaid: PropTypes.func.isRequired,
  checkBeforeApply: PropTypes.func.isRequired,
  isFnaInvalid: PropTypes.bool.isRequired,
  rowIndex: PropTypes.number.isRequired
};

ApplicationItemHeader.defaultValue = {
  rowIndex: -1
};

export default ApplicationItemHeader;
