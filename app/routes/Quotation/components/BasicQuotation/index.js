import {
  REDUCER_TYPE_CHECK as WEB_API_TYPES_CHECK,
  REDUCER_TYPE_CHECK as WEB_API_REDUCER_TYPE_CHECK,
  REDUCER_TYPES as WEB_WEB_API_REDUCER_TYPES,
  TIME_FORMAT,
  utilities
} from "eab-web-api";
import * as _ from "lodash";
import moment from "moment";
import PropTypes from "prop-types";
import React, { Component } from "react";
import HTML from "react-native-render-html";
import {
  Alert,
  Image,
  ScrollView,
  Text,
  View,
  Modal,
  KeyboardAvoidingView
} from "react-native";
import icCircleArrowDown from "../../../../assets/images/icCircleArrowDown.png";
import icCircleArrowUp from "../../../../assets/images/icCircleArrowUp.png";
import icRemove from "../../../../assets/images/icRemove.png";
import EABBottomSheet from "../../../../components/EABBottomSheet";
import Button from "../../../../components/EABButton";
import { RECTANGLE_BUTTON_1 } from "../../../../components/EABButton/constants";
import EABCard from "../../../../components/EABCard";
import CheckBox from "../../../../components/EABCheckBox";
import Chip from "../../../../components/EABChip";
import Dialog from "../../../../components/EABDialog";
import EABSearchBar from "../../../../components/EABSearchBar";
import EABSearchField from "../../../../components/EABSearchField";
import Table from "../../../../components/EABTable";
import EABTableFooter from "../../../../components/EABTableFooter";
import TableHeader from "../../../../components/EABTableHeader";
import EABTableSection from "../../../../components/EABTableSection";
import TableSectionHeader from "../../../../components/EABTableSectionHeader";
import EABTextField from "../../../../components/EABTextField";
import Selector from "../../../../components/Selector";
import EABHUD from "../../../../containers/EABHUD";
import { DATE_PICKER } from "../../../../components/Selector/constants";
import { FLAT_LIST } from "../../../../components/SelectorField/constants";
import REDUCER_TYPES_CHECK from "../../../../constants/REDUCER_TYPE_CHECK";
import * as REDUCER_TYPES from "../../../../constants/REDUCER_TYPES";
import TranslatedText from "../../../../containers/TranslatedText";
import { ContextConsumer } from "../../../../context";
import Theme from "../../../../theme";
import EABi18n, { isTextPathUndefined } from "../../../../utilities/EABi18n";
import getEABTextByLangType, {
  languageTypeConvert
} from "../../../../utilities/getEABTextByLangType";
import {
  BOTTOM_SHEET_COLLAPSE_HEIGHT,
  BOTTOM_SHEET_EXPAND_HEIGHT
} from "../../constants/QUOTATION_CONSTANTS";
import RiderDialog from "../../containers/RiderDialog";
import clientInfoObject from "../../utilities/clientInfoObject";
import policyOptionOnChange from "../../utilities/policyOptionOnChange";
import {
  removeDisabledRiders,
  removeInclusiveRiders
} from "../../utilities/riderHandler";
import ProfileUI from "../ProfileUI";
import { FUND_MAX_SELECTION } from "./constants";
import styles from "./styles";
import TermAndCondition from "./TermAndCondition";

const { DATE_FORMAT_INPUT } = TIME_FORMAT;
const { TEXT_STORE } = REDUCER_TYPES;
const { QUOTATION, OPTIONS_MAP, CLIENT } = WEB_WEB_API_REDUCER_TYPES;
const { fundChecker, common, numberFormatter } = utilities;
const {
  getCurrency,
  getCurrencySign,
  getOptionTitle,
  fillTextVariable,
  getAgeByUnit
} = common;
const {
  numberToCurrency,
  numberToCurrencyForQuotation,
  currencyToNumber
} = numberFormatter;

/**
 * <BasicQuotation />
 * @description basic quotation component. all handling is in the component.
 * @param {function} quote - call backend to update quotation
 * @param {function} resetQuot - reset quotation
 * @param {function} updateRiders - call backend to update riders
 * @param {function} getFunds - get availableFunds
 * */
export default class BasicQuotation extends Component {
  static adjustModelRange({ portfolio }) {
    const totalGroups = portfolio.groups.length;
    let curMax = 0;
    let maximumGroupUpperLimit = 0;

    const newGroups = _.each(
      portfolio.groups,
      (group, index, originalGroups) => {
        let min = 0;
        let max = 0;
        const currentRiskRating = _.get(group, "riskRatings[0]");
        maximumGroupUpperLimit = _.get(
          _.reduce(originalGroups, (groupA, groupB) => {
            const accumlatedPercentageByRiskRating = validGroup =>
              _.get(validGroup, "riskRatings[0]") >= currentRiskRating ||
              validGroup.calculatedByReduce
                ? validGroup.percentage || 0
                : 0;
            const percentageA = accumlatedPercentageByRiskRating(groupA);
            const percentageB = accumlatedPercentageByRiskRating(groupB);
            return {
              calculatedByReduce: true,
              percentage: percentageA + percentageB
            };
          }),
          "percentage"
        );

        // Copy preset precentage
        if (!_.isNumber(group.percentageNew)) {
          group.percentageNew = group.percentage;
        }

        if (index === 0) {
          max = group.percentage;
        } else {
          max = maximumGroupUpperLimit - curMax;
        }

        if (index === totalGroups - 1) {
          min = group.percentage;
        }

        group.min = min;
        group.max = max;

        if (group.percentageNew > group.max) {
          group.percentageNew = group.max;
        }

        // Maximum possible upper limit after User input
        curMax += group.percentageNew || 0;
      }
    );

    const newportfolioAllocs = _.cloneDeep(portfolio);
    newportfolioAllocs.groups = newGroups;

    return newportfolioAllocs;
  }
  constructor(props) {
    super(props);

    this.getTableConfig = this.getTableConfig.bind(this);
    this.getSelectFundsTable = this.getSelectFundsTable.bind(this);
    this.getSelectPortfolioTable = this.getSelectPortfolioTable.bind(this);
    this.onChangePayMode = this.onChangePayMode.bind(this);
    this.openFundsDialogCallBack = this.openFundsDialogCallBack.bind(this);
    this.openPortfolioDialogCallBack = this.openPortfolioDialogCallBack.bind(
      this
    );
    this.clientInfoArray = this.clientInfoArray.bind(this);
    this.changeInvOpt = this.changeInvOpt.bind(this);
    this.selectInvOpt = this.selectInvOpt.bind(this);
    this.portfolioOptions = this.portfolioOptions.bind(this);
    this.riskDescription = this.riskDescription.bind(this);
    this.state = {
      shouldShowSelectRiderDialog: false,
      shouldShowSelectFundsDialog: false,
      shouldShowSelectPortfolioDialog: false,
      shouldExpandBottomSheet: false,
      selectedFunds: {},
      fundAllocsBackUp: {},
      topUpAllocsBackUp: {},
      portfolioAllocsBackUp: this.props.quotation.fund
        ? this.props.quotation.fund.adjustedModel
        : {},
      portfolioAllocsTemp: this.props.quotation.fund
        ? this.props.quotation.fund.adjustedModel
        : {},
      focusedField: "",
      focusedPlan: "",
      searchRisk: "*",
      invOpt: this.props.quotation.fund
        ? this.props.quotation.fund.invOpt
        : "mixedAssets",
      portfolio: this.props.quotation.fund
        ? this.props.quotation.fund.portfolio
        : null,
      fundsSearchText: "",
      openDisclaimer: false,
      isLoading: false,
      paymentMethod: _.get(this.props.quotation.policyOptions, "paymentMethod")
        ? this.props.quotation.policyOptions.paymentMethod
        : "",
      isFirstLoad: _.isEmpty(this.props.quotation.policyOptions),
      openWarning: false
    };
  }

  componentDidMount() {
    const { quotation, textStore } = this.props;
    if (quotation.baseProductCode === "PVTVUL") {
      setTimeout(() => {
        Alert.alert(
          EABi18n({
            textStore,
            language: "ENGLISH",
            path: "alert.warning"
          }),
          EABi18n({
            language: "ENGLISH",
            textStore,
            path: "quotation.alert.openPVTVUL"
          }),
          [
            {
              text: EABi18n({
                language: "ENGLISH",
                textStore,
                path: "button.ok"
              }),
              onPress: () => {}
            }
          ]
        );
      }, 500);
    }
  }

  // ===========================================================================
  // class methods
  // ===========================================================================
  onChangePayMode({ value, language }) {
    const { resetQuot, quote, quotation, inputConfigs, textStore } = this.props;
    const inputConfig = inputConfigs[quotation.baseProductCode] || undefined;
    const payMode = _.find(
      inputConfig.payModes,
      pm => pm.mode === quotation.paymentMode
    );
    const newQuotation = {
      ...quotation,
      paymentMode: value
    };

    let payModeCheck = false;
    if (payMode && payMode.onChange) {
      if (
        !payMode.onChange.to ||
        payMode.onChange.to === value ||
        payMode.onChange.to.indexOf(value) > -1
      ) {
        payModeCheck = payMode.onChange.action === "resetQuot";
      }
    }

    if (payModeCheck) {
      const params = payMode.onChange.params || {};
      const { keepPolicyOptions, keepPlans, keepFunds } = params;

      return Alert.alert(
        EABi18n({
          textStore,
          language,
          path: "alert.confirm"
        }),
        EABi18n({ language, textStore, path: "quotation.resetQuotation" }),
        [
          {
            text: EABi18n({ language, textStore, path: "button.no" }),
            onPress: () => {},
            style: "cancel"
          },
          {
            text: EABi18n({ language, textStore, path: "button.yes" }),
            onPress: () => {
              resetQuot({
                quotation: newQuotation,
                keepConfigs: true,
                keepPolicyOptions,
                keepPlans,
                callback: keepFunds
              });
            }
          }
        ]
      );
    }

    return quote(newQuotation);
  }

  // ===========================================================================
  // getters
  // ===========================================================================
  /**
   * isQuotationHaveFunds
   * @description indicates whether the quotation have funds or not
   * @return {boolean} quotation funds status
   * */
  get isQuotationHaveFunds() {
    const { planDetails, quotation } = this.props;

    return fundChecker({ planDetails, quotation });
  }
  /**
   * getCurrentPortfolioModel
   * @description indicates whether the quotation have funds or not
   * @return {boolean} quotation funds status
   * */
  get getCurrentPortfolioModel() {
    const { optionsMap } = this.props;
    const { portfolio } = this.state;
    let model = {};
    _.each(optionsMap.portfolioModels.portfolios, p => {
      _.each(p.models, m => {
        if (m.id === portfolio) {
          model = m;
        }
      });
    });
    return model;
  }
  /**
   * getRiderButton
   * @return {element} select rider button element
   * */
  get getRiderButton() {
    const { quotation, planDetails, inputConfigs, textStore } = this.props;
    const { riderList } = inputConfigs[quotation.baseProductCode];
    if (_.size(planDetails) > 1) {
      return (
        <ContextConsumer>
          {({ language }) => (
            <Button
              buttonType={RECTANGLE_BUTTON_1}
              isDisable={!riderList || !riderList.length}
              onPress={() => {
                this.setState({
                  shouldShowSelectRiderDialog: true
                });
              }}
            >
              <Text style={Theme.rectangleButtonStyle1Label}>
                {EABi18n({
                  language,
                  textStore,
                  path: "quotation.button.selectRiders"
                })}
              </Text>
            </Button>
          )}
        </ContextConsumer>
      );
    }
    return null;
  }
  /**
   * @description get mandatory policy options
   * @return {array} array of mandatory policy options object.
   * */
  get getMandatoryPolicyOptions() {
    const { inputConfigs } = this.props;
    const policyOptionsGroups = {};
    let mandatoryPolicyOptions = [];

    Object.keys(inputConfigs).forEach(key => {
      inputConfigs[key].policyOptions.forEach(policyOption => {
        // initial policyOptionsGroup
        if (!Array.isArray(policyOptionsGroups[policyOption.groupID])) {
          policyOptionsGroups[policyOption.groupID] = [];
        }

        if (policyOption.type !== "emptyBlock") {
          policyOptionsGroups[policyOption.groupID].push(policyOption);
        }
      });
    });

    Object.keys(policyOptionsGroups).forEach(groupKey => {
      /* get group first field */
      const firstField = policyOptionsGroups[groupKey][0];

      if (firstField.type !== "checkbox") {
        mandatoryPolicyOptions = [
          ...mandatoryPolicyOptions,
          ...policyOptionsGroups[groupKey]
        ];
      }
    });

    return mandatoryPolicyOptions;
  }

  get sign() {
    const { quotation, optionsMap } = this.props;

    return getCurrencySign({
      compCode: quotation.compCode,
      ccy: quotation.ccy,
      optionsMap
    });
  }

  get hasTopUpAlloc() {
    const { quotation, inputConfigs } = this.props;
    return !!inputConfigs[quotation.baseProductCode].topUpSelect;
  }

  get portfolioAllocs() {
    const { getCurrentPortfolioModel } = this;
    const { optionsMap, availableFunds } = this.props;

    if (_.isEmpty(getCurrentPortfolioModel)) {
      return {};
    }
    function addSkippedFundGroup({ riskRating }) {
      return {
        min: 0,
        max: 0,
        percentage: 0,
        percentageNew: 0,
        riskRatings: [riskRating]
      };
    }

    function handleSpecialFundGroup({ fundsGroup, riskRating }) {
      const index = _.findIndex(
        fundsGroup,
        d => d.type === getCurrentPortfolioModel.title
      );
      if (index > -1) {
        return _.some(
          availableFunds,
          f =>
            f.riskRating === riskRating &&
            fundsGroup[index].funds.includes(f.fundCode)
        );
      }
      return false;
    }
    const maxRiskRating = _.get(
      getCurrentPortfolioModel.groups,
      "[0].riskRatings[0]"
    );
    const optionsMapRiskRatings = [];

    _.each(_.get(optionsMap, "riskRating.options"), riskRating => {
      optionsMapRiskRatings.push(riskRating.value);
    });

    let riskLevelIndex = _.findIndex(
      _.sortBy(optionsMapRiskRatings, riskRating => riskRating),
      value => value === maxRiskRating
    );

    const availableFundGroup = [];
    const specialFundGroup = [
      { type: "Architas", funds: optionsMap.portfolioModels.architasFunds }
    ];
    let matchingRiskRating;

    _.each(getCurrentPortfolioModel.groups, group => {
      const groupRiskRating = _.get(group, "riskRatings[0]");
      matchingRiskRating = optionsMapRiskRatings[riskLevelIndex];
      if (groupRiskRating !== matchingRiskRating) {
        const riskRatingDiff = matchingRiskRating - groupRiskRating;
        for (let i = riskRatingDiff; i > 0; i -= 1) {
          const missedRiskRating = optionsMapRiskRatings[riskLevelIndex];
          if (
            handleSpecialFundGroup({
              fundsGroup: specialFundGroup,
              riskRating: missedRiskRating
            })
          ) {
            availableFundGroup.push(
              addSkippedFundGroup({ riskRating: missedRiskRating })
            );
          } else if (
            !_.some(
              specialFundGroup,
              s => s.type === getCurrentPortfolioModel.title
            )
          ) {
            availableFundGroup.push(
              addSkippedFundGroup({ riskRating: missedRiskRating })
            );
          }
          riskLevelIndex -= 1;
        }
      }
      availableFundGroup.push(group);
      riskLevelIndex -= 1;
    });
    for (let i = riskLevelIndex; i >= 0; i -= 1) {
      matchingRiskRating = optionsMapRiskRatings[i];
      if (
        handleSpecialFundGroup({
          fundsGroup: specialFundGroup,
          riskRating: matchingRiskRating
        })
      ) {
        availableFundGroup.push(
          addSkippedFundGroup({ riskRating: matchingRiskRating })
        );
      } else if (
        !_.some(
          specialFundGroup,
          s => s.type === getCurrentPortfolioModel.title
        )
      ) {
        availableFundGroup.push(
          addSkippedFundGroup({ riskRating: matchingRiskRating })
        );
      }
    }

    const portfolioAllocs = _.cloneDeep(getCurrentPortfolioModel);
    portfolioAllocs.groups = availableFundGroup;
    return portfolioAllocs;
  }

  get fundAllocs() {
    const { quotation } = this.props;
    const { funds } = quotation.fund || {};
    const fundAllocs = {};

    _.each(funds, fund => {
      fundAllocs[fund.fundCode] = fund.alloc;
    });

    return fundAllocs;
  }

  get topUpAllocs() {
    const { hasTopUpAlloc } = this;
    const { quotation } = this.props;
    const { funds } = quotation.fund || {};
    if (hasTopUpAlloc) {
      const topUpAllocs = {};
      _.each(funds, fund => {
        topUpAllocs[fund.fundCode] = fund.topUpAlloc;
      });

      return topUpAllocs;
    }
    return {};
  }

  get filterRiskRatings() {
    const { getCurrentPortfolioModel, isPortfolioAdjusted } = this;
    const { invOpt, portfolioAllocsBackUp } = this.state;

    let filterRiskRatings = null;
    if (invOpt === "buildPortfolio" && getCurrentPortfolioModel) {
      if (getCurrentPortfolioModel.any) {
        filterRiskRatings = [1, 2, 3, 4, 5];
      } else {
        filterRiskRatings = [];
        if (isPortfolioAdjusted && portfolioAllocsBackUp) {
          _.each(portfolioAllocsBackUp.groups, group => {
            if (Number(group.percentageNew) > 0) {
              filterRiskRatings = filterRiskRatings.concat(group.riskRatings);
            }
          });
        } else {
          _.each(getCurrentPortfolioModel.groups, group => {
            filterRiskRatings = filterRiskRatings.concat(group.riskRatings);
          });
        }
      }
    }
    return filterRiskRatings;
  }

  get getPortfolioAllocTotal() {
    const { isPortfolioSelected } = this;
    const { portfolioAllocsTemp } = this.state;

    if (!isPortfolioSelected) {
      return 0;
    }
    let totalPercentage = 0;
    _.each(portfolioAllocsTemp.groups, group => {
      totalPercentage += group.percentageNew;
    });
    return totalPercentage;
  }

  get getFundAllocBackUpTotal() {
    const { fundAllocsBackUp } = this.state;

    return _.reduce(fundAllocsBackUp, (sum, alloc) => sum + (alloc || 0), 0);
  }

  get getTopUpAllocBackUpTotal() {
    const { topUpAllocsBackUp } = this.state;

    return _.reduce(topUpAllocsBackUp, (sum, alloc) => sum + (alloc || 0), 0);
  }

  get riskProfile() {
    const { quotation } = this.props;

    return quotation.extraFlags.fna && quotation.extraFlags.fna.riskProfile;
  }

  get getFundTableRows() {
    const { hasTopUpAlloc, isInvOptBuildPortfolio } = this;
    const { quotation, availableFunds, quote, textStore } = this.props;

    return quotation.fund !== null
      ? quotation.fund.funds.map((fund, index) => {
          const availableFundObject = _.find(
            availableFunds,
            availableFund => availableFund.fundCode === fund.fundCode
          );

          if (availableFundObject) {
            const returnObject = {
              key: availableFundObject.fundCode,
              fundName: availableFundObject.fundName,
              ccy: availableFundObject.ccy,
              assetClass: availableFundObject.assetClass,
              riskRating: availableFundObject.riskRating,
              alloc: fund.alloc,
              removeFunction: language => {
                let fundBackUp = _.cloneDeep(quotation.fund);

                // if only select one fund, reset quotation fund key
                if (fundBackUp.funds.length !== 1 && !isInvOptBuildPortfolio) {
                  fundBackUp.funds.splice(index, 1);
                } else {
                  fundBackUp = null;
                }
                Alert.alert(
                  EABi18n({
                    textStore,
                    language,
                    path: "alert.confirm"
                  }),
                  EABi18n({
                    textStore,
                    language,
                    path: isInvOptBuildPortfolio
                      ? "quotation.warning.confirmCleanFund"
                      : "quotation.warning.removeFund"
                  }),
                  [
                    {
                      text: EABi18n({
                        language,
                        textStore,
                        path: "button.no"
                      }),
                      onPress: () => {}
                    },
                    {
                      text: EABi18n({
                        language,
                        textStore,
                        path: "button.yes"
                      }),
                      onPress: () => {
                        quote({
                          ...quotation,
                          fund: fundBackUp,
                          searchRisk: "*"
                        });
                      }
                    }
                  ]
                );
              }
            };

            if (hasTopUpAlloc) {
              returnObject.topUpAlloc = fund.topUpAlloc;
            }

            return returnObject;
          }
          throw new Error(
            `availableFunds do not contain fund of fund code "${fund.fundCode}"`
          );
        })
      : null;
  }
  /**
   * @description true if portfolio is selected
   * @return {boolean}
   * */
  get isPortfolioSelected() {
    const { portfolio } = this.state;
    return !_.isEmpty(portfolio);
  }

  get isPortfolioAdjusted() {
    const { portfolioAllocs } = this;
    const { portfolioAllocsBackUp } = this.state;

    return (
      !_.isEqual(portfolioAllocs, portfolioAllocsBackUp) &&
      !_.isEmpty(portfolioAllocsBackUp)
    );
  }

  /**
   * @description indicate invOpt is BuildPortfolio
   * @return {boolean}
   * */
  get isInvOptBuildPortfolio() {
    const { invOpt } = this.state;

    return invOpt === "buildPortfolio";
  }
  /**
   * @description get selected funds list. HAVE TO BE AWARE, this list is for
   *   the funds that selected in the select fund dialog, it does't mean this
   *   list has been save to the quotation.
   * @requires hasTopUpAlloc
   * @requires fundAllocs
   * @requires topUpAllocs
   * @return {array} selected funds list
   * */
  get getSelectedFunds() {
    const { hasTopUpAlloc, getCurrentPortfolioModel } = this;
    const { fundAllocsBackUp, topUpAllocsBackUp } = this.state;
    const { availableFunds, optionsMap } = this.props;
    const { architasFunds } = optionsMap.portfolioModels;

    let filterResult = [];
    if (
      getCurrentPortfolioModel &&
      getCurrentPortfolioModel.title === "Architas"
    ) {
      _.each(availableFunds, f => {
        if (architasFunds.includes(f.fundCode)) filterResult.push(f);
      });
    } else {
      filterResult = _.filter(
        availableFunds,
        f =>
          fundAllocsBackUp[f.fundCode] !== undefined ||
          (hasTopUpAlloc && topUpAllocsBackUp[f.fundCode] !== undefined)
      );
    }
    return filterResult;
  }

  // ===========================================================================
  // class methods
  // ===========================================================================
  /**
   * @name getTableConfig
   * @return {object} table config
   * */
  getTableConfig(language) {
    const { sign } = this;
    const {
      quote,
      quotation,
      planDetails,
      inputConfigs,
      textStore,
      updateQuotation,
      quotationErrors,
      updateRiders
    } = this.props;
    const { focusedField, focusedPlan } = this.state;
    const targetInputConfig = inputConfigs[quotation.baseProductCode];
    const { riderList } = targetInputConfig;
    const targetPlanDetail = planDetails[quotation.baseProductCode];
    const bpDetail = planDetails[quotation.baseProductCode];
    const requireGST = bpDetail && bpDetail.gstInd && bpDetail.gstInd === "Y";
    const planErrors =
      quotationErrors && quotationErrors.planErrors
        ? quotationErrors.planErrors
        : {};
    const hasPolTerm = !targetInputConfig.isPolTermHide;
    const hasPaymentTerm = !targetInputConfig.isPremTermHide;
    const hasClass = !!targetInputConfig.hasClass;
    const hasSumAssured = targetInputConfig.canViewSumAssured;
    const hasOthSa = targetPlanDetail.othSaInd === "Y";
    const header = [
      <View key="Plan name">
        <Text style={Theme.tableColumnLabel}>
          {EABi18n({ language, textStore, path: "quotation.table.name" })}
        </Text>
      </View>
    ];
    const isLMP = quotation.baseProductCode === "LMP";
    const isFlexi =
      quotation.baseProductCode === "FPX" ||
      quotation.baseProductCode === "FSX";
    const isFAMA =
      quotation.baseProductCode === "PNP" ||
      quotation.baseProductCode === "PNP2" ||
      quotation.baseProductCode === "PNPP" ||
      quotation.baseProductCode === "PNPP2";
    const isBAA = quotation.baseProductCode === "BAA";

    const basicPlanData = {
      premPostfix: !_.isEmpty(bpDetail.premPostfix)
        ? getEABTextByLangType({
            data: bpDetail.premPostfix,
            language
          })
        : ""
    };

    const getStyle = ({ columnKey }) => {
      switch (String(columnKey)) {
        case "Plan name":
          switch (String(quotation.baseProductCode)) {
            case "BAA":
              return styles.baaCovNameHeader;
            case "PUL":
            case "AWT":
            case "PNP":
            case "PNP2":
            case "PNPP":
            case "PNPP2":
              return styles.compactNameHeader;
            default:
              return styles.covNameHeader;
          }
        case "policy term":
          switch (String(quotation.baseProductCode)) {
            case "LMP":
              return styles.lmpPolTermHeader;
            case "ESP":
              return styles.espPolTermHeader;
            default:
              return styles.polTermHeader;
          }
        case "payment term":
          switch (String(quotation.baseProductCode)) {
            case "LMP":
            case "AWT":
            case "PNP":
            case "PNP2":
            case "PNPP":
            case "PNPP2":
            case "PUL":
              return styles.specialPaymentTermHeader;
            default:
              return styles.paymentTermHeader;
          }
        case "premium":
          switch (String(quotation.baseProductCode)) {
            case "FPX":
            case "FSX":
            case "AWT":
            case "PNP":
            case "PNP2":
            case "PNPP":
            case "PNPP2":
            case "PUL":
              return styles.compactPremiumHeader;
            default:
              return styles.premiumHeader;
          }
        default:
          return null;
      }
    };

    const tableConfig = [
      {
        key: "Plan name",
        style: getStyle({ columnKey: "Plan name" })
      }
    ];
    /* get header */
    if (hasPolTerm) {
      tableConfig.push({
        key: "policy term",
        style: getStyle({ columnKey: "policy term" })
      });
      header.push(
        <View key="policy term">
          <Text style={Theme.tableColumnLabel}>
            {EABi18n({
              language,
              textStore,
              path: "quotation.table.policyTerm"
            })}
            {bpDetail.polTermRemarkSymbol || ""}
          </Text>
        </View>
      );
    }

    if (hasPaymentTerm) {
      tableConfig.push({
        key: "payment term",
        style: getStyle({ columnKey: "payment term" })
      });
      header.push(
        <View key="payment term">
          <Text style={Theme.tableColumnLabel}>
            {EABi18n({
              language,
              textStore,
              path: "quotation.table.paymentTerm"
            })}
            {bpDetail.premTermRemarkSymbol || ""}
          </Text>
        </View>
      );
    }

    if (hasClass) {
      tableConfig.push({
        key: "class",
        style: isFlexi || isBAA ? styles.specialClassHeader : styles.classHeader
      });
      const classNamePath = () =>
        `${quotation.baseProductCode}.classTitle.${languageTypeConvert(
          language
        )}`;
      const className = () => _.get(inputConfigs, classNamePath(language));
      const occupationClass =
        className(language) ||
        EABi18n({
          language,
          textStore,
          path: "quotation.table.occupationClass"
        });

      header.push(
        <View key="occupation class">
          <Text style={Theme.tableColumnLabel}>{occupationClass}</Text>
        </View>
      );
    }

    if (hasSumAssured) {
      tableConfig.push({
        key: "sum assured",
        style: isFlexi
          ? styles.specialSumAssuredHeader
          : styles.sumAssuredHeader
      });
      header.push(
        <View key="sum assured">
          <Text style={styles.rightAlign}>
            <Text style={Theme.tableColumnLabel}>
              <Text>
                {EABi18n({
                  language,
                  textStore,
                  path: "quotation.table.header.sumAssured"
                })}
              </Text>
              <Text>
                {EABi18n({
                  language,
                  textStore,
                  path: "quotation.table.header.sumAssured2"
                })}
              </Text>
            </Text>
          </Text>
        </View>
      );
    }

    if (hasOthSa) {
      tableConfig.push({
        key: "other",
        style: isLMP ? styles.specialOthSaHeader : styles.othSaHeader
      });
      const otherName = () => bpDetail.othSaDesc[languageTypeConvert(language)];

      header.push(
        <View key="other">
          <Text style={Theme.tableColumnLabel}>{otherName(language)}</Text>
        </View>
      );
    }

    tableConfig.push({
      key: "premium",
      style: getStyle({ columnKey: "premium" })
    });
    header.push(
      <View key="premium">
        <Text style={Theme.tableColumnLabel}>
          {EABi18n({
            language,
            textStore,
            path: "quotation.table.premium"
          })}
        </Text>
      </View>
    );

    tableConfig.push({
      key: "canncelButton",
      style: styles.cancelButton
    });
    header.push(<View key="remove button" />);

    const basicPlan = [];
    const riders = [];
    const basicPlanBreakDown = [];
    const ridersBreakDown = [];

    /* get plan row and rider row */
    _.each(quotation.plans, (plan, index) => {
      const isBasicPlan = plan.covCode === quotation.baseProductCode;
      const els = isBasicPlan ? basicPlan : riders;
      const breakDownEls = isBasicPlan ? basicPlanBreakDown : ridersBreakDown;
      const planDetail = planDetails[plan.covCode] || {};
      const planInputConfig = inputConfigs[plan.covCode] || {};
      const planError = planErrors[plan.covCode] || [];

      if (_.get(plan, "hiddenRider") && plan.hiddenRider === "Y") {
        return;
      }
      /* get plan row */
      const tableColumn = [];

      if (isFAMA) {
        tableColumn.push(
          <View key={`${plan.covCode}A`}>
            <Text style={Theme.subheadPrimary}>
              {getEABTextByLangType({
                data: quotation.baseProductName,
                language
              })}
            </Text>
          </View>
        );
      } else {
        tableColumn.push(
          <View key={`${plan.covCode}A`}>
            <Text style={Theme.subheadPrimary}>
              {getEABTextByLangType({ data: plan.covName, language })}
            </Text>
          </View>
        );
      }

      if (hasPolTerm) {
        /* get options */
        const options = [];
        if (planInputConfig.policyTermList) {
          planInputConfig.policyTermList.forEach(policyTerm => {
            options.push({
              key: `${policyTerm.value}`,
              label: getEABTextByLangType({
                data: policyTerm.title,
                language
              }),
              value: policyTerm.value
            });
          });
        }

        /* get label */
        let value = "";

        const optionValue = plan.policyTerm;
        if (options.length) {
          if (
            optionValue &&
            options.findIndex(option => option.value === optionValue) !== -1
          ) {
            value = optionValue;
          } else {
            plan.policyTerm = undefined;
            planInputConfig.canEditSumAssured = false;
            value = EABi18n({
              textStore,
              language,
              path: "button.select"
            });
          }
        } else {
          value = "-";
        }

        tableColumn.push(
          <View key={`${plan.covCode}B`}>
            <Selector
              testID={`Quotation__Non-Shield__ddlPolicyTerm__${plan.covCode}`}
              selectorType={FLAT_LIST}
              hasCard={false}
              value={value}
              popoverOptions={{
                preferredContentSize: [400, 150]
              }}
              flatListOptions={{
                data: options,
                renderItemOnPress: (label, option) => {
                  const plans = _.cloneDeep(quotation.plans);
                  plans[index].policyTerm = option.value;

                  quote({
                    ...quotation,
                    plans
                  });
                },
                selectedValue: value
              }}
              disable={
                !options.length ||
                !planInputConfig ||
                !planInputConfig.canEditPolicyTerm
              }
            />
          </View>
        );
      }

      if (hasPaymentTerm) {
        /* get options */
        const options = [];
        if (planInputConfig.premTermList) {
          planInputConfig.premTermList.forEach(policyTerm => {
            options.push({
              key: `${policyTerm.value}`,
              label: getEABTextByLangType({
                data: policyTerm.title,
                language
              }),
              value: policyTerm.value
            });
          });
        }

        /* get label */
        let value = "";
        const optionValue = plan.premTerm;
        if (options.length) {
          if (
            optionValue &&
            options.findIndex(option => option.value === optionValue) !== -1
          ) {
            value = optionValue;
          } else {
            value = EABi18n({
              textStore,
              language,
              path: "button.select"
            });
          }
        } else {
          value = "-";
        }
        tableColumn.push(
          <View key={`${plan.covCode}C`}>
            <Selector
              testID={`Quotation__Non-Shield__ddlPremiumPaymentTerm__${
                plan.covCode
              }`}
              selectorType={FLAT_LIST}
              hasCard={false}
              value={value}
              flatListOptions={{
                data: options,
                renderItemOnPress: (label, option) => {
                  const plans = _.cloneDeep(quotation.plans);
                  plans[index].premTerm = option.value;

                  quote({
                    ...quotation,
                    plans
                  });
                },
                selectedValue: value
              }}
              disable={!planInputConfig || !planInputConfig.canEditPremTerm}
            />
          </View>
        );
      }

      if (hasClass) {
        /* get options */
        const options = [];
        if (planInputConfig.classTypeList) {
          planInputConfig.classTypeList.forEach(policyTerm => {
            options.push({
              key: `${policyTerm.value}`,
              label: getEABTextByLangType({
                data: policyTerm.title,
                language
              }),
              value: policyTerm.value
            });
          });
        }

        /* get label */
        let value = plan.covClass || "-";
        if (options.length) {
          value =
            plan.covClass ||
            EABi18n({ textStore, language, path: "button.select" });
        }

        tableColumn.push(
          <View key={`${plan.covCode}D`}>
            <Selector
              testID="Quotation__Non-Shield__btnPremiumPaymentTerm"
              selectorType={FLAT_LIST}
              hasCard={false}
              value={value}
              popoverOptions={{
                preferredContentSize: [400, 150]
              }}
              flatListOptions={{
                data: options,
                renderItemOnPress: (label, option) => {
                  const plans = _.cloneDeep(quotation.plans);
                  plans[index].covClass = option.value;

                  quote({
                    ...quotation,
                    plans
                  });
                },
                selectedValue: value
              }}
              disable={!planInputConfig || !planInputConfig.canEditClassType}
            />
          </View>
        );
      }

      if (hasSumAssured) {
        const sumAssuredNumberToCurrency = numberToCurrency({
          value: plan.sumInsured,
          decimal: 0,
          isFocus: focusedField === "sumInsured",
          maxValue: 99999999999
          // use 11 digits maxValue to format 10 digits sumInsured so that sumAssuredNumberToCurrency can have 11 digits
        });

        const sumAssuredMaxLength = sign.length + 10 + Math.floor(10 / 3);

        const checkIfEditable =
          planInputConfig.canEditSumAssured ||
          quotation.plans[index].covCode === quotation.baseProductCode
            ? sumAssuredNumberToCurrency
            : "";

        const checkIfNeedPlaceholder =
          quotation.plans[index].covCode === quotation.baseProductCode ||
          planInputConfig.canEditSumAssured
            ? EABi18n({
                textStore,
                language,
                path: "quotation.placeholder.enterSumAssured"
              })
            : "";

        const checkSumInsuredValue = !quotation.plans[index].sumInsured
          ? checkIfEditable
          : sumAssuredNumberToCurrency;

        tableColumn.push(
          <View style={styles.sumAssuredTableColumn} key={`${plan.covCode}E`}>
            <EABTextField
              testID={`Quotation__Non-Shield__txtSumAssured__${plan.covCode}`}
              inputStyle={{ textAlign: "right" }}
              supportLabelPlaceholder={false}
              placeholder={checkIfNeedPlaceholder}
              supportErrorMessage={false}
              value={
                !!planInputConfig && !planInputConfig.canViewSumAssured
                  ? "-"
                  : checkSumInsuredValue
              }
              onFocus={() => {
                this.setState({
                  focusedField: "sumInsured",
                  focusedPlan: planDetail.covCode
                });
              }}
              onChange={value => {
                const newPlans = _.cloneDeep(quotation.plans);
                if (
                  value.indexOf(sign) === -1 &&
                  (newPlans[index].sumInsured !== null ||
                    newPlans[index].sumInsured.indexOf(sign) !== -1)
                ) {
                  newPlans[index].sumInsured = null;
                } else {
                  value = value.replace(/\./g, "");
                  const removedPrefixValue = value.replace(sign, "");

                  newPlans[index].sumInsured = currencyToNumber(
                    removedPrefixValue
                  );
                }

                newPlans[index].calcBy = "sumAssured";

                updateQuotation({
                  ...quotation,
                  plans: newPlans
                });
              }}
              onBlur={() => {
                this.setState(
                  {
                    focusedField: null,
                    focusedPlan: null
                  },
                  () => {
                    quote(quotation);
                  }
                );
              }}
              returnWithPrefix
              prefix={
                !!planInputConfig && !planInputConfig.canViewSumAssured
                  ? ""
                  : sign
              }
              maxLength={sumAssuredMaxLength}
              editable={!!planInputConfig.canEditSumAssured}
              keyboardType="numeric"
            />
            {!_.isEmpty(plan.saPostfix) ? (
              <Text style={Theme.fieldTextOrHelperTextNormal}>
                {getEABTextByLangType({
                  data: planDetail.saPostfix,
                  language
                })}
              </Text>
            ) : null}
          </View>
        );
      }

      if (hasOthSa) {
        const othSaNumberToCurrency =
          numberToCurrency({
            value: plan[bpDetail.othSaKey],
            decimal: bpDetail.othSaDecimal,
            isFocus: focusedField === "othSa"
          }) || "";

        const checkOthSaValue = !quotation.plans[index][bpDetail.othSaKey]
          ? ""
          : othSaNumberToCurrency;
        const hasOthSaRule = !planInputConfig.canViewOthSa
          ? "-"
          : checkOthSaValue;
        const checkIfNeedPlaceholder =
          quotation.plans[index].covCode === quotation.baseProductCode ||
          planInputConfig.canEditOthSa
            ? fillTextVariable({
                text: EABi18n({
                  path: "quotation.placeholder.enterOthSa",
                  language,
                  textStore
                }),
                data: [
                  {
                    isPath: false,
                    value: bpDetail.othSaDesc[
                      languageTypeConvert(language)
                    ].toLowerCase()
                  }
                ],
                EABi18n,
                language,
                textStore
              })
            : "";
        const inputStyle =
          checkIfNeedPlaceholder.length > 27 &&
          (!hasOthSaRule || !checkOthSaValue)
            ? { textAlign: "right", fontSize: 15 }
            : { textAlign: "right" };

        tableColumn.push(
          <View key={`${plan.covCode}F`}>
            <EABTextField
              testID={`Quotation__Non-Shield__txtOtherSa__${plan.covCode}`}
              inputStyle={inputStyle}
              supportLabelPlaceholder={false}
              placeholder={checkIfNeedPlaceholder}
              supportErrorMessage={false}
              value={
                !!planInputConfig && !planInputConfig.canEditOthSa
                  ? hasOthSaRule
                  : checkOthSaValue
              }
              onFocus={() => {
                this.setState({
                  focusedField: "othSa",
                  focusedPlan: planDetail.covCode
                });
              }}
              onChange={value => {
                const newPlans = _.cloneDeep(quotation.plans);
                newPlans[index][bpDetail.othSaKey] = currencyToNumber(value);

                updateQuotation({
                  ...quotation,
                  plans: newPlans
                });
              }}
              onBlur={() => {
                this.setState(
                  {
                    focusedField: null,
                    focusedPlan: null
                  },
                  () => {
                    quote(quotation);
                  }
                );
              }}
              prefix={
                !!planInputConfig &&
                (!quotation.plans[index][bpDetail.othSaKey] ||
                  !planInputConfig.canViewOthSa)
                  ? ""
                  : sign
              }
              maxLength={
                /* default can view premium (value === undefined) */
                !!planInputConfig && planInputConfig.canViewPremium === false
                  ? bpDetail.othSaDecimal
                  : sign.length +
                    bpDetail.othSaLength +
                    Math.floor(bpDetail.othSaLength / 3)
              }
              editable={!!planInputConfig.canEditOthSa}
              keyboardType="numeric"
            />
          </View>
        );
      }

      // get premium decimal
      const premiumDecimal = planInputConfig.premInput
        ? planInputConfig.premInput.decimal
        : 2;
      const premiumMaxLenth =
        /* default can view premium (value === undefined) */
        !!planInputConfig && planInputConfig.canViewPremium === false
          ? 10 + premiumDecimal + Math.floor(7 / 3)
          : sign.length + 10 + Math.floor(7 / 3);
      const premiumNumberToCurrency = numberToCurrencyForQuotation({
        value: plan.premium,
        decimal: premiumDecimal,
        isFocus: focusedField === "premium"
      });

      const checkIfEditable = planInputConfig.canEditPremium
        ? premiumNumberToCurrency
        : "";

      const checkIfNeedPlaceholder = planInputConfig.canEditPremium
        ? EABi18n({
            textStore,
            language,
            path: "quotation.placeholder.enterPremium"
          })
        : "";

      const checkPremiumValue = !quotation.plans[index].premium
        ? checkIfEditable
        : premiumNumberToCurrency;

      tableColumn.push(
        <View key={`${plan.covCode}G`}>
          <EABTextField
            testID={`Quotation__Non-Shield__txtPremium__${plan.covCode}`}
            numberOfLine={1}
            inputStyle={{ textAlign: "right" }}
            supportLabelPlaceholder={false}
            placeholder={checkIfNeedPlaceholder}
            supportErrorMessage={false}
            value={
              /* default can view premium (value === undefined) */
              !!planInputConfig && planInputConfig.canViewPremium === false
                ? "-"
                : checkPremiumValue
            }
            onFocus={() => {
              this.setState({
                focusedField: "premium",
                focusedPlan: planDetail.covCode
              });
            }}
            onChange={value => {
              const newPlans = _.cloneDeep(quotation.plans);
              newPlans[index].premium =
                value.indexOf(".") > -1
                  ? currencyToNumber(value)
                  : currencyToNumber(value.slice(0, premiumMaxLenth - 5));
              newPlans[index].calcBy = "premium";

              updateQuotation({
                ...quotation,
                plans: newPlans
              });
            }}
            onBlur={value => {
              const quotationClone = _.cloneDeep(quotation);
              this.setState(
                {
                  focusedField: null,
                  focusedPlan: null
                },
                () => {
                  quotationClone.plans[index].premium =
                    value.indexOf(".") > -1
                      ? currencyToNumber(value)
                      : currencyToNumber(value.slice(0, premiumMaxLenth - 5));
                  quotationClone.plans[index].calcBy = "premium";
                  quote(premiumDecimal > 0 ? quotation : quotationClone);
                }
              );
            }}
            prefix={
              /* default can view premium (value === undefined) */
              !!planInputConfig && planInputConfig.canViewPremium === false
                ? ""
                : sign
            }
            maxLength={premiumMaxLenth}
            editable={!!planInputConfig.canEditPremium}
            keyboardType="numeric"
          />
          {basicPlanData.premPostfix !== "" && quotation.premium > 0 ? (
            <Text style={Theme.tableColumnLabel}>
              {basicPlanData.premPostfix}
            </Text>
          ) : null}
        </View>
      );
      let showDeleteButton = false;
      _.forEach(riderList, element => {
        if (element.covCode === plan.covCode) {
          showDeleteButton = element.compulsory !== "Y";
        }
      });

      if (!isBasicPlan && showDeleteButton) {
        tableColumn.push(
          <View key="remove button">
            <Button
              testID="RemoveButton"
              onPress={() => {
                Alert.alert(
                  EABi18n({
                    textStore,
                    language,
                    path: "alert.confirm"
                  }),
                  EABi18n({
                    textStore,
                    language,
                    path: "quotation.warning.removeRider"
                  }),
                  [
                    {
                      text: EABi18n({
                        language,
                        textStore,
                        path: "button.no"
                      }),
                      onPress: () => {}
                    },
                    {
                      text: EABi18n({
                        language,
                        textStore,
                        path: "button.yes"
                      }),
                      onPress: () => {
                        let selectedCovCodes = [];
                        _.each(quotation.plans, p => {
                          if (p.covCode !== planDetail.covCode) {
                            selectedCovCodes.push(p.covCode);
                          }
                        });
                        selectedCovCodes = removeInclusiveRiders({
                          covCode: plan.covCode,
                          selectedCovCodes,
                          quotation,
                          bpDetail
                        });
                        selectedCovCodes = removeDisabledRiders({
                          selectedCovCodes,
                          quotation,
                          bpDetail,
                          riderList
                        });
                        updateRiders({
                          covCodes: selectedCovCodes
                        });
                      }
                    }
                  ]
                );
              }}
            >
              <Image source={icRemove} />
            </Button>
          </View>
        );
      } else {
        tableColumn.push(<View key="remove button" />);
      }
      els.push(
        <EABTableSection key={plan.covCode} style={styles.tableRow}>
          {tableColumn}
        </EABTableSection>
      );

      /* get hint row */
      if (focusedPlan === plan.covCode) {
        let limit = null;
        let multiple = null;

        switch (focusedField) {
          case "premium": {
            limit = planInputConfig.premlim;
            if (
              planInputConfig.premInput &&
              planInputConfig.premInput.factor &&
              planInputConfig.premInput.factor > 1
            ) {
              multiple = planInputConfig.premInput.factor;
            }
            break;
          }
          case "sumInsured": {
            limit = planInputConfig.benlim;
            if (
              planInputConfig.saInput &&
              planInputConfig.saInput.factor &&
              planInputConfig.saInput.factor > 1
            ) {
              multiple = planInputConfig.saInput.factor;
            }
            break;
          }
          case "othSa":
            break;
          default:
            throw new Error(`unexpected state focusedField: "${focusedField}"`);
        }

        if (limit && (limit.min || limit.max)) {
          const { min, max } = limit;
          let path = "";
          let params = null;

          switch (true) {
            case !!min && !!max: {
              path = `quotation.hint.range.${focusedField}`;
              params = [
                {
                  value: getCurrency({ value: min, sign, decimals: 2 }),
                  isPath: false
                },
                {
                  value: getCurrency({ value: max, sign, decimals: 2 }),
                  isPath: false
                }
              ];
              break;
            }
            case !!max: {
              path = `quotation.hint.max.${focusedField}`;
              params = [
                {
                  value: getCurrency({ value: max, sign, decimals: 2 }),
                  isPath: false
                },
                {
                  value: getCurrency({ value: multiple, sign, decimals: 0 }),
                  isPath: false
                }
              ];
              break;
            }
            case !!min: {
              path = `quotation.hint.min.${focusedField}`;
              params = [
                {
                  value: `${sign}${numberToCurrency({
                    value: min,
                    decimal: 2
                  })}`,
                  isPath: false
                },
                {
                  value: _.includes(quotation.baseProductId, "VUL")
                    ? getCurrency({ value: multiple, sign: null, decimals: 0 })
                    : getCurrency({ value: multiple, sign, decimals: 0 }),
                  isPath: false
                }
              ];
              break;
            }
            default:
              throw new Error(`unexpected limit`);
          }

          if (multiple) {
            path += ".multiple";
            params.push({
              value: getCurrency({
                value: multiple,
                sign,
                decimals: 0
              }),
              isPath: false
            });
          }

          els.push(
            <EABTableSection
              key={path}
              style={styles.tableRow}
              tableConfig={[{ key: "hint", style: styles.tableHint }]}
            >
              <View>
                <TranslatedText
                  style={Theme.fieldTextOrHelperTextBrand}
                  path={path}
                  replace={params}
                />
              </View>
            </EABTableSection>
          );
        } else if (planInputConfig.planInfoHintMsg) {
          els.push(
            <EABTableSection
              key="hint"
              style={styles.tableRow}
              tableConfig={[{ key: "hint", style: styles.tableHint }]}
            >
              <View>
                <Text style={Theme.fieldTextOrHelperTextBrand}>
                  {planInputConfig.planInfoHintMsg}
                </Text>
              </View>
            </EABTableSection>
          );
        } else if (planInputConfig.planInfoHintMsg_othSa) {
          els.push(
            <EABTableSection
              key="hint"
              style={styles.tableRow}
              tableConfig={[{ key: "hint", style: styles.tableHint }]}
            >
              <View>
                <Text style={Theme.fieldTextOrHelperTextBrand}>
                  {planInputConfig.planInfoHintMsg_othSa}
                </Text>
              </View>
            </EABTableSection>
          );
        }
      } else if (planError.length > 0) {
        // because the product is config by user, we can not limit the error
        // format. so we just support these two cases.
        const errorLogger = {};
        planError.forEach(error => {
          let errorElement = null;
          let errorKey = "";
          if (error.msg) {
            errorKey = error.msg + plan.covCode;
            errorElement = errorLogger[error.msg] ? null : (
              <Text style={Theme.fieldTextOrHelperTextNegative}>
                {error.msg}
              </Text>
            );
            errorLogger[error.msg] = true;
          } else if (_.trim(error.code)) {
            errorKey = error.code + plan.covCode;
            if (!errorLogger[error.code]) {
              let errorMessage = isTextPathUndefined({
                language,
                textStore,
                path: `quotation.error.${error.code}`
              })
                ? error.code
                : EABi18n({
                    language,
                    textStore,
                    path: `quotation.error.${error.code}`
                  });
              const params =
                error.msgPara &&
                error.msgPara.map(variable => ({
                  value: getEABTextByLangType({ language, data: variable }),
                  isPath: false
                }));

              if (params) {
                errorMessage = fillTextVariable({
                  text: errorMessage,
                  data: params,
                  EABi18n,
                  language,
                  textStore
                });
              }

              // some products have a useless error code " "
              if (errorMessage !== " " && errorMessage !== "") {
                errorElement = (
                  <Text style={Theme.fieldTextOrHelperTextNegative}>
                    {errorMessage}
                  </Text>
                );
              }
            }
            errorLogger[error.msg] = true;
          }
          // some product's planError.length is larger than 1
          if (errorElement !== null) {
            els.push(
              <EABTableSection
                key={errorKey}
                style={styles.tableRow}
                tableConfig={[{ key: "error", style: styles.tableHint }]}
              >
                <View>{errorElement}</View>
              </EABTableSection>
            );
          }
        });
      }

      /* get break down */
      const pDec =
        planInputConfig.premInput && planInputConfig.premInput.decimal;
      const saDec = planInputConfig.saInput && planInputConfig.saInput.decimal;
      const sumInsured =
        !planInputConfig ||
        !planInputConfig.canViewSumAssured ||
        !quotation.sumInsured
          ? "-"
          : getCurrency({
              value: plan.sumInsured || 0,
              sign,
              decimals: saDec || 0
            }) || "-";

      breakDownEls.push(
        <EABTableSection key={plan.covCode} style={styles.tableRow}>
          <View>
            <Text style={Theme.subheadPrimary}>
              {getEABTextByLangType({ data: plan.covName, language })}
            </Text>
          </View>
          <View>
            <Text style={Theme.subheadPrimary}>{sumInsured}</Text>
          </View>
          <View>
            <Text style={Theme.subheadPrimary}>
              {getCurrency({
                value: requireGST
                  ? (plan.monthPrem || 0) +
                    ((plan.tax && plan.tax.monthTax) || 0)
                  : plan.monthPrem || "-",
                sign,
                decimals: pDec || 2
              }) || "-"}
            </Text>
          </View>
          <View>
            <Text style={Theme.subheadPrimary}>
              {getCurrency({
                value: requireGST
                  ? (plan.quarterPrem || 0) +
                    ((plan.tax && plan.tax.quarterTax) || 0)
                  : plan.quarterPrem || "-",
                sign,
                decimals: pDec
              }) || "-"}
            </Text>
          </View>
          <View>
            <Text style={Theme.subheadPrimary}>
              {getCurrency({
                value: requireGST
                  ? (plan.halfYearPrem || 0) +
                    ((plan.tax && plan.tax.halfYearTax) || 0)
                  : plan.halfYearPrem || "-",
                sign,
                decimals: pDec
              }) || "-"}
            </Text>
          </View>
          <View>
            <Text style={Theme.subheadPrimary}>
              {getCurrency({
                value: requireGST
                  ? (plan.yearPrem || 0) + ((plan.tax && plan.tax.yearTax) || 0)
                  : plan.yearPrem || "-",
                sign,
                decimals: pDec
              }) || "-"}
            </Text>
          </View>
        </EABTableSection>
      );
    });

    /* get rider header */
    const riderObject = {
      riders,
      ridersBreakDown
    };
    if (riders.length > 0) {
      riderObject.riderHeader = (
        <TableSectionHeader
          key="rider title"
          style={styles.basicPlanHeader}
          tableConfig={[
            {
              key: "A",
              style: styles.planHeader
            }
          ]}
        >
          <View>
            <Text style={Theme.captionBrand}>
              {EABi18n({
                language,
                textStore,
                path: "quotation.table.riders"
              })}
            </Text>
          </View>
        </TableSectionHeader>
      );
    }

    return {
      tableConfig,
      header: (
        <TableHeader key="table header" style={styles.tableHeader}>
          {header}
        </TableHeader>
      ),
      basicPlanHeader: (
        <TableSectionHeader
          key="basic plan title"
          style={styles.basicPlanHeader}
          tableConfig={[
            {
              key: "A",
              style: styles.planHeader
            }
          ]}
        >
          <View>
            <Text style={Theme.captionBrand}>
              {EABi18n({
                language,
                textStore,
                path: "quotation.table.basicPlan"
              })}
            </Text>
          </View>
        </TableSectionHeader>
      ),
      basicPlan,
      basicPlanBreakDown,
      ...riderObject
    };
  }

  getSelectPortfolioTable(language) {
    const { textStore, optionsMap } = this.props;
    const { portfolioAllocsTemp, portfolio } = this.state;

    function customizeAllocation({ minValue, maxValue }) {
      const allocOptions = [];
      for (let n = Number(minValue); n <= Number(maxValue); n += 1) {
        allocOptions.push({
          key: `${n}%`,
          label: `${n}%`,
          value: n
        });
      }
      return allocOptions;
    }
    function getPortfolioModelTitle({ portfolioList }) {
      let currentPortfolioRiskProfile = {};
      portfolioList.forEach(portfolioOption => {
        portfolioOption.models.forEach(model => {
          if (model.id === portfolio) {
            currentPortfolioRiskProfile = portfolioOption;
          }
        });
      });

      currentPortfolioRiskProfile =
        portfolioList[
          _.findIndex(portfolioList, portfolioOption =>
            _.isEqual(currentPortfolioRiskProfile, portfolioOption)
          )
        ].riskProfile;

      return EABi18n({
        textStore,
        language,
        path: `quotation.fund.riskProfile.${currentPortfolioRiskProfile}`
      });
    }

    const items = [];
    const groupsCount =
      portfolioAllocsTemp.groups !== null
        ? portfolioAllocsTemp.groups.length
        : null;
    let groupsIndex = 0;
    _.each(portfolioAllocsTemp.groups, group => {
      const riskName = getOptionTitle({
        value: group.riskRatings[0],
        optionsMap: optionsMap.riskRating,
        language: languageTypeConvert(language)
      });
      groupsIndex += 1;
      const minimum =
        groupsIndex !== groupsCount ? group.min : group.percentage;

      items.push({
        riskName,
        riskRating: group.riskRatings[0],
        percentage: group.percentage,
        minimum,
        maximum: group.max,
        selectedValue: group.percentageNew
      });
    });
    const tableConfig = [
      {
        key: "riskName",
        style: styles.fundDialogField
      },
      {
        key: "riskPercentage",
        style: styles.fundDialogField
      },
      {
        key: "riskPercentageRange",
        style: styles.fundDialogField
      },
      {
        key: "riskAllocation",
        style: styles.fundDialogField
      }
    ];

    const tableHeader = [
      <View key="riskName">
        <Text style={Theme.tableColumnLabel}>
          {getPortfolioModelTitle({
            portfolioList: optionsMap.portfolioModels.portfolios
          })}
        </Text>
      </View>,
      <View key="riskPercentage">
        <Text style={Theme.tableColumnLabel}>{portfolioAllocsTemp.title}</Text>
      </View>,
      <View key="riskPercentageRange">
        <Text style={Theme.tableColumnLabel}>
          {`${EABi18n({
            language,
            textStore,
            path: "quotation.portfolio.possibleRange"
          })}(%)`}
        </Text>
      </View>,
      <View key="riskAllocation">
        <Text style={Theme.tableColumnLabel}>
          {`${EABi18n({
            language,
            textStore,
            path: "quotation.portfolio.adjustedAllocs"
          })}(%)`}
        </Text>
      </View>
    ];

    const tableRows = _.map(items, risk => (
      <EABTableSection key={risk.riskRating}>
        <View>
          <Text style={Theme.subheadPrimary}>{risk.riskName}</Text>
        </View>
        <View>
          <Text style={Theme.subheadPrimary}>{`${risk.percentage}%`}</Text>
        </View>
        <View>
          <Text style={Theme.subheadPrimary}>{`${risk.minimum}% to ${
            risk.maximum
          }%`}</Text>
        </View>
        <View>
          <Selector
            selectorType={FLAT_LIST}
            hasCard={false}
            hasLabel
            popoverOptions={{
              preferredContentSize: [200, 500]
            }}
            flatListOptions={{
              data: customizeAllocation({
                minValue: risk.minimum,
                maxValue: risk.maximum
              }),
              renderItemOnPress: (label, option) => {
                const newPortfolioAllocs = _.cloneDeep(portfolioAllocsTemp);
                const newGroup = _.each(newPortfolioAllocs.groups, group => {
                  if (risk.riskRating === group.riskRatings[0]) {
                    group.percentageNew = option.value;
                  }
                });
                newPortfolioAllocs.groups = newGroup;
                this.setState({
                  portfolioAllocsTemp: BasicQuotation.adjustModelRange({
                    portfolio: newPortfolioAllocs
                  })
                });
              },
              selectedValue: `${risk.selectedValue}%`
            }}
          />
        </View>
      </EABTableSection>
    ));

    return {
      tableConfig,
      tableHeader,
      tableRows
    };
  }

  getSelectFundsTable(language) {
    const {
      hasTopUpAlloc,
      filterRiskRatings,
      riskProfile,
      getCurrentPortfolioModel
    } = this;
    const { selectedFunds, fundAllocsBackUp, topUpAllocsBackUp } = this.state;
    const { optionsMap, textStore, availableFunds } = this.props;
    const { searchRisk, fundsSearchText } = this.state;
    const { architasFunds } = optionsMap.portfolioModels;
    const allocOptions = [];

    // if unselect, return true,
    // if select fund, then check select fund < 10
    const selectFundRule = fund =>
      !selectedFunds[fund.fundCode]
        ? _.filter(selectedFunds, f => f).length < FUND_MAX_SELECTION
        : true;

    for (let n = 0; n <= 100; n += 1) {
      allocOptions.push({
        key: `${n}%`,
        label: `${n}%`,
        value: n
      });
    }

    let filterFunds = availableFunds;
    if (
      getCurrentPortfolioModel &&
      getCurrentPortfolioModel.title === "Architas"
    ) {
      filterFunds = _.filter(filterFunds, f =>
        architasFunds.includes(f.fundCode)
      );
    }

    const searchValues = fundsSearchText.split(" ").map(v => v.toLowerCase());

    const tableConfig = [
      {
        key: "check box",
        style: styles.fundDialogFieldCheckBox
      },
      {
        key: "fund name",
        style: styles.fundDialogFieldFundName
      },
      {
        key: "currency",
        style: styles.fundDialogCurrencyField
      },
      {
        key: "asset class",
        style: styles.fundDialogField
      },
      {
        key: "risk rating",
        style: styles.fundDialogField
      },
      {
        key: "premium allocation",
        style: styles.fundDialogPercentageField
      }
    ];

    const tableHeader = [
      <View key="check box">{/* alignment for check box */}</View>,
      <View key="fundName">
        <Text style={Theme.tableColumnLabel}>
          {EABi18n({
            language,
            textStore,
            path: "quotation.fund.fundList"
          })}
        </Text>
      </View>,
      <View key="currency">
        <Text style={Theme.tableColumnLabel}>
          {EABi18n({
            language,
            textStore,
            path: "quotation.fund.currency"
          })}
        </Text>
      </View>,
      <View key="assetClass">
        <Text style={Theme.tableColumnLabel}>
          {EABi18n({
            language,
            textStore,
            path: "quotation.fund.assetClass"
          })}
        </Text>
      </View>,
      <View key="riskRating">
        <Text style={Theme.tableColumnLabel}>
          {EABi18n({
            language,
            textStore,
            path: "quotation.fund.riskRating"
          })}
        </Text>
      </View>,
      <View key="premium allocation">
        <Text style={Theme.tableColumnLabel}>
          {EABi18n({
            language,
            textStore,
            path: "quotation.fund.premiumAllocation"
          })}
        </Text>
      </View>
    ];

    if (hasTopUpAlloc) {
      tableConfig.push({
        key: "top up allocation",
        style: styles.fundDialogField
      });
      tableHeader.push(
        <View key="top up allocation">
          <Text style={Theme.tableColumnLabel}>
            {EABi18n({
              language,
              textStore,
              path: "quotation.fund.topUpAllocation"
            })}
          </Text>
        </View>
      );
    }

    const tableRows =
      filterFunds.length === 0 ? (
        <EABTableSection
          key="no funds"
          tableConfig={[{ key: "no fund description", style: { flex: 1 } }]}
        >
          <View>
            <Text>
              {EABi18n({
                language,
                textStore,
                path: "quotation.fund.noFund"
              })}
            </Text>
          </View>
        </EABTableSection>
      ) : (
        _.map(filterFunds, fund => {
          const fundName = getEABTextByLangType({
            data: fund.fundName,
            language
          });
          const assetClassDesc = getOptionTitle({
            value: fund.assetClass,
            optionsMap: optionsMap.assetClass,
            language: languageTypeConvert(language)
          });
          const riskRatingDesc = getOptionTitle({
            value: fund.riskRating,
            optionsMap: optionsMap.riskRating,
            language: languageTypeConvert(language)
          });

          // use explicit risk ratings first if exists, for model portfolio
          if (filterRiskRatings) {
            if (filterRiskRatings.indexOf(fund.riskRating) === -1) {
              return null;
            }
          } else if (riskProfile && fund.riskRating > riskProfile) {
            return null;
          }

          if (
            searchRisk &&
            searchRisk !== "*" &&
            fund.riskRating !== searchRisk
          ) {
            return null;
          }

          if (fundsSearchText) {
            const matchedValues = _.filter(
              searchValues,
              v =>
                fundName.toLowerCase().indexOf(v) > -1 ||
                fund.ccy.toLowerCase().indexOf(v) > -1 ||
                fund.assetClass.toLowerCase().indexOf(v) > -1
            );
            if (matchedValues.length !== searchValues.length) {
              return null;
            }
          }

          return (
            <EABTableSection key={fund.fundCode}>
              <View>
                <CheckBox
                  style={styles.checkBoxWrapper}
                  checkBoxStyle={styles.checkBox}
                  options={[
                    {
                      key: fund.fundCode,
                      isSelected: !!selectedFunds[fund.fundCode],
                      fundCode: fund.fundCode
                    }
                  ]}
                  onPress={() => {
                    /* max 10 funds */
                    if (selectFundRule(fund)) {
                      this.setState({
                        // if cancel, reset the alloc
                        fundAllocsBackUp: {
                          ...fundAllocsBackUp,
                          [fund.fundCode]:
                            selectedFunds[fund.fundCode] === true
                              ? 0
                              : fundAllocsBackUp[fund.fundCode]
                        },
                        // if cancel, reset the alloc
                        topUpAllocsBackUp: {
                          ...topUpAllocsBackUp,
                          [fund.fundCode]:
                            selectedFunds[fund.fundCode] === true
                              ? 0
                              : fundAllocsBackUp[fund.fundCode]
                        },
                        selectedFunds: {
                          ...selectedFunds,
                          [fund.fundCode]: !selectedFunds[fund.fundCode]
                        }
                      });
                    } else {
                      Alert.alert(
                        EABi18n({
                          textStore,
                          language,
                          path: "alert.warning"
                        }),
                        EABi18n({
                          path: "quotation.warning.maxSelected",
                          language,
                          textStore
                        }),
                        [
                          {
                            text: EABi18n({
                              language,
                              textStore,
                              path: "button.ok"
                            }),
                            onPress: () => {}
                          }
                        ]
                      );
                    }
                  }}
                />
              </View>
              <View>
                <Text style={Theme.subheadPrimary}>{fundName}</Text>
              </View>
              <View>
                <Text style={Theme.subheadPrimary}>{fund.ccy}</Text>
              </View>
              <View>
                <Text style={Theme.subheadPrimary}>{assetClassDesc}</Text>
              </View>
              <View>
                <Text style={Theme.subheadPrimary}>{riskRatingDesc}</Text>
              </View>
              <View>
                <Selector
                  selectorType={FLAT_LIST}
                  hasCard={false}
                  popoverOptions={{
                    preferredContentSize: [200, 500]
                  }}
                  flatListOptions={{
                    data: allocOptions,
                    renderItemOnPress: (label, option) => {
                      if (selectFundRule(fund)) {
                        this.setState({
                          fundAllocsBackUp: {
                            ...fundAllocsBackUp,
                            [fund.fundCode]: option.value
                          },
                          selectedFunds: {
                            ...selectedFunds,
                            [fund.fundCode]: option.value !== 0
                          }
                        });
                      } else {
                        Alert.alert(
                          EABi18n({
                            textStore,
                            language,
                            path: "alert.warning"
                          }),
                          EABi18n({
                            path: "quotation.warning.maxSelected",
                            language,
                            textStore
                          }),
                          [
                            {
                              text: EABi18n({
                                language,
                                textStore,
                                path: "button.ok"
                              }),
                              onPress: () => {}
                            }
                          ]
                        );
                      }
                    },
                    selectedValue: fundAllocsBackUp[fund.fundCode] || 0
                  }}
                />
              </View>
              {hasTopUpAlloc ? (
                <View>
                  <Selector
                    selectorType={FLAT_LIST}
                    hasCard={false}
                    popoverOptions={{
                      preferredContentSize: [200, 500]
                    }}
                    flatListOptions={{
                      data: allocOptions,
                      renderItemOnPress: (label, option) => {
                        /* max 10 funds */
                        if (selectFundRule(fund)) {
                          this.setState({
                            fundAllocsBackUp: {
                              ...fundAllocsBackUp,
                              [fund.fundCode]:
                                fundAllocsBackUp[fund.fundCode] || 0
                            },
                            topUpAllocsBackUp: {
                              ...topUpAllocsBackUp,
                              [fund.fundCode]: option.value
                            },
                            selectedFunds: {
                              ...selectedFunds,
                              [fund.fundCode]: option.value !== 0
                            }
                          });
                        } else {
                          Alert.alert(
                            EABi18n({
                              textStore,
                              language,
                              path: "alert.warning"
                            }),
                            EABi18n({
                              path: "quotation.warning.maxSelected",
                              language,
                              textStore
                            }),
                            [
                              {
                                text: EABi18n({
                                  language,
                                  textStore,
                                  path: "button.ok"
                                }),
                                onPress: () => {}
                              }
                            ]
                          );
                        }
                      },
                      selectedValue: topUpAllocsBackUp[fund.fundCode] || 0
                    }}
                  />
                </View>
              ) : null}
            </EABTableSection>
          );
        })
      );

    return {
      tableConfig,
      tableHeader,
      tableRows
    };
  }

  productSpecialHandle({ language, type, id, value, policyOption }) {
    const { quotation, quote, textStore, resetQuot } = this.props;
    let update = false;
    const newQuotation = _.cloneDeep(quotation);
    switch (type) {
      case "policyOption":
        if (
          (quotation.baseProductCode === "FPX" ||
            quotation.baseProductCode === "FSX") &&
          !quotation.policyOptions.insuranceCharge &&
          id === "basicBenefit"
        ) {
          update = true;
          newQuotation.policyOptions.insuranceCharge = "yrt";
        }

        if (update) {
          policyOptionOnChange({
            quotation: newQuotation,
            quote,
            resetQuot,
            textStore,
            language,
            policyOption,
            newValue: value
          });
        }
        break;
      default:
        break;
    }
  }

  openPortfolioDialogCallBack() {
    const { portfolioAllocs, isPortfolioSelected } = this;
    const { portfolioAllocsBackUp } = this.state;

    if (isPortfolioSelected && _.isEmpty(portfolioAllocsBackUp)) {
      this.setState({
        shouldShowSelectPortfolioDialog: true,
        portfolioAllocsBackUp: BasicQuotation.adjustModelRange({
          portfolio: portfolioAllocs
        }),
        portfolioAllocsTemp: BasicQuotation.adjustModelRange({
          portfolio: portfolioAllocs
        })
      });
    } else {
      this.setState({
        shouldShowSelectPortfolioDialog: true
      });
    }
  }

  openFundsDialogCallBack() {
    const { fundAllocs, topUpAllocs } = this;
    const { quotation, getFunds } = this.props;
    const { portfolio, invOpt } = quotation.fund || this.state;

    this.setState({ isLoading: true }, () => {
      getFunds({
        invOpt,
        callback: () => {
          this.setState(
            {
              isLoading: false
            },
            () => {
              const selectedFunds = {};
              _.each(fundAllocs, (alloc, fundCode) => {
                selectedFunds[fundCode] = true;
              });
              _.each(topUpAllocs, (alloc, fundCode) => {
                selectedFunds[fundCode] = true;
              });

              setTimeout(() => {
                this.setState({
                  shouldShowSelectFundsDialog: true,
                  invOpt,
                  portfolio,
                  selectedFunds,
                  fundAllocsBackUp: fundAllocs,
                  topUpAllocsBackUp: topUpAllocs
                });
              }, 200);
            }
          );
        }
      });
    });
  }
  /**
   * @description get options for portfolio field
   * @requires riskDescription
   * @requires riskProfile
   * @return {array} options array
   * */
  portfolioOptions(language) {
    const { riskProfile, riskDescription } = this;
    const { optionsMap, textStore } = this.props;
    function getPortfolioModelDesc({ riskProfileData, modelTitle, groups }) {
      const riskProfileDesc = EABi18n({
        textStore,
        language,
        path: `quotation.fund.riskProfile.${riskProfileData}`
      });
      const riskDesc = _.join(
        _.map(groups, group => riskDescription({ language, group })),
        ", "
      );

      return `${riskProfileDesc} | ${modelTitle} (${riskDesc})`;
    }

    const portfolioOptionsMap = optionsMap.portfolioModels.portfolios.filter(
      option => option.riskProfile <= riskProfile
    );

    const returnArray = [];
    if (portfolioOptionsMap) {
      portfolioOptionsMap.forEach(portfolioOption => {
        portfolioOption.models.forEach(model => {
          if (
            model.title === "Architas" &&
            (this.props.quotation.baseProductCode === "FSX" ||
              this.props.quotation.baseProductCode === "FPX" ||
              this.props.quotation.baseProductCode === "AWICP")
          ) {
            return;
          }
          returnArray.push({
            key: model.id,
            label: model.any
              ? getEABTextByLangType({
                  data: model.title,
                  language
                })
              : getPortfolioModelDesc({
                  riskProfileData: portfolioOption.riskProfile,
                  modelTitle: model.title,
                  groups: model.groups
                }),
            value: model.id
          });
        });
      });
    }

    return returnArray;
  }
  /**
   * @description get the risk description for group
   * @return {string} risk description
   * */
  riskDescription({ isAdjusted, group, language }) {
    const { optionsMap } = this.props;

    const riskRatingsDesc = _.join(
      _.map(group.riskRatings, value =>
        getOptionTitle({
          value,
          optionsMap: optionsMap.riskRating,
          language: languageTypeConvert(language)
        })
      ),
      " / "
    );

    return `${
      isAdjusted ? group.percentageNew : group.percentage
    }% ${riskRatingsDesc}`;
  }

  clientInfoArray(language) {
    const {
      quotation,
      optionsMap,
      textStore,
      dependant,
      clientPhoto
    } = this.props;
    const returnArray = [];

    if (quotation.sameAs === "N") {
      returnArray.push(
        clientInfoObject({
          key: "proposer",
          language,
          textStore,
          optionsMap,
          policytype: quotation.policyOptions.policytype,
          gender: quotation.pGender,
          age: quotation.pAge,
          smoke: quotation.pSmoke,
          occupation: quotation.pOccupation,
          residence: quotation.pResidence,
          fullName: quotation.pFullName,
          covCode: quotation.baseProductCode,
          role: "P",
          photo: clientPhoto
        })
      );
    }

    returnArray.push(
      clientInfoObject({
        key: "insured",
        language,
        textStore,
        optionsMap,
        policytype: quotation.policyOptions.policytype,
        gender: quotation.iGender,
        age: quotation.iAge,
        smoke: quotation.iSmoke,
        occupation: quotation.iOccupation,
        residence: quotation.iResidence,
        role: "I",
        fullName: quotation.iFullName,
        covCode: quotation.baseProductCode,
        photo:
          quotation.sameAs === "N"
            ? dependant[quotation.iCid].photo
            : clientPhoto
      })
    );

    return returnArray;
  }
  /**
   * @description select invOpt
   * @param {string} invOptData - selected invOpt option
   * @param language
   * @param {boolean} [shouldCloseDisclaimer=false] - should close disclaimer
   *   dialog
   * */
  selectInvOpt({ invOptData, language, shouldCloseDisclaimer = false }) {
    const { fundAllocs, topUpAllocs, hasTopUpAlloc } = this;
    const { textStore, quote, quotation } = this.props;
    const { openDisclaimer } = this.state;

    if (!_.isEmpty(fundAllocs) || (hasTopUpAlloc && !_.isEmpty(topUpAllocs))) {
      Alert.alert(
        EABi18n({
          textStore,
          language,
          path: "alert.confirm"
        }),
        EABi18n({
          textStore,
          language,
          path: "quotation.warning.confirmCleanFund"
        }),
        [
          {
            text: EABi18n({ language, textStore, path: "button.no" }),
            onPress: () => {}
          },
          {
            text: EABi18n({
              language,
              textStore,
              path: "button.yes"
            }),
            onPress: () => {
              this.setState(
                {
                  searchRisk: "*",
                  invOpt: invOptData,
                  portfolio: null,
                  openDisclaimer: shouldCloseDisclaimer
                    ? false
                    : openDisclaimer,
                  selectedFunds: {},
                  fundAllocsBackUp: {},
                  topUpAllocsBackUp: {}
                },
                () => {
                  quote({
                    ...quotation,
                    fund: null
                  });
                }
              );
            }
          }
        ]
      );
    } else {
      this.setState({
        invOpt: invOptData,
        portfolio: null,
        openDisclaimer: shouldCloseDisclaimer ? false : openDisclaimer
      });
    }
  }
  /**
   * @description indicate which behavior should be trigger on onChange
   *   investment Option event
   * */
  changeInvOpt({ option, language }) {
    const { selectInvOpt } = this;

    if (option.value === "buildPortfolio") {
      this.setState({
        openDisclaimer: true
      });
    } else {
      this.setState({
        portfolioAllocsBackUp: {},
        portfolioAllocsTemp: {}
      });
      selectInvOpt({ invOptData: option.value, language });
    }
  }

  render() {
    // =========================================================================
    // variables
    // =========================================================================
    const {
      getTableConfig,
      clientInfoArray,
      isQuotationHaveFunds,
      onChangePayMode,
      getRiderButton,
      openFundsDialogCallBack,
      openPortfolioDialogCallBack,
      getSelectFundsTable,
      getSelectPortfolioTable,
      getMandatoryPolicyOptions,
      sign,
      hasTopUpAlloc,
      fundAllocs,
      topUpAllocs,
      filterRiskRatings,
      getCurrentPortfolioModel,
      getFundAllocBackUpTotal,
      getTopUpAllocBackUpTotal,
      getPortfolioAllocTotal,
      riskProfile,
      changeInvOpt,
      getFundTableRows,
      selectInvOpt,
      isInvOptBuildPortfolio,
      portfolioOptions,
      riskDescription,
      getSelectedFunds,
      isPortfolioSelected,
      isPortfolioAdjusted,
      portfolioAllocs
    } = this;
    const {
      fundsSearchText,
      shouldShowSelectRiderDialog,
      shouldShowSelectFundsDialog,
      shouldShowSelectPortfolioDialog,
      shouldExpandBottomSheet,
      fundAllocsBackUp,
      portfolioAllocsBackUp,
      portfolioAllocsTemp,
      topUpAllocsBackUp,
      portfolio,
      invOpt,
      searchRisk,
      openDisclaimer,
      isLoading
    } = this.state;
    const {
      agent,
      optionsMap,
      updateRiders,
      planDetails,
      textStore,
      allocFunds,
      inputConfigs,
      quotation,
      quote,
      updateQuotation,
      resetQuot,
      availableFunds,
      quotWarnings,
      quotationErrors
    } = this.props;

    // =========================================================================
    // functions
    // =========================================================================
    const clientInfoUnitStyle = index => {
      if (index === 0) {
        return styles.clientInfoFirst;
      }
      return styles.clientInfo;
    };
    // =========================================================================
    // render functions
    // =========================================================================
    /**
     * renderClientInfo
     * @requires clientInfoArray
     * @requires clientInfoUnitStyle
     * @return {element} client info section UI
     * */
    const renderClientInfo = () => (
      <ContextConsumer>
        {({ language }) => (
          <View style={styles.clientInfoWrapper}>
            {clientInfoArray(language).map((client, index) => (
              <View key={client.key} style={clientInfoUnitStyle(index)}>
                <View style={styles.role}>
                  <Text style={Theme.captionBrand}>{client.role}</Text>
                </View>
                <ProfileUI
                  photo={client.photo}
                  name={client.fullName}
                  firstDescription={client.infoA}
                  secondDescription={client.infoB}
                />
              </View>
            ))}
          </View>
        )}
      </ContextConsumer>
    );
    /**
     * renderConfig
     * @return {element} config section UI
     * */
    const renderConfig = language => {
      const noteGroup = [];
      const basicPlan = planDetails[quotation.baseProductCode];

      /* get currency options array */
      const currencyOptions = [];

      if (basicPlan && basicPlan.currencies) {
        // get currency options form optionsMap
        const ccyOptions = _.find(
          optionsMap.ccy.currencies,
          c => c.compCode === agent.compCode
        );
        // get residence country available currency options
        const currencies = _.find(
          basicPlan.currencies,
          c => c.country === "*" || c.country === quotation.iResidence
        );
        if (currencies && currencies.ccy) {
          _.each(ccyOptions.options, ccyOption => {
            if (currencies.ccy.indexOf(ccyOption.value) > -1) {
              currencyOptions.push({
                key: ccyOption.value,
                label: getEABTextByLangType({
                  data: ccyOption.title,
                  language
                }),
                value: ccyOption.value
              });
            }
          });
        }
      }
      return (
        <View style={styles.configWrapper}>
          <EABCard
            isPlainText
            isSelected={false}
            style={styles.configField}
            contentStyle={styles.checkBoxField}
          >
            <CheckBox
              testID="Quotation__Non-Shield"
              checkBoxStyle={{
                paddingVertical: 0
              }}
              options={[
                {
                  key: "label",
                  testID: "BackDate",
                  text: EABi18n({
                    language,
                    textStore,
                    path: "quotation.config.backDate"
                  }),
                  isSelected: quotation.isBackDate === "Y"
                }
              ]}
              onPress={() =>
                quote({
                  ...quotation,
                  isBackDate: quotation.isBackDate === "Y" ? "N" : "Y"
                })
              }
              disable={basicPlan.allowBackdate !== "Y"}
            />
          </EABCard>
          {quotation.isBackDate === "Y" ? (
            <Selector
              testID="Quotation__Non-Shield__dateCommencementDate"
              style={styles.configField}
              selectorType={DATE_PICKER}
              hasLabel
              label={EABi18n({
                language,
                textStore,
                path: "quotation.config.selectedCommencementDate"
              })}
              disable={basicPlan.allowBackdate !== "Y"}
              datePickerIOSOptions={{
                onDateChange: value => {
                  const { effDate } = planDetails[quotation.baseProductCode];
                  let errorMsg = "";
                  const alertDayFormat = moment(effDate).format(
                    DATE_FORMAT_INPUT
                  );
                  const alertDayFormatForDisplay = moment(effDate).format(
                    "DD-MM-YYYY"
                  );
                  const getDay = `${value.getMonth() +
                    1}/${value.getDate()}/${value.getFullYear()}`;
                  const getDatFormat = moment(getDay, "MM/DD/YYYY").format(
                    DATE_FORMAT_INPUT
                  );

                  let monthDiff =
                    12 * (moment().format("YYYY") - value.getFullYear()) +
                    (moment().format("MM") - (value.getMonth() + 1));

                  if (moment().format("DD") - value.getDate() > 0) {
                    monthDiff += 1;
                  }

                  if (monthDiff > 6) {
                    errorMsg = EABi18n({
                      language,
                      textStore,
                      path: "quotation.error.backDateOlderThanSixMonths"
                    });
                  } else if (getDatFormat < alertDayFormat) {
                    errorMsg = fillTextVariable({
                      text: (errorMsg = EABi18n({
                        language,
                        textStore,
                        path: "quotation.error.backDateBeforeEffDate"
                      })),
                      data: [
                        {
                          value: alertDayFormatForDisplay
                        }
                      ],
                      EABi18n,
                      language,
                      textStore
                    });
                  } else {
                    const entryAge = _.find(
                      basicPlan.entryAge,
                      ea => ea.country === "*" || quotation.iResidence
                    );
                    if (entryAge) {
                      if (entryAge.ominAge) {
                        const dob = new Date(quotation.pDob);
                        const age = getAgeByUnit(
                          entryAge.ominAgeUnit,
                          value,
                          dob
                        );
                        if (age < entryAge.ominAge) {
                          errorMsg = EABi18n({
                            textStore,
                            language,
                            path: "quotation.error.backdateOwnerMinAge"
                          });
                        }
                      }
                      if (entryAge.iminAge) {
                        const dob = new Date(quotation.iDob);
                        const age = getAgeByUnit(
                          entryAge.iminAgeUnit,
                          value,
                          dob
                        );
                        if (age < entryAge.iminAge) {
                          errorMsg = EABi18n({
                            textStore,
                            language,
                            path: "quotation.error.backdateInsuredMinAge"
                          });
                        }
                      }
                    }
                  }

                  if (errorMsg) {
                    Alert.alert(
                      EABi18n({
                        textStore,
                        language,
                        path: "alert.warning"
                      }),
                      errorMsg,
                      [
                        {
                          text: EABi18n({
                            language,
                            textStore,
                            path: "button.ok"
                          }),
                          onPress: () => {}
                        }
                      ]
                    );
                  } else {
                    quote({
                      ...quotation,
                      riskCommenDate: moment(value).format(DATE_FORMAT_INPUT)
                    });
                  }
                },
                date: new Date(quotation.riskCommenDate) || new Date(),
                mode: "date",
                maximumDate: new Date(),
                minimumDate: new Date(
                  moment()
                    .add(-6, "month")
                    .format("YYYY-MM-DD")
                )
              }}
            />
          ) : null}
          <Selector
            testID="Quotation__Non-Shield__ddlCurrency"
            style={styles.configField}
            selectorType={FLAT_LIST}
            hasLabel
            label={EABi18n({
              language,
              textStore,
              path: "quotation.config.currency"
            })}
            flatListOptions={{
              data: currencyOptions,
              renderItemOnPress: itemValue => {
                const newQuotation = {
                  ...this.props.quotation,
                  ccy: itemValue
                };

                quote(newQuotation);
              },
              selectedValue: quotation.ccy
            }}
          />
          {getMandatoryPolicyOptions.map(policyOption => {
            switch (policyOption.type) {
              case "picker": {
                const options = [];
                policyOption.options.forEach(option => {
                  options.push({
                    label: getEABTextByLangType({
                      data: option.title,
                      language
                    }),
                    key: option.value
                  });
                });

                /* get label */
                let value = "";
                const optionValue = quotation.policyOptions[policyOption.id];
                if (options.length) {
                  if (optionValue) {
                    if (
                      _.isEqual(policyOption.id, "paymentMethod") &&
                      ["cpfisoa", "cpfissa"].includes(optionValue)
                    ) {
                      if (!_.isEqual(this.state.paymentMethod, optionValue)) {
                        this.setState({
                          paymentMethod: optionValue,
                          openWarning: true
                        });
                      }
                    }
                    if (
                      options.findIndex(
                        option => option.key === optionValue
                      ) !== -1
                    ) {
                      value = optionValue;
                    } else {
                      // if has value but can not match any option, reset it
                      const newQuotation = _.cloneDeep(quotation);
                      newQuotation.policyOptions[policyOption.id] = null;

                      quote(newQuotation);
                    }
                  } else {
                    value = EABi18n({
                      textStore,
                      language,
                      path: "button.select"
                    });
                  }
                } else if (["extraMortality"].indexOf(policyOption.id) > -1) {
                  value = `${policyOption.value}`;
                } else {
                  value = "-";
                }

                // get error message
                let hasError = false;
                let errorMessage = "";
                if (
                  quotationErrors &&
                  quotationErrors.mandatoryErrors &&
                  quotationErrors.mandatoryErrors[policyOption.id]
                ) {
                  hasError = true;
                  errorMessage = EABi18n({
                    textStore,
                    language,
                    path: "error.302"
                  });
                }

                return (
                  <Selector
                    testID="Quotation__Non-Shield__ddlPolicyOption"
                    key={policyOption.id}
                    style={styles.configField}
                    selectorType={FLAT_LIST}
                    hasLabel
                    label={getEABTextByLangType({
                      data: policyOption.title,
                      language
                    })}
                    flatListOptions={{
                      data: options,
                      renderItemOnPress: (label, option) => {
                        if (policyOption.id === "estFee") {
                          Alert.alert(
                            EABi18n({
                              textStore,
                              language,
                              path: "alert.warning"
                            }),
                            EABi18n({
                              textStore,
                              language,
                              path: "quotation.alert.establishmentFeeChange"
                            }),
                            [
                              {
                                text: EABi18n({
                                  textStore,
                                  language,
                                  path: "button.ok"
                                }),
                                onPress: () => {}
                              }
                            ],
                            { cancelable: false }
                          );
                        }

                        this.productSpecialHandle({
                          language,
                          type: "policyOption",
                          id: policyOption.id,
                          value: option.key,
                          policyOption
                        });

                        policyOptionOnChange({
                          quotation,
                          quote,
                          resetQuot,
                          textStore,
                          language,
                          policyOption,
                          newValue: option.key
                        });
                      },
                      selectedValue: value
                    }}
                    disable={policyOption.disable === "Y"}
                    hasError={hasError}
                    hintText={errorMessage}
                  />
                );
              }
              case "header":
                return (
                  <View
                    key={policyOption.id}
                    style={styles.configFieldFullWidth}
                  >
                    <Text style={Theme.subheadSecondary}>
                      {policyOption.note}
                    </Text>
                  </View>
                );
              case "linebreak":
                return <View style={styles.configFieldFullWidth} />;
              case "note":
                noteGroup.push({
                  key: policyOption.id,
                  message: policyOption.note
                });
                return null;
              case "popupMsg":
                return null;
              case "termandcondition":
                return _.get(
                  this.props,
                  "quotation.policyOptions.hasBoughtAxaPlan"
                ) === "Y" ? (
                  <TermAndCondition
                    testID="Quotation__Non-Shield__TermAndCondition"
                    inputConfigs={inputConfigs}
                    quotation={quotation}
                    quote={quote}
                    policyOption={policyOption}
                    textStore={textStore}
                    language={language}
                  />
                ) : null;

              case "text": {
                // get error message
                let isError = false;
                let errorMessage = "";
                if (
                  quotationErrors &&
                  quotationErrors.mandatoryErrors &&
                  quotationErrors.mandatoryErrors[policyOption.id]
                ) {
                  isError = true;
                  errorMessage = EABi18n({
                    textStore,
                    language,
                    path: "error.302"
                  });
                }
                return (
                  <EABCard
                    isPlainText
                    isSelected={false}
                    style={
                      isError
                        ? [
                            styles.configField,
                            {
                              minHeight: 55,
                              borderWidth: 1,
                              borderColor: "red"
                            }
                          ]
                        : styles.configField
                    }
                    contentStyle={
                      policyOption.disable !== "Y"
                        ? [
                            styles.checkBoxField,
                            { paddingTop: Theme.alignmentXL }
                          ]
                        : styles.checkBoxField
                    }
                  >
                    <View style={styles.textFieldWrapper}>
                      <View>
                        <Text
                          style={
                            isError
                              ? [
                                  styles.textFieldTitle,
                                  { paddingTop: 15, minHeight: 75 }
                                ]
                              : styles.textFieldTitle
                          }
                        >
                          {getEABTextByLangType({
                            data: policyOption.title,
                            language
                          })}
                        </Text>
                      </View>
                      <View>
                        {policyOption.disable !== "Y" ? (
                          <EABTextField
                            inputStyle={
                              isError
                                ? {
                                    textAlign: "right",
                                    paddingTop: 15,
                                    minHeight: 30
                                  }
                                : { textAlign: "right" }
                            }
                            supportLabelPlaceholder={false}
                            value={
                              quotation.policyOptions[policyOption.id] || "0"
                            }
                            onFocus={() => {}}
                            onChange={value => {
                              const newPolicyOptions = _.cloneDeep(
                                quotation.policyOptions
                              );
                              value = Number(value.replace(/-/g, ""));
                              if (
                                value > policyOption.max &&
                                _.isNumber(policyOption.max)
                              ) {
                                value = policyOption.max;
                              } else if (
                                value < policyOption.min &&
                                _.isNumber(policyOption.min)
                              ) {
                                value = policyOption.min;
                              }
                              newPolicyOptions[
                                policyOption.id
                              ] = numberToCurrency({
                                value,
                                decimal: _.isEmpty(
                                  policyOption.textDecimalPlace
                                )
                                  ? 0
                                  : policyOption.textDecimalPlace,
                                maxValue: _.isEmpty(policyOption.max)
                                  ? 99999999
                                  : policyOption.max
                              });

                              updateQuotation({
                                ...quotation,
                                policyOptions: newPolicyOptions
                              });
                              quote({
                                ...quotation,
                                policyOptions: {
                                  ...quotation.policyOptions,
                                  [policyOption.id]: Number(
                                    numberToCurrency({
                                      value: value.toString().replace(/,/g, ""),
                                      decimal:
                                        policyOption.textDecimalPlace === ""
                                          ? 0
                                          : Number(
                                              policyOption.textDecimalPlace
                                            )
                                    })
                                  )
                                }
                              });
                            }}
                            onBlur={() => {}}
                            maxLength={policyOption.maxLength}
                            isError={isError}
                            hintText={errorMessage}
                            keyboardType="numeric"
                          />
                        ) : (
                          <Text style={styles.textFieldValue}>
                            {_.isEmpty(
                              _.get(
                                this.props.quotation.policyOptions,
                                policyOption.id
                              )
                            )
                              ? policyOption.value || 0
                              : this.props.quotation.policyOptions[
                                  policyOption.id
                                ]}
                          </Text>
                        )}
                      </View>
                    </View>
                  </EABCard>
                );
              }
              default:
                return (
                  <EABSearchField
                    testID="Quotation__Non-Shield__PolicyOption"
                    key={policyOption.id}
                    style={styles.configFieldWithHeight}
                    value={quotation.policyOptions[policyOption.id]}
                    title={EABi18n({
                      language,
                      textStore,
                      path: "quotation.table.occupationClass"
                    })}
                    placeholder={getEABTextByLangType({
                      data: policyOption.title,
                      language
                    })}
                    onChange={value => {
                      updateQuotation({
                        ...quotation,
                        policyOptions: {
                          ...quotation.policyOptions,
                          [policyOption.id]: value
                        }
                      });
                    }}
                    onBlur={() => {
                      quote(quotation);
                    }}
                    editable={policyOption.disable !== "Y"}
                  />
                );
            }
          })}
          {noteGroup.map(note => (
            <View key={note.key} style={styles.configFieldFullWidth}>
              <Text style={Theme.subheadSecondary}>{note.message}</Text>
            </View>
          ))}
          {quotWarnings.map(warning => (
            <View
              key={`warning-${warning.length}`}
              style={styles.warningFullBoxWidth}
            >
              <Text style={styles.warningText}>{warning}</Text>
            </View>
          ))}
        </View>
      );
    };
    /**
     * paymentModeRow
     * @description generate payment mode chips
     * @requires onChangePayMode
     * @return {array} payment mode chips
     * */
    const paymentModeRow = () => {
      const inputConfig = inputConfigs[quotation.baseProductCode];
      const returnArray = [];

      if (inputConfig) {
        inputConfig.payModes.forEach((payMode, index) => {
          const getTitle = language => {
            let title = "quotation.payMode.";

            switch (payMode.mode) {
              case "M":
                title += "monthly";
                break;
              case "Q":
                title += "quarterly";
                break;
              case "S":
                title += "semiAnnually";
                break;
              case "A":
                title += "annually";
                break;
              case "L":
                title += "singlePremium";
                break;
              default:
                throw Error(`unknown payment mode ${payMode.mode}`);
            }

            return EABi18n({ language, textStore, path: title }).toUpperCase();
          };

          returnArray.push(
            <ContextConsumer key={payMode.mode}>
              {({ language }) => (
                <Chip
                  testID={`Quotation__Non-Shield__btn${getTitle(language)}`}
                  containerStyle={
                    index === 0
                      ? styles.paymentModeChipFirst
                      : styles.paymentModeChip
                  }
                  title={getTitle(language)}
                  onPress={() => {
                    onChangePayMode({ value: payMode.mode, language });
                  }}
                  isSelected={payMode.mode === quotation.paymentMode}
                />
              )}
            </ContextConsumer>
          );
        });
      }

      return returnArray;
    };
    /**
     * remarks
     * @param {string} language - UI language type
     * @return {array|element} remark element
     * */
    const remarks = language => {
      const basicPlan = planDetails[quotation.baseProductCode];

      if (basicPlan && basicPlan.remarks) {
        const remark = getEABTextByLangType({
          data: basicPlan.remarks,
          language
        });

        // some products have useless remark. length rule is use to filter
        // these useless remarks
        if (remark.length < 5) {
          return null;
        }
        if (remark.indexOf("%%%") === -1) {
          return (
            <View style={styles.remarks}>
              <Text style={Theme.subheadSecondary}>{remark}</Text>
            </View>
          );
        }
        const remarkArray = remark.split("%%%");
        const remarkA = remarkArray[0];
        const remarkB = remarkArray[1];
        return [
          <View key="A" style={styles.remarks}>
            <Text style={Theme.subheadSecondary}>{remarkA}</Text>
          </View>,
          <View key="B" style={styles.remarks}>
            <Text style={Theme.subheadSecondary}>{remarkB}</Text>
          </View>
        ];
      }
      return null;
    };
    /**
     * @name renderTableSection
     * @requires paymentModeRow
     * @requires getRiderButton
     * @requires remarks
     * @return {element} table section UI
     * */
    const renderTableSection = language => (
      <View style={styles.tableWrapper}>
        <View style={styles.paymentMode}>
          <Text style={styles.paymentModeTitle}>
            {EABi18n({
              language,
              textStore,
              path: "quotation.payMode.title"
            }).toUpperCase()}:
          </Text>
          {paymentModeRow()}
        </View>
        <Table
          style={styles.table}
          tableConfig={getTableConfig(language).tableConfig}
        >
          {getTableConfig(language).header}
          {getTableConfig(language).basicPlanHeader}
          {getTableConfig(language).basicPlan}
          {getTableConfig(language).riderHeader}
          {getTableConfig(language).riders}
        </Table>
        {remarks(language)}
        {getRiderButton}
      </View>
    );
    /**
     * @name renderFundSection
     * @requires isQuotationHaveFunds
     * @return {element} quotation fund section
     * */
    const renderFundSection = language =>
      isQuotationHaveFunds ? (
        <View key="Fund Section" style={styles.fundWrapper}>
          <View style={styles.fundTitleWrapper}>
            <Text style={Theme.captionBrand}>
              {EABi18n({
                language,
                textStore,
                path: "quotation.fund.title"
              }).toUpperCase()}
            </Text>
          </View>
          {riskProfile ? (
            <View style={styles.riskProfileInfo}>
              <Text style={Theme.subheadPrimary}>
                {`${EABi18n({
                  textStore,
                  language,
                  path: "quotation.hint.assessedRiskProfile"
                })}: ${EABi18n({
                  textStore,
                  language,
                  path: `quotation.hint.riskProfile.${riskProfile}`
                })}`}
              </Text>
            </View>
          ) : null}
          <View style={styles.fundOptionWrapper}>
            <Selector
              style={styles.configFieldFirst}
              selectorType={FLAT_LIST}
              hasLabel
              label={EABi18n({
                language,
                textStore,
                path: "quotation.config.investmentOption"
              })}
              flatListOptions={{
                data: [
                  {
                    key: "mixedAssets",
                    label: EABi18n({
                      language,
                      textStore,
                      path: "quotation.options.mixedAssets"
                    }),
                    value: "mixedAssets"
                  },
                  ...(riskProfile
                    ? [
                        {
                          key: "buildPortfolio",
                          label: EABi18n({
                            language,
                            textStore,
                            path: "quotation.options.buildPortfolio"
                          }),
                          value: "buildPortfolio"
                        }
                      ]
                    : []),
                  {
                    key: "optimizeToProfile",
                    label: EABi18n({
                      language,
                      textStore,
                      path: "quotation.options.optimizeToProfile"
                    }),
                    value: "optimizeToProfile"
                  }
                ],
                renderItemOnPress: (label, option) => {
                  changeInvOpt({ language, option });
                },
                selectedValue: invOpt
              }}
            />
            {isInvOptBuildPortfolio ? (
              <Selector
                style={styles.configFieldFirst}
                selectorType={FLAT_LIST}
                hasLabel
                label={EABi18n({
                  language,
                  textStore,
                  path: "quotation.config.portfolio"
                })}
                popoverOptions={{
                  preferredContentSize: [750, 200]
                }}
                flatListOptions={{
                  data: portfolioOptions(language),
                  renderItemOnPress: (label, option) => {
                    if (_.isEmpty(availableFunds)) {
                      this.props.getFunds({
                        productID: quotation.baseProductId,
                        invOpt,
                        paymentMethod:
                          quotation.policyOptions &&
                          quotation.policyOptions.paymentMethod
                      });
                    }
                    if (
                      !_.isEmpty(fundAllocs) ||
                      ((hasTopUpAlloc && !_.isEmpty(topUpAllocs)) ||
                        isPortfolioAdjusted)
                    ) {
                      Alert.alert(
                        EABi18n({
                          textStore,
                          language,
                          path: "alert.confirm"
                        }),
                        EABi18n({
                          textStore,
                          language,
                          path: "quotation.warning.confirmCleanFund"
                        }),
                        [
                          {
                            text: EABi18n({
                              textStore,
                              language,
                              path: "button.no"
                            }),
                            onPress: () => {}
                          },
                          {
                            text: EABi18n({
                              textStore,
                              language,
                              path: "button.yes"
                            }),
                            onPress: () => {
                              this.setState(
                                {
                                  searchRisk: "*",
                                  portfolio: option.value,
                                  portfolioAllocsBackUp: {},
                                  portfolioAllocsTemp: {}
                                },
                                () => {
                                  quote({
                                    ...quotation,
                                    fund: null
                                  });
                                }
                              );
                            }
                          }
                        ]
                      );
                    } else {
                      this.setState({
                        portfolio: option.value,
                        portfolioAllocsBackUp: {},
                        portfolioAllocsTemp: {}
                      });
                    }
                  },
                  selectedValue: portfolio
                }}
                hasError={portfolio === null}
                hintText={
                  portfolio === null
                    ? EABi18n({
                        textStore,
                        language,
                        path: "error.302"
                      })
                    : ""
                }
              />
            ) : null}
          </View>
          <View style={styles.fundDetail}>
            {availableFunds !== [] && quotation.fund !== null ? (
              <Table
                style={styles.fundTable}
                tableConfig={[
                  {
                    key: "fund name",
                    style: styles.fundDialogField
                  },
                  {
                    key: "currency",
                    style: styles.fundDialogField
                  },
                  {
                    key: "asset class",
                    style: styles.fundDialogField
                  },
                  {
                    key: "risk rating",
                    style: styles.fundDialogField
                  },
                  {
                    key: "premium allocation",
                    style: styles.fundDialogField
                  },
                  ...(hasTopUpAlloc
                    ? [
                        {
                          key: "top up allocation",
                          style: styles.fundDialogField
                        }
                      ]
                    : []),
                  {
                    key: "cancel Button",
                    style: styles.cancelButton
                  }
                ]}
              >
                <TableHeader key="title" style={styles.fundTableBackground}>
                  <View key="fundName">
                    <Text style={Theme.tableColumnLabel}>
                      {EABi18n({
                        language,
                        textStore,
                        path: "quotation.fund.fundList"
                      })}
                    </Text>
                  </View>
                  <View key="currency">
                    <Text style={Theme.tableColumnLabel}>
                      {EABi18n({
                        language,
                        textStore,
                        path: "quotation.fund.currency"
                      })}
                    </Text>
                  </View>
                  <View key="assetClass">
                    <Text style={Theme.tableColumnLabel}>
                      {EABi18n({
                        language,
                        textStore,
                        path: "quotation.fund.assetClass"
                      })}
                    </Text>
                  </View>
                  <View key="riskRating">
                    <Text style={Theme.tableColumnLabel}>
                      {EABi18n({
                        language,
                        textStore,
                        path: "quotation.fund.riskRating"
                      })}
                    </Text>
                  </View>
                  <View key="premium allocation">
                    <Text style={Theme.tableColumnLabel}>
                      {EABi18n({
                        language,
                        textStore,
                        path: "quotation.fund.premiumAllocation"
                      })}
                    </Text>
                  </View>
                  {hasTopUpAlloc ? (
                    <View key="top up allocation">
                      <Text style={Theme.tableColumnLabel}>
                        {EABi18n({
                          language,
                          textStore,
                          path: "quotation.fund.topUpAllocation"
                        })}
                      </Text>
                    </View>
                  ) : null}
                  <View />
                </TableHeader>
                {getFundTableRows.map(row => (
                  <EABTableSection
                    key={row.key}
                    style={styles.fundTableBackground}
                  >
                    <View>
                      <Text style={Theme.subheadPrimary}>
                        {getEABTextByLangType({
                          data: row.fundName,
                          language
                        })}
                      </Text>
                    </View>
                    <View>
                      <Text style={Theme.subheadPrimary}>{row.ccy}</Text>
                    </View>
                    <View>
                      <Text style={Theme.subheadPrimary}>
                        {getOptionTitle({
                          value: row.assetClass,
                          optionsMap: optionsMap.assetClass,
                          language: languageTypeConvert(language)
                        })}
                      </Text>
                    </View>
                    <View>
                      <Text style={Theme.subheadPrimary}>
                        {getOptionTitle({
                          value: row.riskRating,
                          optionsMap: optionsMap.riskRating,
                          language: languageTypeConvert(language)
                        })}
                      </Text>
                    </View>
                    <View>
                      <Text style={Theme.subheadPrimary}>
                        {`${row.alloc}%`}
                      </Text>
                    </View>
                    {hasTopUpAlloc ? (
                      <View>
                        <Text style={Theme.subheadPrimary}>
                          {row.topUpAlloc ? `${row.topUpAlloc}%` : "0%"}
                        </Text>
                      </View>
                    ) : null}
                    <View>
                      <Button
                        onPress={() => {
                          row.removeFunction(language);
                        }}
                      >
                        <Image source={icRemove} />
                      </Button>
                    </View>
                  </EABTableSection>
                ))}
              </Table>
            ) : (
              <Text style={Theme.bodyNegative}>
                {EABi18n({
                  textStore,
                  language,
                  path: "quotation.error.pleaseSelectFund"
                })}
              </Text>
            )}
          </View>
          <View style={styles.buttonRow}>
            <View style={styles.padRightButton}>
              <Button
                buttonType={RECTANGLE_BUTTON_1}
                onPress={openFundsDialogCallBack}
              >
                <Text style={Theme.rectangleButtonStyle1Label}>
                  {EABi18n({
                    language,
                    textStore,
                    path: "quotation.button.selectFunds"
                  })}
                </Text>
              </Button>
            </View>
            {isPortfolioSelected && this.portfolioAllocs.groups.length > 1 ? (
              <View style={styles.buttonRow}>
                <View style={styles.padRightButton}>
                  <Button
                    buttonType={RECTANGLE_BUTTON_1}
                    onPress={() => {
                      if (
                        !_.isEmpty(fundAllocs) ||
                        (hasTopUpAlloc && !_.isEmpty(topUpAllocs))
                      ) {
                        Alert.alert(
                          EABi18n({
                            textStore,
                            language,
                            path: "alert.confirm"
                          }),
                          EABi18n({
                            textStore,
                            language,
                            path: isInvOptBuildPortfolio
                              ? "quotation.warning.confirmCleanFund"
                              : "quotation.warning.removeFund"
                          }),
                          [
                            {
                              text: EABi18n({
                                language,
                                textStore,
                                path: "button.no"
                              }),
                              onPress: () => {}
                            },
                            {
                              text: EABi18n({
                                language,
                                textStore,
                                path: "button.yes"
                              }),
                              onPress: () => {
                                this.setState({
                                  searchRisk: "*",
                                  selectedFunds: {},
                                  fundAllocsBackUp: {},
                                  topUpAllocsBackUp: {}
                                });
                                quote({
                                  ...quotation,
                                  fund: null
                                });
                              }
                            }
                          ]
                        );
                      } else {
                        openPortfolioDialogCallBack();
                      }
                    }}
                  >
                    <Text style={Theme.rectangleButtonStyle1Label}>
                      {EABi18n({
                        language,
                        textStore,
                        path: "quotation.portfolio.title"
                      })}
                    </Text>
                  </Button>
                </View>
                {isPortfolioAdjusted ? (
                  <Button
                    buttonType={RECTANGLE_BUTTON_1}
                    onPress={() => {
                      Alert.alert(
                        EABi18n({
                          textStore,
                          language,
                          path: "alert.confirm"
                        }),
                        EABi18n({
                          textStore,
                          language,
                          path: isInvOptBuildPortfolio
                            ? "quotation.warning.confirmCleanFund"
                            : "quotation.warning.removeFund"
                        }),
                        [
                          {
                            text: EABi18n({
                              language,
                              textStore,
                              path: "button.no"
                            }),
                            onPress: () => {}
                          },
                          {
                            text: EABi18n({
                              language,
                              textStore,
                              path: "button.yes"
                            }),
                            onPress: () => {
                              this.setState({
                                portfolioAllocsBackUp: {},
                                selectedFunds: {},
                                fundAllocsBackUp: {},
                                topUpAllocsBackUp: {}
                              });
                              quote({
                                ...quotation,
                                fund: null
                              });
                            }
                          }
                        ]
                      );
                    }}
                  >
                    <Text style={Theme.rectangleButtonStyle1Label}>
                      {EABi18n({
                        language,
                        textStore,
                        path: "quotation.portfolio.reset"
                      })}
                    </Text>
                  </Button>
                ) : null}
              </View>
            ) : null}
          </View>
          {isPortfolioAdjusted ? (
            <View style={styles.adjustedWarning}>
              <Text style={Theme.bodyNegative}>
                {EABi18n({
                  language,
                  textStore,
                  path: "quotation.portfolio.adjusted.1"
                })}
              </Text>
              <Text style={Theme.bodyNegative}>
                {EABi18n({
                  language,
                  textStore,
                  path: "quotation.portfolio.adjusted.2"
                })}
                {
                  _.find(portfolioOptions(language), p =>
                    _.isEqual(p.key, portfolio)
                  ).label
                }
              </Text>
            </View>
          ) : null}
          {this.riskProfile ? (
            <Text style={styles.riskNote}>
              {EABi18n({
                path: "quotation.hint.riskNote",
                textStore,
                language
              })}
            </Text>
          ) : null}
          <Dialog isOpen={openDisclaimer}>
            <View style={styles.disclaimerDialogToolBar}>
              <Button
                onPress={() => {
                  this.setState({ openDisclaimer: false });
                }}
              >
                <Text style={Theme.textButtonLabelNormalAccent}>
                  {EABi18n({
                    language,
                    textStore,
                    path: "button.cancel"
                  })}
                </Text>
              </Button>
              <Button
                onPress={() => {
                  selectInvOpt({
                    invOptData: "buildPortfolio",
                    language,
                    shouldCloseDisclaimer: true
                  });
                }}
              >
                <Text style={Theme.textButtonLabelNormalEmphasizedAccent}>
                  {EABi18n({
                    language,
                    textStore,
                    path: "button.ok"
                  })}
                </Text>
              </Button>
            </View>
            <Table
              tableConfig={[
                {
                  key: "model",
                  style: {
                    flex: 1,
                    justifyContent: "center",
                    alignItems: "center"
                  }
                },
                {
                  key: "time",
                  style: {
                    flex: 1,
                    justifyContent: "center",
                    alignItems: "center"
                  }
                }
              ]}
              style={styles.fundDialogHeader}
            >
              <TableHeader key="header">
                <View>
                  <Text style={Theme.bodyBrand}>
                    {EABi18n({
                      textStore,
                      language,
                      path: "quotation.hint.riskProfileModel"
                    })}
                  </Text>
                </View>
                <View>
                  <Text style={Theme.bodyBrand}>
                    {EABi18n({
                      textStore,
                      language,
                      path: "quotation.hint.investmentTime"
                    })}
                  </Text>
                </View>
              </TableHeader>
              <EABTableSection key="a">
                <View>
                  <Text style={Theme.bodyPrimary}>
                    {EABi18n({
                      textStore,
                      language,
                      path: "quotation.hint.conservative"
                    })}
                  </Text>
                </View>
                <View>
                  <TranslatedText
                    style={Theme.subheadPrimary}
                    path="quotation.hint.disclaimerYears"
                    replace={[{ value: "3", isPath: false }]}
                  />
                </View>
              </EABTableSection>
              <EABTableSection key="b">
                <View>
                  <Text style={Theme.bodyPrimary}>
                    {EABi18n({
                      textStore,
                      language,
                      path: "quotation.hint.moderatelyConservative"
                    })}
                  </Text>
                </View>
                <View>
                  <TranslatedText
                    style={Theme.subheadPrimary}
                    path="quotation.hint.disclaimerYears"
                    replace={[{ value: "5", isPath: false }]}
                  />
                </View>
              </EABTableSection>
              <EABTableSection key="c">
                <View>
                  <Text style={Theme.bodyPrimary}>
                    {EABi18n({
                      textStore,
                      language,
                      path: "quotation.hint.balanced"
                    })}
                  </Text>
                </View>
                <View>
                  <TranslatedText
                    style={Theme.subheadPrimary}
                    path="quotation.hint.disclaimerYears"
                    replace={[{ value: "7", isPath: false }]}
                  />
                </View>
              </EABTableSection>
              <EABTableSection key="d">
                <View>
                  <Text style={Theme.bodyPrimary}>
                    {EABi18n({
                      textStore,
                      language,
                      path: "quotation.hint.moderatelyAggressive"
                    })}
                  </Text>
                </View>
                <View>
                  <TranslatedText
                    style={Theme.subheadPrimary}
                    path="quotation.hint.disclaimerYears"
                    replace={[{ value: "10", isPath: false }]}
                  />
                </View>
              </EABTableSection>
            </Table>
            <Text style={styles.disclaimerDialogFooter}>
              {EABi18n({
                textStore,
                language,
                path: "quotation.hint.disclaimerHint"
              })}
            </Text>
          </Dialog>
        </View>
      ) : null;
    /**
     * @name renderFundsDialog
     * @requires isQuotationHaveFunds
     * @return {array} dialog tool bar and funds table
     * */
    const renderFundsDialog = language => {
      const groups = [];
      let isPortfolioInvalid = false;

      if (isInvOptBuildPortfolio) {
        if (isPortfolioAdjusted) {
          isPortfolioInvalid = !!_.find(portfolioAllocsBackUp.groups, group => {
            const funds = _.filter(
              getSelectedFunds,
              f => group.riskRatings.indexOf(f.riskRating) > -1
            );
            const groupTotal = _.reduce(
              funds,
              (sum, fund) => sum + (fundAllocsBackUp[fund.fundCode] || 0),
              0
            );
            const groupTopUpTotal = _.reduce(
              funds,
              (sum, fund) => sum + (topUpAllocsBackUp[fund.fundCode] || 0),
              0
            );
            return (
              groupTotal !== group.percentageNew ||
              (hasTopUpAlloc && groupTopUpTotal !== group.percentageNew)
            );
          });
        } else if (getCurrentPortfolioModel) {
          isPortfolioInvalid = !!_.find(
            getCurrentPortfolioModel.groups,
            group => {
              const funds = _.filter(
                getSelectedFunds,
                f => group.riskRatings.indexOf(f.riskRating) > -1
              );
              const groupTotal = _.reduce(
                funds,
                (sum, fund) => sum + (fundAllocsBackUp[fund.fundCode] || 0),
                0
              );
              const groupTopUpTotal = _.reduce(
                funds,
                (sum, fund) => sum + (topUpAllocsBackUp[fund.fundCode] || 0),
                0
              );
              return (
                groupTotal !== group.percentage ||
                (hasTopUpAlloc && groupTopUpTotal !== group.percentage)
              );
            }
          );
        } else {
          isPortfolioInvalid = true;
        }

        // get portfolio group information
        if (isPortfolioAdjusted) {
          portfolioAllocsBackUp.groups.forEach(group => {
            const groupTotal = _.reduce(
              _.filter(
                getSelectedFunds,
                f => group.riskRatings.indexOf(f.riskRating) > -1
              ),
              (sum, fund) => sum + (fundAllocsBackUp[fund.fundCode] || 0),
              0
            );
            if (Number(group.percentageNew !== 0)) {
              groups.push({
                groupName: `${riskDescription({
                  isAdjusted: true,
                  language,
                  group
                })}: `,
                total: `${groupTotal}%`,
                hasError: group.percentageNew !== groupTotal
              });
            }
          });
        } else if (
          getCurrentPortfolioModel &&
          !getCurrentPortfolioModel.any &&
          getCurrentPortfolioModel.groups
        ) {
          getCurrentPortfolioModel.groups.forEach(group => {
            const groupTotal = _.reduce(
              _.filter(
                getSelectedFunds,
                f => group.riskRatings.indexOf(f.riskRating) > -1
              ),
              (sum, fund) => sum + (fundAllocsBackUp[fund.fundCode] || 0),
              0
            );

            groups.push({
              groupName: `${riskDescription({
                language,
                group
              })}: `,
              total: `${groupTotal}%`,
              hasError: group.percentage !== groupTotal
            });
          });
        }
      }

      // get button disable status
      const disableSaveRule =
        getFundAllocBackUpTotal !== 100 ||
        (hasTopUpAlloc && getTopUpAllocBackUpTotal !== 100) ||
        isPortfolioInvalid;

      // get risk rating options
      const riskRatingOptions = [
        {
          key: "all",
          label: EABi18n({
            textStore,
            language,
            path: "quotation.options.allFunds"
          }),
          value: "*"
        }
      ];
      optionsMap.riskRating.options.forEach(option => {
        if (
          // no filter rule
          (!(getCurrentPortfolioModel && filterRiskRatings) && !riskProfile) ||
          // portfolio rule
          (getCurrentPortfolioModel &&
            filterRiskRatings &&
            filterRiskRatings.indexOf(option.value) > -1) ||
          // riskProfile rule
          (!isPortfolioSelected && (riskProfile && option.value <= riskProfile))
        ) {
          riskRatingOptions.push({
            key: `${option.value}`,
            label: getEABTextByLangType({ data: option.title, language }),
            value: option.value
          });
        }
      });

      return isQuotationHaveFunds ? (
        <Dialog key="Funds Dialog" isOpen={shouldShowSelectFundsDialog}>
          <View style={styles.fundHeader}>
            <View style={styles.fundDialogToolBarRowFirst}>
              <View style={styles.toolBarSectionA}>
                <Button
                  onPress={() => {
                    this.setState({
                      shouldShowSelectFundsDialog: false,
                      selectedFunds: {},
                      fundAllocsBackUp: {},
                      topUpAllocsBackUp: {},
                      searchRisk: "*",
                      fundsSearchText: ""
                    });
                  }}
                >
                  <Text style={Theme.textButtonLabelNormalAccent}>
                    {EABi18n({
                      language,
                      textStore,
                      path: "button.cancel"
                    })}
                  </Text>
                </Button>
              </View>
              <View style={styles.toolBarSectionB}>
                <Text style={Theme.title}>
                  {EABi18n({
                    language,
                    textStore,
                    path: "quotation.button.availableFundList"
                  })}
                </Text>
              </View>
              <View style={styles.toolBarSectionC}>
                <Button
                  onPress={() => {
                    allocFunds({
                      invOpt,
                      portfolio,
                      fundAllocs: fundAllocsBackUp,
                      topUpAllocs: topUpAllocsBackUp,
                      hasTopUpAlloc,
                      adjustedModel: portfolioAllocsBackUp,
                      callback: () => {
                        this.setState({ shouldShowSelectFundsDialog: false });
                      }
                    });
                  }}
                  isDisable={disableSaveRule}
                >
                  <Text style={Theme.textButtonLabelNormalEmphasizedAccent}>
                    {EABi18n({
                      language,
                      textStore,
                      path: "button.save"
                    })}
                  </Text>
                </Button>
              </View>
            </View>
            <EABSearchBar
              style={styles.fundDialogToolBarRow}
              placeholder={EABi18n({
                textStore,
                language,
                path: "quotation.fund.searchPlaceholder"
              })}
              value={fundsSearchText}
              onChange={value => {
                this.setState({
                  fundsSearchText: value
                });
              }}
            />
            <View style={styles.fundSearchOptionsRow}>
              <View style={styles.fundPlaceholder}>
                {riskProfile ? (
                  <Text style={Theme.subheadPrimary}>
                    {`${EABi18n({
                      textStore,
                      language,
                      path: "quotation.hint.assessedRiskProfile"
                    })}: ${EABi18n({
                      textStore,
                      language,
                      path: `quotation.hint.riskProfile.${riskProfile}`
                    })}`}
                  </Text>
                ) : null}
              </View>
              <Selector
                selectorType={FLAT_LIST}
                hasCard={false}
                selectorTitle={EABi18n({
                  textStore,
                  language,
                  path: "quotation.config.riskRating"
                })}
                popoverOptions={{
                  preferredContentSize: [300, 200]
                }}
                flatListOptions={{
                  renderItemOnPress: (label, option) => {
                    this.setState({
                      searchRisk: option.value
                    });
                  },
                  selectedValue: searchRisk,
                  data: riskRatingOptions
                }}
              />
            </View>
          </View>
          <Table
            tableConfig={getSelectFundsTable(language).tableConfig}
            style={styles.fundDialogHeader}
          >
            <TableHeader key="funds table header">
              {getSelectFundsTable(language).tableHeader}
            </TableHeader>
          </Table>
          <ScrollView style={styles.fundDialogScrollView}>
            <Table tableConfig={getSelectFundsTable(language).tableConfig}>
              {getSelectFundsTable(language).tableRows}
            </Table>
          </ScrollView>
          <Table
            tableConfig={[{ key: "footer", style: styles.footerColumn }]}
            style={styles.fundDialogFooter}
          >
            <EABTableFooter key="funds table footer">
              <View>
                <View style={styles.noticeMessageRow}>
                  {isInvOptBuildPortfolio &&
                    groups.map((group, index) => (
                      <View>
                        <Text style={Theme.headerBrand}>
                          {group.groupName}
                          <Text
                            style={
                              group.hasError
                                ? Theme.headerNegative
                                : Theme.headerBrand
                            }
                          >
                            {`${group.total}${
                              index !== groups.length - 1 ? ", " : ""
                            }`}
                          </Text>
                        </Text>
                      </View>
                    ))}
                </View>
                <View style={styles.noticeMessageRow}>
                  <View style={styles.noticeMessage}>
                    <Text
                      style={
                        getFundAllocBackUpTotal !== 100
                          ? Theme.headerNegative
                          : Theme.headerBrand
                      }
                    >
                      {`${getFundAllocBackUpTotal}% ${EABi18n({
                        textStore,
                        language,
                        path: "quotation.fund.allocated"
                      }).toUpperCase()}`}
                    </Text>
                  </View>
                  {hasTopUpAlloc ? (
                    <View style={styles.noticeMessage}>
                      <Text
                        style={
                          getTopUpAllocBackUpTotal !== 100
                            ? Theme.headerNegative
                            : Theme.headerBrand
                        }
                      >
                        {`${getTopUpAllocBackUpTotal}% ${EABi18n({
                          textStore,
                          language,
                          path: "quotation.fund.topUpAllocated"
                        }).toUpperCase()}`}
                      </Text>
                    </View>
                  ) : null}
                </View>
                {disableSaveRule ? (
                  <View>
                    <Text
                      style={
                        getFundAllocBackUpTotal !== 100
                          ? Theme.headerNegative
                          : Theme.headerBrand
                      }
                    >
                      {`${EABi18n({
                        textStore,
                        language,
                        path: "quotation.fund.allocatedError"
                      })}`}
                    </Text>
                  </View>
                ) : null}
                {this.riskProfile ? (
                  <Text style={styles.riskNote}>
                    {EABi18n({
                      path: "quotation.hint.riskNote",
                      textStore,
                      language
                    })}
                  </Text>
                ) : null}
              </View>
            </EABTableFooter>
          </Table>
        </Dialog>
      ) : null;
    };
    /**
     * @name renderPortfolioDialog
     * @requires isPortfolioSelected
     * @return {array} dialog toolbar and fund allocation table
     */
    const renderPortfolioDialog = language => {
      const disableSaveRule = getPortfolioAllocTotal !== 100;

      return !_.isEmpty(portfolioAllocsBackUp) ? (
        <Dialog key="Portfolio Dialog" isOpen={shouldShowSelectPortfolioDialog}>
          <View style={styles.disclaimerDialogToolBar}>
            <View style={styles.toolBarSectionA}>
              <Button
                onPress={() => {
                  this.setState({
                    shouldShowSelectPortfolioDialog: false,
                    portfolioAllocsBackUp:
                      isPortfolioAdjusted && getPortfolioAllocTotal === 100
                        ? portfolioAllocsBackUp
                        : {}
                  });
                }}
              >
                <Text style={Theme.textButtonLabelNormalAccent}>
                  {EABi18n({
                    language,
                    textStore,
                    path: "button.cancel"
                  })}
                </Text>
              </Button>
            </View>
            <View style={styles.toolBarSectionB}>
              <Text style={Theme.title}>
                {EABi18n({
                  language,
                  textStore,
                  path: "quotation.portfolio.title"
                })}
              </Text>
            </View>
            <View style={styles.toolBarSectionC}>
              <Button
                onPress={() => {
                  if (!_.isEqual(portfolioAllocsTemp, portfolioAllocs)) {
                    this.setState({
                      shouldShowSelectPortfolioDialog: false,
                      portfolioAllocsBackUp: portfolioAllocsTemp
                    });
                  } else {
                    this.setState({ shouldShowSelectPortfolioDialog: false });
                  }
                }}
                isDisable={disableSaveRule}
              >
                <Text style={Theme.textButtonLabelNormalEmphasizedAccent}>
                  {EABi18n({
                    language,
                    textStore,
                    path: "button.save"
                  })}
                </Text>
              </Button>
            </View>
          </View>
          <Table
            tableConfig={getSelectPortfolioTable(language).tableConfig}
            style={styles.fundDialogHeader}
          >
            <TableHeader key="portfolio table header">
              {getSelectPortfolioTable(language).tableHeader}
            </TableHeader>
          </Table>
          <ScrollView style={styles.fundDialogScrollView}>
            <Table tableConfig={getSelectPortfolioTable(language).tableConfig}>
              {getSelectPortfolioTable(language).tableRows}
            </Table>
          </ScrollView>
          <Table
            tableConfig={[{ key: "footer", style: styles.footerColumn }]}
            style={styles.fundDialogFooter}
          >
            <EABTableFooter key="portfolio table footer ">
              <View>
                <View style={styles.noticeMessageRow}>
                  {disableSaveRule ? (
                    <View>
                      <Text style={Theme.headerNegative}>
                        {`${EABi18n({
                          textStore,
                          language,
                          path: "quotation.fund.allocatedError"
                        })}`}
                      </Text>
                    </View>
                  ) : null}
                </View>
              </View>
            </EABTableFooter>
          </Table>
        </Dialog>
      ) : null;
    };
    /**
     * renderBottomSheet
     * @param {string} language - UI text language type
     * @return {element} quotation bottom sheet element
     * */
    const renderBottomSheet = language => {
      const breakDownTableConfig = [
        {
          key: "covName",
          style: styles.breakDownTableConfigCovName
        },
        {
          key: "sumInsured",
          style: styles.breakDownTableConfigSumInsured
        },
        {
          key: "monthPrem",
          style: styles.breakDownTableConfigMonthPrem
        },
        {
          key: "quarterPrem",
          style: styles.breakDownTableConfigQuarterPrem
        },
        {
          key: "halfYearPrem",
          style: styles.breakDownTableConfigHalfYearPrem
        },
        {
          key: "yearPrem",
          style: styles.breakDownTableConfigYearPrem
        }
      ];

      const planInputConfig = inputConfigs[quotation.plans[0].covCode] || {};

      const premiumDecimal = planInputConfig.premInput
        ? planInputConfig.premInput.decimal
        : 2;

      return (
        <EABBottomSheet
          key="quotationBottomSheet"
          containerStyle={styles.bottomSheetContainer}
          style={styles.bottomSheetWrapper}
          expandHeight={BOTTOM_SHEET_EXPAND_HEIGHT}
          collapseHeight={BOTTOM_SHEET_COLLAPSE_HEIGHT}
          isExpand={shouldExpandBottomSheet}
        >
          <View
            style={[
              styles.bottomSheetBar,
              { height: BOTTOM_SHEET_COLLAPSE_HEIGHT }
            ]}
          >
            <View style={styles.quotationDetailWrapper}>
              {quotation.paymentMode !== "L" && (
                <View style={{ flexDirection: "row" }}>
                  <View style={styles.paymentMethodFirst}>
                    <View style={styles.paymentMethodTitleWrapper}>
                      <TranslatedText
                        style={Theme.captionBrand}
                        path="quotation.quotationDetail.monthlyTotal"
                        funcOnText={text => text.toUpperCase()}
                      />
                    </View>
                    <Text style={Theme.bodyPrimary}>
                      {quotation.totMonthPrem === 0 || !quotation.totMonthPrem
                        ? `${sign}${numberToCurrency({
                            value: 0,
                            decimal: premiumDecimal
                          })}`
                        : getCurrency({
                            value: quotation.totMonthPrem,
                            sign,
                            decimals: premiumDecimal
                          })}
                    </Text>
                  </View>
                  <View style={styles.paymentMethod}>
                    <View style={styles.paymentMethodTitleWrapper}>
                      <TranslatedText
                        style={Theme.captionBrand}
                        path="quotation.quotationDetail.quarterlyTotal"
                        funcOnText={text => text.toUpperCase()}
                      />
                    </View>
                    <Text style={Theme.bodyPrimary}>
                      {quotation.totQuarterPrem === 0 ||
                      !quotation.totQuarterPrem
                        ? `${sign}${numberToCurrency({
                            value: 0,
                            decimal: premiumDecimal
                          })}`
                        : getCurrency({
                            value: quotation.totQuarterPrem,
                            sign,
                            decimals: premiumDecimal
                          })}
                    </Text>
                  </View>
                  <View style={styles.paymentMethod}>
                    <View style={styles.paymentMethodTitleWrapper}>
                      <TranslatedText
                        style={Theme.captionBrand}
                        path="quotation.quotationDetail.semiAnnualTotal"
                        funcOnText={text => text.toUpperCase()}
                      />
                    </View>
                    <Text style={Theme.bodyPrimary}>
                      {quotation.totHalfyearPrem === 0 ||
                      !quotation.totHalfyearPrem
                        ? `${sign}${numberToCurrency({
                            value: 0,
                            decimal: premiumDecimal
                          })}`
                        : getCurrency({
                            value: quotation.totHalfyearPrem,
                            sign,
                            decimals: premiumDecimal
                          })}
                    </Text>
                  </View>
                </View>
              )}
              <View
                style={
                  quotation.paymentMode !== "L"
                    ? styles.paymentMethod
                    : styles.paymentMethodFirst
                }
              >
                <View style={styles.paymentMethodTitleWrapper}>
                  {quotation.paymentMode !== "L" ? (
                    <TranslatedText
                      style={Theme.captionBrand}
                      path="quotation.quotationDetail.annualTotal"
                      funcOnText={text => text.toUpperCase()}
                    />
                  ) : (
                    <TranslatedText
                      style={Theme.captionBrand}
                      path="quotation.quotationDetail.totalSinglePremium"
                      funcOnText={text => text.toUpperCase()}
                    />
                  )}
                </View>
                <Text style={Theme.bodyPrimary}>
                  {quotation.totYearPrem === 0 || !quotation.totYearPrem
                    ? `${sign}${numberToCurrency({
                        value: 0,
                        decimal: premiumDecimal
                      })}`
                    : `${sign}${numberToCurrency({
                        value: quotation.totYearPrem,
                        decimal: premiumDecimal
                      })}`}
                </Text>
              </View>
            </View>
            {quotation.paymentMode !== "L" && (
              <Button
                onPress={() => {
                  this.setState({
                    shouldExpandBottomSheet: !shouldExpandBottomSheet
                  });
                }}
              >
                <Image
                  source={
                    shouldExpandBottomSheet
                      ? icCircleArrowDown
                      : icCircleArrowUp
                  }
                />
                <Text style={Theme.textButtonLabelSmallAccent}>
                  {shouldExpandBottomSheet
                    ? EABi18n({
                        language,
                        textStore,
                        path: "quotation.button.hideBreakdown"
                      })
                    : EABi18n({
                        language,
                        textStore,
                        path: "quotation.button.showBreakdown"
                      })}
                </Text>
              </Button>
            )}
          </View>
          {shouldExpandBottomSheet
            ? [
                <Table
                  key="header"
                  style={styles.breakdownHeaderWrapper}
                  tableConfig={breakDownTableConfig}
                >
                  <TableHeader key="table header" style={styles.tableHeader}>
                    <View>
                      <Text style={Theme.tableColumnLabel}>
                        {EABi18n({
                          language,
                          textStore,
                          path: "quotation.table.name"
                        })}
                      </Text>
                    </View>
                    <View>
                      <Text style={Theme.tableColumnLabel}>
                        {EABi18n({
                          language,
                          textStore,
                          path: "quotation.table.footer.sumAssured"
                        })}
                      </Text>
                    </View>
                    <View>
                      <Text style={Theme.tableColumnLabel}>
                        {EABi18n({
                          language,
                          textStore,
                          path: "quotation.table.monthlyPremium"
                        })}
                      </Text>
                    </View>
                    <View>
                      <Text style={Theme.tableColumnLabel}>
                        {EABi18n({
                          language,
                          textStore,
                          path: "quotation.table.quarterlyPremium"
                        })}
                      </Text>
                    </View>
                    <View>
                      <Text style={Theme.tableColumnLabel}>
                        {EABi18n({
                          language,
                          textStore,
                          path: "quotation.table.semiAnnualPremium"
                        })}
                      </Text>
                    </View>
                    <View>
                      <Text style={Theme.tableColumnLabel}>
                        {EABi18n({
                          language,
                          textStore,
                          path: "quotation.table.annualPremium"
                        })}
                      </Text>
                    </View>
                  </TableHeader>
                </Table>,
                <ScrollView
                  key="content"
                  style={styles.breakdownContentWrapper}
                >
                  <Table tableConfig={breakDownTableConfig}>
                    <TableSectionHeader
                      key="basic plan title"
                      style={styles.basicPlanHeader}
                      tableConfig={[
                        {
                          key: "A",
                          style: { flex: 1 }
                        }
                      ]}
                    >
                      <View>
                        <Text style={Theme.captionBrand}>
                          {EABi18n({
                            language,
                            textStore,
                            path: "quotation.table.basicPlan"
                          }).toUpperCase()}
                        </Text>
                      </View>
                    </TableSectionHeader>
                    {getTableConfig(language).basicPlanBreakDown}
                    {getTableConfig(language).ridersBreakDown.length > 0 ? (
                      <TableSectionHeader
                        key="rider title"
                        style={styles.basicPlanHeader}
                        tableConfig={[
                          {
                            key: "A",
                            style: { flex: 1 }
                          }
                        ]}
                      >
                        <View>
                          <Text style={Theme.captionBrand}>
                            {EABi18n({
                              language,
                              textStore,
                              path: "quotation.table.riders"
                            }).toUpperCase()}
                          </Text>
                        </View>
                      </TableSectionHeader>
                    ) : null}
                    {getTableConfig(language).ridersBreakDown}
                  </Table>
                </ScrollView>
              ]
            : null}
        </EABBottomSheet>
      );
    };

    const renderModal = language => {
      const { baseProductCode } = this.props.quotation;
      const { isFirstLoad } = this.state;
      let { openWarning } = this.state;

      if (isFirstLoad) {
        openWarning = true;
      }
      if (
        !openWarning ||
        !["PNP", "PNPP", "PNP2", "PNPP2", "AWICP"].includes(baseProductCode)
      ) {
        return null;
      }
      const getContent = () => {
        if (["PNP", "PNPP", "PNP2", "PNPP2"].includes(baseProductCode)) {
          const pathPrefix = ["PNP", "PNPP"].includes(baseProductCode)
            ? "PNP"
            : "PNP2";
          return (
            <View>
              <View style={styles.warningPaddingBottom}>
                <Text style={styles.warningContentBold}>
                  {EABi18n({
                    language,
                    textStore,
                    path: `quotation.warning.${pathPrefix}.text1.a`
                  })}{" "}
                  <Text style={{ textDecorationLine: "underline" }}>
                    {EABi18n({
                      language,
                      textStore,
                      path: `quotation.warning.${pathPrefix}.text1.b`
                    })}
                  </Text>
                </Text>
                <Text style={styles.warningContent}>
                  {EABi18n({
                    language,
                    textStore,
                    path: `quotation.warning.${pathPrefix}.text2`
                  })}
                </Text>
              </View>
              <View style={styles.warningPaddingBottom}>
                <Text style={styles.warningContent}>
                  {EABi18n({
                    language,
                    textStore,
                    path: `quotation.warning.${pathPrefix}.text3`
                  })}
                </Text>
              </View>
              <View style={styles.warningPaddingBottom}>
                <Text style={styles.warningContentBold}>
                  {EABi18n({
                    language,
                    textStore,
                    path: `quotation.warning.${pathPrefix}.text4.a`
                  })}
                </Text>
                <Text style={styles.warningContent}>
                  {EABi18n({
                    language,
                    textStore,
                    path: `quotation.warning.${pathPrefix}.text4.b`
                  })}
                </Text>
              </View>
              {pathPrefix === "PNP" ? (
                <View style={styles.warningPaddingBottom}>
                  <Text style={styles.warningContent}>
                    {EABi18n({
                      language,
                      textStore,
                      path: `quotation.warning.${pathPrefix}.text5`
                    })}
                  </Text>
                </View>
              ) : null}
            </View>
          );
        } else if (["AWICP"].includes(baseProductCode)) {
          const findAlert = _.find(
            this.props.inputConfigs[baseProductCode].policyOptions,
            p => _.isEqual(p.id, "paymentMethod")
          );
          const alertText = !_.isEmpty(findAlert)
            ? findAlert.alertOnValue[this.state.paymentMethod]
            : "CANNOT RETREIVE ALERT TEXT";
          return (
            <View>
              <HTML
                html={alertText}
                tagsStyles={{
                  p: {
                    fontSize: 15
                  }
                }}
              />
            </View>
          );
        }
        return null;
      };
      return (
        <View>
          <Modal animationType="fade" transparent>
            <View style={styles.warningContainer}>
              <View style={styles.warningInnerContainer}>
                <View style={styles.warningPaddingBottom}>
                  <Text
                    style={
                      ["AWICP"].includes(baseProductCode)
                        ? styles.warningTitleBold
                        : styles.warningTitle
                    }
                  >
                    {EABi18n({
                      language,
                      textStore,
                      path: "quotation.warning.modal.title"
                    })}
                  </Text>
                </View>
                {getContent()}
                <View style={styles.warningBtnRowPadRight}>
                  <Button
                    onPress={() => {
                      this.setState({ openWarning: false, isFirstLoad: false });
                    }}
                  >
                    <Text style={Theme.textButtonLabelNormalAccent}>
                      {EABi18n({
                        language,
                        textStore,
                        path: "button.ok"
                      })}
                    </Text>
                  </Button>
                </View>
              </View>
            </View>
          </Modal>
        </View>
      );
    };

    return (
      <ContextConsumer>
        {({ language }) => [
          <KeyboardAvoidingView
            key="quotationScrollView"
            behavior="padding"
            keyboardVerticalOffset={60}
          >
            <ScrollView
              style={styles.scrollView}
              contentContainerStyle={styles.contentContainerStyle}
              showsVerticalScrollIndicator={false}
            >
              <View style={styles.container}>
                {renderClientInfo()}
                {renderConfig(language)}
                {renderTableSection(language)}
                {renderFundSection(language)}
                <RiderDialog
                  isOpen={shouldShowSelectRiderDialog}
                  subQuotation={quotation}
                  subInputConfigs={inputConfigs}
                  onClose={() => {
                    this.setState({ shouldShowSelectRiderDialog: false });
                  }}
                  onSave={covCodes => {
                    updateRiders({ covCodes });
                  }}
                />
                {renderModal(language)}
                {renderFundsDialog(language)}
                {renderPortfolioDialog(language)}
              </View>
            </ScrollView>
          </KeyboardAvoidingView>,
          renderBottomSheet(language),
          <EABHUD key="loading hud" isOpen={isLoading} />
        ]}
      </ContextConsumer>
    );
  }
}

BasicQuotation.propTypes = {
  agent: PropTypes.oneOfType([PropTypes.object]).isRequired,
  quotation: WEB_API_REDUCER_TYPE_CHECK[QUOTATION].quotation.isRequired,
  planDetails: WEB_API_REDUCER_TYPE_CHECK[QUOTATION].planDetails.isRequired,
  inputConfigs: WEB_API_REDUCER_TYPE_CHECK[QUOTATION].inputConfigs.isRequired,
  quotationErrors: PropTypes.oneOfType([PropTypes.object]).isRequired,
  quotWarnings: WEB_API_REDUCER_TYPE_CHECK[QUOTATION].quotWarnings.isRequired,
  optionsMap: WEB_API_TYPES_CHECK[OPTIONS_MAP].isRequired,
  dependant: WEB_API_TYPES_CHECK[CLIENT].dependantProfiles.isRequired,
  quote: PropTypes.func.isRequired,
  resetQuot: PropTypes.func.isRequired,
  updateRiders: PropTypes.func.isRequired,
  updateQuotation: PropTypes.func.isRequired,
  getFunds: PropTypes.func.isRequired,
  allocFunds: PropTypes.func.isRequired,
  availableFunds:
    WEB_API_REDUCER_TYPE_CHECK[QUOTATION].availableFunds.isRequired,
  textStore: REDUCER_TYPES_CHECK[TEXT_STORE].isRequired,
  clientPhoto: WEB_API_TYPES_CHECK[CLIENT].dependantProfiles.isRequired
};
