import React, { PureComponent } from "react";
import { View, Text, Image } from "react-native";

import { REDUCER_TYPES, REDUCER_TYPE_CHECK, utilities } from "eab-web-api";
import icNested from "../../../../assets/images/icNested.png";
import EABTableSectionHeader from "../../../../components/EABTableSectionHeader";
import TranslatedText from "../../../../containers/TranslatedText";
import EABTable from "../../../../components/EABTable";
import EABTableSection from "../../../../components/EABTableSection";
import { ContextConsumer } from "../../../../context";
import EABTableHeader from "../../../../components/EABTableHeader";
import EABTableFooter from "../../../../components/EABTableFooter";
import getEABTextByLangType from "../../../../utilities/getEABTextByLangType";
import styles from "./styles";
import Theme from "../../../../theme";

const { getCurrency, getCurrencySign } = utilities.common;

/**
 * ApplicationShieldPlanDetails
 * @description item in ApplicationsList inside Application Summary page.
 * @param {object} value - value of application / quotation item object
 * */

class ApplicationShieldPlanDetails extends PureComponent {
  getProposalTable(language) {
    const { optionsMap } = this.props;
    const { values } = this.props.applicationPlanDetailsData.applicationForm;
    let proposerDetails = {};
    const shieldTableContent = [];
    if (values.proposer) {
      proposerDetails = values.proposer.groupedPlanDetails;
      shieldTableContent.push(
        <EABTableSectionHeader
          key="proposer"
          tableConfig={[{ key: "name", style: styles.tableSectionHeader }]}
        >
          <View>
            <Text style={Theme.captionBrand}>
              {values.proposer.personalInfo.firstName.toUpperCase()}
            </Text>
          </View>
        </EABTableSectionHeader>
      );
    }
    if (proposerDetails) {
      proposerDetails.basicPlanList.forEach((value, index) => {
        shieldTableContent.push(
          <EABTableSection
            key={`${values.proposer.personalInfo.firstName}_${index + 1}`}
          >
            <View>
              <Text style={styles.tableSectionTitle}>
                {getEABTextByLangType({
                  data: value.covName,
                  language
                })}
              </Text>
            </View>
            <View>
              <Text style={styles.tableSectionTitle}>{value.planType}</Text>
            </View>
            <View>
              <Text style={Theme.subheadPrimary}>
                {value.paymentMethodDesc}
              </Text>
            </View>
            <View>
              <Text style={styles.tableSectionTitle}>{value.payFreqDesc}</Text>
            </View>
            <View style={styles.premiumView}>
              <Text style={styles.tableSectionTitle}>
                {getCurrencySign({
                  compCode: "01",
                  ccy: values.proposer.extra.ccy,
                  optionsMap
                })}
                {getCurrency({
                  value: value.premium,
                  sign: "",
                  decimals: 2
                })}
              </Text>
              <TranslatedText
                language={language}
                path="application.planDetails.shield.premiumHint"
              />
            </View>
          </EABTableSection>
        );
      });
    }
    if (
      proposerDetails.totalRiderPremium &&
      proposerDetails.totalRiderPremium !== 0
    ) {
      proposerDetails.riderPlanList.forEach((value, index) => {
        shieldTableContent.push(
          <EABTableSection key={`${value.covCode}_${index + 1}`}>
            <View>
              <Image style={{ width: 24, height: 24 }} source={icNested} />
              <Text style={styles.tableSectionTitle}>
                {getEABTextByLangType({
                  data: value.covName,
                  language
                })}
              </Text>
            </View>

            <View>
              <Text style={styles.tableSectionTitle}>{value.planType}</Text>
            </View>
            <View>
              <Text style={styles.tableSectionTitle}>
                {value.paymentMethodDesc}
              </Text>
            </View>
            <View>
              <Text style={styles.tableSectionTitle}>{value.payFreqDesc}</Text>
            </View>
            <View />
          </EABTableSection>
        );
      });
      shieldTableContent.push(
        <EABTableFooter
          key={`${values.proposer.personalInfo.firstName}-footer`}
          tableConfig={[
            {
              key: `${values.proposer.personalInfo.firstName}-footerValue`,
              style: styles.tableFooterColumn2
            }
          ]}
        >
          <View style={styles.tableFooterValueView}>
            <TranslatedText
              style={Theme.tableHeaderTitle}
              language={language}
              path="application.planDetails.shield.totalRiderPremium"
            />
            <Text style={Theme.subheadPrimary}>
              {getCurrencySign({
                compCode: "01",
                ccy: values.proposer.extra.ccy,
                optionsMap
              })}
              {getCurrency({
                value: proposerDetails.totalRiderPremium,
                sign: "",
                decimals: 2
              })}
            </Text>
          </View>
        </EABTableFooter>
      );
    }
    return (
      <EABTable
        tableConfig={[
          {
            key: "plan_name",
            style: [styles.tableSectionColumn1, styles.tableSection]
          },
          {
            key: "planType",
            style: [styles.tableSectionColumn2, styles.tableSection]
          },
          {
            key: "payment_method",
            style: [styles.tableSectionColumn3, styles.tableSection]
          },
          {
            key: "payment_mode",
            style: [styles.tableSectionColumn4, styles.tableSection]
          },
          {
            key: "premium",
            style: [styles.tableSectionColumn5, styles.tableSection]
          }
        ]}
      >
        <EABTableHeader key="fundSelectionTableHeader">
          <View>
            <TranslatedText
              style={styles.tableHeaderTitle}
              language={language}
              path="application.summary.item_header_planName"
            />
          </View>
          <View>
            <TranslatedText
              style={styles.tableHeaderTitle}
              language={language}
              path="application.summary.item_header_policyType"
            />
          </View>
          <View>
            <TranslatedText
              style={styles.tableHeaderTitle}
              language={language}
              path="quotation.table.paymentMethod"
            />
          </View>
          <View>
            <TranslatedText
              style={styles.tableHeaderTitle}
              language={language}
              path="quotation.payMode.title"
            />
          </View>
          <View>
            <TranslatedText
              style={styles.tableHeaderTitle}
              language={language}
              path="application.planDetails.shield.premium"
            />
          </View>
        </EABTableHeader>
        {shieldTableContent}
        {this.getInsuredTable(language)}
      </EABTable>
    );
  }

  getInsuredTable(language) {
    const { optionsMap } = this.props;
    const { values } = this.props.applicationPlanDetailsData.applicationForm;
    let insuredDetails = [];
    const shieldTableContent = [];
    if (values.insured) {
      insuredDetails = values.insured;
    }
    if (!insuredDetails || !insuredDetails.length) {
      return null;
    }
    insuredDetails.forEach((value, index) => {
      shieldTableContent.push(
        <EABTableSectionHeader
          key={`${values.insured[index].personalInfo.firstName}_${index + 1}`}
          tableConfig={[{ key: "name", style: styles.tableSectionHeader }]}
        >
          <View>
            <Text style={Theme.captionBrand}>
              {values.insured[index].personalInfo.firstName.toUpperCase()}
            </Text>
          </View>
        </EABTableSectionHeader>
      );
      value.groupedPlanDetails.basicPlanList.forEach((planValue, planIndex) => {
        shieldTableContent.push(
          <EABTableSection
            key={`${values.insured[index].personalInfo.firstName}_${planIndex +
              1}`}
          >
            <View>
              <Text style={styles.tableSectionTitle}>
                {getEABTextByLangType({
                  data: planValue.covName,
                  language
                })}
              </Text>
            </View>
            <View>
              <Text style={styles.tableSectionTitle}>{planValue.planType}</Text>
            </View>
            <View>
              <Text style={Theme.subheadPrimary}>
                {planValue.paymentMethodDesc}
              </Text>
            </View>
            <View>
              <Text style={styles.tableSectionTitle}>
                {planValue.payFreqDesc}
              </Text>
            </View>
            <View style={styles.premiumView}>
              <Text style={styles.tableSectionTitle}>
                {getCurrencySign({
                  compCode: "01",
                  ccy: values.insured[index].extra.ccy,
                  optionsMap
                })}
                {getCurrency({
                  value: planValue.premium,
                  sign: "",
                  decimals: 2
                })}
              </Text>
              <TranslatedText
                language={language}
                path="application.planDetails.shield.premiumHint"
              />
            </View>
          </EABTableSection>
        );
      });
      if (
        value.groupedPlanDetails.totalRiderPremium &&
        value.groupedPlanDetails.totalRiderPremium !== 0
      ) {
        value.groupedPlanDetails.riderPlanList.forEach(
          (riderValue, riderIndex) => {
            shieldTableContent.push(
              <EABTableSection key={`${riderValue.covCode}_${riderIndex + 1}`}>
                <View>
                  <Image style={{ width: 24, height: 24 }} source={icNested} />
                  <Text style={styles.tableSectionTitle}>
                    {getEABTextByLangType({
                      data: riderValue.covName,
                      language
                    })}
                  </Text>
                </View>

                <View>
                  <Text style={styles.tableSectionTitle}>
                    {riderValue.planType}
                  </Text>
                </View>
                <View>
                  <Text style={styles.tableSectionTitle}>
                    {riderValue.paymentMethodDesc}
                  </Text>
                </View>
                <View>
                  <Text style={styles.tableSectionTitle}>
                    {riderValue.payFreqDesc}
                  </Text>
                </View>
                <View />
              </EABTableSection>
            );
          }
        );
        shieldTableContent.push(
          <EABTableFooter
            key={`${values.insured[index].personalInfo.firstName}-footer`}
            tableConfig={[
              {
                key: `${
                  values.insured[index].personalInfo.firstName
                }-footerValue`,
                style: styles.tableFooterColumn2
              }
            ]}
          >
            <View style={styles.tableFooterValueView}>
              <TranslatedText
                style={Theme.tableHeaderTitle}
                language={language}
                path="application.planDetails.shield.totalRiderPremium"
              />
              <Text style={Theme.subheadPrimary}>
                {getCurrencySign({
                  compCode: "01",
                  ccy: values.insured[index].extra.ccy,
                  optionsMap
                })}
                {getCurrency({
                  value: value.groupedPlanDetails.totalRiderPremium,
                  sign: "",
                  decimals: 2
                })}
              </Text>
            </View>
          </EABTableFooter>
        );
      }
    });
    return shieldTableContent;
    // keep for insured table header
    // tableCollection.push(
    //   <EABTable
    //     tableConfig={[
    //       {
    //         key: "plan_name",
    //         style: [styles.tableSectionColumn1, styles.tableSection]
    //       },
    //       {
    //         key: "planType",
    //         style: [styles.tableSectionColumn2, styles.tableSection]
    //       },
    //       {
    //         key: "payment_method",
    //         style: [styles.tableSectionColumn3, styles.tableSection]
    //       },
    //       {
    //         key: "payment_mode",
    //         style: [styles.tableSectionColumn4, styles.tableSection]
    //       },
    //       {
    //         key: "premium",
    //         style: [styles.tableSectionColumn5, styles.tableSection]
    //       }
    //     ]}
    //   >
    //     <EABTableHeader key="fundSelectionTableHeader">
    //       <View>
    //         <TranslatedText
    //           style={styles.tableHeaderTitle}
    //           language={language}
    //           path="application.summary.item_header_planName"
    //         />
    //       </View>
    //       <View>
    //         <TranslatedText
    //           style={styles.tableHeaderTitle}
    //           language={language}
    //           path="application.summary.item_header_policyType"
    //         />
    //       </View>
    //       <View>
    //         <TranslatedText
    //           style={styles.tableHeaderTitle}
    //           language={language}
    //           path="quotation.table.paymentMethod"
    //         />
    //       </View>
    //       <View>
    //         <TranslatedText
    //           style={styles.tableHeaderTitle}
    //           language={language}
    //           path="quotation.payMode.title"
    //         />
    //       </View>
    //       <View>
    //         <TranslatedText
    //           style={styles.tableHeaderTitle}
    //           language={language}
    //           path="application.planDetails.shield.premium"
    //         />
    //       </View>
    //     </EABTableHeader>
    //     {shieldTableContent}
    //   </EABTable>
    // );
    // return tableCollection;
  }

  render() {
    return (
      <ContextConsumer>
        {({ language }) => (
          <View style={styles.scrollViewContentSubview}>
            <View style={styles.fundSelectionTable}>
              {this.getProposalTable(language)}
            </View>
          </View>
        )}
      </ContextConsumer>
    );
  }
}

ApplicationShieldPlanDetails.propTypes = {
  optionsMap: REDUCER_TYPE_CHECK[REDUCER_TYPES.OPTIONS_MAP].isRequired,
  applicationPlanDetailsData:
    REDUCER_TYPE_CHECK[REDUCER_TYPES.APPLICATION].planDetails
      .applicationPlanDetailsData.isRequired
};

export default ApplicationShieldPlanDetails;
