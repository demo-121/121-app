import { combineReducers } from "redux";
import ACTION_TYPES from "../constants/ACTION_TYPES";
import { PAYMENT } from "../constants/REDUCER_TYPES";

const paymentMenthod = (state = "", action) => {
  switch (action.type) {
    case ACTION_TYPES[PAYMENT].PAYMENT_METHOD:
      state = action.paymentMenthod;
      return state;
    default:
      return state;
  }
};

const isOnlineAuthCompleted = (state = false, { type }) => {
  switch (type) {
    case ACTION_TYPES[PAYMENT].ONLINE_AUTH_SUCCESS:
      state = true;
      return state;
    case ACTION_TYPES[PAYMENT].ONLINE_AUTH_FAIL:
      state = false;
      return state;
    default:
      return state;
  }
};

export default combineReducers({
  paymentMenthod,
  isOnlineAuthCompleted
});
