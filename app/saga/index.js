import { takeEvery, takeLatest } from "redux-saga/effects";
import { ACTION_TYPES, REDUCER_TYPES } from "eab-web-api";
import inflightJob from "./inflightJob";
import navigate from "./navigate";
import APP_ACTION_TYPES from "../constants/ACTION_TYPES";
import {
  LOGIN,
  NAVIGATION,
  PAYMENT,
  SUBMISSION,
  POLICYNUMBER,
  INFLIGHT_JOB
} from "../constants/REDUCER_TYPES";

import {
  onlineAuthCode,
  onlineAuthCode2,
  onlineAuthCodeForPayment,
  saveNewAppPassword,
  saveNewAppPasswordDEV,
  resetAppPasswordManually,
  verify121OnlineAuth,
  verify121OnlineAuthForPayment,
  checkLoginStatus,
  setEnvironment,
  loginByAppPassword,
  loginByAppPasswordDEV,
  loginByOnlineAuth,
  checkTimeBomb,
  updateAgentProfile,
  checkTokenExpiryDate,
  checkLastDataSyncTime,
  setLastDataSyncTime,
  checkAppLocked,
  setAppLocked,
  checkFailAttempt,
  checkValidDevice,
  runView
} from "./login";
import { preCheck, preCheckOnDataSync } from "./dataSync";
import { dataSyncForSubmission, submission } from "./submission";
import {
  checkPolicyNumber,
  initPolicyNumber,
  applyPolicyNumber,
  submitPolicyNumber,
  showPolicyNumber,
  counterCheckPolicyNumber
} from "./policyNumber";

export default function* rootSaga() {
  yield takeEvery(
    APP_ACTION_TYPES[NAVIGATION].NAVIGATE_TO_NEEDS,
    navigate.navigateToNeeds
  );

  yield takeEvery(APP_ACTION_TYPES[LOGIN].ONLINE_AUTH_CODE, onlineAuthCode);

  yield takeEvery(APP_ACTION_TYPES[LOGIN].ONLINE_AUTH_CODE_2, onlineAuthCode2);
  yield takeEvery(APP_ACTION_TYPES[LOGIN].NEW_APP_PASSWORD, saveNewAppPassword);
  yield takeEvery(
    APP_ACTION_TYPES[LOGIN].NEW_APP_PASSWORD_DEV,
    saveNewAppPasswordDEV
  );
  yield takeEvery(
    APP_ACTION_TYPES[LOGIN].RESET_APP_PASSWORD_MANUALLY2,
    resetAppPasswordManually
  );
  yield takeEvery(
    APP_ACTION_TYPES[LOGIN].LOGIN_BY_ONLINE_AUTH2,
    verify121OnlineAuth
  );

  yield takeEvery(APP_ACTION_TYPES[LOGIN].CHECK_LOGIN_STATUS, checkLoginStatus);
  yield takeEvery(APP_ACTION_TYPES[LOGIN].SET_ENVIRONMENT, setEnvironment);
  yield takeEvery(
    APP_ACTION_TYPES[LOGIN].LOGIN_BY_APP_PASSWORD,
    loginByAppPassword
  );
  yield takeEvery(
    APP_ACTION_TYPES[LOGIN].LOGIN_BY_APP_PASSWORD_DEV,
    loginByAppPasswordDEV
  );
  yield takeEvery(
    APP_ACTION_TYPES[LOGIN].LOGIN_BY_ONLINE_AUTH,
    loginByOnlineAuth
  );

  yield takeEvery(APP_ACTION_TYPES[LOGIN].CHECK_TIME_BOMB, checkTimeBomb);

  yield takeEvery(
    ACTION_TYPES[REDUCER_TYPES.PROPOSAL].CLOSE_PROPOSAL,
    navigate.closeProposal
  );
  yield takeEvery(
    APP_ACTION_TYPES[PAYMENT].LOGIN_BY_ONLINE_AUTH_FOR_PAYMENT,
    loginByOnlineAuth
  );

  yield takeEvery(
    APP_ACTION_TYPES[PAYMENT].ONLINE_AUTH_CODE_FOR_PAYMENT,
    onlineAuthCodeForPayment
  );
  yield takeEvery(
    APP_ACTION_TYPES[PAYMENT].LOGIN_BY_ONLINE_AUTH2_FOR_PAYMENT,
    verify121OnlineAuthForPayment
  );
  yield takeEvery(
    APP_ACTION_TYPES[SUBMISSION].DATA_SYNC_SUBMISSION,
    dataSyncForSubmission
  );
  yield takeEvery(APP_ACTION_TYPES[SUBMISSION].SUBMIT_APPLICATION, submission);

  yield takeEvery(
    APP_ACTION_TYPES[LOGIN].CHECK_TOKEN_EXPIRY_DATE,
    checkTokenExpiryDate
  );

  // Policy Number
  yield takeEvery(
    APP_ACTION_TYPES[POLICYNUMBER].CHECK_POLICY_NUMBER,
    checkPolicyNumber
  );
  yield takeEvery(
    APP_ACTION_TYPES[POLICYNUMBER].INIT_POLICY_NUMBER,
    initPolicyNumber
  );
  yield takeEvery(
    APP_ACTION_TYPES[POLICYNUMBER].APPLY_POLICY_NUMBER,
    applyPolicyNumber
  );
  yield takeEvery(
    APP_ACTION_TYPES[POLICYNUMBER].COUNTER_CHECK_POLICY_NUMBER,
    counterCheckPolicyNumber
  );
  yield takeEvery(
    APP_ACTION_TYPES[POLICYNUMBER].SUBMIT_POLICY_NUMBER,
    submitPolicyNumber
  );
  yield takeEvery(
    APP_ACTION_TYPES[POLICYNUMBER].SHOW_POLICY_NUMBER,
    showPolicyNumber
  );
  yield takeEvery(
    APP_ACTION_TYPES[LOGIN].CHECK_LAST_DATA_SYNC_TIME,
    checkLastDataSyncTime
  );

  yield takeEvery(
    APP_ACTION_TYPES[LOGIN].SET_LAST_DATA_SYNC_TIME,
    setLastDataSyncTime
  );
  yield takeEvery(
    APP_ACTION_TYPES[LOGIN].UPDATE_AGENT_PROFILE,
    updateAgentProfile
  );
  yield takeEvery(APP_ACTION_TYPES[LOGIN].CHECK_APP_LOCKED, checkAppLocked);
  yield takeEvery(APP_ACTION_TYPES[LOGIN].SET_APP_LOCKED, setAppLocked);
  yield takeEvery(APP_ACTION_TYPES[LOGIN].CHECK_FAIL_ATTEMPT, checkFailAttempt);
  yield takeEvery(APP_ACTION_TYPES[LOGIN].CHECK_VALID_DEVICE, checkValidDevice);
  yield takeEvery(APP_ACTION_TYPES[LOGIN].PRE_CEHCK, preCheck);
  yield takeEvery(
    APP_ACTION_TYPES[LOGIN].PRE_CEHCK_ON_DATA_SYNC,
    preCheckOnDataSync
  );
  yield takeEvery(APP_ACTION_TYPES[LOGIN].RUN_VIEW, runView);
  yield takeLatest(
    APP_ACTION_TYPES[INFLIGHT_JOB].RUN_INFLIGHT_JOBS,
    inflightJob
  );
}
