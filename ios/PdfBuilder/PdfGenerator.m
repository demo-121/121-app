//
//  PdfGenerator.m
//  EASE
//
//  Created by Osric Wong on 18/5/2018.
//  Copyright © 2018 Facebook. All rights reserved.
//

#import "PdfGenerator.h"
#import <React/RCTUtils.h>
#import <React/RCTLog.h>
#import <React/RCTConvert.h>
#import "PdfHelper.h"
#import "EABPrintPageRenderer.h"

@implementation PdfGenerator {
  NSString *_fileName;
  NSString *_filePath;
  NSString *_lang;
  BOOL _base64;
  UIWebView *_handleDOMWebView;
  UIWebView *_webView;
  UIImage *_overlayPEP;
  UIImage *_overlayNPTP;
  UIImage *_overlayWLP;
  UIImage *_companyLogo;
  UIImage *_barcode;
  NSMutableDictionary *_options;
  RCTPromiseResolveBlock _resolve;
  RCTPromiseRejectBlock _reject;
}

RCT_EXPORT_MODULE();

+ (BOOL)requiresMainQueueSetup
{
  return YES;
}

- (dispatch_queue_t)methodQueue {
  return dispatch_get_main_queue();
}

@synthesize bridge = _bridge;

- (instancetype)init
{
  if (self = [super init]) {
    _webView = [[UIWebView alloc] initWithFrame:self.bounds];
    _webView.delegate = self;
    
    _handleDOMWebView = [[UIWebView alloc] initWithFrame:self.bounds];
    _handleDOMWebView.delegate = self;
    
    NSBundle *bundle = [NSBundle mainBundle];
    _overlayNPTP = [UIImage imageWithContentsOfFile: [bundle pathForResource:@"overlayNPTP" ofType:@"png"]];
    _overlayPEP = [UIImage imageWithContentsOfFile: [bundle pathForResource:@"overlayPEP" ofType:@"png"]];
    _overlayWLP = [UIImage imageWithContentsOfFile: [bundle pathForResource:@"overlayWLP" ofType:@"png"]];
    _companyLogo = [UIImage imageWithContentsOfFile: [bundle pathForResource:@"companyLogo" ofType:@"png"]];
    _barcode = [UIImage imageWithContentsOfFile: [bundle pathForResource:@"barcode" ofType:@"png"]];
    
    [self addSubview:_handleDOMWebView];
    [self addSubview:_webView];
  }
  return self;
}

RCT_EXPORT_METHOD(addTitle:(NSString *)pdfBase64Data
                  title:(NSString *)title
                  callback:(RCTResponseSenderBlock)callback) {
  NSLog(@"addTitle...start");

  // Save in temp path
  NSString *filePath = NSTemporaryDirectory();
  NSString *tempPdfFileName = @"pdf.pdf";
  
  NSString *tempPdfPath = [NSString stringWithFormat:@"%@%@", filePath, tempPdfFileName];
  NSURL *tempPdfUrl = [NSURL URLWithString:tempPdfPath];
  NSData * pdfData = [[NSData alloc] initWithBase64EncodedString:pdfBase64Data options:NSDataBase64DecodingIgnoreUnknownCharacters];
  [pdfData writeToFile:[tempPdfUrl absoluteString] atomically:YES];
  
  // File path for result pdf
  NSString *resultPdfFileName = @"addWordsPdf.pdf";
  NSString *resultPdfPath = [NSString stringWithFormat:@"%@%@", filePath, resultPdfFileName];
  NSURL *resultUrl = [NSURL fileURLWithPath:resultPdfPath];
  
  CFURLRef pdfURLOutput = (CFURLRef)CFBridgingRetain(resultUrl);

  // Add title to pdf
  CFMutableDictionaryRef myDictionary = CFDictionaryCreateMutable(NULL, 0,
                        &kCFTypeDictionaryKeyCallBacks,
                        &kCFTypeDictionaryValueCallBacks);
  CFDictionarySetValue(myDictionary, kCGPDFContextTitle, CFBridgingRetain(title));
  
  // Create the output context
  CGContextRef writeContext = CGPDFContextCreateWithURL(pdfURLOutput, NULL, myDictionary);
  CFURLRef pdfURL = (CFURLRef)CFBridgingRetain([NSURL fileURLWithPath:tempPdfPath]);
  
  // file ref
  CGPDFDocumentRef pdfRef = CGPDFDocumentCreateWithURL((CFURLRef) pdfURL);
  NSInteger numberOfPages = CGPDFDocumentGetNumberOfPages(pdfRef);
  
  // Loop variables
  CGPDFPageRef page;
  CGRect mediaBox;
  
  // Read the PDF and generate the output pages
  NSLog(@"addTitle GENERATING PAGES from PDF (%@)...", tempPdfFileName);
  for (int i = 1; i <= numberOfPages; i++) {
    page = CGPDFDocumentGetPage(pdfRef, i);
    mediaBox = CGPDFPageGetBoxRect(page, kCGPDFMediaBox);
    CGContextBeginPage(writeContext, &mediaBox);
    CGContextDrawPDFPage(writeContext, page);
    
    CGContextEndPage(writeContext);
  }
  
  CGPDFDocumentRelease(pdfRef);
  CFRelease(pdfURL);
  CFRelease(pdfURLOutput);
  
  CGPDFContextClose(writeContext);
  CGContextRelease(writeContext);
  
  // Delete Temp PDF
  NSFileManager *manager = [NSFileManager defaultManager];
  NSError *error = nil;
  [manager removeItemAtPath:tempPdfPath error:&error];
  
  // Get Base64 string for changed PDF and Delete Temp PDF
  NSData *addWordsPdfData = [NSData dataWithContentsOfFile:resultPdfPath];
  NSString *addWordsPdfString = [addWordsPdfData base64EncodedStringWithOptions:NSDataBase64EncodingEndLineWithLineFeed];
  [manager removeItemAtPath:resultPdfPath error:&error];
  
  NSLog(@"addTitle COMPLETED for PDF");
  
  callback(@[addWordsPdfString]);
}

RCT_EXPORT_METHOD(addWordsPdfs:(NSString *)pdfBase64Data
                  agentName:(NSString *)agentName
                  proposerName:(NSString *)proposerName
                  x1:(CGFloat)x1
                  y1:(CGFloat)y1
                  x2:(CGFloat)x2
                  y2:(CGFloat)y2
                  callback:(RCTResponseSenderBlock)callback) {
  NSLog(@"addWordsPdfs...start");
  UIFont *regularFont = [UIFont fontWithName:@"SourceSansPro-Regular" size:10];
  NSDictionary *regularAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                     regularFont, NSFontAttributeName,
                                     [NSNumber numberWithFloat:1.0], NSBaselineOffsetAttributeName, nil];
  
  // Save in temp path
  NSString *filePath = NSTemporaryDirectory();
  NSString *tempPdfFileName = @"pdf.pdf";
  
  NSString *tempPdfPath = [NSString stringWithFormat:@"%@%@", filePath, tempPdfFileName];
  NSURL *tempPdfUrl = [NSURL URLWithString:tempPdfPath];
  NSData * pdfData = [[NSData alloc] initWithBase64EncodedString:pdfBase64Data options:NSDataBase64DecodingIgnoreUnknownCharacters];
  [pdfData writeToFile:[tempPdfUrl absoluteString] atomically:YES];
  
  // File path for result pdf
  NSString *resultPdfFileName = @"addWordsPdf.pdf";
  NSString *resultPdfPath = [NSString stringWithFormat:@"%@%@", filePath, resultPdfFileName];
  NSURL *resultUrl = [NSURL fileURLWithPath:resultPdfPath];
  
  CFURLRef pdfURLOutput = (CFURLRef)CFBridgingRetain(resultUrl);

  // Create the output context
  CGContextRef writeContext = CGPDFContextCreateWithURL(pdfURLOutput, NULL, NULL);
  CFURLRef pdfURL = (CFURLRef)CFBridgingRetain([NSURL fileURLWithPath:tempPdfPath]);
  
  // file ref
  CGPDFDocumentRef pdfRef = CGPDFDocumentCreateWithURL((CFURLRef) pdfURL);
  NSInteger numberOfPages = CGPDFDocumentGetNumberOfPages(pdfRef);

  // Loop variables
  CGPDFPageRef page;
  CGRect mediaBox;
  
  // Read the PDF and generate the output pages
  NSLog(@"addWords GENERATING PAGES from PDF (%@)...", tempPdfFileName);
  for (int i = 1; i <= numberOfPages; i++) {
    page = CGPDFDocumentGetPage(pdfRef, i);
    mediaBox = CGPDFPageGetBoxRect(page, kCGPDFMediaBox);
    CGContextBeginPage(writeContext, &mediaBox);
    CGContextDrawPDFPage(writeContext, page);
    
    // since eapp form use different papaer size, check and set the pageSize
    CGSize pageSize = kPageSizeA4;
    if ([_options[@"type"] isEqualToString:@"EAPP"]) {
      pageSize = kPageSizeA4_eapp;
    }
    
    if (i == 1) {
      CGFloat yAdjust = 10.0;
      CGFloat y1Translate = pageSize.height - y1;
      CGFloat y2Translate = pageSize.height - y2;
      
      //Draw text
      UIGraphicsPushContext(writeContext);
      
      CGRect bounds = CGContextGetClipBoundingBox(writeContext);
      CGContextScaleCTM(writeContext, 1.0f, -1.0f);
      CGContextTranslateCTM(writeContext, 0.0f, -bounds.size.height);
      
      [agentName drawAtPoint: CGPointMake(x1, y1Translate - yAdjust)
              withAttributes:regularAttributes];
      
      [proposerName drawAtPoint: CGPointMake(x2, y2Translate - yAdjust)
              withAttributes:regularAttributes];
      
      UIGraphicsPopContext();
    }
    
    CGContextEndPage(writeContext);
  }
  
  CGPDFDocumentRelease(pdfRef);
  CFRelease(pdfURL);
  CFRelease(pdfURLOutput);
  
  CGPDFContextClose(writeContext);
  CGContextRelease(writeContext);
  
  // Delete Temp PDF
  NSFileManager *manager = [NSFileManager defaultManager];
  NSError *error = nil;
  [manager removeItemAtPath:tempPdfPath error:&error];
  
  // Get Base64 string for changed PDF and Delete Temp PDF
  NSData *addWordsPdfData = [NSData dataWithContentsOfFile:resultPdfPath];
  NSString *addWordsPdfString = [addWordsPdfData base64EncodedStringWithOptions:NSDataBase64EncodingEndLineWithLineFeed];
  [manager removeItemAtPath:resultPdfPath error:&error];
  
  NSLog(@"addWords COMPLETED for PDF");
  
  callback(@[addWordsPdfString]);
}


RCT_EXPORT_METHOD(convertToHTML:(NSString *)xml
                  xslt:(NSString *)xslt
                  resolver:(RCTPromiseResolveBlock)resolve
                  rejecter:(RCTPromiseRejectBlock)reject)
{
  NSString *html = [PdfHelper transformXml:xml template:xslt];
  resolve(@[html]);
}

RCT_EXPORT_METHOD(convertToPDF:(NSString *)reportHTML
                  options:(NSDictionary *)options
                  resolver:(RCTPromiseResolveBlock)resolve
                  rejecter:(RCTPromiseRejectBlock)reject)
{
  NSLog(@"convertToPDF....start");
  _resolve = resolve;
  _reject = reject;
  
  if (options[@"fileName"]){
    _fileName = [RCTConvert NSString:options[@"fileName"]];
  } else {
    _fileName = [[NSProcessInfo processInfo] globallyUniqueString];
  }
  
  if (options[@"directory"] && [options[@"directory"] isEqualToString:@"docs"]){
    // allow document query by iTune, and sync with iCloud
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsPath = [paths objectAtIndex:0];
    _filePath = [NSString stringWithFormat:@"%@/%@.pdf", documentsPath, _fileName];
  } else {
    _filePath = [NSString stringWithFormat:@"%@%@.pdf", NSTemporaryDirectory(), _fileName];
  }
  
  _base64 = options[@"base64"] && [options[@"base64"] boolValue];
  _lang = options[@"lang"] ? [RCTConvert NSString:options[@"lang"]] : @"en";
  _options = [NSMutableDictionary dictionaryWithDictionary:options];
  NSMutableDictionary *footer = _options[@"footer"];
  if ([options[@"type"] isEqualToString:@"BI"]) {
    [footer setObject:@[
                        @"EAB Insurance Pte Ltd (Co. Reg. No. 199903512M)",
                        @"8 Shenton Way, #24-01, EAB Tower, Singapore 068811",
                        @"EAB CUSTOMER CARE CENTRE: #01-21/22 TEL: 6880 4888 FAX: 6880 5501",
                        @"www.EAB.com.sg"]
               forKey:@"address"];
  } else if ([options[@"type"] isEqualToString:@"EAPP"]) {
    [footer setObject:@[
                        @"EAB Insurance Pte Ltd (Company Reg. No. 199903512M)",
                        @"8 Shenton Way #24-01 EAB Tower Singapore 068811",
                        @"EAB Customer Centre #B1-01 Tel: 1800 880 4888 Fax: 6880 5501",
                        @"www.EAB.com.sg"]
               forKey:@"address"];
  }

  NSLog(@"convertToPDF....before load");
  [_handleDOMWebView loadHTMLString:reportHTML baseURL:nil];
}

RCT_EXPORT_METHOD(convert:(NSString *)commonTemplateJson
                  reportTemplateJson:(NSString *)reportTemplateJson
                  xml:(NSString *)xml
                  options:(NSDictionary *)options
                  resolver:(RCTPromiseResolveBlock)resolve
                  rejecter:(RCTPromiseRejectBlock)reject)
{
  _resolve = resolve;
  _reject = reject;
  
  if (options[@"fileName"]){
    _fileName = [RCTConvert NSString:options[@"fileName"]];
  } else {
    _fileName = [[NSProcessInfo processInfo] globallyUniqueString];
  }
  
  if (options[@"directory"] && [options[@"directory"] isEqualToString:@"docs"]){
    // allow document query by iTune, and sync with iCloud
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsPath = [paths objectAtIndex:0];
    _filePath = [NSString stringWithFormat:@"%@/%@.pdf", documentsPath, _fileName];
  } else {
    _filePath = [NSString stringWithFormat:@"%@%@.pdf", NSTemporaryDirectory(), _fileName];
  }
  
  if (options[@"base64"] && [options[@"base64"] boolValue]) {
    _base64 = true;
  } else {
    _base64 = false;
  }
  
  if (options[@"lang"]){
    _lang = [RCTConvert NSString:options[@"lang"]];
  } else {
    _lang = @"en";
  }
  
  [PdfHelper generatePdf:self webView:_webView commonTemplateJson:commonTemplateJson reportTemplateJson:reportTemplateJson xml:xml templateLang:_lang];
}

- (void)updateOptionsByEvaluatingJavaScript {
  NSString *planCodes = [_handleDOMWebView stringByEvaluatingJavaScriptFromString:@"document.querySelector('div.planCodes').textContent"];
  [_options[@"footer"] setObject:planCodes forKey:@"planCodes"];
}

- (NSString *)removeElementsByEvaluatingJavaScript {
  //remove footer
  [_handleDOMWebView stringByEvaluatingJavaScriptFromString:@"document.querySelectorAll('div.footer').forEach(function(a){a.remove()})"];
  
  //remove overlay
  [_handleDOMWebView stringByEvaluatingJavaScriptFromString:@"document.querySelectorAll('div.overlay').forEach(function(a){a.remove()})"];
  
  //remove signature box
  [_handleDOMWebView stringByEvaluatingJavaScriptFromString:@"document.querySelectorAll('div.signature').forEach(function(a){a.remove()})"];
  
  return [_handleDOMWebView stringByEvaluatingJavaScriptFromString:@"document.documentElement.outerHTML"];
}

#pragma mark - UIWebViewDelegate functions
- (void)webViewDidFinishLoad:(UIWebView *)webView {
  if (!webView.isLoading) {
    if (webView == _handleDOMWebView) {
      [self updateOptionsByEvaluatingJavaScript];
      NSString *resultHtml = [self removeElementsByEvaluatingJavaScript];

      NSLog(@"convertToPDF....generatePdf");
      [PdfHelper generatePdf:self webView:_webView html:resultHtml];
    } else {
      
      // since eapp form use different papaer size, check and set the pageSize
      CGSize pageSize = kPageSizeA4;
      if ([_options[@"type"] isEqualToString:@"EAPP"]) {
        pageSize = kPageSizeA4_eapp;
      }
      
      UIEdgeInsets margins = UIEdgeInsetsZero;
      EABPrintPageRenderer *render = [[EABPrintPageRenderer alloc] init];
      [render addPrintFormatter:webView.viewPrintFormatter startingAtPageAtIndex:0];
      render.headerHeight = [_options[@"headerHeight"] floatValue];
      render.footerHeight = [_options[@"footerHeight"] floatValue];
      render.companyLogo = _companyLogo;
      render.barcode = _barcode;
      if ([_options[@"overlayNumOfPage"] intValue] > 0) {
        switch ([_options[@"overlayType"] intValue]) {
          default:
            render.overlay = _overlayPEP;
            break;
          case 2:
            render.overlay = _overlayNPTP;
            break;
          case 3:
            render.overlay = _overlayWLP;
            break;
        }      
      }
      render.options = _options;
      CGRect printableRect = CGRectMake(margins.left,
                                        margins.top,
                                        pageSize.width - margins.left - margins.right,
                                        pageSize.height - margins.top - margins.bottom);
      
      CGRect paperRect = CGRectMake(0, 0, pageSize.width, pageSize.height);
      [render setValue:[NSValue valueWithCGRect:paperRect] forKey:@"paperRect"];
      [render setValue:[NSValue valueWithCGRect:printableRect] forKey:@"printableRect"];
      NSData *pdfData = [render printToPDF];
      [self stopWebView];
      if (pdfData) {
        NSString *pdfBase64 = @"";
        
        [pdfData writeToFile:_filePath atomically:YES];
        if (_base64) {
          pdfBase64 = [pdfData base64EncodedStringWithOptions:0];
        }
        NSDictionary *data = [NSDictionary dictionaryWithObjectsAndKeys:
                              pdfBase64, @"base64",
                              _filePath, @"filePath", nil];
        NSLog(@"convertToPDF...completed");
        _resolve(data);
      } else {
        NSError *error;
        _reject(RCTErrorUnspecified, nil, RCTErrorWithMessage(error.description));
      }
    }
  }
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
  if (!webView.isLoading) {
    [self stopWebView];
    _reject(RCTErrorUnspecified, nil, RCTErrorWithMessage(error.description));
  }
}

- (void)stopWebView {
  [_webView stopLoading];
  [_webView removeFromSuperview];
  _webView = nil;
}

@end
