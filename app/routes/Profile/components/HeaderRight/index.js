import {
  REDUCER_TYPE_CHECK as WEB_API_TYPES_CHECK,
  REDUCER_TYPES
} from "eab-web-api";
import PropTypes from "prop-types";
import React, { Component } from "react";
import { View } from "react-native";
import EABButton from "../../../../components/EABButton";
import TranslatedText from "../../../../containers/TranslatedText";
import Theme from "../../../../theme";
import { ContextConsumer } from "../../../../context";

const { CLIENT } = REDUCER_TYPES;

/**
 * HeaderRight
 * @param {object} navigation - react navigation props
 * @param {function} cancelCreateClient - clean data and close dialog
 * @param {function} readyToAddFamilyMember - initialize client form for family member
 * @param {function} readyToEditFamilyMember - initialize client form for editing family member
 * @param {function} saveClient - save client data from client form
 * @param {function} readyToEditClient - initialize client form for subject client
 * @param {function} saveTrustedIndividual - save trusted individual data from
 *   client form
 * @param {function} loadClient - load profile data to client form
 * */
export default class HeaderRight extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <ContextConsumer>
        {({ showCreateProfileDialog }) => (
          <View
            style={{
              marginTop: Theme.alignmentS,
              height: "100%",
              justifyContent: "flex-start"
            }}
          >
            <EABButton
              style={{
                marginRight: Theme.alignmentXL
              }}
              onPress={() => {
                this.props.loadClient(this.props.profile);
                this.props.readyToEditClient(() => {});
                showCreateProfileDialog();
              }}
            >
              <TranslatedText
                style={Theme.textButtonLabelNormalAccent}
                path="button.editProfile"
              />
            </EABButton>
          </View>
        )}
      </ContextConsumer>
    );
  }
}

HeaderRight.propTypes = {
  loadClient: PropTypes.func.isRequired,
  readyToEditClient: PropTypes.func.isRequired,
  profile: WEB_API_TYPES_CHECK[CLIENT].profile.isRequired
};
