import React, { Component } from "react";
import ClientFormContact from "../../../../containers/ClientFormContact";
import ClientFormProfile from "../../../../containers/ClientFormProfile";

/**
 * <EditLinkClientForm />
 * */
export default class EditLinkClientForm extends Component {
  constructor(props) {
    super(props);

    this.state = {
      segmentedControlArray: [
        {
          key: "A",
          title: "Profile",
          isShowingBadge: false,
          onPress: () => {
            this.setState({ showProfileTab: "A" });
            this.props.navigation.setParams({
              showProfileTab: "A"
            });
          }
        },
        {
          key: "B",
          title: "Contact",
          isShowingBadge: false,
          onPress: () => {
            this.setState({ showProfileTab: "B" });
            this.props.navigation.setParams({
              showProfileTab: "B"
            });
          }
        }
      ],
      showProfileTab: "A"
    };
  }

  // ===========================================================================
  // life circles
  // ===========================================================================
  componentDidMount() {
    this.props.navigation.setParams({
      segmentedControlArray: this.state.segmentedControlArray,
      showProfileTab: this.state.showProfileTab
    });
  }

  componentDidUpdate() {
    const { isDisable } = this.props.navigation.state.params;
    const validateStatus = this.isDisableSaveClient;
    if (isDisable !== validateStatus) {
      this.props.navigation.setParams({
        isDisable: validateStatus
      });
    }
  }

  render() {
    return this.state.showProfileTab === "A" ? (
      <ClientFormProfile key="Profile" />
    ) : (
      <ClientFormContact key="Contact" />
    );
  }
}
