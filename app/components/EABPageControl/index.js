import PropTypes from "prop-types";
import React, { Component } from "react";
import { Dimensions, NetInfo, ScrollView, View } from "react-native";
import Theme from "../../theme";

import styles from "./styles";

/**
 * <EABPageControl />
 * @description page control component. all page height can not more than 100% and width can
 *   not more than window width 100%. Have to be aware, this is not a full control component.
 *   you cannot change the page by any component methods.
 * @param {array} pages - the array of page object
 * @param {string} pages[].key - page key
 * @param {element} pages[].component - page component
 * @param {element} pages[].canScroll - indicate use ScrollView or not
 * @example see ./code.sample.md
 * */
class EABPageControl extends Component {
  constructor(props) {
    super(props);

    this.handleConnectionChange = this.handleConnectionChange.bind(this);

    this.state = {
      pageOffset: 0,
      isNetworkConnected: false
    };
  }

  componentDidMount() {
    // listen to network connection, isConnected: ${bool}
    NetInfo.isConnected.addEventListener(
      "connectionChange",
      this.handleConnectionChange
    );
    NetInfo.isConnected.fetch().done(this.handleConnectionChange);
  }

  componentWillUnmount() {
    NetInfo.isConnected.removeEventListener(
      "connectionChange",
      this.handleConnectionChange
    );
  }

  handleConnectionChange(isConnected) {
    this.setState({ isNetworkConnected: isConnected });
  }

  render() {
    /**
     * functions
     * */
    const handleMomentumScrollEnd = event => {
      if (this.state.isNetworkConnected) {
        this.props.updateAgentProfile();
      }
      const { x } = event.nativeEvent.contentOffset || 0;
      this.setState({
        pageOffset: x
      });
    };

    const renderPages = () => {
      const { pages } = this.props;
      const { pageStyle, pageContentContainerStyle } = styles;
      return pages.map(page => {
        const { key, component } = page;
        return page.canScroll ? (
          <ScrollView
            key={key}
            style={pageStyle}
            showsVerticalScrollIndicator={false}
            contentContainerStyle={pageContentContainerStyle}
          >
            {component}
          </ScrollView>
        ) : (
          React.cloneElement(component, { key })
        );
      });
    };

    const renderToolBar = () => {
      const { pages } = this.props;
      const { pageOffset } = this.state;
      const { dotFirst, dot } = styles;
      return pages.map((page, index) => {
        const { key } = page;
        const dotStyle = index === 0 ? dotFirst : dot;
        const backgroundColor =
          pageOffset === index * Dimensions.get("window").width
            ? Theme.darkGrey
            : Theme.coolGrey;
        const viewStyle = [dotStyle, { backgroundColor }];
        return <View key={key} style={viewStyle} />;
      });
    };

    return (
      <View style={styles.container}>
        <ScrollView
          horizontal
          pagingEnabled
          showsHorizontalScrollIndicator={false}
          style={styles.pageContainer}
          contentContainerStyle={styles.pageContainerContent}
          onMomentumScrollEnd={handleMomentumScrollEnd}
        >
          {renderPages()}
        </ScrollView>
        <View style={styles.toolBar}>{renderToolBar()}</View>
      </View>
    );
  }
}

EABPageControl.propTypes = {
  updateAgentProfile: PropTypes.func.isRequired,
  pages: PropTypes.arrayOf(
    PropTypes.shape({
      key: PropTypes.string.isRequired,
      component: PropTypes.element.isRequired
    })
  ).isRequired
};

export default EABPageControl;
