import PropTypes from "prop-types";
import React, { Component } from "react";
import { Animated, Image, TouchableWithoutFeedback, View } from "react-native";

import styles from "./styles";

/**
 * <FloatingCard />
 * @description Floating Card component.
 * @param {object|number=} style - the floating card container custom style
 * @param {boolean} isSelected - indicates whether is selected or not
 * @param {boolean} hasBorder - indicates whether has border effect or not
 * @param {boolean} isFullBoxImageBackground - indicates whether it i sfull box image background
 * @param {function} onPress - function that when press the floating card
 * @param {boolean} disable - indicates whether it is disable or not
 * @param {string} children - the content
 * @param {string} image - the content
 * */
class EABFloatingCard extends Component {
  constructor() {
    super();

    this.state = {
      isOnPress: false,
      animateValue: new Animated.Value(0)
    };
  }

  get animatedViewStyles() {
    const { style, isSelected, hasBorder } = this.props;
    const { animateValue } = this.state;
    const { container } = styles;
    return [
      container,
      style,
      {
        transform: [
          {
            scale: animateValue.interpolate({
              inputRange: [0, 1],
              outputRange: [1, 0.95]
            })
          }
        ]
      },
      hasBorder && isSelected && styles.selectedColor,
      hasBorder && !isSelected && styles.unselectedColor
    ];
  }

  get contentViewStyle() {
    const { isFullBoxImageBackground } = this.props;
    const { contentFullBox, content } = styles;
    return isFullBoxImageBackground ? contentFullBox : content;
  }

  get imageStyle() {
    const { isFullBoxImageBackground } = this.props;
    const { imageBackgroundFullBox, imageBackground } = styles;
    return isFullBoxImageBackground ? imageBackgroundFullBox : imageBackground;
  }

  render() {
    /**
     * variables
     * */
    const { onPress, isSelected, disable, testID } = this.props;
    /**
     * functions
     * */
    const handleLongPressIn = () => {
      const { animateValue } = this.state;
      Animated.timing(animateValue, {
        toValue: 1,
        duration: 100,
        useNativeDriver: true
      }).start();
    };

    const handleLongPressOut = () => {
      const { isOnPress, animateValue } = this.state;
      if (!isOnPress) {
        Animated.timing(animateValue, {
          toValue: 0,
          duration: 100,
          useNativeDriver: true
        }).start();
      }
    };

    const renderContent = () => {
      const { children } = this.props;
      return <View style={this.contentViewStyle}>{children}</View>;
    };

    const renderContentImage = () => {
      const { image } = this.props;
      return <Image style={this.imageStyle} source={image} />;
    };

    return (
      <TouchableWithoutFeedback
        testID={testID}
        onPress={() => {
          if (!disable) onPress(!isSelected);
        }}
        onPressIn={!disable && handleLongPressIn}
        onPressOut={!disable && handleLongPressOut}
      >
        <Animated.View style={this.animatedViewStyles}>
          <View style={styles.contentContainer}>
            {renderContent()}
            {renderContentImage()}
          </View>
        </Animated.View>
      </TouchableWithoutFeedback>
    );
  }
}

EABFloatingCard.propTypes = {
  style: PropTypes.oneOfType([
    PropTypes.object,
    PropTypes.number,
    PropTypes.array
  ]),
  hasBorder: PropTypes.bool.isRequired,
  isSelected: PropTypes.bool.isRequired,
  isFullBoxImageBackground: PropTypes.bool,
  onPress: PropTypes.func.isRequired,
  children: PropTypes.node.isRequired,
  image: PropTypes.oneOfType([PropTypes.object, PropTypes.number]).isRequired,
  disable: PropTypes.bool.isRequired,
  testID: PropTypes.string
};

EABFloatingCard.defaultProps = {
  style: {},
  isFullBoxImageBackground: true,
  testID: ""
};

export default EABFloatingCard;
