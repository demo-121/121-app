import { ACTION_TYPES, REDUCER_TYPES } from "eab-web-api";
import { connect } from "react-redux";
import { TEXT_STORE } from "../../../constants/REDUCER_TYPES";
import QuickQuoteHistory from "../components/QuickQuoteHistory";

const { QUOTATION, OPTIONS_MAP, CLIENT } = REDUCER_TYPES;

/**
 * <QuickQuoteHistory />
 * @description quick quote history page.
 * @requires QuickQuoteHistory - QuickQuoteHistory UI
 * */
const mapStateToProps = state => ({
  textStore: state[TEXT_STORE],
  quickQuotes: state[QUOTATION].quickQuotes,
  optionsMap: state[OPTIONS_MAP],
  profile: state[CLIENT].profile
});

const mapDispatchToProps = dispatch => ({
  openProposal: ({ quotationId, callback }) => {
    dispatch({
      type: ACTION_TYPES[QUOTATION].SAGA_VIEW_PROPOSAL,
      quotationId,
      callback
    });
  },
  queryQuickQuote: callback => {
    dispatch({
      type: ACTION_TYPES[REDUCER_TYPES.QUOTATION].GET_QUICK_QUOTES,
      callback
    });
  },
  deleteQuickQuote: quotIds => {
    dispatch({
      type: ACTION_TYPES[QUOTATION].DELETE_QUICK_QUOTES,
      quotIds
    });
  },
  setAvoidNavigate: bool => {
    dispatch({
      type: ACTION_TYPES[QUOTATION].AVOID_NAVIGATE,
      bool
    });
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(QuickQuoteHistory);
