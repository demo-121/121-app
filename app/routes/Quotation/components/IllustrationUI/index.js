import React, { Component } from "react";
import {
  StyleSheet,
  Animated,
  View,
  ScrollView,
  PanResponder,
  Text
} from "react-native";
import * as _ from "lodash";
import {
  LANGUAGE_TYPES,
  REDUCER_TYPE_CHECK as WEB_API_TYPES_CHECK,
  REDUCER_TYPES as WEB_API_REDUCER_TYPE,
  utilities
} from "eab-web-api";
import EABButton from "../../../../components/EABButton";
import APP_REDUCER_TYPE_CHECK from "../../../../constants/REDUCER_TYPE_CHECK";
import { TEXT_STORE } from "../../../../constants/REDUCER_TYPES";
import Theme from "../../../../theme";
import EABAreaChart from "../../../../components/EABAreaChart";
import EABTable from "../../../../components/EABTable";
import EABTableSection from "../../../../components/EABTableSection";
import EABTableHeader from "../../../../components/EABTableHeader";
import TranslatedText from "../../../../containers/TranslatedText";
import EABi18n from "../../../../utilities/EABi18n";
import getEABTextByLangType from "../../../../utilities/getEABTextByLangType";
import styles from "./styles";

const { QUOTATION, PROPOSAL, OPTIONS_MAP } = WEB_API_REDUCER_TYPE;
const { getCurrencySign } = utilities.common;
const { numberToCurrency } = utilities.numberFormatter;

export default class IllustrationUI extends Component {
  constructor(props) {
    super(props);

    const { illustrateData, quotation } = props;

    const data = illustrateData[quotation.baseProductCode].illustration
      ? illustrateData[quotation.baseProductCode].illustration
      : illustrateData[quotation.baseProductCode];

    this.state = {
      age: data[0].age,
      left: new Animated.Value(0)
    };

    this.panResponder = PanResponder.create({
      onStartShouldSetPanResponder: () => true,
      onPanResponderGrant: e => {
        const { nativeEvent } = e;
        const age = Math.round(
          (nativeEvent.locationX / 880) * (this.maxAge - this.minAge) +
            this.minAge
        );

        if (nativeEvent.locationX > 0 && nativeEvent.locationX < 880) {
          const y =
            (age - this.minAge) * this.tableRowHeight -
            (this.tableHeight - this.tableRowHeight) / 2;

          if (y < 0) {
            this.scrollView.scrollTo({
              x: 0,
              y: 0
            });
          } else if (y > this.tableContentHeight - this.tableHeight) {
            this.scrollView.scrollTo({
              x: 0,
              y: this.tableContentHeight - this.tableHeight
            });
          } else {
            this.scrollView.scrollTo({
              x: 0,
              y
            });
          }

          Animated.event([
            {
              nativeEvent: {
                locationX: this.state.left
              }
            },
            null
          ])(e);

          if (this.state.age !== age) {
            this.setState({
              age
            });
          }
        }
      },

      onPanResponderMove: (e, gesture) => {
        const { nativeEvent } = e;

        if (nativeEvent.locationX > 0 && nativeEvent.locationX < 880) {
          const age = Math.round(
            (nativeEvent.locationX / 880) * (this.maxAge - this.minAge) +
              this.minAge
          );

          const y =
            (age - this.minAge) * this.tableRowHeight -
            (this.tableHeight - this.tableRowHeight) / 2;

          if (y < 0) {
            this.scrollView.scrollTo({
              x: 0,
              y: 0
            });
          } else if (y > this.tableContentHeight - this.tableHeight) {
            this.scrollView.scrollTo({
              x: 0,
              y: this.tableContentHeight - this.tableHeight
            });
          } else {
            this.scrollView.scrollTo({
              x: 0,
              y
            });
          }

          Animated.event([
            {
              nativeEvent: {
                locationX: this.state.left
              }
            },
            null
          ])(e, gesture);

          if (this.state.age !== age) {
            this.setState({
              age
            });
          }
        }
      }
    });

    this.renderTopSection = this.renderTopSection.bind(this);
    this.renderDataTable = this.renderDataTable.bind(this);
    this.renderChartAge = this.renderChartAge.bind(this);
  }

  componentDidMount() {
    const { navigation, quotation, planDetails, textStore } = this.props;

    const projectionRates = _.get(
      planDetails,
      `[${quotation.baseProductCode}].projection`,
      []
    );
    const rateOptions = _.map(projectionRates, pRate => {
      const rate = _.get(pRate, "title.en");
      let rateTitle;
      if (_.isNaN(Number(rate))) {
        if (Number(Object.keys(pRate.rates)[0])) {
          rateTitle = Number(Object.keys(pRate.rates)[0]).toFixed(2);
        } else {
          rateTitle = rate;
        }
      } else {
        rateTitle = Number(rate).toFixed(2);
      }

      return {
        value: rate,
        label: _.replace(
          EABi18n({
            path: "illustration.select.title",
            textStore,
            language: LANGUAGE_TYPES.ENGLISH
          }),
          "{1}",
          !_.isNumber(rateTitle) ? rateTitle : Number(rateTitle).toFixed(2)
        )
      };
    });
    const selectedRateValue = _.get(rateOptions, "[0].value");

    navigation.setParams({ rateOptions, selectedRateValue });
  }

  get minAge() {
    const { illustrateData, quotation } = this.props;
    const data = illustrateData[quotation.baseProductCode].illustration
      ? illustrateData[quotation.baseProductCode].illustration
      : illustrateData[quotation.baseProductCode];
    return _.isNumber(data[0].age) ? data[0].age : quotation.iAge + 1;
  }

  get maxAge() {
    const { illustrateData, quotation } = this.props;
    const data = illustrateData[quotation.baseProductCode].illustration
      ? illustrateData[quotation.baseProductCode].illustration
      : illustrateData[quotation.baseProductCode];
    return _.isNumber(data[data.length - 1].age)
      ? data[data.length - 1].age
      : quotation.iAge + data.length;
  }

  get sign() {
    const { quotation, optionsMap } = this.props;

    return getCurrencySign({
      compCode: quotation.compCode,
      ccy: quotation.ccy,
      optionsMap
    });
  }

  renderTopSection({ titles, data }) {
    const { sign } = this;
    const { illustrateData, quotation } = this.props;
    const { age } = this.state;
    const illustrationBasic = _.get(
      illustrateData,
      `[${quotation.baseProductCode}]`
    );
    let dataItemIndex = _.findIndex(
      illustrationBasic,
      item => age === item.age
    );
    if (dataItemIndex < 0) {
      dataItemIndex = age - quotation.iAge - 1;
    }
    const dataItem = data[dataItemIndex];

    const topSection = [];
    _.forEach(titles, (title, i) => {
      const checkSurrenderValueChartText =
        title.key === "columnTitle3"
          ? styles.SurrenderValueChartText
          : styles.PremiumPaidChartText;
      const checkDeathBenefitChartText =
        title.key === "columnTitle2"
          ? styles.DeathBenefitChartText
          : checkSurrenderValueChartText;
      if (i === 0) {
        // age
        topSection.push(
          <View style={styles.textContainer}>
            <Text style={styles.ageValue}>
              {dataItem[i]}
              <TranslatedText
                style={styles.ageText}
                path="illustration.chart.yearsOld"
              />
            </Text>
          </View>
        );
      } else {
        // values
        topSection.push(
          <View style={styles.textContainer}>
            <Text style={styles.chartValue}>
              {sign +
                numberToCurrency({
                  value: dataItem[i],
                  decimal: 0
                })}
            </Text>
            <Text style={checkDeathBenefitChartText}>
              {_.toUpper(title.text)}
            </Text>
          </View>
        );
      }
    });

    return topSection;
  }

  renderDataTable({ tableConfig, data }) {
    const { sign } = this;
    const { age } = this.state;
    return (
      <EABTable style={styles.dataTableContainer} tableConfig={tableConfig}>
        {data.map(items => {
          const dataContent = [];

          _.forEach(items, (item, index) => {
            if (index === 0) {
              dataContent.push(
                <View
                  key={`${index}`}
                  style={styles.columnContainer}
                  onLayout={e => {
                    this.tableRowHeight = e.nativeEvent.layout.height;
                  }}
                >
                  <EABButton
                    containerStyle={styles.rowItemFirst}
                    activeOpacity={1}
                    onPress={() => {
                      this.setState(
                        {
                          age: items[0]
                        },
                        Animated.timing(this.state.left, {
                          toValue:
                            ((items[0] - this.minAge) /
                              (this.maxAge - this.minAge)) *
                            880
                        }).start
                      );
                    }}
                  >
                    <Text>{item}</Text>
                  </EABButton>
                </View>
              );
            } else {
              dataContent.push(
                <View key={`${index}`} style={styles.columnContainer}>
                  <EABButton
                    containerStyle={styles.rowItem}
                    activeOpacity={1}
                    onPress={() => {
                      this.setState(
                        {
                          age: items[0]
                        },
                        Animated.timing(this.state.left, {
                          toValue:
                            ((items[0] - this.minAge) /
                              (this.maxAge - this.minAge)) *
                            880
                        }).start
                      );
                    }}
                  >
                    <Text>
                      {sign +
                        numberToCurrency({
                          value: item,
                          decimal: 0
                        })}
                    </Text>
                  </EABButton>
                </View>
              );
            }
          });
          return (
            <EABTableSection
              key={`${items[0]}`}
              style={{
                ...(age === items[0]
                  ? { backgroundColor: Theme.brandSuperTransparent }
                  : {})
              }}
            >
              {dataContent}
            </EABTableSection>
          );
        })}
      </EABTable>
    );
  }

  renderChartAge({ data }) {
    const { illustrateData, quotation } = this.props;
    const { age } = this.state;
    const illustrationBasic = _.get(
      illustrateData,
      `[${quotation.baseProductCode}]`
    );
    let ageItemIndex = _.findIndex(
      illustrationBasic,
      ageItem => age === ageItem.age
    );
    if (ageItemIndex < 0) {
      ageItemIndex = age - quotation.iAge - 1;
    }
    const ageDataItem = data[ageItemIndex];
    const ageDataArray = [];

    _.forEach(data, (title, i) => {
      if (i === 0) {
        ageDataArray.push(
          <View key={title} style={styles.ageDataArray}>
            <Text style={styles.ageDataArrayText}>{ageDataItem[i]}</Text>
          </View>
        );
      }
    });

    return ageDataArray;
  }

  render() {
    const {
      navigation,
      textStore,
      illustrateData,
      quotation,
      planDetails
    } = this.props;
    const { left } = this.state;
    const illustrationBasic = _.get(
      illustrateData,
      `[${quotation.baseProductCode}]`
    );
    const selectedRateValue = navigation.getParam("selectedRateValue");
    const { illustrationColumns } = planDetails[quotation.baseProductCode];

    // prepare title
    const titles = [
      {
        key: "age",
        text: EABi18n({
          path: "illustration.chart.lifeAssuredAge",
          textStore,
          language: LANGUAGE_TYPES.ENGLISH
        })
      }
    ];

    _.forEach(illustrationColumns, (col, i) => {
      titles.push({
        key: `columnTitle${i + 1}`,
        text: getEABTextByLangType({
          data: col.title,
          language: LANGUAGE_TYPES.ENGLISH
        })
      });
    });

    // prepare data
    let maxGrid = 0;
    const data = [];
    const illustration = _.get(illustrationBasic, "illustration");
    const illustrationArray = _.isArray(illustration)
      ? illustration
      : illustrationBasic;

    _.forEach(illustrationArray, (iData, i) => {
      const columns = [];
      if (_.isNumber(iData.age)) {
        columns.push(iData.age);
      } else {
        columns.push(quotation.iAge + i + 1);
      }
      _.forEach(illustrationColumns, iColumn => {
        const { id, isRelatedToProjectionRate } = iColumn;
        if (_.isArray(illustration)) {
          if (iData[id]) {
            let columnValue = 0;
            if (isRelatedToProjectionRate === "Y") {
              if (selectedRateValue === "4") {
                columnValue = _.get(iData, `[${id}][0]`);
              } else {
                columnValue = _.get(iData, `[${id}][1]`);
              }
            } else {
              columnValue = iData[id];
            }
            columns.push(Math.floor(columnValue));
          }
        } else {
          const columnValue =
            isRelatedToProjectionRate === "Y"
              ? iData[id][selectedRateValue]
              : iData[id];
          columns.push(Math.floor(columnValue));
        }
      });
      data.push(columns);
      maxGrid = _.max(_.concat([maxGrid], columns));
    });

    // prepare tableConfig
    const tableConfig = [];
    _.forEach(titles, (title, i) => {
      tableConfig.push({
        key: title.key,
        style: i === 0 ? styles.firstHeaderItem : styles.headerItem
      });
    });

    // prepare chart config
    const chartSVG = [
      { fill: "rgba(0,0,143,0.1)", stroke: "rgb(0,0,143)" },
      {
        fill: "rgba(28,197,78,0.1)",
        stroke: "rgb(28,197,78)"
      },
      {
        fill: "rgba(240,118,98,0.1)",
        stroke: "rgb(240,118,98)"
      }
    ];
    const chart = [];
    _.forEach(chartSVG, (svg, i) => {
      chart.push({
        style: StyleSheet.absoluteFill,
        gridMax: maxGrid,
        gridMin: 0,
        yAccessor: ({ item }) => item[i + 1],
        xAccessor: ({ item }) => item[0],
        svg: chartSVG[i]
      });
    });

    return !_.isEmpty(selectedRateValue) ? (
      <View style={styles.rootView}>
        <View style={styles.topSection}>
          {this.renderTopSection({ titles, data })}
        </View>
        <View {...this.panResponder.panHandlers} style={styles.graphContainer}>
          <Animated.View style={[styles.playhead, { left }]}>
            <Animated.View style={[styles.ageDataArrayNotice]}>
              {this.renderChartAge({ data })}
            </Animated.View>
          </Animated.View>
          <EABAreaChart data={data} chart={chart} />
        </View>
        <EABTable style={styles.headerTableContainer} tableConfig={tableConfig}>
          <EABTableHeader key="header">
            {_.map(titles, title => (
              <View key={title.key}>
                <Text style={Theme.captionSecondary}>
                  {title.text.toUpperCase()}
                </Text>
              </View>
            ))}
          </EABTableHeader>
        </EABTable>
        <ScrollView
          ref={scrollView => {
            this.scrollView = scrollView;
          }}
          onLayout={e => {
            this.tableHeight = e.nativeEvent.layout.height;
          }}
          onContentSizeChange={(contentWidth, contentHeight) => {
            this.tableContentHeight = contentHeight;
          }}
        >
          {this.renderDataTable({ tableConfig, data })}
        </ScrollView>
      </View>
    ) : null;
  }
}

IllustrationUI.propTypes = {
  textStore: APP_REDUCER_TYPE_CHECK[TEXT_STORE].isRequired,
  quotation: WEB_API_TYPES_CHECK[QUOTATION].quotation.isRequired,
  illustrateData: WEB_API_TYPES_CHECK[PROPOSAL].illustrateData.isRequired,
  planDetails: WEB_API_TYPES_CHECK[QUOTATION].planDetails.isRequired,
  optionsMap: WEB_API_TYPES_CHECK[OPTIONS_MAP].isRequired
};
