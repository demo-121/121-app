import React, { PureComponent } from "react";
import { View, ScrollView, Image } from "react-native";
import LinearGradient from "react-native-linear-gradient";
import PropTypes from "prop-types";
import * as _ from "lodash";
import { REDUCER_TYPES, REDUCER_TYPE_CHECK } from "eab-web-api";

import icNext from "../../../../assets/images/icNext.png";
import icPrevious from "../../../../assets/images/icPrevious.png";
import icCircleArrowDown from "../../../../assets/images/icCircleArrowDown.png";
import APP_REDUCER_TYPE_CHECK from "../../../../constants/REDUCER_TYPE_CHECK";
import { TEXT_STORE } from "../../../../constants/REDUCER_TYPES";
import EABBottomSheet from "../../../../components/EABBottomSheet";
import EABButton from "../../../../components/EABButton";
import {
  RECTANGLE_BUTTON_1,
  RECTANGLE_BUTTON_2
} from "../../../../components/EABButton/constants";
import EASEProgressTabs from "../../../../components/EASEProgressTabs";
import EABi18n from "../../../../utilities/EABi18n";
import TranslatedText from "../../../../containers/TranslatedText";

import { ContextConsumer } from "../../../../context";
import Recommendation from "../../containers/Recommendation";
import Budget from "../../containers/Budget";
import Acceptance from "../../containers/Acceptance";
import ApplicationItemDetails from "../../containers/ApplicationItemDetails";
import ApplicationItemShieldDetails from "../../containers/ApplicationItemShieldDetails";

import Theme from "../../../../theme";
import styles from "./styles";
import {
  BOTTOM_SHEET_COLLAPSE_HEIGHT,
  BOTTOM_SHEET_STEP_EXPAND_HEIGHT,
  BOTTOM_SHEET_PLAN_DETAILS_EXPAND_HEIGHT
} from "./constants";

const { PRE_APPLICATION } = REDUCER_TYPES;

/**
 * RecommendationScreen
 * @description RecommendationScreen page.
 * @param {object} textStore - reducer textStore state
 * @param {number} currentStep - indicate current step of Recommendation Screen page
 * @param {number} completedStep - indicate completed step of Recommendation Screen page
 * @param {boolean} showDetails - indicate the Details footer is shown
 * @param {object} detailsValue - value in Details footer
 * @param {func} closeDialog - action to close dialog
 * @param {func} closeRecommendation - action when closing Recommendation Screen page before closing dialog
 * @param {func} hideRecommendationDetails - action after Hide Details is clicked
 * @param {func} updateRecommendationStep - action to update step in Recommendation Screen page
 * */

class RecommendationScreen extends PureComponent {
  constructor(props) {
    super(props);

    this.renderTitleBar = this.renderTitleBar.bind(this);
    this.renderForm = this.renderForm.bind(this);
    this.renderFooter = this.renderFooter.bind(this);

    this.state = {};
  }

  renderTitleBar() {
    const { closeDialog, closeRecommendation } = this.props;
    return (
      <View style={styles.titleBar}>
        <EABButton
          testID="PreAppilcation__Recommendation__btnSave"
          containerStyle={[Theme.headerLeft, { left: Theme.alignmentXL }]}
          onPress={() => {
            closeRecommendation();
            closeDialog();
          }}
        >
          <TranslatedText
            style={styles.titleBarButtonTitle}
            path="button.save"
          />
        </EABButton>
        <View style={styles.titleBarViewLeft} />
        <View style={styles.titleBarViewMiddle}>
          <TranslatedText
            style={styles.titleBarTitle}
            path="recommendation.title"
          />
        </View>
        <View style={styles.titleBarViewRight}>
          <EABButton
            testID="PreAppilcation__Recommendation__btnDone"
            style={styles.titleBarButtonRight}
            onPress={() => {
              closeRecommendation();
              closeDialog();
            }}
          >
            <TranslatedText
              style={styles.titleBarButtonTitle}
              path="button.done"
            />
          </EABButton>
        </View>
      </View>
    );
  }

  renderForm() {
    const { currentStep } = this.props;

    if (currentStep === 0) {
      return <Recommendation />;
    } else if (currentStep === 1) {
      return <Budget />;
    }
    return <Acceptance />;
  }

  renderFooter(language, steps) {
    const {
      currentStep,
      completedStep,
      showDetails,
      detailsValue,
      hideRecommendationDetails,
      updateRecommendationStep
    } = this.props;

    return showDetails ? (
      <EABBottomSheet
        containerStyle={styles.bottomSheetContainer}
        style={styles.bottomSheetView}
        isExpand={showDetails}
        expandHeight={BOTTOM_SHEET_PLAN_DETAILS_EXPAND_HEIGHT}
        collapseHeight={BOTTOM_SHEET_COLLAPSE_HEIGHT}
      >
        <View style={styles.planDetailsContentView}>
          <View style={styles.planDetailsControlView}>
            <EABButton
              style={styles.planDetailsButton}
              onPress={() => {
                hideRecommendationDetails();
              }}
            >
              <Image source={icCircleArrowDown} />
              <TranslatedText
                style={styles.planDetailsButtonTitle}
                path="button.hideDetails"
              />
            </EABButton>
          </View>
          <ScrollView
            style={styles.scrollView}
            contentContainerStyle={styles.scrollViewContent}
            showsVerticalScrollIndicator={false}
          >
            <View style={styles.planDetailsView}>
              {detailsValue.quotType === "SHIELD" ? (
                <ApplicationItemShieldDetails value={detailsValue} />
              ) : (
                <ApplicationItemDetails value={detailsValue} />
              )}
            </View>
          </ScrollView>
        </View>
      </EABBottomSheet>
    ) : (
      <EABBottomSheet
        containerStyle={styles.bottomSheetContainer}
        style={styles.bottomSheetStepView}
        isExpand={!showDetails}
        expandHeight={BOTTOM_SHEET_STEP_EXPAND_HEIGHT}
        collapseHeight={BOTTOM_SHEET_COLLAPSE_HEIGHT}
      >
        {currentStep === 0 ? (
          <View />
        ) : (
          <EABButton
            style={styles.stepButtonLeft}
            buttonType={RECTANGLE_BUTTON_2}
            onPress={() => {
              updateRecommendationStep(currentStep - 1);
            }}
          >
            <Image style={Theme.iconStyle} source={icPrevious} />
            <TranslatedText style={Theme.title} path="button.previous" />
          </EABButton>
        )}
        {currentStep === steps.length - 1 ? (
          <View />
        ) : (
          <EABButton
            testID="PreAppilcation__Recommendation__btnNext"
            style={styles.stepButtonRight}
            buttonType={RECTANGLE_BUTTON_1}
            isDisable={currentStep > completedStep}
            onPress={() => {
              updateRecommendationStep(currentStep + 1);
            }}
          >
            <TranslatedText style={Theme.title} path="button.next" />
            <Image style={Theme.iconStyle} source={icNext} />
          </EABButton>
        )}
      </EABBottomSheet>
    );
  }

  render() {
    const {
      currentStep,
      completedStep,
      textStore,
      updateRecommendationStep
    } = this.props;

    return (
      <ContextConsumer>
        {({ language }) => {
          const steps = [
            {
              key: "step_recommendation",
              testID: "Recommendation",
              title: _.toUpper(
                EABi18n({
                  path: "recommendation.step.recommendation",
                  language,
                  textStore
                })
              ),
              canPress: true,
              onPress: () => {
                updateRecommendationStep(0);
              },
              isCompleted: completedStep >= 0
            },
            {
              key: "step_budget",
              testID: "Budget",
              title: _.toUpper(
                EABi18n({
                  path: "recommendation.step.budget",
                  language,
                  textStore
                })
              ),
              canPress: completedStep >= 0,
              onPress: () => {
                updateRecommendationStep(1);
              },
              isCompleted: completedStep >= 1
            },
            {
              key: "step_acceptance",
              testID: "ClientAcceptance",
              title: _.toUpper(
                EABi18n({
                  path: "recommendation.step.acceptance",
                  language,
                  textStore
                })
              ),
              canPress: completedStep >= 1,
              onPress: () => {
                updateRecommendationStep(2);
              },
              isCompleted: completedStep >= 2
            }
          ];

          return (
            <LinearGradient
              {...Theme.LinearGradientProps}
              style={styles.container}
            >
              <View style={styles.header}>
                {this.renderTitleBar()}
                <EASEProgressTabs
                  testID="PreAppilcation__Recommendation"
                  steps={steps}
                  currentStepIndex={currentStep}
                />
              </View>
              <View style={styles.KeyboardAvoidingViewWrapper}>
                {this.renderForm()}
              </View>
              {this.renderFooter(language, steps)}
            </LinearGradient>
          );
        }}
      </ContextConsumer>
    );
  }
}

RecommendationScreen.propTypes = {
  textStore: APP_REDUCER_TYPE_CHECK[TEXT_STORE].isRequired,
  currentStep: PropTypes.number.isRequired,
  completedStep: PropTypes.number.isRequired,
  showDetails: PropTypes.bool.isRequired,
  detailsValue: PropTypes.oneOfType([
    REDUCER_TYPE_CHECK[PRE_APPLICATION].ApplicationItem.value.isRequired,
    PropTypes.shape({})
  ]),
  closeDialog: PropTypes.func.isRequired,
  closeRecommendation: PropTypes.func.isRequired,
  hideRecommendationDetails: PropTypes.func.isRequired,
  updateRecommendationStep: PropTypes.func.isRequired
};

RecommendationScreen.defaultProps = {
  detailsValue: {}
};

export default RecommendationScreen;
