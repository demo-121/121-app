import PropTypes from "prop-types";
import React, { Component } from "react";
import { Image, Text, TouchableOpacity, View } from "react-native";
import icChipAdd from "../../assets/images/icChipAdd.png";
import icChipSelected from "../../assets/images/icChipSelected.png";
import Theme from "../../theme";
import getStyleArray from "../../utilities/getStyleArray";
import styles from "./styles";

/**
 * <EABChip />
 * @description chip component
 * @requires icChipSelected - @/assets/images/icChipSelected.png
 * @requires icChipAdd - @/assets/images/icChipAdd.png
 * @param {string} title - chip title
 * @param {boolean} isSelected - indicates whether the chip is selected or not
 * @param {function} onPress - the function that will be fire by the onPress
 *   event
 * @param {boolean} [isIconChip=false] - indicates whether the chip have icon
 *   or not
 * @param {number} [activeOpacity=.5] - the opacity value when the chip is
 *   pressed
 * @param {string} [unselectedColor=brandTransparent] - the chip's color when
 *   it is unselected
 * @param {string} [selectedColor=brand] - the chip's color when it is selected
 * @param {object|array|number} [style={}] - the chip style
 * @param {object|array|number} [containerStyle={}] - the chip's container style
 * */
class EABChip extends Component {
  get chipStyle() {
    const { isIconChip } = this.props;
    const { chipContainer, chipContainerIcon } = styles;
    return isIconChip ? chipContainerIcon : chipContainer;
  }

  get chipContainerStyles() {
    const { isSelected, selectedColor, unselectedColor, style } = this.props;
    const backgroundColor = isSelected ? selectedColor : unselectedColor;
    return [this.chipStyle, getStyleArray(style), { backgroundColor }];
  }

  get textIconStyle() {
    const { isIconChip } = this.props;
    const { textIcon } = styles;
    return isIconChip ? textIcon : Theme.chipLabelBrand;
  }

  get chipTextStyle() {
    const { isSelected, selectedColor } = this.props;
    const color = isSelected ? Theme.white : selectedColor;
    return [this.textIconStyle, { color }];
  }

  render() {
    /**
     * variables
     * */
    const {
      title,
      isIconChip,
      onPress,
      isSelected,
      activeOpacity,
      testID
    } = this.props;
    /**
     * functions
     * */
    const renderChipText = () => (
      <Text style={this.chipTextStyle}>{title}</Text>
    );

    const renderChipIcon = () => {
      if (!isIconChip) {
        return null;
      }
      const source = isSelected ? icChipSelected : icChipAdd;
      const { image } = styles;
      return <Image style={image} source={source} />;
    };

    return (
      <TouchableOpacity
        testID={testID}
        style={getStyleArray(this.props.containerStyle)}
        activeOpacity={activeOpacity}
        onPress={() => onPress(!isSelected)}
      >
        <View style={this.chipContainerStyles}>
          {renderChipText()}
          {renderChipIcon()}
        </View>
      </TouchableOpacity>
    );
  }
}

EABChip.propTypes = {
  title: PropTypes.string.isRequired,
  isSelected: PropTypes.bool.isRequired,
  isIconChip: PropTypes.bool,
  onPress: PropTypes.func.isRequired,
  activeOpacity: PropTypes.number,
  unselectedColor: PropTypes.string,
  selectedColor: PropTypes.string,
  containerStyle: PropTypes.oneOfType([
    PropTypes.object,
    PropTypes.array,
    PropTypes.number
  ]),
  style: PropTypes.oneOfType([
    PropTypes.object,
    PropTypes.array,
    PropTypes.number
  ]),
  testID: PropTypes.string
};

EABChip.defaultProps = {
  isIconChip: false,
  unselectedColor: Theme.brandTransparent,
  selectedColor: Theme.brand,
  activeOpacity: 0.5,
  containerStyle: {},
  style: {},
  testID: ""
};

export default EABChip;
