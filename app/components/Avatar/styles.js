import { StyleSheet } from "react-native";
import Theme from "../../theme";

export default StyleSheet.create({
  clientPhotoWrapper: {
    borderRadius: Theme.radiusL,
    backgroundColor: Theme.brandTransparent,
    flexBasis: "auto",
    justifyContent: "center",
    alignItems: "center",
    overflow: "hidden"
  },
  clientIconInner: {
    height: 50,
    width: 50,
    justifyContent: "center",
    alignItems: "center",
    marginVertical: Theme.alignmentL,
    marginHorizontal: Theme.alignmentXL,
    backgroundColor: Theme.brandTransparent,
    borderRadius: 25
  },
  clientIconText: {
    color: Theme.white,
    fontSize: Theme.fontSizeXXXM,
    fontWeight: "bold"
  }
});
