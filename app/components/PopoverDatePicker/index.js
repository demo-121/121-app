import { Platform } from "react-native";
import PopoverDatePicker from "./PopoverDatePicker.ios";

export default Platform.select({
  ios: PopoverDatePicker,
  android: {}
});
