import moment from "moment";
import { utilities } from "eab-web-api";
import { languageTypeConvert } from "./getEABTextByLangType";
import EABi18n from "./EABi18n";
import icCall from "../assets/images/icCall.png";
import icEmail from "../assets/images/icEmail.png";
import icDirections from "../assets/images/icDirections.png";
import { ADDRESS, EMAIL, PHONENUMBER } from "../constants/PROFILE_DATA_TYPES";

const { common, trigger, numberFormatter } = utilities;
const { getOptionTitle } = common;
const { optionCondition } = trigger;
const { numberToCurrency } = numberFormatter;

export default function({
  language,
  profile,
  optionsMap,
  dependantProfiles,
  textStore
}) {
  const getAddressText = () => {
    const {
      addrBlock,
      addrEstate,
      unitNum,
      addrStreet,
      addrCountry,
      addrCity,
      residenceCountry,
      postalCode
    } = profile;

    const unitNumText = unitNum ? `${unitNum}, ` : "";
    const addrBlockText = addrBlock ? `${addrBlock}, ` : "";
    const addrEstateText = addrEstate ? `${addrEstate},` : "";
    const addrStreetText = addrStreet ? `${addrStreet},` : "";
    const addrPostalCodeText = postalCode ? `${postalCode},` : "";
    const firstLine = addrPostalCodeText ? `${addrPostalCodeText}\n` : "";
    const secondLine = addrBlockText ? `${addrBlockText}\n` : "";
    const thirdLine = addrStreetText ? `${addrStreetText}\n` : "";
    const fourthLine = unitNumText ? `${unitNumText}\n` : "";
    const fifthLine = addrEstateText ? `${addrEstateText}\n` : "";

    let cityData = "";
    if (addrCity) {
      cityData = optionCondition({
        value: residenceCountry,
        options: "city",
        optionsMap,
        showIfNoValue: false
      })
        ? `${getOptionTitle({
            value: addrCity,
            optionsMap: optionsMap.city,
            language: languageTypeConvert(language)
          })}, `
        : `${addrCity}, `;
    }
    const countryData =
      getOptionTitle({
        value: addrCountry,
        optionsMap: optionsMap.residency,
        language: languageTypeConvert(language)
      }) || "";
    let sixthLine = `${cityData}${countryData}\n`;
    if (sixthLine !== "") {
      sixthLine = `${sixthLine}`;
    }

    return (
      firstLine + secondLine + thirdLine + fourthLine + fifthLine + sixthLine
    );
  };

  /* get familyMembers section */
  const familyMembers = [];
  if (profile.dependants) {
    profile.dependants.forEach(member => {
      const memberProfile = dependantProfiles[member.cid];

      if (memberProfile) {
        familyMembers.push({
          key: memberProfile.cid,
          relationship: getOptionTitle({
            value: member.relationship,
            optionsMap: optionsMap.relationship,
            language: languageTypeConvert(language)
          }),
          name: memberProfile.fullName,
          photo: memberProfile.photo
        });
      }
    });
  }

  /* show Language Spoken */
  const displayLanguage = profile.language
    ? profile.language
        .split(",")
        .map(item =>
          getOptionTitle({
            value: item,
            optionsMap: optionsMap.language,
            language: languageTypeConvert(language)
          })
        )
        .join(",")
    : "";

  return {
    familyMembers,
    fullName: profile.fullName,
    photo: profile.photo,
    trustedIndividuals: profile.trustedIndividuals
      ? {
          photo: profile.trustedIndividuals.tiPhoto,
          fullName: profile.trustedIndividuals.fullName,
          idDocType:
            profile.trustedIndividuals.idDocType === "other" &&
            profile.trustedIndividuals.idDocTypeOther !== ""
              ? profile.trustedIndividuals.idDocTypeOther
              : getOptionTitle({
                  value: profile.trustedIndividuals.idDocType,
                  optionsMap: optionsMap.idDocType,
                  language: languageTypeConvert(language)
                }),
          idCardNo: profile.trustedIndividuals.idCardNo,
          relationship:
            profile.trustedIndividuals.relationship === "OTH" &&
            profile.trustedIndividuals.relationshipOther !== ""
              ? profile.trustedIndividuals.relationshipOther
              : getOptionTitle({
                  value: profile.trustedIndividuals.relationship,
                  optionsMap: optionsMap.relationship,
                  language: languageTypeConvert(language)
                })
        }
      : null,
    leftSection: [
      {
        id: "a",
        title: EABi18n({
          textStore,
          language,
          path: "clientProfile.personalInformation.title"
        }).toUpperCase(),
        items: [
          {
            id: "a",
            title: EABi18n({
              textStore,
              language,
              path: "clientProfile.personalInformation.gender"
            }),
            value: getOptionTitle({
              value: profile.gender,
              optionsMap: {
                type: "layout",
                options: [
                  {
                    title: {
                      en: "Male"
                    },
                    value: "M"
                  },
                  {
                    title: {
                      en: "Female"
                    },
                    value: "F"
                  }
                ]
              },
              language: languageTypeConvert(language)
            }),
            shouldShow: true
          },
          {
            id: "b",
            title: EABi18n({
              textStore,
              language,
              path: "clientProfile.personalInformation.dateOfBirth"
            }),
            value: moment(profile.dob, "YYYY-MM-DD").isValid()
              ? `${moment(profile.dob, "YYYY-MM-DD").format("DD/MM/YYYY")}(${
                  profile.age
                })`
              : "",
            shouldShow: true
          },
          {
            id: "c",
            title: EABi18n({
              textStore,
              language,
              path: "clientProfile.personalInformation.nationality"
            }),
            value: getOptionTitle({
              value: profile.nationality,
              optionsMap: optionsMap.nationality,
              language: languageTypeConvert(language)
            }),
            shouldShow: true
          },
          {
            id: "j",
            title: EABi18n({
              textStore,
              language,
              path: "clientProfile.personalInformation.PRStatus"
            }),
            value: getOptionTitle({
              value: profile.prStatus,
              optionsMap: {
                type: "layout",
                options: [
                  {
                    title: {
                      en: "Yes"
                    },
                    value: "Y"
                  },
                  {
                    title: {
                      en: "No"
                    },
                    value: "N"
                  }
                ]
              },
              language: languageTypeConvert(language)
            }),
            shouldShow:
              profile.nationality !== "" && profile.nationality !== "N1"
          },
          {
            id: "d",
            title: EABi18n({
              textStore,
              language,
              path: "clientProfile.personalInformation.countryOfResidence"
            }),
            value: getOptionTitle({
              value: profile.residenceCountry,
              optionsMap: optionsMap.residency,
              language: languageTypeConvert(language)
            }),
            shouldShow: true
          },
          {
            id: "city state",
            title: EABi18n({
              textStore,
              language,
              path: "clientProfile.personalInformation.cityState"
            }),
            value: getOptionTitle({
              value: profile.residenceCity,
              optionsMap: optionsMap.city,
              language: languageTypeConvert(language)
            }),
            shouldShow: optionCondition({
              value: profile.residenceCountry,
              options: "city",
              optionsMap,
              showIfNoValue: false
            })
          },
          {
            id: "e",
            title: EABi18n({
              textStore,
              language,
              path: "clientProfile.personalInformation.idDocType"
            }),
            value:
              profile.idDocType === "other"
                ? profile.idDocTypeOther
                : getOptionTitle({
                    value: profile.idDocType,
                    optionsMap: optionsMap.idDocType,
                    language: languageTypeConvert(language)
                  }),
            shouldShow: true
          },
          {
            id: "f",
            title: EABi18n({
              textStore,
              language,
              path: "clientProfile.personalInformation.idCardNo"
            }),
            value: profile.idCardNo,
            shouldShow: true
          },
          {
            id: "g",
            title: EABi18n({
              textStore,
              language,
              path: "clientProfile.personalInformation.smokingHabit"
            }),
            value: getOptionTitle({
              value: profile.isSmoker,
              optionsMap: {
                type: "layout",
                options: [
                  {
                    title: {
                      en: "Non-smoker"
                    },
                    value: "N"
                  },
                  {
                    title: {
                      en: "Smoker"
                    },
                    value: "Y"
                  }
                ]
              },
              language: languageTypeConvert(language)
            }),
            shouldShow: true
          },
          {
            id: "i",
            title: EABi18n({
              textStore,
              language,
              path: "clientProfile.personalInformation.maritalStatus"
            }),
            value: getOptionTitle({
              value: profile.marital,
              optionsMap: optionsMap.marital,
              language: languageTypeConvert(language)
            }),
            shouldShow: true
          },
          {
            id: "h",
            title: EABi18n({
              textStore,
              language,
              path: "clientProfile.personalInformation.language"
            }),
            // value: profile.language ? profile.language : ""
            value: profile.languageOther
              ? displayLanguage.replace("Others", profile.languageOther)
              : displayLanguage,
            shouldShow: true
          }
        ]
      },
      {
        id: "b",
        title: EABi18n({
          textStore,
          language,
          path: "clientProfile.workEducation.title"
        }).toUpperCase(),
        items: [
          {
            id: "a",
            title: EABi18n({
              textStore,
              language,
              path: "clientProfile.workEducation.education"
            }),
            value: getOptionTitle({
              value: profile.education,
              optionsMap: optionsMap.education,
              language: languageTypeConvert(language)
            }),
            shouldShow: true
          },
          {
            id: "b",
            title: EABi18n({
              textStore,
              language,
              path: "clientProfile.workEducation.employStatus"
            }),
            value:
              profile.employStatus === "ot"
                ? profile.employStatusOther
                : getOptionTitle({
                    value: profile.employStatus,
                    optionsMap: optionsMap.employStatus,
                    language: languageTypeConvert(language)
                  }),
            shouldShow: true
          },
          {
            id: "c",
            title: EABi18n({
              textStore,
              language,
              path: "clientProfile.workEducation.occupation"
            }),
            value: getOptionTitle({
              value: profile.occupation,
              optionsMap: optionsMap.occupation,
              language: languageTypeConvert(language)
            }),
            shouldShow: true
          },
          {
            id: "d",
            title: EABi18n({
              textStore,
              language,
              path: "clientProfile.workEducation.industry"
            }),
            value: getOptionTitle({
              value: profile.industry,
              optionsMap: optionsMap.industry,
              language: languageTypeConvert(language)
            }),
            shouldShow: true
          },
          {
            id: "e",
            title: EABi18n({
              textStore,
              language,
              path: "clientProfile.workEducation.nameOfEmployBusinessSchool"
            }),
            value: profile.organization,
            shouldShow: true
          },
          {
            id: "f",
            title: EABi18n({
              textStore,
              language,
              path: "clientProfile.workEducation.countryOfEmployBusinessSchool"
            }),
            value:
              profile.organizationCountry === "N/A"
                ? "N/A"
                : getOptionTitle({
                    value: profile.organizationCountry,
                    optionsMap: optionsMap.residency,
                    language: languageTypeConvert(language)
                  }),
            shouldShow: true
          },
          {
            id: "g",
            title: EABi18n({
              textStore,
              language,
              path: "clientProfile.workEducation.typeOfPass"
            }),
            value: getOptionTitle({
              value: profile.pass,
              optionsMap: optionsMap.pass,
              language: languageTypeConvert(language)
            }),
            shouldShow:
              profile.nationality &&
              profile.nationality !== "N1" &&
              profile.nationality !== "N2" &&
              profile.prStatus &&
              profile.prStatus !== "Y"
          },
          {
            id: "h",
            title: EABi18n({
              textStore,
              language,
              path: "clientProfile.workEducation.passExpiryDate"
            }),
            value:
              moment(new Date(profile.passExpDate)).isValid() &&
              profile.passExpDate !== null
                ? `${moment(new Date(profile.passExpDate), "YYYY-MM-DD").format(
                    "DD/MM/YYYY"
                  )}`
                : "",
            shouldShow:
              profile.nationality &&
              profile.nationality !== "N1" &&
              profile.nationality !== "N2" &&
              profile.prStatus &&
              profile.prStatus !== "Y"
          },
          {
            id: "i",
            title: EABi18n({
              textStore,
              language,
              path: "clientProfile.workEducation.income"
            }),
            value:
              profile.allowance >= 0
                ? `$${numberToCurrency({
                    value: profile.allowance,
                    decimal: 0
                  })}`
                : "",
            shouldShow: true
          }
        ]
      }
    ],
    rightSection: [
      {
        id: "a",
        title: EABi18n({
          textStore,
          language,
          path: "clientProfile.contact.title"
        }).toUpperCase(),
        items: [
          {
            id: "a",
            icon: icCall,
            title: EABi18n({
              textStore,
              language,
              path: "clientProfile.contact.mobile"
            }),
            value:
              profile.mobileNo !== ""
                ? `${profile.mobileCountryCode} ${profile.mobileNo}`
                : "-",
            type: PHONENUMBER
          },
          {
            id: "b",
            icon: icCall,
            title: EABi18n({
              textStore,
              language,
              path: "clientProfile.contact.otherMobile"
            }),
            value:
              profile.otherNo !== ""
                ? `${profile.otherMobileCountryCode} ${profile.otherNo}`
                : "-",
            type: PHONENUMBER
          },
          {
            id: "d",
            icon: icEmail,
            title: EABi18n({
              textStore,
              language,
              path: "clientProfile.contact.email"
            }),
            value: profile.email,
            type: EMAIL
          },
          {
            id: "e",
            icon: icDirections,
            title: EABi18n({
              textStore,
              language,
              path: "clientProfile.contact.homeAddress"
            }),
            value: getAddressText(language),
            type: ADDRESS
          }
        ]
      }
    ]
  };
}
