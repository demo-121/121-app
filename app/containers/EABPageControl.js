import { connect } from "react-redux";
import EABPageControl from "../components/EABPageControl";
import APP_ACTION_TYPES from "../constants/ACTION_TYPES";
import { LOGIN } from "../constants/REDUCER_TYPES";

/**
 * <EASEAssets />
 * @requires EASEAssets - EASEAssets UI
 * alertFunction in the postalOnBlur -- for the alert box message popup
 * */
const mapStateToProps = () => ({});

const mapDispatchToProps = dispatch => ({
  updateAgentProfile() {
    dispatch({
      type: APP_ACTION_TYPES[LOGIN].UPDATE_AGENT_PROFILE
    });
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(EABPageControl);
