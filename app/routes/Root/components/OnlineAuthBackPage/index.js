import PropTypes from "prop-types";
import React, { PureComponent } from "react";
import {
  Alert,
  View,
  Image,
  NativeModules,
  NativeEventEmitter
} from "react-native";
import axaLogo from "../../../../assets/images/axaLogo.png";
import { dataSync as dataSyncImport } from "../../../../utilities/DataManager";
import {
  TYPE_FIRST_WARNING,
  TYPE_SECOND_WARNING,
  TYPE_TIME_BOMB
} from "../../../../constants/TIME_BOMB";
import styles from "./styles";
import TranslatedText from "../../../../containers/TranslatedText";

/**
 * <AppPassword />
 * @param {function} verifyOnlineAuth - method for verifying 121 Online
 * */
export default class OnlineAuthBackPage extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      timeBombType: null
    };
    const dataSync = new NativeEventEmitter(NativeModules.DataSyncEventManager);
    this.dataSyncListener = dataSync.addListener(
      "DataSyncEventHandler",
      data => {
        if (data === "SUCCESS") {
          this.props.setLastDataSyncTime();
          if (this.isDone) {
            if (
              this.props.agent.role !== "04" &&
              !this.props.agent.managerCode &&
              this.props.agent.rawData &&
              this.props.agent.rawData.upline1Code &&
              this.props.agent.agentCode !==
                this.props.agent.rawData.upline1Code
            ) {
              setTimeout(() => {
                Alert.alert(
                  `[Error] You won't be able to submit case as there is no supervisor information linked to you. Please contact your firm's administrator.`
                );
              }, 200);
              this.isDone = false;
            }
          }
        }
      }
    );

    this.verify121OnlineAuth = this.verify121OnlineAuth.bind(this);
    this.dataSyncTrigger = this.dataSyncTrigger.bind(this);
    this.verify121OnlineAuth();
  }
  dataSyncTrigger() {
    dataSyncImport(
      this.props.login.environment,
      this.props.agent.agentCode,
      this.props.login.profileId,
      `Bearer ${this.props.login.authToken}`,
      false,
      false,
      this.props.login.sessionCookieSyncGatewaySession,
      this.props.login.sessionCookieExpiryDate,
      this.props.login.sessionCookiePath,
      this.props.login.syncDocumentIds,
      () => {}
    );
  }

  verify121OnlineAuth() {
    const title = "Data Sync is required";
    if (this.props.verify121OnlineAuth != null) {
      this.props.verify121OnlineAuth(errorResult => {
        if (errorResult.hasError) {
          if (this.state.timeBombType === TYPE_TIME_BOMB) {
            setTimeout(() => {
              Alert.alert(
                `The app has been locked as you have not synced your data recently. Please uninstall and install the app again.`
              );
            }, 500);
          } else if (errorResult.errorMsg) {
            if (errorResult.error) {
              setTimeout(() => {
                Alert.alert(
                  "Warning",
                  `[Error] ${errorResult.error}`,
                  [
                    {
                      text: "OK",
                      onPress: () => this.props.navigateToOnlineLoginPage(),
                      style: "cancel"
                    }
                  ],
                  { cancelable: false }
                );
              }, 200);
            } else {
              setTimeout(() => {
                Alert.alert(
                  "Warning",
                  `[Error] ${errorResult.errorMsg}`,
                  [
                    {
                      text: "OK",
                      onPress: () => this.props.navigateToOnlineLoginPage(),
                      style: "cancel"
                    }
                  ],
                  { cancelable: false }
                );
              }, 200);
            }
          } else {
            setTimeout(() => {
              Alert.alert(`[Error] Authorization Failed (Error 004)`);
            }, 200);
          }
        } else {
          this.props.checkTimeBomb(showAlert => {
            if (showAlert) {
              if (!showAlert.hasError) {
                if (showAlert.result === TYPE_TIME_BOMB) {
                  this.setState({
                    timeBombType: TYPE_TIME_BOMB
                  });
                  setTimeout(() => {
                    Alert.alert(
                      `The app has been locked as you have not synced your data recently. Please uninstall and install the app again.`
                    );
                  }, 500);
                } else if (!this.props.isTimezoneValid) {
                  setTimeout(() => {
                    Alert.alert(
                      "Warning",
                      `This app requires your device to be in Singapore time zone. Please change the time zone to Singapore and log in again.`,
                      [
                        {
                          text: "OK",
                          onPress: () => {
                            this.props.logout();
                          },
                          style: "cancel"
                        }
                      ],
                      { cancelable: false }
                    );
                  }, 200);
                } else if (showAlert.result === TYPE_SECOND_WARNING) {
                  const daysLeft = showAlert.daysLeft
                    ? showAlert.daysLeft
                    : null;
                  setTimeout(() => {
                    Alert.alert(
                      title,
                      `App is locked because Timebomb for this App will explode ${daysLeft} day(s). Please perform data sync first`,
                      [
                        {
                          text: "OK",
                          onPress: () => {
                            this.isDone = true;
                            this.dataSyncTrigger();
                          },
                          style: "cancel"
                        }
                      ],
                      { cancelable: false }
                    );
                  }, 500);
                } else if (showAlert.result === TYPE_FIRST_WARNING) {
                  const daysLeft = showAlert.daysLeft
                    ? showAlert.daysLeft
                    : null;
                  // 5 days warning
                  setTimeout(() => {
                    Alert.alert(
                      "Warning",
                      `Time Bomb for this App will explode in ${daysLeft} day(s). Please connect to internet and Login`,
                      [
                        {
                          text: "OK",
                          onPress: () => {
                            this.isDone = true;
                            this.dataSyncTrigger();
                          },
                          style: "cancel"
                        }
                      ],
                      { cancelable: false }
                    );
                  }, 500);
                } else {
                  this.isDone = true;
                  this.dataSyncTrigger();
                }
              }
            }
          });
        }
      });
    }
  }

  render() {
    if (this.state.timeBombType === TYPE_TIME_BOMB) {
      return (
        <View style={styles.container}>
          <View style={styles.loginForm}>
            <View style={styles.logoWrapper}>
              <Image style={styles.logo} source={axaLogo} />
            </View>
            <TranslatedText style={styles.welcome} path="login.timebomb" />
          </View>
        </View>
      );
    }
    return (
      <View style={styles.container}>
        <View style={styles.loginForm}>
          <View style={styles.logoWrapper}>
            <Image style={styles.logo} source={axaLogo} />
          </View>
          <TranslatedText style={styles.welcome} path="login.waiting" />
        </View>
      </View>
    );
  }
}

OnlineAuthBackPage.propTypes = {
  login: PropTypes.oneOfType([PropTypes.object]).isRequired,
  agent: PropTypes.oneOfType([PropTypes.object]).isRequired,
  verify121OnlineAuth: PropTypes.func.isRequired,
  setLastDataSyncTime: PropTypes.func.isRequired,
  checkTimeBomb: PropTypes.func.isRequired,
  // timeBombType: PropTypes.string.isRequired,
  logout: PropTypes.func.isRequired,
  isTimezoneValid: PropTypes.bool.isRequired,
  navigateToOnlineLoginPage: PropTypes.func
};

OnlineAuthBackPage.defaultProps = {
  navigateToOnlineLoginPage: ""
};
