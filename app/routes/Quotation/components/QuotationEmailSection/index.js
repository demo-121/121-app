import {
  REDUCER_TYPE_CHECK as WEB_API_REDUCER_TYPE_CHECK,
  REDUCER_TYPES,
  USER_TAB_TYPES
} from "eab-web-api";
import PropTypes from "prop-types";
import SafariView from "react-native-safari-view";
import React, { PureComponent } from "react";
import Config from "react-native-config";
import { View, Alert, Linking, NetInfo } from "react-native";
import APP_REDUCER_TYPE_CHECK from "../../../../constants/REDUCER_TYPE_CHECK";
import { TEXT_STORE } from "../../../../constants/REDUCER_TYPES";
import EABi18n from "../../../../utilities/EABi18n";
import EABHUD from "../../../../components/EABHUD";
import EABButton from "../../../../components/EABButton";
import TranslatedText from "../../../../containers/TranslatedText";
import { ContextConsumer } from "../../../../context";
import EASEEmailContentDialog from "../../../../containers/EASEEmailContentDialog";
import { PROPOSAL } from "../../../../../node_modules/eab-web-api/lib/constants/REDUCER_TYPES";
import styles from "./styles";

const { CLIENT, QUOTATION } = REDUCER_TYPES;
const { CLIENT_TAB, AGENT_TAB } = USER_TAB_TYPES;

/**
 * <QuotationEmailSection />
 * @description Quotation Email Button in APP bar
 * @param {boolean} canEmail - boolean whether enable or disable the Email button
 * */
export default class QuotationEmailSection extends PureComponent {
  constructor() {
    super();
    this.getAuthCodeCallback = this.getAuthCodeCallback.bind(this);
    this.setfunction = this.setfunction.bind(this);
    this.state = {
      function: null,
      isNetworkConnected: false,
      isLoading: false,
      setOnlineDone: false
    };
    this.handleConnectionChange = this.handleConnectionChange.bind(this);
    NetInfo.isConnected.fetch().done(this.handleConnectionChange);
  }
  componentDidMount() {
    Linking.addEventListener("url", this.getAuthCodeCallback);
    NetInfo.isConnected.addEventListener(
      "connectionChange",
      this.handleConnectionChange
    );
  }

  componentWillUnmount() {
    Linking.removeEventListener("url", this.getAuthCodeCallback);
    NetInfo.isConnected.removeEventListener(
      "connectionChange",
      this.handleConnectionChange
    );
  }

  getAuthCodeCallback(response) {
    const { setOnlineAuthCodeForPayment } = this.props;
    if (response.url) {
      const urlGet = response.url;
      SafariView.dismiss();
      this.setState(
        {
          isLoading: true
        },
        () => {
          const segment1 = urlGet.replace("easemobile://?", "");
          const variables = segment1.split("&");
          const codeString = variables.find(variable =>
            variable.includes("code")
          );
          const [, authCode] = codeString.split("=");
          if (!this.props.isHandlingListener) {
            setOnlineAuthCodeForPayment({
              authCode,
              callback: callback => {
                if (callback.hasError) {
                  if (callback.errorMsg) {
                    setTimeout(() => {
                      Alert.alert(`[Error] ${callback.errorMsg}`);
                    }, 200);
                  } else {
                    setTimeout(() => {
                      Alert.alert(
                        `error msg: Authorization for submission Failed (Error 001)`
                      );
                    }, 200);
                  }
                } else {
                  this.setState({
                    setOnlineDone: true
                  });
                }
              }
            });
          }
        }
      );
    }
  }
  setfunction(inputFunction) {
    if (this.state.setOnlineDone) {
      this.setState({
        function: inputFunction,
        isLoading: false
      });
    }
  }
  handleConnectionChange(isConnected) {
    this.setState({ isNetworkConnected: isConnected });
  }
  pressHandler() {
    SafariView.isAvailable()
      .then(
        SafariView.show({
          url: this.path
          // url: "eabtest://"
        })
      )
      .catch(error => {
        // Fallback WebView code for iOS 8 and earlier
        setTimeout(() => {
          Alert.alert(`[Error]${error}`);
        }, 200);
      });
  }

  render() {
    const {
      profile,
      textStore,
      proposalEmail,
      openEmailDialog,
      checkTokenExpiryDate,
      checkValidDevice,
      loginByOnlineAuthForPayment,
      environment,
      sendEmail,
      tabOnPress,
      tab,
      checkBoxOnPress,
      proposal,
      canEmail
    } = this.props;
    const { isNetworkConnected } = this.state;
    if (Config.ENV_NAME === "PROD") {
      this.path = Config.GET_AUTH_CODE_FULL_URL;
    } else if (environment === "DEV") {
      this.path = Config.DEV_GET_AUTH_CODE_FULL_URL;
    } else if (environment === "SIT1_EAB") {
      this.path = Config.SIT1_EAB_GET_AUTH_CODE_FULL_URL;
    } else if (environment === "SIT2_EAB") {
      this.path = Config.SIT2_EAB_GET_AUTH_CODE_FULL_URL;
    } else if (environment === "SIT3_EAB") {
      this.path = Config.SIT3_EAB_GET_AUTH_CODE_FULL_URL;
    } else if (environment === "SIT_AXA") {
      this.path = Config.SIT_AXA_GET_AUTH_CODE_FULL_URL;
    } else if (environment === "UAT") {
      this.path = Config.UAT_GET_AUTH_CODE_FULL_URL;
    } else if (environment === "PRE_PROD") {
      this.path = Config.PRE_PROD_GET_AUTH_CODE_FULL_URL;
    } else {
      this.path = Config.SIT3_EAB_GET_AUTH_CODE_FULL_URL;
    }

    const { reportOptions } = proposalEmail;
    const { pdfData } = proposal;

    const checkBoxOptions = pdfData.map(item => {
      let isSelected;

      if (reportOptions) {
        if (tab === AGENT_TAB) {
          isSelected = reportOptions.agent[item.id];
        } else if (tab === CLIENT_TAB) {
          isSelected = reportOptions.client[item.id];
        }
      } else {
        isSelected = true;
      }
      return {
        key: item.id,
        text: item.label,
        isSelected
      };
    });
    if (this.state.function !== null) {
      this.state.function();
    }
    return (
      <ContextConsumer>
        {({ showEmailContentDialog, language }) => (
          <View>
            {this.setfunction(() => {
              if (this.state.function !== null && this.state.setOnlineDone) {
                this.setState(
                  {
                    setOnlineDone: false
                  },
                  () => {
                    showEmailContentDialog(QUOTATION);
                  }
                );
              }
            })}
            <EABButton
              testID="QuotationEmail__btnEmail"
              style={styles.button}
              isDisable={!canEmail}
              onPress={() => {
                if (this.state.isNetworkConnected) {
                  openEmailDialog(() => {
                    checkTokenExpiryDate(status => {
                      // status = false if its need to do online auth
                      if (!status) {
                        const title = EABi18n({
                          language,
                          textStore,
                          path: "needs.text.email.title"
                        });
                        const warningMessage = EABi18n({
                          language,
                          textStore,
                          path: "needs.text.onlineAuthWarning"
                        });
                        const openSafari = EABi18n({
                          language,
                          textStore,
                          path: "needs.text.openSafari"
                        });
                        const cancel = EABi18n({
                          language,
                          textStore,
                          path: "needs.text.cancel"
                        });
                        loginByOnlineAuthForPayment();
                        Alert.alert(
                          title,
                          warningMessage,
                          [
                            {
                              text: openSafari,
                              onPress: () => this.pressHandler()
                            },
                            {
                              text: cancel,
                              onPress: () => {},
                              style: "cancel"
                            }
                          ],
                          { cancelable: false }
                        );
                      } else {
                        checkValidDevice(checkValidDeviceResult => {
                          if (!checkValidDeviceResult.hasError) {
                            showEmailContentDialog(QUOTATION);
                          } else {
                            setTimeout(() => {
                              Alert.alert(
                                `[Error] Authorization Failed (Error 007)`
                              );
                            }, 200);
                          }
                        });
                      }
                    });
                  });
                } else {
                  setTimeout(() => {
                    Alert.alert(
                      "Warning",
                      `Please connect to the internet to proceed further`,
                      [
                        {
                          text: "OK",
                          onPress: () => {},
                          style: "cancel"
                        }
                      ],
                      { cancelable: false }
                    );
                  }, 200);
                }
              }}
            >
              <TranslatedText style={styles.text} path="button.email" />
            </EABButton>
            <EASEEmailContentDialog
              isNetworkConnected={isNetworkConnected}
              checkBoxOptions={checkBoxOptions}
              checkBoxOnPress={option => {
                checkBoxOnPress({
                  tab,
                  option
                });
              }}
              tabOnPress={option => {
                tabOnPress(option);
              }}
              sendEmail={sendEmail}
              reportEmail={proposalEmail}
              tab={tab}
              disableClientTab={!profile.email}
              reportType={QUOTATION}
            />
            <EABHUD isOpen={this.state.isLoading} />
          </View>
        )}
      </ContextConsumer>
    );
  }
}

QuotationEmailSection.propTypes = {
  openEmailDialog: PropTypes.func.isRequired,
  textStore: APP_REDUCER_TYPE_CHECK[TEXT_STORE].isRequired,
  environment: PropTypes.string.isRequired,
  profile: WEB_API_REDUCER_TYPE_CHECK[CLIENT].profile.isRequired,
  checkTokenExpiryDate: PropTypes.func.isRequired,
  checkValidDevice: PropTypes.func.isRequired,
  setOnlineAuthCodeForPayment: PropTypes.func.isRequired,
  isHandlingListener: PropTypes.bool.isRequired,
  loginByOnlineAuthForPayment: PropTypes.func.isRequired,
  proposal: WEB_API_REDUCER_TYPE_CHECK[PROPOSAL].isRequired,
  sendEmail: PropTypes.func.isRequired,
  checkBoxOnPress: PropTypes.func.isRequired,
  tab: PropTypes.bool.isRequired,
  tabOnPress: PropTypes.func.isRequired,
  canEmail: PropTypes.bool.isRequired,
  proposalEmail: WEB_API_REDUCER_TYPE_CHECK[PROPOSAL].proposalEmail.isRequired
};
