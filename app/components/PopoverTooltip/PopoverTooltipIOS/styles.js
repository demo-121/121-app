import { StyleSheet } from "react-native";
import Theme from "../../../theme";

export default StyleSheet.create({
  popoverWrapper: {
    padding: Theme.alignmentXL,
    width: "100%",
    height: "100%",
    alignItems: "center",
    justifyContent: "flex-start"
  },
  textView: {
    width: "100%"
  },
  image: {
    width: 24,
    height: 24
  }
});
