import { connect } from "react-redux";
import EASEEmailContentDialog from "../components/EASEEmailContentDialog";
import { TEXT_STORE } from "../constants/REDUCER_TYPES";

/**
 * EASEEmailContentDialog
 * @requires EASEEmailContentDialog - EASEEmailContentDialog UI
 * */
const mapStateToProps = state => ({
  textStore: state[TEXT_STORE]
});

const mapDispatchToProps = () => ({});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(EASEEmailContentDialog);
