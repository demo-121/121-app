import React, { PureComponent } from "react";
import { View, Text } from "react-native";
import PropTypes from "prop-types";
import * as _ from "lodash";
import { REDUCER_TYPES, REDUCER_TYPE_CHECK } from "eab-web-api";

import APP_REDUCER_TYPE_CHECK from "../../../../constants/REDUCER_TYPE_CHECK";
import { TEXT_STORE } from "../../../../constants/REDUCER_TYPES";
import { RECTANGLE_BUTTON_2 } from "../../../../components/EABButton/constants";
import EABButton from "../../../../components/EABButton";
import EABTextBox from "../../../../components/EABTextBox";
import EABTextSelection from "../../../../components/EABTextSelection";
import EABFormSectionHeader from "../../../../components/EABFormSectionHeader";
import TranslatedText from "../../../../containers/TranslatedText";
import EABi18n from "../../../../utilities/EABi18n";
import Theme from "../../../../theme";

import styles from "./styles";
import { ContextConsumer } from "../../../../context";

const { RECOMMENDATION } = REDUCER_TYPES;

/**
 * RecommendationFormShield
 * @description Form for Shield Product in Recommendation page.
 * @param {object} textStore - reducer textStore state
 * @param {object} value - data for Recommendation page
 * @param {boolean} isChosen - indicate the quotation is in chosen list or not
 * @param {boolean} disabled - change all UI to non-editable
 * @param {func} showRecommendationDetails - action to do after View Details is clicked
 * @param {func} updateRecommendationData - update Recommendation state with inputted data
 * */

class RecommendationFormShield extends PureComponent {
  static checkHasRop(value) {
    return (
      _.get(value, "rop_shield.ropBlock.shieldRopAnswer_0") === "Y" ||
      _.get(value, "rop_shield.ropBlock.shieldRopAnswer_1") === "Y" ||
      _.get(value, "rop_shield.ropBlock.shieldRopAnswer_2") === "Y" ||
      _.get(value, "rop_shield.ropBlock.shieldRopAnswer_3") === "Y" ||
      _.get(value, "rop_shield.ropBlock.shieldRopAnswer_4") === "Y" ||
      _.get(value, "rop_shield.ropBlock.shieldRopAnswer_5") === "Y"
    );
  }

  constructor(props) {
    super(props);

    this.renderWarningNote = this.renderWarningNote.bind(this);
    this.renderRequiredComponent = this.renderRequiredComponent.bind(this);
    this.renderOptionalComponent = this.renderOptionalComponent.bind(this);
    this.warningNoteStatementsText = [
      {
        key: "0",
        title: "recommendation.recommendationShield.title.warningNoteStatement1"
      },
      {
        key: "1",
        title: "recommendation.recommendationShield.title.warningNoteStatement2"
      },
      {
        key: "2",
        title: "recommendation.recommendationShield.title.warningNoteStatement3"
      },
      {
        key: "3",
        title: "recommendation.recommendationShield.title.warningNoteStatement4"
      },
      {
        key: "4",
        title: "recommendation.recommendationShield.title.warningNoteStatement5"
      },
      {
        key: "5",
        title: "recommendation.recommendationShield.title.warningNoteStatement6"
      },
      {
        key: "6",
        title: "recommendation.recommendationShield.title.warningNoteStatement7"
      }
    ];
    this.state = {};
  }

  updateStateValue({ path, newPathValue }) {
    const { value, updateRecommendationData } = this.props;
    const newValue = _.cloneDeep(value);
    _.set(newValue, path, newPathValue);

    // reset hidden field value
    if (!RecommendationFormShield.checkHasRop(newValue)) {
      _.set(newValue, "rop_shield.ropBlock.ropQ2", "");
      _.set(newValue, "rop_shield.ropBlock.ropQ3", "");
      _.set(newValue, "rop_shield.ropBlock.ropQ1sub3", "");
    }

    updateRecommendationData(newValue);
  }

  renderWarningNote(language) {
    return (
      <View style={styles.warningView}>
        {this.warningNoteStatementsText.map(statement => (
          <View
            key={`warningNoteStatement-${statement.key}`}
            style={styles.warningStatementView}
          >
            {statement.key === "0" ? null : (
              <Text style={styles.warningStatementPoint}>-</Text>
            )}
            <TranslatedText
              style={styles.warningStatement}
              language={language}
              path={statement.title}
            />
          </View>
        ))}
      </View>
    );
  }

  renderRequiredComponent(language) {
    const { value, isChosen, disabled, textStore } = this.props;

    const ropQ1Answer = [];
    if (isChosen) {
      const iCidRopAnswerMap = _.get(
        value,
        "rop_shield.ropBlock.iCidRopAnswerMap"
      );
      const shieldInsuredPlans = _.get(value, "extra.shieldInsuredPlans");
      _.forEach(iCidRopAnswerMap, (answerId, cid) => {
        const plan = _.find(shieldInsuredPlans, p => p.cid === cid);

        ropQ1Answer.push(
          <View key={`${answerId}`} style={styles.textSelection}>
            <Text style={styles.textSelectionTitle}>
              {_.get(plan, "fullName", "")}
              <Text style={Theme.fieldTextOrHelperTextNegative}> *</Text>
            </Text>
            <EABTextSelection
              testID="PreAppilcation__Recommendation"
              options={[
                {
                  key: `${answerId}-yes`,
                  testID: "ReplacementOfPolicyYes",
                  title: EABi18n({
                    path: "textSelection.yes",
                    language,
                    textStore
                  }),
                  isSelected:
                    _.get(value, `rop_shield.ropBlock.${answerId}`) === "Y"
                },
                {
                  key: `${answerId}-no`,
                  testID: "ReplacementOfPolicyNo",
                  title: EABi18n({
                    path: "textSelection.no",
                    language,
                    textStore
                  }),
                  isSelected:
                    _.get(value, `rop_shield.ropBlock.${answerId}`) === "N"
                }
              ]}
              editable={!disabled}
              onPress={option => {
                const newValue = option.key === `${answerId}-yes` ? "Y" : "N";
                this.updateStateValue({
                  path: `rop_shield.ropBlock.${answerId}`,
                  newPathValue: newValue
                });
              }}
            />
            {_.get(value, `rop_shield.ropBlock.${answerId}`) !== "Y" &&
            _.get(value, `rop_shield.ropBlock.${answerId}`) !== "N" ? (
              <View>
                <TranslatedText
                  style={Theme.fieldTextOrHelperTextNegative}
                  language={language}
                  path="error.302"
                />
              </View>
            ) : null}
          </View>
        );
      });
    }

    return (
      <View>
        <View style={styles.blockViewBottom}>
          <EABFormSectionHeader
            isRequired
            tooltipOptions={{
              preferredContentSize: [420, 150]
            }}
            tooltipText={EABi18n({
              path: "recommendation.recommendation.title.reason.tooltip",
              language,
              textStore
            })}
            style={styles.title}
            title={EABi18n({
              path: "recommendation.recommendation.title.reason",
              language,
              textStore
            })}
          />
          <EABTextBox
            testID="PreAppilcation__Recommendation__txtReasonsForPlanAndRiders"
            style={styles.textBox}
            numberOfLines={5}
            maxLength={5000}
            editable={!disabled}
            value={_.get(value, "reason")}
            onChange={newValue => {
              this.updateStateValue({
                path: "reason",
                newPathValue: newValue
              });
            }}
          />
          {_.get(value, "reason").trim() === "" ? (
            <View>
              <TranslatedText
                style={Theme.fieldTextOrHelperTextNegative}
                language={language}
                path="error.302"
              />
            </View>
          ) : null}
        </View>
        <View style={styles.blockViewBottom}>
          <EABFormSectionHeader
            isRequired
            style={styles.title}
            title={EABi18n({
              path: "recommendation.recommendation.title.benefit",
              language,
              textStore
            })}
          />
          <EABTextBox
            testID="PreAppilcation__Recommendation__txtBenefitOfPlan"
            style={styles.textBox}
            numberOfLines={5}
            maxLength={5000}
            editable={!disabled}
            value={_.get(value, "benefit")}
            onChange={newValue => {
              this.updateStateValue({
                path: "benefit",
                newPathValue: newValue
              });
            }}
            onBlur={newValue => {
              if (_.trim(newValue.nativeEvent.text).length === 0) {
                this.updateStateValue({
                  path: "benefit",
                  newPathValue: _.get(value, "extra.recommendBenefitDefault")
                });
              }
            }}
          />
        </View>
        <View style={styles.blockViewBottom}>
          <EABFormSectionHeader
            isRequired
            style={styles.title}
            title={EABi18n({
              path: "recommendation.recommendation.title.limitation",
              language,
              textStore
            })}
          />
          <EABTextBox
            testID="PreAppilcation__Recommendation__txtLimitationForPlanAndRiders"
            style={styles.textBox}
            numberOfLines={5}
            maxLength={5000}
            editable={!disabled}
            value={_.get(value, "limitation")}
            onChange={newValue => {
              this.updateStateValue({
                path: "limitation",
                newPathValue: newValue
              });
            }}
            onBlur={newValue => {
              if (_.trim(newValue.nativeEvent.text).length === 0) {
                this.updateStateValue({
                  path: "limitation",
                  newPathValue: _.get(value, "extra.recommendLimitDefault")
                });
              }
            }}
          />
        </View>

        {isChosen
          ? ((
              <EABFormSectionHeader
                style={styles.title}
                title={EABi18n({
                  path: "recommendation.recommendation.title.rop",
                  language,
                  textStore
                })}
              />
            ),
            (
              <View style={styles.blockView}>
                <EABFormSectionHeader
                  style={styles.title}
                  title={EABi18n({
                    path: "recommendation.recommendationShield.title.ropQ1",
                    language,
                    textStore
                  })}
                />
                {ropQ1Answer}
              </View>
            ))
          : null}
      </View>
    );
  }

  renderOptionalComponent(language) {
    const { value, disabled, textStore } = this.props;

    return (
      <View>
        <View style={styles.blockView}>
          <EABFormSectionHeader
            isRequired
            style={styles.title}
            title={EABi18n({
              path: "recommendation.recommendationShield.title.ropQ2",
              language,
              textStore
            })}
          />
          <View style={styles.textSelection}>
            <EABTextSelection
              testID="PreAppilcation__Recommendation"
              options={[
                {
                  key: "ropQ2-yes",
                  testID: "ReplacementAdvisedYes",
                  title: EABi18n({
                    path: "textSelection.yes",
                    language,
                    textStore
                  }),
                  isSelected: _.get(value, "rop_shield.ropBlock.ropQ2") === "Y"
                },
                {
                  key: "ropQ2-no",
                  testID: "ReplacementAdvisedNo",
                  title: EABi18n({
                    path: "textSelection.no",
                    language,
                    textStore
                  }),
                  isSelected: _.get(value, "rop_shield.ropBlock.ropQ2") === "N"
                }
              ]}
              editable={!disabled}
              onPress={option => {
                const newValue = option.key === "ropQ2-yes" ? "Y" : "N";
                this.updateStateValue({
                  path: "rop_shield.ropBlock.ropQ2",
                  newPathValue: newValue
                });
              }}
            />
            {_.get(value, "rop_shield.ropBlock.ropQ2") !== "Y" &&
            _.get(value, "rop_shield.ropBlock.ropQ2") !== "N" ? (
              <View>
                <TranslatedText
                  style={Theme.fieldTextOrHelperTextNegative}
                  language={language}
                  path="error.302"
                />
              </View>
            ) : null}
          </View>
        </View>
        <View style={styles.blockView}>
          <EABFormSectionHeader
            isRequired
            style={styles.title}
            title={EABi18n({
              path: "recommendation.recommendationShield.title.ropQ3",
              language,
              textStore
            })}
          />
          <View style={styles.textSelection}>
            <EABTextSelection
              testID="PreAppilcation__Recommendation"
              options={[
                {
                  key: "ropQ3-yes",
                  testID: "SatisfactionInTheEventYes",
                  title: EABi18n({
                    path: "textSelection.yes",
                    language,
                    textStore
                  }),
                  isSelected: _.get(value, "rop_shield.ropBlock.ropQ3") === "Y"
                },
                {
                  key: "ropQ3-no",
                  testID: "StatisfactionInTheEventNo",
                  title: EABi18n({
                    path: "textSelection.no",
                    language,
                    textStore
                  }),
                  isSelected: _.get(value, "rop_shield.ropBlock.ropQ3") === "N"
                }
              ]}
              editable={!disabled}
              onPress={option => {
                const newValue = option.key === "ropQ3-yes" ? "Y" : "N";
                this.updateStateValue({
                  path: "rop_shield.ropBlock.ropQ3",
                  newPathValue: newValue
                });
              }}
            />
            {_.get(value, "rop_shield.ropBlock.ropQ3") !== "Y" &&
            _.get(value, "rop_shield.ropBlock.ropQ3") !== "N" ? (
              <View>
                <TranslatedText
                  style={Theme.fieldTextOrHelperTextNegative}
                  language={language}
                  path="error.302"
                />
              </View>
            ) : null}
          </View>
        </View>
        <View style={styles.blockViewNoBorder}>
          <EABFormSectionHeader
            isRequired
            style={styles.title}
            title={EABi18n({
              path: "recommendation.recommendationShield.title.ropQ1Sub3",
              language,
              textStore
            })}
          />
          <EABTextBox
            testID="PreAppilcation__Recommendation__txtBasicPolicyReplacement"
            numberOfLines={5}
            maxLength={3000}
            editable={!disabled}
            style={styles.textBox}
            value={_.get(value, "rop_shield.ropBlock.ropQ1sub3")}
            onChange={newValue => {
              this.updateStateValue({
                path: "rop_shield.ropBlock.ropQ1sub3",
                newPathValue: newValue
              });
            }}
          />
          {_.get(value, "rop_shield.ropBlock.ropQ1sub3").trim() === "" ? (
            <View>
              <TranslatedText
                style={[Theme.fieldTextOrHelperTextNegative, { height: 50 }]}
                language={language}
                path="error.302"
              />
            </View>
          ) : null}
        </View>
        {this.renderWarningNote(language)}
      </View>
    );
  }

  render() {
    const { quotId, value, showRecommendationDetails } = this.props;

    const shieldInsuredPlans = _.get(value, "extra.shieldInsuredPlans", []);
    const insuredPlans = _.map(shieldInsuredPlans, plan => (
      <View style={styles.planDetailContentViewRowShield}>
        <Text style={styles.planDetailTitleShield}>{plan.shieldInsured}</Text>
        <Text style={styles.planDetailContentShield}>{plan.shieldPlan}</Text>
      </View>
    ));

    return (
      <ContextConsumer>
        {({ language }) => (
          <View style={styles.container}>
            <TranslatedText
              style={styles.header}
              path="recommendation.recommendation.header"
              funcOnText={_.toUpper}
            />
            <View style={styles.planDetailBlockView}>
              <View style={styles.planDetailContentView}>
                <View style={styles.planDetailContentViewRow}>
                  <TranslatedText
                    style={styles.planDetailTitle}
                    path="recommendation.recommendation.proposalNo"
                  />
                  <Text style={styles.planDetailContent}>{quotId}</Text>
                </View>
                <TranslatedText
                  style={styles.plansHeader}
                  path="recommendation.recommendationShield.plans.header"
                  funcOnText={_.toUpper}
                />
                {insuredPlans}
              </View>
              <View style={styles.planDetailButtonView}>
                <EABButton
                  buttonType={RECTANGLE_BUTTON_2}
                  onPress={() => {
                    showRecommendationDetails();
                  }}
                >
                  <TranslatedText path="recommendation.recommendation.button.viewDetails" />
                </EABButton>
              </View>
            </View>
            {this.renderRequiredComponent(language)}
            {RecommendationFormShield.checkHasRop(value)
              ? this.renderOptionalComponent(language)
              : null}
          </View>
        )}
      </ContextConsumer>
    );
  }
}

RecommendationFormShield.propTypes = {
  textStore: APP_REDUCER_TYPE_CHECK[TEXT_STORE].isRequired,
  quotId: PropTypes.string.isRequired,
  value:
    REDUCER_TYPE_CHECK[RECOMMENDATION].recommendation.valueByQuotId.isRequired,
  isChosen: PropTypes.bool.isRequired,
  disabled: PropTypes.bool.isRequired,
  showRecommendationDetails: PropTypes.func.isRequired,
  updateRecommendationData: PropTypes.func.isRequired
};

export default RecommendationFormShield;
