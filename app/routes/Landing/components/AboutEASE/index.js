import React from "react";
import { Text, View, Image } from "react-native";
import EaseLogo from "../../../../assets/images/logo.png";
import Theme from "../../../../theme";
import styles from "./styles";

/**
 * <About EASE />
 * @description About EASE type layout of home page
 * */
export default () => (
  <View style={styles.container}>
    <View style={styles.logoWrapper}>
      <Image source={EaseLogo} />
    </View>
    <View style={styles.wrapper}>
      <View>
        <View style={styles.descriptionTitleContainer}>
          <Text style={Theme.AboutEASETitle}>
            121Demo is powered by EAB Systems’ 121 System.
          </Text>
        </View>
        <View style={styles.descriptionDisclaimerContainer}>
          <Text style={Theme.AboutEASEDisclaimer}>
            Copyright © EAB Systems (Hong Kong) Limited. All rights reserved.
          </Text>
        </View>
        <View style={styles.descriptionContainer}>
          <Text style={Theme.AboutEASEDescription}>
            121 System is the property of EAB Systems (Hong Kong) Limited and is
            protected by copyright and other intellectual property laws and
            treaties. 121 System is licensed and may not be used without
            permission. All rights in and title to 121 System are expressly
            reserved by EAB Systems (Hong Kong) Limited.
          </Text>
        </View>

        <View style={styles.descriptionContainer}>
          <Text style={Theme.AboutEASEDescription}>
            121 System is an integrated sales support system that provides an
            effective solution for your sales force and customer needs. Product
            modules include Financial Needs Analysis, Policy Illustration,
            Application (Proposal, Submission and Approval with eSignature),
            Workbench, Reports, Needs Configurator, Product Configurator,
            Campaign Management, Channel Management and Audit/Analytics. 121
            System is scalable and extensible, and can operate online or offline
            on multiple platforms including iOS and the web.
          </Text>
        </View>
      </View>
    </View>
  </View>
);
