import PropTypes from "prop-types";
import { REDUCER_TYPE_CHECK, REDUCER_TYPES, utilities } from "eab-web-api";
import React, { Component } from "react";
import { View, ScrollView, Text } from "react-native";
import * as _ from "lodash";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import LinearGradient from "react-native-linear-gradient";
import completedBadge from "../../../../assets/images/icCompletedMiniBadge.png";
import incompletedBadge from "../../../../assets/images/icIncompleteMiniBadge.png";
import EABButton from "../../../../components/EABButton";
import EABSegmentedControl from "../../../../components/EABSegmentedControl";
import EABTable from "../../../../components/EABTable";
import EABTableSection from "../../../../components/EABTableSection";
import EABTextBox from "../../../../components/EABTextBox";
import EABTextField from "../../../../components/EABTextField";
import EABTextSelection from "../../../../components/EABTextSelection";
import PopoverTooltip from "../../../../components/PopoverTooltip/PopoverTooltipIOS";
import APP_REDUCER_TYPE_CHECK from "../../../../constants/REDUCER_TYPE_CHECK";
import { TEXT_STORE } from "../../../../constants/REDUCER_TYPES";
import EABHUD from "../../../../containers/EABHUD";
import TranslatedText from "../../../../containers/TranslatedText";
import { ContextConsumer } from "../../../../context";
import Theme from "../../../../theme";
import EABi18n from "../../../../utilities/EABi18n";
import confirmationAlert from "../../../../utilities/confirmationAlert";
import NetWorthChart from "../NetWorthChart";
import styles from "./styles";
import {
  RECTANGLE_BUTTON_1,
  RECTANGLE_BUTTON_2
} from "../../../../components/EABButton/constants";

const { FNA, CLIENT } = REDUCER_TYPES;
const { numberFormatter } = utilities;
const { numberToCurrency, currencyToNumber } = numberFormatter;

/**
 * <FinancialEvaluation />
 * @description fna financial evaluation section
 * @param {function} closeDialog - close dialog function
 * @param {function} switchToNA - close dialog and open needs analysis dialog
 * @param {function} switchToPDA - close dialog and open personal data
 *   acknowledgement dialog
 * @param {function} updateFE - update FE reducer
 * */
export default class FinancialEvaluation extends Component {
  constructor(props) {
    super(props);

    this.saveHandledFE = this.saveHandledFE.bind(this);
    this.getDependantData = this.getDependantData.bind(this);

    this.state = {
      shouldSave: false,
      activeClientCID: this.props.profile.cid,
      isLoading: false
    };
  }
  // ===========================================================================
  // getters
  // ===========================================================================
  /**
   * @description get the relationship between the client and the editing person
   * @return {object} relationship. if editing person is client, return "owner"
   *   in relationship key.
   * */
  get relationshipObject() {
    const { profile } = this.props;
    const { activeClientCID } = this.state;

    // is owner?
    if (activeClientCID === profile.cid) {
      return {
        cid: profile.cid,
        relationship: "owner"
      };
    }

    // get relationship
    return _.find(
      profile.dependants,
      dependant => dependant.cid === activeClientCID
    );
  }
  /**
   * @description get spouse data
   * @return {object|null} spouse data object or null(can not found spouse
   *   object or applicant is not "joint")
   * */
  get getSpouseData() {
    const { pda, profile, dependantProfiles } = this.props;

    if (pda.applicant === "joint") {
      let returnData = {};
      profile.dependants.forEach(dependant => {
        if (dependant.relationship === "SPO") {
          returnData = dependantProfiles[dependant.cid];
        }
      });
      return returnData;
    }
    return null;
  }
  /**
   * @description map fe data
   * @return {object} fe data object
   * */
  get feData() {
    const { relationshipObject } = this;
    const { fe } = this.props;

    switch (relationshipObject.relationship) {
      case "owner":
        return fe.owner;
      case "SPO":
        return fe.spouse;
      case "SON":
      case "DAU": {
        return _.find(
          fe.dependants,
          dependant => dependant.cid === relationshipObject.cid
        );
      }
      default:
        throw new Error(`can not found ${relationshipObject.cid} feData.`);
    }
  }
  /**
   * @description get on change function
   * @return {function} on change function
   * */
  get onChange() {
    const { relationshipObject } = this;
    const { fe, updateFE, pda, profile, dependantProfiles } = this.props;

    switch (relationshipObject.relationship) {
      case "owner":
        return ({ value, key, shouldResetClient }) => {
          this.setState(key === "init" ? {} : { shouldSave: true }, () => {
            updateFE({
              feData: {
                ...fe,
                owner: {
                  ...fe.owner,
                  init: key === "init" ? fe.owner.init : false,
                  [key]: value
                },
                revisit: false,
                spouse:
                  shouldResetClient === "spouse"
                    ? {
                        ...fe.spouse,
                        init: true
                      }
                    : fe.spouse
              },
              pdaData: pda,
              profileData: profile,
              dependantProfilesData: dependantProfiles
            });
          });
        };
      case "SPO":
        return ({ value, key, shouldResetClient }) => {
          this.setState(key === "init" ? {} : { shouldSave: true }, () => {
            updateFE({
              feData: {
                ...fe,
                spouse: {
                  ...fe.spouse,
                  init: key === "init" ? fe.spouse.init : false,
                  [key]: value
                },
                revisit: false,
                owner:
                  shouldResetClient === "owner"
                    ? {
                        ...fe.owner,
                        init: true
                      }
                    : fe.owner
              },
              pdaData: pda,
              profileData: profile,
              dependantProfilesData: dependantProfiles
            });
          });
        };
      case "SON":
      case "DAU":
        return ({ value, key, shouldResetClient }) => {
          const dependantsData = _.cloneDeep(fe.dependants) || [];
          const index = _.findIndex(
            dependantsData,
            dependant => dependant.cid === relationshipObject.cid
          );

          dependantsData[index].init =
            key === "init" ? dependantsData[index].init : false;
          dependantsData[index][key] = value;

          this.setState(key === "init" ? {} : { shouldSave: true }, () => {
            updateFE({
              feData: {
                ...fe,
                owner:
                  shouldResetClient === "owner"
                    ? {
                        ...fe.owner,
                        init: true
                      }
                    : fe.owner,
                revisit: false,
                spouse:
                  shouldResetClient === "spouse"
                    ? {
                        ...fe.spouse,
                        init: true
                      }
                    : fe.spouse,
                dependants: dependantsData
              },
              pdaData: pda,
              profileData: profile,
              dependantProfilesData: dependantProfiles
            });
          });
        };
      default:
        throw new Error(
          `onChange got unexpected relationship "${
            relationshipObject.relationship
          }".`
        );
    }
  }
  /**
   * @description indicate fe have errors or not
   * @return {boolean} fe errors status
   * */
  get isFEError() {
    const { activeClientCID } = this.state;
    const { feErrors } = this.props;
    let hasError = false;

    Object.keys(feErrors).forEach(clientCid => {
      Object.keys(feErrors[clientCid]).forEach(key => {
        // if only active client init is false, but others client is done, user
        // allow to next to na
        if (activeClientCID !== clientCid || key !== "isFinish") {
          hasError = hasError || feErrors[clientCid][key].hasError;
        }
      });
    });

    return hasError;
  }
  // ===========================================================================
  // class methods
  // ===========================================================================
  /**
   * @description get dependant data, only can get son or daughter data
   * @return {object|null} spouse dependant data or null(can not found
   *   dependant object or applicant is not "joint")
   * */
  getDependantData(cid) {
    const { dependantProfiles } = this.props;

    const dependantProfile = dependantProfiles[cid] || null;

    if (
      dependantProfile !== null &&
      (dependantProfile.relationship === "SON" ||
        dependantProfile.relationship === "DAU")
    ) {
      return dependantProfile;
    }
    return null;
  }

  /**
   * @description save FE after click yes in confirmation alert
   * */
  saveFeWithConfirm(callback) {
    const { saveFE } = this.props;
    this.setState({ isLoading: true, shouldSave: false }, () => {
      saveFE({
        confirm: true,
        callback: () => {
          this.setState({ isLoading: false }, () => {
            callback();
          });
        }
      });
    });
  }

  /**
   * @description save FE after clicking Done button
   * */
  saveFeWithoutConfirm({ language, callback }) {
    const { textStore, saveFE } = this.props;
    this.setState({ isLoading: true, shouldSave: false }, () => {
      saveFE({
        confirm: false,
        callback: code => {
          this.setState({ isLoading: false, shouldSave: !!code }, () => {
            if (code !== undefined) {
              setTimeout(() => {
                confirmationAlert({
                  messagePath: `error.${code}`,
                  language,
                  textStore,
                  yesOnPress: () => {
                    this.saveFeWithConfirm(callback);
                  }
                });
              }, 200);
            } else {
              setTimeout(callback, 200);
            }
          });
        }
      });
    });
  }

  /**
   * @description save fe data
   * */
  saveHandledFE({ callback, language }) {
    const { feData, onChange } = this;
    const { shouldSave } = this.state;

    if (shouldSave || feData.init === true) {
      onChange({ key: "init", value: false });

      this.saveFeWithoutConfirm({
        language,
        callback
      });
    } else {
      callback();
    }
  }

  render() {
    // =========================================================================
    // variables
    // =========================================================================
    const {
      getSpouseData,
      getDependantData,
      relationshipObject,
      feData,
      onChange,
      saveHandledFE,
      isFEError
    } = this;
    const {
      pda,
      feErrors,
      switchToNA,
      switchToPDA,
      textStore,
      profile,
      closeDialog
    } = this.props;
    const { activeClientCID } = this.state;
    const editingClientErrors = feErrors[relationshipObject.cid];
    const { isLoading } = this.state;
    const defaultTableConfig = [
      {
        key: "title",
        style: styles.tableTitleWrapper
      },
      {
        key: "field",
        style: styles.tableFieldWrapper
      }
    ];
    const displayTableConfig = [
      {
        key: "column",
        style: styles.tableDisplayRow
      }
    ];
    // =========================================================================
    // render functions
    // =========================================================================
    /**
     * @description get segments options data
     * @return {array} segments options array
     * */
    const headerSegments = language => {
      // add proposer
      let ownerHasError = false;
      Object.keys(feErrors[profile.cid]).forEach(key => {
        if (activeClientCID === profile.cid) {
          ownerHasError =
            ownerHasError ||
            (key !== "isFinish" && feErrors[profile.cid][key].hasError);
        } else {
          ownerHasError = ownerHasError || feErrors[profile.cid][key].hasError;
        }
      });
      const segments = [
        {
          key: profile.cid,
          title: profile.fullName,
          isShowingBadge: true,
          source: ownerHasError ? incompletedBadge : completedBadge,
          onPress: () => {
            this.setState(
              {
                isLoading: true
              },
              () => {
                saveHandledFE({
                  callback: () => {
                    this.setState({
                      isLoading: false,
                      activeClientCID: profile.cid
                    });
                  },
                  language
                });
              }
            );
          }
        }
      ];

      // add spouse
      if (pda.applicant === "joint") {
        let spouseHasError = false;
        Object.keys(feErrors[getSpouseData.cid]).forEach(key => {
          if (activeClientCID === getSpouseData.cid) {
            spouseHasError =
              spouseHasError ||
              (key !== "isFinish" && feErrors[getSpouseData.cid][key].hasError);
          } else {
            spouseHasError =
              spouseHasError || feErrors[getSpouseData.cid][key].hasError;
          }
        });

        segments.push({
          key: getSpouseData.cid,
          title: getSpouseData.fullName,
          isShowingBadge: true,
          source: spouseHasError ? incompletedBadge : completedBadge,
          onPress: () => {
            this.setState(
              {
                isLoading: true
              },
              () => {
                saveHandledFE({
                  callback: () => {
                    this.setState({
                      isLoading: false,
                      activeClientCID: getSpouseData.cid
                    });
                  },
                  language
                });
              }
            );
          }
        });
      }

      // add dependants
      if (pda.dependants !== "") {
        pda.dependants.split(",").forEach(cid => {
          const dependant = getDependantData(cid);

          // filter rule
          if (dependant !== null) {
            let dependantHasError = false;
            Object.keys(feErrors[cid]).forEach(key => {
              if (activeClientCID === cid) {
                dependantHasError =
                  dependantHasError ||
                  (key !== "isFinish" && feErrors[cid][key].hasError);
              } else {
                dependantHasError =
                  dependantHasError || feErrors[cid][key].hasError;
              }
            });

            segments.push({
              key: dependant.cid,
              title: dependant.fullName,
              isShowingBadge: true,
              source: dependantHasError ? incompletedBadge : completedBadge,
              onPress: () => {
                this.setState(
                  {
                    isLoading: true
                  },
                  () => {
                    saveHandledFE({
                      callback: () => {
                        this.setState({
                          isLoading: false,
                          activeClientCID: dependant.cid
                        });
                      },
                      language
                    });
                  }
                );
              }
            });
          }
        });
      }
      return segments;
    };
    /**
     * @description function that use to generate table field row
     * @return {element} combined table row
     * */
    const tableFiled = ({
      language,
      key,
      titlePath,
      testID,
      maxLength = 11 + Math.floor(10 / 3),
      shouldResetClient = ""
    }) => (
      <EABTableSection key={key}>
        <View>
          <TranslatedText style={Theme.subheadPrimary} path={titlePath} />
        </View>
        <View>
          <EABTextField
            testID={`FinancialEvalutaion__txt${testID}`}
            inputStyle={styles.tableFieldInputStyle}
            supportLabelPlaceholder={false}
            supportErrorMessage={false}
            placeholder={EABi18n({
              textStore,
              language,
              path: "fe.tableFieldPlaceholder"
            })}
            prefix="$"
            value={numberToCurrency({
              value: feData[key],
              decimal: 0
            })}
            onChange={value => {
              onChange({
                key,
                value: currencyToNumber(value),
                shouldResetClient
              });
            }}
            maxLength={maxLength}
          />
        </View>
      </EABTableSection>
    );
    const netWorthField = ({ key, language, labelPath, testID }) => {
      const currency = numberToCurrency({
        value: feData[key],
        decimal: 0
      });
      const maxLength = 11 + currency.replace(/[^,]/g, "").length;

      return (
        <EABTextField
          key={key}
          placeholder={EABi18n({
            textStore,
            language,
            path: labelPath
          })}
          testID={`FinancialEvalutaion__txt${testID}`}
          prefix="$"
          value={currency}
          onChange={newValue => {
            let value;
            if (newValue.length !== maxLength) {
              value = currencyToNumber(newValue) || 0;
            } else {
              value =
                currencyToNumber(newValue.substring(0, newValue.length - 1)) ||
                0;
            }

            onChange({
              key,
              value
            });
          }}
          maxLength={11 + currency.replace(/[^,]/g, "").length}
        />
      );
    };
    /**
     * @description function that use to generate common budget field
     * @return {element} combined budget field
     * */
    const commonBudgetField = ({ language, key, error, testID }) => (
      <EABTextField
        key={key}
        testID={`FinancialEvalutaion__txt${testID}`}
        style={styles.budgetField}
        placeholder={EABi18n({
          textStore,
          language,
          path: "fe.myBudget.fieldTitle"
        })}
        prefix="$"
        value={numberToCurrency({
          value: feData[key],
          decimal: 0
        })}
        onChange={value => {
          onChange({
            key,
            value: currencyToNumber(value) || 0
          });
        }}
        maxLength={11 + Math.floor(10 / 3)}
        isError={error.hasError}
        hintText={EABi18n({
          textStore,
          language,
          path: error.message
        })}
      />
    );
    /**
     * @description render net worth section
     * @return {array} element array
     * */
    const renderNetWorth = language => {
      const assetsFields = [
        {
          key: "savAcc",
          testID: "SavingAccount",
          labelPath: "fe.netWorth.savingsAccount"
        },
        {
          key: "fixDeposit",
          testID: "FixedDeposits",
          labelPath: "fe.netWorth.fixedDeposits"
        },
        {
          key: "invest",
          testID: "Investment",
          labelPath: "fe.netWorth.mutualFundDirectInvestment"
        },
        {
          key: "property",
          testID: "Property",
          labelPath: "fe.netWorth.property"
        },
        {
          key: "car",
          testID: "Car",
          labelPath: "fe.netWorth.car"
        },
        {
          key: "cpfOa",
          testID: "CPF-OA",
          labelPath: "fe.netWorth.CPFOA"
        },
        {
          key: "cpfSa",
          testID: "CPF-SA",
          labelPath: "fe.netWorth.CPFSA"
        },
        {
          key: "cpfMs",
          testID: "CPF-Medisave",
          labelPath: "fe.netWorth.CPFMedisave"
        },
        {
          key: "srs",
          testID: "SRS",
          labelPath: "fe.netWorth.SRS"
        }
      ];

      const liabilityFields = [
        {
          key: "mortLoan",
          testID: "MortgageLoan",
          labelPath: "fe.netWorth.mortgageLoan"
        },
        {
          key: "motLoan",
          testID: "MotorLoan",
          labelPath: "fe.netWorth.motorLoan"
        },
        {
          key: "eduLoan",
          testID: "EducationLoan",
          labelPath: "fe.netWorth.educationLoan"
        }
      ];

      const otherAssetCurrency = numberToCurrency({
        value: feData.otherAsset,
        decimal: 0
      });
      const otherLiabilitesCurrency = numberToCurrency({
        value: feData.otherLiabilites,
        decimal: 0
      });

      return [
        <View key="total" style={styles.netWorthTotalWrapper}>
          <Text style={Theme.bodyPrimary}>
            {`${EABi18n({
              textStore,
              language,
              path: "fe.netWorth.totalTitle"
            })}: `}
            <Text
              style={
                feData.netWorth < 0 ? Theme.bodyNegative : Theme.bodyPositive
              }
            >
              ${numberToCurrency({
                value: feData.netWorth,
                decimal: 0,
                maxValue: 999999999999999999
              })}
            </Text>
          </Text>
        </View>,
        <View key="net worth form" style={styles.netWorthForm}>
          <View style={styles.assetsWrapper}>
            <TranslatedText
              style={styles.assetsTitle}
              path="fe.netWorthChart.assets"
              funcOnText={text => text.toUpperCase()}
            />
            {assetsFields.map(({ key, labelPath, testID }) =>
              netWorthField({ key, labelPath, language, testID })
            )}
            <EABTextField
              testID="FinancialEvalutaion__txtAssetsOther"
              placeholder={EABi18n({
                textStore,
                language,
                path: "fe.netWorth.otherTitle"
              })}
              value={feData.otherAssetTitle}
              onChange={value => {
                onChange({ key: "otherAssetTitle", value });
              }}
              maxLength={30}
              isError={editingClientErrors.otherAssetTitleError.hasError}
              hintText={EABi18n({
                textStore,
                language,
                path: editingClientErrors.otherAssetTitleError.message
              })}
              isRequired={
                feData.otherAssetTitle !== "" || feData.otherAsset !== 0
              }
            />
            <EABTextField
              testID="FinancialEvalutaion__txtAssetsOtherValue"
              placeholder={
                feData.otherAssetTitle ||
                EABi18n({
                  textStore,
                  language,
                  path: "fe.netWorth.other"
                })
              }
              prefix="$"
              value={otherAssetCurrency}
              onChange={value => {
                onChange({
                  key: "otherAsset",
                  value: currencyToNumber(value) || 0
                });
              }}
              maxLength={11 + otherAssetCurrency.replace(/[^,]/g, "").length}
              isError={editingClientErrors.otherAsset.hasError}
              hintText={EABi18n({
                textStore,
                language,
                path: editingClientErrors.otherAsset.message
              })}
              isRequired={
                feData.otherAssetTitle !== "" || feData.otherAsset !== 0
              }
            />
          </View>
          <NetWorthChart
            assets={feData.assets}
            liabilities={feData.liabilities}
          />
          <View style={styles.liabilitiesWrapper}>
            <TranslatedText
              style={styles.liabilitiesTitle}
              path="fe.netWorthChart.liabilities"
              funcOnText={text => text.toUpperCase()}
            />
            {liabilityFields.map(({ key, labelPath, testID }) =>
              netWorthField({ key, labelPath, language, testID })
            )}
            <EABTextField
              testID="FinancialEvalutaion__txtLoansOther"
              placeholder={EABi18n({
                textStore,
                language,
                path: "fe.netWorth.otherTitle"
              })}
              value={feData.otherLiabilitesTitle}
              onChange={value => {
                onChange({ key: "otherLiabilitesTitle", value });
              }}
              maxLength={30}
              isError={editingClientErrors.otherLiabilitesTitleError.hasError}
              hintText={EABi18n({
                textStore,
                language,
                path: editingClientErrors.otherLiabilitesTitleError.message
              })}
              isRequired={
                feData.otherLiabilitesTitle !== "" ||
                feData.otherLiabilites !== 0
              }
            />
            <EABTextField
              testID="FinancialEvalutaion__txtLoansOtherValue"
              placeholder={
                feData.otherLiabilitesTitle ||
                EABi18n({
                  textStore,
                  language,
                  path: "fe.netWorth.other"
                })
              }
              prefix="$"
              value={otherLiabilitesCurrency}
              onChange={value => {
                onChange({
                  key: "otherLiabilites",
                  value: currencyToNumber(value) || 0
                });
              }}
              maxLength={
                11 + otherLiabilitesCurrency.replace(/[^,]/g, "").length
              }
              isError={editingClientErrors.otherLiabilites.hasError}
              hintText={EABi18n({
                textStore,
                language,
                path: editingClientErrors.otherLiabilites.message
              })}
              isRequired={
                feData.otherLiabilitesTitle !== "" ||
                feData.otherLiabilites !== 0
              }
            />
          </View>
        </View>,
        relationshipObject.relationship === "owner" &&
        feData.assets === 0 &&
        feData.liabilities === 0 ? (
          <View key="noAL" style={styles.emptyWrapper}>
            <View style={styles.emptyTitleWrapper}>
              <Text style={Theme.bodyPrimary}>
                {EABi18n({
                  textStore,
                  language,
                  path: "fe.netWorth.noAL.title"
                })}
                <Text style={Theme.bodyNegative}>*</Text>
              </Text>
              <TranslatedText
                style={Theme.bodySecondary}
                path="fe.netWorth.noAL.description"
              />
            </View>
            <EABTextBox
              testID="FinancialEvalutaion__txtRelationshipReason"
              placeholder={EABi18n({
                textStore,
                language,
                path: "fe.reasonPlaceholder"
              })}
              value={feData.noALReason}
              onChange={value => {
                onChange({ key: "noALReason", value });
              }}
              maxLength={300}
            />
            {editingClientErrors.noALReason.hasError ? (
              <View style={styles.errorRow}>
                <TranslatedText
                  style={Theme.fieldTextOrHelperTextNegative}
                  path={editingClientErrors.noALReason.message}
                />
              </View>
            ) : null}
          </View>
        ) : null
      ];
    };
    /**
     * @description render existing insurance portfolio section
     * @return {array} element array
     * */
    const renderExistingInsurancePortfolio = language => [
      <View key="EIP" style={styles.EIPWrapper}>
        <View style={styles.sumAssuredWrapper}>
          <EABTable style={styles.table} tableConfig={defaultTableConfig}>
            {tableFiled({
              language,
              key: "lifeInsProt",
              testID: "LifeInsuranceProtection",
              titlePath: "fe.existingInsurancePortfolio.sumAssured.LIP"
            })}
            {tableFiled({
              language,
              key: "retirePol",
              testID: "RetirementPolicy",
              titlePath: "fe.existingInsurancePortfolio.sumAssured.RP"
            })}
            {tableFiled({
              language,
              key: "ednowSavPol",
              testID: "EndowmentPolicy",
              titlePath: "fe.existingInsurancePortfolio.sumAssured.EP"
            })}
            {tableFiled({
              language,
              key: "eduFund",
              testID: "EducationFund",
              titlePath: "fe.existingInsurancePortfolio.sumAssured.EF"
            })}
            {tableFiled({
              language,
              key: "invLinkPol",
              testID: "InvestmentLinkedPolicies",
              titlePath: "fe.existingInsurancePortfolio.sumAssured.ILP"
            })}
            {tableFiled({
              language,
              key: "disIncomeProt",
              testID: "DisabilityIncomeProtection",
              titlePath: "fe.existingInsurancePortfolio.sumAssured.DI"
            })}
            {tableFiled({
              language,
              key: "ciPort",
              testID: "CriticalIllnessProtection",
              titlePath: "fe.existingInsurancePortfolio.sumAssured.CI"
            })}
            {tableFiled({
              language,
              key: "personAcc",
              testID: "PersonalAccident",
              titlePath: "fe.existingInsurancePortfolio.sumAssured.PA"
            })}
            <EABTableSection key="hospNSurg">
              <View>
                <TranslatedText
                  style={Theme.subheadPrimary}
                  path="fe.existingInsurancePortfolio.sumAssured.HS"
                />
              </View>
              <View>
                <EABTextField
                  testID="FinancialEvalutaion__txtHospital&Surgical"
                  inputStyle={styles.tableFieldInputStyle}
                  supportLabelPlaceholder={false}
                  supportErrorMessage={false}
                  value={feData.hospNSurg}
                  onChange={value => {
                    onChange({
                      key: "hospNSurg",
                      value
                    });
                  }}
                  maxLength={50}
                />
              </View>
            </EABTableSection>
          </EABTable>
          {editingClientErrors.assured.hasError ? (
            <View style={styles.errorRow}>
              <TranslatedText
                style={Theme.fieldTextOrHelperTextNegative}
                path={editingClientErrors.assured.message}
              />
            </View>
          ) : null}
        </View>
        <View style={styles.EIPRightWrapper}>
          <View style={styles.EIPSectionTitleWrapper}>
            <Text style={Theme.tagLine1Primary}>
              {EABi18n({
                textStore,
                language,
                path: "fe.existingInsurancePortfolio.insurancePremiums.title"
              })}
              {feData.eiPorf !== 0 ? (
                <Text style={Theme.tagLine1Negative}>*</Text>
              ) : null}
            </Text>
          </View>
          <View style={styles.annualPremiumsWrapper}>
            <View style={styles.EIPSectionTitleWrapper}>
              <TranslatedText
                style={Theme.bodyPrimary}
                path="fe.existingInsurancePortfolio.annualPremiums.title"
              />
            </View>
            <EABTable style={styles.table} tableConfig={defaultTableConfig}>
              {tableFiled({
                language,
                key: "pAInsPrem",
                testID: "InsurancePremiumsProposer",
                titlePath:
                  "fe.existingInsurancePortfolio.annualPremiums.proposer",
                shouldResetClient: "owner"
              })}
              {profile.marital === "M"
                ? tableFiled({
                    language,
                    key: "sAInsPrem",
                    testID: "InsurancePremiumsSpouse",
                    titlePath:
                      "fe.existingInsurancePortfolio.annualPremiums.spouse",
                    shouldResetClient: "spouse"
                  })
                : null}
              {tableFiled({
                language,
                key: "oAInsPrem",
                testID: "InsurancePremiumsOther",
                titlePath: "fe.existingInsurancePortfolio.annualPremiums.other"
              })}
            </EABTable>
          </View>
          <View style={styles.otherPremiums}>
            <View style={styles.EIPSectionTitleWrapper}>
              <TranslatedText
                style={Theme.bodyPrimary}
                path="fe.existingInsurancePortfolio.otherPremiums.title"
              />
            </View>
            <EABTable style={styles.table} tableConfig={defaultTableConfig}>
              {tableFiled({
                language,
                key: "caInsPrem",
                testID: "OtherPremiumsSinglePremiums(Cash)",
                titlePath: "fe.existingInsurancePortfolio.otherPremiums.SPC"
              })}
              {tableFiled({
                language,
                key: "csInsPrem",
                testID: "OtherPremiumsSinglePremiums(CPF/SRS)",
                titlePath: "fe.existingInsurancePortfolio.otherPremiums.SPCS"
              })}
              {tableFiled({
                language,
                key: "mdInsPrem",
                testID: "OtherPremiums-Premiums",
                titlePath: "fe.existingInsurancePortfolio.otherPremiums.P"
              })}
              {tableFiled({
                language,
                key: "fpInsPrem",
                testID: "OtherPremiumsLimitedPayPremiums",
                titlePath: "fe.existingInsurancePortfolio.otherPremiums.LPP"
              })}
            </EABTable>
          </View>
          {editingClientErrors.premiums.hasError ? (
            <View style={styles.errorRow}>
              <TranslatedText
                style={Theme.fieldTextOrHelperTextNegative}
                path={editingClientErrors.premiums.message}
              />
            </View>
          ) : null}
        </View>
      </View>,
      relationshipObject.relationship === "owner" &&
      feData.eiPorf === 0 &&
      feData.aInsPrem === 0 &&
      feData.sInsPrem === 0 ? (
        <View key="noEI" style={styles.emptyWrapper}>
          <View style={styles.emptyTitleWrapper}>
            <Text style={Theme.bodyPrimary}>
              {EABi18n({
                textStore,
                language,
                path: "fe.existingInsurancePortfolio.noEI.title"
              })}
              <Text style={Theme.bodyNegative}>*</Text>
            </Text>
            <TranslatedText
              style={Theme.bodySecondary}
              path="fe.existingInsurancePortfolio.noEI.description"
            />
          </View>
          <EABTextBox
            testID="FinancialEvalutaion__txtNoEIReason"
            placeholder={EABi18n({
              textStore,
              language,
              path: "fe.reasonPlaceholder"
            })}
            value={feData.noEIReason}
            onChange={value => {
              onChange({ key: "noEIReason", value });
            }}
            maxLength={300}
          />
          {editingClientErrors.noEIReason.hasError ? (
            <View style={styles.errorRow}>
              <TranslatedText
                style={Theme.fieldTextOrHelperTextNegative}
                path={editingClientErrors.noEIReason.message}
              />
            </View>
          ) : null}
        </View>
      ) : null
    ];
    /**
     * @description render cash flow section
     * @return {array} element array
     * */
    const renderCashFlow = language => [
      <View key="cash flow" style={styles.CFWrapper}>
        <View style={styles.disposableWrapper}>
          <View style={styles.disposableTitleWrapper}>
            <TranslatedText
              style={Theme.bodyPrimary}
              path="fe.cashFlow.monthlyDisposableIncome"
              funcOnText={text => `${text}:`}
            />
            <TranslatedText
              style={Theme.bodyPrimary}
              path="fe.cashFlow.annualDisposableIncome"
              funcOnText={text => `${text}:`}
            />
          </View>
          <View>
            <Text
              style={
                feData.mDisposeIncome < 0
                  ? Theme.bodyNegative
                  : Theme.bodyPositive
              }
            >
              ${numberToCurrency({
                value: feData.mDisposeIncome,
                decimal: 0
              })}
            </Text>
            <Text
              style={
                feData.aDisposeIncome < 0
                  ? Theme.bodyNegative
                  : Theme.bodyPositive
              }
            >
              ${numberToCurrency({
                value: feData.aDisposeIncome,
                decimal: 0,
                maxValue: 999999999999999
              })}
            </Text>
          </View>
        </View>
        <View style={styles.CFForm}>
          <EABTable
            style={styles.CFFormTableFirst}
            tableConfig={defaultTableConfig}
          >
            <EABTableSection
              key="monthly income"
              tableConfig={displayTableConfig}
            >
              <View>
                <View>
                  <TranslatedText
                    style={Theme.subheadPrimary}
                    path="fe.cashFlow.monthlyIncomeOrCashAllowance"
                  />
                </View>
                <View>
                  <Text style={Theme.bodyPositive}>
                    ${numberToCurrency({
                      value: feData.mIncome,
                      decimal: 0
                    })}
                  </Text>
                </View>
              </View>
            </EABTableSection>
            {tableFiled({
              language,
              key: "lessMCPF",
              testID: "LessMonthlyCPF",
              titlePath: "fe.cashFlow.lessMonthlyCPF"
            })}
            <EABTableSection
              key="net monthly income"
              tableConfig={displayTableConfig}
            >
              <View>
                <View>
                  <TranslatedText
                    style={Theme.bodyBrand}
                    path="fe.cashFlow.netMonthlyIncome"
                  />
                </View>
                <View>
                  <Text style={Theme.bodyPositive}>
                    ${numberToCurrency({
                      value: feData.netMIcncome,
                      decimal: 0
                    })}
                  </Text>
                </View>
              </View>
            </EABTableSection>
            {tableFiled({
              language,
              key: "aBonus",
              testID: "AnnualBonus",
              titlePath: "fe.cashFlow.annualBonusAdditionalIncome"
            })}
            <EABTableSection
              key=""
              tableConfig={displayTableConfig}
              style={styles.emptyTableSection}
            >
              <View />
            </EABTableSection>
            <EABTableSection
              key="annual income"
              tableConfig={displayTableConfig}
              style={styles.annualIncome}
            >
              <View>
                <View>
                  <TranslatedText
                    style={Theme.bodyBrand}
                    path="fe.cashFlow.totalAnnualIncome"
                  />
                </View>
                <View>
                  <Text style={Theme.bodyPositive}>
                    ${numberToCurrency({
                      value: feData.totAIncome,
                      decimal: 0,
                      maxValue: 999999999999999
                    })}
                  </Text>
                </View>
              </View>
            </EABTableSection>
          </EABTable>
          <EABTable style={styles.CFFormTable} tableConfig={defaultTableConfig}>
            {tableFiled({
              language,
              key: "personExpenses",
              testID: "PersonalExpenses",
              titlePath: "fe.cashFlow.personalExpenses"
            })}
            {tableFiled({
              language,
              key: "householdExpenses",
              testID: "HouseholdExpenses",
              titlePath: "fe.cashFlow.householdExpenses"
            })}
            <EABTableSection key="insurance" tableConfig={displayTableConfig}>
              <View>
                <View style={styles.insurancePremiumsTitleWrapper}>
                  <TranslatedText
                    style={Theme.subheadPrimary}
                    path="fe.cashFlow.insurancePremiums"
                  />
                  <PopoverTooltip
                    text={EABi18n({
                      textStore,
                      language,
                      path: "fe.cashFlow.insurancePremiumsToolTip"
                    })}
                  />
                </View>
                <View>
                  <Text style={Theme.bodyNegative}>
                    ${numberToCurrency({
                      value: feData.insurancePrem,
                      decimal: 2
                    })}
                  </Text>
                </View>
              </View>
            </EABTableSection>
            {tableFiled({
              language,
              key: "regSav",
              testID: "RegularSavings",
              titlePath: "fe.cashFlow.regularSavings"
            })}
            <EABTableSection key="other title">
              <View>
                <EABTextField
                  inputStyle={styles.tableFieldInputStyle}
                  supportErrorMessage={false}
                  testID="FinancialEvalutaion__txtExpenseOther"
                  placeholder={EABi18n({
                    textStore,
                    language,
                    path: "fe.netWorth.otherTitle"
                  })}
                  value={feData.otherExpenseTitle}
                  onChange={value => {
                    onChange({
                      key: "otherExpenseTitle",
                      value
                    });
                  }}
                  isRequired={
                    (feData.otherExpenseTitle !== "" &&
                      feData.otherExpense === 0) ||
                    (feData.otherExpenseTitle === "" &&
                      feData.otherExpense !== 0)
                  }
                  isError={editingClientErrors.otherExpenseTitle.hasError}
                  maxLength={30}
                />
              </View>
              <View>
                <EABTextField
                  inputStyle={styles.tableFieldInputStyle}
                  testID="FinancialEvalutaion__txtExpenseOtherCost"
                  supportErrorMessage={false}
                  placeholder=""
                  prefix="$"
                  value={numberToCurrency({
                    value: feData.otherExpense,
                    decimal: 0
                  })}
                  onChange={value => {
                    onChange({
                      key: "otherExpense",
                      value: currencyToNumber(value) || 0
                    });
                  }}
                  isError={editingClientErrors.otherExpense.hasError}
                  maxLength={11 + Math.floor(10 / 3)}
                />
              </View>
            </EABTableSection>
            <EABTableSection
              key="monthly expenses"
              tableConfig={displayTableConfig}
            >
              <View>
                <View>
                  <TranslatedText
                    style={Theme.bodyBrand}
                    path="fe.cashFlow.totalMonthlyExpenses"
                  />
                </View>
                <View>
                  <Text style={Theme.bodyNegative}>
                    ${numberToCurrency({
                      value: feData.totMExpense,
                      decimal: 2
                    })}
                  </Text>
                </View>
              </View>
            </EABTableSection>
            <EABTableSection
              key="annual expenses"
              tableConfig={displayTableConfig}
            >
              <View>
                <View>
                  <TranslatedText
                    style={Theme.bodyBrand}
                    path="fe.cashFlow.totalAnnualExpenses"
                  />
                </View>
                <View>
                  <Text style={Theme.bodyNegative}>
                    ${numberToCurrency({
                      value: feData.totAExpense,
                      decimal: 0,
                      maxValue: 999999999999999
                    })}
                  </Text>
                </View>
              </View>
            </EABTableSection>
          </EABTable>
        </View>
      </View>,
      relationshipObject.relationship === "owner" &&
      (feData.totAIncome === 0 || feData.totAExpense === 0) ? (
        <View key="noCF" style={styles.emptyWrapper}>
          <View style={styles.emptyTitleWrapper}>
            <Text style={Theme.bodyPrimary}>
              {EABi18n({
                textStore,
                language,
                path: "fe.cashFlow.noCF.title"
              })}
              <Text style={Theme.bodyNegative}>*</Text>
            </Text>
          </View>
          <EABTextBox
            testID="FinancialEvalutaion__txtNoCFReason"
            placeholder={EABi18n({
              textStore,
              language,
              path: "fe.reasonPlaceholder"
            })}
            value={feData.noCFReason}
            onChange={value => {
              onChange({ key: "noCFReason", value });
            }}
            maxLength={300}
          />
          {editingClientErrors.noCFReason.hasError ? (
            <View style={styles.errorRow}>
              <TranslatedText
                style={Theme.fieldTextOrHelperTextNegative}
                path={editingClientErrors.noCFReason.message}
              />
            </View>
          ) : null}
        </View>
      ) : null,
      relationshipObject.relationship === "owner" ? (
        <View key="additionComment" style={styles.additionCommentWrapper}>
          <View style={styles.additionCommentTitle}>
            <TranslatedText
              style={Theme.bodyPrimary}
              path="fe.cashFlow.additionalComments.title"
            />
          </View>
          <EABTextBox
            testID="FinancialEvalutaion__txtAdditionalComments"
            placeholder={EABi18n({
              textStore,
              language,
              path: "fe.cashFlow.additionalComment.placeholder"
            })}
            value={feData.additionComment}
            onChange={value => {
              onChange({ key: "additionComment", value });
            }}
            maxLength={300}
          />
        </View>
      ) : null
    ];
    /**
     * @description render My Budget (Annual Disposable Income Committed for
     *   Regular Premium Budget)
     * @return {array} element array
     * */
    const renderMyBudgeADICRPB = language => [
      <EABTextField
        key="aRegPremBudget"
        testID="FinancialEvalutaion__txtMyBudget"
        style={styles.budgetField}
        placeholder={EABi18n({
          textStore,
          language,
          path: "fe.myBudget.fieldTitle"
        })}
        prefix="$"
        value={numberToCurrency({
          value: feData.aRegPremBudget,
          decimal: 0
        })}
        onChange={value => {
          onChange({
            key: "aRegPremBudget",
            value: currencyToNumber(value) || 0
          });
        }}
        maxLength={11 + Math.floor(10 / 3)}
        isError={editingClientErrors.budget.hasError}
        hintText={EABi18n({
          textStore,
          language,
          path: editingClientErrors.budget.message
        })}
      />,
      ...(parseFloat(feData.aRegPremBudget) !== 0
        ? [
            <EABTextField
              key="sourceOfFunds"
              style={styles.sourceOfFunds}
              testID="FinancialEvalutaion__txtMyBudgetSourceOfFunds"
              placeholder={EABi18n({
                textStore,
                language,
                path: "fe.myBudget.regular.sourceOfFunds"
              })}
              value={feData.aRegPremBudgetSrc}
              onChange={value => {
                onChange({
                  key: "aRegPremBudgetSrc",
                  value
                });
              }}
              maxLength={30}
              isError={editingClientErrors.aRegPremBudgetSrc.hasError}
              hintText={EABi18n({
                textStore,
                language,
                path: editingClientErrors.aRegPremBudgetSrc.message
              })}
              isRequired
            />,
            <View key="additionComment" style={styles.additionCommentWrapper}>
              <Text style={Theme.bodyPrimary}>
                {EABi18n({
                  textStore,
                  language,
                  path: "fe.myBudget.regular.confirmBudget"
                })}
                {feData.mDisposeIncome === 0 ? (
                  <Text style={Theme.bodyNegative}>*</Text>
                ) : null}
              </Text>
              {editingClientErrors.confirmBudget.hasError ? (
                <TranslatedText
                  style={Theme.fieldTextOrHelperTextNegative}
                  path={editingClientErrors.confirmBudget.message}
                />
              ) : null}
              <EABTextSelection
                style={styles.budgetJustify}
                testID="FinancialEvalutaion"
                options={[
                  {
                    key: "yes",
                    testID: "MyBudgetYes",

                    title: EABi18n({
                      path: "fe.myBudget.yes",
                      language,
                      textStore
                    }),
                    value: "Y",
                    isSelected: feData.confirmBudget === "Y"
                  },
                  {
                    key: "no",
                    testID: "MyBudgetNo",
                    title: EABi18n({
                      path: "fe.myBudget.no",
                      language,
                      textStore
                    }),
                    value: "N",
                    isSelected: feData.confirmBudget === "N"
                  }
                ]}
                editable={feData.mDisposeIncome === 0}
                onPress={({ value }) => {
                  onChange({ key: "confirmBudget", value });
                }}
              />
              {feData.confirmBudget === "Y" ? (
                <EABTextBox
                  style={styles.budgetJustify}
                  testID="FinancialEvalutaion__txtConfirmMyBudget"
                  placeholder={EABi18n({
                    textStore,
                    language,
                    path: "fe.reasonPlaceholder"
                  })}
                  value={feData.confirmBudgetReason}
                  onChange={value => {
                    onChange({ key: "confirmBudgetReason", value });
                  }}
                  maxLength={300}
                />
              ) : null}
              {editingClientErrors.confirmBudgetReason.hasError ? (
                <View style={styles.errorRow}>
                  <TranslatedText
                    style={Theme.fieldTextOrHelperTextNegative}
                    path={editingClientErrors.confirmBudgetReason.message}
                  />
                </View>
              ) : null}
            </View>
          ]
        : [])
    ];
    /**
     * @description render My Budget (Single Premium)
     * @return {array} element array
     * */
    const renderMyBudgeSingle = language => [
      <EABTextField
        key="aRegPremBudget"
        testID="FinancialEvalutaion__txtMyBudgetSingle"
        style={styles.budgetField}
        placeholder={EABi18n({
          textStore,
          language,
          path: "fe.myBudget.fieldTitle"
        })}
        prefix="$"
        value={numberToCurrency({
          value: feData.singPrem,
          decimal: 0
        })}
        onChange={value => {
          onChange({
            key: "singPrem",
            value: currencyToNumber(value) || 0
          });
        }}
        isError={editingClientErrors.budget.hasError}
        hintText={EABi18n({
          textStore,
          language,
          path: editingClientErrors.budget.message
        })}
        maxLength={11 + Math.floor(10 / 3)}
      />,
      ...(parseFloat(feData.singPrem) !== 0
        ? [
            <EABTextField
              key="sourceOfFunds"
              testID="FinancialEvalutaion__txtMyBudgetSingleSourceOfFunds"
              style={styles.sourceOfFunds}
              placeholder={EABi18n({
                textStore,
                language,
                path: "fe.myBudget.single.sourceOfFunds"
              })}
              value={feData.singPremSrc}
              onChange={value => {
                onChange({
                  key: "singPremSrc",
                  value
                });
              }}
              maxLength={30}
              isError={editingClientErrors.singPremSrc.hasError}
              hintText={EABi18n({
                textStore,
                language,
                path: editingClientErrors.singPremSrc.message
              })}
              isRequired
            />,
            <View key="additionComment" style={styles.additionCommentWrapper}>
              <Text style={Theme.bodyPrimary}>
                {EABi18n({
                  textStore,
                  language,
                  path: "fe.myBudget.single.confirmBudget"
                })}
                {feData.savAcc + feData.fixDeposit + feData.invest === 0 ? (
                  <Text style={Theme.bodyNegative}>*</Text>
                ) : null}
              </Text>
              {editingClientErrors.confirmSingPremBudget.hasError ? (
                <TranslatedText
                  style={Theme.fieldTextOrHelperTextNegative}
                  path={editingClientErrors.confirmSingPremBudget.message}
                />
              ) : null}
              <EABTextSelection
                style={styles.budgetJustify}
                testID="FinancialEvalutaion"
                options={[
                  {
                    key: "yes",
                    testID: "MyBudgetSingleYes",
                    title: EABi18n({
                      path: "fe.myBudget.yes",
                      language,
                      textStore
                    }),
                    value: "Y",
                    isSelected: feData.confirmSingPremBudget === "Y"
                  },
                  {
                    key: "no",
                    testID: "MyBudgetSingleNo",
                    title: EABi18n({
                      path: "fe.myBudget.no",
                      language,
                      textStore
                    }),
                    value: "N",
                    isSelected: feData.confirmSingPremBudget === "N"
                  }
                ]}
                editable={
                  feData.savAcc + feData.fixDeposit + feData.invest === 0
                }
                onPress={({ value }) => {
                  onChange({ key: "confirmSingPremBudget", value });
                }}
              />
              {feData.confirmSingPremBudget === "Y" ? (
                <EABTextBox
                  style={styles.budgetJustify}
                  testID="FinancialEvalutaion__txtConfirmSingPremBudget"
                  placeholder={EABi18n({
                    textStore,
                    language,
                    path: "fe.reasonPlaceholder"
                  })}
                  value={feData.confirmSingPremBudgetReason}
                  onChange={value => {
                    onChange({ key: "confirmSingPremBudgetReason", value });
                  }}
                  maxLength={300}
                />
              ) : null}
              {editingClientErrors.confirmSingPremBudgetReason.hasError ? (
                <View style={styles.errorRow}>
                  <TranslatedText
                    style={Theme.fieldTextOrHelperTextNegative}
                    path={
                      editingClientErrors.confirmSingPremBudgetReason.message
                    }
                  />
                </View>
              ) : null}
            </View>
          ]
        : [])
    ];
    /**
     * @description render forceIncome
     * @return {array} element array
     * */
    const renderForceIncome = language => [
      <EABTextSelection
        testID="FinancialEvalutaion"
        key="force income button"
        options={[
          {
            key: "yes",
            testID: "ForceIncomeYes",
            title: EABi18n({
              path: "fe.myBudget.yes",
              language,
              textStore
            }),
            value: "Y",
            isSelected: feData.forceIncome === "Y"
          },
          {
            key: "no",
            testID: "ForceIncomeNo",
            title: EABi18n({
              path: "fe.myBudget.no",
              language,
              textStore
            }),
            value: "N",
            isSelected: feData.forceIncome === "N"
          }
        ]}
        onPress={({ value }) => {
          onChange({ key: "forceIncome", value });
        }}
      />,
      editingClientErrors.forceIncome.hasError ? (
        <View style={styles.errorRow}>
          <TranslatedText
            style={Theme.fieldTextOrHelperTextNegative}
            path={editingClientErrors.forceIncome.message}
          />
        </View>
      ) : null,
      feData.forceIncome === "Y" ? (
        <View key="force income reason" style={styles.emptyWrapper}>
          <View style={styles.additionCommentTitle}>
            <TranslatedText
              style={Theme.bodyPrimary}
              path="fe.plansOrFactors.justifyTitle"
            />
          </View>
          <EABTextBox
            testID="FinancialEvalutaion__txtJusifyTitle"
            placeholder={EABi18n({
              textStore,
              language,
              path: "fe.reasonPlaceholder"
            })}
            value={feData.forceIncomeReason}
            onChange={value => {
              onChange({ key: "forceIncomeReason", value });
            }}
            maxLength={300}
          />
          {editingClientErrors.forceIncomeReason.hasError ? (
            <View style={styles.errorRow}>
              <TranslatedText
                style={Theme.fieldTextOrHelperTextNegative}
                path={editingClientErrors.forceIncomeReason.message}
              />
            </View>
          ) : null}
        </View>
      ) : null
    ];
    /**
     * @description Indicate which fe part should be displayed. Do not need to
     *   do any relationship checking because we had already checked in the
     *   getter "headerSegments".
     * @requires relationshipObject
     * @return {array} fe section array
     * */
    const feSection = () => {
      switch (relationshipObject.relationship) {
        case "owner":
          return [
            {
              titlePath: "fe.netWorth.title",
              sectionError: feErrors[relationshipObject.cid].netWorth,
              isMandatory: false,
              tooltip: {
                hasToolTip: false
              },
              renderFunction: renderNetWorth,
              dataSource: "owner"
            },
            {
              titlePath: "fe.existingInsurancePortfolio.title",
              sectionError: null,
              isMandatory: feData.aInsPrem !== 0,
              tooltip: {
                hasToolTip: true,
                toolTipPath: "fe.existingInsurancePortfolio.toolTip",
                tooltipSize: [400, 180]
              },
              renderFunction: renderExistingInsurancePortfolio
            },
            {
              titlePath: "fe.cashFlow.title",
              sectionError: null,
              isMandatory: false,
              tooltip: {
                hasToolTip: false
              },
              renderFunction: renderCashFlow
            },
            {
              titlePath: "fe.myBudget.regular.title",
              sectionError: null,
              isMandatory: false,
              tooltip: {
                hasToolTip: false
              },
              renderFunction: renderMyBudgeADICRPB
            },
            {
              titlePath: "fe.myBudget.single.title",
              sectionError: null,
              isMandatory: false,
              tooltip: {
                hasToolTip: false
              },
              renderFunction: renderMyBudgeSingle
            },
            {
              titlePath: "fe.myBudget.cpfOA.title",
              sectionError: null,
              isMandatory: false,
              tooltip: {
                hasToolTip: false
              },
              renderFunction: language =>
                commonBudgetField({
                  language,
                  key: "cpfOaBudget",
                  testID: "CPF-OA-Budge",
                  error: editingClientErrors.cpfOaBudget
                })
            },
            {
              titlePath: "fe.myBudget.cpfSA.title",
              sectionError: null,
              isMandatory: false,
              tooltip: {
                hasToolTip: false
              },
              renderFunction: language =>
                commonBudgetField({
                  language,
                  key: "cpfSaBudget",
                  testID: "CPF-SA-Budge",
                  error: editingClientErrors.cpfSaBudget
                })
            },
            {
              titlePath: "fe.myBudget.cpfM.title",
              sectionError: null,
              isMandatory: false,
              tooltip: {
                hasToolTip: false
              },
              renderFunction: language =>
                commonBudgetField({
                  language,
                  key: "cpfMsBudget",
                  testID: "CPF-Medisave-Budge",
                  error: editingClientErrors.cpfMsBudget
                })
            },
            {
              titlePath: "fe.myBudget.srs.title",
              sectionError: null,
              isMandatory: false,
              tooltip: {
                hasToolTip: false
              },
              renderFunction: language =>
                commonBudgetField({
                  language,
                  key: "srsBudget",
                  testID: "CPF-SRS-Budge",
                  error: editingClientErrors.srsBudget
                })
            },
            {
              titlePath: "fe.plansOrFactors.title",
              sectionError: null,
              isMandatory: true,
              tooltip: {
                hasToolTip: true,
                toolTipPath: "fe.plansOrFactors.toolTip",
                tooltipSize: [400, 150]
              },
              renderFunction: renderForceIncome
            }
          ];
        case "SPO": {
          return [
            {
              titlePath: "fe.netWorth.title",
              sectionError: null,
              isMandatory: false,
              tooltip: {
                hasToolTip: false
              },
              renderFunction: renderNetWorth
            },
            {
              titlePath: "fe.existingInsurancePortfolio.title",
              sectionError: null,
              isMandatory: feData.aInsPrem !== 0,
              tooltip: {
                hasToolTip: true,
                toolTipPath: "fe.existingInsurancePortfolio.toolTip",
                tooltipSize: [400, 180]
              },
              renderFunction: renderExistingInsurancePortfolio
            },
            {
              titlePath: "fe.cashFlow.title",
              sectionError: null,
              isMandatory: false,
              tooltip: {
                hasToolTip: false
              },
              renderFunction: renderCashFlow
            }
          ];
        }
        case "SON":
        case "DAU":
          return [
            {
              titlePath: "fe.existingInsurancePortfolio.title",
              sectionError: null,
              isMandatory: feData.aInsPrem !== 0,
              tooltip: {
                hasToolTip: true,
                toolTipPath: "fe.existingInsurancePortfolio.toolTip",
                tooltipSize: [400, 180]
              },
              renderFunction: renderExistingInsurancePortfolio
            }
          ];
        default:
          throw new Error(
            `feSection unexpected relationship "${
              relationshipObject.relationship
            }".`
          );
      }
    };

    return (
      <ContextConsumer>
        {({ language }) => (
          <LinearGradient
            {...Theme.LinearGradientProps}
            style={styles.container}
          >
            <View style={styles.header}>
              <View style={styles.titleBar}>
                <EABButton
                  containerStyle={Theme.headerLeft}
                  testID="FinancialEvaluation__btnSave"
                  onPress={() => {
                    saveHandledFE({ callback: closeDialog, language });
                  }}
                >
                  <TranslatedText
                    style={Theme.textButtonLabelNormalEmphasizedAccent}
                    path="button.save"
                  />
                </EABButton>
                <TranslatedText style={Theme.title} path="fe.title" />
                <EABButton
                  containerStyle={Theme.headerRight}
                  testID="FinancialEvaluation__btnDone"
                  onPress={() => {
                    saveHandledFE({ callback: closeDialog, language });
                  }}
                  isDisable={isFEError}
                >
                  <TranslatedText
                    style={Theme.textButtonLabelNormalEmphasizedAccent}
                    path="button.done"
                  />
                </EABButton>
              </View>
              <ScrollView
                testID="FinancialEvaluation__SegmentedControlScroll"
                style={styles.segmentWrapper}
                contentContainerStyle={styles.segmentScrollView}
                horizontal
                showsHorizontalScrollIndicator={false}
              >
                <EABSegmentedControl
                  segments={headerSegments(language)}
                  activatedKey={this.state.activeClientCID}
                />
              </ScrollView>
            </View>
            <KeyboardAwareScrollView extraScrollHeight={40}>
              <EABHUD isOpen={isLoading}>
                <ScrollView
                  testID="FinancialEvaluation__PageScroll"
                  style={styles.scrollView}
                  contentContainerStyle={styles.scrollViewContent}
                >
                  <View style={styles.mainWrapper}>
                    {feSection().map((section, index) => (
                      <View
                        key={section.titlePath}
                        style={
                          styles[
                            index === 0
                              ? "sectionWrapperFirst"
                              : "sectionWrapper"
                          ]
                        }
                      >
                        <View style={styles.titleRow}>
                          <View style={styles.titleWrapper}>
                            <Text style={Theme.tagLine1Primary}>
                              {`${index + 1}. ${EABi18n({
                                textStore,
                                language,
                                path: section.titlePath
                              })}`}
                              {section.isMandatory ? (
                                <Text style={Theme.tagLine1Negative}>*</Text>
                              ) : null}
                            </Text>
                            {section.tooltip.hasToolTip ? (
                              <PopoverTooltip
                                popoverOptions={{
                                  preferredContentSize:
                                    section.tooltip.tooltipSize
                                }}
                                text={EABi18n({
                                  textStore,
                                  language,
                                  path: section.tooltip.toolTipPath
                                })}
                              />
                            ) : null}
                          </View>
                          {section.sectionError !== null &&
                          section.sectionError.hasError ? (
                            <View style={styles.errorRow}>
                              <TranslatedText
                                style={Theme.fieldTextOrHelperTextNegative}
                                path={section.sectionError.message}
                              />
                            </View>
                          ) : null}
                        </View>
                        {section.renderFunction(language)}
                      </View>
                    ))}
                    <View style={styles.sectionWrapperFirst} />
                  </View>
                </ScrollView>
              </EABHUD>
            </KeyboardAwareScrollView>
            <View style={styles.footer}>
              <EABButton
                testID="FinancialEvalutaion__btnBackToPDA"
                buttonType={RECTANGLE_BUTTON_2}
                onPress={() => {
                  saveHandledFE({ callback: switchToPDA, language });
                }}
              >
                <TranslatedText
                  style={Theme.rectangleButtonStyle2Label}
                  path="fe.button.backToPersonalDataAcknowledgement"
                />
              </EABButton>
              <EABButton
                testID="FinancialEvalutaion__btnContunueToNA"
                buttonType={RECTANGLE_BUTTON_1}
                onPress={() => {
                  saveHandledFE({ callback: switchToNA, language });
                }}
                isDisable={isFEError}
              >
                <TranslatedText
                  style={Theme.rectangleButtonStyle1Label}
                  path="fe.button.continueToNeedsAnalysis"
                />
              </EABButton>
            </View>
          </LinearGradient>
        )}
      </ContextConsumer>
    );
  }
}

FinancialEvaluation.propTypes = {
  profile: REDUCER_TYPE_CHECK[CLIENT].profile.isRequired,
  dependantProfiles: REDUCER_TYPE_CHECK[CLIENT].dependantProfiles.isRequired,
  pda: REDUCER_TYPE_CHECK[FNA].pda.isRequired,
  fe: REDUCER_TYPE_CHECK[FNA].fe.isRequired,
  feErrors: REDUCER_TYPE_CHECK[FNA].feErrors.isRequired,
  saveFE: PropTypes.func.isRequired,
  closeDialog: PropTypes.func.isRequired,
  switchToNA: PropTypes.func.isRequired,
  switchToPDA: PropTypes.func.isRequired,
  textStore: APP_REDUCER_TYPE_CHECK[TEXT_STORE].isRequired,
  updateFE: PropTypes.func.isRequired
};
