import PropTypes from "prop-types";
import React, { Component } from "react";
import { View } from "react-native";
import EABButton from "../../../../components/EABButton";
import TranslatedText from "../../../../containers/TranslatedText";
import Theme from "../../../../theme";
import cleanClientData from "../../../../utilities/cleanClientData";
import { ContextConsumer } from "../../../../context";

/**
 * HeaderLeft
 * @param {object} navigation - react navigation props
 * @param {function} cancelCreateClient - clean data and close dialog
 * @param {function} readyToAddFamilyMember - initialize client form for family member
 * @param {function} readyToEditFamilyMember - initialize client form for editing family member
 * @param {function} saveClient - save client data from client form
 * @param {function} readyToEditClient - initialize client form for subject client
 * @param {function} saveTrustedIndividual - save trusted individual data from
 *   client form
 * @param {function} loadClient - load profile data to client form
 * */
export default class HeaderLeft extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <ContextConsumer>
        {({ hideClientNavigator }) => (
          <View
            style={{
              marginTop: 21,
              height: "100%",
              justifyContent: "flex-start"
            }}
          >
            <EABButton
              style={{
                marginLeft: Theme.alignmentXL
              }}
              onPress={() => {
                this.props.reloadContactList();
                hideClientNavigator();
                cleanClientData(this.props.dispatch);
              }}
            >
              <TranslatedText
                style={Theme.textButtonLabelNormalAccent}
                path="button.close"
              />
            </EABButton>
          </View>
        )}
      </ContextConsumer>
    );
  }
}

HeaderLeft.propTypes = {
  reloadContactList: PropTypes.func.isRequired
};
