import { StyleSheet } from "react-native";
import Theme from "../../theme";

export default StyleSheet.create({
  cardContent: {
    justifyContent: "center"
  },
  selectorWrap: {
    flexDirection: "row",
    justifyContent: "space-between"
  },
  selectorTitleWrapper: {
    marginRight: Theme.alignmentXXS
  },
  optionWrapper: {
    flexDirection: "row"
  },
  dropdownImage: {
    flexBasis: "auto",
    marginLeft: Theme.alignmentXS,
    width: 24,
    height: 24
  },
  checkBoxButton: {
    alignItems: "center",
    justifyContent: "center",
    margin: 3,
    marginRight: Theme.alignmentL + 3,
    borderRadius: Theme.radiusXS,
    width: 18,
    height: 18
  },
  checkBoxButtonTick: {
    width: 12,
    height: 6,
    transform: [{ translateY: -1 }, { rotate: "-45deg" }]
  },
  checkBoxButtonTickSelected: {
    width: 12,
    height: 6,
    transform: [{ translateY: -1 }, { rotate: "-45deg" }],
    borderLeftWidth: 2,
    borderBottomWidth: 2,
    borderColor: Theme.white
  },
  labelWithCard: {
    ...Theme.bodyPrimary,
    flexBasis: "auto",
    marginRight: Theme.alignmentXL
  },
  displayTextWithCard: {
    flex: 1,
    alignItems: "flex-end",
    justifyContent: "center"
  }
});
