import PropTypes from "prop-types";
import React, { PureComponent } from "react";
import { TextInput, View, Text } from "react-native";
import Theme from "../../theme";
import getStyleArray from "../../utilities/getStyleArray";
import styles from "./styles";
import TranslatedText from "../../containers/TranslatedText";

/**
 * <EABTextBox />
 * @description text box component
 * @param {string} value - text box value
 * @param {string} placeholder - input field label
 * @param {function} onChange - the function will be fire by the text box onChange event
 * @param {function} onBlur - field on blur call back with "value" parameter
 * @param {number} [numberOfLines=4] - default line of text box
 * @param {boolean} [editable=true] - indicates whether the text box is editable or not
 * @param {boolean=} isError - indicates whether the text field is error or not
 * @param {string=} hintText - error message
 * @param {string} [keyboardType='default'] - determines which keyboard to open. check
 *   https://facebook.github.io/react-native/docs/textinput.html#keyboardtype to get more
 *   information
 * @param {object|number=} style - text field container style.
 * @param {boolean} [isAutoFocus=false] - indicates whether the text box will be auto focus or not
 * @param {object|number=} style - text field container style.
 * @param {number} [maxLength=-1] - limit the character length in text box
 * */
export default class EABTextBox extends PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      isFocus: false
    };
  }

  render() {
    const { isError, testID } = this.props;
    const { isFocus } = this.state;

    const renderTextBox = () => (
      <TextInput
        testID={testID}
        style={[
          styles.textInput,
          {
            height:
              Theme.bodyPrimary.lineHeight * this.props.numberOfLines +
              Theme.alignmentM * 2
          }
        ]}
        multiline
        onChangeText={value => this.props.onChange(value)}
        onBlur={value => this.props.onBlur(value)}
        value={this.props.value}
        autoFocus={this.props.isAutoFocus}
        keyboardType={this.props.keyboardType}
        editable={this.props.editable}
        placeholder={this.props.placeholder}
        maxLength={this.props.maxLength}
      />
    );

    const displayHintText = () => {
      switch (true) {
        case isError || (isError && !isFocus):
          return (
            <Text style={Theme.fieldTextOrHelperTextNegative}>
              {this.props.hintText}
            </Text>
          );
        default:
          return null;
      }
    };

    const renderRemainText = () => (
      <View key="hint" style={styles.textWrapper}>
        <TranslatedText
          style={Theme.textboxRemainLabelNoSpacing}
          path="textbox.remainText"
        />
        <Text style={Theme.textboxRemainLabelNoSpacing}>
          {this.props.maxLength - this.props.value.length}
        </Text>
      </View>
    );

    return [
      <View
        key="textbox"
        style={[styles.container, ...getStyleArray(this.props.style)]}
      >
        {renderTextBox()}
      </View>,
      <View style={styles.hitTextWrapper}>{displayHintText()}</View>,
      renderRemainText()
    ];
  }
}

EABTextBox.propTypes = {
  value: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
  onBlur: PropTypes.func,
  numberOfLines: PropTypes.number,
  editable: PropTypes.bool,
  keyboardType: PropTypes.string,
  isAutoFocus: PropTypes.bool,
  placeholder: PropTypes.string,
  maxLength: PropTypes.number,
  style: PropTypes.oneOfType([
    PropTypes.number,
    PropTypes.object,
    PropTypes.array
  ]),
  isError: PropTypes.bool,
  hintText: PropTypes.string,
  testID: PropTypes.string
};
EABTextBox.defaultProps = {
  onBlur: value => value,
  numberOfLines: 4,
  editable: true,
  keyboardType: "default",
  isAutoFocus: false,
  placeholder: "",
  maxLength: 100,
  style: {},
  isError: false,
  hintText: "",
  testID: ""
};
