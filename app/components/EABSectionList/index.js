import React, { PureComponent } from "react";
import {
  View,
  Image,
  Text,
  SectionList,
  TouchableWithoutFeedback
} from "react-native";
import PropTypes from "prop-types";

import icCompleted from "../../assets/images/icCompleted.png";
import icIncomplete from "../../assets/images/icIncomplete.png";
import Theme from "../../theme";

import styles from "./styles";

/**
 * <EABSectionList />
 * @description EABSectionList UI.
 * @param {array} sections
 * @param {string} sections.key key for render section
 * @param {string} sections.title title for one section
 * @param {object} sections.data list of item under one section
 * @param {string} sections.data.key key for render item
 * @param {string} sections.data.id id for selected item
 * @param {boolean} [sections.data.completed] indicate item is completed
 * @param {boolean} sections.data.selected indicate item is selected
 * @param {string} sections.data.title item title
 * @param {string} [sections.data.subtitle] item date String
 * @param {function} onPress - the function that will be fire by section list onPress event.
 * @param {boolean} [showCompleteIcon=false] -indicate the complete icon is shown or not
 * @param {boolean} [showSubtitle=false] -indicate the complete icon is shown or not
 * @param {boolean} [showSectionHeader=false] -indicate the complete icon is shown or not
 *  it will return the sections.data.id to be the function parameter.
 * */

class EABSectionList extends PureComponent {
  constructor(props) {
    super(props);

    this.state = {};
  }

  render() {
    const {
      sections,
      onPress,
      showCompleteIcon,
      showSubtitle,
      showSectionHeader,
      testID
    } = this.props;
    return (
      <SectionList
        testID={`${testID}__PageScroll`}
        style={styles.container}
        renderItem={({ item }) => (
          <TouchableWithoutFeedback
            testID={`${testID}__btn${item.testID}`}
            key={item.key}
            onPress={() => onPress(item.id)}
          >
            <View
              style={
                item.selected ? styles.itemSelectedView : styles.itemNormalView
              }
            >
              <View style={styles.itemTitleView}>
                {showCompleteIcon ? (
                  <Image
                    style={styles.image}
                    source={item.completed ? icCompleted : icIncomplete}
                  />
                ) : null}
                <Text style={styles.itemTitle}>{item.title}</Text>
              </View>
              {showSubtitle ? (
                <View style={styles.itemSubTitleView}>
                  <Text
                    style={
                      showCompleteIcon
                        ? styles.itemSubTitleWithIcon
                        : styles.itemSubTitleWithoutIcon
                    }
                  >
                    {item.subtitle}
                  </Text>
                </View>
              ) : null}
            </View>
          </TouchableWithoutFeedback>
        )}
        renderSectionHeader={({ section }) => (
          <View key={section.key} style={styles.sectionView}>
            {showSectionHeader ? (
              <Text style={Theme.headerBrand}>{section.title}</Text>
            ) : null}
          </View>
        )}
        sections={sections}
      />
    );
  }
}

EABSectionList.propTypes = {
  sections: PropTypes.arrayOf(
    PropTypes.shape({
      key: PropTypes.string.isRequired,
      data: PropTypes.arrayOf(
        PropTypes.shape({
          key: PropTypes.string.isRequired,
          id: PropTypes.string.isRequired,
          title: PropTypes.string.isRequired,
          subtitle: PropTypes.string,
          selected: PropTypes.bool.isRequired,
          completed: PropTypes.bool
        })
      ).isRequired,
      title: PropTypes.string.isRequired
    }).isRequired
  ).isRequired,
  onPress: PropTypes.func.isRequired,
  showCompleteIcon: PropTypes.bool,
  showSubtitle: PropTypes.bool,
  showSectionHeader: PropTypes.bool,
  testID: PropTypes.string
};

EABSectionList.defaultProps = {
  showCompleteIcon: false,
  showSubtitle: false,
  showSectionHeader: false,
  testID: ""
};

export default EABSectionList;
