import { connect } from "react-redux";
import { ACTION_TYPES, REDUCER_TYPES } from "eab-web-api";
import Table from "../../components/DynamicViews/Table";
import { TEXT_STORE } from "../../constants/REDUCER_TYPES";

const { APPLICATION, OPTIONS_MAP } = REDUCER_TYPES;
/**
 * Table
 * @description Table in DynamicViews
 * */

const mapStateToProps = state => ({
  optionsMap: state[OPTIONS_MAP],
  textStore: state[TEXT_STORE]
});

const mapDispatchToProps = dispatch => ({
  deleteApplicationFormTableRecord: ({ path, dataId, recordIndex }) => {
    dispatch({
      type: ACTION_TYPES[APPLICATION].SAGA_DELETE_APPLICATION_FORM_TABLE_RECORD,
      path,
      dataId,
      recordIndex
    });
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Table);
