import { StyleSheet } from "react-native";
import Theme from "../../../../theme";

const statementContentColumn = {
  flex: 1,
  flexDirection: "column"
};

const selectedPlansTableRowValueView = {
  backgroundColor: Theme.white,
  flexBasis: 720,
  borderColor: Theme.lightGrey,
  borderLeftWidth: 1,
  borderRightWidth: 1
};

/**
 * styles
 * */
export default StyleSheet.create({
  container: {
    height: "100%",
    flexDirection: "row",
    justifyContent: "space-between"
  },
  sectionListView: {
    flexBasis: 312
  },
  scrollView: {
    flex: 1,
    width: "100%"
  },
  scrollViewContent: {
    flexGrow: 1,
    alignItems: "center"
  },
  acceptanceContentView: {
    flex: 1,
    width: Theme.boxMaxWidth
  },
  card: {
    width: "100%",
    marginTop: Theme.alignmentXL
  },
  cardContent: {
    flex: 1,
    flexDirection: "column",
    margin: Theme.alignmentXL
  },
  statementTitle: {
    ...Theme.tagLine1Primary
  },
  statementView: {
    flexDirection: "row",
    marginTop: Theme.alignmentXL
  },
  statementContentColumnLeft: {
    ...statementContentColumn,
    marginRight: Theme.alignmentXL
  },
  statementParagraphView: {
    flexDirection: "row",
    justifyContent: "flex-start"
  },
  statementParagraphPoint: {
    ...Theme.subheadPrimary,
    flexBasis: 15,
    marginRight: Theme.alignmentXS
  },
  statementParagraphContent: {
    ...Theme.subheadPrimary,
    marginBottom: Theme.alignmentXL
  },
  statementParagraph: {
    ...Theme.subheadPrimary
  },
  selectedPlansView: {
    flexDirection: "column",
    marginBottom: Theme.alignmentXL
  },
  selectedPlansStatement: {
    marginTop: Theme.alignmentXL
  },
  selectedPlansTableView: {
    flexDirection: "column",
    marginTop: Theme.alignmentL
  },
  selectedPlansTableViewShield: {
    flexDirection: "column",
    marginTop: Theme.alignmentL,
    width: 720
  },
  selectedPlansTableRowView: {
    flexDirection: "row"
  },
  selectedPlansTableRowTitleView: {
    backgroundColor: Theme.brandTransparent,
    flexBasis: 256
  },
  selectedPlansTableRowTitle: {
    ...Theme.indicatorPrimary,
    marginLeft: Theme.alignmentXL,
    marginTop: 14,
    marginBottom: 14
  },
  selectedPlansTableRowValueViewTop: {
    ...selectedPlansTableRowValueView
  },
  selectedPlansTableRowValueViewBottom: {
    ...selectedPlansTableRowValueView,
    borderBottomWidth: 1
  },
  selectedPlansTableRowValue: {
    ...Theme.subheadPrimary,
    marginLeft: Theme.alignmentXL,
    marginTop: 14,
    marginBottom: 14
  },
  textBox: {
    width: 664,
    backgroundColor: Theme.white
  }
});
