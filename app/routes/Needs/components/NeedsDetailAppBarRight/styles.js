import { StyleSheet } from "react-native";
import Theme from "../../../../theme";

/**
 * styles
 * */
export default StyleSheet.create({
  container: {
    flexDirection: "row",
    marginRight: Theme.alignmentXL
  },
  buttonFirst: {
    marginRight: Theme.alignmentXL
  }
});
