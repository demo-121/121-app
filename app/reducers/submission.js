import { combineReducers } from "redux";
import ACTION_TYPES from "../constants/ACTION_TYPES";
import { SUBMISSION } from "../constants/REDUCER_TYPES";

const application = (state = null, action) => {
  switch (action.type) {
    case ACTION_TYPES[SUBMISSION].VALIDATE_APPLICATION:
      return Object.assign({}, state, action.applicationResult);
    default:
      return state;
  }
};

export default combineReducers({
  application
});
