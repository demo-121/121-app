import {
  REDUCER_TYPE_CHECK as WEB_API_TYPES_CHECK,
  REDUCER_TYPES
} from "eab-web-api";
import React, { Component } from "react";
import { Text, View } from "react-native";
import Avatar from "../../../../components/Avatar";
import REDUCER_TYPE_CHECK from "../../../../constants/REDUCER_TYPE_CHECK";
import { TEXT_STORE } from "../../../../constants/REDUCER_TYPES";
import Theme from "../../../../theme";
import profileLayout from "../../../../utilities/profileLayout";
import { ContextConsumer } from "../../../../context";

const { CLIENT, OPTIONS_MAP } = REDUCER_TYPES;

/**
 * ClientInfo
 * @param {object} navigation - react navigation props
 * @param {function} cancelCreateClient - clean data and close dialog
 * @param {function} readyToAddFamilyMember - initialize client form for family member
 * @param {function} readyToEditFamilyMember - initialize client form for editing family member
 * @param {function} saveClient - save client data from client form
 * @param {function} readyToEditClient - initialize client form for subject client
 * @param {function} saveTrustedIndividual - save trusted individual data from
 *   client form
 * @param {function} loadClient - load profile data to client form
 * */
export default class ClientInfo extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { profile, optionsMap, dependantProfiles, textStore } = this.props;
    return (
      <ContextConsumer>
        {({ language }) => (
          <View
            style={{
              marginTop: Theme.alignmentS,
              alignItems: "center",
              height: "100%"
            }}
          >
            <Avatar
              key="photo"
              style={{ marginBottom: Theme.alignmentXS }}
              source={
                profileLayout({
                  language,
                  profile,
                  optionsMap,
                  dependantProfiles,
                  textStore
                }).photo
              }
            />
            <Text key="name" style={Theme.title}>
              {`${profile.title} ${
                profileLayout({
                  language,
                  profile,
                  optionsMap,
                  dependantProfiles,
                  textStore
                }).fullName
              }`}
            </Text>
          </View>
        )}
      </ContextConsumer>
    );
  }
}

ClientInfo.propTypes = {
  profile: WEB_API_TYPES_CHECK[CLIENT].profile.isRequired,
  optionsMap: WEB_API_TYPES_CHECK[OPTIONS_MAP].isRequired,
  dependantProfiles: WEB_API_TYPES_CHECK[CLIENT].dependantProfiles.isRequired,
  textStore: REDUCER_TYPE_CHECK[TEXT_STORE].isRequired
};
