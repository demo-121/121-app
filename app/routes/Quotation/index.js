import React from "react";
import { View } from "react-native";
import { createStackNavigator, createAppContainer } from "react-navigation";
import * as _ from "lodash";
import EABButton from "../../components/EABButton";
import EABSegmentedControl from "../../components/EABSegmentedControl";
import Selector from "../../components/Selector";
import { FLAT_LIST } from "../../components/SelectorField/constants";
import TranslatedText from "../../containers/TranslatedText";
import { ContextConsumer } from "../../context";
import Theme from "../../theme";
import { CLONE, RE_QUOTE } from "./components/ProposalHeaderLeft/constants";
import ProposalDownloadButton from "./containers/ProposalDownloadButton";
import ProposalHeaderLeft from "./containers/ProposalHeaderLeft";
import QuotationAppBarLeft from "./containers/QuotationAppBarLeft";
import QuotationAppBarRight from "./containers/QuotationAppBarRight";
import QuotationUI from "./containers/QuotationUI";
import ProposalUI from "./containers/ProposalUI";
import IllustrationUI from "./containers/IllustrationUI";
import QuotationEmailSection from "./containers/QuotationEmailSection";
import NavigationService from "../../utilities/NavigationService";

const QuotationNavigator = createStackNavigator(
  {
    QuotationUI: {
      screen: QuotationUI,
      navigationOptions: ({ navigation }) => ({
        title: "Quotation",
        headerLeft: <QuotationAppBarLeft />,
        headerRight: <QuotationAppBarRight navigation={navigation} />
      })
    },
    ProposalUI: {
      screen: ProposalUI,
      navigationOptions: ({ navigation }) => {
        const canEmail = navigation.getParam("canEmail");
        const canRequote = navigation.getParam("canRequote");
        const canClone = navigation.getParam("canClone");
        const canIllustrate = navigation.getParam("canIllustrate");
        const quotationId = navigation.getParam("quotationId");
        const activatedKey = navigation.getParam("activatedKey");
        const segments = navigation.getParam("segments");
        const queryQuickQuote = navigation.getParam("queryQuickQuote");
        const cleanQuotation = navigation.getParam("cleanQuotation");
        const isQuickQuote = navigation.getParam("isQuickQuote");
        const avoidNavigate = navigation.getParam("avoidNavigate");
        const getApplicationsList = navigation.getParam("getApplicationsList");
        const setAvoidNavigate = navigation.getParam("setAvoidNavigate");
        const updateUiApplicationListFilter = navigation.getParam(
          "updateUiApplicationListFilter"
        );

        return {
          title: "Proposal",
          headerTitle: () => (
            <View
              style={{
                paddingTop: 10,
                height: "100%",
                alignItems: "center"
              }}
            >
              <TranslatedText
                style={{
                  ...Theme.title,
                  marginBottom: Theme.alignmentL
                }}
                path="proposal.title"
              />
              {_.isArray(segments) ? (
                <EABSegmentedControl
                  segments={segments}
                  activatedKey={activatedKey}
                />
              ) : null}
            </View>
          ),
          headerLeft: () => {
            if (canRequote) {
              return (
                <ProposalHeaderLeft
                  type={RE_QUOTE}
                  back={navigation.goBack}
                  quotationId={quotationId}
                />
              );
            }

            if (canClone) {
              return (
                <ProposalHeaderLeft
                  type={CLONE}
                  back={navigation.goBack}
                  quotationId={quotationId}
                />
              );
            }
            return null;
          },
          headerRight: (
            <ContextConsumer>
              {({
                closeQuotationDialog,
                isShowingQuotationNavigator,
                hideQuotationNavigator,
                setBottomTabNavigatorLoading
              }) => (
                <View style={{ flexDirection: "row", marginBottom: 50 }}>
                  <ProposalDownloadButton
                    quotationId={quotationId}
                    activatedKey={activatedKey}
                  />
                  {canEmail ? (
                    <QuotationEmailSection canEmail={canEmail} />
                  ) : null}

                  {canIllustrate ? (
                    <EABButton
                      style={{
                        marginRight: Theme.alignmentXL
                      }}
                      onPress={() => {
                        navigation.navigate({ routeName: "IllustrationUI" });
                      }}
                    >
                      <TranslatedText
                        style={Theme.textButtonLabelNormalAccent}
                        path="proposal.button.illustrate"
                      />
                    </EABButton>
                  ) : null}
                  <EABButton
                    testID="Quotation__btnDone"
                    style={{
                      marginRight: Theme.alignmentXL
                    }}
                    onPress={() => {
                      setBottomTabNavigatorLoading(true);
                      if (isShowingQuotationNavigator) {
                        hideQuotationNavigator();
                      } else {
                        closeQuotationDialog();
                      }

                      setTimeout(() => {
                        if (isQuickQuote) {
                          queryQuickQuote(() => {
                            if (avoidNavigate) {
                              setAvoidNavigate(false);
                              setBottomTabNavigatorLoading(false);
                            } else {
                              NavigationService.navigate(
                                "Products",
                                "QuickQuoteHistory",
                                {},
                                () => {
                                  cleanQuotation();
                                  setBottomTabNavigatorLoading(false);
                                }
                              );
                            }
                          });
                        } else {
                          updateUiApplicationListFilter();
                          getApplicationsList(() => {
                            NavigationService.navigate(
                              "Clients",
                              "Applications",
                              {},
                              () => {
                                cleanQuotation();
                                setBottomTabNavigatorLoading(false);
                              }
                            );
                          });
                        }
                      }, 1000);
                    }}
                  >
                    <TranslatedText
                      style={Theme.textButtonLabelNormalEmphasizedAccent}
                      path="button.done"
                    />
                  </EABButton>
                </View>
              )}
            </ContextConsumer>
          ),
          headerStyle: { height: 92 }
        };
      }
    },
    IllustrationUI: {
      screen: IllustrationUI,
      navigationOptions: ({ navigation }) => {
        const rateOptions = navigation.getParam("rateOptions", []);
        const selectedRateValue = navigation.getParam("selectedRateValue");

        return {
          title: "Illustration",
          headerTitle: (
            <TranslatedText style={Theme.title} path="illustration.title" />
          ),
          headerRight: (
            <Selector
              displayTextStyle={Theme.textButtonLabelNormalAccent}
              hasCard={false}
              value={selectedRateValue}
              // TODO: Remove once confirmed
              // selectorType={PICKER}
              // pickerOptions={{
              //   items: rateOptions,
              //   onValueChange: selectedValue => {
              //     navigation.setParams({ selectedRateValue: selectedValue });
              //   },
              //   selectedValue: selectedRateValue
              // }}

              selectorType={FLAT_LIST}
              flatListOptions={{
                renderItemOnPress: (itemKey, item) => {
                  navigation.setParams({ selectedRateValue: item.value });
                },
                extraData: this.state,
                keyExtractor: item => item.key,
                selectedValue: selectedRateValue,
                data: rateOptions
              }}
            />
          )
        };
      }
    }
  },
  {
    initialRouteName: "QuotationUI",
    defaultNavigationOptions: {
      headerTitleStyle: Theme.title,
      headerTintColor: Theme.textButtonLabelNormalAccent.color
    }
  }
);

export default createAppContainer(QuotationNavigator);
