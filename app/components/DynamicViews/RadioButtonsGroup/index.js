import PropTypes from "prop-types";
import React, { PureComponent } from "react";
import { Text, View } from "react-native";
import * as _ from "lodash";
import { REDUCER_TYPES, REDUCER_TYPE_CHECK } from "eab-web-api";
import EABRadioButton from "../../EABRadioButton";
import styles from "./styles";
import Theme from "../../../theme";
import { ContextConsumer } from "../../../context";
import PopoverTooltip from "../../PopoverTooltip/PopoverTooltipIOS";

class RadioButtonsGroup extends PureComponent {
  render() {
    const { template, rootValues, path, onUpdateValues, error } = this.props;
    const { options, title, mandatory, id, disabled, tips } = template;
    const value = _.get(rootValues, `${path}.${id}`, "");
    const getHightlightedTitleText = oldTitleText => {
      const newTitleText = [];
      const openBracketString = oldTitleText.indexOf("{{");
      const closeBracketString = oldTitleText.indexOf("}}");
      const middleTitleText = oldTitleText.substring(
        openBracketString + 2,
        closeBracketString
      );
      const beforeTitleText = oldTitleText.substring(0, openBracketString);
      const afterTitleText = oldTitleText.substring(closeBracketString + 2);
      newTitleText.push(
        <Text key={id} style={styles.title}>
          {beforeTitleText}
          <Text key={id} style={styles.replacedTitle}>
            {middleTitleText}
          </Text>
          {afterTitleText}
        </Text>
      );
      return newTitleText;
    };
    const result = [];
    const checkHasAsterisk = title === "" ? null : " *";
    result.push(
      <View>
        <Text style={styles.title}>
          {title.indexOf("{{") === -1 ? title : getHightlightedTitleText(title)}
          {mandatory ? (
            <Text style={styles.requiredHint}>{checkHasAsterisk}</Text>
          ) : null}
        </Text>
        {tips ? (
          <View>
            <PopoverTooltip
              popoverOptions={{
                preferredContentSize: [490, 200]
              }}
              text={tips.msg}
            />
          </View>
        ) : null}
      </View>
    );
    if (_.get(error, `${path}.${id}.hasError`, false)) {
      result.push(
        <ContextConsumer>
          {({ language }) => (
            <Text style={Theme.fieldTextOrHelperTextNegative}>
              {_.get(error, `${path}.${id}.message.${language}`, "")}
            </Text>
          )}
        </ContextConsumer>
      );
    }
    // Map "value: XXX" to "key: XXX", for adapting to EABRadioButton
    _.map(options, option => {
      if (!("key" in option) && "value" in option) {
        _.assign(option, { key: option.value });
      }
    });
    result.push(
      <EABRadioButton
        options={options}
        style={styles.replacedRadioButtonStyle}
        selectedOptionKey={value}
        disabled={!!disabled}
        onPress={option => {
          onUpdateValues({
            id,
            value: option.value,
            path,
            iTemplate: template
          });
        }}
      />
    );
    return result;
  }
}

RadioButtonsGroup.propTypes = {
  onUpdateValues: PropTypes.func.isRequired,
  template: REDUCER_TYPE_CHECK[REDUCER_TYPES.APPLICATION].template.isRequired,
  rootValues:
    REDUCER_TYPE_CHECK[REDUCER_TYPES.APPLICATION].rootValues.isRequired,
  path: PropTypes.string.isRequired,
  error: REDUCER_TYPE_CHECK[REDUCER_TYPES.APPLICATION].error.isRequired
};

RadioButtonsGroup.defaultProps = {};

export default RadioButtonsGroup;
