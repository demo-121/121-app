import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import {
  NativeModules,
  View,
  requireNativeComponent,
  I18nManager
} from "react-native";
import styles from "./styles";

const RNPopoverHostView = requireNativeComponent("RNPopoverHostView");

/**
 * <EABPopover />
 * @description ios popover component
 * @param {number} [sourceView=0] - the reactNode of The view containing the anchor rectangle
 *   for the popover
 * @param {array} [preferredContentSize=[512, 100]] - The preferred size for the view controller’s
 *   view. Like [200, 200]
 * @param {boolean} [visible=true] - determines whether your popover is visible
 * @param {boolean} [animated=false] - determines whether present or dismiss popover use animation
 * @param {boolean} [cancelable=false] - determines whether dismiss popover when clicking the out
 *   space
 * @param {string} [popoverBackgroundColor=null] - set popover back ground color. Like '#FFF'
 * @param {array} [sourceRect=null] - the rectangle in the specified view in which to anchor the
 *   popover
 * @param {array} [permittedArrowDirections=[0]] - The arrow directions that you prefer for the
 *   popover. (0: show on any, 1: up, 2: down, 3: left, 4: right). Like [0, 1, 2, 3, 4]
 * @param {function} [onShow=()=>{}] - allows passing a function that will be called once the
 *   popover has been shown
 * @param {function} [onHide=()=>{}] - allows passing a function that will be called once the
 *   popover has been hidden
 * @param {node} [children=null] - content of popover
 * */
export default class Popover extends PureComponent {
  static dismiss(reactTag, animated = true) {
    return NativeModules.RNPopoverHostViewManager.dismiss(reactTag, animated);
  }

  constructor(props) {
    super(props);
    this.side = I18nManager.isRTL ? "right" : "left";
  }

  render() {
    if (this.props.visible === false) {
      return null;
    }

    return (
      <RNPopoverHostView style={styles.popover} {...this.props}>
        <View style={[styles.container, { [this.side]: 0 }]}>
          {this.props.children}
        </View>
      </RNPopoverHostView>
    );
  }
}

Popover.propTypes = {
  sourceView: PropTypes.number,
  preferredContentSize: PropTypes.arrayOf(PropTypes.number),
  visible: PropTypes.bool,
  animated: PropTypes.bool,
  cancelable: PropTypes.bool,
  popoverBackgroundColor: PropTypes.string,
  sourceRect: PropTypes.arrayOf(PropTypes.number),
  permittedArrowDirections: PropTypes.arrayOf(PropTypes.number),
  onShow: PropTypes.func,
  onHide: PropTypes.func,
  children: PropTypes.node
};
Popover.defaultProps = {
  sourceView: 0,
  preferredContentSize: [512, 100],
  visible: true,
  animated: false,
  cancelable: false,
  popoverBackgroundColor: null,
  sourceRect: null,
  permittedArrowDirections: [0],
  onShow: () => {},
  onHide: () => {},
  children: null
};
