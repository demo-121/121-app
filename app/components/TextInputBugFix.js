import React, { Component } from "react";
import PropTypes from "prop-types";
import { TextInput, Platform } from "react-native";
import getStyleArray from "../utilities/getStyleArray";

/**
 * @description this component is use to fix the value bugs of <TextInput />
 * @see {@link https://github.com/facebook/react-native/issues/18874}
 * @param {string} value
 * @param {function} onChangeText
 * */
class TextInputBugFix extends Component {
  constructor(props) {
    super(props);

    this.state = {
      value: this.props.value
    };

    this.onChangeTextBugFix = this.onChangeTextBugFix.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.value !== undefined) {
      this.setState({ value: nextProps.value });
    }
  }

  // override onChangeText event
  onChangeTextBugFix(value) {
    clearTimeout(this.textTimeout);
    this.setState({ value });
    this.textTimeout = setTimeout(() => {
      setTimeout(() => {
        this.props.onChangeText(value);
      }, 0);
    }, this.props.onChangeTimeOut);
  }

  render() {
    return (
      <TextInput
        testID={this.props.testID}
        {...this.props}
        style={[...getStyleArray(this.props.style), { width: "100%" }]}
        value={this.state.value}
        onChangeText={this.onChangeTextBugFix}
      />
    );
  }
}

TextInputBugFix.propTypes = {
  value: PropTypes.string.isRequired,
  onChangeText: PropTypes.func.isRequired,
  onChangeTimeOut: PropTypes.number,
  style: PropTypes.oneOfType([
    PropTypes.number,
    PropTypes.object,
    PropTypes.array
  ]),
  testID: PropTypes.string
};

TextInputBugFix.defaultProps = {
  onChangeTimeOut: 500,
  style: {},
  testID: ""
};

// use normal TextInput on android
export default (Platform.OS === "ios" ? TextInputBugFix : TextInput);
