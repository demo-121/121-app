import { connect } from "react-redux";
import { ACTION_LIST, REDUCER_TYPES } from "eab-web-api";
import { TEXT_STORE } from "../../../constants/REDUCER_TYPES";
import RecommendationFormNormal from "../components/RecommendationFormNormal";

const { OPTIONS_MAP, AGENT } = REDUCER_TYPES;

/**
 * RecommendationFormNormal
 * @description Form for normal product in Recommendation page.
 * @requires RecommendationFormNormal - RecommendationFormNormal UI
 * */
const mapStateToProps = (state, ownProps) => ({
  agent: state[AGENT],
  textStore: state[TEXT_STORE],
  optionsMap: state[OPTIONS_MAP],
  value: state[REDUCER_TYPES.RECOMMENDATION].recommendation[ownProps.quotId],
  error:
    state[REDUCER_TYPES.RECOMMENDATION].error.recommendation[ownProps.quotId],
  isChosen:
    state[REDUCER_TYPES.RECOMMENDATION].recommendation.chosenList.indexOf(
      ownProps.quotId
    ) >= 0,
  disabled: state[REDUCER_TYPES.RECOMMENDATION].component.disabled
});

const mapDispatchToProps = (dispatch, ownProps) => ({
  showRecommendationDetails: () => {
    ACTION_LIST[REDUCER_TYPES.RECOMMENDATION].updateRecommendationDetails({
      dispatch,
      newShowDetails: true
    });
  },
  updateRecommendationData: newValue => {
    ACTION_LIST[REDUCER_TYPES.RECOMMENDATION].updateRecommendationData({
      dispatch,
      quotationId: ownProps.quotId,
      newValue
    });
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(RecommendationFormNormal);
