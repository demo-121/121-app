import React, { Component } from "react";
import { USER_TAB_TYPES, REDUCER_TYPES } from "eab-web-api";
import { View, Text, Dimensions, ScrollView, Alert, Image } from "react-native";
import PropTypes from "prop-types";
import HTML from "react-native-render-html";
import axaLogo from "../../assets/images/axaLogo.png";
import APP_REDUCER_TYPE_CHECK from "../../constants/REDUCER_TYPE_CHECK";
import { TEXT_STORE } from "../../constants/REDUCER_TYPES";
import EABi18n from "../../utilities/EABi18n";
import { ContextConsumer } from "../../context";
import EABSegmentedControl from "../EABSegmentedControl";
import EABDialog from "../EABDialog";
import EABCheckBox from "../EABCheckBox";
import EABButton from "../EABButton";
import styles from "./style";
import TranslatedText from "../../containers/TranslatedText";

const { CLIENT_TAB, AGENT_TAB } = USER_TAB_TYPES;
const { APPLICATION } = REDUCER_TYPES;

/**
 * EASEEmailContentDialog
 * @description The Dialog on the Report Email Content
 * @param {array} checkBoxOptions - the option of the checkbox
 * @param {function} checkBoxOnPress - the action when press the checkBox
 * @param {function} tabOnPress - the action when press the segment
 * @param {function} sendEmail - the action when press the Send button
 * @param {object} reportEmail - the data of the email report
 * @param {string} tab - the current segment (client, agent)
 * @param {boolean} disableClientTab - the boolean that able or disable the Client segment
 * @param {string} reportType - different reportType (QUOTATION, FNA, QUOTATION)
 * */
export default class EASEEmailContentDialog extends Component {
  constructor() {
    super();

    this.scrollView = React.createRef();
  }
  render() {
    // =========================================================================
    // variables
    // =========================================================================
    const {
      checkBoxOnPress,
      sendEmail,
      tab,
      tabOnPress,
      disableClientTab,
      reportType,
      textStore
    } = this.props;

    const getSegments = language => {
      const segments = [];

      if (!disableClientTab) {
        segments.push({
          key: CLIENT_TAB,
          title: EABi18n({
            language,
            textStore,
            path: "emailDialog.client"
          }),
          isShowingBadge: false,
          onPress: option => {
            tabOnPress(option);
            this.scrollView.current.scrollTo({ x: 0, y: 0, animated: false });
          }
        });
      }

      segments.push({
        key: AGENT_TAB,
        title: EABi18n({
          language,
          textStore,
          path: "emailDialog.agent"
        }),
        isShowingBadge: false,
        onPress: option => {
          tabOnPress(option);
          this.scrollView.current.scrollTo({ x: 0, y: 0, animated: false });
        }
      });

      return segments;
    };

    const renderTemplete = () => {
      const { reportEmail, checkBoxOptions } = this.props;
      let options = checkBoxOptions;
      if (reportEmail && reportEmail.emails) {
        const result = reportEmail.emails.map(item => {
          if (item.id === tab || item.receiverType === tab) {
            if (reportType === APPLICATION) {
              // get object from checkBoxOptions array
              options = checkBoxOptions.find(
                email => email.length > 0 && email[0].id === item.id
              );
            }
            return (
              <View style={styles.applicationWrapper}>
                <Text style={styles.productName}>
                  <TranslatedText path="emailDialog.productName" />:{" "}
                  {item.baseProductName}
                </Text>
                <EABCheckBox
                  options={options}
                  isVerticalStyle={false}
                  onPress={checkBoxOnPress}
                />
                <View>
                  <View style={styles.emailWrapper}>
                    <Text style={styles.toText}>
                      <TranslatedText
                        style={styles.headerButton}
                        path="emailDialog.to"
                      />
                      : {item.to.join(",")}
                    </Text>
                    <View style={styles.emailContentWrapper}>
                      <HTML
                        html={item.content}
                        tagsStyles={{
                          div: {
                            fontSize: 17
                          }
                        }}
                        imagesMaxWidth={Dimensions.get("window").width}
                      />
                      <View style={styles.logoWrapper}>
                        <Image style={styles.logo} source={axaLogo} />
                      </View>
                    </View>
                  </View>
                </View>
              </View>
            );
          }
          return null;
        });
        return result;
      }
      return null;
    };

    return (
      <ContextConsumer>
        {({ emailContentDialogType, hideEmailContentDialog, language }) => (
          <EABDialog isOpen={emailContentDialogType === reportType}>
            <View style={styles.dialogHeader}>
              <View style={styles.dialogContentWrapper}>
                <EABButton
                  onPress={() => {
                    hideEmailContentDialog();
                  }}
                >
                  <TranslatedText
                    style={styles.headerButton}
                    path="button.cancel"
                  />
                </EABButton>
                <TranslatedText
                  style={styles.headerMiddleText}
                  path="button.email"
                />
                <EABButton
                  onPress={() => {
                    if (this.props.isNetworkConnected) {
                      sendEmail(hideEmailContentDialog());
                    } else {
                      setTimeout(() => {
                        Alert.alert(
                          "Warning",
                          `Please connect to the internet to proceed further`,
                          [
                            {
                              text: "OK",
                              onPress: () => renderTemplete(),
                              style: "cancel"
                            }
                          ],
                          { cancelable: false }
                        );
                      }, 200);
                    }
                  }}
                >
                  <TranslatedText
                    style={styles.headerButton}
                    path="button.send"
                  />
                </EABButton>
              </View>
              <View style={styles.segmentWrapper}>
                <View style={styles.emptySpace} />
                <EABSegmentedControl
                  segments={getSegments(language)}
                  activatedKey={tab}
                />
                <View style={styles.emptySpace} />
              </View>
            </View>
            <ScrollView ref={this.scrollView}>
              <View style={styles.dialogBottomContentWrapper}>
                <View style={styles.bottomContent}>
                  <TranslatedText
                    style={styles.selectText}
                    path="emailDialog.selectDocument"
                  />
                  {renderTemplete()}
                </View>
              </View>
            </ScrollView>
          </EABDialog>
        )}
      </ContextConsumer>
    );
  }
}

EASEEmailContentDialog.propTypes = {
  checkBoxOnPress: PropTypes.func.isRequired,
  sendEmail: PropTypes.func.isRequired,
  tabOnPress: PropTypes.func.isRequired,
  tab: PropTypes.bool.isRequired,
  disableClientTab: PropTypes.bool.isRequired,
  reportType: PropTypes.string.isRequired,
  checkBoxOptions: PropTypes.oneOfType([PropTypes.object]).isRequired,
  reportEmail: PropTypes.oneOfType([PropTypes.object]).isRequired,
  textStore: APP_REDUCER_TYPE_CHECK[TEXT_STORE].isRequired,
  isNetworkConnected: PropTypes.bool.isRequired
};
