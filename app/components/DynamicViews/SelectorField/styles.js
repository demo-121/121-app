import { StyleSheet } from "react-native";
import Theme from "../../../theme";

export default StyleSheet.create({
  hboxItemMarginLeft: {
    width: 320,
    marginLeft: Theme.alignmentXL
  },
  container: {
    marginBottom: 20
  },
  deleteButton: {
    flex: 1,
    width: 72
  },
  datePicker: {
    flex: 1
  },
  datePickerContainer: {
    display: "flex",
    flexDirection: "row"
  },
  requiredHint: {
    ...Theme.bodySecondary,
    marginRight: 40,
    paddingTop: 10,
    color: Theme.negative
  },
  titleText: {},
  textContainer: {
    flexDirection: "row",
    width: "82%"
  }
});
