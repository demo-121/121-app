import { Platform } from "react-native";
import PopoverBaseIOS from "./PopoverBase.ios";

export default Platform.select({
  ios: PopoverBaseIOS,
  android: {}
});
