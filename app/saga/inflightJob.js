import { ACTION_TYPES, REDUCER_TYPES } from "eab-web-api";
import moment from "moment";
import { call, put } from "redux-saga/effects";
import * as _ from "lodash";
import { Alert } from "react-native";
import INFLIGHT_JOB_LIST, {
  IMMEDIATE,
  INFLIGHT_JOB_LIST_FILE_NAME,
  SCHEDULED
} from "../constants/INFLIGHT_JOB_LIST";
import {
  getDocumentWithID,
  updateDocumentWithID
} from "../utilities/DataManager";

import APP_ACTION_TYPES from "../constants/ACTION_TYPES";
import runInflightJob from "../utilities/runInflightJob";
import { LOGIN } from "../constants/REDUCER_TYPES";

const { CONFIG } = REDUCER_TYPES;

const getListDocument = () =>
  new Promise((resolve, reject) => {
    getDocumentWithID(INFLIGHT_JOB_LIST_FILE_NAME, result => {
      if (result.error && result.error.length > 0) {
        if (result.error === "not_found") {
          resolve(INFLIGHT_JOB_LIST);
        }
        reject(
          new Error(
            `can not get inflight job list document. docID: ${INFLIGHT_JOB_LIST_FILE_NAME}`
          )
        );
      } else {
        resolve(result);
      }
    });
  });
const updateListDocument = docData =>
  new Promise((resolve, reject) => {
    updateDocumentWithID(INFLIGHT_JOB_LIST_FILE_NAME, docData, result => {
      if (result.error && result.error.length > 0) {
        reject(
          new Error(
            `can not update inflight job list document. docID: ${INFLIGHT_JOB_LIST_FILE_NAME}`
          )
        );
      } else {
        resolve(result);
      }
    });
  });

export default function*(action) {
  const { callback = () => {} } = action;

  try {
    yield put({
      type: ACTION_TYPES[CONFIG].START_LOADING
    });

    // get job list
    const { data: initialJobList } = yield call(getListDocument);
    const newJobList = {};

    Object.keys(INFLIGHT_JOB_LIST.data).forEach(jobKey => {
      const job = INFLIGHT_JOB_LIST.data[jobKey];

      if (
        initialJobList &&
        Object.prototype.hasOwnProperty.call(initialJobList, jobKey)
      ) {
        newJobList[job.jobName] = {
          ...job,
          isExecuted: initialJobList[jobKey].isExecuted
        };
      } else {
        newJobList[job.jobName] = _.cloneDeep(job);
      }
    });

    let isAlertTriggered = false;

    Object.keys(newJobList).some(jobKey => {
      const job = newJobList[jobKey];
      if (job.isExecuted === false) {
        // show an alert to let them know performing inflight jobs
        isAlertTriggered = true;
        return true;
      }
      return false;
    });

    if (isAlertTriggered) {
      yield put({
        type: APP_ACTION_TYPES[LOGIN].ENABLE_INFLIGHT_ALERT
      });
    }

    yield call(() =>
      Promise.all(
        Object.keys(newJobList).map(async jobKey => {
          const job = newJobList[jobKey];

          // couchbase file have useless key, like _rev
          if (job.isExecuted === false) {
            switch (job.effectiveType) {
              case IMMEDIATE:
                await runInflightJob(job.jobName);
                newJobList[jobKey].isExecuted = true;
                break;
              case SCHEDULED:
                if (moment().valueOf() >= job.executionTime) {
                  await runInflightJob(job.jobName);
                  newJobList[jobKey].isExecuted = true;
                }
                break;
              default:
                throw new Error(
                  `unexpected job type. job name: ${job.jobName}`
                );
            }
          }
        })
      )
    );
    // update Invalidation Document. job will update document too. so need to
    // get latest
    const { messages = [] } = yield call(getListDocument);
    yield call(updateListDocument, {
      data: newJobList,
      messages: messages.length !== 0 ? [] : messages
    });

    yield put({
      type: ACTION_TYPES[CONFIG].FINISH_LOADING
    });
    if (messages.length !== 0) {
      setTimeout(() => {
        let msg = "";
        if (Array.isArray(messages)) {
          for (let index = 0; index < messages.length; index += 1) {
            msg += `${messages[index].message}\n\n`;
          }
        } else {
          msg = messages;
        }

        Alert.alert(null, msg, [
          {
            text: "OK",
            onPress: () => {
              setTimeout(callback, 200);
            }
          }
        ]);
      }, 1000);
    } else {
      setTimeout(callback, 1000);
    }
  } catch (e) {
    yield put({
      type: ACTION_TYPES[CONFIG].FINISH_LOADING
    });
    throw new Error(`run inflight job fail. reason: ${e.toString()}`);
  }
}
