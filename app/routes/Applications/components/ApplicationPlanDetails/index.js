import React, { PureComponent } from "react";
import { View, Text } from "react-native";
import * as _ from "lodash";
import moment from "moment";
import {
  FILTER_TYPES,
  REDUCER_TYPES,
  REDUCER_TYPE_CHECK,
  TIME_FORMAT,
  utilities
} from "eab-web-api";
import TranslatedText from "../../../../containers/TranslatedText";
import APP_REDUCER_TYPE_CHECK from "../../../../constants/REDUCER_TYPE_CHECK";
import { TEXT_STORE } from "../../../../constants/REDUCER_TYPES";
import EABTable from "../../../../components/EABTable";
import EABTableSection from "../../../../components/EABTableSection";
import { ContextConsumer } from "../../../../context";
import EABTableHeader from "../../../../components/EABTableHeader";
import { getOptionValue } from "../../../../utilities/getOptionList";
import getEABTextByLangType from "../../../../utilities/getEABTextByLangType";
import Theme from "../../../../theme";
import styles from "./styles";
import EABi18n from "../../../../utilities/EABi18n";

const { OPTIONS_MAP } = REDUCER_TYPES;
const { getCurrency, getCurrencySign } = utilities.common;

/**
 * ApplicationPlanDetails
 * @description item in ApplicationsList inside Application Summary page.
 * @param {object} value - value of application / quotation item object
 * */

class ApplicationPlanDetails extends PureComponent {
  static filterApplicationItems({ item, selectedFilter }) {
    const isApplication =
      _.get(item, "type") === "application" ||
      _.get(item, "type") === "masterApplication";
    const appStatus = _.get(item, "appStatus");

    switch (selectedFilter) {
      case FILTER_TYPES.APPLICATION_LIST.ALL:
        return true;
      case FILTER_TYPES.APPLICATION_LIST.PROPOSED:
        return !isApplication && appStatus !== "INVALIDATED";
      case FILTER_TYPES.APPLICATION_LIST.APPLYING:
        return (
          isApplication &&
          ["SUBMITTED", "INVALIDATED", "INVALIDATED_SIGNED"].indexOf(
            appStatus
          ) < 0
        );
      case FILTER_TYPES.APPLICATION_LIST.SUBMITTED:
        return isApplication && appStatus === "SUBMITTED";
      case FILTER_TYPES.APPLICATION_LIST.INVALIDATED:
        return appStatus === "INVALIDATED";
      case FILTER_TYPES.APPLICATION_LIST.INVALIDATED_SIGNED:
        return isApplication && appStatus === "INVALIDATED_SIGNED";
      default:
        return false;
    }
  }
  static getConvertedKey({ keyID }) {
    const convertDict = {
      allocAWI: "alloc",
      headerPaymentMethod: "paymentMethod",
      singlePremium: "premium"
    };

    return !_.isEmpty(
      _.find(Object.keys(convertDict), k => _.isEqual(k, keyID))
    )
      ? convertDict[keyID]
      : keyID;
  }

  static getTableConfig({ baseProductCode }) {
    const config = [
      {
        code: "SAV",
        showRiderTable: false,
        tableFields: [
          "planName.BasicRider",
          "sumInsured.benefits",
          "polTermDesc",
          "premTermDesc"
        ],
        otherPlanDetailsFields: [
          "ccy",
          "isBackDate",
          "riskCommenDate",
          "paymentMode",
          "premium.any"
        ]
      },
      {
        code: "HIM",
        showRiderTable: false,
        tableFields: [
          "planName.BasicRider",
          "sumInsured.benefits",
          "polTermDesc",
          "premTermDesc"
        ],
        otherPlanDetailsFields: [
          "ccy",
          "isBackDate",
          "riskCommenDate",
          "paymentMode",
          "premium.any"
        ]
      },
      {
        code: "HER",
        showRiderTable: false,
        tableFields: [
          "planName.BasicRider",
          "sumInsured.benefits",
          "polTermDesc",
          "premTermDesc"
        ],
        otherPlanDetailsFields: [
          "ccy",
          "isBackDate",
          "riskCommenDate",
          "paymentMode",
          "premium.any"
        ]
      },
      {
        code: "BAA",
        showRiderTable: false,
        tableFields: ["planName.BasicRider", "sumInsured.benefits"],
        otherPlanDetailsFields: [
          "ccy",
          "covClass",
          "paymentMode",
          "premium.GST"
        ]
      },
      {
        code: "FPX",
        showRiderTable: true,
        tableFields: ["planName", "sumInsured", "totalPremium"],
        riderPlanField: ["planName.rider", "sumInsured", "polTermDesc"],
        otherPlanDetailsFields: [
          "ccy",
          "paymentMode",
          "riskCommenDate",
          "basicBenefit",
          "topUpAmt",
          "insuranceCharge",
          "rspAmount",
          "rspPayFreq"
        ],
        fundTableFields: {
          hasTopUpAmt: ["fundName", "alloc", "topUpAlloc"],
          noTopUpAmt: ["fundName", "alloc"]
        }
      },
      {
        code: "FSX",
        showRiderTable: true,
        tableFields: ["planName", "sumInsured", "totalPremium"],
        riderPlanField: ["planName.rider", "sumInsured", "polTermDesc"],
        otherPlanDetailsFields: [
          "ccy",
          "paymentMode",
          "riskCommenDate",
          "basicBenefit",
          "topUpAmt",
          "insuranceCharge",
          "rspAmount",
          "rspPayFreq"
        ],
        fundTableFields: {
          hasTopUpAmt: ["fundName", "alloc", "topUpAlloc"],
          noTopUpAmt: ["fundName", "alloc"]
        }
      },
      {
        code: "TPX",
        showRiderTable: true,
        tableFields: [
          "planName",
          "sumInsured.benefits",
          "polTermDesc",
          "premTermDesc",
          "totalPremium"
        ],
        riderPlanField: [
          "planName.rider",
          "sumInsured.benefits",
          "polTermDesc",
          "premTermDesc",
          "premium.rider"
        ],
        otherPlanDetailsFields: [
          "ccy",
          "paymentMode",
          "planType",
          "isBackDate",
          "riskCommenDate",
          "occupationClass",
          "indexation"
        ]
      },
      {
        code: "TPPX",
        showRiderTable: true,
        tableFields: [
          "planName",
          "sumInsured.benefits",
          "polTermDesc",
          "premTermDesc",
          "totalPremium"
        ],
        riderPlanField: [
          "planName.rider",
          "sumInsured.benefits",
          "polTermDesc",
          "premTermDesc",
          "premium.rider"
        ],
        otherPlanDetailsFields: [
          "ccy",
          "paymentMode",
          "planType",
          "isBackDate",
          "riskCommenDate",
          "occupationClass",
          "indexation"
        ]
      },
      {
        code: "AWT",
        showRiderTable: false,
        tableFields: ["planName", "premTermDesc", "premium"],
        otherPlanDetailsFields: [
          "ccy",
          "paymentMode",
          "deathBenefit",
          "insuranceCharge",
          "topUpAmt",
          "wdAmount",
          "wdFreq",
          "wdFromAge",
          "wdToAge",
          "rspAmount",
          "rspPayFreq"
        ],
        fundTableFields: {
          hasTopUpAmt: ["fundName", "alloc", "topUpAlloc"],
          noTopUpAmt: ["fundName", "alloc"]
        }
      },
      {
        code: "PUL",
        showRiderTable: false,
        tableFields: ["planName", "premTermDesc", "premium"],
        otherPlanDetailsFields: [
          "ccy",
          "paymentMode",
          "deathBenefit",
          "insuranceCharge"
        ],
        fundTableFields: {
          always: ["fundName", "alloc"]
        }
      },
      {
        code: "LMP",
        showRiderTable: true,
        tableFields: [
          "planName",
          "sumInsured.benefits",
          "multiplyBenefit",
          "polTermDesc",
          "premTermDesc"
        ],
        riderPlanField: [
          "planName.rider",
          "sumInsured.benefits",
          "multiplyBenefit",
          "polTermDesc",
          "premTermDesc"
        ],
        otherPlanDetailsFields: [
          "ccy",
          "isBackDate",
          "riskCommenDate",
          "paymentMode",
          "multiFactor",
          "premium.any"
        ]
      },
      {
        code: "ESP",
        showRiderTable: true,
        tableFields: [
          "planName",
          "sumInsured.benefits",
          "polTermDesc",
          "premTermDesc"
        ],
        riderPlanField: [
          "planName.rider",
          "sumInsured.benefits",
          "polTermDesc",
          "premTermDesc"
        ],
        otherPlanDetailsFields: [
          "ccy",
          "isBackDate",
          "riskCommenDate",
          "guaranteedCashPayoutType",
          "paymentMode",
          "premium.any"
        ]
      },
      {
        code: "RHP",
        showRiderTable: true,
        tableFields: [
          "planName",
          "gteedAnnualRetireIncome",
          "polTermDesc",
          "premTermDesc"
        ],
        riderPlanField: [
          "planName.rider",
          "gteedAnnualRetireIncome",
          "polTermDesc",
          "premTermDesc"
        ],
        otherPlanDetailsFields: [
          "ccy",
          "paymentTerm",
          "payoutType",
          "isBackDate",
          "riskCommenDate",
          "payoutTerm",
          "retirementAge",
          "premium.any",
          "paymentMode",
          "paymentMethod"
        ]
      },
      {
        code: "RHP.L",
        showRiderTable: true,
        tableFields: ["planName", "gteedAnnualRetireIncome", "polTermDesc"],
        riderPlanField: [
          "planName.rider",
          "gteedAnnualRetireIncome",
          "polTermDesc"
        ],
        otherPlanDetailsFields: [
          "ccy",
          "paymentTerm",
          "payoutType",
          "isBackDate",
          "riskCommenDate",
          "payoutTerm",
          "retirementAge",
          "premium.any",
          "paymentMode",
          "paymentMethod"
        ]
      },
      {
        code: "AWICA",
        showRiderTable: false,
        tableFields: [
          "planName",
          "sumInsured.benefits",
          "headerPaymentMethod",
          "singlePremium"
        ],
        otherPlanDetailsFields: [
          "ccy",
          "singlePremSalesCharge",
          "rspAmount",
          "rspPayFreq",
          "rspSalesCharges"
        ],
        fundTableFields: {
          hasTopUpAmt: ["fundName", "allocAWI", "topUpAlloc"],
          noTopUpAmt: ["fundName", "allocAWI"]
        }
      },
      {
        code: "AWICP",
        showRiderTable: false,
        tableFields: [
          "planName",
          "sumInsured.benefits",
          "headerPaymentMethod",
          "singlePremium"
        ],
        otherPlanDetailsFields: [
          "ccy",
          "singlePremSalesCharge",
          "rspAmount",
          "rspPayFreq",
          "rspSalesCharges"
        ],
        fundTableFields: {
          hasTopUpAmt: ["fundName", "allocAWI", "topUpAlloc"],
          noTopUpAmt: ["fundName", "allocAWI"]
        }
      },
      {
        code: "LITE",
        showRiderTable: true,
        tableFields: [
          "planName",
          "sumInsured.benefits",
          "polTermDesc",
          "premTermDesc"
        ],
        riderPlanField: [
          "planName.rider",
          "sumInsured.benefits",
          "polTermDesc",
          "premTermDesc"
        ],
        otherPlanDetailsFields: [
          "ccy",
          "isBackDate",
          "riskCommenDate",
          "paymentMode",
          "multiFactorDesc",
          "premium.any"
        ]
      },
      {
        code: "LITE_ADB",
        showRiderTable: true,
        tableFields: [
          "planName",
          "sumInsured.benefits",
          "polTermDesc",
          "premTermDesc"
        ],
        riderPlanField: [
          "planName.rider",
          "sumInsured.benefits",
          "polTermDesc",
          "premTermDesc"
        ],
        otherPlanDetailsFields: [
          "ccy",
          "isBackDate",
          "riskCommenDate",
          "paymentMode",
          "multiFactorDesc",
          "premium.any",
          "occupationClass"
        ]
      },
      {
        code: "NPE",
        showRiderTable: true,
        tableFields: [
          "planName",
          "incomePayout",
          "polTermDesc",
          "premTermDesc"
        ],
        riderPlanField: [
          "planName.rider",
          "incomePayout",
          "polTermDesc",
          "premTermDesc"
        ],
        otherPlanDetailsFields: [
          "ccy",
          "paymentTerm",
          "payoutTermDesc",
          "paymentMode",
          "accumulationPeriod",
          "isBackDate",
          "riskCommenDate",
          "premium.any"
        ]
      },
      {
        code: "NPE.L",
        showRiderTable: true,
        tableFields: ["planName", "incomePayout", "polTermDesc"],
        riderPlanField: ["planName.rider", "incomePayout", "polTermDesc"],
        otherPlanDetailsFields: [
          "ccy",
          "paymentTerm",
          "payoutTermDesc",
          "paymentMethod",
          "accumulationPeriod",
          "isBackDate",
          "riskCommenDate",
          "premium.any"
        ]
      },
      {
        code: "ESC",
        showRiderTable: false,
        tableFields: [
          "planName.BasicRider",
          "sumInsured.benefits",
          "polTermDesc",
          "premTermDesc"
        ],
        otherPlanDetailsFields: [
          "ccy",
          "isBackDate",
          "riskCommenDate.ddmmyyyy",
          "paymentMode",
          "hasBoughtAxaPlan",
          "premium.discount"
        ]
      }
    ];
    return config.find(value => value.code === baseProductCode);
  }
  static getNamePath({ name }) {
    switch (name) {
      case "hasBoughtAxaPlan":
        return "application.planDetails.discount";
      case "planName.rider":
        return "application.planDetails.planName.rider";
      case "planName":
        return "application.summary.item_section_header_basic_plan";
      case "sumInsured.benefits":
        return "quotation.table.sumAssuredBenefits";
      case "planName.BasicRider":
        return "application.summary.item_section_header_basic_plan_rider";
      case "premium.discount":
        return "application.planDetails.premium.discount";

      case "premium.rider":
        return "application.planDetails.premium.rider";
      case "premium.GST":
        return "application.planDetails.totalPremiumAmountGST";
      case "premium":
        return "application.planDetails.premium";
      case "singlePremium":
        return "application.planDetails.singlePremium";
      case "singlePremSalesCharge":
        return "application.planDetails.singlePremSalesCharge";
      case "premium.any":
        return "application.planDetails.totalPremiumAmount";
      case "paymentTerm":
        return "application.planDetails.paymentTerm";
      case "paymentMethod":
        return "application.planDetails.paymentMethod";
      case "headerPaymentMethod":
        return "application.planDetails.headerPaymentMethod";
      case "retirementAge":
        return "application.planDetails.retirementAge";
      case "payoutTerm":
        return "application.planDetails.payoutTerm";
      case "payoutType":
        return "application.planDetails.payoutType";
      case "gteedAnnualRetireIncome":
        return "application.planDetails.gteedAnnualRetireIncome";
      case "guaranteedCashPayoutType":
        return "application.planDetails.guaranteedCashPayoutType";
      case "multiFactorDesc":
      case "multiFactor":
        return "application.planDetails.multiFactor";
      case "multiplyBenefit":
        return "application.planDetails.multiplyBenefit";
      case "wdFromAge":
        return "application.planDetails.wdFromAge";
      case "wdToAge":
        return "application.planDetails.wdToAge";
      case "wdFreq":
        return "application.planDetails.wdFreq";
      case "wdAmount":
        return "application.planDetails.wdAmount";
      case "wdOption":
        return "application.planDetails.wdOption";
      case "deathBenefit":
        return "application.planDetails.deathBenefit";
      case "indexation":
        return "application.planDetails.indexation";
      case "occupationClass":
        return "application.planDetails.occupationClass";
      case "planType":
        return "application.planDetails.planType";
      case "alloc":
        return "application.planDetails.alloc";
      case "allocAWI":
        return "application.planDetails.allocAWI";
      case "topUpAlloc":
        return "application.planDetails.topUpAlloc";
      case "rspSalesCharges":
        return "application.planDetails.rspSalesCharges";
      case "rspAmount":
        return "application.planDetails.rspAmount";
      case "rspPayFreq":
        return "application.planDetails.rspPayFreq";
      case "insuranceCharge":
        return "application.planDetails.insuranceCharge";
      case "topUpAmt":
        return "application.planDetails.topUpAmount";
      case "basicBenefit":
        return "application.planDetails.basicBenefit";
      case "sumInsured":
        return "application.planDetails.sumAssured";
      case "polTermDesc":
        return "application.summary.item_header_policyTerm";
      case "premTermDesc":
        return "application.summary.item_header_premTermDesc";
      case "ccy":
        return "application.planDetails.currency";
      case "incomePayout":
        return "application.planDetails.incomePayout";
      case "isBackDate":
        return "application.planDetails.backdating";
      case "paymentMode":
        return "application.planDetails.paymentMode";
      case "riskCommenDate.ddmmyyyy":
        return "application.planDetails.selectedCommencementDate.ddmmyyyy";
      case "riskCommenDate":
        return "application.planDetails.selectedCommencementDate";
      case "totalPremium":
        return "application.planDetails.headerTotalPremiumAmount";
      case "totYearPrem":
        return "application.planDetails.totalPremiumAmount";
      case "totYearPrem.GST":
        return "application.planDetails.totalPremiumAmountGST";
      case "covClass":
        return "application.planDetails.occupationClass";
      case "payoutTermDesc":
        return "application.planDetails.payoutTermDesc";
      case "accumulationPeriod":
        return "application.planDetails.accumulationPeriod";
      case "A":
        return "application.summary.annual";
      case "S":
        return "application.summary.semiAnnual";
      case "Q":
        return "application.summary.quarterly";
      case "M":
        return "application.summary.monthly";
      case "L":
        return "application.summary.single";
      case "ESCRemind":
        return "application.planDetails.planName.ESCRemind";
      default:
        return "";
    }
  }

  constructor(props) {
    super(props);
    this.state = {};
  }

  /**
   * getPlanTitle
   * @description return other paln details section
   * @return {object} plan title
   * */
  getPlanTitle(language) {
    const { application } = this.props;
    const titleText = getEABTextByLangType({
      data: application.quotation.baseProductName,
      language
    });
    return <Text style={styles.planHeader}>{titleText}</Text>;
  }

  /**
   * getPaymentModePath
   * @description return the text path of the payment mode
   * @return {string} path string
   * */
  getPaymentModePath(value) {
    const { application } = this.props;
    switch (
      value || application.applicationForm.values.planDetails.paymentMode
    ) {
      case "A":
      case "annual":
        return "application.summary.annual";
      case "S":
      case "semiAnnual":
        return "application.summary.semiAnnual";
      case "Q":
      case "quarterly":
        return "application.summary.quarterly";
      case "M":
        return "application.summary.monthly";
      case "L":
        return "application.summary.single";
      default:
        return "";
    }
  }

  /**
   * getFundDetails
   * @description return Fund Selection section
   * @return {object} fund section view
   * */
  getFundDetails(language) {
    const { application } = this.props;
    if (_.isEmpty(application)) {
      return null;
    }
    const productDetails = this.getProductDetails();
    if (!productDetails || _.isEmpty(productDetails)) {
      return null;
    }
    const { planDetails } = application.applicationForm.values;
    if (
      _.isEmpty(planDetails.fundList) ||
      !planDetails.fundList ||
      !productDetails.fundTableFields
    ) {
      return null;
    }
    const tableConfig = [];
    const header = [];
    if (productDetails.fundTableFields.always) {
      productDetails.fundTableFields.always.forEach((value, index) => {
        tableConfig.push({
          key: `${index}_${value}`,
          style: styles[`tableSectionColumn${index === 0 ? 1 : 2}`]
        });
        header.push(
          <View style={styles.tableHeader}>
            <TranslatedText
              style={styles.tableHeaderTitle}
              language={language}
              path={
                value === "fundName"
                  ? "application.planDetails.fundSelectionTableHeaderFundName"
                  : ApplicationPlanDetails.getNamePath({ name: value })
              }
            />
          </View>
        );
      });
    } else {
      productDetails.fundTableFields[
        planDetails.topUpAmt ? "hasTopUpAmt" : "noTopUpAmt"
      ].forEach((value, index) => {
        tableConfig.push({
          key: `${index}_${value}`,
          style: styles[`tableSectionColumn${index === 0 ? 1 : 2}`]
        });
        header.push(
          <View style={styles.tableHeader}>
            <TranslatedText
              style={styles.tableHeaderTitle}
              language={language}
              path={
                value === "fundName"
                  ? "application.planDetails.fundSelectionTableHeaderFundName"
                  : ApplicationPlanDetails.getNamePath({ name: value })
              }
            />
          </View>
        );
      });
    }
    return (
      <View style={styles.fundSelectionView}>
        <View style={styles.sectionHeader}>
          <TranslatedText
            style={styles.sectionTitle}
            path="application.planDetails.fundDetailsTitle"
            language={language}
          />
        </View>
        <EABTable tableConfig={tableConfig}>
          <EABTableHeader key="fundSelectionTableHeader">
            {header}
          </EABTableHeader>
          {this.getFundDetailsTableContent({
            fundList: planDetails.fundList,
            language
          })}
        </EABTable>
      </View>
    );
  }

  /**
   * getFundDetailsTableContent
   * @description return Fund Selection Table
   * @param {array} fundList The list of funds selected by the user
   * @return {object} table fund content
   * */
  getFundDetailsTableContent({ fundList, language }) {
    const { application } = this.props;
    if (_.isEmpty(application)) {
      return "";
    }
    const { planDetails } = application.applicationForm.values;
    const productDetails = this.getProductDetails();
    if (!productDetails || _.isEmpty(productDetails)) {
      return null;
    }
    const fundTableContent = [];
    fundList.forEach(value => {
      const content = [];
      if (productDetails.fundTableFields.always) {
        productDetails.fundTableFields.always.forEach(fundValue => {
          content.push(
            <View style={styles.tableSection}>
              <Text style={styles.tableSectionTitle}>
                {fundValue === "fundName"
                  ? getEABTextByLangType({
                      data: value.fundName,
                      language
                    })
                  : this.valueReMap({
                      id: fundValue,
                      value:
                        value[
                          ApplicationPlanDetails.getConvertedKey({
                            keyID: fundValue
                          })
                        ],
                      language
                    })}
              </Text>
            </View>
          );
        });
      } else {
        productDetails.fundTableFields[
          planDetails.topUpAmt ? "hasTopUpAmt" : "noTopUpAmt"
        ].forEach(fundValue => {
          content.push(
            <View style={styles.tableSection}>
              <Text style={styles.tableSectionTitle}>
                {fundValue === "fundName"
                  ? getEABTextByLangType({
                      data: value.fundName,
                      language
                    })
                  : this.valueReMap({
                      id: fundValue,
                      value:
                        value[
                          ApplicationPlanDetails.getConvertedKey({
                            keyID: fundValue
                          })
                        ],
                      language
                    })}
              </Text>
            </View>
          );
        });
      }

      fundTableContent.push(
        <EABTableSection key={value.fundCode}>{content}</EABTableSection>
      );
    });
    return fundTableContent;
  }

  /**
   * getOtherPlanDetails
   * @description return other paln details section
   * @return {object} other plan details view
   * */
  getOtherPlanDetails(language) {
    const { application, textStore } = this.props;
    if (_.isEmpty(application)) {
      return "";
    }
    const { baseProductCode } = application.quotation;
    let extraText = "";
    const rspSection = [];
    const rwSection = [];
    const productDetails = this.getProductDetails();
    if (!productDetails || _.isEmpty(productDetails)) {
      return null;
    }
    const { planDetails } = this.props.application.applicationForm.values;
    let titleCss = styles.otherPlanDetailsDataTitle;

    if (
      productDetails.otherPlanDetailsFields.find(
        value =>
          value === "premium.any" ||
          value === "premium.GST" ||
          value === "riskCommenDate" ||
          value === "riskCommenDate.ddmmyyyy"
      )
    ) {
      titleCss = styles.otherPlanDetailsDataLongTitle;
    }
    return (
      <View style={styles.otherPlanDetailsView}>
        <View style={styles.sectionHeader}>
          <TranslatedText
            style={styles.sectionTitle}
            path="application.planDetails.otherPlanDetailsTitle"
            language={language}
          />
        </View>
        <View style={styles.sectionDataView}>
          {productDetails.otherPlanDetailsFields.map(value => {
            let extraTitleText = null;
            if (
              baseProductCode === "RHP" &&
              value === "paymentMethod" &&
              planDetails.paymentMode !== "L"
            ) {
              return null;
            }
            if (
              baseProductCode === "RHP" &&
              value === "paymentMode" &&
              planDetails.paymentMode === "L"
            ) {
              return null;
            }
            if (value === "wdAmount") {
              if (!planDetails.wdAmount) {
                return null;
              }
              rwSection.push(value);
            }
            if (value === "wdFreq") {
              if (!planDetails.wdFreq || !planDetails.wdAmount) {
                return null;
              }
              rwSection.push(value);
            }
            if (value === "wdFromAge") {
              if (!planDetails.wdFromAge || !planDetails.wdAmount) {
                return null;
              }
              rwSection.push(value);
            }
            if (value === "wdToAge") {
              if (!planDetails.wdToAge || !planDetails.wdAmount) {
                return null;
              }
              rwSection.push(value);
            }
            if (value === "insuranceCharge" && !planDetails.insuranceCharge) {
              return null;
            }
            if (value === "riskCommenDate" && planDetails.isBackDate === "N") {
              return null;
            }
            if (
              value === "riskCommenDate.ddmmyyyy" &&
              planDetails.isBackDate === "N"
            ) {
              return null;
            }
            if (value === "topUpAmt" && !planDetails.topUpAmt) {
              return null;
            }
            if (value === "rspAmount") {
              if (!planDetails.rspSelect || !planDetails.rspAmount) {
                return null;
              }
              rspSection.push(value);
            }
            if (value === "rspPayFreq") {
              if (!planDetails.rspSelect || !planDetails.rspPayFreq) {
                return null;
              }
              rspSection.push(value);
            }
            if (value === "rspSalesCharges") {
              if (!planDetails.rspSelect || !planDetails.rspSalesCharges) {
                return null;
              }
              rspSection.push(value);
            }
            if (
              value === "indexation" &&
              (planDetails.planType !== "toAge" || !planDetails.indexation)
            ) {
              return null;
            }
            if (rspSection.length || rwSection.length) {
              return null;
            }
            if (value === "hasBoughtAxaPlan") {
              extraTitleText = [
                <Text style={{ color: Theme.red }}> (*)</Text>,
                <Text>:</Text>
              ];
              extraText = (
                <TranslatedText
                  style={styles.otherPlanDetailsExtraText}
                  path={ApplicationPlanDetails.getNamePath({
                    name: "ESCRemind"
                  })}
                  language={language}
                />
              );
            }
            return (
              <View style={styles.otherPlanDetailsDataView}>
                <Text style={titleCss}>
                  {EABi18n({
                    language,
                    textStore,
                    path: ApplicationPlanDetails.getNamePath({ name: value })
                  })}
                  {extraTitleText}
                </Text>
                <Text style={styles.otherPlanDetailsDataValue}>
                  {this.valueReMap({
                    id: value,
                    value:
                      planDetails[
                        ApplicationPlanDetails.getConvertedKey({
                          keyID: value.split(".")[0]
                        })
                      ],
                    language
                  })}
                </Text>
              </View>
            );
          })}
          <Text>{extraText}</Text>
          {rwSection.length ? (
            <View style={styles.otherPlanDetailsView}>
              <View style={styles.sectionHeader}>
                <TranslatedText
                  style={styles.sectionTitle}
                  path="application.planDetails.rwDetails"
                  language={language}
                />
              </View>
              <View style={styles.sectionDataView}>
                {rwSection.map(value => (
                  <View style={styles.otherPlanDetailsDataView}>
                    <TranslatedText
                      style={titleCss}
                      path={ApplicationPlanDetails.getNamePath({
                        name: value
                      })}
                      language={language}
                    />
                    <Text style={styles.otherPlanDetailsDataValue}>
                      {this.valueReMap({
                        id: value,
                        value:
                          planDetails[
                            ApplicationPlanDetails.getConvertedKey({
                              keyID: value.split(".")[0]
                            })
                          ],
                        language
                      })}
                    </Text>
                  </View>
                ))}
              </View>
            </View>
          ) : null}
          {rspSection.length ? (
            <View style={styles.otherPlanDetailsView}>
              <View style={styles.sectionHeader}>
                <TranslatedText
                  style={styles.sectionTitle}
                  path="application.planDetails.rspDetails"
                  language={language}
                />
              </View>
              <View style={styles.sectionDataView}>
                {rspSection.map(value => (
                  <View style={styles.otherPlanDetailsDataView}>
                    <TranslatedText
                      style={titleCss}
                      path={ApplicationPlanDetails.getNamePath({
                        name: value
                      })}
                      language={language}
                    />
                    <Text style={styles.otherPlanDetailsDataValue}>
                      {this.valueReMap({
                        id: value,
                        value:
                          planDetails[
                            ApplicationPlanDetails.getConvertedKey({
                              keyID: value.split(".")[0]
                            })
                          ],
                        language
                      })}
                    </Text>
                  </View>
                ))}
              </View>
            </View>
          ) : null}
        </View>
      </View>
    );
  }

  getProductDetails() {
    const { application } = this.props;
    let { baseProductCode } = application.quotation;
    const { planDetails } = application.applicationForm.values;
    const { paymentMode } = planDetails;

    switch (baseProductCode.toUpperCase()) {
      case "RHP":
        if (paymentMode === "L") {
          baseProductCode = "RHP.L";
        }
        break;
      case "LITE":
        if (planDetails.riderList.find(list => list.covCode === "LITE_ADB")) {
          baseProductCode = "LITE_ADB";
        }
        break;
      case "NPE":
        if (paymentMode === "L") {
          baseProductCode = "NPE.L";
        }
        break;
      default:
        break;
    }

    return ApplicationPlanDetails.getTableConfig({
      baseProductCode
    });
  }

  getTableHeader({ rider }) {
    const productDetails = this.getProductDetails();
    if (!productDetails || _.isEmpty(productDetails)) {
      return null;
    }
    const header = [];
    if (rider) {
      productDetails.riderPlanField.forEach(value => {
        const fieldName = value.split(".");
        header.push(
          <View
            style={[
              styles.tableHeader,
              fieldName[0] === "planName" ? styles.left : styles.right
            ]}
          >
            <TranslatedText
              style={styles.tableHeaderTitle}
              path={ApplicationPlanDetails.getNamePath({ name: value })}
            />
          </View>
        );
      });
    } else {
      productDetails.tableFields.forEach(value => {
        const fieldName = value.split(".");
        header.push(
          <View
            style={[
              styles.tableHeader,
              fieldName[0] === "planName" ? styles.left : styles.right
            ]}
          >
            <TranslatedText
              style={styles.tableHeaderTitle}
              path={ApplicationPlanDetails.getNamePath({ name: value })}
            />
          </View>
        );
      });
    }
    return <EABTableHeader key="table-header">{header}</EABTableHeader>;
  }

  getTableContent({ rider, language }) {
    const productDetails = this.getProductDetails();
    if (!productDetails || _.isEmpty(productDetails)) {
      return null;
    }

    const { planDetails } = this.props.application.applicationForm.values;
    const content = [];
    const isSpecialProduct = _.find(["AWICA", "AWICP"], sp =>
      _.isEqual(productDetails.code, sp)
    );
    if (rider) {
      planDetails.riderList.forEach(value => {
        content.push(
          <EABTableSection key={value.covCode}>
            {productDetails.riderPlanField.map(detail => {
              const fieldName = detail.split(".");
              return (
                <View
                  style={[
                    styles.tableSection,
                    fieldName[0] === "planName" ? styles.left : styles.right
                  ]}
                >
                  <Text style={styles.tableSectionTitle}>
                    {fieldName[0] === "planName"
                      ? getEABTextByLangType({
                          data: value.covName,
                          language
                        })
                      : this.valueReMap({
                          id: detail,
                          value:
                            value[
                              ApplicationPlanDetails.getConvertedKey({
                                keyID: fieldName[0]
                              })
                            ],
                          language
                        })}
                  </Text>
                </View>
              );
            })}
          </EABTableSection>
        );
      });
    } else {
      planDetails[
        productDetails.showRiderTable ? "displayPlanList" : "planList"
      ].forEach(value => {
        content.push(
          <EABTableSection key={value.covCode}>
            {productDetails.tableFields.map(detail => {
              const fieldName = detail.split(".");
              return (
                <View
                  style={[
                    styles.tableSection,
                    fieldName[0] === "planName" ? styles.left : styles.right
                  ]}
                >
                  <Text style={styles.tableSectionTitle}>
                    {fieldName[0] === "planName"
                      ? `${getEABTextByLangType({
                          data: value.covName,
                          language
                        })} ${
                          value.saPostfix
                            ? `(${getEABTextByLangType({
                                data: value.saPostfix,
                                language
                              })})`
                            : ""
                        }`
                      : this.valueReMap({
                          id: detail,
                          value: !isSpecialProduct
                            ? value[
                                ApplicationPlanDetails.getConvertedKey({
                                  keyID: fieldName[0]
                                })
                              ]
                            : planDetails[
                                ApplicationPlanDetails.getConvertedKey({
                                  keyID: fieldName[0]
                                })
                              ] ||
                              value[
                                ApplicationPlanDetails.getConvertedKey({
                                  keyID: fieldName[0]
                                })
                              ],
                          language
                        })}
                  </Text>
                </View>
              );
            })}
          </EABTableSection>
        );
      });
    }
    return content;
  }

  valueReMap({ id, value, language }) {
    const { application, optionsMap, textStore } = this.props;
    const { planDetails } = application.applicationForm.values;

    const newId = ApplicationPlanDetails.getConvertedKey({
      keyID: id.split(".")[0]
    });

    switch (newId) {
      case "multiFactorDesc":
      case "premTermDesc":
      case "paymentTerm":
      case "wdFromAge":
      case "wdToAge":
      case "polTermDesc":
      case "ccy":
      case "occupationClass":
      case "wdOption":
      case "covClass":
      case "payoutTermDesc":
      case "accumulationPeriod":
        return value;
      case "singlePremSalesCharge":
      case "rspSalesCharges":
        return String(value).endsWith("%") ? value : `${value}%`;
      case "hasBoughtAxaPlan":
      case "indexation":
      case "isBackDate":
        return value === "Y" ? "Yes" : "No";
      case "paymentMode":
        return EABi18n({
          language,
          textStore,
          path: this.getPaymentModePath()
        });
      case "riskCommenDate":
        return moment(planDetails.riskCommenDate).format(
          TIME_FORMAT.DATE_TIME_FORMAT_3
        );
      case "basicBenefit":
        return EABi18n({
          language,
          textStore,
          path:
            value === "C"
              ? "application.planDetails.basicBenefitChoiceCover"
              : "application.planDetails.basicBenefitMaxCover"
        });
      case "totalPremium":
      case "wdAmount":
      case "premium":
      case "topUpAmt":
      case "rspAmount":
        if (value === " - ") {
          return value;
        }
        return `${getCurrencySign({
          compCode: "01",
          ccy: planDetails.ccy,
          optionsMap
        })}${getCurrency({
          value,
          sign: "",
          decimals: 2
        })}`;
      case "totYearPrem":
        if (value === " - ") {
          return value;
        }
        return `${getCurrencySign({
          compCode: "01",
          ccy: planDetails.ccy,
          optionsMap
        })}${getCurrency({
          value: planDetails.totYearPrem,
          sign: "",
          decimals: 2
        })}`;
      case "insuranceCharge":
        return getOptionValue({
          optionMap: optionsMap.insuranceCharge,
          language,
          value
        });
      case "wdFreq":
      case "rspPayFreq":
        return EABi18n({
          language,
          textStore,
          path: this.getPaymentModePath(value)
        });
      case "retirementAge":
        return `Age ${value}`;
      case "multiFactor":
        return `${value} Times`;
      case "allocAWI":
      case "topUpAlloc":
      case "alloc":
        return `${value}%`;
      case "planType":
        switch (value) {
          case "toAge":
            return "To Age";
          case "renew":
            return "Renewable";
          default:
            return null;
        }
      case "deathBenefit":
        switch (value) {
          case "basic":
            return "Basic";
          case "enhanced":
            return "Enhanced";
          default:
            return null;
        }
      case "guaranteedCashPayoutType":
        switch (value) {
          case "paidOut":
            return "Paid Out";
          case "accumulated":
            return "Accumulated";
          default:
            return null;
        }
      case "incomePayout":
        if (value === " - " || value === 0 || value === undefined) {
          return " - ";
        }
        return `${getCurrencySign({
          compCode: "01",
          ccy: planDetails.ccy,
          optionsMap
        })}${getCurrency({
          value,
          sign: "",
          decimals: 2
        })}`;
      case "sumInsured":
      case "multiplyBenefit":
      case "gteedAnnualRetireIncome":
        if (value === " - " || value === 0) {
          return " - ";
        }
        return `${getCurrencySign({
          compCode: "01",
          ccy: planDetails.ccy,
          optionsMap
        })}${getCurrency({
          value,
          sign: "",
          decimals: 0
        })}`;
      case "payoutType":
        switch (value) {
          case "Level":
            return "Level";
          case "Inflated":
            return "Inflated";
          default:
            return null;
        }
      case "payoutTerm":
        switch (value) {
          case "15":
            return "15 Years";
          case "20":
            return "20 Years";
          case "99":
            return "Lifetime";
          default:
            return null;
        }
      case "paymentMethod":
        switch (value) {
          case "cash":
            return "Cash";
          case "srs":
            return "SRS";
          case "cpfisoa":
            return "CPFIS-OA";
          case "cpfissa":
            return "CPFIS-SA";
          default:
            return null;
        }
      default:
        return "";
    }
  }

  renderTable(language) {
    const productDetails = this.getProductDetails();
    if (!productDetails || _.isEmpty(productDetails)) {
      return null;
    }
    const { planDetails } = this.props.application.applicationForm.values;
    const tableConfig = [];
    const riderTableConfig = [];
    if (productDetails.showRiderTable) {
      productDetails.riderPlanField.forEach((value, index) => {
        riderTableConfig.push({
          key: `${index}_${value}`,
          style: styles[`tableSectionColumn${index === 0 ? 1 : 2}`]
        });
      });
    }
    productDetails.tableFields.forEach((value, index) => {
      tableConfig.push({
        key: `${index}_${value}`,
        style: styles[`tableSectionColumn${index === 0 ? 1 : 2}`]
      });
    });
    return (
      <View>
        <EABTable tableConfig={tableConfig}>
          {this.getTableHeader({ rider: false })}
          {this.getTableContent({ rider: false, language })}
        </EABTable>
        {productDetails.showRiderTable && !_.isEmpty(planDetails.riderList) ? (
          <View style={{ marginTop: 16 }}>
            <EABTable tableConfig={riderTableConfig}>
              {this.getTableHeader({ rider: true })}
              {this.getTableContent({ rider: true, language })}
            </EABTable>
          </View>
        ) : null}
      </View>
    );
  }

  render() {
    const { application } = this.props;
    if (_.isEmpty(application)) {
      return null;
    }
    return (
      <ContextConsumer>
        {({ language }) => (
          <View style={styles.scrollViewContentSubview}>
            {this.getPlanTitle(language)}
            {this.renderTable(language)}
            {this.getOtherPlanDetails(language)}
            {this.getFundDetails(language)}
          </View>
        )}
      </ContextConsumer>
    );
  }
}

ApplicationPlanDetails.propTypes = {
  optionsMap: REDUCER_TYPE_CHECK[OPTIONS_MAP].isRequired,
  application:
    REDUCER_TYPE_CHECK[REDUCER_TYPES.PRE_APPLICATION].ApplicationItem.value
      .isRequired,
  textStore: APP_REDUCER_TYPE_CHECK[TEXT_STORE].isRequired
};

export default ApplicationPlanDetails;
