import { StyleSheet } from "react-native";

export default StyleSheet.create({
  scrollView: {
    width: "100%",
    height: "100%"
  },
  scrollViewContent: {
    alignItems: "center"
  }
});
