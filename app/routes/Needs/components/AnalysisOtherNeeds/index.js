import PropTypes from "prop-types";
import React, { Component } from "react";
import { Text, View } from "react-native";
import * as _ from "lodash";
import {
  utilities,
  REDUCER_TYPE_CHECK as WEB_API_REDUCER_TYPE_CHECK,
  REDUCER_TYPES
} from "eab-web-api";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import EABTextField from "../../../../components/EABTextField";
import PopoverTooltip from "../../../../components/PopoverTooltip/PopoverTooltipIOS";
import APP_REDUCER_TYPE_CHECK from "../../../../constants/REDUCER_TYPE_CHECK";
import { TEXT_STORE } from "../../../../constants/REDUCER_TYPES";
import EABButton from "../../../../components/EABButton";
import SelectorField from "../../../../components/SelectorField";
import { FLAT_LIST } from "../../../../components/SelectorField/constants";
import {
  RECTANGLE_BUTTON_1,
  RECTANGLE_BUTTON_2
} from "../../../../components/EABButton/constants";
import Theme from "../../../../theme";
import EABi18n from "../../../../utilities/EABi18n";
import EABStepper from "../../../../components/EABStepper";
import { ContextConsumer } from "../../../../context";
import styles from "../Analysis/styles";

const { FNA, CLIENT } = REDUCER_TYPES;
const { fillTextVariable } = utilities.common;
const { numberToCurrency, currencyToNumber } = utilities.numberFormatter;
/**
 * AnalysisOtherNeeds
 * @description Analysis - Other Needs - Template
 * */
export default class AnalysisOtherNeeds extends Component {
  constructor(props) {
    super(props);

    this.onChange = this.onChange.bind(this);
    this.renderGoals = this.renderGoals.bind(this);
    this.renderShortfall = this.renderShortfall.bind(this);
  }

  componentDidMount() {
    this.props.initNaAnalysis();
  }

  componentDidUpdate(prevProps) {
    if (prevProps.selectedProfile !== this.props.selectedProfile) {
      this.props.initNaAnalysis();
    }
  }

  onChange({ value, key, goalIndex = -1 }) {
    const {
      selectedProfile,
      updateNA,
      na,
      pda,
      profile,
      dependantProfiles,
      fe
    } = this.props;
    const naData = _.cloneDeep(na);

    if (goalIndex !== -1) {
      naData.other[selectedProfile].goals[goalIndex][key] = value;
      naData.other[selectedProfile].init = false;

      updateNA({
        naData,
        pdaData: pda,
        profileData: profile,
        dependantProfilesData: dependantProfiles,
        feData: fe,
        completedStep: 1
      });
    } else {
      naData.other[selectedProfile][key] = value;
      naData.other[selectedProfile].init = false;

      updateNA({
        naData,
        pdaData: pda,
        profileData: profile,
        dependantProfilesData: dependantProfiles,
        feData: fe,
        completedStep: 1
      });
    }
  }

  get selectedGoals() {
    const { data } = this;
    const newGoals = [];

    for (let goalIndex = 0; goalIndex < data.goalNo; goalIndex += 1) {
      newGoals.push(data.goals[goalIndex]);
    }

    return newGoals;
  }

  get data() {
    const { selectedProfile, na } = this.props;

    return na.other[selectedProfile];
  }

  renderGoals(language) {
    const { selectedGoals, onChange } = this;
    const { textStore, isOtherError, selectedProfile } = this.props;

    return selectedGoals.map((goal, goalIndex) => (
      <View>
        <View style={styles.goalsWrapper}>
          <Text style={styles.analysisProductPointTitle}>
            {`1. ${EABi18n({
              language,
              textStore,
              path: "na.needs.other.goalNameHeader"
            })}`}
            <Text style={styles.requiredHint}> *</Text>
          </Text>
          <EABTextField
            testID={`NeedsAnalysis__ON__txtOtherNeedsTitle__gp${goalIndex}`}
            style={styles.analysisTextInputLong}
            value={goal.goalName}
            placeholder={EABi18n({
              language,
              textStore,
              path: "na.needs.other.goalNamePlaceholder"
            })}
            isError={
              isOtherError[selectedProfile].goals[goalIndex].goalName.hasError
            }
            hintText={EABi18n({
              textStore,
              language,
              path:
                isOtherError[selectedProfile].goals[goalIndex].goalName
                  .messagePath
            })}
            onChange={value => {
              onChange({ key: "goalName", value, goalIndex });
            }}
          />
          <EABTextField
            testID={`NeedsAnalysis__ON__txtOtherNeedsValue__gp${goalIndex}`}
            style={styles.analysisTextInputShort}
            prefix="$"
            value={numberToCurrency({
              value: goal.needsValue || 0,
              decimal: 0
            })}
            isError={
              isOtherError[selectedProfile].goals[goalIndex].needsValue.hasError
            }
            hintText={EABi18n({
              textStore,
              language,
              path:
                isOtherError[selectedProfile].goals[goalIndex].needsValue
                  .messagePath
            })}
            onChange={value => {
              onChange({
                key: "needsValue",
                value:
                  value !== "0" || value !== "" ? currencyToNumber(value) : "",
                goalIndex
              });
            }}
          />

          <Text style={styles.analysisProductPointTitle}>
            {`2. ${EABi18n({
              language,
              textStore,
              path: "na.needs.other.typeofNeeds"
            })}`}
            <Text style={styles.requiredHint}> *</Text>
          </Text>
          {isOtherError[selectedProfile].goals[goalIndex].typeofNeeds
            .hasError ? (
            <View>
              <Text style={Theme.fieldTextOrHelperTextNegative}>
                {EABi18n({
                  textStore,
                  language,
                  path:
                    isOtherError[selectedProfile].goals[goalIndex].typeofNeeds
                      .messagePath
                })}
              </Text>
            </View>
          ) : null}
          <View style={styles.analysisHospitalisationButtonContainer}>
            <EABButton
              testID={`NeedsAnalysis__ON__btnTypeOfNeedsSaving__gp${goalIndex}`}
              buttonType={
                goal.typeofNeeds === "SAVINGS"
                  ? RECTANGLE_BUTTON_1
                  : RECTANGLE_BUTTON_2
              }
              style={styles.typeOfNeedsButtonForSavings}
              onPress={() => {
                onChange({ key: "typeofNeeds", value: "SAVINGS", goalIndex });
              }}
            >
              <Text style={styles.sumbitButton}>
                {EABi18n({
                  language,
                  textStore,
                  path: "na.needs.other.SAVINGS"
                })}
              </Text>
            </EABButton>
            <EABButton
              testID={`NeedsAnalysis__ON__btnTypeOfNeedsProtection__gp${goalIndex}`}
              buttonType={
                goal.typeofNeeds === "PROTECTION"
                  ? RECTANGLE_BUTTON_1
                  : RECTANGLE_BUTTON_2
              }
              style={styles.typeOfNeedsButtonForProtection}
              onPress={() => {
                onChange({
                  key: "typeofNeeds",
                  value: "PROTECTION",
                  goalIndex
                });
              }}
            >
              <Text style={styles.sumbitButton}>
                {EABi18n({
                  language,
                  textStore,
                  path: "na.needs.other.PROTECTION"
                })}
              </Text>
            </EABButton>
          </View>

          <Text style={styles.analysisProductPointTitle}>
            {`3. ${EABi18n({
              language,
              textStore,
              path: "na.needs.other.estimatedMaturity"
            })}`}
          </Text>
          <View style={{ flexDirection: "row" }}>
            <SelectorField
              testID={`NeedsAnalysis__ON__ddlExistingInsurancePolicies__gp${goalIndex}`}
              style={styles.analysisSelectorHalfWidth}
              value={goal.extInsuSelField}
              placeholder={EABi18n({
                textStore,
                language,
                path: "na.needs.other.pleaseSelect"
              })}
              selectorType={FLAT_LIST}
              flatListOptions={{
                renderItemOnPress: (itemKey, item) => {
                  onChange({
                    key: "extInsuSelField",
                    value: item.value,
                    goalIndex
                  });
                },
                data: [
                  {
                    label: EABi18n({
                      language,
                      textStore,
                      path: "na.needs.other.SAVINGS"
                    }),
                    value: "SAVINGS",
                    key: "savings"
                  },
                  {
                    label: EABi18n({
                      language,
                      textStore,
                      path: "na.needs.other.Protection"
                    }),
                    value: "Protection",
                    key: "protection"
                  }
                ]
              }}
            />
            <EABTextField
              testID={`NeedsAnalysis__ON__txtExistingInsurancePoliciesValue__gp${goalIndex}`}
              style={styles.analysisTextFieldHalfWidth}
              prefix="$"
              value={numberToCurrency({
                value: goal.extInsuDisplay || 0,
                decimal: 0
              })}
              onChange={value => {
                onChange({
                  key: "extInsuDisplay",
                  value:
                    value !== "0" || value !== ""
                      ? currencyToNumber(value)
                      : "",
                  goalIndex
                });
              }}
            />
          </View>
        </View>
        <View style={styles.analysisSectionBorderLine} />
      </View>
    ));
  }

  renderShortfall(language) {
    const { selectedGoals } = this;
    const { textStore } = this.props;

    return selectedGoals.map((goal, index) => {
      const isTotShortfallNegative = goal.totShortfall < 0;

      return (
        <View
          style={[
            styles.shortfallView,
            {
              marginBottom: 0,
              bottom: index === 0 ? 55 : 55 * (index + 1)
            }
          ]}
        >
          <View style={styles.shortfallWrapper}>
            <PopoverTooltip
              popoverOptions={{ preferredContentSize: [350, 100] }}
              text={EABi18n({
                textStore,
                language,
                path: isTotShortfallNegative
                  ? "na.needs.common.shortfallContent"
                  : "na.needs.common.surplusContent"
              })}
            />
            <View style={styles.subShortfallView}>
              <Text style={Theme.fieldTextOrHelperTextNormal}>
                {`${fillTextVariable({
                  text: EABi18n({
                    path: isTotShortfallNegative
                      ? "na.needs.other.shortfall"
                      : "na.needs.other.surplus",
                    language,
                    textStore
                  }),
                  data: [
                    {
                      value: index + 1,
                      isPath: false
                    }
                  ],
                  EABi18n,
                  language,
                  textStore
                })}: `}
                <Text
                  style={
                    isTotShortfallNegative
                      ? Theme.headlineNegative
                      : Theme.headlinePositive
                  }
                >
                  {`$${numberToCurrency({
                    value: Math.abs(goal.totShortfall || 0),
                    decimal: 2
                  })}`}
                </Text>
              </Text>
            </View>
          </View>
        </View>
      );
    });
  }

  render() {
    const { onChange, data } = this;
    const { textStore, selectedProfile, isOtherError } = this.props;

    return (
      <ContextConsumer>
        {({ language }) => (
          <View>
            <KeyboardAwareScrollView extraScrollHeight={100}>
              <View key={123} style={styles.analysisProductContent}>
                <Text style={styles.title}>
                  {EABi18n({
                    language,
                    textStore,
                    path: "na.needs.other"
                  })}
                </Text>
                <Text style={styles.analysisProductPointTitle}>
                  {EABi18n({
                    language,
                    textStore,
                    path: "na.needs.other.goalNo"
                  })}
                  <Text style={styles.requiredHint}> *</Text>
                </Text>
                {isOtherError[selectedProfile].goalNo.hasError ? (
                  <View>
                    <Text style={Theme.fieldTextOrHelperTextNegative}>
                      {EABi18n({
                        textStore,
                        language,
                        path: isOtherError[selectedProfile].goalNo.messagePath
                      })}
                    </Text>
                  </View>
                ) : null}
                <EABStepper
                  testID="NeedsAnalysis__ON"
                  value={data.goalNo}
                  step={1}
                  min={1}
                  max={3}
                  onChange={value => {
                    onChange({ key: "goalNo", value });
                  }}
                />
                <View style={styles.analysisSectionBorderLine} />

                {this.renderGoals(language)}
              </View>
              <View style={{ marginBottom: Theme.alignmentXXXXXL }} />
            </KeyboardAwareScrollView>
            <View style={styles.shortfallOtherNeedsContainer}>
              {this.renderShortfall(language)}
            </View>
          </View>
        )}
      </ContextConsumer>
    );
  }
}

AnalysisOtherNeeds.propTypes = {
  selectedProfile: PropTypes.string.isRequired,
  textStore: APP_REDUCER_TYPE_CHECK[TEXT_STORE].isRequired,
  initNaAnalysis: PropTypes.func.isRequired,
  updateNA: PropTypes.func.isRequired,
  profile: WEB_API_REDUCER_TYPE_CHECK[CLIENT].profile.isRequired,
  dependantProfiles:
    WEB_API_REDUCER_TYPE_CHECK[CLIENT].dependantProfiles.isRequired,
  na: WEB_API_REDUCER_TYPE_CHECK[FNA].na.isRequired,
  fe: WEB_API_REDUCER_TYPE_CHECK[FNA].fe.isRequired,
  pda: WEB_API_REDUCER_TYPE_CHECK[FNA].pda.isRequired,
  isOtherError: PropTypes.oneOfType([PropTypes.object]).isRequired
};
