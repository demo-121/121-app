import { NavigationActions } from "react-navigation";
import * as _ from "lodash";

let _navigator = [];

const setNavigator = navigatorRef => {
  if (
    !_navigator.find(
      container =>
        container.topLevelNavigator === navigatorRef.topLevelNavigator
    ) &&
    navigatorRef.ref
  ) {
    _navigator.push(navigatorRef);
  }
};

const navigate = (topLevelNavigator, routeName, params, callback) => {
  _navigator.some(container => {
    if (topLevelNavigator === container.topLevelNavigator) {
      container.ref.dispatch(
        NavigationActions.navigate({
          routeName,
          params
        })
      );
      if (_.isFunction(callback)) {
        callback();
      }
      return true;
    }
    return false;
  });
};

const clearNavigationRef = topLevelNavigator => {
  if (!topLevelNavigator) {
    _navigator = [];
  } else {
    _navigator.some((container, index) => {
      if (topLevelNavigator === container.topLevelNavigator) {
        _navigator.splice(index, 1);
        return true;
      }
      return false;
    });
  }
};

export default {
  navigate,
  setNavigator,
  clearNavigationRef
};
