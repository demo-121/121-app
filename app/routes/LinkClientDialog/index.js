import React from "react";
import { createStackNavigator, createAppContainer } from "react-navigation";
import EABSegmentedControl from "../../components/EABSegmentedControl";
import TranslatedText from "../../containers/TranslatedText";
import Theme from "../../theme";
import TrustedIndividualForm from "./components/TrustedIndividualForm";
import EditLinkClientForm from "./components/EditLinkClientForm";
import SaveButton from "./containers/SaveButton";
import SearchLinkClient from "./containers/SearchLinkClient";
import LinkClinetSearchBar from "./containers/LinkClinetSearchBar";

const LinkClientDialogNavigator = createStackNavigator(
  {
    SearchLinkClient: {
      screen: SearchLinkClient,
      navigationOptions: ({ navigation }) => ({
        title: "Search",
        headerTitle: <LinkClinetSearchBar navigation={navigation} />
      })
    },
    EditLinkClientForm: {
      screen: EditLinkClientForm,
      navigationOptions: ({ navigation }) => {
        if (navigation.state.params) {
          const {
            segmentedControlArray,
            showProfileTab
          } = navigation.state.params;

          return {
            headerTitle: (
              <EABSegmentedControl
                style={{ marginBottom: Theme.alignmentXS }}
                segments={segmentedControlArray}
                activatedKey={showProfileTab}
              />
            ),
            headerRight: (
              <SaveButton navigation={navigation} labelPath="button.link" />
            )
          };
        }
        return {};
      }
    },
    TrustedIndividualForm: {
      screen: TrustedIndividualForm,
      navigationOptions: ({ navigation }) => ({
        headerTitle: (
          <TranslatedText
            style={Theme.title}
            path="clientProfile.trustedIndividual.title"
          />
        ),
        headerRight: (
          <SaveButton navigation={navigation} labelPath="button.save" />
        )
      })
    }
  },
  {
    initialRouteName: "SearchLinkClient",
    defaultNavigationOptions: {
      headerTintColor: Theme.textButtonLabelNormalAccent.color
    }
  }
);

export default createAppContainer(LinkClientDialogNavigator);
