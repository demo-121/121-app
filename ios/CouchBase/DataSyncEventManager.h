//
//  DataSyncEventManager.h
//  EASE
//
//  Created by MacAdmin on 4/9/18.
//  Copyright © 2018 Facebook. All rights reserved.
//

#import <React/RCTBridgeModule.h>
#import <React/RCTEventEmitter.h>
#import "React/RCTBridge.h"
@interface DataSyncEventManager : RCTEventEmitter <RCTBridgeModule>
-(void)sendDataSyncStatus:(NSString*) status;
@end

