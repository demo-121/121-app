import { StyleSheet } from "react-native";

/**
 * styles
 * */
export default StyleSheet.create({
  pdf: {
    width: "100%",
    height: "100%"
  },
  container: {
    width: "100%",
    height: "100%",
    alignItems: "flex-start"
  }
});
