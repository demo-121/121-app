import { ActionSheetIOS } from "react-native";

export default function({ optionsList, onChange, selectedList }) {
  const genOptionsList = optionMap => {
    const options = [];
    optionMap.forEach(value => {
      if (selectedList.indexOf(value.title.replace(/ /g, "")) === -1) {
        options.push(value.title);
      }
    });
    return options;
  };

  const getTargetValue = (optionMap, targetIndex) => optionMap[targetIndex];

  const newOptionsList = genOptionsList(optionsList);
  ActionSheetIOS.showActionSheetWithOptions(
    {
      options: newOptionsList,
      destructiveButtonIndex: -1,
      cancelButtonIndex: -1
    },
    buttonIndex => {
      onChange(getTargetValue(newOptionsList, buttonIndex));
    }
  );
}
