import * as _ from "lodash";
import getPostalCodeFileName from "../../../utilities/getPostalCodeFileName";

export const checkPostalCode = ({
  template,
  rootValues,
  path,
  newValue,
  currentValue
}) => {
  const { mapping, countryAllowed } = template;
  if (
    _.isString(newValue) &&
    _.size(newValue) === 6 &&
    newValue !== currentValue
  ) {
    if (mapping && countryAllowed) {
      const countryAllowedValue = _.get(
        rootValues,
        `${path}.${countryAllowed}`
      );
      // R2 is Singapore
      if (countryAllowedValue === "R2") {
        return true;
      }
    }
  }
  return false;
};

export const postalCodeFunc = ({
  newValue,
  id,
  path,
  template,
  updateAddress,
  onUpdateValues,
  callback
}) => {
  const fileName = getPostalCodeFileName(newValue);
  if (fileName) {
    onUpdateValues({ id, value: newValue, path, iTemplate: template });
    updateAddress({
      fileName,
      postalCode: newValue,
      fieldType: "residentialAddress",
      path,
      callback
    });
  }
};

export const checkUnitNo = ({ currentValue, newValue }) =>
  _.isString(currentValue) &&
  currentValue.length === 0 &&
  _.isString(newValue) &&
  newValue.length > 0;

export const unitNoFunc = ({
  id,
  newValue,
  path,
  template,
  onUpdateValues
}) => {
  onUpdateValues({ id, value: `Unit ${newValue}`, path, iTemplate: template });
};

export const isNumberInRange = ({ value, max, min }) => {
  let passMinMax = true;
  if (_.isNumber(max) && value > max) {
    passMinMax = false;
  }
  if (_.isNumber(min) && value < min) {
    passMinMax = false;
  }
  return passMinMax;
};

export const numberToStringWithDecimal = ({ value, decimal }) => {
  let result = value;
  const strArr = _.split(_.toString(value), ".");
  if (strArr.length > 1 && strArr[1].length > decimal) {
    result = Math.floor(value * 10 ** decimal) / 10 ** decimal;
    result = result.toFixed(decimal);
    result = _.includes(_.toString(result), ".00")
      ? parseInt(result, 10)
      : result;
  }
  return result;
};

export const numberNotInRangeFunc = ({ newValue, max, decimal }) => {
  let result;
  const toFixedMax = max.toFixed(decimal);

  const specialStringLengthCheck =
    newValue > max ? newValue.toString().slice(0, max.toString().length) : max;

  result =
    newValue.toString().length > toFixedMax.toString().length
      ? specialStringLengthCheck
      : parseInt(newValue, 10);

  if (parseInt(result, 10) > max) {
    result = result.toString().slice(0, 1);
    result = parseInt(result, 10);
    if (result < 1) {
      result = "";
    }
  }

  if (result > max) {
    result = "";
    if (decimal === 0) {
      result = result.toString().slice(0, 1);
    }
  }

  return result;
};

export const getMaxValueWithValidation = ({
  max,
  validation,
  rootValues,
  path
}) => {
  if (_.get(validation, "type") === "sum") {
    const relatedField = _.get(rootValues, `${path}.${validation.id}`, "");
    let maxLength = validation.max - relatedField.length;
    maxLength = _.min([maxLength, max]);
    return maxLength;
  }
  return null;
};

export default {};
