import { REDUCER_TYPES } from "eab-web-api";
import { connect } from "react-redux";
import { TEXT_STORE } from "../../../constants/REDUCER_TYPES";
import ClientInfo from "../components/ClientInfo";

const { OPTIONS_MAP, CLIENT_FORM, CLIENT } = REDUCER_TYPES;

/**
 * ClientInfo
 * @requires ClientInfo - ClientInfo UI
 * */
const mapStateToProps = state => ({
  clientForm: state[CLIENT_FORM],
  textStore: state[TEXT_STORE],
  optionsMap: state[OPTIONS_MAP],
  profile: state[CLIENT].profile,
  dependantProfiles: state[CLIENT].dependantProfiles
});

const mapDispatchToProps = () => ({});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ClientInfo);
