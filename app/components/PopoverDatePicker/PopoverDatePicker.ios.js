import PropTypes from "prop-types";
import React from "react";
import { DatePickerIOS, View, Text } from "react-native";
import APP_REDUCER_TYPE_CHECK from "../../constants/REDUCER_TYPE_CHECK";
import { TEXT_STORE } from "../../constants/REDUCER_TYPES";
import { ContextConsumer } from "../../context";
import EABi18n from "../../utilities/EABi18n";
import Popover from "../Popover";
import PopoverBase from "../Popover/PopoverBase";
import EABButton from "../../components/EABButton";
import Theme from "../../theme";
import styles from "./styles";
/**
 *
 * <PopoverDatePicker />
 *
 * @description ios popover datepicker component
 * @param {object} [datePickerIOSOptions={date: new Date(), isShowconfirmButton: false, onDateChange: () => {}}, buttonCancel: "Cancel", buttonDone: "Done"] - props of
 *   DatePickerIOS's object
 * @param {string} buttonCancel -- use for header of date picker title
 * @param {string} buttonDone -- use for header of date picker title
 * */
export default class PopoverDatePicker extends PopoverBase {
  constructor(props) {
    super(props);
    this.state = {
      date: this.props.datePickerIOSOptions.date || new Date()
    };
  }

  render() {
    // =========================================================================
    // variables
    // =========================================================================
    const { textStore } = this.props;

    return (
      <ContextConsumer>
        {({ language }) => (
          <Popover
            preferredContentSize={[400, 280]}
            {...this.props.popoverOptions}
          >
            <View style={Theme.dialogHeader}>
              <View style={styles.headerRow}>
                <EABButton
                  testID="btnDatePickerCancel"
                  onPress={() => {
                    this.props.popoverOptions.onHide();
                  }}
                >
                  <Text style={Theme.textButtonLabelNormalAccent}>
                    {EABi18n({
                      language,
                      textStore,
                      path: "button.cancel"
                    })}
                  </Text>
                </EABButton>
                <EABButton
                  testID="btnDatePickerDone"
                  onPress={() => {
                    this.props.popoverOptions.onHide();

                    setTimeout(() => {
                      this.props.datePickerIOSOptions.onDateChange(
                        this.state.date
                      );
                    }, 500); // this (solves) the issue that the alert dialog (if any) to be closed along with onHide()
                  }}
                >
                  <Text style={Theme.textButtonLabelNormalAccent}>
                    {EABi18n({
                      language,
                      textStore,
                      path: "button.done"
                    })}
                  </Text>
                </EABButton>
              </View>
            </View>
            <DatePickerIOS
              testID="datePicker" // TODO: Sam doesnt work
              {...this.props.datePickerIOSOptions}
              date={this.state.date}
              onDateChange={value => {
                this.setState({ date: value });
              }}
            />
          </Popover>
        )}
      </ContextConsumer>
    );
  }
}

PopoverDatePicker.propTypes = {
  ...PopoverBase.propTypes,
  datePickerIOSOptions: PropTypes.oneOfType([PropTypes.object]).isRequired
};
PopoverDatePicker.defaultProps = {
  ...PopoverBase.defaultProps,
  datePickerIOSOptions: {
    testID: "date",
    date: new Date(),
    onDateChange: () => {},
    buttonCancel:
      "Cancel" /** use for header of date picker -- default value */,
    buttonDone: "Done" /** use for header of date picker -- default value */
  },
  textStore: APP_REDUCER_TYPE_CHECK[TEXT_STORE].isRequired
};
