/* eslint-disable prettier/prettier */
import { LANGUAGE_TYPES, TEXT_STORE } from "eab-web-api";

export default {
  ...TEXT_STORE[LANGUAGE_TYPES.ENGLISH],
  "login.appPassword.password_do_not_meet_password_policy": "Password doesn’t meet the security guidelines.",
  "login.appPassword.password_do_not_match": "Passwords do not match. Please re-enter the password again.",
  "login.appPassword.new_pw_is_updated_successfully": "APP Password is updated successfully.",
  "login.appActivation": "EAB Mobile wants you to perform App Activation",
  "login.timebomb":"The app has been locked as you have not synced your data recently. Please uninstall and install the app again.",
  "login.waiting": "Just a moment...  we'll take you to EAB demo soon.",
  "login.connectNetwork": "Please connect your device to the internet for online registration process",
  "login.onlineAuth": "EAB Mobile wants you to perform Online Authentication",
  "login.onlineAuthFailAttempt": "You have entered an incorrect password 5 times. Online Authentication is required",
  "login.onlineAuthAppPasswordExpiry": "App Password has been expired. Online Authentication and Reset App Password are required",
  "login.openSafari": "Continue",
  "login.AppPassword": "App Password",
  "login.ResetAppPassword": "Reset App Password",
  "login.LogIn": "Continue",
  "login.ResetPassword": "Reset",
  "login.EnvironmentSettingHeader": "Environment Setting",
  "login.EnvironmentSetting": "Please Select Environment",
  "login.EnvironmentButton": "OK",
  // Payment
  "payment.PaymentSettingHeader": "Payment",
  "payment.PaymentSetting": "Please Select Payment Method",
  "payment.PaymentButton": "Pay",
  "payment.PrePayment": "Online Authentication is required.",
  "payment.onlinePayment": "EAB Mobile wants you to perform online payment",
  "payment.goToSafari": "Continue",
  // Submission
  "submission.SubmissionSettingHeader": "Submission",
  "submission.SubmitButton": "Submit",
  "submission.subHeader": "Online Authentication is required.",
  "submission.onlineAuth": "EAB Mobile wants you to perform online payment",
  "submission.goToSafari": "Continue",
  "submission.SubmissionCompleted": "Submission Completed",
  "submission.backButton": "Back",
  // System message
  "system.message.ML0001": "You are not authorized to access the application.",
  "system.message.ML0002": "Do you wish to logout?",
  "system.message.ML0003": "Please check your internet connectivity and try again.",
  "system.message.ML0004": "You have entered an incorrect password <x> times. After <5-x> unsuccessful attempts your account would be locked.",
  "system.message.ML0006": "Your password for this account will expire in <x> day(s). Please change the password before it expires. You can change password by clicking on Reset Password available on Menu after Online Login.",
  "system.message.ML0007": "App Password has been expired. Please consider resetting the same.",
  "system.message.ML0008": "Timebomb for this App will explode in <x> day(s). Please connect to Internet and Login.",
  "system.message.ML0009": "Timebomb is exploded. All data in the App is erased.",
  "system.message.ML0010":
    "Your account is locked. Please unlock through Reset Password.",
  "system.message.ML0011": "Device Clock is not accurate. Please fix it.",
  "system.message.ML0012": "Jailbreak is detected. App is closed now.",
  "system.message.ML0013": "Password doesn’t meet the security guidelines",
  "system.message.ML0014":
    "Passwords do not match. Please re-enter the password again",
  "system.message.ML0015": "APP Password is updated successfully",
  "system.message.ML0016":
    "Please login with internet connection to reset your APP password",
  "system.message.ML0017":
    "App is locked because Timebomb for this App will explode in <x> day(s). Please connect to Internet and Login.",
  "system.message.ML0018":
    "This is not your registered device. For security, please remove the App in this device.",
  "system.message.ML0019":
    "Transaction date is invalid. It cannot be opened until matching correct systemdate/time.",
  "system.message.ML0020":
    "You won’t be able to submit case as there is no supervisor information linked to you. Please contact your firm’s administrator.",
  /**
   * @name sideNav
   * @description landing side navigation bar text
   * */
  "sideNav.resetAppPassword": "Reset App Password",
  "sideNav.dataSync": "Data Sync",
  "sideNav.lastDataSync": "Last Sync Date",
  "sideNav.environment": "Environment",
  "sideNav.shield": "Total Policy No. of Shield",
  "sideNav.nonShield": "Total Policy No. of Non Shield",
  "sideNav.confirmLogOut": "Confirm to log out?"
};
