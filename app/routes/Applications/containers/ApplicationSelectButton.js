import { connect } from "react-redux";
import { ACTION_TYPES, REDUCER_TYPES } from "eab-web-api";
import { TEXT_STORE } from "../../../constants/REDUCER_TYPES";
import ApplicationSelectButton from "../components/ApplicationSelectButton";

/**
 * ApplicationSelectButton
 * @description Button to select Applications.
 * @requires ApplicationSelectButton - ApplicationSelectButton UI
 * */
const mapStateToProps = state => ({
  textStore: state[TEXT_STORE],
  applicationsList: state[REDUCER_TYPES.PRE_APPLICATION].applicationsList
});

const mapDispatchToProps = dispatch => ({
  updateUiSelectMode: ({ selectModeOn, callback }) => {
    dispatch({
      type: ACTION_TYPES[REDUCER_TYPES.PRE_APPLICATION].UPDATE_UI_SELECT_MODE,
      selectModeOn,
      callback
    });
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ApplicationSelectButton);
