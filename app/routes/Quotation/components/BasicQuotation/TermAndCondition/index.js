import * as _ from "lodash";
import PropTypes from "prop-types";
import {
  REDUCER_TYPE_CHECK as WEB_API_REDUCER_TYPE_CHECK,
  REDUCER_TYPES as WEB_WEB_API_REDUCER_TYPES
} from "eab-web-api";
import { Text, View } from "react-native";
import React, { Component } from "react";

import styles from "./styles";
import EABDialog from "../../../../../components/EABDialog/EABDialog.ios";
import EABCheckBox from "../../../../../components/EABCheckBox";
import EABButton from "../../../../../components/EABButton";
import ImageViewer from "../../../../../containers/ImageViewer";
import REDUCER_TYPES_CHECK from "../../../../../constants/REDUCER_TYPE_CHECK";
import * as REDUCER_TYPES from "../../../../../constants/REDUCER_TYPES";
import { ContextConsumer } from "../../../../../context";
import { getAttachmentWithID } from "../../../../../utilities/DataManager";
import Theme from "../../../../../theme";
import EABi18n from "../../../../../utilities/EABi18n";

const { QUOTATION } = WEB_WEB_API_REDUCER_TYPES;
const { TEXT_STORE } = REDUCER_TYPES;

export default class TermAndCondition extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isOpen: false
    };
  }

  render() {
    const {
      inputConfigs,
      quotation,
      quote,
      policyOption,
      textStore,
      testID
    } = this.props;

    const { isOpen } = this.state;

    return (
      <ContextConsumer>
        {({ language, setSignDocImageSource, setImageViewerSource }) => (
          <View style={styles.wrapper}>
            <EABCheckBox
              testID={testID}
              style={styles.checkBox}
              labelStyle={styles.checkBoxLabel}
              options={[
                {
                  key: "label",
                  testID: "ReadAndAgree",
                  text: _.get(
                    inputConfigs,
                    `${quotation.baseProductCode}.policyOptions[1].title.en`
                  ),
                  isSelected: !!_.get(
                    quotation,
                    "policyOptions.termAndCondition"
                  )
                }
              ]}
              onPress={option => {
                _.set(
                  quotation,
                  "policyOptions.termAndCondition",
                  !option.isSelected
                );
                quote({ ...quotation });
              }}
            />

            <EABButton
              testID={`${testID}__btnViewTermsAndCondition`}
              style={styles.viewTermAndConditionButton}
              onPress={() => {
                setImageViewerSource("");
                getAttachmentWithID(
                  quotation.baseProductId,
                  policyOption.termAndConditionAttId,
                  response => {
                    setSignDocImageSource(response.data);
                    this.setState({
                      isOpen: true
                    });
                  }
                );
              }}
            >
              <Text style={Theme.textButtonLabelNormalEmphasizedAccent}>
                {EABi18n({
                  textStore,
                  language,
                  path: "quotation.button.viewTermAndCondition"
                })}
              </Text>
            </EABButton>

            <EABDialog isOpen={isOpen}>
              <View style={styles.dialogToolBar}>
                <EABButton
                  onPress={() => {
                    this.setState({
                      isOpen: false
                    });
                    setSignDocImageSource("");
                  }}
                >
                  <Text style={Theme.textButtonLabelNormalAccent}>
                    {EABi18n({
                      language,
                      textStore,
                      path: "button.done"
                    })}
                  </Text>
                </EABButton>
              </View>
              <ImageViewer />
            </EABDialog>
          </View>
        )}
      </ContextConsumer>
    );
  }
}

TermAndCondition.propTypes = {
  inputConfigs: WEB_API_REDUCER_TYPE_CHECK[QUOTATION].inputConfigs.isRequired,
  quotation: WEB_API_REDUCER_TYPE_CHECK[QUOTATION].quotation.isRequired,
  quote: PropTypes.func.isRequired,
  policyOption: PropTypes.oneOfType([PropTypes.object]).isRequired,
  textStore: REDUCER_TYPES_CHECK[TEXT_STORE].isRequired,
  testID: PropTypes.string.isRequired
};

TermAndCondition.defaultValue = {
  testID: ""
};
