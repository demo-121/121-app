import { connect } from "react-redux";
import { REDUCER_TYPES, ACTION_TYPES } from "eab-web-api";
import { setLastDataSyncTime } from "../../../actions/login";
import APP_ACTION_TYPES from "../../../constants/ACTION_TYPES";
import { LOGIN, PAYMENT, TEXT_STORE } from "../../../constants/REDUCER_TYPES";
import PaymentButton from "../components/PaymentButton";

/**
 * PaymentButton
 * @description PaymentButton in Payment section.
 * @requires PaymentButton - PaymentButton UI
 * */
const mapStateToProps = state => ({
  textStore: state[TEXT_STORE],
  submission: state[REDUCER_TYPES.PAYMENT_AND_SUBMISSION].submission,
  payment: state[REDUCER_TYPES.PAYMENT_AND_SUBMISSION].payment,
  environment: state[LOGIN].environment,
  onlinePaymentStatus:
    state[REDUCER_TYPES.PAYMENT_AND_SUBMISSION].onlinePaymentStatus,
  application: state[REDUCER_TYPES.APPLICATION].application,
  isDisableEappButton:
    state[REDUCER_TYPES.APPLICATION].component.isDisableEappButton,
  agentCode: state[LOGIN].agentCode,
  authToken: state[LOGIN].authToken,
  lastDataSyncTime: state[LOGIN].lastDataSyncTime
});

const mapDispatchToProps = dispatch => ({
  disableEappButton: newIsDisableEappButton => {
    dispatch({
      type: ACTION_TYPES[REDUCER_TYPES.APPLICATION].IS_DISABLE_EAPP_BUTTON,
      newIsDisableEappButton
    });
  },
  closeDialogAndGetApplicationList: callback => {
    dispatch({
      type:
        ACTION_TYPES[REDUCER_TYPES.APPLICATION]
          .SAGA_CLOSE_DIALOG_AND_GET_APPLICATION_LIST,
      callback
    });
  },
  refreshPaymentStatus(params) {
    dispatch({
      ...params,
      type:
        ACTION_TYPES[REDUCER_TYPES.PAYMENT_AND_SUBMISSION]
          .REFRESH_PAYMENT_STATUS
    });
  },
  getPayment: ({ docId, callback }) => {
    dispatch({
      type: ACTION_TYPES[REDUCER_TYPES.PAYMENT_AND_SUBMISSION].SAGA_GET_PAYMENT,
      docId,
      callback
    });
  },
  getPaymentURL(params) {
    dispatch({
      ...params,
      type: ACTION_TYPES[REDUCER_TYPES.PAYMENT_AND_SUBMISSION].GET_PAYMENT_URL
    });
  },
  loginByOnlineAuthForPayment: () => {
    dispatch({
      type: APP_ACTION_TYPES[PAYMENT].LOGIN_BY_ONLINE_AUTH_FOR_PAYMENT
    });
  },
  setOnlineAuthCodeForPayment: ({ authCode, callback }) => {
    dispatch({
      type: APP_ACTION_TYPES[PAYMENT].ONLINE_AUTH_CODE_FOR_PAYMENT,
      authCode,
      callback
    });
  },
  preCheck(params) {
    dispatch({
      ...params,
      type: APP_ACTION_TYPES[LOGIN].PRE_CEHCK
    });
  },
  dataSync(params) {
    dispatch({
      ...params,
      type: APP_ACTION_TYPES[LOGIN].PRE_CEHCK_ON_DATA_SYNC
    });
  },
  continueApplicationFormShield: ({ applicationId, callback }) => {
    dispatch({
      type:
        ACTION_TYPES[REDUCER_TYPES.APPLICATION]
          .SAGA_CONTINUE_APPLICATION_FORM_SHIELD,
      applicationId,
      callback
    });
  },
  setLastDataSyncTime() {
    setLastDataSyncTime({ dispatch });
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PaymentButton);
