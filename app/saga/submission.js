import { put, call, select } from "redux-saga/effects";
import { REDUCER_TYPES, ACTION_TYPES } from "eab-web-api";
import { SUBMISSION } from "../constants/REDUCER_TYPES";
import APP_ACTION_TYPES from "../constants/ACTION_TYPES";
// import { copyFileSync } from "fs";
import { submitApplication } from "../apis/submission";

const { CONFIG } = REDUCER_TYPES;

export function* dataSyncForSubmission(action) {
  const { docId, applicationResult } = action;
  try {
    const isDataSyncDone = true;
    let dataSyncResult = {};
    if (docId != null && docId.trim().length > 0) {
      yield put({
        type: APP_ACTION_TYPES[SUBMISSION].VALIDATE_APPLICATION,
        applicationResult
      });
      // Call to dataSync
      if (isDataSyncDone) {
        dataSyncResult = {
          hasError: false
        };
      } else {
        dataSyncResult = {
          hasError: true
        };
      }
    } else {
      dataSyncResult = {
        hasError: true,
        errorMsg: "No Application Id to perform data sync"
      };
    }
    yield put({
      type: ACTION_TYPES[CONFIG].FINISH_LOADING
    });
    action.callback(dataSyncResult);
  } catch (e) {
    const errorMsg = e;
    const dataSyncResult = {
      hasError: true,
      errorMsg
    };
    yield put({
      type: ACTION_TYPES[CONFIG].FINISH_LOADING
    });
    action.callback(dataSyncResult);
  }
}
export function* submission(action) {
  const { docId } = action;
  try {
    // let application = yield select(state => state.application.application);
    const application = yield select(state => state.submission.application); // To be delted
    const authToken = yield select(state => state.login.authToken);
    const submitResponse = yield call(
      submitApplication,
      authToken,
      docId,
      application
    );
    if (!submitResponse.hasError) {
      action.callback(submitResponse);
    } else {
      action.callback(submitResponse);
    }
  } catch (e) {
    const errorMsg = e;
    const dataSyncResult = {
      hasError: true,
      errorMsg
    };
    yield put({
      type: ACTION_TYPES[CONFIG].FINISH_LOADING
    });
    action.callback(dataSyncResult);
  }
}
export default {
  dataSyncForSubmission,
  submission
};
