import PropTypes from "prop-types";
import React, { Component } from "react";
import { View } from "react-native";
import getStyleArray from "../../utilities/getStyleArray";
import styles from "./styles";

/**
 * <EABTable />
 * @description table component. please use it with <EABTableHeader />,
 *   <EABTableSectionHeader
 *   />, <EABTableSection /> and <EABTableFooter />. <EABTable /> will apply
 *   tableConfig to these component.
 * @param {object|number=} style - the table container custom style
 * @param {array} tableConfig - array of table cell config object
 * @param {string} tableConfig[].key - table cell key
 * @param {object} tableConfig[].style - table cell style
 * @example see ./code.sample.md
 * */
export default class EABTable extends Component {
  constructor(props) {
    super(props);
    this.setUpProps = this.setUpProps.bind(this);
    this.renderContent = this.renderContent.bind(this);
  }

  get tableChildren() {
    const { children } = this.props;
    return Array.isArray(children) ? children : [children];
  }

  setUpProps(child) {
    const { tableConfig } = this.props;
    return {
      ...child.props,
      tableConfig:
        child.props.tableConfig.length !== 0
          ? child.props.tableConfig
          : tableConfig,
      isInitialized: true
    };
  }

  renderContent() {
    return this.tableChildren.map(child => {
      if (child && child !== null) {
        if (Array.isArray(child)) {
          return child.map(c => React.cloneElement(c, this.setUpProps(c)));
        }
        return React.cloneElement(child, this.setUpProps(child));
      }
      return null;
    });
  }

  render() {
    const { style } = this.props;
    const { container } = styles;
    return (
      <View style={[container, ...getStyleArray(style)]}>
        {this.renderContent()}
      </View>
    );
  }
}

EABTable.propTypes = {
  style: PropTypes.oneOfType([
    PropTypes.number,
    PropTypes.object,
    PropTypes.array
  ]),
  tableConfig: PropTypes.arrayOf(
    PropTypes.shape({
      key: PropTypes.string.isRequired,
      style: PropTypes.oneOfType([
        PropTypes.number,
        PropTypes.object,
        PropTypes.array
      ])
    })
  ).isRequired,
  children: PropTypes.node.isRequired
};

EABTable.defaultProps = {
  style: []
};
