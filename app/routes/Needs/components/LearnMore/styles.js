import { StyleSheet } from "react-native";
import Theme from "../../../../theme";

/**
 * styles
 * */
export default StyleSheet.create({
  container: {
    width: "100%",
    height: "100%",
    justifyContent: "center",
    alignItems: "center"
  },
  scrollView: {
    width: "100%",
    height: "100%"
  },
  scrollViewContainerStyle: {
    paddingVertical: Theme.alignmentXL,
    justifyContent: "center",
    alignItems: "center",
    width: "100%"
  },
  cardBox: {
    flexDirection: "row",
    flexWrap: "wrap",
    justifyContent: "center",
    margin: -20,
    maxWidth: "100%"
  },
  card: {
    margin: 20
  },
  cardTitle: {
    ...Theme.display2,
    marginBottom: Theme.alignmentXXS
  }
});
