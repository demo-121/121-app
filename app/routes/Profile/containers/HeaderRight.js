import { ACTION_TYPES, REDUCER_TYPES, utilities } from "eab-web-api";
import { connect } from "react-redux";
import { TEXT_STORE } from "../../../constants/REDUCER_TYPES";
import HeaderRight from "../components/HeaderRight";

const { dataMapping } = utilities;
const { profileMapForm } = dataMapping;

const { OPTIONS_MAP, CLIENT_FORM, CLIENT } = REDUCER_TYPES;

/**
 * HeaderRight
 * @requires HeaderRight - HeaderRight UI
 * */
const mapStateToProps = state => ({
  clientForm: state[CLIENT_FORM],
  textStore: state[TEXT_STORE],
  optionsMap: state[OPTIONS_MAP],
  profile: state[CLIENT].profile,
  dependantProfiles: state[CLIENT].dependantProfiles
});

const mapDispatchToProps = dispatch => ({
  dispatch,
  reloadContactList(callback) {
    dispatch({
      type: ACTION_TYPES[CLIENT].GET_CONTACT_LIST,
      callback
    });
  },
  loadClient(profile) {
    dispatch({
      type: ACTION_TYPES[CLIENT_FORM].LOAD_CLIENT,
      ...profileMapForm(profile)
    });
  },
  readyToEditClient(callback) {
    dispatch({
      type: ACTION_TYPES[CLIENT_FORM].INITIAL_CLIENT_FORM,
      configData: {
        isCreate: false,
        isFamilyMember: false,
        isApplication: false,
        isProposerMissing: false,
        isFromProfile: true,
        isFromProduct: false
      },
      callback
    });
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(HeaderRight);
