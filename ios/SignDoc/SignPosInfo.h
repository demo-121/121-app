//
//  SignPosInfo.h
//  EASE
//
//  Created by Felix on 9/1/2019.
//  _
//
#include <UIKit/UIKit.h>

@interface SignPosInfo : NSObject {
  double posX;
  double posY;
}

@property (nonatomic, assign) double posX;
@property (nonatomic, assign) double posY;

+(SignPosInfo*) getPos;

@end
/* SignPosInfo_h */
