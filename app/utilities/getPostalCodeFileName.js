import * as _ from "lodash";

export default function(code) {
  const covPostalCode =
    code && code.length === 6
      ? Number(_.toString(code).substr(0, 2))
      : Number(`0${_.toString(code).substr(0, 1)}`);
  if (!_.isNumber(covPostalCode) || code.length !== 6) {
    return undefined;
  }
  if (covPostalCode < 20) {
    return "_00_19";
  } else if (covPostalCode < 30) {
    return "_20_29";
  } else if (covPostalCode < 40) {
    return "_30_39";
  } else if (covPostalCode < 50) {
    return "_40_49";
  } else if (covPostalCode < 60) {
    return "_50_59";
  } else if (covPostalCode < 70) {
    return "_60_69";
  } else if (covPostalCode < 80) {
    return "_70_79";
  } else if (covPostalCode < 90) {
    return "_80_89";
  }
  return "_";
}
