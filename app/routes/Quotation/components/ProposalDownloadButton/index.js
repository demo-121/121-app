import { REDUCER_TYPE_CHECK, REDUCER_TYPES } from "eab-web-api";
import PropTypes from "prop-types";
import React, { PureComponent } from "react";
import EABButton from "../../../../components/EABButton";
import TranslatedText from "../../../../containers/TranslatedText";
import Theme from "../../../../theme";
import { exportAttachmentFromCB } from "../../../../utilities/DocumentManager";
import styles from "./styles";

const { PROPOSAL } = REDUCER_TYPES;

/**
 * @description Proposal download button
 * */
export default class ProposalDownloadButton extends PureComponent {
  /**
   * @description indicate can download or not
   * @return {boolean} status
   * */
  get canDownload() {
    const { activatedKey, pdfData } = this.props;
    const pdf = pdfData && pdfData.find(data => data.id === activatedKey);

    return pdf && !!pdf.docId && !!pdf.attachmentId;
  }
  /**
   * @description if can download, return pdf data
   * @return {object}
   * */
  get targetPdfData() {
    const { canDownload } = this;
    const { activatedKey, pdfData } = this.props;

    return canDownload ? pdfData.find(data => data.id === activatedKey) : {};
  }

  render() {
    // =========================================================================
    // variables
    // =========================================================================
    const { canDownload, targetPdfData } = this;
    const { quotationId } = this.props;

    return canDownload ? (
      <EABButton
        style={styles.button}
        onPress={() => {
          exportAttachmentFromCB({
            docId: quotationId,
            attachmentId: targetPdfData.attachmentId,
            fileName: targetPdfData.fileName
          });
        }}
      >
        <TranslatedText
          style={Theme.textButtonLabelNormalAccent}
          path="proposal.button.download"
        />
      </EABButton>
    ) : null;
  }
}

ProposalDownloadButton.propTypes = {
  quotationId: PropTypes.string.isRequired,
  activatedKey: PropTypes.string.isRequired,
  pdfData: REDUCER_TYPE_CHECK[PROPOSAL].proposal.isRequired
};
