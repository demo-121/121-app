import PropTypes from "prop-types";
import React, { Component } from "react";
import { Image, TextInput, View } from "react-native";
import icSearchMini from "../../assets/images/icSearchMini.png";
import Theme from "../../theme";
import getStyleArray from "../../utilities/getStyleArray";
import styles from "./styles";
/**
 * <EABSearchBar />
 * @description text input component. base on
 *   {@link https://facebook.github.io/react-native/docs/textinput.html}
 * @param {string} value - the input field value
 * @param {function} onChange - value on change call back. return a onchange
 * @param {string} placeholder - input field label
 * @param {string} [keyboardType='default'] - determines which keyboard to open. check
 *   https://facebook.github.io/react-native/docs/textinput.html#keyboardtype to get more
 *   information
 * @param {boolean} [editable=true] - if false, text is not editable
 * @param {boolean=} isAutoFocus - focus the input field when launch page
 * @param {object|number=} style - text field container style.
 * */
class EABSearchBar extends Component {
  get styleArray() {
    const { style } = this.props;
    return getStyleArray(style);
  }

  get rootStyles() {
    const { container } = styles;
    return [container].concat(this.styleArray);
  }

  render() {
    const {
      editable,
      value,
      placeholder,
      keyboardType,
      isAutoFocus,
      onChange
    } = this.props;
    const { icon, textInput } = styles;
    return (
      <View style={this.rootStyles}>
        <Image style={icon} source={icSearchMini} />
        <TextInput
          testID={this.props.testID}
          style={textInput}
          editable={editable}
          value={value}
          placeholder={placeholder}
          placeholderTextColor={Theme.mediumGrey}
          keyboardType={keyboardType}
          autoFocus={isAutoFocus}
          onChangeText={onChange}
          clearButtonMode="always"
        />
      </View>
    );
  }
}

EABSearchBar.propTypes = {
  value: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
  editable: PropTypes.bool,
  placeholder: PropTypes.string,
  keyboardType: PropTypes.string,
  isAutoFocus: PropTypes.bool,
  style: PropTypes.oneOfType([
    PropTypes.number,
    PropTypes.object,
    PropTypes.array
  ]),
  testID: PropTypes.string
};

EABSearchBar.defaultProps = {
  placeholder: "Search",
  editable: true,
  keyboardType: "default",
  isAutoFocus: false,
  style: {},
  testID: ""
};

export default EABSearchBar;
