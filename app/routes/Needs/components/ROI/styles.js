import { StyleSheet } from "react-native";
import Theme from "../../../../theme";

export default StyleSheet.create({
  scrollView: {
    flex: 1,
    width: "100%"
  },
  scrollViewContent: {
    alignItems: "center"
  },
  container: {
    paddingVertical: Theme.alignmentXL,
    alignItems: "flex-start",
    width: Theme.boxMaxWidth
  },
  statement: { ...Theme.tagLine1Primary, paddingBottom: 15 },
  remark: {
    color: "grey",
    fontSize: 18,
    paddingTop: 20,
    paddingBottom: 20,
    width: 630
  },
  reasonWrapper: { width: "100%", marginTop: 20 },
  hintText: {
    ...Theme.fieldTextOrHelperTextNegative,
    marginTop: 10
  },
  star: {
    color: Theme.negative
  }
});
