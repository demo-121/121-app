import { connect } from "react-redux";
import { ACTION_TYPES, REDUCER_TYPES, NEEDS } from "eab-web-api";
import { TEXT_STORE } from "../../../constants/REDUCER_TYPES";
import AnalysisPlanningForChildrensHeadstart from "../components/AnalysisPlanningForChildrensHeadstart";

const { FNA, CLIENT } = REDUCER_TYPES;
const { PCHEADSTART } = NEEDS;

/**
 * AnalysisPlanningForChildrensHeadstart
 * @requires AnalysisPlanningForChildrensHeadstart - AnalysisPlanningForChildrensHeadstart UI
 * */
const mapStateToProps = state => ({
  textStore: state[TEXT_STORE],
  na: state[FNA].na,
  fe: state[FNA].fe,
  pda: state[FNA].pda,
  profile: state[CLIENT].profile,
  dependantProfiles: state[CLIENT].dependantProfiles,
  isPcHeadstartError: state[FNA].isNaError.pcHeadstart
});

const mapDispatchToProps = dispatch => ({
  initNaAnalysis: () => {
    dispatch({
      type: ACTION_TYPES[FNA].INIT_NA_ANALYSIS,
      aspect: PCHEADSTART
    });
  },
  updateNA: ({
    naData,
    pdaData,
    profileData,
    feData,
    dependantProfilesData
  }) => {
    dispatch({
      type: ACTION_TYPES[FNA].UPDATE_NA,
      naData: {
        ...naData,
        completedStep: 2
      },
      pdaData,
      profileData,
      feData,
      dependantProfilesData
    });
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AnalysisPlanningForChildrensHeadstart);
