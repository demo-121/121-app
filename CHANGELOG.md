# 1.3.6 - b006
* 37541: Incorrect header for sum assured/Benefit for ART (II) (IOS) [#2110](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/2110)
* 37542: IOS: eBI- ART2 - Redeposit Period value keep refreshing back to 1 during input  [#2109](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/2109)
* R14 - CR185 - medium - iOS: ART 2 - ePI a few blank pages inside [#2096](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/2096)
* 37539: Incorrect max value for duration of retirement in years (IOS) [#2107](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/2107)
* R14 - CR185 - High - iOS: ART 2 - errors in FNA report [#2092](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/2092)
* 37542: IOS: eBI- ART2 - Redeposit Period value keep refreshing back to 1 during input [#2109](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/2109)
* 37514: Incorrect benefit and limitation text when user only choose shield Plan A [#2111](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/2111)
* 37513: Extra text in Limitation section for shield when user choose Shield Plan A with enhance care rider. [#2112](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/2112)
* 37480: B&L text missing for shield [#2113](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/2113)
* 37540: IOS :ART2:Unable to proceed to Pre app from recommendation, recommendation shown as incomplete [#2114](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/2114)
* 37530: ePI | CR183 - iOS - Financial Consultant’s Name and Proposer’s Name is not displayed below their respective name field [#2115](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/2115)

# 1.3.6 - b005
Note: This is a non-product version of b004
* CR183 - inflight- one time message for inflight case is incorrect [#2083](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/2083)

# 1.3.6 - b004
* R14 - CR185 - medium - iOS: ART 2 - re-deposit period is reset to 1 [#2095](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/2095)
* R14 - CR185 - medium - iOS: Annual Retirement Income should not be shown for non-Retirement Needs [#2103](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/2103)
* 37526: Annual retirement income is missing under recommendation in FNA report. (IOS) [#2106](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/2106)
* R14 - CR185 - High - iOS: ART 2 - Plan code missing [#2093](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/2093)
* 37525: Annual retirement income is not showing in FNA report for inflight cases. (IOS) [#2105](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/2105)
* 37431: IOS | ePI |CR177 | ALT | PI |Spacing Issues [#2104](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/2104)
* 37436: ePI | CR177 |ALT and LMP |ECI Rider is not available when residence is UAE |Post R13 [#2077](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/2077)

# 1.3.6 - b003
* R14 - CR185 - medium - iOS: missing picture icon in shortfall (FNA Summary) for retirement planning [#2100](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/2100)
* R14 - CR185 - medium - iOS: Should not show Annual Retirement Income for surplus [#2101](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/2101)

# 1.3.6 - b002
Note: release 14 app sync CR185 & CR194 with web
* CR183 - 37481- eBI- Basic Shield (Plan A,B and Standard) PS is appearing twice and Enhanced Care Rider (Plan A,B Standard) PS is not showing. [#2085](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/2085)
* CR138 - Benefit and limitation text is not changed after switch to other plan in client choice [#2086](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/2086)

# 1.3.6 - b001
Note: release 14 for non-production
# v1.3.5 - b018
* 37433: eBI - IOS - AXA Wealth Treasure - PI Template Issues - to be fixed POST Release 13 [#2080](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/2080)

# v1.3.5 - b017
* 37433: eBI - IOS - AXA Wealth Treasure - PI Template Issues - to be fixed POST Release 13 [#2080](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/2080)

# v1.3.5 - b016
* 37433: eBI - IOS - AXA Wealth Treasure - PI Template Issues - to be fixed POST Release 13 [#2080](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/2080)

# v1.3.5 - b015
* 37433: eBI - IOS - AXA Wealth Treasure - PI Template Issues - to be fixed POST Release 13 [#2080](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/2080)

# v1.3.5 - b014
* 37477: ePI | TP and TPP | Plancode,Date,Plan Details ,Proposer and LA details not populated in Main PI page 1 [#2075](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/2075)
* 37433: eBI - IOS - AXA Wealth Treasure - PI Template Issues - to be fixed POST Release 13 [#2080](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/2080)
* 37472: unable to scroll FNA report when user scroll on the report [#2082](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/2082)
* Severe - Unable to generate FNA report [#2081](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/2081)

# v1.3.5 - b013
* CR193 – Flexi Series changes - cannot generate proposal for both FS and FP [#2071](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/2071)
* 37433: eBI - IOS - AXA Wealth Treasure - PI Template Issues - to be fixed POST Release 13 [#2080](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/2080)
* 37472: unable to scroll FNA report when user scroll on the report [#2076](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/2076)
* 37476: unable to view cases under FNA history [#2074](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/2074)
* 37473: IOS : eApp:Unable to do online payment [#2072](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/2072)
* 37421: IOS |ePI | CR177 | ESP and SS |Illustration Chart IRR % in dropdown is displayed as NaN%-to be fixed post R13 [#2079](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/2079)
* 37477: ePI | TP and TPP | Plancode,Date,Plan Details ,Proposer and LA details not populated in Main PI page 1 [#2075](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/2075)
* 37478: IOS: eBI - Pulsar - Overlapping in Acknowledgement Pag [#2073](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/2073)
* 37479: ePI | ALT,LMP,ART,RHP | Unable to create Proposal ,Continuously loading when clicked on Create Proposal [#2078](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/2078)
* 37473: IOS : eApp:Unable to do online payment [#2072](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/2072)

# v1.3.5 - b012
* iOS Upgrade - Severe - Products screen does not respond to the “party” chosen [#2056](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/2056)

# v1.3.5 - b011
* iOS Upgrade - High - Rider check box status is not aligned to selected rider [#2061](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/2061)
* iOS Upgrade - High - Able to check dimmed check box [#2062](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/2062)
* 37433 - eBI - IOS - AXA Wealth Treasure - PI Template Issues - to be fixed POST Release 13 [#2066](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/2066)
* CR193 – Flexi Series changes [#2070](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/2070)

# v1.3.5 - b010
* iOS Upgrade - Medium - Incremental Counter error [#2054](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/2054)
* iOS Upgrade - Severe - Products screen does not respond to the “party” chosen [#2056](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/2056)
* 37432: eBI - IOS - AXA Wealth Treasure - UI Issues - to be fixed POST Release 13 [#2064](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/2064)
* 37433: eBI - IOS - AXA Wealth Treasure - PI Template Issues - to be fixed POST Release 13 [#2066](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/2066)
* iOS Upgrade - High - Navigation is not work after generate proposal [#2067](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/2067)
* iOS Upgrade - High - No internet dialog pop up when send email even there is connection [#2068](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/2068)
* 37433: eBI - IOS - AXA Wealth Treasure - PI Template Issues - to be fixed POST Release 13 [#2069](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/2069)

# v1.3.5 - b009
Note: release 13 for non-production by using SDK 12 with environment choosing page
* 37435: Text got crop in consent notice page in IOS version [#2060](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/2060)
* iOS Upgrade - Severe - checkboxes not respond [#2055](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/2055)
* iOS Upgrade - High - Some Female specific products missing [#2058](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/2058)
* iOS Upgrade - Severe - When Create Proposal button is clicked, PI either could not be generated or generating a wrong PI [#2059](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/2059)
* 37415: Director received notification on 'unable to submit case as there is no supervisor linked' in EASE iOS app [#2053](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/2053)

# v1.3.5 - b008
Note: release 13 for non-production by using SDK 12 with environment choosing page

# v1.3.5 - b007
Note: release 13 for non-production by using SDK 12

# v1.3.4 - b020
Note: release 13 for production

# v1.3.4 - b019
* 37426: eBI: IOS VUL–No Error Message display at Per Mille loading Period in Year(s) & it is displaying (Min Prem and Min SA are met) [#2052](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/2052)
* 37350: eBI: IOS Private Wealth VUL – PI Template Issues.
 [#2050](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/2050)
* 37411: IOS | ePI | CR177 | TP, TPP, ALT | Missing riders Product Summary
 [#2043](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/2043)

# v1.3.4 - b018
* 37179: eApp : IOS - Fields are editable before the click of done button-AWI ,RHP,ART [#2051](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/2051)

# v1.3.4 - b017
* 37411: IOS | ePI | CR177 | TP, TPP, ALT | Missing riders Product Summary [#2043](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/2043)
* 37350: eBI: IOS Private Wealth VUL – PI Template Issues. [#2050](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/2050)
* 37179: eApp : IOS - Fields are editable before the click of done button-AWI ,RHP,ART [#2051](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/2051)
* 37337: eBI: IOS VUL - Quotation Screen UI Issues. [#2024](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/2024)

# v1.3.4 - b016
* 37337: eBI: IOS VUL - Quotation Screen UI Issues. [#2024](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/2024)
* 37412: eBI: IOS_Private Wealth VUL- Withdrawal UI Issues [#2044](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/2044)
* 37215: Getting time bomb error message upon installation of V 15 [#2045](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/2045)
* 37414: eBI - IOS - App crashed upon clicking Illustration Chart [#2046](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/2046)
* 37416: eBI - iOS - Chosen Investment Rate of Return is not equal to 4 percent, missing percent format in PI template [#2047](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/2047)
* 37335: IOS :Cross border warning note missing in Personal details section [#2048](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/2048)
* 37413: eBI: IOS/Web - VUL - Illustration IRR is showing 1.5.0 % in Policy Illustration. It should shown 1.5%. [#2049](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/2049)

# v1.3.4 - b015
* 37404: ePI | CR177 | :IOS Invalidation is not happening when the City/state field of a profile is changed after PI generation. [#2037](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/2037)
* 37352: eBI: IOS Private Wealth VUL – Minimum Premium tooltip the word “Premium” first letter should display as capital letter. [#2019](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/2019)
* 37390: eBI: IOS VUL - Singpost Channel should not allowed. V1.3.4-b10 (SIT Environment) [#2039](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/2039)
* 37407: eBI - iOS - 3rd withdrawal UI error [#2038](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/2038)
* 37409: Ebi: IOS VUL – Risk Class (Standard, Preferred and Super Preferred) Per mille loading should be greyout and uneditable fields. [#2040](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/2040)
* 37386: IOS :Ciity/State and Others field is not populated into personal details and Eapp form [#2031](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/2031)
* 37408: eBI: IOS VUL Products - PI Cover Page is showing blank/empty details at Page 1. [#2042](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/2042)
* 37179: eApp : Fields are editable before the click of done button-AWI ,RHP,ART [#1980](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1980)
* 37384: IOS :Ciity/State and Others field is not populated into personal details and Eapp form [#2031](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/2031)

# v1.3.4 - b014
* Sync to latest products, aligned with web
* 37342: eBI:IOS Private Wealth VUL-% of Account Value- Minimum Withdrawal is not met. (User still allowing to create proposal). [#2021](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/2021)
* 37397: ePI - iOS | CR177 | Residence : United States | ECI rider is available for selection incorrectly for ALT [#2033](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/2033)
* 37391: IOS | ePI | CR177 | ALT| Text Overlap [#2034](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/2034)
* 37392: IOS | ePI | CR177 | Philippines-Mindanao, Sulu Archipelago, Zamboanga Peninsula |All Products must be declined [#2030](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/2030)
* 37393: IOS | ePI |CR177|Philippines-OTHER THAN Mindanao, Sulu Archipelago Zamboanga Peninsula |ECI must be declined [#2035](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/2035)
* 37384: IOS :Ciity/State and Others field is not populated into personal details and Eapp form [#2031](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/2031)
* 37340:  IOS :New city list for few countries is not aligned as per CR177 [#2036](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/2036)

# v1.3.4 - b013
* 37352: eBI: IOS Private Wealth VUL – Minimum Premium tooltip the word “Premium” first letter should display as capital letter.[#2019](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/2019)
* 37307: eBI - iOS - Funds - Asset Class is not displayed for Amundi Funds Cash USD in iOS
 [#2029](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/2029)
* 37378: ePI: IOS Private Wealth VUL - Missing Pop Up message [#2028](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/2028)
* 37384: 37384: IOS :Ciity/State and Others field is not populated into personal details and Eapp form [#2031](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/2031)
* 37386: 37386: eBI | IOS - CR184 - FP/FS: Yearly Renewable Term is not the default option under Insurance Charge after basic benefit selection [#2032](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/2032)

# v1.3.4 - b012
* 37381: ePI_IOS Private Wealth VUL - The Illustration Chart is not reflecting any values for death benefit and surrender value. [#2027](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/2027)
* 37344: eBI: IOS VUL – Illustration Chart – Dropdown list should display as value. IRR_Low “4.00” and IRR_High “8.00”. Description
 [#2023](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/2023)
* 37379: ePI: IOS VUL - Extra Nationality_Residency message [#2025](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/2025)
* 37337: eBI: IOS VUL - Quotation Screen UI Issues. [#2024](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/2024)
* 37229: Page hanging after clicking save. [#2026](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/2026)

# v1.3.4 - b011
* Sync to latest products, aligned with web
* Updated ReleaseVersion to 10.0
* 37337: eBI: IOS VUL - Quotation Screen UI Issues. [#2024](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/2024)
* 37344: eBI: IOS VUL – Illustration Chart – Dropdown list should display as value. IRR_Low “4.00” and IRR_High “8.00”. Description
 [#2023](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/2023)
* 37348: EApp: IOS Mainland China Visitor declaration form is missing if mailing address is China [#2018](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/2018)

# v1.3.4 - b010
* 37348: EApp: IOS Mainland China Visitor declaration form is missing if mailing address is China [#2018](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/2018)
* 37352: eBI: IOS Private Wealth VUL – Minimum Premium tooltip the word “Premium” first letter should display as capital letter.
 [#2019](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/2019)
* 37355: eBI: IOS Private Wealth VUL – Illustration Chart – Extra comma “,” in front of VALUE at Surrender Value. [#2020](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/2020)
* 37342: eBI:IOS Private Wealth VUL-% of Account Value- Minimum Withdrawal is not met. (User still allowing to create proposal). [#2021](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/2021)
* 37296: iOS - Connection error on submit freezes the entire screen.
 (User still allowing to create proposal). [#2022](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/2022)
* 37344: eBI: IOS VUL – Illustration Chart – Dropdown list should display as value. IRR_Low “4.00” and IRR_High “8.00”. Description
 [#2023](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/2023)

# v1.3.4 - b009
* Sync to latest products, aligned with web
* 37336: IOS :COE missing in the supporting docs for 1st and 3rd party cases [#2014](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/2014)
* 37331: IOS :Unable to proceed to EApp on clicking the Apply button [#2015](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/2015)
* 37338: IOS :No value can be entered in the Others field of City /state in the client profile [#2016](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/2016)
* 37339: IOS :Crashing at Signature page [#2017](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/2017)

# v1.3.4 - b008
* CR180 - (iOS) Three funds - AUSD, FGOA and FGOB are not found in AWT, AWI (cash/srs) and Pulsar [#191](http://192.168.222.60:8001/AXA-SG/ease-web/issues/191)
* VUL - iOS- Extra orange bar of Cover page [#185]((http://192.168.222.60:8001/AXA-SG/ease-web/issues/185)
* 37331: APP :Unable to proceed to EApp on clicking the Apply button [#2011]((http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/2011)
* IOS UAT | Auto Data sync issues [#2012]((http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/2012)
* Sync app-core api with web

# v1.3.4 - b007
* CR180 - (iOS) Three funds - AUSD, FGOA and FGOB are not found in AWT, AWI (cash/srs) and Pulsar [#191](http://192.168.222.60:8001/AXA-SG/ease-web/issues/191)
* VUL - iOS Options [#146]((http://192.168.222.60:8001/AXA-SG/ease-web/issues/146)
* VUL - iOS - UI Update - Joint product feature on UI support/ changes [#95]((http://192.168.222.60:8001/AXA-SG/ease-web/issues/95)
* VUL - iOS - UI Update - Add topic title on the product grouping [#93]((http://192.168.222.60:8001/AXA-SG/ease-web/issues/93)
* VUL - iOS - UI update - Add decimal part support for the policy option text field [#94]((http://192.168.222.60:8001/AXA-SG/ease-web/issues/94)
* CR191 - iOS - VUL Template [#196]((http://192.168.222.60:8001/AXA-SG/ease-web/issues/196)
* CR180 - iOS - Fund Changes Feb [#107]((http://192.168.222.60:8001/AXA-SG/ease-web/issues/107)
* CR184 - Web - Removal of Level Pay COI options for FP and FS [#108]((http://192.168.222.60:8001/AXA-SG/ease-web/issues/108)
* CR180 - (web) PHS of Amundi Funds Cash USD is wrong, it is showing PHS for FGOA [#190]((http://192.168.222.60:8001/AXA-SG/ease-web/issues/190)
* VUL - iOS - Country with city excluded for banning to access the product [#96]((http://192.168.222.60:8001/AXA-SG/ease-web/issues/96)
* CR192 - iOS - Changes under NATIONALITY_Cat tab [#174]((http://192.168.222.60:8001/AXA-SG/ease-web/issues/174)
* CR192 - iOS - Changes under RESIDENCY_Cat tab [#175]((http://192.168.222.60:8001/AXA-SG/ease-web/issues/175)
* CR192 - iOS - Changes under ALL RULES tab [#173]((http://192.168.222.60:8001/AXA-SG/ease-web/issues/173)
* Solve unable to generate products list

# v1.3.4 - b006
Note release 13 for non-production & CR changes 180,184, 191, 192
# v1.3.6 - b006
Note: Test upgrade

# v1.3.5 - b005
Note: Test upgrade build (Per-User Project setting -> Build system: Use Shared Setting)

# v1.3.5 - b004
Note: Test upgrade build (Release Mode)

# v1.3.5 - b003
Note: Test upgrade build (Debug Mode)

# v1.3.5 - b002
Note: Test upgrade build

# v1.3.5 - b001
Note: Test upgrade build

# v1.3.4 - b005
* 37192: EApp:Name of organisation is missing in EApp form. [#2008](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/2008)
* 37230: Incorrect layout when user search client [#2006](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/2006)
* 36975: Online payment: Submission Issues for Shield [#1956](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1956)
* 36963: IOS BI SHIELD | Clien tprofile pop up on SHIELD BI Quotaton page should be aligned with web [#1905](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1905)
* 37179: eApp : Fields are editable before the click of done button-AWI ,RHP,ART [#1980](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1980)
* 37130: Error 400 when user is doing an online login [#1957](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1957)

# v1.3.4 - b004
Note release 13 for non-production & CR changes

# v1.3.4 - b003
* 37120: unable to proceed for Retirement needs for spouse even though all mandatory fields had been entered [#1950](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1950)

# v1.3.4 - b002
Note: release 13 for non-production

# v1.3.4 - b001
Note: release 13 for non-production

# v1.3.4 - b003
* 37120: unable to proceed for Retirement needs for spouse even though all mandatory fields had been entered [#1950](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1950)


# v1.3.3 - b015
Note: postLaunch branch for production

# v1.3.3 - b014
Note: postLaunch branch for non-production

# v1.3.3 - b013
Note: postLaunch branch for production 

# v1.3.3 - b012
Note: v1.3.3 - b011 postLaunch branch for production 

# v1.3.3 - b011
Note: postLaunch branch and fix Data Sync Issue

# v1.3.3 - b010
Note: Fix build for new product RSA

# v1.3.3 - b006
Note: First build for newProductTest Branch

# v1.3.3 - b005
* 35657: Framework : Unable to upload Agent Profile Picture and it showing blank. [#2000](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/2000)
* 37197: eBI : LMP_ Unable to view the rider Critical Illness PremiumEraser in quotation page [#1999](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1999)
* 37196: eBI - Pulsar & AWT - Change of currency without selecting payment mode allows User to create proposal [#1994](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1994)
* 37159: eBI - Architas fund range should not be available on the UI for Inspire Flexi Series & AWI - CPF in Build a Portfolio Option [#1988](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1988)
* 37198: ESCC product not visible to users not holding HI certificate [#1987](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1987)
# v1.3.4 - b002
Note: release 13 for non-production

# v1.3.4 - b001
Note: release 13 for non-production

# v1.3.3 - b004
* 35657: Framework : Unable to upload Agent Profile Picture and it showing blank. [#2000](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/2000)
* 37197: eBI : LMP_ Unable to view the rider Critical Illness PremiumEraser in quotation page [#1999](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1999)
* 37196: eBI - Pulsar & AWT - Change of currency without selecting payment mode allows User to create proposal [#1994](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1994)
* 37159: eBI - Architas fund range should not be available on the UI for Inspire Flexi Series & AWI - CPF in Build a Portfolio Option [#1988](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1988)
* 37198: ESCC product not visible to users not holding HI certificate [#1987](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1987)

# v1.3.3 - b004
* 35657: Framework : Unable to upload Agent Profile Picture and it showing blank. [#2000](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/2000)
* 37197: eBI : LMP_ Unable to view the rider Critical Illness PremiumEraser in quotation page [#1999](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1999)
* 37196: eBI - Pulsar & AWT - Change of currency without selecting payment mode allows User to create proposal [#1994](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1994)
* 37159: eBI - Architas fund range should not be available on the UI for Inspire Flexi Series & AWI - CPF in Build a Portfolio Option [#1988](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1988)
* 37198: ESCC product not visible to users not holding HI certificate [#1987](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1987)

# v1.3.3 - b003
Note: Fix issue always show cover page in BI
* 37194: eBI - AXA Singapore Dollar Fund (SGD) - Missing Text (SGD) in UI Screen and generated PI [#1983](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1983)
* 37195: eFNA - ART Product - Wrong sentence displayed in FNA Report [#1984](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1984)

# v1.3.3 - b002

# v1.3.3 - b001
Note: It's a copy of v1.3.1 build 007. It's for Apple's review firstly.

# v1.3.2 - b007
Note: It's production version of v1.3.2 - 006 

# v1.3.2 - b006
### Defects
* 37188: eBI - IOS - Display of Legg Mason SG Dollar Fund on the Fund list [#1981](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1981)

# v1.3.2 - b005
Note: update sysParameter

# v1.3.2 - b004
Note: Build again

# v1.3.2 - b003
### Defects
* 37178: IOS | Loading at client and product page
* 37167: eBI - IOS -CR172: Missing one-time message for Invalidation and cases are not invalidated.

# v1.3.2 - b002
* 37167: eBI - IOS -CR172: Missing one-time message for Invalidation and cases are not invalidated.

# v1.3.2 - b001
Note: It's a copy of v1.3.0 build 026. It's for Apple's review firstly.

# v1.3.1 - b007
### Defects
* 37179: eApp : Fields are editable before the click of done button-AWI ,RHP,ART [#1980](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1980)
* 37169: Missing one digit under Annual replacement/Living expenses for income pro and CI need [#1978](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1978)
* 36997 UAT [#1977](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1977)
* 36656 [#1976](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1976)
* 37157: eApp: Data sync defects after online payment/submission in web [#1974](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1974)
* 36963: IOS BI SHIELD | Clien tprofile pop up on SHIELD BI Quotaton page should be aligned with web [#1905](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1905)

# v1.3.1 - b006
### Defects
* 37130: Error 400 when user is doing an online login [#1975](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1975)
* 35686: eApp: Signatures should be in the middle of the box after signing [#1774](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1774)
* 36589: eBI : Shield_STP case_There is overlapping of wordings in PI Signature page 1 [#1613](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1613)
* 36508 Eapp : Application Form sign is not center aligned [#1549](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1549)

# v1.3.1 - b005
Note: release 12 Branch and fixed version of v1.3.1 - b003

# v1.3.1 - b004
### Defects
* 37157: eApp: Data sync defects after online payment/submission in web [#1974](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1974)
* 37160: eSub - "submission ocurred" message comes even for a new client [#1973](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1973)
* 37156: Product type not reset after changing priority [#1971](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1971)
* 37154: FNA got reset after user review PDPA [#1970](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1970)
* 36995: EAPP: Insurability Information - Medical and Health Information questions for all Life Products [#1923](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1923)

# v1.3.1 - b003
Note: release 12 Branch

# v1.3.1 - b002
### Defects
* 36231: eAPP - after data sync, incorrect number of applications in WEB,on client search [#1969](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1969)
* 36975: Online payment: Submission Issues for Shield [#1956](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1956)

# v1.3.1 - b001
Note: It's a copy of v1.3.0 build 026. It's for Apple's review firstly.

# v1.3.0 - b036
Note: production version of v1.3.0 build 35

# v1.3.0 - b035
### Defects
* 37145: eApp :Edit existing insurance portfolio is not working for ART [#1968](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1968)
* 37057 [#1967](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1967)
* 37140: eBI: ART_BI template issues (Single / Regular Premium) [#1962](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1962)
* 37130: Error 400 when user is doing an online login [#1957](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1957)
* 36975: Online payment: Submission Issues for Shield [#1956](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1956)

# v1.3.0 - b034
### Defects
* 37057 [#1967](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1967)
* 37146: eBI :DTA (Single) – Product Summary for (Waiver of Premium and Waiver of Premium Plus) is incorrect. [#1966](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1966)
* 37130: Error 400 when user is doing an online login [#1957](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1957)

# v1.3.0 - b033
### Defects
* 37142: eBI - unable to create proposal for ART [#1965](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1965)
* 37137 [#1964](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1964)

# v1.3.0 - b032
### Defects
* 36975: Online payment: Submission Issues for Shield [#1956](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1956)

# v1.3.0 - b031
### Payment

# v1.3.0 - b030
### Defects
* 37140: eBI: ART_BI template issues (Single / Regular Premium) [#1962](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1962)
* 37135: EFNA - Needs - Getting message BI will be invalidated even when there were no changes made. [#1961](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1961)
* 37073: eBI - Invalidation - Product Types are not showing previous selection after Invalidation in FNA Redo [#1943](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1943)

# v1.3.0 - b028
### Defects
36978: IOS Sync | Online payment on web completed, but offline payment got submitted on IOS [#1960](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1960)

# v1.3.0 - b027
### Defects
* 36960: eBI : DTA Single & DTA Joint_Cover page and PI , the wordings are overlapped. [#1959](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1959)
* 36859: eBI: AWI/AWT : Unable to receive email at Application Stage [#1958](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1958)
* 37118: Regression: EAPP: Eapp form layout issue [#1949](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1949)
* 37103: eApp :Online payment :User is able to do double payment after successful online payment if pay button is clicked before data syn [#1928](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1928)
* 36913: eBI - ALT - BI Template [#1847](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1847)


# v1.3.0 - b026
### Defects
* 36868: Trusted individual section is showing as undefined [#1785](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1785)
* 36975: Online payment: Submission Issues for Shield [#1956](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1956)
* 36965: For shield, post successful payment, user is redirected to applications tab and then app crashes and displays white screen. [#1951](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1951)
* 37132: eApp ESCC Payment drop down not displayed after one bundling is invalidated [#1955](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1955)

# v1.3.0 - b025
### Defects
* 36975: Online payment: Submission Issues for Shield [#1956](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1956)
* 37132: eApp ESCC Payment drop down not displayed after one bundling is invalidated [#1955](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1955)
* 36965: For shield, post successful payment, user is redirected to applications tab and then app crashes and displays white screen.[#1951](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1951)
* 37126, 37125 [#1954](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1954)
* 36989: eApp: BandAid: ROP table not populating in eapp declaration for existing…
 [#1953](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1953)
* 38692 Re-deposit Period is shown with e.g. "7 Year(s)".
 [#1952](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1952)
 * 37115: eApp - validation issue of Mailing Address
 [#1944](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1944)
* 37073: eBI - Invalidation - Product Types are not showing previous selection after Invalidation in FNA Redo
 [#1943](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1943)
* 37130: Error 400 when user is doing an online login [#1957](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1957)

# v1.3.0 - b024
### Skip

# v1.3.0 - b023
### Defects
* 37120: unable to proceed for Retirement needs for spouse even though all mandatory fields had been entered [#1950](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1950)
* 37118: Regression: EAPP: Eapp form layout issue [#1949](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1949)
* 37117: eBI - MA, MA PLUS, FA, FA PLUS - Basic plan name is changed after requote [#1948](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1948)
* 37116: eBI : ART_Tool Tip is not displaying in quotation page [#1947](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1947)
* 37112: eBI: Surrender Value and Table of Deduction (Incorrect Values). From Policy Year 3 onwards. [#1946](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1946)
* 37119: eBI : ART_Premium Waiver (UN) rider value is not consistent in displaying the correct value at Quotation page & PI [#1945](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1945)
* 37115: eApp - validation issue of Mailing Address [#1944](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1944)
* 37073: eBI - Invalidation - Product Types are not showing previous selection after Invalidation in FNA Redo [#1943](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1943)
* 37101: eApp :Online payment :User is able to do double payment after successful online payment when logged in offline [#1926](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1926)
* 36901: eBI: ART - Cover Page and PI Template Issues. Description [#1870](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1870)
* 0036913: eBI - ALT - BI Template [#1847](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1847)
* 36955: EAPP: EAPP form formatting issue for ALT [#1830](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1830)

# v1.3.0 - b022
### Defects
* 37103: eApp :Online payment :User is able to do double payment after successful online payment if pay button is clicked before data syn [#1928](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1928)


# v1.3.0 - b021
### Defects
* 36860: eBI - Products Page - unable to see products name in full and require to scroll right and left to see all the products [#1942](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1942)
* 36979: eBI : MA, MA Plus, FA & FA Plus_Product Name is display as MumCare in Quick quote History [#1941](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1942)

# v1.3.0 - b020
### Defects
* 37111: unable to receive email at BI and Pre app stage for AWT [#1940](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1940)
* 37110: unable to receive pre app email for AWI and ART [#1938](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1938)
* 37103: eApp :Online payment :User is able to do double payment after successful online payment if pay button is clicked before data syn [#1928](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1928)
* 37094: eAPP: sucessful credit payment in wildcard did not get the successful details shown on eApp payment screen [#1939](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1939)
* 37100: eFNA - Pre-Eapp mails not recieved by client for ART [#1936](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1936)
* 37095: Error message pop up when user click on pay for IPP options [#1937](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1937)
* 36928: eBI - ART - Premium Waiver (UN) Values Issues [#1935](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1935)

# v1.3.0 - b019
### Defects
* 37062: There is a Re-Try button when system auto sync at the second time [#1934](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1934)
* 36989 post launch [#1933](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1933)
* 37040: eBI - missing client profile after app crash upon leaving the ipad idle and running in the background [#1932](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1932)
* 37105: eBI | ESCC: Term and Condition PDF is missing after ESCC PS when "Yes" is selected for "Bought a policy from AXA.." question [#1931](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1931)
* 37066: eApp:Currency symbol worng for foreign currency [#1930](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1930)
* 37088 UAT  [#1929](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1929)
* 37037: EBI | ESCC Terms and condition field is missing [#1885](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1885)
* 36927: Incorrect numbering in priority page [#1814](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1814)

# v1.3.0 - b018
### Defects
* 37103: eApp :Online payment :User is able to do double payment after successful online payment if pay button is clicked before data syn [#1928](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1928)
* 37091: eAPP: ESCC - Backdating is no. “selected commencement date” should not be shown [#1927](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1927)
* 37101: eApp :Online payment :User is able to do double payment after successful online payment when logged in offline [#1926](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1926)
* 36969: Online Payment: Pulsar Submission Issue [#1925](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1925)
* 36919, 37057 [#1924](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1924)
* 36995: EAPP: Insurability Information - Medical and Health Information questions for all Life Products [#1923](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1923)
* 36979 hot fix for  [#1922](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1922)
* 37092: eApp - Personal Details | Referrer ID validation issue [#1921](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1921)
* 37052: eFNA 0 FNA Report - Blank Page appearing before "My Budget section" - Page 13 [#1898](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1898)

# v1.3.0 - b017
### Defects
* 37042: eBI : ART - $1 difference in TDC on the cover page and $1 difference in supplementary death benefit [#1920](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1920)
* 37063: eBI: DATA Sync – (AWI CPF) Product image is not appearing from EASE Web to IOS. [#1908](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1908)
* 36901: eBI: ART - Cover Page and PI Template Issues. Description
[#1870](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1870)
* 0036913: eBI - ALT - BI Template
[#1847](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1847)
* 36959: User face error 007 when trigger pre app email. [#1853](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1853)

# v1.3.0 - b016
### Defects
* 36849: e-BI | Quotation Page for ART with UN Rider [#1918](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1918)
* 37083: eBI - ART - Regular Premium value issues in Quotation page [#1917](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1917)
* 37081, 36932 [#1916](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1916)
* 0037071: EAPP: Payment Page when IPP is selected, it keeps loading [#1915](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1915)

# v1.3.0 - b015
### Defects
* 0035994: eApp:Insurability Info question 1-SHIELD [#1232](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1232)
* 37037: EBI | ESCC Terms and condition field is missing [#1885](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1885)
* 36909: eBI | User able to generate PI before the launch date (8 Oct 2018) for AXA Life Treasure and AXA Retire Treasure. [#1810](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1810)
* 0036986: eBI - ESCC - Annualized Premium Issues [#1842](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1842)
* 36905: eBI: AWI Cash/SRS – The user is allowing to key in premium with DECIMAL at Premium field. [#1899](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1899)
* 36825: eApp :Plan Details blank for ART [#1770](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1770)
* 36919: eApp: ESCC : Plan Details page is blank [#1913](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1913)

# v1.3.0 - b014
### Defects
* 36918: eBI - ALT - Select Riders pop-up issues [#1912](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1912)
* 36968, 36842 [#1911](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1911)
* 37022: eBI - DVT - Quick Quote History Refresh Issue [#1910](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1910)
* 36923 [#1909](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1909)
* 37063: eBI: DATA Sync – (AWI CPF) Product image is not appearing from EASE Web to IOS. [#1908](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1908)
* 36938: eBI: MA, MA PLUS, FA & FA PLUS_Headers in quotation page are too wide [#1904](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1904)
* 36994: EAPP: EAPP form formatting issue under Residency & Insurance Info for ALT,AWI,ART [#1903](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1903)
* 37074: eBI - ILP – UI Issue with Adjust Fund Allocation, The Word “Total of XX Records” of the 3rd risk rating is not showing. [#1901](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1901)
* 36994: EAPP: EAPP form formatting issue under Residency & Insurance Info for ALT,AWI,ART [#1900](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1900)
* 36922: eBI: ART - All 1st Party Riders' PS are missing [#1835](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1835)
* 36955: EAPP: EAPP form formatting issue for ALT [#1830](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1830)
* 36907: eBI - MA, MA PLUS, FA, FA PLUS - no prompt message being displayed [#1829](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1829)
* 36840: eApp: AWI (OA/SA): SAQ Declaration notes defect [#1788](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1788)
* 36775: eBI - AWT - Template issues when more than three funds are selected Description [#1735](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1735)

# v1.3.0 - b013
### Defects
* 36983, 36989 post launch [#1907](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1907)
* 37062: There is a Re-Try button when system auto sync at the second time [#1906](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1906)
* 36837: Missing sentence in FNA report for ART under recommendation. [#1902](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1902)
* 36905: eBI: AWI Cash/SRS – The user is allowing to key in premium with DECIMAL at Premium field. [#1899](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1899)
* 36959: User face error 007 when trigger pre app email. [#1853](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1853)
* 37070: eBI : Shield : Quick Quote History - Life Assureds name are not shown in full [#1896](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1896)


# v1.3.0 - b012
### Defects
* 37052: eFNA 0 FNA Report - Blank Page appearing before "My Budget section" - Page 13 [#1898](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1898)
* 37070: eBI : Shield : Quick Quote History - Life Assureds name are not shown in full [#1896](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1896)
* 37008: Achievement is not showing for agent profile [#1895](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1895)
* 37044: eFNA - Client Profile - Deependant profile is getting duplicated on randm basis [#1893](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1893)

# v1.3.0 - b011
### Defects
* 37046: eFNA - CI need and Plnning for child - CI is not done but ESCC is available [#1892](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1892)
* 37045: eFNA - CI need is not done but ESCC is available for proposer [#1890](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1890)
* 37056: Needs analysis become incomplete when user click on next on product type page [#1889](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1889)
* 37000: eApp: ESCC : eapp form fotmating issue for residency qns [#1874](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1874)
* 36956: EAPP: Payment page [#1827](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1827)
* 0036808: EApp :Incorrect age for certain DOB [#1783](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1783)

# v1.3.0 - b010
### Defects
* 36842: eApp: AWI (OA/SA): OA/SA Payment defect [#1884](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1884)
* UAT 37048 [#1883](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1883)
* 37026, 37006, 36923, 36964, 36985, 36979, 36988, 37059, 36990 [#1882](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1882)
* 37057: EAPP: EAPP form border line issue for AWT under Personal Details [#1879](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1879)
* 36467: eApp::Social Visit pass/others tool tip alignment issue [#1878](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1878)
* 36919 [#1858](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1858)
* 36868: Trusted individual section is showing as undefined [#1785](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1785)
* 35592 [#1543](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1543)

# v1.3.0 - b009
### Defects
* 37057: EAPP: EAPP form border line issue for AWT under Personal Details [#1879](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1879)
* 36467: eApp::Social Visit pass/others tool tip alignment issue [#1878](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1878)
* 36971: After user delete the BI at applying tab, BI is totally deleted. [#1877](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1877)
* 36915: eApp: AWI (OA/SA): RSP details are not displayed in Plan Details UI [#1876](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1876)
* 37043: eApp:Double colon appearing inside medical question [#1875](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1875)
* 36962: eBI : ESCC_Cover page and PI template issues [#1839](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1839)
* 36195, 35779 [#1654](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1654)

# v1.3.0 - b008
### Defects
* 35724: eApp: All products :DBS IPP tool tip is missing [#1873](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1873)
* 37021: For shield, it should not ask ROP question for non-recommended product [#1865](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1865)
* 37004, 36823 [#1862](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1862)
* 37031: eBI - Display of ipad keyboard for Quotation screen and Options tab [#1860](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1860)
* 37001: eApp: ESCC : Question g is not displayed for LA - 3rd party case in Insurab info section UI and eApp forms [#1857](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1857)
* 37003, 37004 [#1845](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1845)
* 36958: eBI : Family Advantage & Family Advantage Plus_BI template issues [#1832](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1832)
* 36943: EAPP: PEP table in Declaration [#1808](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1808)

# v1.3.0 - b007
### Defects
* 36901: eBI: ART - Cover Page and PI Template Issues. Description [#1870](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1870)
* 36849: e-BI | Quotation Page for ART with UN Rider [#1869](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1869)
* 37020: eBI: ART is not available in the Products Page [#1868](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1868)
* 37033 [#1864](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1864)
* 36969 [#1863](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1863)
* 37004, 36823 [#1862](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1862)
* 37002: eApp :AWI,ART,ALT :App crashes when the user clicks on AXS SAM link [#1861](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1861)
* 36999: eApp: ESCC : PEP table showing double colon in in declaration page [#1859](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1859)
* 36919 [#1858](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1858)

# v1.3.0 - b006
### Defects
* 36974: eApp: Online Payment: tool tip is missing for IPP transactions[#1856](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1856)
* 36941[#1855](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1855)
* 36959: User face error 007 when trigger pre app email.[#1853](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1853)
* 0036885: eApp: Shield: Juvenile warning note is incorrect in eapp form [#1801](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1801)
* 36829: Client Profile - Type of Pass & Pass Expiry Date [#1791](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1791)

# v1.3.0 - b005
### Defects
* 36931: Incorrect message when no internet is connected [#1851](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1851)
* 36981: eFNA - other needs - Saving type is getting populated under Protection section. [#1849](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1849)
* 36930, 36929, 37006, 37015, 37028 [#1848](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1848)
* 0036913: eBI - ALT - BI Template [#1847](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1847)
* 36982: Fields is not refresh in FNA report [#1838](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1838)
* 36948: eApp: Online Payment: no response scenario should show only AXS_SAM available for payment  [#1836](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1836)
* 36907: eBI - MA, MA PLUS, FA, FA PLUS - no prompt message being displayed [#1829](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1829)
* 35894: PRE-EAPP - Wrong question is showing in budget section [#1826](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1826)

# v1.3.0 - b004
### Defects
* 36980: needs analysis shows as incomplete after proceeding back to needs section [#1837](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1837)
* 36962: eBI : ESCC_Cover page and PI template issues [#1839](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1839)
* 0036992: eBI - DTA Single Life - Premium value is wrong [#1840](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1840)
* 0036986: eBI - ESCC - Annualized Premium Issues [#1842](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1842)
* 36917: eBI - ALT - TDC value in the cover page not same as excel PI [#1843](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1843)
* 36993: eBI | ALT - <YTS_Low> value on the Cover page and PI is higher than excel PI. [#1844](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1844)
* 36922: eBI: ART - All 1st Party Riders' PS are missing [#1835](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1835)
* 0036862: eApp:Existing insurance portfolio is not populated for 3rd party cases after editing the FNA [#1799](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1799)
* 36971 [#1846](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1846)
* 36896: eBI: ART – Signature Page is showing 8 Mandatory Signatures at Application Stage.[#1828](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1828)
* 36825: eApp :Plan Details blank for ART [#1770](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1770)
* 36835: eApp EApp form formatting issue -ART [#1779](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1779)
* 36955: EAPP: EAPP form formatting issue for ALT [#1830](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1830)
* 36775: eBI - AWT - Template issues when more than three funds are selected Description [#1735](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1735)
* 36912: eBI - MA, MA PLUS, FA, FA PLUS - occupation class fields are being displayed [#1833](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1833)
* 36953: ALT reductant rider found [#1834](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1834)
* 37003, 37004 [#1845](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1845)
* 36907: eBI - MA, MA PLUS, FA, FA PLUS - no prompt message being displayed [#1829](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1829)

# v1.3.0 - b003

# v1.3.0 - b002

# v1.3.0 - b001
### Defects
* CK issues: 36903, 36624, 36600, 36926, 36890, 36942  [#1818](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1818)
* 36927: Incorrect numbering in priority page [#1814](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1814)
* 36682: Eapp:Subsequent payment method label must not be displayed in the premium details page for foriegn currency [#1813](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1813)
* 36839: Insurability Information - Incomplete details displayed [#1812](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1812)
* 36933: EAPP: Details of Regular Doctor in Insurability Information [#1811](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1811)
* 36909: eBI | User able to generate PI before the launch date (8 Oct 2018) for AXA Life Treasure and AXA Retire Treasure. [#1810](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1810)
* 36934: incorrect of S$ shown under policy to be replace under ROP [#1809](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1809)
* 36943: EAPP: PEP table in Declaration [#1808](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1808)
* 36873 36870 36876 36877 36871 36881 36872 36874 36886 [#1790](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1790)
* 36821: eApp :Plan Details blank for AWI CASH /SRS/CPF OA SA [#1768](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1768)


# v1.2.0 - b015
### Defects
* 36906: eBI - ALT - Riders' Product Summary are missing [#1806](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1806)
* 36914: eBI - ALT - Wrong max sum assured for adult [#1805](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1805)
* 36624: eBI : STP_ "In-progress application will be invalidated" message should not appear if there is no "in-progress" application [#1803](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1803)
* 36925: Missing spacing for submit button and thank you message [#1802](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1802)
* 36867: Drop down not showing FOR YOU when user select spouse in QQ and back to STP [#1796](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1796)
* 36878: eBI - AWT - CR163 FAIR The word "Non-editable" is not shown in the Options tab [#1795](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1795)
* 36873 36870 36876 36877 36871 36881 36872 36874 36886 [#1790](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1790)
* 0036783: Eapp:Field length is small for ROP table [#1776](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1776)
* 36837: Missing sentence in FNA report for ART under recommendation. [#1764](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1764)
* 36775: eBI - AWT - Template issues when more than three funds are selected Description [#1735](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1735)


# v1.2.0 - b014
### Defects
* payment issues: 36893, 36891, 36890   0 of 3 tasks completed [#1800](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1800)
* 36895: eBI - AWI CPF - CPF warning message is showing after re-quoting PI [#1797](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1797)
* 36837: Missing sentence in FNA report for ART under recommendation. [#1764](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1764)


# v1.2.0 - b013
### Defects
* 36878: eBI - AWT - CR163 FAIR The word "Non-editable" is not shown in the Options tab [#1795](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1795)
* 0036864: Eapp:FATCA tool tip in the declaration page [#1794](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1794)
* 36855: Client Profile for Juvenile [#1793](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1793)
* 36239, 36848, 36580 [#1792](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1792)
* 36829: Client Profile - Type of Pass & Pass Expiry Date [#1791](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1791)
* 36873 36870 36876 36877 36871 36881 36872 36874 36886 [#1790](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1790)
* 36839: Insurability Information - Incomplete details displayed [#1789](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1789)
* 36840: eApp: AWI (OA/SA): SAQ Declaration notes defect [#1788](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1788)
* 36861: Guaranteed Annual income not reflected in application page for retire treasure [#1787](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1787)
* 0036783: Eapp:Field length is small for ROP table [#1776](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1776)
* 0036817: Missing default text in reason box in budget section for both question. [#1775](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1775)
* 36822: eApp Eapp form plan details incorrect for AWI CASH/SRS [#1773](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1773)
* 36844: eApp: AWI (OA/SA): Payor details contact no field is displayed wrongly [#1772](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1772)
* 36825: eApp :Plan Details blank for ART [#1770](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1770)
* 36823: View Plan Details - Blank screen [#1769](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1769)
* 36775: eBI - AWT - Template issues when more than three funds are selected Description [#1735](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1735)

# v1.2.0 - b012

# v1.2.0 - b011
### Defects
* 36865: Warning icon shown under needs, when used had not completed all section for the first time [#1786](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1786)
* 36868: Trusted individual section is showing as undefined [#1785](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1785)
* 36869: Extra space for the text in needs section [#1784](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1784)
* 36616: Extra space for reason box.-reopen fe and cka [#1781](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1781)
* UAT 36038 [#1292](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1292)

# v1.2.0 - b010
### Defects
* 36853: Pre-eApp: AWI (OA/SA): RSP is not showing Annual/Semi-Annual/Quarterly in the pre app page [#1782](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1782)
* 36838: eApp: AWI (OA/SA): FATCA/CRS is displayed for AWI OA/SA [#1780](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1780)
* 36835: eApp EApp form formatting issue -ART [#1779](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1779)
* 36854: eApp: AWI (OA/SA): RSP details are displayed when RSP in not selected in Plan details eapp form [#1778](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1778)
* 36852: eApp: AWI (OA/SA): Plan details eapp form layout issue [#1777](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1777)
* 0036783: Eapp:Field length is small for ROP table [#1776](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1776)
* 0036817: Missing default text in reason box in budget section for both question. [#1775](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1775)
* 36822: eApp Eapp form plan details incorrect for AWI CASH/SRS [#1773](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1773)
* CR163 - FAIR Day 2 Static documents changes [#1750](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1750)
* 36506: when user scroll up, user is unable to see shortfall surplus value [#1725](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1725)
* 36514: Values not show in one row under recommendation section [#1682](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1682)
* [UAT: 36351] eBI - All products - lines not bold [#1677](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1677)

# v1.2.0 - b009
### Defects
* 36843: No pop up message is shown when user select payment method OA or SA in quotation page [#1771](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1771)
* 36825: eApp :Plan Details blank for ART [#1770](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1770)
* 36823: View Plan Details - Blank screen [#1769](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1769)
* 36821: eApp :Plan Details blank for AWI CASH /SRS/CPF OA SA [#1768](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1768)
* 0036831: eApp :Invalid date error message is not removed if the user enters and invalid date and enters a valid date again [#1766](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1766)

# v1.2.0 - b008
### Defects
* 36826, 36848, 36641, 35973, 36097 [#1767](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1767)
* CR163 - Savvy Saver [#1765](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1765)
* 0035608: eApp: Insurability Info: Life Style & Habits Qn3 defects [#1074](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1074)

# v1.2.0 - b007
### Defects
* 36837: Missing sentence in FNA report for ART under recommendation. [#1764](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1764)
* 36752: Eapp :Incorrect error message for Submitted and in progress application bundled [#1763](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1763)
* 36753: no message shown on ios post login no upline manager available [#1759](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1759)
* 36616: Extra space for reason box. [#1758](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1758)
* CR171 - 20181206 - Updated PS and GP for AWI [#1754](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1754)
* 36592: Date of Sync displayed in the landing page is cut off. [#1719](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1719)

# v1.2.0. - b019

# v1.2.0. - b018

# v1.2.0. - b017

# v1.2.0. - b016

# v1.0.4 - b001

# v1.0.3 - b009

# v1.2.0 - b006
### Defects
* UAT 36834 [#1757](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1757)
* UAT 36420 [#1756](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1756)
* Release 10 - Mum and Family Advantage, DTA Single and Joint Life (quick quote only) [#1755](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1755)
* UAT 35841 [#1738](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1738)

# v1.2.0 - b005
### Defects
* CR171 - 20181206 - Updated PS and GP for AWI [#1754](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1754)
* CR163 - FAIR Day 2 Static documents changes [#1750](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1750)
* 36813: Missing tool tip when only personal accident need is done in product type page [#1748](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1748)
* 36506: when user scroll up, user is unable to see shortfall surplus value [#1725](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1725)

# v1.2.0 - b004
### Defects
* 36514: Values not show in one row under recommendation section [#1682](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1682)
* 36816: No error message for user to review spouse client profile [#1751](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1751)
* 35179: [GIT:787] eFNA - Missing and incorrect arrangement of tab in PDPA. [#1742](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1742)
* CR163 - FAIR Day 2 Static documents changes [#1750](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1750)
* 36807: CKA is not reset after data sync. [#1747](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1747)
* 36813: Missing tool tip when only personal accident need is done in product type page [#1748](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1748)
* 35289, 36388, 36580 [#1744](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1744)

# v1.2.0 - b003
### Defects
* 36794: EAPP | Border lines being cut off [#1740](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1740)
* UAT 36567 [#1739](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1739)
* UAT 35841 [#1738](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1738)

# v1.2.0 - b002
### Defects
* 36803: Incorrect header for Critical illness need for myself and spouse [#1737](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1737)
* enable all the 8b and 10b products [#1736](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1736)
* 36775: eBI - AWT - Template issues when more than three funds are selected Description [#1735](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1735)
* 36670: eBI: ESP- It showing undefined_YR in UI Screen for 1st Party and 3rd Party Rider. [#1734](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1734)
* 36686: eBI - Pulsar - Template issues [#1733](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1733)
* 36773: eBI - Quick Quote History - display of Life Assureds name [#1731](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1731)

# v1.2.0 - b001
### Defects
* 36778: Incorrect Calcuation When you enter 10 digits of value which require x12 [#1727](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1727)
* 36501: User is unable to see selection for the last LA [#1730](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1730)
* UAT: 35604: eApp: Term: Insurance history qn2 validations are incorrect [#1396](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1396)
* 36506: when user scroll up, user is unable to see shortfall surplus value [#1725](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1725)
* 0035608: eApp: Insurability Info: Life Style & Habits Qn3 defects [#1074](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1074)
* 36777: User is allow to enter “–“ for value of assets field [#1726](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1726)
* 36517: eApp:Eapp form:hyphen in the blank field in personal details section [#1720](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1720)
* 36680: Eapp:Comma missing for monthly allowance [#1717](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1717)
* UAT 35792 [#971](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/971)
* 36144: eApp: Shield: Payment Page: GIRO and warning messages display error [#1699](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1699)
* 36586: Supporting Documents – No page break for (E-App, e-FNA and e-PI) system document [#1698](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1698)
* 36097: AWT :Supporting document not able to see e-FNA under Policy Documents [#1695](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1695)
* 36641: after user had click on the select button on application stage, it is not reset when user proceed back and forth [#1692](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1692)
* 36567, 36603 [#1689](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1689)
* 36224: EFNA LA not in sequence [#1683](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1683)
* [UAT:35247] iOS | Release 2 | Needs Analysis | [Medium] Incorrect status when completed or incomplete all section[749](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/749)

# v1.0.2 - b001
Note: fix time bomb issue

# v1.0.1 - b088
### Defects
* 36793: User enter text under less than given budget, but is reflecting more than your given budget [#1729](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1729)

# v1.0.1 - b087
### Defects
* [UAT:36543] eApp: All Life products: eapp form formatting issues [#1576](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1576)

# v1.0.1 - b086
### Defects
* 36282: [Required]eApp:Shield:Eapp form shows loading for long time if the user goes back and edit the form [#1724](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1724)
* [UAT:36543] eApp: All Life products: eapp form formatting issues [#1576](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1576)
* 36774: When user proceed back to need after viewing FNA report. Needs Analysis is shown as incomplete [#1723](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1723)
* 36789: eBI - CR164 - revised PHS and FIB for 3 funds are not updated [#1722](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1722)

# v1.0.1 - b084
### Defects
* 0036019: eApp: All Life Products: reflexive table formatting issue. [#1285](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1285)
* 0036015: eApp: Shield: reflexive table formatting issue. [#1287](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1287)
* [UAT:35199] | iOS | Release 2 | Products | LMP | <Medium> Issues on LMP's Quotation Page [#777](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/777)

# v1.0.1 - b083
### Defects
* 36438: [Required]eBI - LMP - error message appears when opening from email and a small dot on the Product Summary [#1715](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1715)
* 36776: eBI - AWT - Issues with the Quotation Page [#1718](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1718)

# v1.0.1 - b082
### Defects
* 36273: [DVT] Mutiple logins with same id is allowed in different devices. [#1703](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1703)
* 36771: value shown in priority page is incorrect when user enter millions or billions values [#1716](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1716)
* 36712: Incorrect Annual replacement income in UI for income protection and Critical illness [#1693](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1693)

# v1.0.1 - b081
### Defects
* 36761: [Required]Client’s Accompaniment section is enable even when user is not a selected client [#1714](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1714)
* 36273: [DVT] Mutiple logins with same id is allowed in different devices. [#1703](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1703)
* 36753: no message shown on ios post login no upline manager available [#1713](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1713)

# v1.0.1 - b080
Note: build again to test

# v1.0.1 - b079
### Defects
* 36723: eBI - CR169 - White screen is showing after requoting on EASE app [#1705](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1705)
* CR164 To unmask funds [#1678](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1678)
* 36748 [#1711](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1711)
* 36739: eBI - incorrect error message when keying in wrong credentials upon logging in app [#1710](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1710)
* 36747: Since there is no achievement shown in EASE admin the achievement header should not show [#1709](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1709)
* 36712: Incorrect Annual replacement income in UI for income protection and Critical illness [#1693](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1693)
* 36734: eBI - Able to Reset the same App Password [#1708](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1708)
* UAT 35792 [#971](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/971)

# v1.0.1 - b078
### Defects
* 36742: Unable to see the 4th LA name when user input max characters for each LA name [#1707](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1707)
* [UAT: 36709] eBI:Timezone (Singapore)- Incorrect Age(Generation Date) once the user change their date and time at Quotation Page. [#1706](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1706)
* 36740: Eapp:Regression Keyword true is coming along with the case name [#1704](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1704)
* Change in landing page of IOS app [#781](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/781)
* CR164 To unmask funds [#1678](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1678)
* [UAT: 36711] eBI : Pulsar_the paragraph in PI is very close to the Adviser and Proposer signatures [#1702](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1702)
* 36712: Incorrect Annual replacement income in UI for income protection and Critical illness [#1693](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1693)
* [UAT:36543] eApp: All Life products: eapp form formatting issues [#1576](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1576)

# v1.0.1 - b077
### Defects
* 36728: eApp: submitted case(AWI/ART/ALT) in web is not accesible and not shown in FNA history [#1701](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1701)
* 36725: eBI:DATA Sync – (Block Solution) No error message appearing for 3rd Party Client Profile (AWI Cash SRS). [#1700](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1700)

# v1.0.1 - b076
### Defects
* 36694: EApp:Regression :LA tab is shown as complete even if mandatory fields are not entered-random [#1687](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1687)
* CR164 To unmask funds [#1678](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1678)
* 36716: eApp: Shield: Personal details page getting ticked even if mandatory fields are not given [#1697](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1697)
* Incorrect hyperlink when user click on the link from supervisor email [#1686](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1686)
* SP in quotation num instead of SA

# v1.0.1 - b075
### Defects
* 36705:Auto data sync is not triggered on online logout [#1696](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1696)
* 36712: Incorrect Annual replacement income in UI for income protection and Critical illness [#1693](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1693)
* CR Update SAV & RHP [#1694](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1694)
* [UAT:35307] | iOS | Release 2 | Products | <Medium> Alignment Issue in UI Screen for ALL Products  [#717](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/717)

# v1.0.1 - b074
### Defects
* 36669: Email not working [#1691](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1691)
* 0036487: Timebomb [#1641](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1641)
* 36674: eBI: EASE IOS – Pre-Prod Environment Issues. [#1690](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1690)

# v1.0.1 - b073
### Defects
* 36694: EApp:Regression :LA tab is shown as complete even if mandatory fields are not entered-random [#1687](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1687)
* 36679: Since FNA is incomplete, user should not be able to proceed into eapp when user click on Continue. [#1688](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1688)
* CR164 To unmask funds [#1678](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1678)
* 36688: Incorrect hyperlink when user click on the link from supervisor email [#1686](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1686)

# v1.0.1 - b072
### Defects
* 36677: Text being Cut off in Trusted Individual section [#1685](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1685)
* 36683: FNA become comeplete when user preceed back to needs [#1684](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1684)
* UAT 36671 [#1680](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1680)

# v1.0.1 - b071
### Defects
* 36514: Values not show in one row under recommendation section [#1682](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1682)
* 36628: Personal Data Acknowledgement - Create Family Member [#1681](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1681)

### Features
* CR160 Revised Product Summary [#1679](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1679)
* CR164 To unmask funds [#1678](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1678)

# v1.0.1 - b070
### Defects
* UAT 36671 [#1680](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1680)
* 36572: eBI : All Products_the header is missing for all the prompt messages [#1674](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1674)

# v1.0.1 - b069
### Defects
* issues: 35223, 36602, 36593 [#1673](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1673)
* 36627: user is unable to see the reason box when user is entering reason in budget section [#1649](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1649)
* ck issues: 36599, 36581, 36568 [#1658](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1658)
* [UAT:35171] | iOS | Release 1 | Quick Quote | <low> Band Aid - Policy Term for Basic Plan & Riders are not displaying in straight line [#612](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/612)
* [UAT:35307] | iOS | Release 2 | Products | <Medium> Alignment Issue in UI Screen for ALL Products [#717](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/717)
* [UAT:36574] eBI : FPFS_PI report issues [#1675](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1675)
* [UAT:36543] eApp: All Life products: eapp form formatting issues [#1576](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1576)
* 36439: eBI - LMP - Drop down for the Policy Term is too close to the Premium Payment Term on the UI [#1676](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1676)
* UAT 36597 [#1646](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1646)
* 36616: Extra space for reason box. [#1672](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1672)
* 36572: eBI : All Products_the header is missing for all the prompt messages [#1674](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1674)

### Features
* 36649: ART | Quotation Page | Rider Premium Waiver (CIUN) is missing for 1st party quotation [#1664](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1664)
* 36647: CR154 | ART | Application Summary | The header "Sum Assured/Benefit" is incorrect [#1665](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1665)

# v1.0.1 - b068
### Defects
* issues: 35223, 36602, 36593 [#1673](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1673)
* 35745 PRE-EAPP - Proposal number got cut off [#1671](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1671)
* 35672: eApp: Personal Details pages issues [#1670](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1670)

# v1.0.1 - b067
### Defects
* 36144: eApp: Shield: Payment Page: GIRO and warning messages display error [#1669](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1669)
* 35674: Eapp:All products :Payment page :Telegraphic transfer mode [#1605](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1605)
* 36527 [#1668](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1668)
* 36631: eBI - Data Sync - ALT - Blank screen is shown when proposal is created on SIT Description [#1663](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1663)
* 35927 [#1667](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1667)
* 36615: Incorrect alignment of text in recommendation page. [#1666](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1666)
* 36478: Eapp:Font color is different for alchohol sub question [#1505](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1505)
* 0035607: eApp: Insurability Info: Alcohol Qn defects [#1073](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1073)
* UAT 35769 [#1628](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1628)

# v1.0.1 - b066
### Defects
* [UAT:36543] eApp: All Life products: eapp form formatting issues [#1576](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1576)
* [UAT:36549] [Need more time] FNA : In FNA report My Budget ,Trusted Individual showing in red border [#1661](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1661)
* 36617: eBI | Quick Quote History | Email Function unavailable and Download function not working [#1659](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1659)
* 0035564: eApp: Declaration notes display error for payor details [#1290](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1290)
* 36625: [DVT] When user enter max character for given name and surname. The name is not fitting in the box [#1642](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1642)
* UAT 35642 [#1635](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1635)
* 36640: Product summary missing in pre eapp email for FS and FP [#1660](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1660)
* 36588: FNA : ROP not getting refresh after FNA values are reset to new values after case submission for exisitng insurance policies. [#1647](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1647)
* ck issues: 36599, 36581, 36568 [#1658](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1658)

# v1.0.1 - b065
### Defects
* UAT 36636 [#1657](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1657)
* 36622: [DVT] Text overlap when user enter max characters for reason box in financial evaluation (FNA report) [#1656](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1656)
* 35927, 36584 [#1655](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1655)
* 0035607: eApp: Insurability Info: Alcohol Qn defects [#1073](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1073)
* disable r8b products. [#1650](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1650)
* 36478: Eapp:Font color is different for alchohol sub question [#1505](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1505)
* 35552: Eapp:Edit Existing Insurance Portfolio at FNA button [#1653](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1653)

# v1.0.1 - b064
### Defects
* 36612: [DVT] user is allow to login to the app without entering password [#1652](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1652)

# v1.0.1 - b063
### Defects
* 35944: [Suggest DVT] Eapp:Invalidation Paid transferred functionality is not functioning  [#1651](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1651)
* [UAT:36543] eApp: All Life products: eapp form formatting issues [#1576](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1576)
* 36605: eBI |ILP |’Risk Rating’ dropdown in available fund list should be ‘All Funds’ by default [#1648](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1648)
* UAT: 0035561: Eapp:Fields missing in the reflexive table of FATCA [#874](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/874)
* 36627: user is unable to see the reason box when user is entering reason in budget section [#1649](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1649)
* disable r8b products. [#1650](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1650)
* 36584 [#1639](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1639)

# v1.0.1 - b062
### Defects
* UAT 36597 [#1646](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1646)
* 36588: FNA : ROP not getting refresh after FNA values are reset to new values after case submission for exisitng insurance policies.[#1647](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1647)
* 36584 [#1639](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1639)
* 36629: eApp:Residential and mailing address -address finder issue-All Products [#1645](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1645)

# v1.0.1 - b061
### Defects
* 36625: [DVT] When user enter max character for given name and surname. The name is not fitting in the box [#1642](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1642)
* 36615: Incorrect alignment of text in recommendation page. [#1644](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1644)
* 36623: more than 10 digits is showing in FNA report [#1637](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1637)
* 36135: Pre App is not shown as pending supervisor approval/approved/expired for submitted case [#1643](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1643)
* 36529, 36431 [#1640](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1640)

# v1.0.1 - b060
### Defects
* 35612: eApp: Insurability Info: Life Style & Habits Qn4 defects [#1391](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1391)
* UAT 35642 [#1635](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1635)
* UAT 36389 [#1594](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1594)
* 35674 shield case [#1630](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1630)
* 36608: eBI | Inspire Flexi | Living Accelerator Benefit is incorrectly rounding to nearest 100 [#1634](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1634)
* 36607: eBI - Inspire FlexiProtector - The tool tips for RSP Amount & Regular Withdrawal have extra dollar sign ($$) Description [#1633](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1633)
* 36606: eBI | LMP | Critical Illness Plus is missing 25 years in the dropdown for age 60 [#1632](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1632)
* 35926: App crashes at need prioritization page randomly [#995](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/995)
* 36610: eApp: All products including shield: signatures are cut or signature is not seen [#1638](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1638)
* 35924: FNA - json file not match [#1636](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1636)

# v1.0.1 - b059
Note: Build 58 was crash, so build it again

# v1.0.1 - b058
### Defects
* 36619: eBI | Quick Quote History | 'BI Number' Field name should be 'PI Number' [#1631](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1631)
* 36583: Unable to see view detail for non shield and incorrect spelling [#1604](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1604)
* UAT: 0035643: eApp: Term: Juvenile qn b defect [#888](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/888)

# v1.0.1 - b057
### Defects
* 36584 eApp: user should not be able to submit without uploading offline payment doc for Director login [#1615](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1615)
* UAT 35769 [#1628](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1628)
* UAT 35569: eApp: : signature section docs not generated properly for 1st and 3rd party [#1332](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1332)
* 36548: My Finances (net worth) [#1627](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1627)

# v1.0.1 - b056
### Defects
* 36601: eBI - LMP - Product Summary for Critical Illness Plus is not the latest [#1623](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1623)
* 36497 [#1625](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1625)
* 35674 [#1626](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1626)
* 36558, 36604 [#1621](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1621)
* 36368: eBI - Quick Quote History - No NRIC field is available [#1624](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1624)
* 36585: Pre-Eapp – UI Screen Issue after Case Submission. [#1622](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1622)
* UAT 35946 [#1383](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1383)

# v1.0.1 - b055
### Defects
* 35674: Eapp:All products :Payment page :Telegraphic transfer mode [#1605](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1605)

# v1.0.1 - b054
### Defects
* UAT 36595 [#1618](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1618)
* UAT 35946 [#1383](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1383)
* 36432: FNA-Upon removing the spouse, client generation on Needs Analysis crashes app [#1603](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1603)
* 35674, 36584 [#1615](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1615)
* 36594: Extra property fill under assets. [#1614](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1614)
* UAT: 36193: eBI - All Products - Release Version Number should follow as per Web [#1176](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1176)
* 36589: eBI : Shield_STP case_There is overlapping of wordings in PI Signature page 1 [#1613](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1613)
* [UAT:36578] eBI - LMP - Issues with Maturity Value table [#1611](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1611)

# v1.0.1 - b053
### Defects
* 36542: eApp: All products: Details of regular doctor qn defect [#1575](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1575)
* 36432: FNA-Upon removing the spouse, client generation on Needs Analysis crashes app [#1425](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1425)
* 36548: My Finances (net worth) [#1571](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1571)
* [UAT: 36545] eApp: All Life products: dollar sign is not displayed for payor's annual income in eapp form [#1563](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1563)
* 0035561: Eapp:Fields missing in the reflexive table of FATCA [#1063](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1063)
* 36566: [Earlier] Incorrect picture is shown in needs page and missing picture in Priority page [#1608](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1608)
* 36279, 36552 [#1607](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1607)
* 36562: eFNA - App crashed after spouse was added. Post crash, client not available and spouse is available. [#1612](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1612)
* 36529: eApp: All products: premium details page is not attached to eapp form after submission [#1610](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1610)
* 35844: Cannot submit the Shield case from mobile API, RC 504 [#1609](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1609)
* 36583: Unable to see view detail for non shield and incorrect spelling [#1604](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1604)
* UAT 35569: eApp: : signature section docs not generated properly for 1st and 3rd party [#1332](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1332)
* 36497: eApp: Term: Female UW qn c header is changed from previous version [#1539](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1539)
* 36432: FNA-Upon removing the spouse, client generation on Needs Analysis crashes app [#1603](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1603)

# v1.0.1 - b052
### Defects
* 36575: eApp: Email in client pop up not redirecting to Application, [#1606](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1606)
* 35674: Eapp:All products :Payment page :Telegraphic transfer mode [#1605](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1605)
* 0035564: eApp: Declaration notes display error for payor details [#1290](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1290)
* [UAT: 36488] eApp: Shield: Signature box alignment issue in Eapp form for Proposer [#1601](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1601)
* UAT: 0035643: eApp: Term: Juvenile qn b defect [#888](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/888)
* UAT: 0035641: eApp: Term: Juvenile qn a defect [#889](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/889)
* 36430: Extra text “years” under time horizon in IOS FNA report [#1478](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1478)
* 36558, 35760, 36573 [#1599](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1599)
* 36565: eBI_FPFS_In options, From Age & To Age alphabets and symbols are able to enter [#1602](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1602)
* 35674: Eapp:All products :Payment page :Telegraphic transfer mode [#1600](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1600)
* 35955: eAPP: Residency & Insuance info - ROP not the same as web [#1589](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1589)

# v1.0.1 - b051
### Defects
* 36550: eFNA | ‘FNA’ already completed but in ‘Products’ it says 'FNA incomplete' [#1598](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1598)
* [UAT: 36570] eBI - LMP - Multiplier Benefit sentence should shift up if there is empty space [#1596](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1596)
* [UAT: 36545] eApp: All Life products: dollar sign is not displayed for payor's annual income in eapp form [#1563](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1563)
* [UAT:36543] eApp: All Life products: eapp form formatting issues [#1576](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1576)
* 36571: When no budget is given or used it should show as Align [#1595](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1595)
* UAT: 36525: Bundling Scenario | Missing Application under Applying Tab [#1597](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1597)
* Error message for submission of Bundling cases [#1525](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1525)
* UAT 36389 [#1594](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1594)
* 36530: Extra validation message for retirement and income protection [#1561](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1561)

# v1.0.1 - b050
### Defects
* 36472: PDPA and Needs anaysis become incomplete after user proceed back to needs section [#1592](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1592)
* 36548: My Finances (net worth) [#1571](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1571)
* UAT 36511 [#1550](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1550)
* 36553: eBI: All Products PoIicy Illustration is showing different Date of Birth and Age (Generation Date) after changing the Time Zone. [#1591](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1591)
* issues: 36557, 36561 [#1590](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1590)
* UAT: 0035556: Eapp:PEP table issues in the declaration page [#873](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/873)

# v1.0.1 - b049
### Defects
* 36419: eApp: Life: Payor details defects [#1574](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1574)
* 36551: eFNA - Age issue [#1580](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1580)
* 36483, 36556 [#1582](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1582)
* 35564 [#1588](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1588)
* UAT 35769 [#987](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/987)
* UAT 35646 [#1300](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1300)
* UAT 36544 [#1587](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1587)
* UAT 36422 [#1583](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1583)
* UAT 36541 [#1585](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1585)
* UAT 36516 [#1586](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1586)
* 36430: Extra text “years” under time horizon in IOS FNA report [#1478](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1478)

# v1.0.1 - b048
### Defects
* 36430: Extra text “years” under time horizon in IOS FNA report [#1478](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1478)
* 36554: eBI | Proposal |Email |when connected to internet ,’ Please Connect to internet’ msg appears. [#1578](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1578)
* UAT 36270: EApp:All products :No signature expiry warning and expiry of cases [#1334](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1334)
* 35777 [#1577](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1577)
* UAT 35946 [#1383](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1383)
* UAT 35626 [#1529](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1529)
* UAT 35561 [#972](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/972)
* 36419: eApp: Life: Payor details defects [#1574](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1574)
* 36552: eApp:Shield :Done button gets disabled when the user has reached the max file size [#1573](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1573)
* 36279 [#1570](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1570)
* 35776: eBI : Shield_"Total Premium Rider" is missing in Quotation page [#1572](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1572)
* 0035992: eApp:Shield Height and weight field validation is wrong [#1097](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1097)
* 35595 [#1569](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1569)
* 36547: eFNA - wrong spelling of Actuarial in CKA in fna report [#1565](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1565)
* [UAT:35222] iOS | Release 2 | Need Analysis | [Medium] Income Protection - User is able to enter more than the value given under assets; "Add button" is incorrect and missing text [#766](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/766)
* 36548: My Finances (net worth) [#1571](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1571)
* 36497: eApp: Term: Female UW qn c header is changed from previous version [#1539](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1539)
* UAT 36535: Eapp:Policy to be replaced are not marked as mandatory in recommendation [#1568](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1568)

# v1.0.1 - b047
Note: Because b46 is crash, build the version again

# v1.0.1 - b046
### Defects
* UAT 36511 [#1550](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1550)
* issues: 36361, 36519, 36403 [#1562](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1562)
* 36537: Regular With drawals should not be shown in E-App form when not selected in BI [#1567](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1567)
* 36135: Pre App is not shown as pending supervisor approval/approved/expired for submitted case [#1566](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1566)
* [UAT:35208, 35209, 35211] IOS | Release 2 | Needs analysis | <High> Errors in task#76 Retirement [#626](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/626)
* 36530: Extra validation message for retirement and income protection [#1561](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1561)
* 35229: [GIT:762] eFNA - Needs for spouse is not shown under view summary. [#1353](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1353)
* [UAT: 36545] eApp: All Life products: dollar sign is not displayed for payor's annual income in eapp form [#1563](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1563)
* [UAT:36528] eBI: CR154_Update fund name is not available under v1.0.1-b044 [#1564](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1564)
* [UAT: 36521] eBI - LMP - generated PDF alignment issues [#1555](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1555)
* 36176: PRE-EAPP - Missing tool tip for shield products in recommendation page [#1554](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1554)
* 36532: CR154 - Changes to FNA report- ( Time horizon renaming) not available [#1556](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1556)
* 0035564: eApp: Declaration notes display error for payor details [#1290](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1290)
* 36333: eApp : Trusted Individual details showing as undefined when CP edited [#1557](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1557)
* 36526, 35760 [#1558](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1558)
* 35777 [#1559](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1559)
* 35674 [#1560](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1560)
* [UAT:35307] | iOS | Release 2 | Products | <Medium> Alignment Issue in UI Screen for ALL Products [#717](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/717)
* [UAT:35171] | iOS | Release 1 | Quick Quote | <low> Band Aid - Policy Term for Basic Plan & Riders are not displaying in straight line [#612](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/612)
* issues: 36137, 36066, 36122, 35625, 35068, 36352, 36362, 36254, 35840, 35022 [#1322](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1322)
* 0036049: Eapp:Tab selection incorrect for 3rd party [#1155](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1155)

# v1.0.1 - b045
### Defects
* 36528: eBI: CR154_Update fund name is not available under v1.0.1-b044 [#1553](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1553)
* UAT 36419 [#1463](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1463)
* 0036135: Pre App is not shown as pending supervisor approval/approved/expired for submitted case [#1492](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1492)

# v1.0.1 - b044
### Defects
* UAT 36419 [#1463](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1463)
* 35046: eFNA - No Tool tip shown for Years income required, Total liabilities and values of assets (Income Protection) [#703](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/703)
* 0035598: Eapp: When Payor's details is others all the field validations are wrong [#1215](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1215)
* 0035596: Eapp: When Payor's details is others some fields are missing for all products [#1132](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1132)
* 35612: eApp: Insurability Info: Life Style & Habits Qn4 defects [#1391](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1391)
* 35208: Needs Analysis for Retirement page age not able to key in ,no value for years of retirement [#1195](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1195)
* UAT: 36387 [Low] [#1536](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1536)
* 0036049: Eapp:Tab selection incorrect for 3rd party [#1155](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1155)
* [UAT: 36508] Eapp : Application Form sign is not center aligned [#1549](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1549)
* 0036060: eApp: Shield: Female UW qn a defect [#1109](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1109)

# v1.0.1 - b043
### Defects
* 36515: Name of the needs is missing in FNA report under recommendation [#1552](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1552)
* 35612: eApp: Insurability Info: Life Style & Habits Qn4 defects [#1391](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1391)
* 36253: When user delete value under existing insurance and insurance premium the fill shows enter amount (regression) [#1244](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1244)
* issues: 36418, 36345, 36333, 36238 [#1428](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1428)
* 0035636: eApp: Term: Lifestyle n habits qn5 defect [#1075](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1075)
* UAT: 0035702: eApp: Term: Juvenile qn d mandatory validation defect [#885](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/885)
* UAT 35946 [#1383](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1383)
* UAT 35626 [#1529](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1529)
* UAT 36511 [#1550](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1550)
* 36368: eBI - Quick Quote History - No NRIC field is available [#1548](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1548)
* issues: 36510, 35672 [#1551](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1551)
* [UAT: 36486] WEB Production defect 34768 for RHP_Sum Assured_benefit in IOS [#1542](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1542)
* UAT 36458: eBI - LMP - Values are wrong in Table of Deductions and Bundle Template Description [#1534](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1534)
* UAT 36176: PRE-EAPP - Missing tool tip for shield products in recommendation page [#1414](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1414)
* 0035894: PRE-EAPP - Wrong question is showing in budget section [#1329](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1329)
* UAT 36504: Unable to see “view detail” button at recommendation screen [#1537](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1537)
* [UAT: 35888] eBI - LMP - PI Template issues [#1219](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1219)
* [UAT:36440] eBI - AWT - Template and UI issues [#1541](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1541)
* UAT 35569: eApp: : signature section docs not generated properly for 1st and 3rd party [#1332](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1332)
* 36373: No value of assets fills is shown for 5 needs [#1547](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1547)
* 36497: eApp: Term: Female UW qn c header is changed from previous version [#1539](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1539)
* 35592 [#1543](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1543)
* 36498, 36467 [#1545](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1545)
* 36245: No tool tip shown for Age when funds are needed (education needs) [#1540](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1540)
* 36252: eApp : Data Sync create a CP in web with 1 BI and data sync ,in App FNA getting reset [#1544](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1544)
* 36478: Eapp:Font color is different for alchohol sub question [#1505](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1505)
* 0035608: eApp: Insurability Info: Life Style & Habits Qn3 defects [#1074](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1074)
* 36500: after deleting values under value of assets, user is allow to proceed [#1538](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1538)
* UAT: 0035556: Eapp:PEP table issues in the declaration page [#873](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/873)

# v1.0.1 - b042
### Defects
* 36491: Customer Profile | Trusted Individual : Crashed when revisit Client's Accompaniment [#1535](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1535)
* 0036050: eApp: Insurability Info -Regular doctor question-3rd party [#1275](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1275)
* 36279, 35760 [#1533](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1533)
* 35046: eFNA - No Tool tip shown for Years income required, Total liabilities and values of assets (Income Protection) [#703](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/703)
* 36421: EFNA - DATA sync - Product type is not synced from IOS, also deselects CKA and RPA automatically [#1532](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1532)
* 36247: When user delete spouse profile, myself options is not selected [#1333](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1333)
* UAT 35796 [#1296](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1296)
* 0035607: eApp: Insurability Info: Alcohol Qn defects [#1073](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1073)

### Features
*  Sprint 4 | CR151 | Updated LMP Lite requirements [#1172](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1172)

# v1.0.1 - b041
### Defects
* [UAT:35913] eBI - LMP - Issues with LMP template [#1035](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1035)
* 35229: [GIT:762] eFNA - Needs for spouse is not shown under view summary. [#1353](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1353)
* 36395: Under Income Protection [#1455](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1455)
* 36489: after user added a profile under selected client section, no information is populating (regression) [#1530](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1530)
* 36159: eFNA- Needs Analysis - planning for specific goals need Description [#1520](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1520)
* 35740: eFNA-Value of Assets-It showing all the options on the dropdown list even the user input only for Savings Account at Financial [#1528](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1528)
* UAT 35626 [#1529](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1529)
* 36156: eApp: Shield: Payment Page: GIRO check box issue [#1527](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1527)
* Status showing "Pending Supervisor Approval" when submission failed [#1526](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1526)
* UAT 35793 [#1380](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1380)
* UAT 36038 [#1292](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1292)
* UAT 35792 [#971](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/971)
* UAT 35946 [#1383](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1383)
* Error message for submission of Bundling cases [#1525](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1525)

# v1.0.1 - b040
### Defects
* 36472: PDPA and Needs anaysis become incomplete after user proceed back to needs section [#1522](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1522)
* 0035573: eApp: Payment : Telegraphic Transfer scrolling and date issue [#1521](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1521)
* 36159: eFNA- Needs Analysis - planning for specific goals need Description [#1520](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1520)
* 35999: [Framework Issue] eApp: App crashes in insurability info page for height n weight fields for Life products [#1506](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1506)
* UAT 35592 [#858](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/858)

# v1.0.1 - b039
### Defects
* 36184: eApp:All products :Premium details missing after submission [#1519](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1519)
* 36414: user is allow to click on continue even though FNA is incomplete [#1518](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1518)
* 0035661: eApp:FS:Eapp form:Plan details-Fund allocation for TopUp Premium is not displayed [#1138](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1138)
* 35720: eApp:FS /FP:RSP details are shown in eapp form even if not selected in BI [#921](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/921)
* 35727: eFNA - Tool tip will show at the bottom for shortfall or surplus (Income Protection) [#1517](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1517)
* 36395: Under Income Protection [#1455](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1455)
* 36023  [#1513](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1513)
* UAT 35569: eApp: : signature section docs not generated properly for 1st and 3rd party [#1332](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1332)
* 36297, 35760, 36293, 36483 [#1510](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1510)
* 0035720: eApp:FS /FP:RSP details are shown in eapp form even if not selected in BI [#1516](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1516)
* 0035636: eApp: Term: Lifestyle n habits qn5 defect [#1075](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1075)
* 0036232: eApp: edit function for place holder is not working and validations are wrong [#1515](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1515)
* 36065: PRE-EAPP - Incorrect naming for chosen proposal(s) and Not chosen proposal(s) [#1514](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1514)
* 36144: eApp: Shield: Payment Page: GIRO and warning messages display error [#1511](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1511)
* 35936: Eapp:Invalidation :Risk profile assessment is shown incomplete when eapp impact field is updated for partially signed cases [#1512](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1512)

# v1.0.1 - b038
### Defects
* 36481: ROI auto input 65[#1507](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1507)
* 36284: after doing data sync, agent profile email is not updated [#1509](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1509)
* 35662: eApp:All products FS:Signature _EApp form:Date and time is missing [#1503](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1503)
* 35573: eApp: Payment : Telegraphic Transfer scrolling and date issue [#949](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/949)
* UAT 36473: IOS - All Products to be avialble in Web UAT are available in IOS [#1508](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1508)
* 36247: When user delete spouse profile, myself options is not selected [#1333](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1333)
* UAT 35796 [#1296](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1296)
* UAT 35975 [#1381](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1381)
* UAT 35849 [#1188](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1188)
* eBi issues: 35806, 35905, 36077 [#1152](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1152)
* UAT 35968 [#1324](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1324)
* 0036028: eApp: Shield : Sex reassignment table format is wrong [#1289](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1289)
* issues: 36418, 36345, 36333, 36238  [#1428](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1428)
* 36168: Incorrect wording for OVER-BUDGET in budget section [#1476](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1476)
* 0036012: eApp: Shield : Smoking qn wordings change and field validations issue [#1098](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1098)
* 35999: [Framework Issue] eApp: App crashes in insurability info page for height n weight fields for Life products [#1506](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1506)
* Follow-up from "supportind document + signature + payment + submission"  [#793](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/793)
* iOS | Release 2 | Products | <low> Image of Shield is not showing in Product page (QQ and STP) [#821](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/821)
* iOS | Release 2 | Products | [Low] Shield - Unknown blue box in the heading of Proposal UI [#870](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/870)
* 0036379: eApp: AWT in after Signing the case ,Not able to find the case in Application Summary [#1453](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1453)
* UAT: 0035702: eApp: Term: Juvenile qn d mandatory validation defect [#885](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/885)
* UAT: 0035705: eApp: Term: Female UW qn b defect [#882](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/882)
* 35552: Eapp:Edit Existing Insurance Portfolio at FNA button [#1504](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1504)
* 0035672: eApp: Personal Details pages issues [#1459](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1459)

# v1.0.1 - b037
### Defects
* UAT 36130: eAPP - unable to deselect documents in pre app email [#1501](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1501)
* 0035989: Band Aid Insurability info table for 2nd qn (Insurance History) is wrong and field validations missing [#1243](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1243)
* issues: 35715, 36410, 36119, 36051, 35602, 36276, 36336, 36341, 36366, 36361 [#1448](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1448)
* UAT 35560 [#1036](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1036)
* 0035019: [GIT:647] eFNA - For other under cash flow, the others value fill is not showing [#1364](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1364)
* 36279, 36023  [#1231](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1231)
* 36463: when user enter value continuously the calculation under assets and liabilities is incorrect  [#1498](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1498)
* 0036025: eApp: Shield : Medical and Health info table format is wrong [#1267](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1267)
* 0036042: eApp: : Medical n Health Information qn (1- 4) defect for 3rd party cases [#1038](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1038)
* 0036250: eApp: Shield: Personal Details shown as incomplete  [#1502](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1502)
* UAT 35789 [#965](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/965)
* 0036279: eApp:Shield:Supporting docs file size [#1500](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1500)
* 35865: After data sync, no LA name is shown in Priority page and in FNA report.[#1499](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1499)
* [UAT:36266] eBI - LMP - Integrated case does not have signature block at the last page of generated PI [#1493](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1493)
* 35904: eBI - LMP - Issues with the Page Numbers [#999](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/999)
* 0035608: eApp: Insurability Info: Life Style & Habits Qn3 defects [#1074](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1074)
* UAT 35857 [#1189](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1189)
* UAT 35790 [#1191](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1191)
* UAT 36281 [#1464](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1464)
* UAT 36419 [#1463](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1463)
* UAT 36443 [#1497](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1497)
* UAT: 0035556: Eapp:PEP table issues in the declaration page [#873](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/873)
* UAT: 0035706: eApp: Term: Female UW qn c defect [#883](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/883)
* 36085: Email not received after cases submitted [#1495](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1495)
* 0036469: eBI - Some of the products are missing on the Products page [#1494](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1494)

# v1.0.1 - b036
### Defects
* 36460: FNA - CKA Criteria - Actuarial Science is spelt wrongly on the UI [#1488](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1488)
* 36470: eBI - Error message is showing for Spouse in Profile page [#1489](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1489)
* [UAT:36266] eBI - LMP - Integrated case does not have signature block at the last page of generated PI [#1493](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1493)
* 35612: eApp: Insurability Info: Life Style & Habits Qn4 defects [#1391](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1391)
* UAT: 0035641: eApp: Term: Juvenile qn a defect [#889](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/889)
* 0036469: eBI - Some of the products are missing on the Products page [#1482](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1482)
* 0036135: Pre App is not shown as pending supervisor approval/approved/expired for submitted case [#1492](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1492)
* 0035893: EAPP - Unable to see the name of the different tabs (shield case) [#1491](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1491)
* 0036086: Eapp:Personal details:type of pass and pass expiry date missing for 1st party and 3rd party cases [#1447](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1447)
* 0036467: eApp::Social Visit pass/others tool tip alignment issue [#1490](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1490)
* 0035551: eApp: Mandatory asterik is not displayed for Mandatory fields in Personal Details Sections [#1457](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1457)
* 35859: Incorrect behavior for data sync [#1487](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1487)
* [UAT:36180] PRE-EAPP - Character count is incorrect for shield [#1411](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1411)
* [UAT:35957] eBI - LMP - Values in the Bundle Template are having a $1 difference [#1040](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1040)
* 5259: [GIT:738] PRE-EAPP - Recommendation : Budget vs Premium arrow displayed and wordings for budget question is wrong [#1477](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1477)
* 0035661: eApp:FS:Eapp form:Plan details-Fund allocation for TopUp Premium is not displayed [#1138](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1138)
* 36253: When user delete value under existing insurance and insurance premium the fill shows enter amount (regression) [#1244](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1244)
* 35605: eFNA - Unable to proceed to products even after completing the needs analysis for Retirement Planning [#1484](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1484)
* 36245: No tool tip shown for Age when funds are needed (education needs) [#1479](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1479)
* 36430: Extra text “years” under time horizon in IOS FNA report [#1478](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1478)
* 36373: No value of assets fills is shown for 5 needs [#1480](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1480)
* 36168: Incorrect wording for OVER-BUDGET in budget section [#1476](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1476)
* issues: 36454, 36462, 36466, 36459 [#1486](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1486)
* 0036393: eBI - To align with the Show Plan Illustration with web [#1485](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1485)
* 36159: eFNA- Needs Analysis - planning for specific goals need [#1483](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1483)
* 36067: eApp: CKA "Year of Graduation" value is not shown after data sync in web from IOS [#1481](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1481)
* UAT 36200: eApp: Email button not working in the Application Summary under Submission tab and also 2 Email buttons are shown [#1441](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1441)
* UAT: 0035703: eApp: Term: Female UW qn a defect [#884](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/884)
* 0036042: eApp: : Medical n Health Information qn (1- 4) defect for 3rd party cases [#1038](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1038)
* 36083: eApp: Shield: For Juvenile cases eapp is autoticked and able to proceed without entering mandatory data [#1392](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1392)
* UAT 36443: Declaration Page blank for third party case [#1472](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1472)

# v1.0.1 - b035
### Defects
* issues: 35715, 36410, 36119, 36051, 35602, 36276, 36336, 36341, 36366, 36361 [#1448](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1448)
* [UAT:36315] eBI : ILP – Risk Rating dropdown list does not getting display the correct list.(E.g. Balanced/Architas ). [#1234](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1234)
* UAT 36443: Declaration Page blank for third party case [#1472](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1427)
* UAT: 0035702: eApp: Term: Juvenile qn d mandatory validation defect [#885](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/885)
* 0035603: eApp: Insurability Info -Family history question [#1022](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1022)
* 0035755: eApp: CC for Her: Medical n Health Information qn 2 (a,b,c) defect [#1093](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1093)
* 0035754: eApp: CC for Her: Medical n Health Information qn 1 defect [#1091](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1091)
* 0035753: eApp: CC for Him: Medical n Health Information qn 2 (a,b,c) defect [#1090](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1090)
* UAT 35939 [#1240](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1240)
* UAT 35660 [#1242](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1242)
* UAT 35994 [#1294](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1294)
* UAT 36038 [#1292](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1292)
* UAT 36009 [#1297](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1297)
* UAT 36149 [#1323](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1323)
* UAT 35282 [#1475](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1475)
* 0035636: eApp: Term: Lifestyle n habits qn5 defect [#1075](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1075)
* UAT 35669 [#1190](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1190)
* UAT 35857 [#1189](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1189)
* 0036001: eApp:All products ::Tool tip is missing for both LA and proposer China Declaration form [#1126](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1126)
* UAT 35856 [#969](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/969)
* UAT 35595 [#855](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/855)
* 0035752: eApp: CC for Him: Medical n Health Information qn 1 defect [#1087](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1087)
* SIT | Data Sync | Sync fail if online has no data [#909](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/909)
* 0036160: eApp: Shield: Payment Page: CPF portion is shown under cash portion [#1437](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1437)
* 0036157: eApp: Shield: Payment Page: Cash portion is shown under CPF portion-user is not able to submit the case [#1471](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1471)
* 0035598: Eapp: When Payor's details is others all the field validations are wrong [#1215](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1215)
* 0035581: Eapp:Insurability Info :Warning note issue for alchohol question [#1067](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1067)
* 36365: Eapp:Insurability Info :App crashes when alcohol qn is answered-All products [#1470](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1470)
* UAT 35931 [#1434](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1434)
* 0036461: data sync offline to online problem [#1469](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1469)
* 0036065: PRE-EAPP - Incorrect naming for chosen proposal(s) and Not chosen proposal(s) [#1468](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1468)
* 0035155: [GIT:667] PRE-APP - User enter 10 digits but the fill only show 7 digits [#1467](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1467)
* 0036211: eApp:SHIELD :System documents are missing [#1466](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1466)
* UAT 36283 [#1465](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1465)
* UAT 36281 [#1464](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1464)
* UAT 36419 [#1463](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1463)

# v1.0.1 - b034
### Defects
* 0036291: Prompt to upload REP for SG insured (Shield) [#1462](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1462)
* 0036214: eApp: Shield :Re entry permit form is missing in the supporting docs for Singapore PR & age is <=15 [#1461](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1461)
* 0036379: eApp: AWT in after Signing the case ,Not able to find the case in Application Summary [#1460](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1460)
* 0036201: eApp: When Mandatory documents are not attached for submitted case Warning icon is missing in front of Supporting documents [#1458](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1458)
* 0035551: eApp: Mandatory asterik is not displayed for Mandatory fields in Personal Details Sections [#1457](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1457)
* UAT 35987 [#1181](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1181)
* 36324: Fields not retain in selected clinet section after user change given name [#1456](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1456)
* 36272: EFNA :Invalidation :Product type default value is reset when FNA is reset [#1321](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1321)
* 36395: Under Income Protection [#1455](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1455)
* 36390: FNA didn't refresh [#1454](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1454)
* 36305: Information is missing under trusted individual section in FNA report [#1436](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1436)

# v1.0.1 - b033
### Defects
* UAT 35968 [#1324](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1324)
* 0035586: eApp: Term: Smoking qn wordings change and field validations issue [#1214](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1214)
* 36232 && shield and non-shield can't complete problem [#1450](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1450)
* 0036012: eApp: Shield : Smoking qn wordings change and field validations issue [#1098](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1098)
* UAT: 0035641: eApp: Term: Juvenile qn a defect [#889](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/889)
* UAT: 0035643: eApp: Term: Juvenile qn b defect [#888](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/888)
* UAT: 0035705: eApp: Term: Female UW qn b defect [#882](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/882)
* UAT 36444: EFNA EAPP - 1 Application invalidated. Upon trying to do another PI, shows PI will be invalidated message [#1449](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1449)
* UAT 36417 EFNA - RPA - Question 2- is not getting synced from ios to web. [#1452](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1452)
* UAT 36416 EFNA - CKA - Year of Graduation is not getting synced from ios to web. [#1451](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1451)
* 0035902: Data Sync - Selective Cases transferred upon Sync and Last Sync time not visible. [#1282](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1282)
* 36441: Data Sync - iOS auto corrupting the RPA upon Sync in B25 [#1440](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1440)
* [UAT:36155] eBI - RHP - generated PI template issue [#1255](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1255)
* 35082: eFNA - Incorrect LA name shown for myself and LAs tab is missing (hospitalization) [#1444](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1444)
* 0036025: eApp: Shield : Medical and Health info table format is wrong [#1267](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1267)
* 36412: After data sync in app, user proceed to the same case and see that needs analysis is shown as incomplete [#1445](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1445)
* issues: 35715, 36410, 36119, 36051, 35602, 36276, 36336, 36341, 36366, 36361 [#1448](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1448)
* 0036086: Eapp:Personal details:type of pass and pass expiry date missing for 1st party and 3rd party cases [#1447](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1447)
* 0036145: eApp: Shield: Payment Page: CPF accuont holder field details are displayed wrong [#1370](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1370)
* 0036290: eApp : Data Sync, date field showing wrongly for insurability info reflexive table in App after data sync Description [#1446](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1446)

### Features
* Sprint 4 | CR158 | add 3 new products (release 10) [#1401](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1401)
* Sprint 4 | AWI BRS | eApp form for all products [#1249](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1249)


# v1.0.1 - b032
### Defects
* 36083: eApp: Shield: For Juvenile cases eapp is autoticked and able to proceed without entering mandatory data [#1392](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1392)
* 0036046: eApp: : Medical n Health Information qn 5 defect-3rd party case Description [#1286](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1286)
* UAT 36182: PRE-EAPP - ROP completed for shield, when user proceed back to commendation page, ROP is reset [#1443](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1443)
* 36335: Show the count of policy number available in App for Shield and Non Shield [#1442](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1442)
* UAT 36200: eApp: Email button not working in the Application Summary under Submission tab and also 2 Email buttons are shown [#1441](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1441)
* 0036398: Crashes for Third Party case at Health Questions [#1437](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1437)
* 0036263: eApp: Submitted case is seen in proposed tab [#1439](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1439)
* 0035019: [GIT:647] eFNA - For other under cash flow, the others value fill is not showing [#1364](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1364)

# v1.0.1 - b031
### Defects
* UAT 35931 [#1434](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1434)
* UAT 35796 [#1296](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1296)
* UAT 36433: After data sync, BI got invalidated in app [#1435](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1435)
* UAT: 36156eApp: Shield: Payment Page: GIRO check box issue [#1433](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1433)
* 0035661: eApp:FS:Eapp form:Plan details-Fund allocation for TopUp Premium is not displayed [#1138](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1138)
* [UAT: 35888] eBI - LMP - PI Template issues [#1219](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1219)
* [UAT:35913] eBI - LMP - Issues with LMP template [#1035](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1035)
* [UAT:36423] eBI - AWT PI Template - Sentence on 2nd page can be shifted to to 1st page [#1413](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1413)
* 36426: user completed PDPA but is shown as incomplete [#1431](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1431)
* UAT 36176: PRE-EAPP - Missing tool tip for shield products in recommendation page [#1414](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1414)
* 36432: FNA-Upon removing the spouse, client generation on Needs Analysis crashes app [#1425](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1425)
* 35893 [#1423](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1423)
* [UAT:36435] eBI - AWT - Regular withdrawal UI issue [#1424](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1424)
* 36195: eApp: Clicking on "AXS/SAM" link in Payment page not working [#1405](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1405)
* UAT: 0035641: eApp: Term: Juvenile qn a defect [#889](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/889)
* UAT: 0035705: eApp: Term: Female UW qn b defect [#882](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/882)
* UAT: 0035643: eApp: Term: Juvenile qn b defect [#888](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/888)
* 35622: Eapp:All products :When Payment method is selected the corresponding receipt is not appearing in Supporting Docs [#1003](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1003)
* Eapp:FS /FP:Initial RSP premium and Initial Top UP premium are displayed below Initial Total Premium page [#864](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/864)
* 35707: Eapp:LMP Payment page-Backdating premium [#1030](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1030)
* UAT: 36174 36004 36169 36350 [#1356](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1356)
* 35766 [#1180](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1180)
* 0036140: eApp: Shield: Payment page display issue [#1432](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1432)
* UAT 36270: EApp:All products :No signature expiry warning and expiry of cases [#1334](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1334)
* UAT 35594 [#860](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/860)
* UAT 36152: eAPP - when user click on the pic of shield in application screen, the page keep loading (Crash) [#1429](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1429)
* UAT 36408 pre - eApp : Shield - ROP question is reset after leaving Recommendation [#1430](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1430)
* [UAT:36180] PRE-EAPP - Character count is incorrect for shield [#1411](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1411)
* issues: 36418, 36345, 36333, 36238 [#1428](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1428)
* 0036133: Pre App :Under the ALL tab , for the submitted cases, Still shown as applying [#1427](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1427)
* [UAT:36322] eBI - AWT - white spacing in AWT in PHS [#1426](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1426)
* UAT 35979 [#1119](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1119)
* UAT: 36193: eBI - All Products - Release Version Number should follow as per Web [#1176](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1176)
* UAT 35792 [#971](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/971)
* UAT: 35718: eApp:All products Select All as NO can be checked even if questions are answered yes [$1422](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1422)
* UAT: 36343: eBI - TP and TPP - Missing Pages in PI and Product Summary and Unable to download documents [#1421](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1421)
* Assets Issues: 35046, 35060, 35208, 35215, 35222, 35605, 35727, 36153, 36159, 36245, 36373 [#1420](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1420)
* 0036400: Budget used for non selected plans should not show in budget section [#1360](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1360)
* UAT 36177: PRE-EAPP - App crash when user click on View detail in recommendation screen for shield [#1416](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1416)
* UAT 36181: PRE-EAPP - Recommendation is done for shield but it is not shown in Client’s Acceptance page [#1412](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1412)
* UAT: 36106: eApp: Data sync if Client deleted in offline ,then after data sync it is not getting deleted in web [#1419](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1419)
* UAT: 36064: eBI - LMP - Plancode is missing [#1418](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1418)
* UAT: 35906: Data Sync not consistent [#1417](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1417)
* UAT: 35668: eApp: Savvy Saver,Retire happy: Insurability Medical Q1 & 2 keyboard hiding for medical questions [#1415](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1415)

# v1.0.1 - b030
Note: this build is only for testing

# v1.0.1 - b029
### Defects
* UAT: 36196: PRE-EAPP - ROP question not showing when one yes options is selected for shield [#1410](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1410)
* UAT: 35147: eAPP - Missing text for payment term and sum assured in application page [#1409](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1409)
* UAT: 36286: eBI - App crashes at Applying stage with 3 PIs [#1408](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1408)
* UAT: 0035705: eApp: Term: Female UW qn b defect [#882](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/882)
* 0035803: Eapp:Payment:GIRO check box must be selected by default and non editable for monthly [#1407](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1407)
* 35609, 35665: Eapp:Payment page issues [#1203](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1203)
* 0035886: AWT Payment page not showing Top-Premium value and the text "Single Premium" within brackets. [#1406](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1406)
* UAT: 36249: [Framework Issue] Shield :Insurability page displayed from half of the page , user has to scroll up [#1404](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1404)
* UAT: 35696: eApp: Unable to select the Plan box after clicking on the Select button [#1403](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1403)
* UAT 35987 [#1181](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1181)

# v1.0.1 - b028
Note: this build is only for testing

# v1.0.1 - b027
### Defects
* 36232, 35649 [#1398](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1398)
* UAT 35945 [#1384](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1384)
* UAT 35946 [#1383](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1383)
* UAT 36021 [#1382](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1382)
* UAT 35975 [#1381](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1381)
* UAT 35797 [#942](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/942)
* UAT 35793 [#1380](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1380)
* UAT 35841 [#1379](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1379)
* UAT 35610 [#1112](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1112)
* UAT 35591 [#851](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/851)
* UAT 36259: eApp: Shield: personal details screen for LA is displayed from the bottom of the screen [#1400](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1400)
* UAT 36261: eApp: Life Products: personal details screen for LA is displayed from the bottom of the screen [#1399](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1399)
* 36367: eBI - Data Sync - Unable to view proposal in Web at application summary (Pre-eapp Page) [#1355](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1355)
* UAT: 0035556: Eapp:PEP table issues in the declaration page [#873](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/873)
* UAT: 35755 eApp: CC for Her: Medical n Health Information qn 2 (a,b,c) defect [#1397](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1397)
* UAT: 35604: eApp: Term: Insurance history qn2 validations are incorrect [#1396](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1396)
* 0036269: Crashes for CrtiCare Health Questions [#1252](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1252)
* 0035667: eApp: Savvy Saver,RHP : Insurability Medical Q1 & 2 [#1216](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1216)
* 0036339: eApp: Shield: UI changes in personal details section for proposer [#1395](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1395)
* 0036338: eApp: Life Products: UI changes in personal details section for Proposer and LA [#1394](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1394)
* 0035596: Eapp: When Payor's details is others some fields are missing for all products [#1132](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1132)
* 0035638: eApp: Term: Medical n Health Information qn (1- 4) defect [#1079](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1079)
* 0036042: eApp: : Medical n Health Information qn (1- 4) defect for 3rd party cases [#1038](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1038)
* UAT: 0035706: eApp: Term: Female UW qn c defect [#883](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/883)

# v1.0.1 - b026
### Defects
* 36017: eApp: All life Products : edit functionality not working for reflexive tables [#1393](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1393)
* 0036025: eApp: Shield : Medical and Health info table format is wrong [#1267](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1267)
* 0035786: eApp: Insurance history qn1 and qn2 scrolling defect [#1095](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1095)
* 36083: eApp: Shield: For Juvenile cases eapp is autoticked and able to proceed without entering mandatory data [#1392](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1392)
* UAT 35645 [#852](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/852)
* UAT 36014 [#1187](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1187)
* 0035753: eApp: CC for Him: Medical n Health Information qn 2 (a,b,c) defect [#1090](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1090)
* 0035754: eApp: CC for Her: Medical n Health Information qn 1 defect [#1091](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1091)
* 0035608: eApp: Insurability Info: Life Style & Habits Qn3 defects [#1074](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1074)
* 0035603: eApp: Insurability Info -Family history question [#1022](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1022)
* UAT 35594 [#860](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/860)
* 35612: eApp: Insurability Info: Life Style & Habits Qn4 defects [#1391](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1391)
* UAT: 0035644: eApp: Term: Juvenile qn c defect [#887](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/887)
* 0036148: Missing E-FNA after Submission [#1390](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1390)
* 0035665: Eapp:FS /FP Payment page issues: [#1389](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1389)
* 0035661: eApp:FS:Eapp form:Plan details-Fund allocation for TopUp Premium is not displayed [#1138](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1138)
* 0036306: Shield not able to enter value for Unit Number and City/State for Residential Address (Personal details) [#1335](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1335)
* 0036194: EAPP | Upload Documents before Submission [#1278](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1278)
* UAT: 35588: Eapp:Personal Details:Address finder issue [#1264](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1264)
* 0036088: eApp:Mandatory doc wrong for LA [#1225](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1225)
* 0035652: eApp:All products ::Supporting Docs:Type of pass as Others [#1224](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1224)
* 0035670: eApp:All products ::Supporting Docs:3rd party payor [#1223](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1223)
* 0036295: EApp: S pass tool tip missing after data sync [#1221](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1221)
* 0036202: eApp: Shield: Supporting docs is not appearing for P and multiple LA [#1136](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1136)
* 0035588: Eapp:Personal Details:Address finder issue [#1065](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1065)
* App crashes on click of Supporting documents [#876](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/876)
* Payment : Telegraphic Transfer scrolling and date issue [#877](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/877)
* 35566: Eapp:Supporting Docs- Other document section-Unable to add documents [#905](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/905)
* 0035995: eApp:Insurability Info :Insurance history question 2-SHIELD [#1235](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1235)
* 36037: eApp: PEP table "Source of WEALTH" others field not showing properly for 1st and 3rd party [#1387](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1387)
* UAT: 36166: eFNA - when user move to spouse section it should land user on assets and liabilities table [#1386](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1386)
* UAT : 35988, 36108, 36073, 35999 Crash Issue of TextInput [#1385](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1385)
* 35867, 34712, 36402 [#1312](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1312)
* 0035989: Band Aid Insurability info table for 2nd qn (Insurance History) is wrong and field validations missing [#1243](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1243)
* UAT 35968 [#1324](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1324)
* 0036386: Eapp:Pass expiry date field is allowing past date [#1378](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1378)
* 0036399: EApp:LA tab is shown incomplete even if mandatory fields are filled in -All products [#1377](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1377)
* 0036194: EAPP | Upload Documents before Submission [#1376](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1376)
* 0035588: Eapp:Personal Details:Address finder issue [#1375](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1375)
* UAT: 0035702: eApp: Term: Juvenile qn d mandatory validation defect [#885](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/885)
* UAT 35678 [#1110](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1110)
* 0035969: eApp: Shield: Supporting docs section error [#1374](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1374)
* 0036207: eApp : Supporting document for submitted case ,on clicking delete icon all the documents are getting deleted. [#1373](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1373)
* 35927: Needs - prioritization - the changing of priority is not functioning [#1369](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1369)
* 0036208: eApp: Supporting document for submitted case ,upload button missing,able to delete upoaded document,multiple copies of documents [#1372](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1372)
* 0036206: eApp: Supporting document for submitted case ,upload button missing,able to delete upoaded document,multiple copies of documents [#1371](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1371)
* 0036145: eApp: Shield: Payment Page: CPF accuont holder field details are displayed wrong [#1370](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1370)
* 0036183: eApp:All products ::Signature is missing in Eapp form from Supporting docs [#1368](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1368)
* 35562: Eapp: Residency and Insurance Info [#1367](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1367)
* 35552: Eapp:Edit Existing Insurance Portfolio at FNA button [#1366](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1366)
* 36306, 35543, 35155, 36065 [#1365](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1365)
* 0035019: [GIT:647] eFNA - For other under cash flow, the others value fill is not showing [#1364](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1364)

Note:
* eab-web-api tag: [1.0.1-b025](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/eab-web-api/tags/1.0.1-b24)
* eab-core-api tag: [1.0.1-b025](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/eab-core-api/tags/1.0.1-b024)
* AXA_SG_APP_web_service tag: [1.9.0.10](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/AXA_SG_APP_web_service/tags/1.9.0.10)

# v1.0.1 - b024
### Defects
* 36057: eFNA - No tool tip for surplus for disability benefit needs [#1317](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1317)
* UAT: 0035703: eApp: Term: Female UW qn a defect [#884](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/884)
* 0036012: eApp: Shield : Smoking qn wordings change and field validations issue [#1098](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1098)
* FNA issues: 36377, 35194, 36234, 36305, 35974, 35976, 35977 [#1363](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1363)
* 36040: iOS Release 2 | Need Selection | Dependent spouse is not shown for Hospitalisation Cost [#1326](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1326)
* 36405: eFNA : Crash when linking Grandfather / Grandmother insured [#1358](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1358)
* UAT 35569: eApp: : signature section docs not generated properly for 1st and 3rd party [#1332](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1332)
* 36247: When user delete spouse profile, myself options is not selected [#1333](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1333)
* 36221: eFNA - When user had not enter reason in risks profile, done button is enable. [#1236](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1236)
* UAT: 35145, 36328 After create proposal it is not landing to application summary proposed tab [#1338](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1338)
* [UAT:36312] eBI: ILP- Adjust Fund allocation-Balanced/Architas - other Risk Rating funds still showing after user adjusted their funds. [#1233](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1233)
* [UAT:36315] eBI : ILP – Risk Rating dropdown list does not getting display the correct list.(E.g. Balanced/Architas ). [#1234](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1234)
* [UAT:36397] Adjust Fund Allocation is not appearing for 2 Architas Models in AWT [#1361](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1361)
* 35229: [GIT:762] eFNA - Needs for spouse is not shown under view summary. [#1353](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1353)
* 0035894: PRE-EAPP - Wrong question is showing in budget section [#1329](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1329)
* 36253: When user delete value under existing insurance and insurance premium the fill shows enter amount (regression) [#1244](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1244)
* UAT 35789 [#965](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/965)
* 35766 [#1180](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1180)
* 36250 [#1362](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1362)
* UAT: 36174 36004 36169 36350 [#1356](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1356)
* 0035849: For all products : PEP table PEP Information Lables name wrong,Country value wrong and not able to Edit the table [#1102](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1102)
* UAT 35849  [#1188](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1188)
* 36278: EFNA :Profile created at PDPA is duplicated-random issue [#1357](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1357)
* UAT 36270: EApp:All products :No signature expiry warning and expiry of cases [#1334](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1334)
* 0036292: eApp:Shield:Unable to open the LA documents [#1354](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1354)
* 0036321: eApp: Shield: Proposer and LA tab movement issue [#1352](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1352)
* 35768: Band Aid not able to view Policy illustration and E-App form after signature [#1010](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1010)
* 0035761: RHP :Error message displayed when the user goes from the payment page back to Signature page [#1351](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1351)
* 0036082: Bundling seems not working [#1350](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1350)
* 0036243: Shield app Unable to submit, no PNO generated [#1349](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1349)
* 0035546: Eapp:Building / estate name field spelling error in personal details section [#1348](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1348)
* 35672: eApp: Personal Details pages issues [#1004](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1004)
* 0036310: eApp : When ID Document Type is others getting highlighted in CP pop up before E-App [#1347](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1347)
* 0036031: eAPP - LMP Sum Assured displayed not correct [#1066](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1066)
* 0036030: Plan Details :FS/FP plus plan details issue [#1345](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1345)
* 0035570: eApp: Term: supporting section pdf documents are blank [#1344](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1344)
* 0035655: eApp:All products ::Supporting Docs:Type of pass as Social Visit Pass [#1343](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1343)
* 0035652: eApp:All products ::Supporting Docs:Type of pass as Others [#1342](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1342)
* 0035670: eApp:All products ::Supporting Docs:3rd party payor [#1341](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1341)
* 0036220: eApp:Shield:Plan details header and details text issue [#1340](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1340)
* 0035626: eApp: TI section is missing in declaration page Description [#1107](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1107)
* 0036295: EApp: S pass tool tip missing after data sync [#1339](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1339)
* 0036092: EAPP | Singpost Branch Details in EAPP UI Personal Details Section is missing [#1337](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1337)

Note:
* eab-web-api tag: [1.0.1-b024](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/eab-web-api/tags/1.0.1-b24)
* eab-core-api tag: [1.0.1-b024](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/eab-core-api/tags/1.0.1-b024)
* AXA_SG_APP_web_service tag: [1.9.0.10](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/AXA_SG_APP_web_service/tags/1.9.0.10)

# v1.0.1 - b023
Note: this build is only for testing data sync

# v1.0.1 - b022
### Defects
* 0036306: Shield not able to enter value for Unit Number and City/State for Residential Address (Personal details) [#1335](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1335)
* UAT 35790 [#1191](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1191)
* UAT 35560 [#1036](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1036)
* 0035752: eApp: CC for Him: Medical n Health Information qn 1 defect [#1087](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1087)
* [UAT:35957] eBI - LMP - Values in the Bundle Template are having a $1 difference [#1040](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1040)
* UAT: 35687: eApp: App crashes when trying to review signed documents [#1331](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1331)
* UAT: 35681: eApp: Not able to view FC signature in the e-App form [#1330](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1330)

Note:
* eab-web-api tag: [1.0.1-b022](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/eab-web-api/tags/1.0.1-b22)
* eab-core-api tag: [1.0.1-b021](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/eab-core-api/tags/1.0.1-b021)
* AXA_SG_APP_web_service tag: [1.9.0.10](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/AXA_SG_APP_web_service/tags/1.9.0.10)

# v1.0.1 - b021
### Defects
* 0036112: Supporting documents issue [#1328](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1328)
* 0036265: EApp:Supporting docs :User is able to upload more than 8 files [#1327](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1327)
* 36272: EFNA :Invalidation :Product type default value is reset when FNA is reset [#1321](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1321)
* 36032: eFNA - When user come back to review CKA “Continue to Risk Assessment” button not enable [#1315](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1315)
* 36057: eFNA - No tool tip for surplus for disability benefit needs [#1317](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1317)
* 0035911: eApp: Occupation as Others is not populated to Eapp personal details  [#1325](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1325)
* [UAT: 35888] eBI - LMP - PI Template issues [#1219](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1219)
* 35649, 35543, 36232 [#1320](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1320)
* 0035638: eApp: Term: Medical n Health Information qn (1- 4) defect [#1079](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1079)
* 0036323: Incorrect wordings for Residency Questions [#1308](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1308)
* UAT: 0035556: Eapp:PEP table issues in the declaration page [#873](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/873)
* 35903: CKA and Risk Profile become incomplete even though all fills had been enter previously [#1313](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1313)
* issues: 36137, 36066, 36238, 36122, 35625, 35068, 36352, 36362, 36254, 35840, 35022 [#1322](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1322)
* UAT: 0035641: eApp: Term: Juvenile qn a defect [#889](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/889)
* UAT: 0035643: eApp: Term: Juvenile qn b defect [#888](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/888)
* UAT 35592 [#1301](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1301)
* UAT 35592 [#858](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/858)
* UAT 35985 [#1319](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1319)
* 0036046: eApp: : Medical n Health Information qn 5 defect-3rd party case Description [#1286](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1286)
* 0036042: eApp: : Medical n Health Information qn (1- 4) defect for 3rd party cases [#1038](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1038)
* 0036369: After data sync, user is unable to create a new client [#1318](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1318)
* 36221: eFNA - When user had not enter reason in risks profile, done button is enable. [#1236](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1236)

### Features
* Sprint 4 | AWI BRS | Behaviour of SOF/SOW changes in iOS [#1247](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1247)

Note:
* eab-web-api tag: [1.0.1-b021](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/eab-web-api/tags/1.0.1-b21)
* eab-core-api tag: [1.0.1-b021](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/eab-core-api/tags/1.0.1-b021)
* AXA_SG_APP_web_service tag: [1.9.0.10](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/AXA_SG_APP_web_service/tags/1.9.0.10)

# v1.0.1 - b020
Note: this build is only for testing sit1 env.

# v1.0.1 - b019
### Defects
* UAT: 36364: eBI - All Products - Unable to Download BI. Download Function is not working. [#1316](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1316)
* UAT 36135 [#1314](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1314)
* UAT 36053 [#1077](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1077)
* 0036313: eApp: Life Products: able to add more than 8 rows for reflexive tables [#1306](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1306)
* 0036019: eApp: All Life Products: reflexive table formatting issue. [#1285](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1285)
* UAT: 36171: eBI_Shield: Download button for QQ is not functioning, the app crashes upon selecting. [#1179](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1179)
* 0035902: Data Sync - Selective Cases transferred upon Sync and Last Sync time not visible. [#1282](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1282)

### Features
* Sprint 4 | AWI BRS | Behaviour of SOF/SOW changes in iOS [#1247](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1247)

Note:
* eab-web-api tag: [1.0.1-b019](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/eab-web-api/tags/1.0.1-b019)
* eab-core-api tag: [1.0.1-b018](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/eab-core-api/tags/1.0.1-b018)
* AXA_SG_APP_web_service tag: [1.9.0.10](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/AXA_SG_APP_web_service/tags/1.9.0.10)

# v1.0.1 - b018
### Defects
* 36136: eFNA - unable to to save when user choose a different relationship [#1311](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1311)
* UAT 35986 [#1310](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1310)
* UAT 35660 [#1242](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1242)
* UAT 35651 [#1239](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1239)
* UAT 35592 [#1301](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1301)
* UAT 35646 [#1300](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1300)
* UAT 35656 [#1299](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1299)
* UAT 35941 [#1298](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1298)
* UAT 36009 [#1297](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1297)
* UAT 35796 [#1296](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1296)
* UAT 36008 [#1295](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1295)
* UAT 35994 [#1294](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1294)
* UAT 35781 [#1293](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1293)
* UAT 36038 [#1292](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1292)
* 0036314: eApp: Shield: able to add more than 8 rows for reflexive tables [#1307](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1307)
* [UAT: 35888] eBI - LMP - PI Template issues  [#1219](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1219)
* 0036287: EAPP : FATCA Country/jurisdiction of Tax Residency alignment issue [#1305](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1305)
* 0036260: eApp: Shield: medical n health qn 5 defect [#1304](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1304)
* 0036090: eApp: Juvenile declaration note is missing in UW section [#1303](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1303)
* 0036296: EApp: Term conversion qn and warning note missing after data sync [#1309](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1309)
* UAT 35987 [#1181](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1181)
* 0036068: PRE-EAPP - Unable to see the last BI in recommendation page [#1291](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1291)
* 0036301: eBI - STP - different email content [#1284](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1284)
* 0036028: eApp: Shield : Sex reassignment table format is wrong [#1289](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1289)
* 0035564: eApp: Declaration notes display error for payor details [#1290](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1290)
* UAT: 35613: eBI |All Products | App not opening in the correct page after screen idle time [#1288](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1288)
* UAT: 36193: eBI - All Products - Release Version Number should follow as per Web [#1176](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1176)
* UAT: 35940: When e-App impacted field is changed FNA and Products page getting reset [#1277](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1277)
* 0036015: eApp: Shield: reflexive table formatting issue. [#1287](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1287)
* [UAT:36312] eBI: ILP- Adjust Fund allocation-Balanced/Architas - other Risk Rating funds still showing after user adjusted their funds. [#1233](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1233)
* [UAT:36315] eBI : ILP – Risk Rating dropdown list does not getting display the correct list.(E.g. Balanced/Architas ). [#1234](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1234)
* [UAT:36155] eBI - RHP - generated PI template issue [#1255](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1255)
* [UAT:36344] ILP-Crash Issue when the user change their Investment option from Build a portfolio of funds to Optimise to your risk profile. [#1274](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1274)
* 36203: CP - when user click on link family member it is showing TI client profile screen [#1279](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1279)
* 36197, 35155, 36065, 35956 [#1281](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1281)
* 36334: Completed FNA but at Product Selection show FNA is incomplete [#1283](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1283)
* UAT 35626: eApp: TI section is missing in declaration page [#1276](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1276)
* 0036050: eApp: Insurability Info -Regular doctor question-3rd party [#1275](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1275)
* 0036269: Crashes for CrtiCare Health Questions [#1252](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1252)
* UAT 35642 [#1280](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1280)
* 0036194: EAPP | Upload Documents before Submission [#1278](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1278)

### Features
* Sprint 4 | AWI BRS | LMP Lite (ALT) BRS - Term conversion questionnaire text change – applicable for FP/FS, LMP,ALT [#1251](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1251)

Note:
* eab-web-api tag: [1.0.1-b018](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/eab-web-api/tags/1.0.1-b018)
* eab-core-api tag: [1.0.1-b018](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/eab-core-api/tags/1.0.1-b018)
* AXA_SG_APP_web_service tag: [1.9.0.10](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/AXA_SG_APP_web_service/tags/1.9.0.10)

# v1.0.1 - b017
### Defects
* 36330: eBI : Email triggered for QQ should NOT be password protected [#1273](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1273)
* 36250: eApp: Shield: Personal Details shown as incomplete [#1253](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1253)
* [UAT: 35888] eBI - LMP - PI Template issues [#1219](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1219)
* 35821: FNA and pre-eapp screens don't have 'Save and close' button [#1199](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1199)
* 36253: When user delete value under existing insurance and insurance premium the fill shows enter amount (regression) [#1244](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1244)
* 36251: After user delete the budget, question are not hiding. (Regression) [#1245](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1245)
* 35653: FNA - Refresh Issue [#1258](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1258)
* 36205: Eapp:Shield :Personal details: [#1259](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1259)
* 0035596: Eapp: When Payor's details is others some fields are missing for all products [#1132](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1132)
* UAT: 36051: eApp: eapp impact field invalidation issue [#1272](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1272)
* UAT: 35838: After deleting invalidated BI, some section in need become incomplete. [#1271](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1271)
* UAT: 36111: eApp: For AWT,ESP 1st party after clicking "Apply" button app is not proceeding to E-App section. [#1270](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1270)
* UAT: 36107: eApp:ESP:Insurability Info doesnt load occasionally for ESP [#1269](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1269)
* UAT: 35634: Eapp:Personal details:No invalidation error message when BI impact field is updated [#1268](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1268)
* 0035883: eApp:All products ::Supporting Docs:Total file size can be more than 20 mb [#1266](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1266)
* 35880: eApp:All products ::Supporting Docs:User is able to upload more than 3 mb [#997](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/997)
* eApp:All products ::Supporting Docs:Type of pass as LTVP [#955](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/955)
* 0036233: eAPP - Supporting Docs : Tool tip is missing for medical test report [#1265](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1265)
* UAT: 35588: Eapp:Personal Details:Address finder issue [#1264](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1264)
* 0035989: Band Aid Insurability info table for 2nd qn (Insurance History) is wrong and field validations missing [#1243](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1243)
* 0035732: Eapp:All products :ID Doc type is populated as others in the personal details section [#1263](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1263)
* 0035966: eApp: Shield: LA details are incorrect in personal details section [#1262](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1262)
* 0036005: eApp All products :Personal details shown as complete even if mandatory fields for LA for 3rd party case [#1261](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1261)
* 0036086: Eapp:Personal details:type of pass and pass expiry date missing for 1st party and 3rd party cases [#1260](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1260)
* 35734, 35965: eApp: 3rd party Personal details not correct for all the products [#953](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/953)
* 0035995: eApp:Insurability Info :Insurance history question 2-SHIELD [#1235](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1235)
* 0036242: Shield :Child health booklet is not displayed in the Supporting docs [#1257](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1257)

### Features
* Sprint 4 | AWI BRS | Behaviour of SOF/SOW changes in iOS [#1247](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1247)

Note:
* eab-web-api tag: [1.0.1-b017](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/eab-web-api/tags/1.0.1-b017)
* eab-core-api tag: [1.0.1-b017](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/eab-core-api/tags/1.0.1-b017)
* AXA_SG_APP_web_service tag: [1.9.0.9](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/AXA_SG_APP_web_service/tags/1.9.0.9)

# v1.0.1 - b016
### Defects
* UAT: 35825: eBI : Shield_For decline nationality/countries, Singpost, the product image is appearing but for Direct is ineligible product [#1256](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1256)
* 0035596: Eapp: When Payor's details is others some fields are missing for all products [#1132](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1132)
* 0035978: Email received from AXA_axa.logo attachment and BI for STP case is not pw protected  [#1254](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1254)
* [UAT:36309] eBI: AWT – Adjust fund allocation – Balanced/Model 3 – Risk Rating list additional row it showing as “Select” [#1220](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1220)
* 36221: eFNA - When user had not enter reason in risks profile, done button is enable.  [#1236](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1236)
* 35244: Other needs Surplus value not auto populated,alignment for PROTECTION button not correct [#1227](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1227)
* 35897: eFNA - Final expenses showing extra zero (income protection)  [#1212](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1212)
* 35942: EFNA - Disability benefit and Critical Illness Benefit Defects [#993](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/993)
* 36185, 36147, 36034 [#1202](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1202)
* UAT: 35871: Data Sync function – Quick Quote history is showing different Products from EASE IOS to EASE Web SIT Environment. [#1238](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1238)
* UAT 35041 [#1237](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1237)
* 0036061: eApp: Shield: Female UW qns a,b,c,d defect [#1117](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1117)
* 0036109: eApp: App crashing in supporting document for 3rd party when clicked on the Uploaded photo for viewing(all products) [#1230](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1230)
* 0035895: For all products for Supporting documents ,app hangs when clicked on uploaded document [#1229](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1229)
* 0036202: eApp: Shield: Supporting docs is not appearing for P and multiple LA [#1228](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1228)
* 0036099: eApp: Shield: CHB is not uploaded in supporting docs if juvenile qns are answered [#1226](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1226)
* 0036088: eApp:Mandatory doc wrong for LA [#1225](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1225)
* eApp:All products ::Supporting Docs:Type of pass as Social Visit Pass [#952](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/952)
* 0035652: eApp:All products ::Supporting Docs:Type of pass as Others [#1224](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1224)
* 0035670: eApp:All products ::Supporting Docs:3rd party payor [#1223](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1223)
* 0036215: eApp: Shield :Discharge summary report is missing if "Discharge summary Report" selected as Yes for sex reassignment surgery q [#1146](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1146)
* 0036216: eApp: Shield Medical Test Report is missing for P and LA [#1147](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1147)
* 0036244: Shield :Other illness questionnaire is missing in supporting docs [#1222](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1222)
* 0036295: EApp: S pass tool tip missing after data sync [#1221](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1221)

### Features
* Sprint 4 | CR159 | Masking of 3 Funds and update fund list [#1175](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1175)

Note:
* eab-web-api tag: [1.0.1-b016](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/eab-web-api/tags/1.0.1-b016)
* eab-core-api tag: [1.0.1-b012](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/eab-core-api/tags/1.0.1-b012)
* AXA_SG_APP_web_service tag: [1.9.0.9](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/AXA_SG_APP_web_service/tags/1.9.0.9)

# v1.0.1 - b015
### Defects
* 36288: Validation for Loading is not working [#1218](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1218)
* 0035585: eApp: Term: Smoking qn incorrect validations [#1217](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1217)
* 0036060: eApp: Shield: Female UW qn a defect [#1109](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1109)
* 0035667: eApp: Savvy Saver,RHP : Insurability Medical Q1 & 2 [#1216](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1216)
* UAT: 0035703: eApp: Term: Female UW qn a defect [#884](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/884)
* 0035598: Eapp: When Payor's details is others all the field validations are wrong [#1215](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1215)
* 0035586: eApp: Term: Smoking qn wordings change and field validations issue [#1214](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1214)
* UAT: 0035702: eApp: Term: Juvenile qn d mandatory validation defect [#885](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/885)
* 0035581: Eapp:Insurability Info :Warning note issue for alchohol question [#1067](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1067)
* UAT: 0035705: eApp: Term: Female UW qn b defect [#882](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/882)
* iOS | Release 2 | FNA | < urgent > Random crashing [#880](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/880)
* 36268: EApp :Getting time bomb error message even if data is synced [#1213](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1213)
* 0036255: eApp: app crash after submission and when logged in again all clients are missing [#1209](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1209)

Note:
* eab-web-api tag: [1.0.1-b015](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/eab-web-api/tags/1.0.1-b015)
* eab-core-api tag: [1.0.1-b012](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/eab-core-api/tags/1.0.1-b012)
* AXA_SG_APP_web_service tag: [1.9.0.9](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/AXA_SG_APP_web_service/tags/1.9.0.9)

# v1.0.1 - b014
### Defects
* 0035649: eApp: Supporting doc page display error [#1208](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1208)
* 0036154: eApp:multiple LA Shield:Plan details header and details [#1207](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1207)
* UAT (Plan Detail Issue): 36030, 35791, 35759, 35863, 35763, 35765, 35672 [#1205](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1205)
* 0036143: Returns back to submision page after Data Sync [#1206](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1206)
* 0035886: AWT Payment page not showing Top-Premium value and the text "Single Premium" within brackets. [#1204](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1204)
* 35739: eApp: 3rd party Personal details populate residential address from proposer is missing [#983](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/983)
* 35609, 35665: Eapp:Payment page issues [#1203](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1203)
* UAT 36275 [#1196](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1196)
* 0035896: Eapp:City is not mandatory in Eapp for few selected countries [#1201](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1201)
* 0035579: Eapp : Editable Personal details fields not getting updated in Client Profile [#1200](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1200)

Note:
* eab-web-api tag: [1.0.1-b014](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/eab-web-api/tags/1.0.1-b014)
* eab-core-api tag: [1.0.1-b012](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/eab-core-api/tags/1.0.1-b012)
* AXA_SG_APP_web_service tag: [1.9.0.9](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/AXA_SG_APP_web_service/tags/1.9.0.9)

# v1.0.1 - b013
### Defects
* [UAT:35227] iOS | Release 2 | Needs Analysis | Priority | <Medium> Retirement Plan not able to drag and drop, calculation wrong for short fall and surplus [#584](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/584)
* 35208: Needs Analysis for Retirement page age not able to key in ,no value for years of retirement [#1195](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1195)
* UAT 35887 eBI : Revised AWT BI Template (Reduction in Yield) – Missing Paragraph and Missing Value at Reduction in Yield. [#1193](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1193)
* 35738: eBI - FPFS - Insurance Charge Issue [#1198](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1198)
* 0035559: Eapp:FATCA formatting issues in the declaration page [#1192](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1192)
* eBi issues: 36228, 36226 [#1197](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1197)
* FNA issues: 36225, 36032, 36011, 35737, 35903 [#1194](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1194)
* 36222: Unable to proceed at Needs if Marital Status = widow or divorced [#1156](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1156)
* [UAT:35233] iOS | Release 2 | Need Analysis | [Medium] Green tick is shown but user had not provided any reason for Retirement planning [#759](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/759)
* UAT 35987 [#1181](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1181)
* 35766 [#1180](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1180)
* 0035565: Eapp: All Insurability info asnwers are not checked as No, if the checkbox "I Confirm the answer to all is NO" is selected [#1059](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1059)
* 0035565: Eapp: All Insurability info asnwers are not checked as No, if the checkbox "I Confirm the answer to all is NO" is selected  [#1061](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1061)
* 0036008: eApp:Insurability Info :Insurance history question 3-SHIELD [#1185](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1185)
* [UAT:35198] | iOS | Release 2 | Products | All Products | <low> Missing Dollar sign “S$” for Basic and Rider Premium [#778](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/778)
* UAT: 36142: eBI: AXA Shield - Missing Full Stop Dot “.” For Life Assured Incomplete Profile error message. [#1186](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1186)
* 35707: Eapp:LMP Payment page-Backdating premium [#1160](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1160)
* 35537, 35543 [#1162](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1162)
* 0036212: eApp: Shield: Plan details crashing issue [#1184](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1184)
* 0036211: eApp:SHIELD :System documents are missing [#1183](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1183)
* [UAT:36170] eBI - ILP - Risk rating issue [#1182](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1182)

Note:
* eab-web-api tag: [1.0.1-b013](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/eab-web-api/tags/1.0.1-b013)
* eab-core-api tag: [1.0.1-b012](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/eab-core-api/tags/1.0.1-b012)
* AXA_SG_APP_web_service tag: [1.9.0.9](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/AXA_SG_APP_web_service/tags/1.9.0.9)

# v1.0.1 - b012
### Defects
* Get policy number 400 error [#1178](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1178)
* UAT 35254: eFNA - Missing pages in FNA report and no borders for respective pages. [#1177](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1177)
* 35551, 36003, 35911, 35542, 35589, 35590 [#1170](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1170)
* 0036265: EApp:Supporting docs :User is able to upload more than 8 files [#1161](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1161)
* 36258: eApp: wordings are incorrect for "is your residential address same as.." qn [#1158](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1158)
* 36246: Missing text in Consent notice page [#1150](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1150)

Note:
* eab-web-api tag: [1.0.1-b012](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/eab-web-api/tags/1.0.1-b012)
* eab-core-api tag: [1.0.1-b012](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/eab-core-api/tags/1.0.1-b012)
* AXA_SG_APP_web_service tag: [1.9.0.8](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/AXA_SG_APP_web_service/tags/1.9.0.8)

# v1.0.1 - b011
### Defects
* eBI issues: 36172, 36101, 35757, 35776, 35782, 35952, 36131, 36162, 36164, 36179 [#1133](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1133)
* UAT: 35605: Unable to proceed to products even after completing the needs analysis for Retirement Planning [#913](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/913)
* UAT 36218  [#1157](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1157)
* UAT 36119 [#1153](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1153)
* 0036248: eApp : Data Sync Client Profile When ID Document Type is others getting highlighted after data sync [#1154](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1154)
* eBi issues: 35806, 35905, 36077 [#1152](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1152)

### Features
* Follow-up from "#974 #977" [#1159](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1159)
* Sprint 4 | CR122 | Other changes | Fund Structure and changes for ILP [#974](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/974)
* Sprint 4 | CR142 | Other changes | Permanent Fix for new fund structure [#977](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/977)

Note:
* eab-web-api tag: [1.0.1-b011](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/eab-web-api/tags/1.0.1-b011)
* eab-core-api tag: [1.0.1-b011](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/eab-core-api/tags/1.0.1-b011)
* AXA_SG_APP_web_service tag: [1.9.0.8](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/AXA_SG_APP_web_service/tags/1.9.0.8)

# v1.0.1 - b010
### Defects
* 36173: After cases submission, no message is shown as per web [#1151](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1151)
* 36150: unexpect error msg prompt at eAPP - insurability or Declaration [#1149](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1149)
* 36138, 36146, 36145, 36141, 36140 [#1148](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1148)
* UAT 35788 [#1145](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1145)
* 0036216: eApp: Shield Medical Test Report is missing for P and LA [#1147](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1147)
* 0036215: eApp: Shield :Discharge summary report is missing if "Discharge summary Report" selected as Yes for sex reassignment surgery q [#1146](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1146)
* 35708 Framework : Offline login account lock [#1143](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1143)
* 0036214: eApp: Shield :Re entry permit form is missing in the supporting docs for Singapore PR & age is <=15 [#1144](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1144)
* 0035752: eApp: CC for Him: Medical n Health Information qn 1 defect [#1087](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1087)
* UAT (Currency Issue) - 35866, 35723, 35717, 35731, 35736 [#1142](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1142)
* 0035673: Eapp:FS /FP Payment page-Top Up Premium [#1141](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1141)

# v1.0.1 - b009
### Defects
* eBI issues: 36172, 36101, 35757, 35776, 35782, 35952, 36131, 36162, 36164, 36170, 36179 [#1133](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1133)
* 0036175: Issues with 3rd party cases on Uploading Supporting Documents [#1121](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1121)
* 0036188: Eapp: Supporting documents tab not showing for 3rd party(for all products) [#1139](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1139)
* 0035661: eApp:FS:Eapp form:Plan details-Fund allocation for TopUp Premium is not displayed [#1138](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1138)
* 0036058: Eapp:Japanese declaration form missing for ist and 3rd party [#1053](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1053)
* 0036029: eApp: Supporting doucment for other documents when same type is selected document type getting removed from listeApp: Supporting [#1056](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1056)
* 0035868: eApp:All products ::Supporting Docs Refresh issue [#1137](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1137)
* 0036202: eApp: Shield: Supporting docs is not appearing for P and multiple LA [#1136](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1136)
* UAT 36095 [#1116](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1116)
* 0036230: eApp: Shield: place holder is missing for other document in supporting docs [#1135](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1135)
* 0036023: eApp: Shield: place holder is missing for other document in supporting docs [#1124](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1124)
* 0035890: eApp:All products ::No place holders if supporting docs is selected as Others [#1134](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1134)
* 36056: PRE-EAPP - Incorrect Benefit and limitation text for Premium Eraser Total and Premium Waiver (UN) under Retire happy [#1125](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1125)
* UAT 36043: Incorrect Benefit and limitation text for LMP [#1085](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1085)
* UAT 35861 [#937](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/937)
* UAT 36096 [#1115](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1115)

# v1.0.1 - b008
### Defects
* UAT 36085: Email not received after cases submitted [#1131](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1131)
* UAT 36158: eBI: AWT - Regular Withdrawal - Incorrect Values [#1129](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1129)
* UAT 36169: eBI - unable to receive email at BI Proposal Stage [#1130](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1130)
* 35890, 36023, 36024: eApp:All products ::No place holders if supporting docs is selected as Others [#1083](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1083)
* 35883: eApp:All products ::Supporting Docs:Total file size can be more than 20 mb [#1101](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1101)
* 0036012: eApp: Shield : Smoking qn wordings change and field validations issue [#1098](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1098)
* 0035638: eApp: Term: Medical n Health Information qn (1- 4) defect [#1079](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1079)
* 35874: eAPP - Spouse name not show next to the proposal name in application screen [#1128](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1128)
* UAT 35947 [#1123](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1123)

# v1.0.1 - b007
### Defects
* 35880 Error message change [#1007](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1007)
* 0035949: eApp: Term/TPP: payment page shows top-up premium for Term/TPP [#1127](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1127)
* UAT 35947 [#1123](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1123)

# v1.0.1 - b006
### Defects
* UAT 35861 [#937](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/937)
* UAT 35947 [#1123](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/112)
* 0036023: eApp: Shield: place holder is missing for other document in supporting docs [#1124](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1124)
* 0035611: EApp:Supporting Docs:Size of the file is shown as 0 KB [#1103](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1103)
* 0035607: eApp: Insurability Info: Alcohol Qn defects [#1073](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1073)
* UAT (Plan Detail Issues) - 35742, 36174, 35756, 35910, 35907, 35763, 35863, 35759, 35791, 36030 [#1122](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1122)
* 0036175: Issues with 3rd party cases on Uploading Supporting Documents [#1121](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1121)
* UAT 36199 [#1120](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1120)

# v1.0.1 - b005
### Defects
* UAT 35554 [#853](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/853)
* UAT 35979 [#1119](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1119)
* UAT 36041 [#1118](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1118)
* [UAT:35227] iOS | Release 2 | Needs Analysis | Priority | <Medium> Retirement Plan not able to drag and drop, calculation wrong for short fall and surplus[#584](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/584)
* UAT 36035 [#1114](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1114)
* UAT 35980 [#1113](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1113)
* UAT 35610 [#1112](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1112)
* UAT 36039 [#1111](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1111)
* UAT 35678 [#1110](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1110)
* UAT 36095 [#1116](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1116)
* UAT 36096 [#1115](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1115)
* UAT 35557 [#1047](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1047)
* UAT 35550 [#1050](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1050)

# v1.0.1 - b004
### Defects
* FNA issues: 35853, 35038, 36054, 35022, 35030, 35033, 35041, 35042, 35046, 35313, 35572, 35653, 35050, 35057, 35663, 35671, 35208, 35211, 35217, 35221, 35237, 35244, 35265, 35076, 35077, 36054, 36010, 34997, 35682, 35079, 35785, 35821, 35828, 35928, 35993 [#1069](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1069)
* 0035833: Eapp:All products :Payor details formatting issue [#1106](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1106)
* 0035992: eApp:Shield Height and weight field validation is wrong [#1097](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1097)
* 0035991: Band Aid Insurability info for Qn2 Insurance History* is missing [#1096](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1096)
* [UAT:35197] | iOS | Release 2 | Products | RHP | <Medium> The words Enter Retirement Income is missing on the Quotation Page [#780](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/780)
* 0035990: EASE Ios not asking for online authentication for internet functionalities [#1108](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1108)
* UAT 35851 [#968](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/968)
* 0036013: eApp: Shield: Supporting doc section mandatory icon is missing [#1104](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1104)
* other need issues: 35239, 35241, 35242 [#1062](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1062)
* 35883: eApp:All products ::Supporting Docs:Total file size can be more than 20 mb [#1101](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1101)

# v1.0.1 - b003
### Defects
* UAT (Plan Detail Issues): 36048, 36044, 36047, 36045, 35575, 35577, 35675, 35742, 35864, 35813 [#1099](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1099)
* FNA issues: 35227, 35135, 35835, 35229, 35877 [#1092](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1092)
* 35890, 36023, 36024: eApp:All products ::No place holders if supporting docs is selected as Others [#1083](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1083)
* 35880 Error message change [#1007](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1007)
* 35927: Needs - prioritization - the changing of priority is not functioning [#1046](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1046)
* [UAT: 35953] FNA pdf is not password protected for email send to customer and agent trigger in FNA [#1089](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1089)

Note: v1.0.1 build 002 was crash when building, so we will skip build 002.

# v1.0.1 - b001
### Defects
* UAT 36053 [#1077](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1077)
* UAT 35930 [#1076](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1076)
* 0035581: Eapp:Insurability Info :Warning note issue for alchohol question [#1067](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1067)
* UAT: 0035641: eApp: Term: Juvenile qn a defect [#889](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/889)
* [UAT: 0035844] Cannot submit the Shield case from mobile API [#1088](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1088)
* UAT 36043: Incorrect Benefit and limitation text for LMP [#1085](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1085)
* 36026: eApp: Supporting doucment for other documents when same type is selected document type getting removed from list [#1049](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1049)
* UAT: 0035643: eApp: Term: Juvenile qn b defect [#888](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/888)
* 35973: eApp: Shield: document size is not uploaded correctly for .PDF and .png [#1070](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1070)
* 35734, 35965: eApp: 3rd party Personal details not correct for all the products [#953](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/953)

# v1.0.0 - b161
### Defects
* other need issues: 35239, 35241, 35242 [#1062](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1062)
* NA flow issues: 35041, 35057, 35063, 35205 [#1052](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1052)
* Retirement Plannings issues: 35208, 35233, 35211, 35809, 35217 [#1045](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1045)
* Income Protection: 35042, 35572, 35899, 35897, 36006, 35881 [#1042](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1042)
* [UAT:35072] iOS | Release 2 | Needs Analysis | <Medium> Incorrect error message and the value should be auto-populated to 50k if less than 50k [#709](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/709)
* FNA issues: 35853, 35038, 36054, 35022, 35030, 35033, 35041, 35042, 35046, 35313, 35572, 35653, 35050, 35057, 35663, 35671, 35208, 35211, 35217, 35221, 35237, 35244, 35265, 35076, 35077, 36054, 36010, 34997, 35682, 35079, 35785, 35821, 35828, 35928, 35993 [#1069](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1069)
* UAT (Supporting Doc Issue) - 35648, 35670, 35862, 35693, 35606 [#1084](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1084)
* UAT 36113 [#1082](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1082)
* 0035561: Eapp:Fields missing in the reflexive table of FATCA [#1063](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1063)
* 0035790: eApp: PEP table country is missing and scrolling defect [#1071](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1071)
* 0035584: Eapp:Insurability Info:Savy Saver,RHP Medical and health information warning note issue [#1068](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1068)
* UAT 35538 [#966](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/966)
* 0035638: eApp: Term: Medical n Health Information qn (1- 4) defect [#1079](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1079)
* 0035640: eApp: Term: Medical n Health Information qn 5 defect [#1080](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1080)

### Features
* Sprint 4 | CR126A | Other changes | FNA report - ROP and Shield and non-shield product changes [#975](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/975)

Note:
* eab-web-api tag: [1.0.0-b161](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/eab-web-api/tags/1.0.0-b161)
* eab-core-api tag: [1.0.0-b160](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/eab-core-api/tags/1.0.0-b160)
* AXA_SG_APP_web_service tag: [1.9.0.5](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/AXA_SG_APP_web_service/tags/1.9.0.5)

# v1.0.0 - b160
### Defects
* UAT 36075 [#1078](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1078)
* UAT 36053 [#1077](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1077)
* UAT 35930 [#1076](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1076)
* 0035636: eApp: Term: Lifestyle n habits qn5 defect [#1075](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1075)

# v1.0.0 - b159
### Defects
* 0036031: eAPP - LMP Sum Assured displayed not correct [#1066](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1066)
* 0035588: Eapp:Personal Details:Address finder issue [#1065](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1065)
* UAT (Plan Detail Issue): 35907, 35909, 35910, 35722, 35863, 35721, 35864, 35791, 35676, 35763, 35759, 35764, 35765, 35742, 35741, 35690, 35692, 35675, 35813, 36030, 36045, 36044, 36048, 36047, 35575, 35577 [#1064](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1064)
* NA flow issues: 35041, 35057, 35063, 35205 [#1052](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1052)
* Income Protection issues: 35881, 36006, 35042, 35046 [#1043](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1043)
* Retirement Plannings issues: 35208, 35233, 35211, 35809, 35217 [#1045](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1045)
* Critical Illness issues: 35061 [#1058](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1058)
* other need issues: 35239, 35241, 35242 [#1062](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1062)
* ROI: 35726 [#1055](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1055)
* 0036081: eBI_Shield_Names in PS page 1 are too close and not aligned [#1060](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1060)
* Income Protection: 35042, 35572, 35899, 35897, 36006, 35881 [#1042](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1042)
* 0036058: Eapp:Japanese declaration form missing for ist and 3rd party [#1053](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1053)
* 0035611: EApp:Supporting Docs:Size of the file is shown as 0 KB [#1057](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1057)
* 0036029: eApp: Supporting doucment for other documents when same type is selected document type getting removed from listeApp: Supporting [#1056](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1056)
* UAT 35550 [#1050](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1050)
* [UAT:35046] iOS | Release 2 | Needs Analysis | <Medium> Errors for Years income required, Total liabilities, Total required and values of assets (Income Protection) [#703](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/703)
* UAT 35558 [#1048](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1048)
* UAT 35557 [#1047](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1047)
* 35042, 35572, 35899 and 35897 [#1042](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1042)
* UAT 35824 [#1041](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1041)
* UAT 35811 [#1039](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1039)

### Features
* Sprint 4 | CR126B | Other changes | Risk Profile Assessment and Non-product UI changes [#976](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/976)
* Sprint 4 | CR126A | Other changes | FNA report - ROP and Shield and non-shield product changes [#975](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/975)
* Sprint 4 | BA BAA | BandAid | CR133 | PDPA Changes submission [#824](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/824)
* Invalidation Rule for EAPP [#387](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/387)
* Invalidation Rule for BI [#386](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/386)

# v1.0.0 - b158
### Defeats
* UAT 35553 [#1037](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1037)
* UAT 35560 [#1036](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1036)
* UAT 35544 [#1034](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1034)
* UAT 35540 [#1033](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1033)
* UAT 35836 [#1032](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1032)
* [UAT:35061] IOS | Release 2 | Needs | User is allow to enter more than 10 digits for Cost of medical treatment (Critical illness) [#668](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/668)
* [UAT:35048] iOS | Release 2 | Needs Analysis | <Medium> Errors for For Final expenses and other funding needs (Income Protection) [#704](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/704)
* 36033, 35878, 35829, 35921, 35048, 35061 and 36055 [#1026](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1026)
* 35805: Eapp:All products :No asterisk symbol for Initial payment method [#1031](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1031)
* [UAT:35072] iOS | Release 2 | Needs Analysis | <Medium> Incorrect error message and the value should be auto-populated to 50k if less than 50k (Personal Accident) [#709](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/709)
* 35707: Eapp:LMP Payment page-Backdating premium [#1030](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1030)
* 35762: All products: Pay button is present for AXS/SAM and checque or cashier order [#1029](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1029)
* 35948: eApp: App crashes in signature page in BI signature section [#1028](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1028)
* 0035603: eApp: Insurability Info -Family history question [#1022](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1022)
* 36059: eApp: Shield: Supporting docs section is not accessible [#1027](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1027)
* UAT 35934 [#1023](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1023)
* UAT 35919 [#1021](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1021)
* UAT 35632 [#1020](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1020)
* UAT 35710 [#1019](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1019)
* 36036: Unable to scroll down where there is more than one BI [#1017](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1017)
* UAT 35730 [#1002](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1002)
* [UAT:35261] iOS | Release 2 | Quick Quote | AXA Shield | <Medium> Blank screen was displayed when selecting "Complete Profile" in Quotation page [#737](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/737)

# v1.0.0 - b157
Note: this build is only for AXA testing submission

# v1.0.0 - b156
### Defects
* UAT 36027: eFNA - Benefits and Limitations : Premium Waiver UN Rider [#1015](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1015)
* [UAT:35072] iOS | Release 2 | Needs Analysis | <Medium> Incorrect error message and the value should be auto-populated to 50k if less than 50k (Personal Accident) [#709](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/709)
* Band Aid when Take Photo is clicked in Supporting document and Cancel button is clicked , Photo still uploaded Description [#1016](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1016)
* CK Oct18 issues [#957](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/957)
* Band Aid when Photo uploaded and clicked on uploaded photo,photo looks blurred [#994](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/994)
* 35926: App crashes at need prioritization page randomly [#995](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/995)
* 35835: Risk profile become incomplete after user revisit need page. [#1014](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1014)
* UAT 35848 [#1013](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1013)
* 35854: Eapp:Shield :Personal Details Proposer is LA is is not populated correctly [#1006](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1006)
* 35855: Eapp:Shield :Country of employer field is not required in Personal Details section [#1000](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1000)
* 35734, 35965: eApp: 3rd party Personal details not correct for all the products [#953](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/953)
* [UAT:35307] | iOS | Release 2 | Products | <Medium> Alignment Issue in UI Screen for ALL Products [#717](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/717)
* 35880 Error message change [#1007](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1007)
* 35879: eApp:All products ::Supporting Docs:User is able to upload more than 8 docs under each section [#1011](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1011)

### Features
* Sprint 4 | AWT | CR122 CR110A | Fund Changes and Revised AWT BI template and change in fund name of APEF to ASPEF [#826](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/826)
* Sprint 4 | CR126B | Other changes | Risk Profile Assessment and Non-product UI changes [#976](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/976)
* Sprint 4 | CR122 | Other changes | Fund Structure and changes for ILP [#974](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/974)
* Sprint 4 | CR149 | Changes to Shield UW medical Qn 1 [#1012](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1012)

# v1.0.0 - b155
### Defects
* 35942: EFNA - Disability benefit and Critical Illness Benefit Defects [#993](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/993)
* 35928: Personal Accident Needs Analysis [#992](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/992)
* 36010: App crash when user proceed back to financial evaluation [#1008](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1008)
* 35971: After product type is reset, Risk profile is not reset and shown as incomplete [#998](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/998)
* 35943, 35951, 35964, 35072 and 35972 [#996](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/996)
* [UAT:35072] iOS | Release 2 | Needs Analysis | <Medium> Incorrect error message and the value should be auto-populated to 50k if less than 50k (Personal Accident) [#709](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/709)
* 35768: Band Aid not able to view Policy illustration and E-App form after signature [#1010](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1010)
* 35761: RHP :Error message displayed when the user goes from the payment page back to Signature page [#1009](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1009)
* IOS | Release 2 | Applicants and dependants | [Severe] Crashed in Needs Analysis when spouse as dependent [#979](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/979)
* UAT 35730 [#1002](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/1002)
* CK Oct 17 issues [#951](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/951)

# v1.0.0 - b154
### Defects
* [UAT: 34954, 34979] Sprint 4 | LMP | CR129 CR131 CR122 CR123 CR139 CR140 | Revise Benefit Limitation text, update Product Summary and Removal of residency loading for China – major cities [#829](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/829)
* 35880: eApp:All products ::Supporting Docs:User is able to upload more than 3 mb [#997](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/997)
* 35926: App crashes at need prioritization page randomly [#995](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/995)
* 35872: In Payment Page for AWT Top-up Premium is showing under Premium Details when Top-Premium is not selected [#985](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/985)
* [UAT: 34954, 34979] | iOS | Release 1 | Quick Quote | <Medium> LMP - Critical Illness PremiumEraser Premium is calculated incorrectly [#565](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/565)

### Features
* Sprint 4 | RHP | CR134 CR137 | update calculation formula, remove SRS payment and revise limitation text for 3rd party policy [#827](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/827)


# v1.0.0 - b153
### Defects
* 35932: eApp: App crashes after Needs Analysis [#991](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/991)
* UAT 35547 [#989](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/989)
* 35803: Eapp:Payment:GIRO check box must be selected by default and non editable for monthly  [#988](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/988)
* UAT 35769 [#987](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/987)
* UAT 35614 [#986](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/986)

# v1.0.0 - b152
### Defects
* [UAT:35827] eBI - Pulsar - Issues with Pulsar[#984](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/984)
* [UAT:35832] eBI -SavvySaver - Plan codes not appearing [#980](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/980)
* [UAT:35891] eBI | LMP |Campaign Closure | Campaign Note still appearing in PI [#982](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/982)
* [UAT: 35100, 35108, 35110, 35113, 35114] iOS | Release 2 | Needs CKA | <High> Needs - Problems in CKA section [#653](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/653)
* eApp:All products ::Supporting Docs:Type of pass as Social Visit Pass [#952](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/952)
* eApp:All products ::Supporting Docs:Type of pass as LTVP [#955](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/955)
* CK Oct18 issues  [#957](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/957)
* iOS | Release 2 | AWT | [High] Product image shown blank, issue about Options - "Regular Withdrawal", PS and FIB tab at proposal page keep looping when clicked.  [#838](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/838)

### Features
*  Sprint 4 | BA BAA | BandAid | CR133 | PDPA Changes submission [#824](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/824)

# v1.0.0 - b151
### Defects
* UAT 35561 [#972](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/972)
* UAT 35591 [#851](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/851)
* UAT 35792 [#971](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/971)
* UAT 35869 [#970](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/970)
* UAT 35856 [#969](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/969)
* UAT 35851 [#968](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/968)
* UAT 35767 [#967](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/967)
* UAT 35538 [#966](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/966)
* UAT 35789 [#965](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/965)
* UAT 35808 [#964](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/964)
* UAT 35804 [#963](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/963)
* UAT 35631 [#962](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/962)
* UAT 35556 [#961](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/961)
* UAT 35716 [#960](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/960)
* UAT 35744 [#959](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/959)
* UAT 35629 [#958](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/958)
* [UAT:35695] eBI - AWT - Regular withdrawal tick box defect [#973](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/973)

### Features
* Sprint 4 | TPX TPPX | Term Protector/ Prime | CR129 and CR123 | Revised campaign details and remove residential loading on CI coverage for China major cities [#678](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/678)

# v1.0.0 - b150
### Defects
* UAT 35912 [#954](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/954)
* UAT 35807 [#944](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/944)
* 35734: eApp: 3rd party Personal details not correct for all the products [#953](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/953)
* eApp:All products ::Supporting Docs:Mainland Proposer's Mainland China Visitor Declaration Form [#950](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/950)
* [UAT:35685] IOS | Release 2 | BI | <Medium>| No message prompt when deleting BI. [#894](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/894)
* eApp:All products ::Supporting Docs:Type of pass as Social Visit Pass [#952](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/952)
* CK Oct 17 issues [#951](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/951)

# v1.0.0 - b149
### Defects
* 35573: eApp: Payment : Telegraphic Transfer scrolling and date issue [#949](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/949)
* [35659] iOS | Release 3 | eapp - App page | <Medium> Design of the Current Application page [#849](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/849)
* [UAT:35229] iOS | Release 2 | Needs | FNA | <Medium> Issues about FNA Summary [#762](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/762)
* 35713: eApp: Term: Payment page display error for currency = USD [#945](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/945)
* 35728: eApp: Term: Payment page display error for currency = AUD [#946](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/946)
* 35870: In Payment Page for PULSAR and AWT when Payment Currency is USD, Currency symbol showing wrongly [#948](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/948)
* 35573: eApp: Payment : Telegraphic Transfer scrolling and date issue [#949](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/949)
* [UAT: 35276] iOS | Release 2 | Needs - Priority | <High> Priority tab not showing needs done for Child. [#672](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/672)
* 35733: Eapp:All products :Monthly Allowance is not displayed if selected as zero [#933](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/933)
* 35854: Eapp:Shield :Personal Details Proposer is LA is is not populated correctly [#947](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/947)
* [UAT:35244] iOS | Release 2 | Needs Analysis | Other needs [Medium] Issue about Surplus value not auto populate, field line, and the alignment for PROTECTION button [#750](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/750)
* [UAT:35241] iOS | Release 2 | Needs Analysis | Other needs | [Medium] Issue about allows alphabets for "I would like to plan for*", Selection for Type of Needs and Asterisk for Estimated maturity field [#752](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/752)
* iOS | Release 2 | Product Page | <severe> app cannot go beyond Products page [#809](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/809)

# v1.0.0 - b148
### Defects
* UAT 35798 [#943](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/943)
* UAT 35797 [#942](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/942)
* UAT 35810 [#941](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/941)
* UAT 35800 [#940](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/940)
* UAT 35567 [#939](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/939)
* 35624: eApp: Cannot generate 3rd party cases [#938](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/938)
* UAT 35861 [#937](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/937)
* UAT 35799 [#936](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/936)
* 35814: Products | smoker status of CP at product page cannot get thru [#935](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/935)
* 35820: Data Sync | Agent's Sequence number is not correctly assigned - work around [#934](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/934)
* 35725: eApp: Term: Payment page display error for currency = EUR [#924](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/924)
* 35719: eApp: Term: Payment page display error for currency = GBP [#923](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/923)
* 35721: eApp: Savvy Saver Plan details not correct [#922](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/922)
* 35720: eApp:FS /FP:RSP details are shown in eapp form even if not selected in BI [#921](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/921)
* 35714: eApp: Rider Value should not be shown for Premium Eraser Total,Smart Payer Premium Eraser ,Smart Payer Premium Earser Plus [#920](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/920)
* 35743: Plan Details :LMP:Plan details section issues [#919](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/919)

# v1.0.0 - b147
### Defects
* [UAT:35206] IOS | Release 2 | Needs - ROI | <Medium> Needs Analysis_ROI (Default value wrong ,extra text and asterisk symbol missing) [#786](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/786)
* [UAT:35000] IOS | Release 2 | Applicants and dependants | [High] No validation when relationship is changed from Spouse to non-Spouse [#500](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/500)

# v1.0.0 - b146
### Defects
* [UAT: 35100, 35108, 35110, 35113, 35114] iOS | Release 2 | Needs CKA | <High> Needs - Problems in CKA section [#653](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/653)
* 35038: Incorrect text in needs page and LAs name not shown for each selection [#925](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/925)
* 35777: Eapp:All products :Personal details character count [#918](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/918)
* UAT Bugs [#916](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/916)
* 35654: Different font size in email content and missing logo  [#917](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/917)
* UAT: 35780: MYSELF AND SPOUSE options should be selected after users click on done. [#914](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/914)

# v1.0.0 - b145
### Defects
* [UAT:35158] eBI | STP | All Products | Waiver of Premium Riders has “S$” instead of “-“ in sum assured in the application page [#901](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/901)
* [35659] iOS | Release 3 | eapp - App page | <Medium> Design of the Current Application page [#849](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/849)
* 35657: eFNA : Unable to upload Agent Profile Picture and it showing blank. [#915](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/915)
* iOS | Release 2 | Client profile | [Medium] Cannot pass Smoking Status validation when completing profile in Products [#867](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/867)
* [UAT:35179] iOS | Release 2 | Needs | PDA | <Medium> Text and the Stepper should be aligned with web [#787](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/787)
* [UAT:35685] IOS | Release 2 | BI | <Medium>| No message prompt when deleting BI. [#894](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/894)
* [UAT: 35568] iOS | Release 3 | eApp: signature save button is invisible [#912](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/912)
* [UAT: 35680] iOS | Release 3 | eApp: Signature fields to remove the word "LOGO" [#910](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/910)
* iOS | Release 2 | FNA | < urgent > Random crashing [#880](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/880)
* [UAT:35150] iOS | Release 2 | Applications - Recommendations | <Medium> Missing asterisk and tooltip of mandatory field for all question under ROP and Incorrect text for Basic of replacement/switch [#702](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/702)

# v1.0.0 - b144
Note: Build again

# v1.0.0 - b143
### Defects
* User is able to enter more than 3000 characters for basis of policy replacement [#700](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/700)
* 35593: Missing text when there a surplus in priority page [#911](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/911)
* 35649: eApp: Supporting doc page display error [#906](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/906)
* 35566: Eapp:Supporting Docs- Other document section-Unable to add documents [#905](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/905)
* 35670: eApp:All products ::Supporting Docs:3rd party payor [#904](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/904)
* 35691: eApp:All products ::Supporting Docs:Mainland Proposer&#39;s Mainland China Visitor Declaration Form [#903](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/903)
* eApp: Term: Plan Details section is incorrect for Basic Plan "Renewable" [#902](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/902)
* iOS | Release 3 | eapp - personal detail | <Medium> Postal code not working [#847](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/847)
* [UAT:35684] eBI : AWT_The product image for AWT is not appearing [#899](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/899)
* [UAT:35688] eBI - AWT - Quotation page & template issues [#900](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/900)

### Features
* Sprint 4 | RHP | CR134 CR137 | update calculation formula, remove SRS payment and revise limitation text for 3rd party policy [#827](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/827)
* Sprint 4 | Shield | CR126A CR133 | FNA report and PDPA Changes submission [#825](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/825)
* [UAT: 34954, 34979] Sprint 4 | LMP | CR129 CR131 CR122 CR123 CR139 CR140 | Revise Benefit Limitation text, update Product Summary and Removal of residency loading for China – major cities [#829](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/829)

# v1.0.0 - b142
### Defects
* Unknown dialog pop up when tapping area outside INCOMPLETE [#833](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/833)
* Payment : Telegraphic Transfer scrolling and date issue [#877](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/877)
* App crashes on click of Supporting documents [#876](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/876)
* Eapp:Han Yu Pin Yin Name label is missing in personal details page [#875](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/875)
* eApp: Term: Plan Details section is incorrect for Basic Plan "To Age" [#871](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/871)
* Eapp:FS /FP:Initial RSP premium and Initial Top UP premium are displayed below Initial Total Premium page [#864](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/864)

Note:
* eab-web-api tag: [1.0.0-b141](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/eab-web-api/tags/1.0.0-b141)
* eab-core-api tag: [1.0.0-b138](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/eab-core-api/tags/1.0.0-b138)
* AXA_SG_APP_web_service tag: [1.0.0.7](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/AXA_SG_APP_web_service/tags/1.0.0.7)

# v1.0.0 - b141
### Defects
* UAT: 0035701: eApp: Term: Juvenile qn c Mandatory field validation defect  [#892](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/892)
* UAT: 0035645: eApp: Term: Juvenile qn d defect [#890](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/890)
* UAT: 0035641: eApp: Term: Juvenile qn a defect  [#889](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/889)
* UAT: 0035643: eApp: Term: Juvenile qn b defect [#888](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/888)
* UAT: 0035644: eApp: Term: Juvenile qn c defect [#887](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/887)
* UAT: 0035697: eApp: Term: Juvenile qn a Mandatory field validation defect [#886](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/886)
* UAT: 0035702: eApp: Term: Juvenile qn d mandatory validation defect [#885](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/885)
* UAT: 0035703: eApp: Term: Female UW qn a defect [#884](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/884)
* UAT: 0035706: eApp: Term: Female UW qn c defect [#883](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/883)
* UAT: 0035705: eApp: Term: Female UW qn b defect [#882](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/882)
* UAT 0035700: eApp: Term: Juvenile qn b Mandatory field validation defect [#881](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/881)
* UAT: 0035556: Eapp:PEP table issues in the declaration page [#873](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/873)
* UAT 35594 [#860](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/860)
* UAT: 0035561: Eapp:Fields missing in the reflexive table of FATCA [#874](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/874)
* UAT: 0035629: Eapp:All products :Medical and health question g is displayed for all ages [#872](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/872)
* Issue #35409:Unable to login after logging out [#865](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/865)

### Features
* Sprint 4 | ASIM ASP ASG ASH | Shield | CR119 | Shield Repricing [#674](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/674)
* Sprint 4 | FSP FSX | Flexi series | CR110A | Revised PHS for PAEQ and INDF and updated FIB [#804](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/804)
* Sprint 4 | AWT | CR122 CR110A | Fund Changes and Revised AWT BI template and change in fund name of APEF to ASPEF [#826](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/826)
* Sprint 4 | PUL | Pulsar | CR131 CR122 CR110A | Revise benefit text, add funds and change fund name [#828](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/828)

Note:
* eab-web-api tag: [1.0.0-b141](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/eab-web-api/tags/1.0.0-b141)
* eab-core-api tag: [1.0.0-b138](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/eab-core-api/tags/1.0.0-b138)
* AXA_SG_APP_web_service tag: [1.0.0.7](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/AXA_SG_APP_web_service/tags/1.0.0.7)

# v1.0.0 - b140
### Defects
* Crashed Issue at Needs Financial Evaluation stage. [#866](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/866)
* eApp: App crashes on click of Plan details section. [#863](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/863)
* eApp: Term: Plan Details section is incorrect for Basic Plan "Renewable" [#862](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/862)
* Eapp:LMP::Backdating premium is not rounded off [#861](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/861)
* "myself and spouse" is wrongly marked [#810](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/810)
* RHP - Issues with Acknowledgement Pages [#732](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/732)

Note:
* eab-web-api tag: [1.0.0-b140](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/eab-web-api/tags/1.0.0-b140)
* eab-core-api tag: [1.0.0-b138](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/eab-core-api/tags/1.0.0-b138)
* AXA_SG_APP_web_service tag: [1.0.0.6](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/AXA_SG_APP_web_service/tags/1.0.0.6)

# v1.0.0 - b139
### Defects
* For bundle case, after completed Client Choice and click "Apply", the page keep looping and cannot proceed to Application [#837](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/837)
* UAT 35594 [#860](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/860)
* UAT 35597 [#859](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/859)
* UAT 35592 [#858](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/858)
* UAT 35701 [#857](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/857)
* UAT 35599 [#856](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/856)
* UAT 35595 [#855](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/855)
* UAT 35548 [#854](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/854)
* UAT 35554 [#853](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/853)
* UAT 35645 [#852](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/852)
* UAT 35591 [#851](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/851)
* UAT 35631 [#850](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/850)
* UAT 35664 [#845](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/845)
* Unable to see all clients when clicking "View All" button  [#797](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/797)
* After completed Product Type and proceed to CKA or Risk Assessment, blank page/crash occur [#831](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/831)

Note:
* eab-web-api tag: [1.0.0-b139](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/eab-web-api/tags/1.0.0-b139)
* eab-core-api tag: [1.0.0-b138](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/eab-core-api/tags/1.0.0-b138)
* AXA_SG_APP_web_service tag: [1.0.0.6](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/AXA_SG_APP_web_service/tags/1.0.0.6)

# v1.0.0 - b138
### Defects
* Savvy Saver cover page duplicate title and overlapping issues [#811](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/811)
* UAT: 35555 [#844](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/844)
* UAT: 35600 [#843](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/843)
* UAT: 35599 [#842](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/842)
* UAT: 35615 [#841](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/841)
* UAT: 35582 [#840](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/840)

### Features
* Revise benefit text, add funds and change fund name [#828](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/828)
* Revise Benefit Limitation text, update Product Summary and Removal of residency loading for China – major cities [#829](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/829)
* Remove residential loading on CI coverage for China major cities [#805](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/805)
* Fund Changes and Revised AWT BI template and change in fund name of APEF to ASPEF [#826](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/826)

Note:
* eab-web-api tag: [1.0.0-b138](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/eab-web-api/tags/1.0.0-b138)
* eab-core-api tag: [1.0.0-b138](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/eab-core-api/tags/1.0.0-b138)
* AXA_SG_APP_web_service tag: [1.0.0.6](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/AXA_SG_APP_web_service/tags/1.0.0.6)

# v1.0.0 - b137
### Defects
* User allow to go into quotation page even when product type selected is whole life/endowment plan [#656](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/656)
* Riders list is not getting refreshed [#712](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/712)
* RHP - Issues with Acknowledgement Pages [#732](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/732)
* Not able to Proceed Need Analysis for Proposer as Spouse and Child showing after deselection [#669](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/669)
* UAT: 35618 [#836](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/836)

### Feature
* Revised PHS for PAEQ and INDF and updated FIB [#804](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/804)

# v1.0.0 - b136
### Feature
* Fund Changes and Revised AWT BI template and change in fund name of APEF to ASPEF [#826](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/826)

# v1.0.0 - b135
### Defects
* Signature Block appears twice in last page of Cover Page document [#735](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/735)
* Issues for AWT PI  [#683](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/683)
* Needs - Problems in CKA section [#653](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/653)
* Missing repeatable header for Surrender Value table when it is over 2 pages [#690](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/690)
* Show 'tick' and 'next' when product type is complete [#701](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/701)
* Showing INCOMPLETE for Risk Profile when completed and not able to proceed Risk Profile from CKA [#758](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/758)
* After user delete the BI, "please add choice for recommendation" is showing [#692](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/692)

# v1.0.0 - b134
### Defects
* Needs - problems in Risk Profile [#655](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/655)
* Retirement Plan Asterisk missing, Order of Name in the list wrong and last line missing [#583](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/583)
* Cannot find profile after completed the profile in FNA [#815](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/815)
* Blank screen was displayed when selecting "Complete Profile" in Quotation page [#737](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/737)
* Page turn blank when Flexi Saver is clicked in either Quick Quote or straight through Product page [#799](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/799)

### Features
* Shield Repricing[#674](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/674)

# v1.0.0 - b133
### Defects
* Warning Message for risk profile assessment and CKA is not available  [#779](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/779)
* Unable to see all clients when clicking "View All" button [#797](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/797)
* After update to new versions, all created clients disappeared [#802](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/802)
* Text and the Stepper should be aligned with web [#787](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/787)
* Refresh issue for auto calculation of "Premium" [#729](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/729)
* Mandatory fields to be asterisked and indicated with a message, in red ,as per EASE web [#801](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/801)

Note:
eab-web-api tag: [1.0.0-b133](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/eab-web-api/tags/1.0.0-b133)
eab-core-api tag: [1.0.0-b125](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/eab-core-api/tags/1.0.0-b125)

# v1.0.0 - b132
### Defects
* UT Errors in task#83 [#632](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/632)
* "ASIM" is getting displayed after selecting the Basic Plan from the drop down list [770](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/770)
* Not able to Proceed Need Analysis for Proposer as Spouse and Child showing after deselection [#669](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/669)
* UT Errors in task#36 [#535](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/535)
* Incorrect status when completed or incomplete all section [#749](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/749)
* Green tick is shown but user had not provided any reason for Retirement planning [#759](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/759)
* "First year Income required" and "Total amount required" is wrong [#768](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/768)
* After user proceed to spouse tab, myself tab green tick is disable. When user proceed back, the reason entered previously is blank [#757](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/757)
* Errors in task#76 Retirement [#626](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/626)
* Financial Evaluation show as completed but user had not review spouse and child section [#789](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/789)

Note:
eab-web-api tag: [1.0.0-b132](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/eab-web-api/tags/1.0.0-b132)
eab-core-api tag: [1.0.0-b125](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/eab-core-api/tags/1.0.0-b125)

# v1.0.0 - b130
### Defects
* No validation when relationship is changed from Spouse to non-Spouse [#500](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/500)
* Shield is missing in product list in normal quote after complete FNA [#633](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/633)
* Needs - Problems in CKA section [#653](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/653)

Note:
eab-web-api tag: [1.0.0-b130](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/eab-web-api/tags/1.0.0-b130)
eab-core-api tag: [1.0.0-b125](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/eab-core-api/tags/1.0.0-b125)

Note: Need test login on this build

# v1.0.0 - b129
### Defects
* If values are entered for sum assured before “$” appears in Quotation Page, it will not be register [721](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/721)
* User is able to enter more than 3000 characters for basis of policy replacement [#700](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/700)
* Max Sum Assured difference between IOS and Web EASE [#741](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/741)

### Features
* Revised Benefit and Limitation text and FNA report [#803](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/803)
* Closure of Inspire DUO [#806](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/806)

Note:
eab-web-api tag: [1.0.0-b129](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/eab-web-api/tags/1.0.0-b129)
eab-core-api tag: [1.0.0-b125](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/eab-core-api/tags/1.0.0-b125)

# v1.0.0 - b128
### Defects
* Issues for Pulsar PI [#685](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/685)
* BI Template issues [#723](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/723)
* Messy overlapping of sentences & words over the footer, incorrect font size, missing Plan Codes [#739](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/739)
* Priority tab not showing needs done for Child. [#672](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/672)
* ROI is limited at 0.5% [#798](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/798)
* Unable to proceed at Needs Analysis (Income Protection) [#713](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/713)
* No decimal place shown for insurance premium and total monthly expenses, missing line and colour not align with web [#775](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/775)
* Showing INCOMPLETE for Risk Profile when completed and not able to proceed Risk Profile from CKA [#758](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/758)
* Assessed risk profile is no changing after the user change the options [#746](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/746)
* Issues about red note and Outcome message [#776](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/776)
* Risk profile not show in green and extra sentence after assessed risk profile [#691](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/691)
* UT Errors in task#83 [#632](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/632)
* Errors in task #74 [#635](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/635)
* Page turn blank when Flexi Saver is clicked in either Quick Quote or straight through Product page [#799](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/799)
* Priority tab is disabled when mandatory fills are given in Analysis [#654](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/654)

Note:
eab-web-api tag: [1.0.0-b128](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/eab-web-api/tags/1.0.0-b128)
eab-core-api tag: [1.0.0-b125](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/eab-core-api/tags/1.0.0-b125)

# v1.0.0 - b127
### Defects
* spouse annual insurance premium does not get reflected [#784](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/784)
* CKA heading and Outcome is wrong [#760](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/760)
* Incorrect spelling for Net worth and cash flow [#705](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/705)

Note:
eab-web-api tag: [1.0.0-b127](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/eab-web-api/tags/1.0.0-b127)
eab-core-api tag: [1.0.0-b125](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/eab-core-api/tags/1.0.0-b125)

# v1.0.0 - b125 
### Defects
* Warning Message for risk profile assessment and CKA is not available [#779](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/779)
* Issues with PI Template [#716](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/716)
* Flexi Saver - blank page [#697](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/697)
* Errors in task#239 [#550](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/550)

### Features
* Change image in app switcher view [#104](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/104)

# v1.0.0 - b124
### Defects 
* subtitle and dash issues under recommendations section [#699](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/699)
* Missing asterisk and tooltip of mandatory field for all question under ROP and Incorrect text for Basic of replacement/switch [#702](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/702)
* The text of Mutual Fund/Direct investment not correct and ASSETS and LIABILITIES position align with web [#774](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/774)
* Incorrect wordings of "Disability benefit" in Needs tab [#792](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/792)
* Missing Dollar sign and Point 11 [#772](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/772)
* Incorrect landing page when user select risk profile [#764](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/764)
* There is no header "Confirm" for the warning message [#747](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/747)
* Issues in IOS offline quotation page is not as per EASE Web [#742](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/742)
* Issues regarding format and logic of Consent Notices tab [#740](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/740)
* Unable to proceed at Needs Analysis [#713](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/713)
* Incorrect fills and layout for acceptance page [#706](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/706)
* Client's Accompaniment wrongly get ticked [#693](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/693)
* Errors in task #74 [#635](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/635)
* Errors in task#239 [#550](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/550)


# v1.0.0 - b122
### Defects
* Blank screen was displayed when selecting "Complete Profile" in Quotation page [#737](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/737)
* "Existing yearly disability benefit" not auto populating from FNA, "years of income" showing 0 instead of a dash [#727](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/727)
* Missing Plan Code at Page 1 in PI [#725](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/725)
* Needs - Problems in CKA section [#653](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/653)
* Outcome note about not meeting CKA criteria [#630](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/630)
* There is no S$ & the values should be in 2 decimal places [#748](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/748)
* eBI - AWT - Regular Withdrawal Defect [#673](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/673)
* Tooltip and alignment of planning for children’s headstart [#640](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/640)

# v1.0.0 - b121
Note: this build is only for demo

# v1.0.0 - b120
Note: this build is only for demo

# v1.0.0 - b119
### Features
* Upload and download files [#107](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/107)

# v1.0.0 - b118
### Defects
* Wrong cash flow calculation under spouse [#718](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/718)
* UT Errors in task#82 [#564](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/564)
* "To Age" Rider Guaranteed Survival Payout is not ticked in the Select Riders list [#719](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/719)
* Upon deleting the rider, blank screen was generated [#722](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/722)
* Incorrect order of needs [#676](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/676)
* For Product type page, no hint text "this field is required" when no product selected [#763](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/763)
* App crashes upon clicking "Next" at Personal Data Acknowledgement [#714](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/714)
* Issues regarding wordings about client's accompanment section [#743](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/743)
* Issues with Client Profile [#726](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/726)
* "*" sign missing for validation message shown for existing insurance portfolio and annual insurance premiums [#707](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/707)

Note:
eab-web-api tag: [1.0.0-b118](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/eab-web-api/tags/1.0.0-b118)
eab-core-api tag: [1.0.0-b115](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/eab-core-api/tags/1.0.0-b115)

# v1.0.0 - b117
Note: this build is only for testing activation

# v1.0.0 - b116
### Defects
* Missing asterisk sign and tooltip under budget section [#681](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/681)
* UAT issue 35096 [#35096](https://itracker.eabsystems.com/mantisbt/view.php?id=35096)

### Features
* Generate, preview and email BI, PS, PHS, Fund Booklet [#47](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/47)

Note:
eab-web-api tag: [1.0.0-b115](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/eab-web-api/tags/1.0.0-b115)
eab-core-api tag: [1.0.0-b115](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/eab-core-api/tags/1.0.0-b115)

# v1.0.0 - b115
### Defects
* Non-smoker should display as Non-Smoker in Quotation Page [#733](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/733)
* Errors in task #74 [#635](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/635)
* LMP - Missing last 6 page of Product Summary [#459](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/459)
* Unable to see proposal summary when user selects "view details" from chosen proposal [#689](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/689)
* Unable to see case info when user selects view details [#686](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/686)
* Errors in task #84 [#634](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/634)
* After user delete the BI, "please add choice for recommendation" is showing [#692](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/692)

# v1.0.0 - b114
Note: this build is only for demo

# v1.0.0 - b113
### Defects
* Errors in task#239 [#550](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/550)
* Errors in task#38 [#548](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/548)
* Errors in task#39 [#563](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/563)
* Client's Accompaniment wrongly get ticked [#693](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/693)
* Errors in task#37 [#538](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/538)
* Status in tab does not change back to incomplete when clear up a complete TI profile [#506](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/506)
* eBI_Shield_Age is displaying as current age instead of Next Birthday Age in Quotation Page [#677](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/677)
* eBI : Shield : The product image is not displaying for decline nationality & country of residence [#675](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/675)

Note:
eab-web-api tag: [1.0.0-b112](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/eab-web-api/tags/1.0.0-b112)
eab-core-api tag: [1.0.0-b112](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/eab-core-api/tags/1.0.0-b112)

# v1.0.0 - b112
### Defects
* FP/FS - "Proposer Loadings" shifts to the right side of the Options tab [#594](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/594)
* UT Errors in task#82 [#564](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/564)
* Value of Assets in Income Protection should show $0 if no values are added under assets in Financial Evaluation [#652](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/652)
* For others under assets and liabilities, the value fields is not showing and reason box does not have word limit [#646](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/646)

# v1.0.0 - b111
Note: this build is only for demo

# v1.0.0 - b110
### Defects
* No combo fields for several fields [#159](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/159)
* TP and TPP - Basic and Rider(s) Sum Assured field should not be allow to input DOTS (......) [#581](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/581)
* For other under cash flow, the others value fill is not showing [#647](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/647)
* currency symbol not shown for rider in new version v1.0.0-b083 [#598](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/598)
* Unable to enter 50 characters for Hospital and surgical (H&S) under Existing insurance portfolio. [#671](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/671)
* EarlySaver Plus and SavvySaver – Backdating function Header in UI Field(Selected Commencement Date) is missing. [#316](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/316)
* Mobile number drop down list is missing country name [#642](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/642)

Note:
eab-web-api tag: [1.0.0-b110](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/eab-web-api/tags/1.0.0-b110)
eab-core-api tag: [1.0.0-b103](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/eab-core-api/tags/1.0.0-b103)

# v1.0.0 - b109
Note: this build is only for demo

# v1.0.0 - b108
### Defects
* Search Option is not available for drop down fields in CP  [#290](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/290)
* No combo fields for several fields [#159](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/159)
* values did not get populated after pick [#531](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/531)
* Client's Accompaniment wrongly get ticked [#693](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/693)
* Priority tab is disabled when mandatory fills are given in Analysis [#654](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/654)
* eBI : Across all products - Onscreen PI, from page 2 onward Main Policy Illustration is not generated [#492](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/492)
* Blank pages are generated and Values are missing in the PI [#490](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/490)
* RHP, LMP - PI error [#450](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/450)
* Additional text on the right under acceptance section [#645](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/645)
* Errors in task#38 [#548](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/548)
* Errors in task#239 [#550](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/550)
* align Financial evaluation's fields as WEB [#521](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/521)
* When user choose nationality as Singaporean, ID number is not showing as mandatory [#658](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/658)
* When user select myself and spouse, spouse client profile pop up, but gender is not auto populated [#659](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/659)
* Trusted individual is missing [#332](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/332)

# v1.0.0 - b107
Note: This build is for testing agent activation

# v1.0.0 - b106
Note: build again to test

# v1.0.0 - b105
### Defects
* Illustration - layout issues [#458](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/458)
* occupation and industry are not cleared when employment status has changed [#285](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/285)
* FP/FS - "Proposer Loadings" shifts to the right side of the Options tab [#594](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/594)
* Unable to see fill when entering values [#649](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/649)
* Error in Trusted Individual Picker #376 [#637](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/637)
* Client Profile_mandatory fields_FNA [#613](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/613)
* TP and TPP - Basic and Rider(s) Sum Assured field should not be allow to input DOTS (......) [#581](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/581)
* Errors in task#40 Financial Evaluation [#579](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/579)

Note:
eab-web-api tag: [1.0.0-b105](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/eab-web-api/tags/1.0.0-b105)
eab-core-api tag: [1.0.0-b103](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/eab-core-api/tags/1.0.0-b103)

# v1.0.0 - b104
### Defects
* User should only be allowed to select up to only 10 dependents [#643](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/643)
* Monthly income is still highlighted in red after user enter zero [#644](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/644)
* Additional text on the right under acceptance section [#645](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/645)
* ESP/CC/SS - In quotation page, the header & values for Basic Plan and Riders should be in right alignment [#343](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/343)
* User is allow to enter more than 200 characters and no validation message (Income Protection) [#650](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/650)
* Errors in task#39 [#563](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/563)
* Landing page - difference in number of countries shown on IOS & EASE Web [#602](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/602)
* Client Profile_mandatory fields_FNA [#613](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/613)
* Errors in task#37 [#538](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/538)
* Total number of records for Country code of "mobile no." and "other no." and "country of employer" : is wrong [#582](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/582)
* Illustration - layout issues [#458](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/458)
* No Scroll bar to view all LAs and Value of assets is not showing in one row [#641](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/641)
* TP/TPP/FP/FS- Note on Quotation's Page should fit the page's width [#585](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/585)
* currency symbol not shown for rider in new version v1.0.0-b083 [#598](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/598)
* occupation and industry are not cleared when employment status has changed [#285](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/285)
* Blank pages are generated and Values are missing in the PI [#490](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/490)
* Flexi series PI - Table for Summary of Loading(s) and Supplementary PI are missing [#488](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/488)

Note:
eab-web-api tag: [1.0.0-b104](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/eab-web-api/tags/1.0.0-b104)
eab-core-api tag: [1.0.0-b103](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/eab-core-api/tags/1.0.0-b103)

# v1.0.0 - b103
### Defects
* TP Prime |The app closes for a particular scenario executed  [#546](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/546)
* Term Protector (Prime) / Term Protector - Crashed when create proposal after deleted rider [#446](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/446)
* Trusted individual is missing [#332](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/332)
* Client Profile_mandatory fields_FNA [#613](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/613)
* LMP - Multiplier Benefit and Premium should display as per EASE website [#562](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/562)
* Trusted Individual Picker #376 [#637](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/637)
* Errors in task#40 Financial Evaluation [#579](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/579)
* Amount fields | System crashed when input value after the original value removed. [#566](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/566)
* Illustration - layout issues [#458](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/458)
* occupation and industry are not cleared when employment status has changed [#285](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/285)
* ESP/CC/SS - In quotation page, the header & values for Basic Plan and Riders should be in right alignment [#343](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/343)
* At the Assets bar, when the value is too long, it overflows to the next row [#618](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/618)
* App crashes on click of "go to FNA" button [#609](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/609)
* all figures are not right justified in Plan Illustration [#603](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/603)
* Client Profile Dialog pop up with spouse' s data [#617](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/617)
* Client Profile ID shown in quick quote [#432](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/432)

### Features
* Supporting documents [#46](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/46)

# v1.0.0 - b102
Note: Build again to test

# v1.0.0 - b101
* UT Errors in task#36 [#535](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/535)
* Relationship disappeared when completing LA's profile [#607](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/607)
* LMP - Refresh issues on the Quotation Page [#561](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/561)
* non-plain english error msg in band aid rider [#604](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/604)
* Errors in task#40 Financial Evaluation [#579](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/579)
* Link family member does not work [#178](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/178)
* Spouse as dependent option displayed when "Myself and Spouse" is selected [#619](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/619)
* No validation when relationship is changed from Spouse to non-Spouse [#500](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/500)

# v1.0.0 - b100
### Defects
* Validation message still showing after all text had been remove [#477](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/477)

# v1.0.0 - b099
### Defects
* Unable to link family member whose relationship deleted before [#596](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/596)
* The app closes for a particular scenario executed [#546](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/546)
* Other Errors in task#40 Financial Evaluation [#599](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/599)
* Term Protector (Prime) / Term Protector - Crashed when create proposal after deleted rider [#446](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/446)
* Myself, Myself and Spouse labels/Input fileds become bad [#527](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/527)
* Band Aid - Policy Term for Basic Plan & Riders are not displaying in straight line [#612](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/612)
* Quick Quote History return screen empty [#611](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/611)
* Client Profile ID shown in quick quote [#432](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/432)
* Crashed when Martial status changed form "Married" to "Single' [#608](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/608)
* values did not get populated after pick [#531](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/531)
* AWT - PI Issues [#460](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/460)

### Features
* Quotation for Shield [#12](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/12)
* Trusted Individual [#131](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/131)

# v1.0.0 - b098
### Defects
* Retire Happy Plus PI | when table is too long, word overlapping occurs [#363](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/363)

# v1.0.0 - b097
### Defects
* No character limit in the "Others" place holder of Language Spoken [#337](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/337)

Note:
eab-web-api tag: [1.0.0-b097](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/eab-web-api/tags/1.0.0-b097)
eab-core-api tag: [1.0.0-b095](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/eab-core-api/tags/1.0.0-b095)

# v1.0.0 - b096
### Defects
* Not enough space between the occupation and others place holder [#487](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/487)
* Term Protector Prime - No enough space for displaying row of total premium [#600](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/600)

Note: 
eab-web-api tag: [1.0.0-b096](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/eab-web-api/tags/1.0.0-b096)
eab-core-api tag: [1.0.0-b095](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/eab-core-api/tags/1.0.0-b095)

# v1.0.0 - b095
Note: Build again to test

# v1.0.0 - b094
### Defects
* No validation when relationship is changed from Spouse to non-Spouse [#500](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/500)
* Band Aid and LMP - Enter sum assured should be spelt out in full [#597](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/597)
* No client profile dialog pop up for incomplete spouse client profile [#502](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/502)
* FP/FS - Error message should display with header “Error” (follow the web version) [#580](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/580)
* TP and TPP - Basic and Rider(s) Sum Assured field should not be allow to input DOTS (......) [#581](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/581)

# v1.0.0 - b093
### Defects
* Retire Happy Plus PI cover pages jammed together [#363](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/363)

# v1.0.0 - b092
### Features
* Client Profile | Trusted Individual Picker [#376](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/376)

Note:
eab-web-api tag: [1.0.0-b092](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/eab-web-api/tags/1.0.0-b092)
eab-core-api tag: [1.0.0-b091](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/eab-core-api/tags/1.0.0-b091)

# v1.0.0 - b091
### Defects
* Term Protector Prime - No enough space for displaying row of total premium [#600](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/600)

### Features
* Application form - Plan details [#51](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/51)
* Pre-application: Acceptance [#26](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/26)
* Pre-application: Budget [#25](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/25)
* Pre-application: Recommendations [#24](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/24)
* Applications tab [#23](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/23)
* FNA summary [#85](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/85)

# v1.0.0 - b090
### Defects
* TP and TPP - Basic and Rider(s) Sum Assured field should not be allow to input DOTS (......) [#581](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/581)

Note: 
eab-web-api tag: [1.0.0-b089](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/eab-web-api/tags/1.0.0-b089) 
eab-core-api tag: [1.0.0-b089](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/eab-core-api/tags/1.0.0-b089)

# v1.0.0 - b089
### Defects
* Other Errors in task#40 Financial Evaluation [#599](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/599)
* Illustration - layout issues [#458](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/458)
* Sum Assured for all products should display as dash "-" instead of S$0.00 in plan illustration [#577](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/577)
* max input length of Name of Employer/Business/School [#192](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/192)
* Dollar sign is missing and the dashes issues [#575](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/575)
* AWT/FP/FS/Pulsar - Fund List button should be "Select Fund(s)" instead [#576](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/576)
* In products screen, for LA "incomplete Profile" is displaying incorrectly since all the mandatory profile are filled [#574](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/574)
* remove decimal place in Illustration [#525](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/525)

# v1.0.0 - b088
### Defects
* Errors in task#40 Financial Evaluation [#579](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/579)
* LMP - Multiplier Benefit and Premium should display as per EASE website [#562](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/562)

Note:
eab-core-api tag: [1.0.0-b087](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/eab-web-api/tags/1.0.0-b087)
eab-web-api tag: [1.0.0-b088](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/eab-core-api/tags/1.0.0-b088)

# v1.0.0 - b087
### Defects
* Decline City of Residence for Thailand should not be eligible for all products except AXA Shield. [#586](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/586)
* Sum Assured for all products should display as dash "-" instead of S$0.00 in plan illustration [#577](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/577)
* LMP - Refresh issues on the Quotation Page [#561](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/561)
* Errors in task#41 [#549](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/549)
* App crashes when the illustration tab is accessed for "Pulsar" [#569](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/569)

### Features
* Local authentication (incl. failed login attempts, app password expiry and reset app password) [#22](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/22)

# v1.0.0 - b085
Note: Fix v1.0.0 build 84 can't log in issue
### Features
* Application form - Personal details [#50](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/50)

# v1.0.0 - b084
### Defects
* For Student below 18, Employment status auto changed to Student when other occupations are selected [#567](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/567)
* RHP - Unable to see the full premium value on the quotation page when it is more than a million [#572](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/572)
* Eligible Products does not update instantly after edited profile [#435](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/435)
* ESP/CC/SS - In quotation page, the header & values for Basic Plan and Riders should be in right alignment [#343](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/343)
* LMP - Unable to see the full premium value [#570](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/570)
* City/State is missing in client profile summary [#571](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/571)

### Features
* Application form - Personal details [#50](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/50)

# v1.0.0 - b083
Note: Fix can not access quotation page issue after clicking one of the products.

# v1.0.0 - b082
### Defects
* Unit # is wrong showed as "undefined" [#533](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/533)
* For Student below 18, Employment status auto changed to Student when other occupations are selected [#567](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/567)


# v1.0.0 - b081
### Defects
* RHP/TP/TPP - Single Premium is not shown in Plan Illustration tab when selected [#496](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/496)
* from client profile to FNA [#532](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/532)
* No character count limit for Others place holder of Type of Pass and monthly allowance [#339](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/339)
* City/State is missing in client profile summary [#571](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/571)

# v1.0.0 - b080
### Defects
* FP - Top-Up Allocation is showing undefined when fund's top-up allocation is 0% [#542](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/542)
* RHP/TP/TPP - Single Premium is not shown in Plan Illustration tab when selected [#496](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/496)

# v1.0.0 - b079
Note: Use hard code agent profile instead of get agent profile from web service

# v1.0.0 - b078
### Defects
* FlexiProtector/FlexiSaver - Living Accelerator Benefit rider sum assured field UI screen should display as per Web [#557](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/557)
* RHP - Unable to input 2 decimal places at Basic Premium field. [#515](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/515)
* "Name of employer" and "Country of employer" issues [#547](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/547)
* TP & TP Prime | The rider display is hidden in the ‘select riders’ pop-up [#560](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/560)
* RHP/TP/TPP - Single Premium is not shown in Plan Illustration tab when selected [#496](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/496)

# v1.0.0 - b077
### Defects
* After inputted in "ID document type", Mandatory checking is disappeared for "ID Number" [#503](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/503)
* from client profile to FNA [#532](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/532)
* SS & ESP - Refresh issue with Sum Assured and Premium [#475](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/475)

# v1.0.0 - b076
### Defects
* Update Fund List [#526](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/526)
* Contact tab of LA is selected by default after editing the Proposer's contact details [#543](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/543)
* AWT - RSP should be displayed as disabled when USD is selected [#516](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/516)
* Re-quote from history summary does not work [#556](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/556)

# v1.0.0 - b075
### Defects
* FlexiProtector/FlexiSaver - Living Accelerator Benefit rider sum assured field UI screen should display as per Web [#557](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/557)
* FP/FS/AWT/Pulsar - Error Message is not showing when you have select more than 10 funds [#544](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/544)
* FP - Top-Up Allocation is showing undefined when fund's top-up allocation is 0% [#542](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/542)
* Initial Premium display at the bottom when plan and rider details not entered should be 0.00 [#536](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/536)
* FlexiProtector/FlexiSaver/AWT - Options of Regular Withdrawal's To Age allows more than 2 digits [#539](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/539)
* Pulsar / AWT - There is no pop out message when clicking delete button to remove the fund & also select more than 100% [#486](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/486)
* Pass expiry date is showing text "Invalid date" [#559](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/559)

# v1.0.0 - b074
### Defects
* Trusted individual is missing [#332](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/332)
* FlexiProtector/FlexiSaver - Living Accelerator Benefit rider sum assured field UI screen should display as per Web [#557](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/557)

# v1.0.0 - b073
### Defects
* FlexiProtector/FlexiSaver/AWT - Options of Regular Withdrawal's To Age allows more than 2 digits [#539](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/539)
* The length of display area for Education, Industry and Occupation are too short for certain options [#162](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/162)
* The total number of records is not shown in any of the drop down list unlike web version [#255](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/255)
* max input length of Name of Employer/Business/School [#192](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/192)
* align PDA text as web [#518](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/518)
* Missing field "Name Order" [#504](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/504)
* FlexiProtector/FlexiSaver - Living Accelerator Benefit rider sum assured field UI screen should display as per Web [#557](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/557)
* No warning icon against spouse in client profile summary page if proposer's marital status is changed [#530](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/530)

# v1.0.0 - b071
### Defects
* from client profile to FNA [#532](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/532)
* Flexi Protector and Flexi Saver - when click "Create Proposal" missing pop up message or pop up error message, may turn blank and hang [#461](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/461)

### Features
* Financial evaluation [#40](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/40)

# v1.0.0 - b070
### Defects
* Occupation name for Industry - Armed Forces / Military is spelled incorrectly [#545](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/545)
* Unit # is wrong showed as "undefined" [#533](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/533)
* FP/FS - Premium values of Optional Riders should be a dash instead of 0 [#513](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/513)
* align Trusted individual's fields as WEB [#520](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/520)
* Input field behavior does not aligned with client profile [#505](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/505)
* Missing field "Name Order" [#504](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/504)
* Pulsar - Display blank page after selecting client profile instead of displaying error message [#514](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/514)
* No character count limit for Others place holder of Type of Pass and monthly allowance [#339](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/339)
* remove decimal place in Illustration [#525](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/525)

# v1.0.0 - b069
### Defects
* Name for Policy Illustration in the Proposal page is different [#517](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/517)
* Update Labels [#524](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/524)

# v1.0.0 - b068
### Defects
* Flexi Protector and Flexi Saver - when click "Create Proposal" missing pop up message or pop up error message, may turn blank and hang [#461](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/461)
* occupation and industry are not cleared when employment status has changed [#285](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/285)
* Title is not displayed in the CP summary page [#476](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/476)
* RHP - Unable to input 2 decimal places at Basic Premium field.[#515](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/515)
* LMP- Error message of SA for Rider is duplicated [#508](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/508)
* Search text is not refreshing after clicking home and click search again [#399](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/399)
* The length of display area for Education, Industry and Occupation are too short for certain options [#162](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/162)
* When "Delete Client" is clicked, system hang [#276](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/276)
* Country code issues (Label text is incorrect) [#292](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/292)
* eBI: The reports in Proposal screen lags when navigating from PI to PS & PHS to FIB.[#512](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/512)
* All Products – Basic Sum Assured/Retirement Income field should display as per FSD [#494](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/494)

# v1.0.0 - b067
### Defects
* Band Aid is missing on QQ screen [#510](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/510)

# v1.0.0 - b066
### Defects
* inputable multi-select UI component for Language field as per web version [#501](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/501)
* Values of the "Options" tab are missing a comma [#489](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/489)
* Link Family member results in blank screen [#528](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/528)
* Wrong label of relationship when relationship is "Others" [#499](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/499)
* Link family member problems [#529](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/529)
* AWT - Regular Withdrawal note is missing [#482](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/482)
* The length of display area for Education, Industry and Occupation are too short for certain options [#162](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/162)
* RHP/TP/TPP - Single Premium is not shown in Plan Illustration tab when selected [#496](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/496)

# v1.0.0 - b065
### Defects
* Unit number delete issue [#309](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/309)
* Illustration - App crash on change at "% of investment return" [#452](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/452)
* Re-quote function does not work [#449](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/449)
* RHP - Unable to input values that are more than 2 digits for Retirement Income when Lifetime is selected [#495](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/495)
* Occupation & Industry fields get reset when employment status is subsequent = full/part time [#438](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/438)
* Monthly income is resets to max value when numbers entered randomly without pause [#491](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/491)

# v1.0.0 - b064
### Defects
* Align Client's Accompaniment on multilingual as web [#519](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/519)
* All Products - Product Category header is missing in Products & Quick Quote Page [#473](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/473)
* inputable multi-select UI component for Language field as per web version [#501](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/501)

# v1.0.0 - b063
### Defects
* The fill is not showing at next row after colon [#480](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/480)
* Mobile number and other mobile number +65 is showing when user did not entered any value [#472](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/472)

### Features
* Needs - Priority (Unit test requirement) [#80](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/80)

# v1.0.0 - b062
### Defects
* Link Family member Dialog does not dismiss [#507](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/507)
* Mobile No and Other No field accepts more than 12 characters and accepts non numeric characters [#329](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/329)
* "Life MultiProtect" and "Band Aid" note on Quotation's Page should fit the page's width [#471](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/471)

### Features
* Needs tab [#36](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/36)
* Consent notices [#39](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/39)

# v1.0.0 - b061
### Defects
* SS & ESP - Refresh issue with Sum Assured and Premium [#475](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/475)
* When value is cleared off from Monthly Income/Allowance (SGD) field $ 0 should be shown by default [#419](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/419)
* Warning message "This field is required" missing for Given Name [#481](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/481)

# v1.0.0 - b060
### Defects
* Residential address is displayed in the wrong order [#478](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/478)

# v1.0.0 - b059
### Defects
* Type of pass and pass expiry date is shown by default after client profile is created just by entering given name and gender [#474](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/474)
* RHP - Duplicated error massage for SA [#463](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/463)

# v1.0.0 - b058
### Defects
* AWT / Pulsar - Selected Funds are missing in the Product Highlight Sheet [#483](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/483)
* Flexi Protector - Create Proposal Button did not enable [#493](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/493)
* Flexi Protector and Flexi Saver - No hint text when text fields of "Option" get focused [#466](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/466)
* Search text is not refreshing after clicking home and click search again [#399](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/399)

### Features
* Client's accompaniment [#38](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/38)
* FNA - Trusted Individual (Unit test requirement) [#239](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/239)

# v1.0.0 - b057
### Defects
* Issue in Quick Quote History [#443](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/443)
* missing thousand separator and dollar sign in Monthly Income/Allowance [#187](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/187)

# v1.0.0 - b056
### Defects
* Date Picker is not working for Pass Expiry Date [#479](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/479)
* Validation message still showing after all text had been remove [#477](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/477)
* The 4 Name fields should be restricted to 30 characters [#293](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/293)
* Flexi Protector and Flexi Saver - At Proposal page, "Product Summary" tab is shown blank [#462](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/462)

# v1.0.0 - b055
### Defects
* Inspire DUO - missing, wrong pop up messages and logic about reset field [#442](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/442)
* Inspire DUO - missing hint text and wrong tooltip [#440](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/440)
* Flexi Protector and Flexi Saver - Policy Option - Missing tool tips in "New Basic Sum Assured" [#465](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/465)
* Term series and Flexi series - Product summary for riders are missing [#468](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/468)
* When "OTHERS (Note: Application shall subject to underwriting) for occupation the other's text field need not be mandatory [#405](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/405)

# v1.0.0 - b054
### Defects
* Breakdown is not showing the full list of chosen riders and is unable to scroll up and down [#266](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/266)
* Illustration - No respond when tab the row in the table [#456](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/456)

# v1.0.0 - b053
### Defects
* Flexi series - Show blank when product is clicked [#470](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/470)
* Validation message still showing after all text had been remove [#477](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/477)

# v1.0.0 - b052
### Defects
* Monthly income is not displayed in the client profile summary if its 0 [#317](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/317)
* All Investment Link products - Backdating field should be dimmed and disable checkbox [#427](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/427)
* Search and Clear function not working in Search Page [#422](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/422)
* Issue in Quick Quote History [#443](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/443)
* LMP - screen shifting when entering SA for Critical Illness Benefit [#425](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/425)
* Confirmation message for deleting riders is missing [#430](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/430)

# v1.0.0 - b051
### Defects
* Term Protector - plan type (to age) is hanged [#426](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/426)
* Inspire DUO - only 7 digits should be allowed to input for Premium [#444](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/444)

### Features
* Needs: Customer knowledge assessment[#82](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/82)

# v1.0.0 - b050
### Defects
* RHP - Error when inputted a very big amount of number [#437](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/437)
* Cannot search profile to link after create new new profile [#433](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/433)
* Type of Pass - The field of "Others", Employment status, Language Spoken are not refreshed [#398](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/398)
* Issue of linking second family member [#434](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/434)

# v1.0.0 - b049
### Defects
* Warning message pop up even the input date is valid [#451](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/451)
* AWT - Missing Tooltip (Top-up , Recurring Single Premium and Regular Withdrawal) field. [#406](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/406)
* Client Profile ID shown in quick quote [#432](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/432)
* Quotation - No error message for invalid Sum Assured Input [#201](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/201)

# v1.0.0 - b048
### Defects
* Language spoken - Value of Others is not cleared up after uncheck and recheck Others [#365](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/365)
* Search and Clear function not working in Search Page [#422](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/422)

### Features
* Applicants and dependants [#37](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/37)

# v1.0.0 - b047
### Defects
* client profile Summary Screen field errors [#388](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/388)

# v1.0.0 - b046
### Defects
* Invalid character in drop down values of "Occupation" [#400](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/400)

# v1.0.0 - b045
### Defects
* Invalid character in drop down values of "Occupation" [#400](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/400)
* Sorting order of Client while Search option clicked in the Landing Page [#399](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/399)
* Term Protector / Term Protector Prime - Incorrect relationship between rider Advanced TPD Payout and DisabilityCash Benefit [#351](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/351)
* The drop down values in the "Occupation" are not displayed completely[#403](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/403)

# v1.0.0 - b044
### Defects
* When "OTHERS (Note: Application shall subject to underwriting) for occupation the other's text field need not be mandatory [#405](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/405)
* flexi Protector - Create Proposal does not enable [#417](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/417)
* flexi series - "% of basic plan" cannot get populated [#415](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/415)
* Wrong field name in the contact details tab [#251](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/251)
* Various problems on Life Multiproduct [#324](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/324)

### Features
* Needs: Product types [#81](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/81)

# v1.0.0 - b043
### Defects
* Agent profile label name is not aligned in a single line [#420](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/420)
* Backdating label color when disabled [#395](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/395)
* Dialog box of Backdating field is auto dismissed [#394](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/394)
* client profile Summary Screen field errors [#388](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/388)
* Type of Pass - The field of "Others" is not refreshed [#398](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/398)

# v1.0.0 - b042
### Defects
* Design of the values on the breakdown list [#314](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/314)
* Term Protector / Term Protector Prime - Cannot uncheck rider for certain riders [#350](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/350)

### Features
* Native UI Component - Sort List [#268](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/268)

# v1.0.0 - b041
### Defects
* The Order of values in the Employment Status drop down list is not in line with Web [#402](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/402)
* Client profile:Search results always shows given name first [#379](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/379)
* Client search result screen shows NRIC# is missing [#323](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/323)

### Features
* Needs: ROI (Unit test requirement) [#42](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/42)
* Needs: Needs selection (Unit test requirement) [#41](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/41)

# v1.0.0 - b040
### Defects
* CP Summary Screen issues [#361](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/361)
* AXA Wealth Treasure & Pulsar - Display logic between "Death Benefit" and "Insurance Charge"[#377](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/377)
* Family member is disappeared after complete profile in Quick Quote [#384](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/384)
* Error of creating third party quotation [#347](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/347)

# v1.0.0 - b039
### Defects
* Only product AWT displayed after complete client profile [#385](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/385)

# v1.0.0 - b038
### Defects
* Unable to key decimal values in Premium [#313](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/313)
* missing thousand separator and dollar sign in Monthly Income/Allowance [#187](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/187)
* Savvy Saver - Nationality & Residency rules display message is not appearing.[#256](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/256)
* Term Protector/Prime - Missing Payment Mode "Single Premium (SP)" when Plan Type is "Renewable"[#368](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/368)
* No place holder when Occupation is "OTHERS (Note: Application shall subject to underwriting)"[#222](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/222)
* Term Protector/Prime - Missing label text in field Occupation Class[#369](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/369)

### Features
* Tooltip [#32](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/32)

# v1.0.0 - b037
### Defects
* click product of CritiCare, Pulsar, AWT resulting in blank screen [#393](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/393)
* Selected option cannot be shown in the Industry field [#391](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/391)

# v1.0.0 - b036
### Defects
* Savvy Saver - Nationality & Residency rules display message is not appearing. [#256](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/256)
* Monthly income is not displayed in the client profile summary if its 0[#317](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/317)

# v1.0.0 - b035
### Defects
* Country code list becomes blank after scrolling down[#318](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/318)
* Language spoken disappeared.[#364](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/364)
* Client name and Currency dropdown are missing after switching to Life Assured in 3rd party case[#380](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/380)
* "Go to Profile" button does not work[#392](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/392)
* ESP : In quotation page, LA occupation, there is spacing before the comma[#342](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/342)
* Education /work information header is incomplete[#252](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/252)
* client profile Summary Screen field errors[#388](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/388)
* Cannot pass mandatory checking when Occupation is "Others"[#374](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/374)

# v1.0.0 - b034
### Defects
* Term Protector / Term Protector Prime - Cannot uncheck rider for certain riders [#350](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/350)
* Early Saver Plus - Cannot uncheck rider for certain riders [#389](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/389)
* SavvySaver,EarlySaver Plus and Band Aid - Rider(s) Delete Button function is not appearing.[#265](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/265)
* Values are missing a comma and should display 2 decimal place [#338](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/338)
* APP in busy/HUD looping[#373](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/373)
* Term Protector / Term Protector Prime - Wrong button in warning dialog box [#352](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/352)
* Term Protector/Prime - Unable to input Policy Term even Plan Type have value [#366](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/366)
* Dropdown is missing in quotation page when Proposer and Life Assured is not eligible for any products[#294](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/294)
* No thousand unit comma separator. [#207](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/207)
* Date picker issue in Backdating field [#357](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/357)
* Date Picker showing default value and missing done and cancel button in the Date Picker.[#263](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/263)
* Unable to key decimal values in Premium[#313](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/313)
* expected to prompt error msg for invalid re-quote[#218](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/218)
* eBI_AXA Early Saver Plus|Error message is not displaying when the auto-calculated Premium is below the minimum[#311](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/311)

# v1.0.0 - b033
### Defects
* Item 1 "Application and Dependents" should be "Personal Data Acknowledgement" [#247](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/247)
* Band Aid - Daily Accident Hospitalisation Income error message is not showing Description [#327](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/327)
* Postal Code issues (No error message for wrong postal code and doesn't remove the old address if postal code changed)[#297](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/297)
* No place holder when Occupation is "OTHERS (Note: Application shall subject to underwriting)" [#222](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/222)
* missing thousand separator and dollar sign in Monthly Income/Allowance [#187](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/187)
* App Hanged after delete client profile [#383](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/383)
* LMP - Malfunctioning for Payment Mode and Multiplying Factor [#304](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/304)
* Term Protecter/Term Protecter Prime - Hanged after select the prodcut [#306](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/306)
* Early Saver Plus - Optional Riders are missing[#335](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/335)
* ESP_ 3rd party riders are not displaying correctly[#330](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/330)
* eBI_AXA Early Saver Plus|Error message is not displaying when the auto-calculated Premium is below the minimum[#331](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/331)
* Term Protector/Prime - Missing label text in field Occupation Class [#369](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/369)
* Term Protector/Prime - Missing Payment Mode "Single Premium (SP)" when Plan Type is "Renewable"[#368](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/368)
* Term Protector/Prime - Missing field Indexation [#367](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/367)
* Term Protector/Prime - Error in displaying currency symbols [#370](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/370)
* "Life Assured" is shown instead of "Proposer" in 3rd Party Cases[#341](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/341)

### Features
* Quotation (incl. riders) [#2](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/2)
* UI | Date Picker missing Cancel and Done button[#358](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/358)
* Quotation - Fund allocation[#11](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/11)

# v1.0.0 - b032
### Defects
* No place holder when Occupation is "OTHERS (Note: Application shall subject to underwriting)"[#222](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/222)
* ESP : In Sum Assured for Payer PremiumEraser riders a dash should appear [#334](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/334)
* Date of birth Mandatory for Family member Client creation and (*) symbol missing in Relationship Label[#315](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/315)
* Relationship -Others field issue [#336](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/336)
* Relationship is shown as Others in the Summary page if Others is selected in CP [#331](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/331)
* occupation and industry are not cleared when employment status has changed [#285](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/285)
* When "Delete Client" is clicked, system hang[#276](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/276)
* Min and Max of Monthly Income/Allowance [#190](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/190)
* SavvySaver,EarlySaver Plus and Band Aid - Rider(s) Delete Button function is not appearing.[#265](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/265)
* SavvySaver - Sum Assured for rider Smart Payer PremiumEraser Plus should be a dash [#319](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/319)
* Band Aid - Band Aid Note is missing on quotation page[#277](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/277)
* missing fields/ wrong label on client summary screen [#171](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/171)
* Date Picker is wrongly displayed [#288](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/288)
* Date picker issue in Backdating field[#357](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/357)
* "Type of Pass" and "Pass Expiry Date" layout issue[#346](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/346)
* RHP - Sum Assured disappear when and after input[#302](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/302)
* Change wordings in quotation page [#249](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/249)

### Features
* Quotation (incl. riders)[#2](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/2)
* UI | Date Picker missing Cancel and Done button [#358](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/358)
* Quotation - Fund allocation [#11](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/11)

# v1.0.0 - b031
### Defects
* Block and Unit Number in the Contact tab accepts more characters than the length specified.[#310](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/310)
* Unable to key decimal values in Premium [#313](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/313)
* Postal Code issues (No error message for wrong postal code and doesn't remove the old address if postal code changed)[#297](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/297)
* Life Assured profile details went blank after the profile was created[#291](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/291)

# v1.0.0 - b030
### Defects
* Term Protector/Prime gets looping when creating PI [#355](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/355)
* Life MultiProtect gets looping when creating PI [#354](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/354)
* Retire Happy Plus get looping when creating PI [#353](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/353)
* Education /work information header is incomplete [#252](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/252)
* Profile picture is not updated after edit [#246](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/246)
* ID Docoument Type should not be editable when Nationality is Singapor [#233](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/233)
* The Label is showing as Delete Client [#298](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/298)
* Relationship is shown as Others in the Summary page if Others is selected in CP [#331](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/331)
* Relationship -Others field issue [#336](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/336)
* Delete button is disappeared. [#345](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/345)
* Country of employer is shown as "-" instead of N/A in the summary screen [#326](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/326)
* Singapore PR Status should not be displayed in Profile when Nationality is Singaporean [#325](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/325)
* RHP - Incorrect text of Payment Method [#305](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/305)
* Follow-up from "Fix on label, style and the function of edit family member (no exact issue related)" [#206](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/206)
* Early Saver Plus - Cannot select "Accumulated" [#226](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/226)

# v1.0.0 - b029
### Defects
* Date Picker is wrongly displayed [#288](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/288)
* Date Picker showing default value and missing done and cancel button in the Date Picker.[#263](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/263)
* Unit number delete issue [#309](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/309)
* Edit Profile done button and Postal code auto fetching Not working [#301](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/301)
* ID Document Type is reset if PR status is changed from Yes to No [#333](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/333)

# v1.0.0 - b028
### Defects
* Star symbol missing next to Singapore PR status caption [#260](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/260)
* “Done” button is shown instead of “Save” button in Family Member’s profile [#312](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/312)
* Delete button in family member profile [#202](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/202)
* Default value of DOB is wrong [#307](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/307)
* DOB field is recognized as null even there is input [#322](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/322)
* The Label is showing as Delete Client [#298](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/298)
* NO character limit for email id field and city/state field [#289](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/289)

# v1.0.0 - b027
### Defects
* “Postal code” finder as part of Client Profile requirement. [#130](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/130)
* Change wordings in quotation page [#249](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/249)
* No profile image in quotation panel [#248](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/248)
* Education and Work information must be aligned as per web [#308](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/308)
* SavvySaver - Smart PayerPremium Eraser & Smart PayerPremium Eraser Plus cannot co-exist,Only allowed one of these riders. [#274](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/274)

# v1.0.0 - b026
### Defects
* occupation and industry are not cleared when employment status has changed [#285](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/285)
* Employment status does not change when date of birth changed [#278](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/278)
* Languages Spoken is not fully displayed [#232](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/232)

# v1.0.0 - b025
### Defects
* Singapore PR status and marital status is missing in the client profile summary page [#273](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/273)
* Country of Residence should not N/A in the drop down List [#295](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/295)
* Relationship "Other" cannot be saved [#284](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/284)
* When Invalid NRIC ID or Fin No given, then cleared ,Input against Singapore directory message does not disapper [#262](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/262)
* Different product page is shown after switching between Profile and Products tab [#259](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/259)
* "Residential address" label name is missing in the contact details section [#258](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/258)
* After selecting any value from dropdown, dropdown options are not closed automatically. [#257](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/257)
* Nationality list is not updated as per the list given in CR 63,country list [#254](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/254)
* Place holder missing if ID doc type is selected as Others [#253](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/253)
* Age is not displayed in the Client profile summary page [#261](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/261)
* Issue about Backdating [#234](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/234)
* enter Postal code to auto-populate residential address is not for non-SG [#286](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/286)
* The options for “Policy Term” and “Payment Term” can only be shown when users scroll down to bottom of list [#280](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/280)

# v1.0.0 - b024
### Defects
* Incorrect SA Tooltip for CritiCare [#267](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/267)
* Contact tab is set as default when clicking on add client icon [#296](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/296)
* Singapore PR status and marital status is missing in the client profile summary page [#273](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/273)
* Date Picker is wrongly displayed [#288](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/288)

# v1.0.0 - b023
### Defects
* missing fields/ wrong label on client summary screen [#171](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/171)
* Date Picker showing default value and missing done and cancel button in the Date Picker.[#263](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/263)
* Others is populated in the client profile summary page instead of the value entered in the placeholder of others [#275](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/275)

# v1.0.0 - b022
### Defects
* SavvySaver - Smart PayerPremium Eraser & Smart PayerPremium Eraser Plus cannot co-exist,Only allowed one of these riders.[#274](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/274)
* Age is not displayed in the Client profile summary page [#261](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/261)

# v1.0.0 - b021
### Defects
* Wrong field name in the contact details tab [#251](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/251)
* Quotation - Alterations [#10](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/10)
* Languages Spoken is not fully displayed [#232](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/232)

# v1.0.0 - b020
### Defects
* Wrong Typo on placeholder [#230](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/230)
* Extra space in flat list [#210](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/210)
* Use flat list instead of picker popover for all dropdown lists [#243](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/243)
* Wrong flowing text of field Han Yu Pin Yin Name [#236](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/236)
* keypad covers input fields [#244](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/244)
* city/state should be an editable field [#172](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/172)
* Country of Residence should not have option "N/A" [#241](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/241)
* Relationship disappear after linking [#242](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/242)
* Delete button in family member profile [#202](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/202)

# v1.0.0 - b019
### Defects
* No place holder when Occupation is "OTHERS (Note: Application shall subject to underwriting)" [#222](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/222)
* Min and Max of Monthly Income/Allowance [#190](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/190)
* Benefits illustration chart and table [#48](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/48)


# v1.0.0 - b018
### Defects
* Relationship between Occupation and Industry [#160](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/160)

# v1.0.0 - b017
### Defects
* Marital Status is able to edited when the relationship is "Married" [#213](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/213)
* city/state should be an editable field [#172](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/172)

# v1.0.0 - b016
Note: Last build was fail. It has been fixed on this build. So it includes defects on last build.
### Defects
* Max date of Pass Expiry date [#209](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/209)
* Search dialog does not dismiss after linked family member [#217](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/217)
* Issues about the search panel of linking family member [#194](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/194)
* No linkage between "Country of Residence" and "City/State" [#188](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/188)
* no mandatory checking hint text for City/State [#205](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/205)
* Issue about searching clients after quitting product selection page [#224](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/224)
* Duplicated family member profile [#177](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/177)
* Client Search - Incorrect hint text in search bar [#148](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/148)
* city/state should be an editable field [#172](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/172)
* Not able to select 3rd party [#215](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/215)
* All Currencies dropdown is empty [#179](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/179)
* No error message for Spouse when marital status change from Married to Non-Married [#216](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/216)
* Error text of relationship display wrongly [#214](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/214)
* Delete button in family member profile [#202](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/202)
* The relationship is not saved after add new family memeber [#199](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/199)
* Country code issues [#157](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/157)
* Quick Quote History checkbox crash [#221](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/221)
* Occupation does not change from student to Househusband [#225](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/225)
* Issues appropriate msg for eligibility [#212](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/212)

# v1.0.0 - b015
### Defects
* Issue about searching clients after quitting product selection page [#224](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/224)
* Issues about the search panel of linking family member [#194](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/194)
* Duplicated family member profile [#177](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/177)
* Client Search - Incorrect hint text in search bar [#148](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/148)
* city/state should be an editable field [#172](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/172)
* Not able to select 3rd party [#215](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/215)
* All Currencies dropdown is empty [#179](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/179)
* No error message for Spouse when marital status change from Married to Non-Married [#216](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/216)
* Error text of relationship display wrongly [#214](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/214)
* Delete button in family member profile [#202](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/202)
* The relationship is not saved after add new family memeber [#199](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/199)
* Country code issues [#157](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/157)
* Quick Quote History checkbox crash [#221](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/221)

# v1.0.0 - b014
### Defects
* Quick Quote History Button not working [#219](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/219)

# v1.0.0 - b013
### Defects
* Dates before today should not be available for selection for Pass Expiry Date [#189](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/189)
* error message when choosing relationship as "Spouse" without Martial input is wrong [#182](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/182)
* max input length of Name of Employer/Business/School [#192](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/192)

# v1.0.0 - b012
### Defects
* Logic between employment status and Occupation & Industry [#174](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/174)
* Employment Status of client aged < 18 should be defaulted as Student [#158](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/158)

# v1.0.0 - b011
### Defects
* Employment status does not auto inputted when Occupation is in a group of certain jobs [#186](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/186)
* QQ is not functioning for Savvy Saver [#149](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/149)
* Profile Picture does not clear up after client profile dialog closed [#166](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/166)
* The length of display area for Education, Industry and Occupation are too short for certain options [#162](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/162)
* Date picker (remove time from datetime) [#122](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/122)
* Selector for Gender * (Male / Female) – default select Male [#121](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/121)

### Features
* Selector fields [#33](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/33)
* Package and access biz rules from web project [#13](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/13)
* Side Nav [#113](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/113)

# v1.0.0 - b010
### Defects
* Relationship between Occupation and Industry [#160](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/160)
* Employment Status of client aged < 18 should be defaulted as Student [#158](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/158)
* Logic between employment status and Occupation & Industry [#174](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/174)
* Formatting [#105](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/105)

# v1.0.0 - b009

### Defects
* Profile Picture does not clear up after client profile dialog closed [#166](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/166)
* ID document type [#135](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/135)
* The default value of ID number of newly add family member[#176](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/176)

# v1.0.0 - b007

### Defects
* The length of display area for Education, Industry and Occupation are too short for certain options [#162](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/162)
* Logic between title and gender [#144](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/144)
* No mandatory check for Other Employment Status [#173](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/173)
* Link family member does not work [#178](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/178)
* Check all text in app mirror to the web version [#126](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/126)
* Name of employer /Country of employer 's label text and underline is overlapped [#143](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/143)


# v1.0.0 - b006
### Features
* Requote - “Clone” [#124](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/124)
* Take and choose photo [#106](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/106)
* client profile Validation popup —> when missing data will popup for input [#123](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/123)
* Keyboard type [#108](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/108)

### Defects
* Lower limit for DOB should be 100 years before today [#153](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/153)
* DOB missing on CP summary [#169](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/169)
* “Set App Password” – Not need in release 1 – disable the set app password function [#128](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/128)
* Remove wording “Power by React Native“ when app loading [#127](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/127)
* Change wordings [#125](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/125)
* Landing page UI issues [#65](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/65)
* employment status in "full time" [#140](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/140)
* family member relationship checking [#163](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/163)

# v1.0.0 - b004

Note: Beacause of not fully merge. We create this new build for it. Please use this build instead of build 3 to test the features and defeats on v1.0.0 - b003.

# v1.0.0 - b003
### Features
* Add/Link dependant [#18](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/18)
* New client / Edit client dialog [#16](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/16)
* Client profile tab [#17](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/17)
* Quotation (incl. riders) [#2](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/2)

### Defects
* Label text of Name of Employment/Business/School and Country [#167](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/167)
* Profile Picture does not clear up after client profile dialog closed [#166](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/166)
* Employment status in "full time" [#140](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/140)
* Mobile no. has no numeric format checking [#138](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/138)
* ID document type [#135](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/135)
* two different agent names shown on the screen [#151](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/151)
* Incorrect Field name of Country of Residence [#152](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/152)
* Other no. with wrong label text [#155](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/155)
* Employment Status of client aged < 18 should be defaulted as Student [#158](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/158)
* DOB should be default to year 1988 [#134](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/134)
* postal code problem [#139](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/139)
* Client Search - Incorrect hint text in search bar [#148](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/148)
* Issues relate to Country of Residence and Country (Residential Address) [#154](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/154)
* Display logic for Nationality and Singapore PR status [#145](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/145)
* show less When Quick Quote is off [#156](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/156)

# v1.0.0 - b002
### Defects
* Logic between title and gender [#144](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/144)
* show less When Quick Quote is off [#156](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/156)

# v1.0.0 – b001

### Features
* Products tab [#7](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/7)
    
### Defects
* Mobile no. has no numeric format checking [#138](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/138)
* Language should be a multi picker, not a typing field [#133](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/133)
*  Done Button of Edit Profile does not work [#132](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/132)
* Default value of Country of Employer does not set to "Singapore" [#150](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/150)
* No error when gender and employment status does not agree [#141](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/141)

# v0.1.0 – b6

### Features
* Add/Link dependant  [#18](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/18)


### Defects
* Client Search - Unable to search with lower case character [#146](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/146)
* Client Search - Incorrect trigger of auto search [#147](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/147)
* Client Profile Logic [#68](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/68)
* Quick quote and quick quote history [#86](http://192.168.222.60:8001/AXA_SG_Mobile_Application_Group/axa-sg-app/issues/86)
