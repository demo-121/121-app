import { createStackNavigator, createAppContainer } from "react-navigation";
import Main from "./containers/Main";

const RootNavigator = createStackNavigator(
  {
    Main: { screen: Main }
  },
  {
    defaultNavigationOptions: {
      header: null
    },
    cardStyle: { backgroundColor: "#e8e8ed" }
  }
);

export default createAppContainer(RootNavigator);
