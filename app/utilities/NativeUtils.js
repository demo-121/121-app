import { NativeModules } from "react-native";

export const mergePdfs = (base64Pdfs, callback) => {
  NativeModules.PdfMerger.mergePdfs(base64Pdfs, callback);
};

export const addPdfTitle = (base64Pdf, title, callback) => {
  NativeModules.PdfGenerator.addTitle(base64Pdf, title, callback);
};

export const addDebugLog = message => {
  NativeModules.Logger.addDebugLog(message);
};

export const addWordsPdfs = (
  base64Pdf,
  agentName,
  proposerName,
  x1,
  y1,
  x2,
  y2,
  callback
) => {
  NativeModules.PdfGenerator.addWordsPdfs(
    base64Pdf,
    agentName,
    proposerName,
    x1,
    y1,
    x2,
    y2,
    callback
  );
};
