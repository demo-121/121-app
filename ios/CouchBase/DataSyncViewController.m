//
//  AlterViewController.m
//  EASE
//
//  Created by MacAdmin on 30/8/18.
//  Copyright © 2018 Facebook. All rights reserved.
//
#define LAST_SYNC_DATE_KEY_IN_KEYCHAIN @"TIME_BOMB_EXPIRY_DATE"
#define LAST_SYNC_TIME_KEY_IN_KEYCHAIN @"LAST_DATA_SYNC_TIME"
#define LAST_SYNC_STATUS_KEY_IN_KEYCHAIN @"LAST_DATA_SYNC_STATUS"
#define SUCCESS @"SUCCESS"
#define PROCESSING @"PROCESSING"
#define COOKIENAME @"SyncGatewaySession"

#import "DataSyncViewController.h"
#import <CouchbaseLite/CouchbaseLite.h>
#import "Reachability.h"
#import "UtilsHandler.h"
#import "AppDelegate.h"
#import "EnvConfig.h"
#import "DataSyncEventManager.h"
#import "EABTableViewController.h"

@interface DataSyncViewController ()
  @property CBLManager* dbManager;
  @property CBLDatabase* localSyncDb;
  @property CBLDatabase* localEncryptDb;
  @property NSArray* docIds;
  @property NSMutableArray<NSDictionary *>* localWinObjs;
  @property NSMutableArray<NSDictionary *>* localChangedObjs;
  @property NSMutableArray<NSDictionary *>* localAllDelObjs;
  @property NSMutableArray<NSDictionary *>* onlineWinObjs;
  @property NSMutableArray<NSDictionary *>* onlineChangedObjs;
  @property NSMutableArray<NSDictionary *>* onlineAllDelObjs;
  @property NSMutableArray<NSString *> *localAllNotExistingDocIds;
  @property AppDelegate *delegate;
  @property UIActivityIndicatorView* indicatorView;
  @property NetworkStatus internetStatus;
  @property double progress;
  @property NSString *dataSyncStatus;//ACTIVATION, DOWNLOAD, UPLOAD
  @property NSMutableArray<NSString *> *policyDocIds;
  @property int retryCount;
  @property NSMutableArray<NSString *> *agentProfileDocIds;

  @property (weak, nonatomic) IBOutlet UILabel *titleLabel;
  @property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;
  @property (weak, nonatomic) IBOutlet UIProgressView *progressView;
  @property (weak, nonatomic) IBOutlet UILabel *percentageLabel;
  @property (weak, nonatomic) IBOutlet UIView *horizontalLineView;
  @property (weak, nonatomic) IBOutlet UIButton *leftButton;
  @property (weak, nonatomic) IBOutlet UIButton *rightButton;
  @property (weak, nonatomic) IBOutlet UIView *verticalLineView;
  @property (weak, nonatomic) IBOutlet NSLayoutConstraint *leftButtonLeftConstraint;
@end

@implementation DataSyncViewController

- (void) viewDidLoad {
  [super viewDidLoad];
  //initialization, connection local db, and show the data sync view
  [self initialization];
  //check last sync status
  [self checkLastSyncStatus];
}

- (void)viewDidAppear:(BOOL)animated {
  [super viewDidAppear:animated];

  //if not click left menu then auto data sync
  if (!self.isLeftMenu) {
    //show the view and data sync begin
    [self dataSyncStart];
  }

  //add view & document search page
  UILongPressGestureRecognizer *longPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(viewLongPress:)];
  longPress.minimumPressDuration = 5;//5 seconds
  [self.view addGestureRecognizer:longPress];
}

/**
 create localDb with Encrypt or not Encrypt
*/
- (void) initialization {
  NSError *error;
  self.dbManager = [CBLManager sharedInstance];
  self.delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
  
  bool isProduction = [@"production" isEqualToString:[[EnvConfig getConfig] objectForKey:@"NODE_ENV"]];
  if(isProduction){
    NSLog(@"=====self.encryptionKey : %@", self.encryptionKey);
    CBLDatabaseOptions* options = [[CBLDatabaseOptions alloc] init];
    options.storageType = kCBLSQLiteStorage;
    options.encryptionKey = self.encryptionKey;
    options.create = YES;
    self.localEncryptDb = [self.dbManager openDatabaseNamed: self.localDbName withOptions:options error:&error];
  }else{
    self.localEncryptDb = [self.dbManager databaseNamed: self.localDbName error: &error];
  }
  
  //set data sync style
  //only left menu need to click 'sync' button to data sync
  if (self.isLeftMenu) {
    self.titleLabel.text = @"Data Synchronization";
    self.descriptionLabel.text = @"Data synchronization will be performed. Press the Sync button to start data synchronization.";
    self.leftButtonLabelText = @"Cancel";
    self.rightButtonLabelText = @"Sync";
    [self.leftButton setTitle:self.leftButtonLabelText forState:UIControlStateNormal];
    [self.rightButton setTitle:self.rightButtonLabelText forState:UIControlStateNormal];
    self.progressView.hidden = YES;
    self.percentageLabel.hidden = YES;
    //set the button event
    [self.leftButton addTarget:self action:@selector(hideView) forControlEvents:UIControlEventTouchDown];
    [self.rightButton addTarget:self action:@selector(checkGeneralDataSync) forControlEvents:UIControlEventTouchDown];
  } else {
    self.titleLabel.text = @"Data Synchronization";
    self.descriptionLabel.text = @"Progressing...";
    self.progressView.progress = 0;
    self.percentageLabel.text = @"0%";
    self.horizontalLineView.hidden = YES;
    self.leftButton.hidden = YES;
    self.rightButton.hidden = YES;
    self.verticalLineView.hidden = YES;
  }
}

/**
 get the last data sync date and last data sync success date
*/
- (void) checkLastSyncStatus {
  NSLog(@"=====this is get the last data sync date from local db.");
  NSError *error;
  NSPredicate *where = [NSPredicate predicateWithFormat:@"type='SYNC_AUD' && agentCode=%@",self.agentCode];
  
  CBLQueryBuilder *queryBuilder = [[CBLQueryBuilder alloc] initWithDatabase:self.localEncryptDb
                                                                     select:@[@"id",@"deviceUUID",@"status",@"syncDate"]
                                                             wherePredicate:where
                                                                    orderBy:nil
                                                                      error:&error];
  CBLQuery* query = [queryBuilder createQueryWithContext:nil];
  CBLQueryEnumerator *rowEnum = [query run:&error];
  if (rowEnum != nil && rowEnum.count > 0) {
    NSDate* lstSync = nil;
    NSString* lstSyncStatus = nil;
    for (CBLQueryRow *row in rowEnum) {
      CBLDocument* doc = row.document;
      if (doc != nil) {
        NSLog(@"id[%@],deviceUUID[%@],status[%@],syncDate[%@]",doc[@"id"],doc[@"deviceUUID"],doc[@"status"],doc[@"syncDate"]);
        lstSync = [[UtilsHandler shareInstance] string2Date:doc[@"syncDate"]];
        lstSyncStatus = doc[@"status"];
        
        if (self.lastSyncDate == nil) {//first row
          self.lastSyncDate = lstSync;
          if ([lstSyncStatus isEqualToString:SUCCESS]) {
            self.lastSyncSuccessDate = lstSync;
          }
        }else{
          NSComparisonResult result = [lstSync compare:self.lastSyncDate];
          switch (result) {
            case NSOrderedAscending: {
              break;
            }
            case NSOrderedSame: {
              break;
            }
            case NSOrderedDescending: {
              self.lastSyncDate = lstSync;
              if ([lstSyncStatus isEqualToString:SUCCESS]) {
                self.lastSyncSuccessDate = lstSync;
              }
              break;
            }
          }
        }
      }
    }
    //last audit log
    //self.lastSyncDate = lstSync;
    //self.lastSyncAuditLogDocId = lstSyncDocId;
  }
}

/**
 show data sync view
*/
- (void) dataSyncStart{
  if (self.lastSyncSuccessDate == nil) {
    [self checkActivation];
  }else{
    [self checkGeneralDataSync];
  }
  //main thread stop 5 second then hidden the self.view
  //dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
  //  [self dismissViewControllerAnimated:NO completion:nil];
  //});
}


/**
 insert data sync begin date & start data sync
*/
-(void)checkActivation
{
  self.dataSyncStatus = @"ACTIVATION";
  //check network
  self.internetStatus = [self internetConnection];
  if (self.internetStatus == ReachableViaWiFi || self.internetStatus == ReachableViaWWAN) {
    //start activation
    [self activation];
  } else{
    self.descriptionLabel.text = @"No network, Please connect your device to online mode and retry again.";
    self.horizontalLineView.hidden = NO;
    self.rightButton.hidden = NO;
    self.leftButtonLeftConstraint.constant = 0;
    [self.rightButton setTitle:@"Re-Try" forState:UIControlStateNormal];
    [self.rightButton removeTarget:self action:nil forControlEvents:UIControlEventTouchDown];
    [self.rightButton addTarget:self action:@selector(retryCheckActivation) forControlEvents:UIControlEventTouchDown];
  }
}

- (void) retryCheckActivation{
  self.descriptionLabel.text = @"Progressing...";
  self.progress = 0;
  [self.progressView setProgress:(self.progress / 100) animated:YES];
  self.percentageLabel.text = @"0%";
  [self addRetryCount];
  [self checkActivation];
}

//return: NotReachable = no internet, ReachableViaWiFi = WIFI, ReachableViaWWAN = 3G/4G/5G
- (NetworkStatus) internetConnection {
  Reachability *reachability =[Reachability reachabilityForInternetConnection];
  NetworkStatus status =[reachability currentReachabilityStatus];
  return status;
}

- (void)handleDataSyncError:(NSInteger) errorCode{
  [self.delegate.push stop];
  [self.delegate.pull stop];
  [[NSNotificationCenter defaultCenter] removeObserver:self name:kCBLReplicationChangeNotification object:self.delegate.push];
  [[NSNotificationCenter defaultCenter] removeObserver:self name:kCBLReplicationChangeNotification object:self.delegate.pull];
  
  if (errorCode == 0) {//data sync success
    NSLog(@"=====Data Sync Success! this data sync status: %@", self.dataSyncStatus);
    if ([self.dataSyncStatus isEqualToString:@"ACTIVATION"] || [self.dataSyncStatus isEqualToString:@"UPLOAD"]) {
      //hidden the data sync view
      self.descriptionLabel.text = @"Completed";
      dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self hideView];
        NSDate *curr_date = [NSDate date];
        //save last sync date into keychain
        [self saveLastSyncDateToKeychain:curr_date];
        [[UtilsHandler shareInstance] saveToKeychainWithKey:LAST_SYNC_STATUS_KEY_IN_KEYCHAIN value:SUCCESS];
        //insert data sync audit log
        [self dataSyncCompletedLog:curr_date];
        //send success to react native
        [[DataSyncEventManager allocWithZone:nil] sendDataSyncStatus:SUCCESS];
      });
      
    }else if ([self.dataSyncStatus isEqualToString:@"DOWNLOAD"]) {
      [self winLogicForDataSync];
      [self mergeDocuments];
      self.dataSyncStatus = @"UPLOAD";
      [self upload];
    }
  } else {
    NSString *message = [NSString stringWithFormat:@"Failed! (Err-D%ld)\nPlease connect your device to online mode and retry again or contact your systems administrator for assistance",(long)errorCode];
    if (self.isBackground) {
      DataSyncEventManager *dataSyncEvent = [DataSyncEventManager allocWithZone:nil];
      [dataSyncEvent sendDataSyncStatus:message];
      [self hideView];
    }else{
      NSLog(@"=====Other Sync error: %ld", errorCode);
      NSLog(@"=====if error code = -1001 , it is timeout");
      NSLog(@"=====if error code = -1022 , it is network problem");
      NSLog(@"=====if error code = 401 , it is Authentication error");
      [self.descriptionLabel setText:message];
      self.horizontalLineView.hidden = NO;
      self.rightButton.hidden = NO;
      self.leftButtonLeftConstraint.constant = 0;
      [self.rightButton setTitle:@"Re-Try" forState:UIControlStateNormal];
      [self.rightButton removeTarget:self action:nil forControlEvents:UIControlEventTouchDown];
      if ([self.dataSyncStatus isEqualToString:@"ACTIVATION"]) {//if activation then Not allowed to Restore
        [self.rightButton addTarget:self action:@selector(retryCheckActivation) forControlEvents:UIControlEventTouchDown];
      }else{
        [self.rightButton addTarget:self action:@selector(retryCheckGeneralDataSync) forControlEvents:UIControlEventTouchDown];
      }
    }
  }
}

- (void) retryCheckGeneralDataSync{
  self.descriptionLabel.text = @"Progressing...";
  self.progress = 0;
  [self.progressView setProgress:(self.progress / 100) animated:YES];
  self.percentageLabel.text = @"0%";
  [self addRetryCount];
  [self checkGeneralDataSync];
}

-(void) activation
{
  //hiddent the data sync button
  self.horizontalLineView.hidden = YES;
  self.leftButton.hidden = YES;
  self.rightButton.hidden = YES;
  self.verticalLineView.hidden = YES;
  [self.leftButton setTitle:@"" forState:UIControlStateNormal];
  [self.rightButton setTitle:@"" forState:UIControlStateNormal];
  [self.leftButton removeTarget:self action:nil forControlEvents:UIControlEventTouchDown];
  [self.rightButton removeTarget:self action:nil forControlEvents:UIControlEventTouchDown];
  
  dispatch_semaphore_t semaphore = dispatch_semaphore_create(0);
  //get the agent profile document ids
  [self getAgtProfileDocIds:semaphore];
  dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER);
  
  //add agent profile document id
  NSMutableArray<NSString *>* activationDocIds = [[NSMutableArray alloc] initWithArray:self.syncDocumentIds];
  if (self.agentProfileDocIds != nil && self.agentProfileDocIds.count > 0) {
    for (NSString *dict in self.agentProfileDocIds) {
      [activationDocIds addObject:dict];
    }
  }
  activationDocIds = [activationDocIds valueForKeyPath:@"@distinctUnionOfObjects.self"]; //remove duplicate
  
  //insert auto log into local db
  [self dataSyncCreateLog:PROCESSING syncDate:[[UtilsHandler shareInstance] date2String:[NSDate date]]];
  [[UtilsHandler shareInstance] saveToKeychainWithKey:LAST_SYNC_STATUS_KEY_IN_KEYCHAIN value:PROCESSING];
  
  NSLog(@"=====start data sync , this is activation , syncDbName[%@], remoteURL[%@]",self.syncDbName, self.remoteURL);
  self.delegate.pull = [self.localEncryptDb createPullReplication: [NSURL URLWithString: self.remoteURL]];
  self.delegate.pull.continuous = NO;

  //fixed "CredStore - performQuery - Error copying matching creds.  Error=-25300"  error
   id<CBLAuthenticator> auth;
   auth = [CBLAuthenticator basicAuthenticatorWithName: self.username password: self.password];
//  [self.delegate.pull setCookieNamed:COOKIENAME withValue:self.sessionCookieSyncGatewaySession path:self.sessionCookiePath expirationDate:self.sessionCookieExpiryDate secure:NO];
//  __block BOOL callbackInvoked = NO;
//  id<CBLAuthenticator> auth = [CBLAuthenticator OpenIDConnectAuthenticator:
//                               ^(NSURL* login, NSURL* authBase, CBLOIDCLoginContinuation cont)
//                               {
//                                 callbackInvoked = YES;
//
//                               }];

  self.delegate.pull.authenticator = auth;
  
  //self.delegate.pull.channels = @[[self.agentCode stringByAppendingString:@"_private_data"]];
  //NSLog(@"self.delegate.pull.channels:%@",self.delegate.pull.channels);
  NSLog(@"=====this pull filter the document ids:%@", activationDocIds);
  self.delegate.pull.documentIDs = activationDocIds;
  
  //only pull Listener
  [[NSNotificationCenter defaultCenter] addObserver: self
                                           selector: @selector(replicationChanged:)
                                               name: kCBLReplicationChangeNotification
                                             object: self.delegate.pull];
  
  [self.delegate.pull start];
  NSLog(@"=====this is activation [start]");
}

-(void) download
{
  //insert auto log into local db
  [self dataSyncCreateLog:PROCESSING syncDate:[[UtilsHandler shareInstance] date2String:[NSDate date]]];
  [[UtilsHandler shareInstance] saveToKeychainWithKey:LAST_SYNC_STATUS_KEY_IN_KEYCHAIN value:PROCESSING];
  
  NSLog(@"=====this is download documents from online db , syncDbName[%@], remoteURL[%@]",self.syncDbName, self.remoteURL);
  self.delegate.pull = [self.localSyncDb createPullReplication: [NSURL URLWithString: self.remoteURL]];
  self.delegate.pull.continuous = NO;
  
  //fixed "CredStore - performQuery - Error copying matching creds.  Error=-25300"  error
    id<CBLAuthenticator> auth;
    auth = [CBLAuthenticator basicAuthenticatorWithName: self.username password: self.password];
  
//  [self.delegate.pull setCookieNamed:COOKIENAME withValue:self.sessionCookieSyncGatewaySession path:self.sessionCookiePath expirationDate:self.sessionCookieExpiryDate secure:NO];
//  __block BOOL callbackInvoked = NO;
//  id<CBLAuthenticator> auth = [CBLAuthenticator OpenIDConnectAuthenticator:
//                               ^(NSURL* login, NSURL* authBase, CBLOIDCLoginContinuation cont)
//                               {
//                                 callbackInvoked = YES;
//
//                               }];
  self.delegate.pull.authenticator = auth;
  
  self.delegate.pull.documentIDs = self.docIds;
  
  //Perform the replacement rootViewController operation in the main thread
  //only pull Listener

  [[NSNotificationCenter defaultCenter] addObserver: self
                                           selector: @selector(replicationChanged:)
                                               name: kCBLReplicationChangeNotification
                                             object: self.delegate.pull];

  
  [self.delegate.pull start];
}

-(void) upload
{
  NSLog(@"=====this is upload documents to online db, syncDbName[%@], remoteURL[%@]",self.syncDbName, self.remoteURL);
  self.delegate.push = [self.localSyncDb createPushReplication: [NSURL URLWithString: self.remoteURL]];
  //self.delegate.pull = [self.localSyncDb createPullReplication: [NSURL URLWithString: self.remoteURL]];
  self.delegate.push.continuous = NO;
  //self.delegate.pull.continuous = NO;

  //fixed "CredStore - performQuery - Error copying matching creds.  Error=-25300"  error
    id<CBLAuthenticator> auth;
    auth = [CBLAuthenticator basicAuthenticatorWithName: self.username password: self.password];
  //  self.delegate.push.authenticator = auth;
  
//  [self.delegate.push setCookieNamed:COOKIENAME withValue:self.sessionCookieSyncGatewaySession path:self.sessionCookiePath expirationDate:self.sessionCookieExpiryDate secure:NO];
//  __block BOOL callbackInvoked = NO;
//  id<CBLAuthenticator> auth = [CBLAuthenticator OpenIDConnectAuthenticator:
//                               ^(NSURL* login, NSURL* authBase, CBLOIDCLoginContinuation cont)
//                               {
//                                 callbackInvoked = YES;
//
//                               }];
  self.delegate.push.authenticator = auth;
  
  //set pull filter
  //self.delegate.pull.documentIDs = self.docIds;
  NSLog(@"=====start data sync filter document Ids: %@", self.docIds);
  
  //set push filter
  [self.localSyncDb setFilterNamed: @"notPushBackupDocs" asBlock: FILTERBLOCK({
    NSString* prefixParam = params[@"prefix"];
    return prefixParam && ![revision[@"_id"] hasPrefix: prefixParam];
  })];
  
  self.delegate.push.filter = @"notPushBackupDocs";
  self.delegate.push.filterParams = @{@"prefix": @"BACKUP_"};
  
  
  //Perform the replacement rootViewController operation in the main thread
  //only pull Listener

  [[NSNotificationCenter defaultCenter] addObserver: self
                                           selector: @selector(replicationChanged:)
                                               name: kCBLReplicationChangeNotification
                                             object: self.delegate.push];
//    [[NSNotificationCenter defaultCenter] addObserver: self
//                                             selector: @selector(replicationChanged:)
//                                                 name: kCBLReplicationChangeNotification
//                                               object: self.delegate.pull];

  
  [self.delegate.push start];
//  [self.delegate.pull start];
}

-(void) dataSync
{
  if ([self.dataSyncStatus isEqualToString:@"ACTIVATION"]) {
    NSLog(@"=====this is activation data sync.");
    [self activation];
  } else if ([self.dataSyncStatus isEqualToString:@"DOWNLOAD"]) {
    NSLog(@"=====this is download data sync.");
    [self download];
  } else if ([self.dataSyncStatus isEqualToString:@"UPLOAD"]) {
    NSLog(@"=====this is upload data sync.");
    [self upload];
  }
}

- (void) replicationChanged: (NSNotification*)n {
  //  if ([[NSThread currentThread] isMainThread]) {
  //    NSLog(@"main thread");
  //  } else {
  //    NSLog(@"not main thread");
  //  }
  
  BOOL completed = NO;
  BOOL active = NO;
  double totalChg = 0;
  double completedChg = 0;
  NSError *error;
  if ([self.dataSyncStatus isEqualToString:@"ACTIVATION"]) {
    active = (self.delegate.pull.status == kCBLReplicationActive);
    totalChg = self.delegate.pull.changesCount;
    completedChg = self.delegate.pull.completedChangesCount;
    completed = (self.delegate.pull.status == kCBLReplicationStopped);
    error = self.delegate.pull.lastError;
  } else if ([self.dataSyncStatus isEqualToString:@"DOWNLOAD"]) {
    active = (self.delegate.pull.status == kCBLReplicationActive);
    totalChg = self.delegate.pull.changesCount;
    completedChg = self.delegate.pull.completedChangesCount;
    completed = (self.delegate.pull.status == kCBLReplicationStopped);
    error = self.delegate.pull.lastError;
  } else if ([self.dataSyncStatus isEqualToString:@"UPLOAD"]) {
    active = (self.delegate.push.status == kCBLReplicationActive);
    totalChg = self.delegate.push.changesCount;
    completedChg = self.delegate.push.completedChangesCount;
    completed = (self.delegate.push.status == kCBLReplicationStopped);
    error = self.delegate.push.lastError;
  }
  
  NSLog(@"=====this is status : [%@]", self.dataSyncStatus);
  if(self.delegate.pull.status == kCBLReplicationStopped){NSLog(@"=====self.delegate.pull.status == kCBLReplicationStopped=====");};
  if(self.delegate.pull.status == kCBLReplicationOffline){NSLog(@"=====self.delegate.pull.status == kCBLReplicationOffline=====");};
  if(self.delegate.pull.status == kCBLReplicationIdle){NSLog(@"=====self.delegate.pull.status == kCBLReplicationIdle=====");};
  if(self.delegate.pull.status == kCBLReplicationActive){NSLog(@"=====self.delegate.pull.status == kCBLReplicationActive=====");};
  if(self.delegate.push.status == kCBLReplicationStopped){NSLog(@"=====self.delegate.push.status == kCBLReplicationStopped=====");};
  if(self.delegate.push.status == kCBLReplicationOffline){NSLog(@"=====self.delegate.push.status == kCBLReplicationOffline=====");};
  if(self.delegate.push.status == kCBLReplicationIdle){NSLog(@"=====self.delegate.push.status == kCBLReplicationIdle=====");};
  if(self.delegate.push.status == kCBLReplicationActive){NSLog(@"=====self.delegate.push.status == kCBLReplicationActive=====");};
  NSLog(@"=====changed documents : %f", totalChg);
  NSLog(@"=====completed documents : %f", completedChg);
  
  NSLog(@"=====this is data sync error code: [%ld]", (long)error.code);
  NSLog(@"=====error information : %@", error);
  
  if (active) {
    if (totalChg > 0.0) {
      if ([self.dataSyncStatus isEqualToString:@"ACTIVATION"]) {
        self.progress = completedChg / totalChg * 100;
      } else if ([self.dataSyncStatus isEqualToString:@"DOWNLOAD"]) {
        self.progress = (completedChg / totalChg * 0.5) * 100;
      } else if ([self.dataSyncStatus isEqualToString:@"UPLOAD"]) {
        self.progress = 50 + (completedChg / totalChg *0.5 * 100);
      }
    }
    double currPercentage = [[self.percentageLabel.text stringByReplacingOccurrencesOfString:@"%" withString:@""] doubleValue];
    //set the progress
    if (currPercentage < self.progress) {
      [self.progressView setProgress:(self.progress / 100) animated:YES];
      [self.percentageLabel setText:[[NSString stringWithFormat: @"%.0lf", self.progress] stringByAppendingString:@"%"]];
    }
  }
  
  //handle error
  if (error != nil) {
    NSLog(@"=====this is data sync error code: [%ld]", (long)error.code);
    NSLog(@"=====error information : %@", error);
    [self handleDataSyncError:error.code];
  }
  
  //data sync completed
  if(completed){
    //sync success
    if ([self.dataSyncStatus isEqualToString:@"ACTIVATION"] || [self.dataSyncStatus isEqualToString:@"UPLOAD"]) {
      if(self.progressView.progress < 1){
        [self.progressView setProgress:1 animated:YES];
        [self.percentageLabel setText:@"100%"];
      }
    }
    [self handleDataSyncError:0];
  }
}

//hidden DataSyncViewController view
- (void)hideView{
  [self dismissViewControllerAnimated:NO completion:nil];
}

//get local esub/epay/changed documents
- (void) getLocalEsubOrEpayOrChangedDocs{
  self.localWinObjs = [NSMutableArray array];
  self.localChangedObjs = [NSMutableArray array];
  self.localAllDelObjs = [NSMutableArray array];
  self.policyDocIds = [NSMutableArray array];
  
  NSMutableArray<NSString *>* esubAndEpyaCustIds = [NSMutableArray array];
  NSMutableArray<NSString *>* otherCustIds = [NSMutableArray array];
  
  //  Get the locally modified document collection from the last sync date
  //  NSDate *lstSync = [self getLastSyncDate];
  
  NSArray *chnagedDocs = [self selectLocalChangedDocsFromLstSync];
  //  Loop the document collection to process the data
  if (chnagedDocs == nil) {
    NSLog(@"Local database no changed document!");
  }else{
    //get local eSub / ePay completed information
    NSDictionary* properties;
    NSDictionary *payment;
    BOOL isSubmittedStatus;
    for (CBLDocument *document in chnagedDocs) {
      properties = document.properties;
      if ([properties[@"type"] isEqualToString:@"application"] || [properties[@"type"] isEqualToString:@"masterApplication"]) {
        payment = properties[@"payment"];
        isSubmittedStatus = [[properties objectForKey:@"isSubmittedStatus"] boolValue];
        if (isSubmittedStatus) {
          [esubAndEpyaCustIds addObject:properties[@"pCid"]];
        } else if (payment!=nil && [@[@"crCard",@"eNets",@"dbsCrCardIpp"] indexOfObject:payment[@"initPayMethod"]] != NSNotFound && [payment[@"trxStatus"] isEqualToString:@"Y"] && payment[@"trxTime"] != nil) {
          [esubAndEpyaCustIds addObject:properties[@"pCid"]];
        } else {
          [otherCustIds addObject:properties[@"pCid"]];
        }
      } else {
        if ([properties[@"type"] isEqualToString:@"cust"]) {
          [otherCustIds addObject:properties[@"cid"]];
        } else if ([properties[@"type"] isEqualToString:@"masterApproval"] || [properties[@"type"] isEqualToString:@"approval"]) {
          [otherCustIds addObject:properties[@"customerId"]];
        } else {
          [otherCustIds addObject:properties[@"pCid"]];
        }
      }
      
    }
    NSLog(@"=====esubAndEpyaCustIds: %@",esubAndEpyaCustIds);
    NSLog(@"=====otherCustIds: %@",otherCustIds);
    
    //remove the duplicates custid
    NSArray<NSString *> * localWinCustIds = [[NSSet setWithArray:esubAndEpyaCustIds] allObjects];
    NSArray<NSString *> * localChangedCustIds = [[NSSet setWithArray:otherCustIds] allObjects];
    //remove the duplicates custid in localWinCustIds
    localChangedCustIds = [localChangedCustIds filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"NOT (SELF in %@)", localWinCustIds]];
    
    NSLog(@"=====localWinCustIds: %@",localWinCustIds);
    NSLog(@"=====localChangedCustIds: %@",localChangedCustIds);
    
    //get the all document id with winCustId
    for (NSString *custId in localWinCustIds) {
      [self.localWinObjs addObject:[self getAllDocIdsWithCustId:custId]];
    }
    
    //get the all changed document id with ChangedCustId
    for (NSString *custId in localChangedCustIds) {
      NSMutableDictionary *changedObj = [NSMutableDictionary dictionaryWithCapacity:3];
      NSNumber *maxUpdDate;
      NSMutableArray<NSString *>* docIds = [NSMutableArray array];
      
      //set custId field
      [changedObj setObject:custId forKey:@"custId"];
      
      //set docIds field
      for (CBLDocument *document in chnagedDocs) {
        properties = document.properties;
        if ([properties[@"cid"] isEqualToString:custId] || [properties[@"pCid"] isEqualToString:custId] || [properties[@"customerId"] isEqualToString:custId]) {
          
          if (maxUpdDate==nil) {
            maxUpdDate = [NSNumber numberWithLongLong:[properties[@"lstChgDate"] longLongValue]];
          }else{
            NSNumber *lstChgDate = [NSNumber numberWithLongLong:[properties[@"lstChgDate"] longLongValue]];
            if ([lstChgDate compare:maxUpdDate] == NSOrderedDescending) {
              maxUpdDate = lstChgDate;
            }
          }
          [docIds addObject:properties[@"id"]?properties[@"id"]:properties[@"_id"]];
        }
      }
      
      [changedObj setObject:[NSString stringWithFormat:@"%@",maxUpdDate] forKey:@"maxUpdDate"];
      [changedObj setObject:docIds forKey:@"docIds"];
      
      [self.localChangedObjs addObject:changedObj];
    }
    
  }
  //handle deleted client
  self.localAllDelObjs = [self getDeletedDocIdsFromLstSync:self.loginId];
  NSLog(@"===local all deleted customer document id list from last sync==>%@",self.localAllDelObjs);
  
  //handle all win logic
  //if online win obj the last change date > local win obj, then remove local win obj array
  //if online win obj the last change date < local win obj, then remove online win obj array
  NSMutableArray<NSDictionary *> *onlineNotWinObjs = [NSMutableArray array];
  NSMutableArray<NSDictionary *> *localNotWinObjs = [NSMutableArray array];
  if (self.onlineWinObjs != nil && self.onlineWinObjs.count > 0
      && self.localWinObjs != nil && self.localWinObjs.count > 0) {
    long long localmaxUpdDate;
    long long onlinemaxUpdDate;
    BOOL isLocalAlwaysWin = NO;
    BOOL isOnlineAlwaysWin;
    for (NSDictionary *local in self.localWinObjs) {
      for (NSDictionary *online in self.onlineWinObjs) {
        if ([local[@"custId"] isEqualToString:online[@"custId"]]) {
          
          //isLocalAlwaysWin = [local[@"isForceDownload"] boolValue];
          isOnlineAlwaysWin = [online[@"isForceDownload"] boolValue];
          
          if(isOnlineAlwaysWin && !isLocalAlwaysWin){
            [localNotWinObjs addObject:local];
          }else if(isLocalAlwaysWin && !isOnlineAlwaysWin){
            [onlineNotWinObjs addObject:online];
          }else{
            localmaxUpdDate = [local[@"maxUpdDate"] longLongValue];
            onlinemaxUpdDate = [online[@"maxUpdDate"] longLongValue];
            if(localmaxUpdDate > onlinemaxUpdDate){
              [onlineNotWinObjs addObject:online];
            }else{
              [localNotWinObjs addObject:local];
            }
          }
          
        }
      }
    }
  }
  
  [self.onlineWinObjs removeObjectsInArray:onlineNotWinObjs];
  [self.localWinObjs removeObjectsInArray:localNotWinObjs];
}

- (NSMutableArray<NSDictionary *>*) getDeletedDocIdsFromLstSync:(NSString *) loginId {
  NSMutableArray<NSDictionary *>* result = [NSMutableArray array];
  NSString *deletedDocIdFileName = [[@"U_" stringByAppendingString:loginId] stringByAppendingString:@"_DELETEDID"];
  CBLDocument* doc = [self.localEncryptDb documentWithID: deletedDocIdFileName];
  long long lastSyncTime = [[UtilsHandler shareInstance] getTimeMillis:self.lastSyncDate];
  if (doc[@"deletedIds"] != nil) {
    for (NSDictionary *dict in doc[@"deletedIds"]) {
      long long deletedTime = [dict[@"deletedTime"] longLongValue];
      if (deletedTime - lastSyncTime > 0) {
        [result addObject:dict];
      }
    }
  }
  return result;
}

//the data return from web service
- (BOOL) getOnlineEsubOrEpayOrChangedDocs:(NSData*) onlineDataFrmWebService {
  NSError* error;
  
  NSString *webServiceStr = [[NSString alloc] initWithData:onlineDataFrmWebService encoding:NSUTF8StringEncoding];
  NSLog(@"=====web service return string : %@", webServiceStr);
  //string to json
  NSDictionary *webServiceJson = [NSJSONSerialization JSONObjectWithData:[webServiceStr dataUsingEncoding:NSUTF8StringEncoding]
                                                                 options:NSJSONReadingMutableContainers
                                                                   error:&error];
  
  if (![webServiceJson[@"status"] isEqualToString:@"success"]) {
    return NO;
  }

  //filter onlineChangedObjs, handle offline error return.
  NSPredicate *predicate=[NSPredicate predicateWithFormat:@"maxUpdDate>0"];
  
  //online win object
  NSMutableArray<NSDictionary *>* onlineWinObjs = [webServiceJson valueForKeyPath:@"result.onlineWinObjs"];
  NSMutableArray<NSDictionary *>* onlineChangedObjs = [webServiceJson valueForKeyPath:@"result.onlineChangedObjs"];
  
  //online win object
  self.onlineWinObjs = [NSMutableArray arrayWithArray:[onlineWinObjs filteredArrayUsingPredicate:predicate]];
  //online onlineChangedObjs object
  self.onlineChangedObjs = [NSMutableArray arrayWithArray:[onlineChangedObjs filteredArrayUsingPredicate:predicate]];
  //online delete object
  self.onlineAllDelObjs = [webServiceJson valueForKeyPath:@"result.deletedIds"];
  
  return YES;
}

//set data sync filter documentIds according to local / online changed document.
- (void) setPullFilterDocIds{
  NSMutableArray<NSString *>* ret = [NSMutableArray array];
  if (self.onlineWinObjs != nil && self.onlineWinObjs.count > 0) {
    for (NSDictionary *obj in self.onlineWinObjs) {
      @try {
        [ret addObjectsFromArray:obj[@"docIds"]];
      } @catch (NSException *exception) {
        NSLog(@"=====onlineChangedObjs, this is data error : %@", obj);
      }
    }
  }
  
  if (self.onlineChangedObjs != nil && self.onlineChangedObjs.count > 0) {
    for (NSDictionary *obj in self.onlineChangedObjs) {
      @try {
        [ret addObjectsFromArray:obj[@"docIds"]];
      } @catch (NSException *exception) {
        NSLog(@"=====onlineChangedObjs, this is data error : %@", obj);
      }
    }
  }
  
  if (self.localWinObjs != nil && self.localWinObjs.count > 0) {
    for (NSDictionary *obj in self.localWinObjs) {
      @try {
        [ret addObjectsFromArray:obj[@"docIds"]];
      } @catch (NSException *exception) {
        NSLog(@"=====onlineChangedObjs, this is data error : %@", obj);
      }
    }
  }
  
  if (self.localChangedObjs != nil && self.localChangedObjs.count > 0) {
    for (NSDictionary *obj in self.localChangedObjs) {
      @try {
        [ret addObjectsFromArray:obj[@"docIds"]];
      } @catch (NSException *exception) {
        NSLog(@"=====onlineChangedObjs, this is data error : %@", obj);
      }
    }
  }
  
  //add deleted document id
  if (self.localAllDelObjs != nil && self.localAllDelObjs.count > 0) {
    for (NSDictionary *dict in self.localAllDelObjs) {
      [ret addObject:dict[@"clientId"]];
    }
  }
  if (self.onlineAllDelObjs != nil && self.onlineAllDelObjs.count > 0) {
    for (NSDictionary *dict in self.onlineAllDelObjs) {
      [ret addObject:dict[@"clientId"]];
    }
  }
  
  //add agent profile document id
  if (self.agentProfileDocIds != nil && self.agentProfileDocIds.count > 0) {
    for (NSString *dict in self.agentProfileDocIds) {
      [ret addObject:dict];
    }
  }
  
  ret = [ret valueForKeyPath:@"@distinctUnionOfObjects.self"]; //remove duplicate
  if (ret !=nil && ret.count > 0) {
    self.docIds = [ret copy];
  }
}

- (NSDictionary*)getAllDocIdsWithCustId:(NSString*) custId{
  NSError *error;
  NSMutableDictionary *ret = [NSMutableDictionary dictionaryWithCapacity:3];
  [ret setObject:custId forKey:@"custId"];//add esub/epay document id
  
  NSMutableArray<NSString *>* docIds = [NSMutableArray array];
  //get the all document where cust id
  NSArray *select = @[@"_id"];
  NSPredicate *where = [NSPredicate predicateWithFormat:@"pCid=%@ || cid=%@ || customerId=%@", custId, custId, custId];
  
  CBLQueryBuilder *queryBuilder = [[CBLQueryBuilder alloc] initWithDatabase:self.localEncryptDb
                                                                     select:select
                                                             wherePredicate:where
                                                                    orderBy:nil
                                                                      error:&error];
  
  CBLQuery* query = [queryBuilder createQueryWithContext:nil];
  CBLQueryEnumerator *rowEnum = [query run:&error];
  
  NSNumber *maxUpdDate;
  NSDictionary* properties;
  if (rowEnum != nil && rowEnum.count > 0) {
    for (CBLQueryRow *row in rowEnum) {
      if (row.document != nil) {
        properties = row.document.properties;
        if (maxUpdDate==nil) {
          maxUpdDate = [NSNumber numberWithLongLong:[properties[@"lstChgDate"] longLongValue]];
        }else{
          NSNumber *lstChgDate = [NSNumber numberWithLongLong:[properties[@"lstChgDate"] longLongValue]];
          if ([lstChgDate compare:maxUpdDate] == NSOrderedDescending) {
            maxUpdDate = lstChgDate;
          }
        }
        [docIds addObject:properties[@"id"]?properties[@"id"]:properties[@"_id"]];
        
        //store policy json document id
        if ([properties[@"type"] isEqualToString:@"masterApproval"] || [properties[@"type"] isEqualToString:@"approval"]) {
          [self.policyDocIds addObject:properties[@"id"]?properties[@"id"]:properties[@"_id"]];
        }
      }
    }
  }
  
  [ret setObject:[NSString stringWithFormat:@"%@",maxUpdDate] forKey:@"maxUpdDate"];
  [ret setObject:docIds forKey:@"docIds"];
  
  return ret;
}

- (void) winLogicForDataSync{
  //*******************************************************************************************************************************
  //***********************get the conflict cust ids***********************
  NSMutableArray<NSString *> *conflictCustIds = [NSMutableArray array];
  for (NSDictionary *localChangedObj in self.localChangedObjs) {
    for (NSDictionary *onlineChangedObj in self.onlineChangedObjs) {
      if ([localChangedObj[@"custId"] isEqualToString:onlineChangedObj[@"custId"]]) {
        [conflictCustIds addObject:localChangedObj[@"custId"]];
      }
    }
  }
  
  NSMutableArray<NSDictionary *> *localChangedObjsNotExistOnline = [NSMutableArray array];
  NSMutableArray<NSDictionary *> *onlineChangedObjsNotExistlocal = [NSMutableArray array];
  //local
  for (NSDictionary *localChangedObj in self.localChangedObjs) {
    if ([conflictCustIds indexOfObject:localChangedObj[@"custId"]] == NSNotFound) {
      [localChangedObjsNotExistOnline addObject:localChangedObj];
      [self.localWinObjs addObject:localChangedObj];
    }
  }
  
  //online
  for (NSDictionary *onlineChangedObj in self.onlineChangedObjs) {
    if ([conflictCustIds indexOfObject:onlineChangedObj[@"custId"]] == NSNotFound) {
      [onlineChangedObjsNotExistlocal addObject:onlineChangedObj];
      [self.onlineWinObjs addObject:onlineChangedObj];
    }
  }
  
  [self.localChangedObjs removeObjectsInArray:localChangedObjsNotExistOnline];
  [self.onlineChangedObjs removeObjectsInArray:onlineChangedObjsNotExistlocal];

  //***********************loop the conflict cust id, last update date win***********************
  if([self.localChangedObjs count] == [self.onlineChangedObjs count]){
    NSDictionary *localObj;
    NSDictionary *onlineObj;
    long long localmaxUpdDate;
    long long onlinemaxUpdDate;
    for( int i = 0; i < [self.localChangedObjs count]; i++){
      localObj = [self.localChangedObjs objectAtIndex:i];
      onlineObj = [self.onlineChangedObjs objectAtIndex:i];
      //localmaxUpdDate = [[UtilsHandler shareInstance] getTimeMillis:[[UtilsHandler shareInstance] string2Date:localObj[@"maxUpdDate"]]];
      //onlinemaxUpdDate = [[UtilsHandler shareInstance] getTimeMillis:[[UtilsHandler shareInstance] string2Date:onlineObj[@"maxUpdDate"]]];
      
      localmaxUpdDate = [localObj[@"maxUpdDate"] longLongValue];
      onlinemaxUpdDate = [onlineObj[@"maxUpdDate"] longLongValue];
      
      if(localmaxUpdDate > onlinemaxUpdDate){
        [self.localWinObjs addObject:localObj];
        //if online exist but local win , the online document need to delete
        NSArray<NSString *> * localChangedDocIdWithSameCustId = [[NSSet setWithArray:localObj[@"docIds"]] allObjects];
        NSArray<NSString *> * onlineChangedDocIdWithSameCustId = [[NSSet setWithArray:onlineObj[@"docIds"]] allObjects];
        onlineChangedDocIdWithSameCustId = [onlineChangedDocIdWithSameCustId filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"NOT (SELF in %@)", localChangedDocIdWithSameCustId]];
        if(onlineChangedDocIdWithSameCustId != nil && [onlineChangedDocIdWithSameCustId count] > 0){
          NSNumber *curr_date_time = [NSNumber numberWithLongLong:[[UtilsHandler shareInstance] getTimeMillis:[NSDate date]]];
          for (NSString *docid in onlineChangedDocIdWithSameCustId) {
            NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
            [dictionary setObject:docid forKey:@"clientId"];
            [dictionary setObject:curr_date_time forKey:@"deletedTime"];
            [self.localAllDelObjs addObject:dictionary];
          }
        }
        
      }else{
        [self.onlineWinObjs addObject:onlineObj];
        //if local exist but online win , the local document need to delete
        NSArray<NSString *> * localChangedDocIdWithSameCustId = [[NSSet setWithArray:localObj[@"docIds"]] allObjects];
        NSArray<NSString *> * onlineChangedDocIdWithSameCustId = [[NSSet setWithArray:onlineObj[@"docIds"]] allObjects];
        localChangedDocIdWithSameCustId = [localChangedDocIdWithSameCustId filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"NOT (SELF in %@)", onlineChangedDocIdWithSameCustId]];
        if(localChangedDocIdWithSameCustId != nil && [localChangedDocIdWithSameCustId count] > 0){
          NSNumber *curr_date_time = [NSNumber numberWithLongLong:[[UtilsHandler shareInstance] getTimeMillis:[NSDate date]]];
          for (NSString *docid in localChangedDocIdWithSameCustId) {
            NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
            [dictionary setObject:docid forKey:@"clientId"];
            [dictionary setObject:curr_date_time forKey:@"deletedTime"];
            [self.onlineAllDelObjs addObject:dictionary];
          }
        }
      }
    }
  }
  //*******************************************************************************************************************************
}

//***********************merge the document according to winObjs***********************
- (void) mergeDocuments{
  [self mergeDocument2Local:self.onlineWinObjs];
  [self mergeDocument2Online:self.localWinObjs];
  //handle deleted
  [self deleteLocalDocumentByOnline:self.onlineAllDelObjs];
  [self deleteOnlineDocumentByLocal:self.localAllDelObjs];
  //handle agent profile
  [self mergeDocument2LocalWithDocIds:self.agentProfileDocIds];
}

- (void) deleteLocalDocumentByOnline:(NSMutableArray<NSDictionary *>*) delObjs{
  if(delObjs != nil && delObjs.count > 0){
    NSError* error;
    for (NSDictionary *obj in delObjs) {
      long long deletedTime = [obj[@"deletedTime"] longLongValue];
      NSString *clientId = obj[@"clientId"];
      CBLDocument* doc = [self.localEncryptDb documentWithID: clientId];
      long long lstChgTime = [doc[@"lstChgDate"] longLongValue];
      if (deletedTime - lstChgTime > 0) {
        if (![doc deleteDocument: &error]) {
          NSLog(@"=====deleteLocalDocumentByOnline faild[%@]", clientId);
        }
      }
    }
  }
}

- (void) deleteOnlineDocumentByLocal:(NSMutableArray<NSDictionary *>*) delObjs{
  if(delObjs != nil && delObjs.count > 0){
    NSError* error;
    for (NSDictionary *obj in delObjs) {
      long long deletedTime = [obj[@"deletedTime"] longLongValue];
      NSString *clientId = obj[@"clientId"];
      CBLDocument* doc = [self.localSyncDb documentWithID: clientId];
      long long lstChgTime = [doc[@"lstChgDate"] longLongValue];
      if (deletedTime - lstChgTime > 0) {
        if (![doc deleteDocument: &error]) {
          NSLog(@"=====deleteOnlineDocumentByLocal faild[%@]", clientId);
        }
      }
    }
  }
}

- (void) mergeDocument2Online:(NSMutableArray<NSDictionary *>*) WinObjs{
  if(WinObjs != nil && WinObjs.count > 0){
    NSLog(@"=====local encrypt database 2 local sync database");
    for (NSDictionary *obj in WinObjs) {
      for (NSString* docId in obj[@"docIds"]) {
        //filter policy document, bcz policy document only web can changed
        if ([self.policyDocIds indexOfObject:docId] == NSNotFound) {
          //need to replace the revId then save to db
          [self replaceDoument2DB:docId sourceDb:self.localEncryptDb targetDocId:docId targetDb:self.localSyncDb];
        }
      }
    }
  }
}

- (void) mergeDocument2Local:(NSMutableArray<NSDictionary *>*) WinObjs{
  if(WinObjs != nil && WinObjs.count > 0){
    NSLog(@"=====local sync database 2 local encrypt database");
    for (NSDictionary *obj in WinObjs) {
      for (NSString* docId in obj[@"docIds"]) {
        //need to replace the revId then save to db
        [self replaceDoument2DB:docId sourceDb:self.localSyncDb targetDocId:docId targetDb:self.localEncryptDb];
      }
    }
  }
  
  if (self.policyDocIds != nil && self.policyDocIds.count > 0) {
    for (NSString *policyDocId in self.policyDocIds) {
      [self replaceDoument2DB:policyDocId sourceDb:self.localSyncDb targetDocId:policyDocId targetDb:self.localEncryptDb];
    }
  }
}

- (void) mergeDocument2LocalWithDocIds:(NSMutableArray<NSString *>*) docIds{
  if(docIds != nil && docIds.count > 0){
    NSLog(@"=====handle agent profile , from local sync database 2 local encrypt database");
    for (NSString* docId in docIds) {
      //need to replace the revId then save to db
      [self replaceDoument2DB:docId sourceDb:self.localSyncDb targetDocId:docId targetDb:self.localEncryptDb];
    }
  }
}

- (void) replaceDoument2DB:(NSString *) sourceDocId sourceDb:(CBLDatabase *)sourceDb targetDocId:(NSString *) targetDocId targetDb :(CBLDatabase *) targetDb{
  NSError* error;
  CBLDocument* sourceDoc = [sourceDb documentWithID: sourceDocId];
  CBLRevision* sourceRev = sourceDoc.currentRevision;
  
  //replace the revNumber
  NSMutableDictionary* sourceProperties = [sourceDoc.properties mutableCopy];
  
  CBLDocument* targetDoc = [targetDb existingDocumentWithID: targetDocId];
  if(targetDoc == nil) {
    targetDoc = [targetDb documentWithID: targetDocId];//create document with docid
    [sourceProperties removeObjectForKey:@"_rev"];
  }else{
    NSLog(@"document exist [%@], the rev = %@, current rev = %@",sourceDocId, sourceProperties[@"_rev"], targetDoc.currentRevisionID);
    sourceProperties[@"_rev"] = targetDoc.currentRevisionID;
  }
  
  if(sourceRev.attachments){
    [sourceProperties removeObjectForKey:@"_attachments"];
  }

  if (![targetDoc putProperties: sourceProperties error: &error]) {
    NSLog(@"replaceDoument2DB has error : %@", error);
  }
  
  //handle the attachments
  if(sourceRev.attachments){
    CBLUnsavedRevision* newRev = [targetDoc.currentRevision createRevision];
    for (CBLAttachment* att in sourceRev.attachments){
      [newRev setAttachmentNamed:att.name withContentType:att.contentType content:att.content];
    }
    [newRev save:&error];
  }
}

//Query the file list modified by the local database after the last synchronization
- (NSArray*) selectLocalChangedDocsFromLstSync {
  NSLog(@"=====last data sync date : [%@]", [[UtilsHandler shareInstance] date2String:self.lastSyncDate]);

  NSMutableArray* ret = nil;
  NSError *error;
  
  CBLQuery* query = [self.localEncryptDb createAllDocumentsQuery];
  query.allDocsMode = kCBLAllDocs;
  CBLQueryEnumerator *rowEnum = [query run:&error];
  if (rowEnum == nil || rowEnum.count == 0) {
    NSLog(@"=====the rowEnum is null or rowEnum count = 0");
    return nil;
  }
  
  ret = [NSMutableArray array];
  long long lastSyncTime = [[UtilsHandler shareInstance] getTimeMillis:self.lastSyncDate];
  for (CBLQueryRow *row in rowEnum) {
    NSDictionary* properties = row.document.properties;
    long long lstChgTime = [properties[@"lstChgDate"] longLongValue];
    if (([properties[@"agentCode"] isEqualToString:self.agentCode]) && (lstChgTime - lastSyncTime > 0)) {
      [ret addObject:row.document];
    }
  }
  
  /**
  NSArray *select = @[@"_id",@"lstChgDate",@"type"];
  
  NSString *lastSyncDateTime = [NSString stringWithFormat:@"%lld", [[UtilsHandler shareInstance] getTimeMillis:self.lastSyncDate]];
  NSPredicate *where = [NSPredicate predicateWithFormat:@"lstChgDate>%@ && agentCode=%@", lastSyncDateTime, self.agentCode];
  
  CBLQueryBuilder *queryBuilder = [[CBLQueryBuilder alloc] initWithDatabase:self.localEncryptDb
                                                                     select:select
                                                             wherePredicate:where
                                                                    orderBy:nil
                                                                      error:&error];
  
  CBLQuery* query = [queryBuilder createQueryWithContext:nil];
  
  CBLQueryEnumerator *rowEnum = [query run:&error];
  if (rowEnum == nil || rowEnum.count == 0) {
    NSLog(@"=====the rowEnum is null or rowEnum count = 0");
    return nil;
  }
  ret = [NSMutableArray array];
  NSLog(@"=====the selectLocalChangedDocsFromLstSync count: [%lu]", (unsigned long)rowEnum.count);
  for (CBLQueryRow *row in rowEnum) {
    [ret addObject:row.document];
  }
  */
  return ret;
}

- (void) dataSyncCreateLog:(NSString *) status syncDate:(NSString *) syncDate{
  NSLog(@"=====this is add data sync audit log into local db when begin");
  NSError *error;
  NSDate *curr_date = [NSDate date];
  NSLog(@"=====data sync start time : [%@]==",[[UtilsHandler shareInstance] date2String:curr_date]);
  //add audit log into local db
  NSString *docId = [NSString stringWithFormat:@"%@%@%@%@%@", @"SYNC_AUD", @"_", self.agentCode,@"_", [NSString stringWithFormat:@"%lld", [[UtilsHandler shareInstance] getTimeMillis:curr_date]]];
  NSLog(@"=====audit log document id : [%@]==", docId);
  
  NSString *deviceUUID = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
  
  NSDictionary* properties = @{@"id":             docId,
                               @"agentCode":      self.agentCode,
                               @"deviceUUID":     deviceUUID,
                               @"status":         status,
                               @"syncDate":       syncDate == nil ? @"" : syncDate,
                               @"type":           @"SYNC_AUD",
                               };
  
  CBLDocument* document = [self.localEncryptDb documentWithID: docId];
  
  if (![document putProperties: properties error: &error]) {
    NSLog(@"=====add data sync audit log has an error: %@", error);
  }else{
    self.currentAuditLogDocId = docId;
    NSLog(@"=====add data sync audit log success");
  }
}

-(void) saveLastSyncDateToKeychain:(NSDate *) curr_date{
  //save the last sync date & time into keychain
  UtilsHandler *utilsHandler = [UtilsHandler shareInstance];
  NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
  formatter.dateFormat = @"yyyy-MM-dd'T'HH:mm:ss:SSSZ";
  NSString *lastSyncDate = [formatter stringFromDate:curr_date];
  NSString *lastSyncDateTime = [NSString stringWithFormat:@"%lld", [utilsHandler getTimeMillis:curr_date]];
  [utilsHandler saveToKeychainWithKey:LAST_SYNC_DATE_KEY_IN_KEYCHAIN value:lastSyncDate];
  [utilsHandler saveToKeychainWithKey:LAST_SYNC_TIME_KEY_IN_KEYCHAIN value:lastSyncDateTime];
}

- (void) dataSyncCompletedLog:(NSDate *) curr_date{
  NSLog(@"=====this is add data sync audit log into local db.");
  NSError *error;
  
  //NSString* lastSyncDateOfKeychain = (NSMutableDictionary *)[[UtilsHandler shareInstance] loadFromKeychainWithKey:LAST_SYNC_DATE_KEY_IN_KEYCHAIN];
  //NSLog(@"=====lastSyncDateOfKeychain : %@", lastSyncDateOfKeychain);
  //add audit log into local db
  NSLog(@"=====data sync end time : [%@]==",[[UtilsHandler shareInstance] date2String:curr_date]);

  CBLDocument* document = [self.localEncryptDb existingDocumentWithID: self.currentAuditLogDocId];
  if (document != nil) {
    NSMutableDictionary* p = [document.properties mutableCopy];
    p[@"status"] = SUCCESS;
    p[@"syncDate"] = [[UtilsHandler shareInstance] date2String:curr_date];
    if (![document putProperties: p error: &error]) {
      NSLog(@"=====update data sync audit log has an error: %@", error);
    }else{
      NSLog(@"=====update data sync audit log success");
    }
  }else{
    NSLog(@"=====the data sync audit log does not exist.");
  }
}


-(void) checkGeneralDataSync{
  //check network
  self.internetStatus = [self internetConnection];
  if (self.internetStatus == ReachableViaWiFi || self.internetStatus == ReachableViaWWAN) {
    [self callWSOnlineChangedDocs];
  } else{
    self.descriptionLabel.text = @"No network, Please connect your device to online mode and retry again.";
    self.horizontalLineView.hidden = NO;
    self.rightButton.hidden = NO;
    [self.rightButton setTitle:@"Re-Try" forState:UIControlStateNormal];
    [self.rightButton removeTarget:self action:nil forControlEvents:UIControlEventTouchDown];
    [self.rightButton addTarget:self action:@selector(retryCheckGeneralDataSync) forControlEvents:UIControlEventTouchDown];
  }
}

-(void) handleWebServiceSuccess:(NSDictionary *) ret{
  if (ret!=nil && [[ret objectForKey:@"statusCode"] intValue] == 200) {//success
    [self clearLocalSyncDb];
    [self getOnlineEsubOrEpayOrChangedDocs:[ret objectForKey:@"data"]];
    //get the changed document in local db
    [self getLocalEsubOrEpayOrChangedDocs];
    
    dispatch_semaphore_t semaphore = dispatch_semaphore_create(0);
    //get the agent profile document ids
    [self getAgtProfileDocIds:semaphore];
    dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER);
    
    //set the data sync filter with documentIds
    [self setPullFilterDocIds];
    //if online / local any changed then show data sync view else redirect to MainView
    if (self.docIds != nil && self.docIds.count > 0) {
      NSLog(@"=====data sync with docids : %@", self.docIds);
      self.dataSyncStatus = @"DOWNLOAD";
      self.localAllNotExistingDocIds = [NSMutableArray array];
      NSLog(@"=====begin to backup the local changed documents");
      for (NSString* docid in self.docIds) {
        [self backupDocumentWithID:docid];
      }
      //begin data sync
      [self dataSync];
    }else{
      //[self.progressView setProgress:1 animated:YES];
      self.progressView.hidden = YES;
      self.percentageLabel.hidden = YES;
      self.descriptionLabel.text = @"Your data is up to date.";
      //self.horizontalLineView.hidden = NO;
      //self.rightButton.hidden = NO;
      //self.leftButtonLeftConstraint.constant = 0;
      //[self.rightButton setTitle:@"Ok" forState:UIControlStateNormal];
      //[self.rightButton removeTarget:self action:nil forControlEvents:UIControlEventTouchDown];
      //[self.rightButton addTarget:self action:@selector(hideView) forControlEvents:UIControlEventTouchDown];
      
      dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self hideView];
        NSDate *curr_date = [NSDate date];
        //save last sync date into keychain
        [self saveLastSyncDateToKeychain:curr_date];
        [[UtilsHandler shareInstance] saveToKeychainWithKey:LAST_SYNC_STATUS_KEY_IN_KEYCHAIN value:SUCCESS];
        //insert data sync audit log
        [self dataSyncCreateLog:SUCCESS syncDate:[[UtilsHandler shareInstance] date2String:curr_date]];
        //send message to react native
        DataSyncEventManager *dataSyncEvent = [DataSyncEventManager allocWithZone:nil];
        [dataSyncEvent sendDataSyncStatus:SUCCESS];
      });
    }
    
  }else{//web service has error
    NSString *msgFormat = @"Failed! (Err-W%ld)\nPlease connect your device to online mode and retry again or contact your systems administrator for assistance";
    NSString *message = @"";
    if (ret == nil) {
      message = [NSString stringWithFormat:msgFormat, -1];
    }else{
      NSError *error = [ret objectForKey:@"error"];
      NSNumber *statusCode = [ret objectForKey:@"statusCode"];
      if (error != nil) {
        message = [NSString stringWithFormat:msgFormat, (long)error.code];
      }else{
        if ([statusCode intValue] == 401) {
          message = @"The token is invalid, please login again";
        }else{
          message = [NSString stringWithFormat:msgFormat, (long)statusCode];
        }
      }
    }
    
    if (self.isBackground) {
      DataSyncEventManager *dataSyncEvent = [DataSyncEventManager allocWithZone:nil];
      [dataSyncEvent sendDataSyncStatus:message];
      [self hideView];
    }else{
      self.descriptionLabel.text = message;
      self.horizontalLineView.hidden = NO;
      self.rightButton.hidden = NO;
      self.leftButtonLeftConstraint.constant = 0;
      [self.rightButton setTitle:@"Re-Try" forState:UIControlStateNormal];
      [self.rightButton removeTarget:self action:nil forControlEvents:UIControlEventTouchDown];
      [self.rightButton addTarget:self action:@selector(retryCheckGeneralDataSync) forControlEvents:UIControlEventTouchDown];
    }
  }
}

-(void) callWSOnlineChangedDocs
{
  //hidden the data sync button
  self.titleLabel.text = @"Data Synchronization";
  self.descriptionLabel.text = @"Progressing...";
  self.progressView.hidden = NO;
  self.percentageLabel.hidden = NO;
  self.progressView.progress = 0;
  self.percentageLabel.text = @"0%";
  self.horizontalLineView.hidden = YES;
  self.leftButton.hidden = YES;
  self.rightButton.hidden = YES;
  self.verticalLineView.hidden = YES;
  
  NSLog(@"===========callWebService===========");
  NSString *wsMethod = @"/dataSync/onlineList";
  NSString *webUrl = [NSString stringWithFormat:@"%@?agentCode=%@&lstChgDate=%lli", [self.wsurl stringByAppendingString:wsMethod], self.agentCode, [[UtilsHandler shareInstance] getTimeMillis:self.lastSyncSuccessDate]];
  NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
  NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration];
  NSURL *serUrl = [NSURL URLWithString:webUrl];
  NSLog(@"=====callWebService, URL[%@], authToken[%@]", webUrl, self.authToken);
  
  NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:serUrl
                                                         cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                     timeoutInterval:60.0];
  [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
  [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
  [request setValue:self.authToken forHTTPHeaderField:@"Authorization"];
  [request setHTTPMethod:@"GET"];
  
  //dispatch_semaphore_t semaphore = dispatch_semaphore_create(0);
  NSURLSessionDataTask* getDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData* data, NSURLResponse* response, NSError* error) {
    NSMutableDictionary *ret = [NSMutableDictionary dictionaryWithCapacity:3];
    if (error!=nil) [ret setObject:error forKey:@"error"];
    NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
    NSNumber *statusCode  = [[NSNumber alloc] initWithLong:[httpResponse statusCode]];
    if (statusCode!=nil) [ret setObject:statusCode forKey:@"statusCode"];
    if (data!=nil) [ret setObject:data forKey:@"data"];
    [self performSelectorOnMainThread:@selector(handleWebServiceSuccess:) withObject:ret waitUntilDone:YES];
    //dispatch_semaphore_signal(semaphore);
  }];
  [getDataTask resume];
  //dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER);
}

- (void) backupDocumentWithID: (NSString *) docId{
  NSError *error;
  NSLog(@"=====backup document : %@",docId);
  if(self.localEncryptDb != nil) {
    CBLDocument* fromdoc = [self.localEncryptDb existingDocumentWithID: docId];
    if (fromdoc != nil) {
      CBLRevision* rev = fromdoc.currentRevision;
      
      NSString *backdocId = [NSString stringWithFormat:@"%@%@",@"BACKUP_",docId];
      NSMutableDictionary* p = [fromdoc.properties mutableCopy];
      //create backup document
      CBLDocument* document = [self.localSyncDb documentWithID: backdocId];
      if(document != nil) {
        p[@"_id"] = document.documentID;
        p[@"_rev"] = document.currentRevisionID;
      }else{
        [p removeObjectForKey:@"_id"];
        [p removeObjectForKey:@"_rev"];
      }
      [p removeObjectForKey:@"_attachments"];
      if (![document putProperties: p error: &error]) {
        NSLog(@"=====HAS ERROR %@",error);
      }
      if(rev.attachments){
        NSError* error;
        CBLUnsavedRevision* newRev = [document.currentRevision createRevision];
        for (CBLAttachment* att in rev.attachments){
          [newRev setAttachmentNamed:att.name withContentType:att.contentType content:att.content];
          [newRev saveAllowingConflict:&error];
        }
      }
    }else{
      [self.localAllNotExistingDocIds addObject:docId];
    }
    
  }
}

- (void) restoreBackupDocuments{
  NSError *error;
  NSPredicate *where = [NSPredicate predicateWithFormat:@"_id LIKE 'BACKUP_*'"];
  
  CBLQueryBuilder *queryBuilder = [[CBLQueryBuilder alloc] initWithDatabase:self.localSyncDb
                                                                     select:@[@"_id"]
                                                             wherePredicate:where
                                                                    orderBy:nil
                                                                      error:&error];
  
  CBLQuery* query = [queryBuilder createQueryWithContext:nil];
  CBLQueryEnumerator *rowEnum = [query run:&error];
  NSLog(@">>>>>>>>>>>>>>>>>  %lu",(unsigned long)rowEnum.count);
  if (rowEnum != nil && rowEnum.count > 0) {
    //restore document
    for (CBLQueryRow *row in rowEnum) {
      CBLDocument* doc = row.document;
      if (doc != nil) {
        NSLog(@"restore document with id[%@],backID[%@]",doc[@"_id"],[doc[@"_id"] substringFromIndex:[doc[@"_id"] rangeOfString:@"BACKUP_"].location]);
        [self replaceDoument2DB:doc[@"_id"] sourceDb:self.localSyncDb targetDocId:[doc[@"_id"] substringFromIndex:[doc[@"_id"] rangeOfString:@"BACKUP_"].location] targetDb:self.localEncryptDb];
      }
    }
  }
  
  //remove the new documents in local db
  if (self.localAllNotExistingDocIds != nil && self.localAllNotExistingDocIds.count > 0) {
    for (NSString* docid in self.localAllNotExistingDocIds) {
      CBLDocument* doc = [self.localEncryptDb documentWithID: docid];
      if (![doc deleteDocument: &error]) {
        NSLog(@"delete local NotExisting document[%@] has an error : %@", docid, error);
      }
    }
  }
  
  //delete the data sync database and remove the backup documents
  //[self deleteSyncDb];
}



//use sync gateway rest api to get agent manager docid
-(void)getAgtProfileDocIds:(dispatch_semaphore_t)semaphore
{
  self.agentProfileDocIds = [NSMutableArray array];
  NSLog(@"=====get the count use sync gateway rest api when downAgentProfile function begin");
  NSString *agentDocId = [@"U_" stringByAppendingString:self.loginId];
  NSString *agentAttachDocId = [@"UX_" stringByAppendingString:self.loginId];
  NSString *eligProductsDocId = @"eligProducts";
  NSString *rlsstatusDocId = [[@"U_" stringByAppendingString:self.loginId] stringByAppendingString:@"_RLSSTATUS"];
  //add docid to agentProfileDocIds
  [self.agentProfileDocIds addObject:agentDocId];
  [self.agentProfileDocIds addObject:agentAttachDocId];
  [self.agentProfileDocIds addObject:eligProductsDocId];
  [self.agentProfileDocIds addObject:rlsstatusDocId];
  //get upline2Code
  NSString *upline2Code;
  CBLDocument* agentDoc = [self.localEncryptDb existingDocumentWithID: agentDocId];
  if (agentDoc != nil) {
    NSMutableDictionary* p = [agentDoc.properties mutableCopy];
    upline2Code = [p valueForKeyPath:@"rawData.upline2Code"];
  }
  NSLog(@"=====agent document id is [%@], the upline2Code is [%@]", agentDocId, upline2Code);
  if (upline2Code != nil && upline2Code.length > 0) {
    NSString *compCode = @"01";
    NSString *wsMethod = @"/dataSync/getUplineProfileDocId";
    NSString *webUrl = [NSString stringWithFormat:@"%@?upline2Code=%@", [self.wsurl stringByAppendingString:wsMethod], upline2Code];

    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration];
    NSURL *serUrl = [NSURL URLWithString:webUrl];
    NSLog(@"=====downAgentProfile, URL[%@], authToken[%@]", webUrl, self.authToken);
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:serUrl
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:60.0];

    
    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    [request setValue:self.authToken forHTTPHeaderField:@"Authorization"];
    [request setHTTPMethod:@"GET"];
    
    NSURLSessionDataTask *getDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
      if (error == nil) {
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
        NSNumber *statusCode  = [[NSNumber alloc] initWithLong:[httpResponse statusCode]];
        if ([statusCode intValue] == 200) {
          NSError* error;
          NSDictionary *dataObj = [NSJSONSerialization JSONObjectWithData:data
                                                                  options:NSJSONReadingAllowFragments
                                                                    error:&error];
          NSLog(@"=====downAgentProfile records : %@", dataObj[@"status"]);
          if ([dataObj[@"status"] isEqualToString:@"success"]) {
            NSLog(@"=====downAgentProfile result : %@",[dataObj valueForKeyPath:@"result"]);
            [self.agentProfileDocIds addObjectsFromArray:[dataObj valueForKeyPath:@"result"]];
          }
        }else{
          NSLog(@"=====call the downAgentProfile has response error. [%@]", statusCode);
        }
      } else {
        NSLog(@"=====call the downAgentProfile has web service error. %@", error);
      }
      dispatch_semaphore_signal(semaphore);
    }];
    [getDataTask resume];
  }else{
    dispatch_semaphore_signal(semaphore);
  }
}

//Here is the comparison of all changed documents, returning local new, online new, conflict list
/**
 - (NSDictionary *)compareChangedDocs:(NSArray *)localChangedDocs onlineChangedDocs:(NSArray *)onlineChangedDocs {
 NSDictionary *result = [[NSDictionary alloc] init];
 
 NSArray *conflictDocs = [localChangedDocs filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"SELF in %@", onlineChangedDocs]];
 NSArray *localNewDocs = [localChangedDocs filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"NOT (SELF in %@)", onlineChangedDocs]];
 NSArray *onlineNewDocs = [onlineChangedDocs filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"NOT (SELF in %@)", conflictDocs]];
 
 NSLog(@"There are conflicting:%@", conflictDocs);
 NSLog(@"local add :%@", localNewDocs);
 NSLog(@"online add:%@", onlineNewDocs);
 
 [result setValue:conflictDocs forKey:@"conflictDocs"];
 [result setValue:localNewDocs forKey:@"localNewDocs"];
 [result setValue:onlineNewDocs forKey:@"onlineNewDocs"];
 return result;
 }*/

//- (void) saveLastSyncDateInKeychain:(NSDate *) date{
//  NSLog(@"=====saveLastSyncDateInKeychain begin");
//  NSDateFormatter *formatter = [[NSDateFormatter alloc ] init];
//  [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.sss'Z'"];
//  NSString *lastSyncDateStr = [formatter stringFromDate:date];
//  KeychainItemWrapper *keychain = [[KeychainItemWrapper alloc] initWithIdentifier:@"TIME_BOMB_EXPIRY_DATE" accessGroup:nil];
//  [keychain resetKeychainItem];
//  [keychain setObject:lastSyncDateStr forKey:(id)kSecValueData];
//  NSLog(@"=====get the last sync date in keychain [%@]", [keychain objectForKey:(id)kSecValueData]);
//}

-(void)clearLocalSyncDb{
  NSError *error;
  if ([self.dbManager databaseExistsNamed:self.syncDbName]) {
    CBLDatabase* localSyncDbTemp = [self.dbManager databaseNamed: self.syncDbName error: &error];
    if (![localSyncDbTemp deleteDatabase: &error]) {
      NSLog(@"=====delete data sync database error: %@", error);
    }else {
      NSLog(@"=====delete data sync database success");
    }
  }
  self.localSyncDb = [self.dbManager databaseNamed: self.syncDbName error: &error];
}

- (void) startLoadingView{
  self.indicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
  self.indicatorView.frame = CGRectMake(80, 80, 80, 80);
  self.indicatorView.backgroundColor = [UIColor grayColor];
  self.indicatorView.center = self.view.center;
  [self.indicatorView setAlpha:1];
  self.indicatorView.layer.cornerRadius = 10;
  self.indicatorView.hidesWhenStopped = YES;
  if (self.isBackground) {
    self.view.backgroundColor = [UIColor clearColor];
  }
  [self.view addSubview:self.indicatorView];
  [self.indicatorView startAnimating];
}

- (void) addRetryCount{
  self.retryCount = self.retryCount + 1;
  NSLog(@"=====re-try count : %d", self.retryCount);
}

- (void) stopLoadingView{
  [self.indicatorView stopAnimating];
}

- (void)viewLongPress:(UILongPressGestureRecognizer *) gestureRecognizer{
  if([gestureRecognizer state] == UIGestureRecognizerStateBegan){
    EABTableViewController *eabTableView = [[EABTableViewController alloc] init];
    eabTableView.authToken = self.authToken;
    [self presentViewController:eabTableView animated:YES completion:^{
      NSLog(@"=====show search local view page.");
    }];
  }
}

- (void)didReceiveMemoryWarning {
  [super didReceiveMemoryWarning];
  // Dispose of any resources that can be recreated.
}
@end
