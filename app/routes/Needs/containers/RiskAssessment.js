import { connect } from "react-redux";
import { ACTION_TYPES, REDUCER_TYPES } from "eab-web-api";
import { TEXT_STORE } from "../../../constants/REDUCER_TYPES";
import RiskAssessment from "../components/RiskAssessment";

const { FNA, FNA_RA, OPTIONS_MAP } = REDUCER_TYPES;

/**
 * RiskAssessment
 * @requires RiskAssessment - RiskAssessment UI
 * */
const mapStateToProps = state => ({
  textStore: state[TEXT_STORE],
  optionsMap: state[OPTIONS_MAP],
  productType: state[FNA].na.productType,
  riskPotentialReturn: state[FNA].na.raSection.owner.riskPotentialReturn,
  avgAGReturn: state[FNA].na.raSection.owner.avgAGReturn,
  smDroped: state[FNA].na.raSection.owner.smDroped,
  alofLosses: state[FNA].na.raSection.owner.alofLosses,
  expInvTime: state[FNA].na.raSection.owner.expInvTime,
  invPref: state[FNA].na.raSection.owner.invPref,
  selfSelectedriskLevel: state[FNA].na.raSection.owner.selfSelectedriskLevel,
  selfRLReasonRP: state[FNA].na.raSection.owner.selfRLReasonRP,
  assessedRL: state[FNA].na.raSection.owner.assessedRL,
  passCka: state[FNA].na.ckaSection.owner.passCka,
  isChanged: state[FNA].naIsChanged,
  isValid: state[FNA].na.raSection.isValid
});

const mapDispatchToProps = dispatch => ({
  initRA: callback => {
    dispatch({
      type: ACTION_TYPES[FNA_RA].INIT_RA,
      callback
    });
  },
  saveNA: ({ confirm, callback }) => {
    dispatch({
      type: ACTION_TYPES[FNA].SAVE_NA,
      confirm,
      callback
    });
  },
  updateRAinit: callback => {
    dispatch({
      type: ACTION_TYPES[FNA_RA].UPDATE_RA_INIT,
      callback
    });
  },
  riskPotentialReturnOnChange: newRiskPotentialReturn => {
    dispatch({
      type: ACTION_TYPES[FNA_RA].RISKPOTENTIALRETURN_ON_CHANGE,
      newRiskPotentialReturn
    });
  },
  avgAGReturnOnChange: newAvgAGReturn => {
    dispatch({
      type: ACTION_TYPES[FNA_RA].AVGAGRETURN_ON_CHANGE,
      newAvgAGReturn: newAvgAGReturn.key
    });
  },
  smDropedOnChange: newSmDroped => {
    dispatch({
      type: ACTION_TYPES[FNA_RA].SMDROPED_ON_CHANGE,
      newSmDroped: newSmDroped.key
    });
  },
  alofLossesOnChange: newAlofLosses => {
    dispatch({
      type: ACTION_TYPES[FNA_RA].ALOFLOSSES_ON_CHANGE,
      newAlofLosses: newAlofLosses.key
    });
  },
  expInvTimeOnChange: newExpInvTime => {
    dispatch({
      type: ACTION_TYPES[FNA_RA].EXPINVTIME_ON_CHANGE,
      newExpInvTime: newExpInvTime.key
    });
  },
  invPrefOnChange: newInvPref => {
    dispatch({
      type: ACTION_TYPES[FNA_RA].INVPREF_ON_CHANGE,
      newInvPref: newInvPref.key
    });
  },
  selfSelectedriskLevelOnChange: newSelfSelectedRiskLevel => {
    dispatch({
      type: ACTION_TYPES[FNA_RA].SELF_SELECTED_RISK_LEVEL_ON_CHANGE,
      newSelfSelectedRiskLevel
    });
  },
  selfRLReasonRPOnChange: newSelfRLReasonRP => {
    dispatch({
      type: ACTION_TYPES[FNA_RA].SELF_RL_REASON_RP_ON_CHANGE,
      newSelfRLReasonRP
    });
  },
  updateRAisValid: isValid => {
    dispatch({
      type: ACTION_TYPES[FNA_RA].UPDATE_RA_IS_VALID,
      isValid
    });
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(RiskAssessment);
