import React, { Component } from "react";
import { View, Text, Animated, ScrollView } from "react-native";
import LinearGradient from "react-native-linear-gradient";
import * as _ from "lodash";
import {
  REDUCER_TYPE_CHECK as WEB_API_REDUCER_TYPE_CHECK,
  REDUCER_TYPES,
  NEEDS,
  utilities
} from "eab-web-api";

import SortableList from "../../../../components/SortableList";
import TranslatedText from "../../../../containers/TranslatedText";
import { TEXT_STORE } from "../../../../constants/REDUCER_TYPES";
import APP_REDUCER_TYPES_CHECK from "../../../../constants/REDUCER_TYPE_CHECK";
import EABi18n from "../../../../utilities/EABi18n";
import { ContextConsumer } from "../../../../context";
import Theme from "../../../../theme";
import styles from "./styles";

const { CLIENT, FNA } = REDUCER_TYPES;
const {
  FIPROTECTION,
  CIPROTECTION,
  DIPROTECTION,
  PAPROTECTION,
  PCHEADSTART,
  HCPROTECTION,
  PSGOALS,
  EPLANNING,
  RPLANNING,
  OTHER
} = NEEDS;
const { needsUtil, numberFormatter } = utilities;
const { numberToCurrency } = numberFormatter;

/**
 * FNASummary
 * @description FNASummary page.
 * */
export default class FNASummary extends Component {
  constructor(props) {
    super(props);

    const { na, profile, dependantProfiles, pda } = props;

    this.shortfallOptions = needsUtil.getNeedsSummaryOptions({
      subType: "shortfall",
      fnaValue: na,
      profile,
      dependantProfiles,
      pda
    });
    this.surplusOptions = needsUtil.getNeedsSummaryOptions({
      subType: "surplus",
      fnaValue: na,
      profile,
      dependantProfiles,
      pda
    });

    this.prepareTitle = this.prepareTitle.bind(this);
    this.renderRow = this.renderRow.bind(this);
  }

  prepareTitle({ option, language }) {
    const { textStore } = this.props;

    if (_.isEmpty(option.title)) {
      switch (option.aid) {
        case FIPROTECTION:
          return EABi18n({
            path: "na.needs.fiProtection",
            textStore,
            language
          });
        case CIPROTECTION:
          return EABi18n({
            path: "na.needs.ciProtection",
            textStore,
            language
          });
        case DIPROTECTION:
          return EABi18n({
            path: "na.priority.diProtection",
            textStore,
            language
          });
        case PAPROTECTION:
          return EABi18n({
            path: "na.needs.paProtection",
            textStore,
            language
          });
        case PCHEADSTART:
          return EABi18n({
            path: "na.needs.pcHeadstart",
            textStore,
            language
          });
        case HCPROTECTION:
          return EABi18n({
            path: "na.needs.hcProtection",
            textStore,
            language
          });
        case PSGOALS:
          return EABi18n({ path: "na.needs.psGoals", textStore, language });
        case EPLANNING:
          return EABi18n({ path: "na.needs.ePlanning", textStore, language });
        case RPLANNING:
          return EABi18n({ path: "na.needs.rPlanning", textStore, language });
        case OTHER:
          return EABi18n({ path: "na.needs.other", textStore, language });
        default:
          return "";
      }
    }
    return option.title;
  }

  renderRow({ data, index, language }) {
    const { textStore } = this.props;

    return (
      <Animated.View style={styles.row}>
        <View style={styles.rowIndex}>
          <Text style={styles.rowIndexTitle}>{`${index + 1}.`}</Text>
        </View>
        <View style={styles.rowCard}>
          <View style={styles.rowDataRow}>
            <TranslatedText
              style={styles.rowDataRowTitle}
              path="na.priority.list.forPerson"
              replace={[
                {
                  value: data.name,
                  isPath: false
                }
              ]}
            />
            {_.isNumber(data.timeHorizon) ? (
              <View style={styles.rowData}>
                <Text style={styles.rowDataTitle}>
                  {EABi18n({
                    path: "na.priority.list.timeHorizon",
                    language,
                    textStore
                  })}:
                </Text>
                <TranslatedText
                  style={styles.rowValueNormal}
                  path="na.priority.list.timeHorizonYears"
                  replace={[
                    {
                      value: data.timeHorizon,
                      isPath: false
                    }
                  ]}
                />
              </View>
            ) : null}
          </View>
          <View style={styles.rowDataRow}>
            <Text style={styles.rowDataRowSubTitle}>
              {this.prepareTitle({ option: data, language })}
            </Text>
            {_.has(data, "totShortfall") ? (
              <View style={styles.rowData}>
                <Text style={styles.rowDataTitle}>
                  {EABi18n({
                    path:
                      data.totShortfall < 0
                        ? "na.priority.list.shortfall"
                        : "na.priority.list.surplus",
                    language,
                    textStore
                  })}:
                </Text>
                <Text
                  style={
                    data.totShortfall < 0
                      ? styles.rowValueNegative
                      : styles.rowValuePositive
                  }
                >
                  {numberToCurrency({
                    value: Math.abs(data.totShortfall)
                  })}
                </Text>
              </View>
            ) : null}
          </View>
          <View style={styles.rowDataRow}>
            <Text style={styles.rowDataRowSubTitle} />
            {_.has(data, "totShortfall") &&
            data.totShortfall < 0 &&
            data.aid === "rPlanning" ? (
              <View style={styles.rowData}>
                <Text style={styles.rowDataTitle}>
                  {EABi18n({
                    path: "na.needs.common.annualRetirementIncome",
                    language,
                    textStore
                  })}:
                </Text>
                <Text
                  style={
                    data.totShortfall < 0
                      ? styles.rowValueNegative
                      : styles.rowValuePositive
                  }
                >
                  {numberToCurrency({
                    value: Math.abs(data.annRetireIncome),
                    ccy: "SGD",
                    maxValue: 999999999999999
                  })}
                </Text>
              </View>
            ) : null}
          </View>
        </View>
      </Animated.View>
    );
  }

  render() {
    return (
      <LinearGradient {...Theme.LinearGradientProps} style={styles.container}>
        <ContextConsumer>
          {({ language }) => (
            <ScrollView>
              {!_.isEmpty(this.shortfallOptions[0]) && (
                <SortableList
                  style={styles.list}
                  contentContainerStyle={styles.listContentContainer}
                  showsVerticalScrollIndicator={false}
                  refreshControl={null}
                  sortingEnabled={false}
                  data={this.shortfallOptions[0]}
                  renderRow={({ data, index }) =>
                    this.renderRow({ data, index, language })
                  }
                  renderHeader={() => (
                    <View style={styles.rowHeaderContainer}>
                      <TranslatedText
                        style={styles.rowHeaderTitle}
                        path="na.priority.list.shortfall"
                        language={language}
                      />
                    </View>
                  )}
                  rowColor={styles.rowColor}
                />
              )}
              {!_.isEmpty(this.surplusOptions[0]) && (
                <SortableList
                  style={styles.list}
                  contentContainerStyle={styles.listContentContainer}
                  showsVerticalScrollIndicator={false}
                  refreshControl={null}
                  sortingEnabled={false}
                  data={this.surplusOptions[0]}
                  renderRow={({ data, index }) =>
                    this.renderRow({
                      data,
                      index: this.shortfallOptions[0].length + index,
                      language
                    })
                  }
                  renderHeader={() => (
                    <View style={styles.rowHeaderContainer}>
                      <TranslatedText
                        style={styles.rowHeaderTitle}
                        path="na.priority.list.surplus"
                        language={language}
                      />
                    </View>
                  )}
                  rowColor={styles.rowColor}
                />
              )}
            </ScrollView>
          )}
        </ContextConsumer>
      </LinearGradient>
    );
  }
}

FNASummary.propTypes = {
  textStore: APP_REDUCER_TYPES_CHECK[TEXT_STORE].isRequired,
  na: WEB_API_REDUCER_TYPE_CHECK[FNA].na.isRequired,
  profile: WEB_API_REDUCER_TYPE_CHECK[CLIENT].profile.isRequired,
  dependantProfiles:
    WEB_API_REDUCER_TYPE_CHECK[CLIENT].dependantProfiles.isRequired,
  pda: WEB_API_REDUCER_TYPE_CHECK[FNA].pda.isRequired
};
