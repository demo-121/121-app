import { StyleSheet, Dimensions } from "react-native";
import Theme from "../../../../theme";

/**
 * styles
 * */
export default StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center"
  },
  profileButtonContainer: {
    height: 48,
    width: Dimensions.get("window").width - 312,
    borderBottomWidth: 1,
    borderColor: Theme.lightGrey,
    alignItems: "center"
  },
  profileButtonScrollView: {},
  profileButtonContentContainer: {
    alignItems: "center",
    justifyContent: "center"
  },
  profileButton: {
    marginTop: Theme.alignmentS,
    marginBottom: Theme.alignmentS
  },
  personalDetailsRedWanningView: {
    marginTop: Theme.alignmentXL
  },
  personalDetailsRedWanning: {
    color: Theme.negative,
    fontSize: Theme.fontSizeXXM
  },
  sectionBorderLine: {
    borderTopWidth: 1,
    borderColor: Theme.lightGrey,
    marginTop: Theme.alignmentXL
  },
  personalDetailOfProposerView: {
    marginTop: Theme.alignmentXL,
    flexDirection: "column"
  },
  detailsOfProposerTitle: {
    fontSize: Theme.fontSizeXXXM,
    marginBottom: Theme.alignmentXL,
    fontWeight: "bold",
    color: Theme.brand
  },
  detailsOfProposerFieldTitle: {
    width: 272,
    fontSize: Theme.fontSizeXXM,
    color: Theme.darkGrey
  },
  bold: {
    fontWeight: "bold"
  },
  detailsOfProposerFieldText: {
    fontSize: Theme.fontSizeXXM,
    color: Theme.grey
  },
  detailsOfProposerFieldView: {
    flexDirection: "row"
  },
  scrollView: {
    width: 664
  },
  sectionView: {
    marginTop: Theme.alignmentXL,
    flexDirection: "column"
  },
  sectionTitleGroup: {
    flex: 1,
    flexDirection: "row",
    marginBottom: Theme.alignmentXL,
    alignItems: "center"
  },
  sectionTitle: {
    fontSize: Theme.fontSizeXXXM,
    marginBottom: Theme.alignmentXL,
    fontWeight: "bold",
    color: Theme.brand
  },
  sectionResidentialTitle: {
    fontSize: Theme.fontSizeXXXM,
    fontWeight: "bold",
    color: Theme.brand
  },
  inputContentView: {
    flexDirection: "row",
    flexWrap: "wrap",
    justifyContent: "space-between"
  },
  inputShort: {
    width: 320
  },
  inputMarginLeft: {
    width: 320,
    marginLeft: Theme.alignmentXL
  },
  residentialAddressQuestion: {
    marginTop: Theme.alignmentXL
  },
  residentialAddressQuestionButton: {
    marginTop: Theme.alignmentL
  },
  footerHeight: {
    height: 94
  },
  singpostBranchDetailsView: {
    marginTop: Theme.alignmentXL,
    flexDirection: "column"
  },
  singpostBranchDetailsTextView: {
    flexDirection: "row"
  },
  branchSelector: {
    flex: 1
  },
  branchText: {
    flex: 1,
    marginLeft: Theme.alignmentL
  },
  titleBarPopulateButton: {
    marginLeft: 20
  }
});
