import { connect } from "react-redux";
import { ACTION_TYPES, ACTION_LIST, REDUCER_TYPES } from "eab-web-api";
import { TEXT_STORE } from "../../../constants/REDUCER_TYPES";
import ProductTypes from "../components/ProductTypes";

const { FNA } = REDUCER_TYPES;
const { updateNAValue } = ACTION_LIST[FNA];

/**
 * ProductTypes
 * @requires ProductTypes - ProductTypes UI
 * */
const mapStateToProps = state => ({
  textStore: state[TEXT_STORE],
  profile: state[REDUCER_TYPES.CLIENT].profile,
  dependantProfiles: state[REDUCER_TYPES.CLIENT].dependantProfiles,
  pda: state[REDUCER_TYPES.FNA].pda,
  na: state[REDUCER_TYPES.FNA].na
});

const mapDispatchToProps = dispatch => ({
  updateNAValue({ target, value }) {
    updateNAValue({
      dispatch,
      target,
      value
    });
  },
  initCompletedStep: value => {
    dispatch({
      type: ACTION_TYPES[FNA].INIT_COMPLETED_STEP_SAGA,
      value
    });
  },
  initProductTypes: () => {
    dispatch({
      type: ACTION_TYPES[FNA].INIT_PRODUCT_TYPES
    });
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProductTypes);
