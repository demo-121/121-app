import { connect } from "react-redux";
import { ACTION_TYPES, REDUCER_TYPES, EAPP } from "eab-web-api";
import { TEXT_STORE, POLICYNUMBER } from "../../../constants/REDUCER_TYPES";
import ApplicationScreen from "../components/ApplicationScreen";
import APP_ACTION_TYPES from "../../../constants/ACTION_TYPES";

const {
  APPLICATION,
  SUPPORTING_DOCUMENT,
  PAYMENT_AND_SUBMISSION
} = REDUCER_TYPES;

/**
 * ApplicationScreen
 * @description Application Form page.
 * @requires ApplicationScreen - ApplicationScreen UI
 * */
const mapStateToProps = state => ({
  isDisableEappButton:
    state[REDUCER_TYPES.APPLICATION].component.isDisableEappButton,
  textStore: state[TEXT_STORE],
  currentStep: state[APPLICATION].component.currentStep,
  completedStep: state[APPLICATION].component.completedStep,
  showPlanDetailsView: state[APPLICATION].component.showPlanDetailsView,
  application: state[APPLICATION].application,
  paymentError: state[PAYMENT_AND_SUBMISSION].error,
  error: state[APPLICATION].error,
  isSubmitted: state[APPLICATION].component.isSubmitted,
  crossAge: state[APPLICATION].crossAge,
  isAlertDelayed: state[APPLICATION].component.isAlertDelayed,
  isCrossAgeAlertShow: state[APPLICATION].component.isCrossAgeAlertShow,
  isSignatureExpiryAlertShow:
    state[APPLICATION].component.isSignatureExpiryAlertShow,
  success: state[APPLICATION].component.success,
  submission: state[REDUCER_TYPES.PAYMENT_AND_SUBMISSION].submission
});

const mapDispatchToProps = dispatch => ({
  updateApplicationStep: ({ nextStep, isShield, callback }) => {
    dispatch({
      type: ACTION_TYPES[APPLICATION].SAGA_UPDATE_EAPP_STEP,
      nextStep,
      isShield,
      callback
    });
  },
  updateUiViewPlanDetails: isPlanDetailsView => {
    dispatch({
      type: ACTION_TYPES[APPLICATION].UPDATE_UI_VIEW_PLAN_DETAILS,
      isPlanDetailsView
    });
  },
  saveForm: ({ currentStep, nextStep, actionType, genPDF, callback }) => {
    dispatch({
      type: ACTION_TYPES[APPLICATION].SAVE_APPLICATION_FORM_BEFORE,
      currentStep,
      nextStep,
      actionType,
      genPDF,
      callback
    });
  },
  saveFormShield: ({ currentStep, actionType, callback }) => {
    dispatch({
      type: ACTION_TYPES[APPLICATION].SAVE_APPLICATION_FORM_SHIELD_BEFORE,
      currentStep,
      actionType,
      callback
    });
  },
  getApplicationList: callback => {
    dispatch({
      type:
        ACTION_TYPES[APPLICATION].SAGA_CLOSE_DIALOG_AND_GET_APPLICATION_LIST,
      callback
    });
  },
  changeTabShield: ({ currentStep, nextStep, callback }) => {
    dispatch({
      type: ACTION_TYPES[APPLICATION].SAGA_CHANGE_TAB_SHIELD,
      currentStep,
      nextStep,
      callback
    });
  },
  getSupportingDocument: ({
    isShield,
    appId,
    appStatus = EAPP.appStatus.APPLYING,
    callback
  }) => {
    dispatch({
      type: isShield
        ? ACTION_TYPES[SUPPORTING_DOCUMENT]
            .SAGA_GET_SUPPORTING_DOCUMENT_DATA_SHIELD
        : ACTION_TYPES[SUPPORTING_DOCUMENT].SAGA_GET_SUPPORTING_DOCUMENT_DATA,
      appId,
      appStatus,
      callback
    });
  },
  updateIsCrossAgeAlertShow: newIsCrossAgeAlertShow => {
    dispatch({
      type: ACTION_TYPES[APPLICATION].UPDATE_UI_SHOW_CROSS_AGE_ALERT,
      newIsAlertDelayed: false,
      newIsCrossAgeAlertShow
    });
  },
  updateIsSignatureExpiryAlert: newIsSignatureExpiryAlertShow => {
    dispatch({
      type: ACTION_TYPES[APPLICATION].UPDATE_UI_SIGNATURE_EXPIRY_ALERT,
      newIsAlertDelayed: false,
      newIsSignatureExpiryAlertShow
    });
  },
  updateShownSignatureExpiryAlert: () => {
    dispatch({
      type: ACTION_TYPES[APPLICATION].SAGA_UPDATE_SHOWN_SIGNATURE_EXPIRY_ALERT
    });
  },
  updateApplicationBackdate: callback => {
    dispatch({
      type: ACTION_TYPES[APPLICATION].SAGA_UPDATE_APPLICATION_BACKDATE,
      callback
    });
  },
  invalidateApplication: callback => {
    dispatch({
      type: ACTION_TYPES[APPLICATION].SAGA_INVALIDATE_APPLICATION,
      callback
    });
  },
  counterCheckPolicyNumber: ({
    isShield,
    shieldPolicyNum,
    nonShieldPolicyNum
  }) => {
    dispatch({
      type: APP_ACTION_TYPES[POLICYNUMBER].COUNTER_CHECK_POLICY_NUMBER,
      isShield,
      shieldPolicyNum,
      nonShieldPolicyNum
    });
  },
  genFNA: callback => {
    dispatch({
      type: ACTION_TYPES[REDUCER_TYPES.APPLICATION].SAGA_GEN_FNA,
      callback
    });
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ApplicationScreen);
