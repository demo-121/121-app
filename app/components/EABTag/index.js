import PropTypes from "prop-types";
import React, { PureComponent } from "react";
import { Text, View } from "react-native";
import styles from "./styles";

/**
 * <EABTag />
 * @description tag component. the tag will auto set the title to upper case
 * @param {string} title - the tag title
 * @param {string} textColor - title text color
 * @param {string} backgroundColor - tag background color
 * */
export default class EABTag extends PureComponent {
  render() {
    return (
      <View
        style={[
          styles.container,
          { backgroundColor: this.props.backgroundColor }
        ]}
      >
        <Text style={[styles.title, { color: this.props.textColor }]}>
          {this.props.title.toUpperCase()}
        </Text>
      </View>
    );
  }
}

EABTag.propTypes = {
  title: PropTypes.string.isRequired,
  textColor: PropTypes.string.isRequired,
  backgroundColor: PropTypes.string.isRequired
};
