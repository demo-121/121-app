import { NativeModules } from "react-native";
import {
  createViewsByJs,
  queryByExistingView,
  getDocumentWithID,
  deleteDocumentWithID,
  updateDocumentWithID,
  getAttachmentWithID,
  uploadAttachmentWithBase64,
  deleteAttachmentWithAttName,
  applicationsCount
} from "../utilities/DataManager";
import { emptyId } from "./serverResponseJson";
import config from "./conf/extConfig.json";
import versionFile from "./conf/version.json";

// let _typeof = typeof Symbol === 'function' && typeof Symbol.iterator === 'symbol' ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === 'function' && obj.constructor === Symbol && obj !== Symbol.prototype ? 'symbol' : typeof obj; };

// var cb = require('couchbase');
// let http = require('http');
// // var dbConfig = require('../dbconfig');
// let fs = require('fs');
// let nPath = require('path');
// let request = require('request');
// let fileHandler = require('../FileHandler');
const _ = require("lodash");

// let fnaPdfName = 'fnaReport';
// let proposalPdfName = 'proposal';
// let appPdfName = 'appPdf';
// let logger = global.logger || console;
// let RemoteUtils = require('../utils/RemoteUtils');

// var ViewQuery = cb.ViewQuery;
// var cluster = null;
// var bucket = null;
// testing tag

const queryFromSG = function queryFromSG(vname, params, cb) {
  if (vname === "applicationsCount") {
    applicationsCount(params, cb);
  } else {
    queryByExistingView(vname, params, cb);
  }
};

const getFromSG = function getFromSG(method, name, cb) {
  if (
    name === null ||
    !name ||
    name === undefined ||
    typeof name !== "string"
  ) {
    // logger.log("ERROR: invalid doc name:", name);
    cb(emptyId);
    return;
  }
  // logger.log(`INFO: getFromSG ${uid}: method: ${method}  name: ${name}`);
  // let startTime = new Date();
  if (name.indexOf("_design/") < 0) {
    if (method === "GET") {
      getDocumentWithID(name, cb);
    } else if (method === "DELETE") {
      deleteDocumentWithID(global.session.agent.profileId, name, cb);
    }
  } else {
    cb(null);
  }
  // logger.log(`INFO: getFromSG End: ${uid}MB: Elapsed ${elapsedTimeMs}ms`);
};

const getDoc = function getDoc(docId, callback) {
  getFromSG("GET", docId, callback);
};
module.exports.getDoc = getDoc;

const globalInit = function globalInit() {
  global.optionsMap = {};
  const optionsMapKeys = [
    "marital",
    "country",
    "occupation",
    "industry",
    "ccy",
    { key: "productLine", docId: "productLine_template" },
    "relationship",
    "nationality",
    "countryTelCode",
    "residency",
    "city",
    "cityCategory",
    "assetClass",
    "riskRating",
    "portfolioModels",
    "idDocType",
    "pass"
  ];
  return Promise.all(
    optionsMapKeys.map(
      val =>
        new Promise(resolve => {
          const docId = val.docId || val;
          const key = val.key || val;
          getDoc(docId, doc => {
            global.optionsMap[key] = doc;
            resolve();
          });
        })
    )
  );
};

module.exports.init = function init(callback) {
  if (!global.config) {
    global.config = config;
  }
  global.config.version = versionFile.version;
  global.config.build = versionFile.build;
  global.logger = console;
  global.debugLog = NativeModules.Logger;
  global.logger.error = global.logger.log;
  global.redisClient = {
    get: () => {},
    set: () => {}
  };

  globalInit().then(() => {
    getDoc("sysParameter", data => {
      if (data && !data.error) {
        const rev = data._rev;
        data._id = "";
        data._rev = "";
        global.config = _.merge(global.config, data, { fileKey: rev });
        callback({ success: true });
      } else if (data && data.error) {
        callback({ success: false, data, error: data.error });
      } else {
        callback({ success: false });
      }
    });
  });
};

module.exports.createView = function createView(ddname, view, callback) {
  if (view != null) {
    const viewsJson = JSON.parse(view);
    createViewsByJs(viewsJson.views, result => {
      callback(result);
    });
  }
};

const getViewRangeNHAF = function getViewRangeNHAF(
  ddname,
  vname,
  start,
  end,
  params,
  callback
) {
  // var query = ViewQuery.range(start, end, true);
  // bucket.query(query, function(err, result) {
  //   if (err) {
  //     callback(false)
  //   } else {
  //     callback(result);
  //   }
  // });
  const query = {
    startkey: start || null,
    endkey: end || null
  };

  if (params) {
    params.forEach((val, key) => {
      query[key] = val;
    });
  }

  // get session
  try {
    queryFromSG(vname, query, res => {
      if (typeof callback === "function") {
        callback(res);
      }
    });
  } catch (ex) {
    callback(false);
  }
};
module.exports.getViewRangeNHAF = getViewRangeNHAF;

const getViewRange = function getViewRange(
  ddname,
  vname,
  start,
  end,
  params,
  callback
) {
  const query = {
    startkey: start || null,
    endkey: end || null
  };

  if (params) {
    Object.keys(params).forEach(p => {
      query[p] = params[p];
    });
  }

  try {
    queryFromSG(vname, query, result => {
      if (typeof callback === "function") {
        callback(result);
      }
    });
  } catch (ex) {
    callback(false);
  }
};
module.exports.getViewRange = getViewRange;

module.exports.getViewByKeys = function getViewByKeys(
  ddname,
  vname,
  keys,
  params,
  batchSize = 40
) {
  let rows = [];
  let totalCnt = 0;
  let promise = Promise.resolve();

  const _loop = function _loop(i) {
    const batchKeys = _.slice(keys, i, i + batchSize);
    const batchParams = Object.assign({}, params, {
      keys: `[${_.join(batchKeys, ",")}]`
    });
    promise = promise.then(
      () =>
        new Promise(resolve => {
          getViewRange(ddname, vname, null, null, batchParams, result => {
            if (result && !result.error) {
              totalCnt += result.total_rows;
              rows = rows.concat(result.rows);
            }
            resolve();
          });
        })
    );
  };

  for (let i = 0; i < keys.length; i += batchSize) {
    _loop(i);
  }
  return promise.then(() => ({
    total_rows: totalCnt,
    rows
  }));
};

module.exports.updateViewIndex = function updateViewIndex(
  ddname,
  vname,
  callback
) {
  // get session
  try {
    // change to be background process
    const backUpdateView = function backUpdateView() {
      // const startTime = new Date();
      queryFromSG(vname, null, res => {
        // const elapsedTimeMs = new Date() - startTime;
        // logger.log(
        //   `View update completed: ${
        //       vname
        //       }, Elapsed ${
        //       elapsedTimeMs
        //       }ms`
        // );
        if (typeof callback === "function") {
          callback(res);
        }
      });
    };
    setTimeout(backUpdateView, 5);
  } catch (ex) {
    if (typeof callback === "function") {
      callback(false);
    }
    // logger.error("ERROR:::::: updateViewIndex::::::::::", ex);
  }
};

module.exports.getDocFromCacheFirst = function getDocFromCacheFirst(
  id,
  callback
) {
  getDoc(id, result => {
    if (result && (result._id || !result.error)) {
      callback(result);
    } else {
      callback(false);
    }
  });
};

module.exports.updDoc = function updDoc(docId, data, callback) {
  updateDocumentWithID(encodeURIComponent(docId), data, callback);
};

module.exports.delDoc = function delDoc(docId, callback) {
  getFromSG("DELETE", docId, callback);
};

module.exports.delDocWithRev = function delDocWithRev(docId, rev, callback) {
  getFromSG("DELETE", docId, callback);
};

module.exports.getAttachment = function getAttachment(
  docId,
  attName,
  callback
) {
  getAttachmentWithID(encodeURIComponent(docId), attName, callback);
};

// module.exports.getBinaryAttachment = function (docId, attName, callback) {
//   getAttachmentByBinary(`${docId  }/${  attName}`, callback);
// };

// module.exports.getBinaryDocument = function (name, callback) {
//   getAttachmentByBinary(name, callback);
// };

module.exports.setAttachment = function setAttachment(
  docId,
  attName,
  mime,
  data,
  callback
) {
  uploadAttachmentWithBase64(docId, attName, mime, data, callback);
};

module.exports.delAttachment = function delAttachment(
  docId,
  attName,
  callback
) {
  deleteAttachmentWithAttName(docId, attName, callback);
};

// by binary
// var getAttachmentByBinary = function getAttachmentByBinary(name, cb) {
//   let options = getOptions('GET', name);
//   let uid = RemoteUtils.SecureRandom(8);
//   logger.log(`INFO: getAttachmentByBinary: ${  uid  }: Options: `, options.path);
//   let startTime = new Date();
//   let req = http.request(options, (res) => {
//     var data = '';
//     res.on('data', function (chunk) {
//       cb(chunk);
//     });
//     res.on('end', function () {
//       var elapsedTimeMs = new Date() - startTime;
//       logger.log('INFO: getAttachmentByBinary: ' + uid + ': End: Elapsed ' + elapsedTimeMs + 'ms');
//       cb(false);
//     });
//   }).on('error', (e) => {
//     logger.error('SG Error: ', e);
//     cb({ error: e });
//   });
//   req.end();
// };

// var deleteFromSG = function deleteFromSG(method, name, rev, updateIndex, cb) {
//   if (name && typeof name !== 'string') {
//     logger.log('ERROR: invalid doc name:', name);
//     cb(false);
//     return;
//   }
//   let options = getOptions(method, `${encodeURIComponent(name)  }?rev=${  rev  }&stale=${  updateIndex ? 'false' : 'ok'}`);
//   let uid = RemoteUtils.SecureRandom(8);
//   logger.log(`INFO: deleteFromSG ${  uid  }: Options: `, options.path);
//   let startTime = new Date();
//   let req = http.request(options, (res) => {
//     var resp = '';
//     res.setEncoding('utf8');
//     res.on('data', function (chunk) {
//       resp += chunk;
//     });
//     res.on('end', function () {
//       var elapsedTimeMs = new Date() - startTime;
//       var dataLength = 0;
//       try {
//         dataLength = Math.round(resp.length / 1048576 * Math.pow(10, 4)) / Math.pow(10, 4);
//       } catch (ex) {
//         dataLength = 'error';
//       }
//       logger.log('INFO: deleteFromSG ' + uid + ': End: , Elapsed ' + elapsedTimeMs + 'ms: ' + dataLength + 'MB');
//       if (typeof cb == 'function') {
//         if (resp) {
//           resp = JSON.parse(resp);
//           cb(resp);
//         } else {
//           // logger.log('Error: deleteFromSG non-string resp?', resp);
//           cb(false);
//         }
//       }
//     });
//   }).on('error', (e) => {
//     logger.log('SG Error: ', e);
//   });
//   req.end();
// };

// module.exports.updFileToSG = function (docId, attchId, rev, filename, mime, cb) {
//   let options = getOptions('PUT', `${encodeURIComponent(docId)  }/${  encodeURIComponent(attchId)  }?rev=${  rev}`, {
//     'Content-Type': mime,
//     'Content-Transfer-Encoding': 'binary',
//   });
//   let uid = RemoteUtils.SecureRandom(8);
//   logger.log(`INFO: updFileToSG: ${  uid  }: Options: `, options.path);

//   let root = global.root || __dirname;
//   let filePath = nPath.join(global.rootPath, global.config.tempFolder, filename);
//   try {
//     fileHandler.decryptFile(filePath, () => {
//       var startTime = new Date();
//       var req = http.request(options, function (res) {
//         var resp = '';
//         res.on('data', function (chunk) {
//           resp += chunk;
//         });
//         res.on('end', function () {
//           var elapsedTimeMs = new Date() - startTime;
//           logger.log('INFO: updFileToSG ' + uid + ': End: Elapsed ' + elapsedTimeMs + 'ms');
//           if (typeof cb == 'function') {
//             if (resp) {
//               resp = JSON.parse(resp);
//               fileHandler.removeFiles(filePath);
//               cb(resp);
//             } else {
//               logger.log('ERROR: updFileToSG:', resp);
//               cb(false);
//             }
//           }
//         });
//       }).on('error', function (e) {
//         logger.log('SG Error: ', e);
//         fileHandler.removeOriginalFile(filePath);
//       });

//       fs.createReadStream(filePath).pipe(req);
//     });
//   } catch (e) {
//     logger.log('ERROR: decrypt exception:', e, filePath);
//   }
// };

module.exports.uploadAttachmentByBase64 = function uploadAttachmentByBase64(
  docId,
  attchId,
  rev,
  data,
  mime,
  cb
) {
  uploadAttachmentWithBase64(docId, attchId, mime, data, cb);
};

// var updToSG = function updToSG(method, name, mime, data, cb) {
//   let options = getOptions(method, name, { 'Content-type': mime });

//   let req = http.request(options, (res) => {
//     var resp = '';
//     res.setEncoding('utf8');
//     res.on('data', function (chunk) {
//       resp += chunk;
//     });
//     res.on('end', function () {
//       if (cb && typeof cb == 'function') {
//         if (resp) {
//           try {
//             resp = JSON.parse(resp);
//             // logger.log('updata to SG result:', resp);
//             if (resp.ok && !resp.error) {
//               cb(resp);
//             } else {
//               logger.error('update CB error:', resp.error, options);
//               cb(false);
//             }
//           } catch (e) {
//             // calling error
//             logger.error('update CB error:', e, resp, options);
//             cb(false);
//           }
//         } else {
//           cb(false);
//         }
//       }
//     });
//   }).on('error', (e) => {
//     logger.log('ERROR:: SG Error: ', e);
//   });
//   if (data) {
//     if ((typeof data === 'undefined' ? 'undefined' : _typeof(data)) == 'object') {
//       data = JSON.stringify(data);
//     }
//     req.write(data);
//   }
//   req.end();
// };

// var getOptions = function getOptions(method, path, headers) {
//   let options = {
//     hostname: global.config.sg.path,
//     path: `/${  global.config.sg.name  }/${  path}`,
//     method,
//   };
//   if (global.config.sg.port) {
//     options.port = global.config.sg.port;
//   }
//   if (global.config.sg.user && global.config.sg.pw) {
//     options.auth = `${global.config.sg.user  }:${  global.config.sg.pw}`;
//   }
//   if (headers) {
//     options.headers = headers;
//   }
//   return options;
// };
