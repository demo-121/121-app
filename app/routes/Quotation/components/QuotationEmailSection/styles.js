import { StyleSheet } from "react-native";
import Theme from "../../../../theme";

export default StyleSheet.create({
  button: {
    marginRight: Theme.alignmentXL
  },
  text: {
    ...Theme.textButtonLabelNormalAccent
  }
});
