import { StyleSheet } from "react-native";
import Theme from "../../theme";

/**
 * styles
 * */
export default StyleSheet.create({
  container: {
    borderColor: Theme.lightGrey,
    borderRightWidth: 1
  },
  sectionTitle: {
    ...Theme.headerBrand
  },
  image: {
    flexBasis: "auto",
    flex: 0,
    marginRight: Theme.alignmentM,
    width: 24,
    height: 24
  },
  itemNormalView: {
    flexDirection: "column",
    paddingHorizontal: Theme.alignmentXL,
    paddingVertical: Theme.alignmentL
  },
  itemSelectedView: {
    backgroundColor: Theme.brandSuperTransparent,
    flexDirection: "column",
    paddingHorizontal: Theme.alignmentXL,
    paddingVertical: Theme.alignmentL
  },
  itemTitleView: {
    flexDirection: "row"
  },
  noSectionTitle: {
    marginTop: Theme.alignmentL
  },
  itemTitle: {
    ...Theme.headlinePrimary,
    flex: 1
  },
  itemSubTitleView: {
    marginTop: Theme.alignmentS
  },
  itemSubTitleWithIcon: {
    ...Theme.subheadSecondary,
    marginLeft: Theme.alignmentXL + 24 + Theme.alignmentL
  },
  itemSubTitleWithoutIcon: {
    ...Theme.subheadSecondary,
    marginLeft: Theme.alignmentXL
  }
});
