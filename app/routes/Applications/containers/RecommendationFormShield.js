import { connect } from "react-redux";
import { ACTION_LIST, REDUCER_TYPES } from "eab-web-api";
import { TEXT_STORE } from "../../../constants/REDUCER_TYPES";
import RecommendationFormShield from "../components/RecommendationFormShield";

/**
 * RecommendationFormShield
 * @description Form for shield product in Recommendation page.
 * @requires RecommendationFormShield - RecommendationFormShield UI
 * */
const mapStateToProps = (state, ownProps) => ({
  textStore: state[TEXT_STORE],
  value: state[REDUCER_TYPES.RECOMMENDATION].recommendation[ownProps.quotId],
  altValue: state[REDUCER_TYPES.RECOMMENDATION].recommendation,
  isChosen:
    state[REDUCER_TYPES.RECOMMENDATION].recommendation.chosenList.indexOf(
      ownProps.quotId
    ) >= 0,
  disabled: state[REDUCER_TYPES.RECOMMENDATION].component.disabled
});

const mapDispatchToProps = (dispatch, ownProps) => ({
  showRecommendationDetails: () => {
    ACTION_LIST[REDUCER_TYPES.RECOMMENDATION].updateRecommendationDetails({
      dispatch,
      newShowDetails: true
    });
  },
  updateRecommendationData: newValue => {
    ACTION_LIST[REDUCER_TYPES.RECOMMENDATION].updateRecommendationData({
      dispatch,
      quotationId: ownProps.quotId,
      newValue
    });
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(RecommendationFormShield);
