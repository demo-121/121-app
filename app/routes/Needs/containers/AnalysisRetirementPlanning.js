import { connect } from "react-redux";
import { ACTION_TYPES, REDUCER_TYPES, NEEDS } from "eab-web-api";
import { TEXT_STORE } from "../../../constants/REDUCER_TYPES";
import AnalysisRetirementPlanning from "../components/AnalysisRetirementPlanning";

const { FNA, CLIENT } = REDUCER_TYPES;
const { RPLANNING } = NEEDS;

/**
 * AnalysisRetirementPlanning
 * @requires AnalysisRetirementPlanning - AnalysisRetirementPlanning UI
 * */
const mapStateToProps = state => ({
  textStore: state[TEXT_STORE],
  profile: state[CLIENT].profile,
  dependantProfiles: state[CLIENT].dependantProfiles,
  na: state[FNA].na,
  fe: state[FNA].fe,
  pda: state[FNA].pda,
  isRPlanningError: state[FNA].isNaError.rPlanning
});

const mapDispatchToProps = dispatch => ({
  initNaAnalysis: () => {
    dispatch({
      type: ACTION_TYPES[FNA].INIT_NA_ANALYSIS,
      aspect: RPLANNING
    });
  },
  updateNA: ({
    naData,
    pdaData,
    profileData,
    feData,
    dependantProfilesData
  }) => {
    dispatch({
      type: ACTION_TYPES[FNA].UPDATE_NA,
      naData: {
        ...naData,
        completedStep: 2
      },
      pdaData,
      profileData,
      feData,
      dependantProfilesData
    });
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AnalysisRetirementPlanning);
