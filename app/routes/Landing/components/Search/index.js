import { REDUCER_TYPE_CHECK, REDUCER_TYPES, utilities } from "eab-web-api";
import PropTypes from "prop-types";
import React, { Component } from "react";
import { ScrollView, Keyboard, Alert } from "react-native";
import LinearGradient from "react-native-linear-gradient";
import ClientList from "../../../../components/ClientList";
import EABHUD from "../../../../containers/EABHUD";
import { ContextConsumer } from "../../../../context";
import Theme from "../../../../theme";
import styles from "./styles";
import { languageTypeConvert } from "../../../../utilities/getEABTextByLangType";
import { getAllApplicationsWithCID } from "../../../../utilities/DataManager";

const { common, searchClient } = utilities;
const { getOptionTitle } = common;
const { CLIENT, OPTIONS_MAP } = REDUCER_TYPES;

/**
 * Search
 * @param {object} navigation - react navigation props
 * @param {function} getProfile - redux action. trigger saga to get profile
 * @param {function} getContactList - get contact list function
 * */
export default class Search extends Component {
  constructor() {
    super();

    this.translateClientArray = this.translateClientArray.bind(this);

    this.state = {
      isLoading: false
    };
  }
  // ===========================================================================
  // life circles
  // ===========================================================================
  componentDidMount() {
    setTimeout(() => {
      this.props.assignEligProductsToAgent(() => {
        this.props.getContactList();
      });
    }, 800);
  }
  // ===========================================================================
  // class methods
  // ===========================================================================
  /**
   * @description translate reducer state to UI usable data
   * @param {string} language - UI language type
   * @param {string} searchText - text used to search the client
   * @param {boolean} isShowAll - indicate show all clients or not
   * */
  translateClientArray({ language, searchText, isViewAll }) {
    const { contactList, optionsMap } = this.props;
    const returnArray = [];

    searchClient({ contactList, isViewAll, searchText }).forEach(client => {
      let idStr;

      if (client.idDocType === "") {
        idStr = "";
      } else if (client.idDocType === "other") {
        idStr = client.idDocTypeOther.concat(": ", client.idCardNo);
      } else {
        idStr = getOptionTitle({
          value: client.idDocType,
          optionsMap: optionsMap.idDocType,
          language: languageTypeConvert(language)
        }).concat(": ", client.idCardNo);
      }

      returnArray.push({
        key: client.id,
        photo: client.photo,
        name: `${client.fullName}`.trim(),
        phone: client.mobileNo,
        email: client.email,
        policies: client.applicationCount,
        idInfo: idStr,
        lastName: client.lastName,
        firstName: client.firstName
      });
    });

    return returnArray;
  }

  render() {
    // =========================================================================
    // variables
    // =========================================================================
    const { translateClientArray } = this;
    const { getProfile } = this.props;
    const { isLoading } = this.state;

    return (
      <ContextConsumer>
        {({
          showClientNavigator,
          language,
          isViewAll,
          searchText,
          resetSearch
        }) => (
          <LinearGradient
            {...Theme.LinearGradientProps}
            style={styles.container}
          >
            <EABHUD isOpen={isLoading} />
            <EABHUD>
              <ScrollView
                style={styles.scrollView}
                contentContainerStyle={styles.scrollViewContent}
                keyboardShouldPersistTaps="always"
                keyboardDismissMode="on-drag"
              >
                <ClientList
                  clientList={translateClientArray({
                    language,
                    isViewAll,
                    searchText
                  })}
                  clientOnPress={client => {
                    this.setState({ isLoading: true }, () => {
                      let isValid = true;

                      getAllApplicationsWithCID(client.key, appList => {
                        if (appList) {
                          appList.some(app => {
                            if (
                              [
                                // "ESC",
                                // "AWICP",
                                // "AWICA",
                                // "LITE",
                                // "NPE",
                                // "PNPP2",
                                // "PNP2",
                                // "PNP",
                                // "PNPP",
                                // "DTS",
                                // "DTJ"
                              ].findIndex(
                                code => code === app.baseProductCode
                              ) !== -1 &&
                              app.appStatus !== "SUBMITTED"
                            ) {
                              isValid = false;
                            }
                            return !isValid;
                          });
                        }
                        if (isValid) {
                          Keyboard.dismiss();
                          getProfile({
                            cid: client.key,
                            callback: () => {
                              this.setState(
                                {
                                  isLoading: false
                                },
                                () => {
                                  setTimeout(() => {
                                    showClientNavigator(resetSearch);
                                  }, 200);
                                }
                              );
                            }
                          });
                        } else {
                          this.setState(
                            {
                              isLoading: false
                            },
                            () => {
                              setTimeout(() => {
                                Alert.alert(
                                  null,
                                  "To continue accessing this client, please login to the EAB online platform",
                                  [
                                    {
                                      text: "OK",
                                      onPress: () => {}
                                    }
                                  ]
                                );
                              }, 200);
                            }
                          );
                        }
                      });
                    });
                  }}
                />
              </ScrollView>
            </EABHUD>
          </LinearGradient>
        )}
      </ContextConsumer>
    );
  }
}

Search.propTypes = {
  contactList: REDUCER_TYPE_CHECK[CLIENT].contactList.isRequired,
  getProfile: PropTypes.func.isRequired,
  getContactList: PropTypes.func.isRequired,
  assignEligProductsToAgent: PropTypes.func.isRequired,
  optionsMap: REDUCER_TYPE_CHECK[OPTIONS_MAP].isRequired
};
