//
//  DocumentPickerEventEmitter.h
//  EASE
//
//  Created by Alex Tang on 27/9/2018.
//  Copyright © 2018 Facebook. All rights reserved.
//

#import <React/RCTBridgeModule.h>
#import <React/RCTEventEmitter.h>

@interface DocumentPickerEventEmitter : RCTEventEmitter <RCTBridgeModule>
-(void)completedImport:(NSDictionary *) options;
-(void)cancelImport;
@end
