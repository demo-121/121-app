import { LANGUAGE_TYPES } from "eab-web-api";
import * as _ from "lodash";

const JSON_LANGUAGE_TYPES = {
  [LANGUAGE_TYPES.ENGLISH]: "en",
  [LANGUAGE_TYPES.TRADITIONAL_CHINESE]: "zh-Hant"
};

/**
 * @return web language constant
 * */
export const languageTypeConvert = languageType =>
  JSON_LANGUAGE_TYPES[languageType];

/**
 * getEABTextByLangType
 * @description convert data to string by on inputted language type
 * @param {object} data - data to be handled
 * @param {string} language - language type, must come from LANGUAGE_TYPES
 * */
export default function({ data, language }) {
  if (data !== undefined) {
    return _.isObject(data)
      ? _.get(data, `${JSON_LANGUAGE_TYPES[language]}`)
      : data;
  }

  return null;
}
