import { createStackNavigator, createAppContainer } from "react-navigation";
import React from "react";
import ProfileDetail from "./containers/ProfileDetail";
import ClientInfo from "./containers/ClientInfo";
import HeaderLeft from "./containers/HeaderLeft";
import HeaderRight from "./containers/HeaderRight";

const ProfileNavigator = createStackNavigator(
  {
    ProfileDetail: {
      screen: ProfileDetail,
      navigationOptions: () => ({
        title: "Profile",
        headerStyle: {
          height: 92
        },
        headerLeft: <HeaderLeft />,
        headerTitle: <ClientInfo />,
        headerRight: <HeaderRight />
      })
    }
  },
  {
    initialRouteName: "ProfileDetail"
  }
);

export default createAppContainer(ProfileNavigator);
