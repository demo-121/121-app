import { connect } from "react-redux";
import { ACTION_TYPES, REDUCER_TYPES } from "eab-web-api";
import { TEXT_STORE } from "../../../constants/REDUCER_TYPES";
import ROI from "../components/ROI";

const { FNA, CLIENT } = REDUCER_TYPES;

/**
 * ROI
 * @requires ROI - ROI UI
 * */
const mapStateToProps = state => ({
  textStore: state[TEXT_STORE],
  na: state[FNA].na,
  fe: state[FNA].fe,
  pda: state[FNA].pda,
  fna: state[FNA],
  profile: state[CLIENT].profile,
  dependantProfiles: state[CLIENT].dependantProfiles
});

const mapDispatchToProps = dispatch => ({
  updateNA: ({
    naData,
    pdaData,
    profileData,
    feData,
    dependantProfilesData
  }) => {
    dispatch({
      type: ACTION_TYPES[FNA].UPDATE_NA,
      naData,
      pdaData,
      profileData,
      feData,
      dependantProfilesData
    });
    dispatch({
      type: ACTION_TYPES[FNA].IAR_RATE_ON_CHANGE
    });
  },
  iarRateNotMatchReasonOnChange: newIarRateNotMatchReason => {
    dispatch({
      type: ACTION_TYPES[FNA].IAR_RATE_NOT_MATCH_REASON_ON_CHANGE,
      newIarRateNotMatchReason
    });
  },
  initCompletedStep: value => {
    dispatch({
      type: ACTION_TYPES[FNA].INIT_COMPLETED_STEP_SAGA,
      value
    });
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ROI);
