export const structureMapping = {
  INS_DATA_DEFAULT: {
    column0: ["a", "b", "c"],
    column1: ["d"],
    column2: ["e"]
  },

  INS01_DATA: {
    column0: ["INS01a", "INS01a1-b", "INS01c"],
    column1: ["INS01d"],
    column2: ["INS01e"]
  },

  INS02_DATA: {
    column0: ["INS02a", "INS02b-b", "INS02c"],
    column1: ["INS02d"],
    column2: ["INS02e"]
  },

  INS03_DATA: {
    column0: ["INS03a", "INS03a1-b", "INS03c"],
    column1: ["INS03d"],
    column2: ["INS03e"]
  },

  INS04_DATA: {
    column0: ["INS04a", "INS04b-b", "INS04c"],
    column1: ["INS04d"],
    column2: ["INS04e"]
  },

  INS05_DATA: {
    column0: ["INS05a", "INS05b"],
    column1: ["INS05c"],
    column2: ["INS05d"]
  },

  HEALTH_SHIELD__DATA_DEFAULT: {
    column0: ["a", "b", "c"],
    column1: ["d", "e"],
    column2: ["f"]
  },

  LIFESTYLE03_DATA: {
    column0: ["LIFESTYLE03a"],
    column1: ["LIFESTYLE03b", "LIFESTYLE03c"],
    column2: ["LIFESTYLE03d"]
  },

  LIFESTYLE04_DATA: {
    column0: ["LIFESTYLE04a", "LIFESTYLE04b"],
    column1: ["LIFESTYLE04c"],
    column2: ["LIFESTYLE04d-b"]
  },

  LIFESTYLE05_DATA: {
    column0: ["LIFESTYLE05a"],
    column1: ["LIFESTYLE05b"],
    column2: ["LIFESTYLE05c"]
  },

  LIFESTYLE06_DATA: {
    column0: ["LIFESTYLE06a", "LIFESTYLE06b"],
    column1: ["LIFESTYLE06c"],
    column2: ["LIFESTYLE06d-b"]
  },

  FAMILY01_DATA: {
    column0: ["FAMILY01a"],
    column1: ["FAMILY01b"],
    column2: ["FAMILY01c"]
  },

  REG_DR01_DATA: {
    column0: ["REG_DR01a", "REG_DR01b"],
    column1: ["REG_DR01c"],
    column2: ["REG_DR01d"]
  },

  HEALTH_DATA_DEFAULT: {
    column0: ["a", "b", "c", "d", "e"],
    column1: ["f"],
    column2: ["g", "h"]
  },

  HEALTH13b_DATA: {
    column0: [
      "HEALTH13b1",
      "HEALTH13b2",
      "HEALTH13b3",
      "HEALTH13b4",
      "HEALTH13b5"
    ],
    column1: ["HEALTH13b6"],
    column2: ["HEALTH13b7", "HEALTH13b8"]
  },

  HEALTH_HIM_DATA_DEFAULT: {
    column0: ["a", "b", "c", "d", "e"],
    column1: ["f"],
    column2: ["g", "h"]
  },

  HEALTH_HIM01_DATA: {
    column0: ["HEALTH_HIM01a"],
    column1: ["HEALTH_HIM01b"],
    column2: ["HEALTH_HIM01c"]
  },

  HEALTH_HIM02_DATA: {
    column0: [
      "HEALTH_HIM02a",
      "HEALTH_HIM02b",
      "HEALTH_HIM02c",
      "HEALTH_HIM02d",
      "HEALTH_HIM02e"
    ],
    column1: ["HEALTH_HIM02f"],
    column2: ["HEALTH_HIM02g", "HEALTH_HIM02h"]
  },

  HEALTH_HIM03_DATA: {
    column0: [
      "HEALTH_HIM03a",
      "HEALTH_HIM03b",
      "HEALTH_HIM03c",
      "HEALTH_HIM03d",
      "HEALTH_HIM03e"
    ],
    column1: ["HEALTH_HIM03f"],
    column2: ["HEALTH_HIM03g", "HEALTH_HIM03h"]
  },

  HEALTH_HIM04_DATA: {
    column0: [
      "HEALTH_HIM04a",
      "HEALTH_HIM04b",
      "HEALTH_HIM04c",
      "HEALTH_HIM04d",
      "HEALTH_HIM04e"
    ],
    column1: ["HEALTH_HIM04f"],
    column2: ["HEALTH_HIM04g", "HEALTH_HIM04h"]
  },

  HEALTH_HER_DATA_DEFAULT: {
    column0: ["a", "b", "c", "d", "e"],
    column1: ["f"],
    column2: ["g", "h"]
  },

  HEALTH_HER01_DATA: {
    column0: ["HEALTH_HER01a"],
    column1: ["HEALTH_HER01b"],
    column2: ["HEALTH_HER01c"]
  },

  HEALTH_GIO_DATA_DEFAULT: {
    column0: ["a", "b", "c", "d", "e"],
    column1: ["f"],
    column2: ["g", "h"]
  },

  HEALTH_GIO03_DATA: {
    column0: [
      "HEALTH_GIO03a",
      "HEALTH_GIO03b",
      "HEALTH_GIO03c",
      "HEALTH_GIO03d",
      "HEALTH_GIO03e"
    ],
    column1: ["HEALTH_GIO03f"],
    column2: ["HEALTH_GIO03g", "HEALTH_GIO03h"]
  },

  PEP01_DATA: {
    column0: ["PEP01a", "PEP01b", "PEP01c", "PEP01d"],
    column1: ["PEP01-bb"],
    column2: ["PEP01-c"]
  },

  CRS_DATA_DEFAULT: {
    column0: ["a"],
    column1: ["b"],
    column2: ["c", "d"]
  },

  // Residency Page
  reside5yrs: {
    column0: ["res5Frm", "res5To"],
    column1: ["res5Country", "res5City"],
    column2: ["res_reason_t", "res_reasons_1"]
  },

  travelPlans: {
    column0: ["travlCountry", "travlCity"],
    column1: ["travlCityDrn", "travlCityFrq"],
    column2: ["res_reason_t", "res_reason_2"]
  },

  // Shield
  ROP01_DATA: {
    column0: ["nameOfCompany"],
    column1: ["planName"],
    column2: [""]
  }
};

export const titleMapping = {
  INS_DATA_DEFAULT: {
    header: [
      "Insurance Information",
      "Condition of Special Terms",
      "Decision & Detailed Reason(s) of special terms"
    ],
    content: {}
  },

  INS01_DATA: {
    header: [
      "Insurance Policy Information",
      "Condition of Special Terms",
      "Decision & Detailed Reason(s) of special terms"
    ],
    content: {
      INS01a: "Insurance Company"
    }
  },

  INS02_DATA: {
    header: [
      "Claim Information",
      "Medical Condition",
      "Claim Details (Duration of hospitalisation/offwork, treatment, follow-up etc)"
    ],
    content: {
      INS02a: "Insurance Company"
    }
  },

  INS03_DATA: {
    header: [
      "Insurance Policy Information",
      "Medical Condition",
      "Decision & Detailed Reason(s) of special terms"
    ],
    content: {
      INS03a: "Insurance Company:",
      "INS03a1-b": "Type of Coverage:",
      INS03c: "Date Incurred (MM/YYYY):",
      INS03d: "",
      INS03e: ""
    }
  },

  INS04_DATA: {
    header: [
      "Claim Information",
      "Medical Condition",
      "Claim Details (treatment, follow-up etc)*"
    ],
    content: {
      INS04a: "Insurance Company:",
      "INS04b-b": "Type of Claim:",
      INS04c: "Date Incurred (MM/YYYY):",
      INS04d: "",
      INS04e: ""
    }
  },

  INS05_DATA: {
    header: [
      "Chronic/pre-existing medical condition you have",
      "Current/ongoing treatment",
      "Any existing medical reports*"
    ],
    content: {
      INS05a: "Chronic/pre-existing medical condition:",
      INS05b: "Year Diagnosed:",
      INS05c: "",
      INS05d: ""
    }
  },

  HEALTH_SHIELD__DATA_DEFAULT: {
    header: [
      "Medical Condition/ Diagnosis",
      "Medication and Next Follow-up visit date",
      "Current Complication"
    ],
    content: {}
  },

  HEALTH_SHIELD_19_DATA: {
    header: [
      "Surgery/Operation information",
      "Current Medication/ Hormonal Therapy and Next Follow-up visit date",
      "Current Complication/ Follow up for other condition due to SRS"
    ],
    content: {}
  },

  HEALTH_SHIELD_21_DATA: {
    header: [
      "Medical Condition/Diagnosis",
      "Details  (Medication/Treatment / Follow up / Complication) ",
      "Doctor Information"
    ],
    content: {}
  },

  LIFESTYLE03_DATA: {
    header: [
      "Substances used",
      "Date",
      "Details (e.g. Treatment, Any other medical condition, Any relapses/complications etc)"
    ],
    content: {
      LIFESTYLE03b: "Date Commenced (MM/YYYY)",
      LIFESTYLE03c: "Date Ceased (MM/YYYY)"
    }
  },

  LIFESTYLE04_DATA: {
    header: ["Country and City", "Duration(how many days per year)", "Reason"],
    content: {
      LIFESTYLE03b: "Date Commenced (MM/YYYY)",
      LIFESTYLE03c: "Date Ceased (MM/YYYY)",
      LIFESTYLE04a: "Country",
      LIFESTYLE04b: "City"
    }
  },

  LIFESTYLE05_DATA: {
    header: [
      "Type of hazardous activities",
      "Frequency (per year)",
      "Details (e.g. Expertise, level of participation, experience etc)"
    ],
    content: {}
  },

  LIFESTYLE06_DATA: {
    header: ["Country and City", "Duration", "Reason"],
    content: {}
  },

  FAMILY01_DATA: {
    header: ["Relationship", "Medical Condition", "Age of Onset"],
    content: {}
  },

  REG_DR01_DATA: {
    header: [
      "Doctor Information",
      "Date of last consultation",
      "Details of Consultations (including Reason, Diagnosis, Treatment, etc.)"
    ],
    content: {}
  },

  HEALTH_GIO_DATA_DEFAULT: {
    header: [
      "Medical Condition/Diagnosis",
      "Details  (Medication/Treatment / Follow up / Complication) ",
      "Doctor Information"
    ],
    content: {}
  },

  HEALTH_GIO03_DATA: {
    header: [
      "Medical Condition/Diagnosis",
      "Details  (Medication/Treatment / Follow up / Complication) ",
      "Doctor Information"
    ],
    content: {
      HEALTH_GIO03a: "Medical Condition/Diagnosis ",
      HEALTH_GIO03b: "Year of diagnosis ",
      HEALTH_GIO03c: "Type of test done (if any)",
      HEALTH_GIO03d: "Date of test done (if any) ",
      HEALTH_GIO03e: "Test result (if any) ",
      HEALTH_GIO03g: "Name of Doctor ",
      HEALTH_GIO03h: "Address of Hospital/Clinic "
    }
  },

  HEALTH_DATA_DEFAULT: {
    header: [
      "Medical Condition/Diagnosis",
      "Details (Medication/ Treatment/ Follow up/ Complication)",
      "Doctor Information"
    ],
    content: {}
  },

  HEALTH11_DATA: {
    header: [
      "Medical Condition/Diagnosis",
      "Medication/ Treatment/ Follow up or any complication",
      "Doctor Information"
    ],
    content: {}
  },

  HEALTH13b_DATA: {
    header: [
      "Medical Condition/Diagnosis",
      "Details (Medication/ Treatment/ Follow up/ Complication)",
      "Doctor Information"
    ],
    content: {
      HEALTH13b1: "Medical Condition/Diagnosis",
      HEALTH13b2: "Year of diagnosis",
      HEALTH13b3: "Type of test done (if any)",
      HEALTH13b4: "Date of test done (if any)",
      HEALTH13b5: "Test result (if any)",
      HEALTH13b7: "Name of Doctor",
      HEALTH13b8: "Address of Hospital/Clinic"
    }
  },

  HEALTH_HIM_DATA_DEFAULT: {
    header: [
      "Medical Condition/Diagnosis",
      "Details (Medication/ Treatment/ Follow up/ Complication)",
      "Doctor Information"
    ],
    content: {}
  },

  HEALTH_HIM01_DATA: {
    header: ["Relationship", "Medical Condition", "Age of Onset"],
    content: {}
  },

  HEALTH_HIM02_DATA: {
    header: [
      "Medical Condition/Diagnosis",
      "Details  (Medication/Treatment / Follow up / Complication) ",
      "Doctor Information"
    ],
    content: {}
  },

  HEALTH_HIM03_DATA: {
    header: [
      "Medical Condition/Diagnosis",
      "Details  (Medication/Treatment / Follow up / Complication) ",
      "Doctor Information"
    ],
    content: {}
  },

  HEALTH_HIM04_DATA: {
    header: [
      "Medical Condition/Diagnosis",
      "Details  (Medication/Treatment / Follow up / Complication) ",
      "Doctor Information"
    ],
    content: {}
  },

  HEALTH_HER_DATA_DEFAULT: {
    header: [
      "Medical Condition/Diagnosis",
      "Details (Medication/ Treatment/ Follow up/ Complication)",
      "Doctor Information"
    ],
    content: {}
  },

  HEALTH_HER01_DATA: {
    header: ["Relationship", "Medical Condition", "Age of Onset"],
    content: {}
  },

  HEALTH_HER02_DATA: {
    header: [
      "Medical Condition/Diagnosis",
      "Details  (Medication/Treatment / Follow up / Complication) ",
      "Doctor Information"
    ],
    content: {}
  },

  HEALTH_HER04_DATA: {
    header: [
      "Medical Condition/Diagnosis",
      "Details  (Medication/Treatment / Follow up / Complication) ",
      "Doctor Information"
    ],
    content: {}
  },

  PEP01_DATA: {
    header: ["PEP Information", "Source of Funds", "Source of Wealth"],
    content: {
      PEP01a: "Relationship",
      PEP01b: "Full name",
      PEP01c: "Position held",
      PEP01d: "Country where he/she is the PEP"
    }
  },

  CRS_DATA_DEFAULT: {
    header: [
      "Country/Jurisdiction of Tax Residency",
      "Tax Identification Number (TIN)",
      "Reason"
    ],
    content: {}
  },

  reside5yrs: {
    header: ["Duration", "Country and City", "Reason"],
    content: {}
  },

  travelPlans: {
    header: ["Country and City", "Duration", "Reason"],
    content: {}
  },

  // Shield
  ROP01_DATA: {
    header: ["Name of Company", "Plan Name", ""],
    content: {
      nameOfCompany: "",
      planName: ""
    }
  }
};

export const shieldStructureMapping = {
  INS_DATA_DEFAULT: {
    column0: ["a", "b", "c"],
    column1: ["d"],
    column2: ["e"]
  },

  INS01_DATA: {
    column0: ["INS01a", "INS01a1-b", "INS01c"],
    column1: ["INS01d"],
    column2: ["INS01e"]
  },

  INS02_DATA: {
    column0: ["INS02a", "INS02b-b", "INS02c"],
    column1: ["INS02d"],
    column2: ["INS02e"]
  },

  INS03_DATA: {
    column0: ["INS03a", "INS03a1-b", "INS03c"],
    column1: ["INS03d"],
    column2: ["INS03e"]
  },

  INS04_DATA: {
    column0: ["INS04a", "INS04b-b", "INS04c"],
    column1: ["INS04d"],
    column2: ["INS04e"]
  },

  INS05_DATA: {
    column0: ["INS05a", "INS05b"],
    column1: ["INS05c"],
    column2: ["INS05d"]
  },

  HEALTH_SHIELD__DATA_DEFAULT: {
    column0: ["a", "b", "c"],
    column1: ["d", "e"],
    column2: ["f"]
  },

  LIFESTYLE03_DATA: {
    column0: ["LIFESTYLE03a"],
    column1: ["LIFESTYLE03b", "LIFESTYLE03c"],
    column2: ["LIFESTYLE03d"]
  },

  LIFESTYLE04_DATA: {
    column0: ["LIFESTYLE04a", "LIFESTYLE04b"],
    column1: ["LIFESTYLE04c"],
    column2: ["LIFESTYLE04d-b"]
  },

  LIFESTYLE05_DATA: {
    column0: ["LIFESTYLE05a"],
    column1: ["LIFESTYLE05b"],
    column2: ["LIFESTYLE05c"]
  },

  LIFESTYLE06_DATA: {
    column0: ["LIFESTYLE06a", "LIFESTYLE06b"],
    column1: ["LIFESTYLE06c"],
    column2: ["LIFESTYLE06d-b"]
  },

  FAMILY01_DATA: {
    column0: ["FAMILY01a"],
    column1: ["FAMILY01b"],
    column2: ["FAMILY01c"]
  },

  REG_DR01_DATA: {
    column0: ["REG_DR01a", "REG_DR01b"],
    column1: ["REG_DR01c"],
    column2: ["REG_DR01d"]
  },

  HEALTH_DATA_DEFAULT: {
    column0: ["a", "b", "c", "d", "e"],
    column1: ["f"],
    column2: ["g", "h"]
  },

  HEALTH13b_DATA: {
    column0: ["HEALTH13b1", "HEALTH13b2", "HEALTH13b3"],
    column1: ["HEALTH13b4", "HEALTH13b5"],
    column2: ["HEALTH13b6"]
  },

  HEALTH_HIM_DATA_DEFAULT: {
    column0: ["a", "b", "c", "d", "e"],
    column1: ["f"],
    column2: ["g", "h"]
  },

  HEALTH_HIM01_DATA: {
    column0: ["HEALTH_HIM01a"],
    column1: ["HEALTH_HIM01b"],
    column2: ["HEALTH_HIM01c"]
  },

  HEALTH_HIM02_DATA: {
    column0: [
      "HEALTH_HIM02a",
      "HEALTH_HIM02b",
      "HEALTH_HIM02c",
      "HEALTH_HIM02d",
      "HEALTH_HIM02e"
    ],
    column1: ["HEALTH_HIM02f"],
    column2: ["HEALTH_HIM02g", "HEALTH_HIM02h"]
  },

  HEALTH_HIM03_DATA: {
    column0: [
      "HEALTH_HIM03a",
      "HEALTH_HIM03b",
      "HEALTH_HIM03c",
      "HEALTH_HIM03d",
      "HEALTH_HIM03e"
    ],
    column1: ["HEALTH_HIM03f"],
    column2: ["HEALTH_HIM03g", "HEALTH_HIM03h"]
  },

  HEALTH_HIM04_DATA: {
    column0: [
      "HEALTH_HIM04a",
      "HEALTH_HIM04b",
      "HEALTH_HIM04c",
      "HEALTH_HIM04d",
      "HEALTH_HIM04e"
    ],
    column1: ["HEALTH_HIM04f"],
    column2: ["HEALTH_HIM04g", "HEALTH_HIM04h"]
  },

  HEALTH_HER_DATA_DEFAULT: {
    column0: ["a", "b", "c", "d", "e"],
    column1: ["f"],
    column2: ["g", "h"]
  },

  HEALTH_HER01_DATA: {
    column0: ["HEALTH_HER01a"],
    column1: ["HEALTH_HER01b"],
    column2: ["HEALTH_HER01c"]
  },

  HEALTH_GIO_DATA_DEFAULT: {
    column0: ["a", "b", "c", "d", "e"],
    column1: ["f"],
    column2: ["g", "h"]
  },

  HEALTH_GIO03_DATA: {
    column0: [
      "HEALTH_GIO03a",
      "HEALTH_GIO03b",
      "HEALTH_GIO03c",
      "HEALTH_GIO03d",
      "HEALTH_GIO03e"
    ],
    column1: ["HEALTH_GIO03f"],
    column2: ["HEALTH_GIO03g", "HEALTH_GIO03h"]
  },

  PEP01_DATA: {
    column0: ["PEP01a", "PEP01b", "PEP01c", "PEP01d"],
    column1: ["PEP01-bb"],
    column2: ["PEP01-c"]
  },

  CRS_DATA_DEFAULT: {
    column0: ["a"],
    column1: ["b", "d"],
    column2: ["c"]
  },

  // Residency Page
  reside5yrs: {
    column0: ["res5Frm", "res5To"],
    column1: ["res5Country", "res5City"],
    column2: ["res_reason_t", "res_reasons_1"]
  },

  travelPlans: {
    column0: ["travlCountry", "travlCity"],
    column1: ["travlCityDrn", "travlCityFrq"],
    column2: ["res_reason_t", "res_reason_2"]
  },

  // Shield
  ROP01_DATA: {
    column0: ["nameOfCompany"],
    column1: ["planName"],
    column2: [""]
  }
};

export const shieldTitleMapping = {
  INS_DATA_DEFAULT: {
    header: [
      "Insurance Information",
      "Condition of Special Terms",
      "Decision & Detailed Reason(s) of special terms"
    ],
    content: {}
  },

  INS01_DATA: {
    header: [
      "Insurance Policy Information",
      "Condition of Special Terms",
      "Decision & Detailed Reason(s) of special terms"
    ],
    content: {
      INS01a: "Insurance Company:",
      "INS01a1-b": "Type of Coverage:",
      INS01c: "Date Incurred (MM/YYYY):",
      INS01d: "",
      INS01e: ""
    }
  },

  INS02_DATA: {
    header: [
      "Claim Information",
      "Medical Condition",
      "Claim Details (Duration of hospitalisation/offwork, treatment, follow-up etc)"
    ],
    content: {
      INS02a: "Insurance Company: ",
      "INS02b-b": "Type of the Claim: ",
      INS02c: "Date incurred (MM/YYYY): ",
      INS02d: "",
      INS02e: ""
    }
  },

  INS03_DATA: {
    header: [
      "Insurance Policy Information",
      "Medical Condition",
      "Decision & Detailed Reason(s) of special terms"
    ],
    content: {
      INS03a: "Insurance Company",
      "INS03a1-b": "Type of Coverage",
      INS03c: "Date Incurred (MM/YYYY)",
      INS03d: "Medical Condition",
      INS03e: "Decision & Detailed Reason(s) of special terms"
    }
  },

  INS04_DATA: {
    header: [
      "Claim Information",
      "Medical Condition",
      "Claim Details (treatment, follow-up etc)*"
    ],
    content: {
      INS04a: "Insurance Company",
      "INS04b-b": "Type of Claim",
      INS04c: "Date Incurred (MM/YYYY)",
      INS04d: "Medical Condition",
      INS04e: "Claim Details (treatment, follow-up etc)"
    }
  },

  INS05_DATA: {
    header: [
      "Chronic/pre-existing medical condition you have",
      "Current/ongoing treatment",
      "Any existing medical reports*"
    ],
    content: {
      INS05a: "Chronic/pre-existing medical condition",
      INS05b: "Year Diagnosed",
      INS05c: "Current/ongoing treatment",
      INS05d: "Any existing medical reports"
    }
  },

  HEALTH_SHIELD__DATA_DEFAULT: {
    header: [
      "Medical Condition/ Diagnosis",
      "Medication and Next Follow-up visit date",
      "Current Complication"
    ],
    content: {}
  },

  HEALTH_SHIELD_19_DATA: {
    header: [
      "Surgery/Operation information",
      "Current Medication/ Hormonal Therapy and Next Follow-up visit date",
      "Current Complication/ Follow up for other condition due to SRS"
    ],
    content: {}
  },

  LIFESTYLE03_DATA: {
    header: [
      "Substances used",
      "Date",
      "Details (e.g. Treatment, Any other medical condition, Any relapses/complications etc)"
    ],
    content: {
      LIFESTYLE03b: "Date Commenced (MM/YYYY)",
      LIFESTYLE03c: "Date Ceased (MM/YYYY)"
    }
  },

  LIFESTYLE04_DATA: {
    header: ["Country and City", "Duration(how many days per year)", "Reason"],
    content: {
      LIFESTYLE03b: "Date Commenced (MM/YYYY)",
      LIFESTYLE03c: "Date Ceased (MM/YYYY)",
      LIFESTYLE04a: "Country",
      LIFESTYLE04b: "City"
    }
  },

  LIFESTYLE05_DATA: {
    header: [
      "Type of hazardous activities",
      "Frequency (per year)",
      "Details (e.g. Expertise, level of participation, experience etc)"
    ],
    content: {
      LIFESTYLE05a: "",
      LIFESTYLE05b: "",
      LIFESTYLE05c: ""
    }
  },

  LIFESTYLE06_DATA: {
    header: ["Country and City", "Duration", "Reason"],
    content: {}
  },

  FAMILY01_DATA: {
    header: ["Relationship", "Medical Condition", "Age of Onset"],
    content: {}
  },

  REG_DR01_DATA: {
    header: [
      "Doctor Information",
      "Date of last consultation",
      "Details of Consultations (including Reason, Diagnosis, Treatment, etc.)"
    ],
    content: {}
  },

  HEALTH_GIO_DATA_DEFAULT: {
    header: [
      "Medical Condition/Diagnosis",
      "Details  (Medication/Treatment / Follow up / Complication) ",
      "Doctor Information"
    ],
    content: {}
  },

  HEALTH_GIO03_DATA: {
    header: [
      "Medical Condition/Diagnosis",
      "Details  (Medication/Treatment / Follow up / Complication) ",
      "Doctor Information"
    ],
    content: {
      HEALTH_GIO03a: "Medical Condition/Diagnosis ",
      HEALTH_GIO03b: "Year of diagnosis ",
      HEALTH_GIO03c: "Type of test done (if any)",
      HEALTH_GIO03d: "Date of test done (if any) ",
      HEALTH_GIO03e: "Test result (if any) ",
      HEALTH_GIO03g: "Name of Doctor ",
      HEALTH_GIO03h: "Address of Hospital/Clinic "
    }
  },

  HEALTH_DATA_DEFAULT: {
    header: [
      "Medical Condition/Diagnosis",
      "Details (Medication/ Treatment/ Follow up/ Complication)",
      "Doctor Information"
    ],
    content: {}
  },

  HEALTH13b_DATA: {
    header: [
      "Medical Condition/ Diagnosis",
      "Medication and Next Follow-up visit date",
      "Current Complication"
    ],
    content: {}
  },

  HEALTH_HIM_DATA_DEFAULT: {
    header: [
      "Medical Condition/Diagnosis",
      "Details (Medication/ Treatment/ Follow up/ Complication)",
      "Doctor Information"
    ],
    content: {}
  },

  HEALTH_HIM01_DATA: {
    header: ["Relationship", "Medical Condition", "Age of Onset"],
    content: {}
  },

  HEALTH_HIM02_DATA: {
    header: [
      "Medical Condition/Diagnosis",
      "Details  (Medication/Treatment / Follow up / Complication) ",
      "Doctor Information"
    ],
    content: {}
  },

  HEALTH_HIM03_DATA: {
    header: [
      "Medical Condition/Diagnosis",
      "Details  (Medication/Treatment / Follow up / Complication) ",
      "Doctor Information"
    ],
    content: {}
  },

  HEALTH_HIM04_DATA: {
    header: [
      "Medical Condition/Diagnosis",
      "Details  (Medication/Treatment / Follow up / Complication) ",
      "Doctor Information"
    ],
    content: {}
  },

  HEALTH_HER_DATA_DEFAULT: {
    header: [
      "Medical Condition/Diagnosis",
      "Details (Medication/ Treatment/ Follow up/ Complication)",
      "Doctor Information"
    ],
    content: {}
  },

  HEALTH_HER01_DATA: {
    header: ["Relationship", "Medical Condition", "Age of Onset"],
    content: {}
  },

  HEALTH_HER02_DATA: {
    header: [
      "Medical Condition/Diagnosis",
      "Details  (Medication/Treatment / Follow up / Complication) ",
      "Doctor Information"
    ],
    content: {}
  },

  HEALTH_HER04_DATA: {
    header: [
      "Medical Condition/Diagnosis",
      "Details  (Medication/Treatment / Follow up / Complication) ",
      "Doctor Information"
    ],
    content: {}
  },

  PEP01_DATA: {
    header: ["PEP Information", "Source of Funds", "Source of Wealth"],
    content: {
      PEP01a: "Relationship",
      PEP01b: "Full name",
      PEP01c: "Position held",
      PEP01d: "Country where he/she is the PEP"
    }
  },

  CRS_DATA_DEFAULT: {
    header: [
      "Country/Jurisdiction of Tax Residency",
      "Tax Identification Number (TIN)",
      "Reason"
    ],
    content: {}
  },

  reside5yrs: {
    header: ["Duration", "Country and City", "Reason"],
    content: {}
  },

  travelPlans: {
    header: ["Country and City", "Duration", "Reason"],
    content: {}
  },

  // Shield
  ROP01_DATA: {
    header: ["Name of Company", "Plan Name", ""],
    content: {
      nameOfCompany: "",
      planName: ""
    }
  }
};
