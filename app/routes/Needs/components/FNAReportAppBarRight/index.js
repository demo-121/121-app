import React, { Component } from "react";
import PropTypes from "prop-types";
import { View, Alert, Linking, NetInfo } from "react-native";
import SafariView from "react-native-safari-view";
import Config from "react-native-config";
import {
  REDUCER_TYPE_CHECK as WEB_API_REDUCER_TYPE_CHECK,
  REDUCER_TYPES,
  USER_TAB_TYPES
} from "eab-web-api";
import EABi18n from "../../../../utilities/EABi18n";
import APP_REDUCER_TYPE_CHECK from "../../../../constants/REDUCER_TYPE_CHECK";
import { TEXT_STORE } from "../../../../constants/REDUCER_TYPES";
import EABHUD from "../../../../components/EABHUD";
import EABButton from "../../../../components/EABButton";
import { ContextConsumer } from "../../../../context";
import TranslatedText from "../../../../containers/TranslatedText";
import Theme from "../../../../theme";
import EASEEmailContentDialog from "../../../../containers/EASEEmailContentDialog";
import EASEEmailDialog from "../../../../containers/EASEEmailDialog";
import styles from "./styles";

const { CLIENT, FNA } = REDUCER_TYPES;
const { CLIENT_TAB, AGENT_TAB } = USER_TAB_TYPES;
/**
 * FNAReportAppBarRight
 * @description needs detail page app bar right.
 * */
export default class FNAReportAppBarRight extends Component {
  constructor() {
    super();
    this.getAuthCodeCallback = this.getAuthCodeCallback.bind(this);
    this.setfunction = this.setfunction.bind(this);

    this.state = {
      function: null,
      isNetworkConnected: false,
      setOnlineDone: false,
      isLoading: false,
      shownEmailDialog: false
    };
    this.path = "";
    this.handleConnectionChange = this.handleConnectionChange.bind(this);
    NetInfo.isConnected.fetch().done(this.handleConnectionChange);
  }
  componentDidMount() {
    Linking.addEventListener("url", this.getAuthCodeCallback);
    NetInfo.isConnected.addEventListener(
      "connectionChange",
      this.handleConnectionChange
    );
  }

  componentWillUnmount() {
    Linking.removeEventListener("url", this.getAuthCodeCallback);
    NetInfo.isConnected.removeEventListener(
      "connectionChange",
      this.handleConnectionChange
    );
  }
  getAuthCodeCallback(response) {
    const { setOnlineAuthCodeForPayment } = this.props;
    if (response.url) {
      const urlGet = response.url;
      SafariView.dismiss();
      this.setState(
        {
          isLoading: true
        },
        () => {
          const segment1 = urlGet.replace("easemobile://?", "");
          const variables = segment1.split("&");
          const codeString = variables.find(variable =>
            variable.includes("code")
          );
          const [, authCode] = codeString.split("=");
          if (!this.props.isHandlingListener) {
            setOnlineAuthCodeForPayment({
              authCode,
              callback: callback => {
                if (callback.hasError) {
                  if (callback.errorMsg) {
                    setTimeout(() => {
                      Alert.alert(`[Error] ${callback.errorMsg}`);
                    }, 200);
                  } else {
                    setTimeout(() => {
                      Alert.alert(
                        `error msg: Authorization for submission Failed (Error 001)`
                      );
                    }, 200);
                  }
                } else {
                  this.setState({
                    setOnlineDone: true
                  });
                }
              }
            });
          }
        }
      );
    }
  }

  setfunction(inputFunction) {
    if (this.state.setOnlineDone) {
      this.setState({
        function: inputFunction,
        isLoading: false
      });
    }
  }
  handleConnectionChange(isConnected) {
    this.setState({ isNetworkConnected: isConnected });
  }
  pressHandler() {
    SafariView.isAvailable()
      .then(
        SafariView.show({
          url: this.path
          // url: "eabtest://"
        })
      )
      .catch(error => {
        // Fallback WebView code for iOS 8 and earlier
        setTimeout(() => {
          Alert.alert(`[Error]${error}`);
        }, 200);
      });
  }

  render() {
    // =========================================================================
    // variables
    // =========================================================================
    const {
      openEmailDialog,
      checkTokenExpiryDate,
      checkValidDevice,
      loginByOnlineAuthForPayment,
      profile,
      environment,
      FNAReportEmail,
      sendEmail,
      tabOnPress,
      tab,
      textStore,
      checkBoxOnPress,
      updateSkipEmailInReport
    } = this.props;
    const { isNetworkConnected } = this.state;
    const { emails, reportOptions } = FNAReportEmail;
    let isSelected;
    if (Config.ENV_NAME === "PROD") {
      this.path = Config.GET_AUTH_CODE_FULL_URL;
    } else if (environment === "DEV") {
      this.path = Config.DEV_GET_AUTH_CODE_FULL_URL;
    } else if (environment === "SIT1_EAB") {
      this.path = Config.SIT1_EAB_GET_AUTH_CODE_FULL_URL;
    } else if (environment === "SIT2_EAB") {
      this.path = Config.SIT2_EAB_GET_AUTH_CODE_FULL_URL;
    } else if (environment === "SIT3_EAB") {
      this.path = Config.SIT3_EAB_GET_AUTH_CODE_FULL_URL;
    } else if (environment === "SIT_AXA") {
      this.path = Config.SIT_AXA_GET_AUTH_CODE_FULL_URL;
    } else if (environment === "UAT") {
      this.path = Config.UAT_GET_AUTH_CODE_FULL_URL;
    } else if (environment === "PRE_PROD") {
      this.path = Config.PRE_PROD_GET_AUTH_CODE_FULL_URL;
    } else {
      this.path = Config.SIT3_EAB_GET_AUTH_CODE_FULL_URL;
    }

    if (emails && reportOptions) {
      if (tab === AGENT_TAB) {
        isSelected = reportOptions.agent.fnaReport;
      } else if (tab === CLIENT_TAB) {
        isSelected = reportOptions.client.fnaReport;
      }
    } else {
      isSelected = true;
    }
    if (this.state.function !== null) {
      this.state.function();
    }

    return (
      <ContextConsumer>
        {({ showEmailDialog, showEmailContentDialog, language }) => (
          <View>
            {this.setfunction(() => {
              if (this.state.function !== null && this.state.setOnlineDone) {
                this.setState(
                  {
                    setOnlineDone: false
                  },
                  () => {
                    showEmailContentDialog(FNA);
                  }
                );
              }
            })}
            <EABButton
              testID="FNAReport__btnEmail"
              style={styles.button}
              onPress={() => {
                updateSkipEmailInReport(false);
                if (!profile.email) {
                  showEmailDialog();
                } else if (this.state.shownEmailDialog) {
                  showEmailContentDialog(FNA);
                } else if (this.state.isNetworkConnected) {
                  openEmailDialog(() => {
                    checkTokenExpiryDate(status => {
                      // status = false if its need to do online auth
                      if (!status) {
                        const title = EABi18n({
                          language,
                          textStore,
                          path: "needs.text.email.title"
                        });
                        const warningMessage = EABi18n({
                          language,
                          textStore,
                          path: "needs.text.onlineAuthWarning"
                        });
                        const openSafari = EABi18n({
                          language,
                          textStore,
                          path: "needs.text.openSafari"
                        });
                        const cancel = EABi18n({
                          language,
                          textStore,
                          path: "needs.text.cancel"
                        });
                        loginByOnlineAuthForPayment();
                        Alert.alert(
                          title,
                          warningMessage,
                          [
                            {
                              text: openSafari,
                              onPress: () => this.pressHandler()
                            },
                            {
                              text: cancel,
                              onPress: () => {},
                              style: "cancel"
                            }
                          ],
                          { cancelable: false }
                        );
                      } else {
                        checkValidDevice(checkValidDeviceResult => {
                          if (!checkValidDeviceResult.hasError) {
                            showEmailContentDialog(FNA);
                          } else {
                            setTimeout(() => {
                              Alert.alert(
                                `[Error] Authorization Failed (Error 007)`
                              );
                            }, 200);
                          }
                        });
                      }
                    });
                  });
                } else {
                  setTimeout(() => {
                    Alert.alert(
                      "Warning",
                      `Please connect to the internet to proceed further`,
                      [
                        {
                          text: "OK",
                          onPress: () => {},
                          style: "cancel"
                        }
                      ],
                      { cancelable: false }
                    );
                  }, 200);
                }
              }}
            >
              <TranslatedText
                style={Theme.textButtonLabelNormalAccent}
                path="button.email"
              />
            </EABButton>
            <EASEEmailDialog />
            <EASEEmailContentDialog
              isNetworkConnected={isNetworkConnected}
              checkBoxOptions={[
                {
                  key: "fnaReport",
                  text: "FNA Report",
                  isSelected
                }
              ]}
              checkBoxOnPress={option => {
                checkBoxOnPress({
                  tab,
                  option
                });
              }}
              tabOnPress={option => {
                tabOnPress(option);
              }}
              sendEmail={sendEmail}
              reportEmail={FNAReportEmail}
              tab={tab}
              disableClientTab={!profile.email}
              reportType={FNA}
            />
            <EABHUD isOpen={this.state.isLoading} />
          </View>
        )}
      </ContextConsumer>
    );
  }
}

FNAReportAppBarRight.propTypes = {
  openEmailDialog: PropTypes.func.isRequired,
  environment: PropTypes.string.isRequired,
  checkTokenExpiryDate: PropTypes.func.isRequired,
  checkValidDevice: PropTypes.func.isRequired,
  setOnlineAuthCodeForPayment: PropTypes.func.isRequired,
  isHandlingListener: PropTypes.bool.isRequired,
  loginByOnlineAuthForPayment: PropTypes.func.isRequired,
  profile: WEB_API_REDUCER_TYPE_CHECK[CLIENT].profile.isRequired,
  FNAReportEmail: WEB_API_REDUCER_TYPE_CHECK[FNA].FNAReportEmail.isRequired,
  sendEmail: PropTypes.func.isRequired,
  checkBoxOnPress: PropTypes.func.isRequired,
  tab: PropTypes.bool.isRequired,
  textStore: APP_REDUCER_TYPE_CHECK[TEXT_STORE].isRequired,
  tabOnPress: PropTypes.func.isRequired,
  updateSkipEmailInReport: PropTypes.func.isRequired
};
