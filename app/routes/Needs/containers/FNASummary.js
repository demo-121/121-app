import { connect } from "react-redux";
import { REDUCER_TYPES } from "eab-web-api";
import FNASummary from "../components/FNASummary";
import { TEXT_STORE } from "../../../constants/REDUCER_TYPES";

const { FNA, CLIENT } = REDUCER_TYPES;

/**
 * FNASummary
 * @requires FNASummary - FNASummary UI
 * */
const mapStateToProps = state => ({
  textStore: state[TEXT_STORE],
  profile: state[CLIENT].profile,
  dependantProfiles: state[CLIENT].dependantProfiles,
  pda: state[FNA].pda,
  na: state[FNA].na
});

const mapDispatchToProps = () => ({});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(FNASummary);
