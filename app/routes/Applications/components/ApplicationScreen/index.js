import React, { PureComponent } from "react";
import { View, ScrollView, Image, Alert } from "react-native";
import LinearGradient from "react-native-linear-gradient";
import PropTypes from "prop-types";
import * as _ from "lodash";
import {
  LANGUAGE_TYPES,
  REDUCER_TYPES,
  REDUCER_TYPE_CHECK,
  EAPP,
  utilities
} from "eab-web-api";

import { ContextConsumer } from "../../../../context";
import APP_REDUCER_TYPE_CHECK from "../../../../constants/REDUCER_TYPE_CHECK";
import { TEXT_STORE } from "../../../../constants/REDUCER_TYPES";

import icNext from "../../../../assets/images/icNext.png";
import icPrevious from "../../../../assets/images/icPrevious.png";
import icCircleArrowDown from "../../../../assets/images/icCircleArrowDown.png";
import icWarning from "../../../../assets/images/icWarning.png";
import { getAttachmentWithID } from "../../../../utilities/DataManager";
import EABBottomSheet from "../../../../components/EABBottomSheet";
import EABButton from "../../../../components/EABButton";
import EABDialog from "../../../../components/EABDialog";
import {
  RECTANGLE_BUTTON_1,
  RECTANGLE_BUTTON_2
} from "../../../../components/EABButton/constants";
import EABHUD from "../../../../containers/EABHUD";
import ApplicationPlanDetails from "../../containers/ApplicationPlanDetails";
import ApplicationShieldPlanDetails from "../../containers/ApplicationShieldPlanDetails";
import EASEProgressTabs from "../../../../components/EASEProgressTabs";
import EABi18n from "../../../../utilities/EABi18n";
import TranslatedText from "../../../../containers/TranslatedText";

import ApplicationForm from "../../containers/ApplicationForm";
import SupportingDocument from "../../../SupportingDocument";
import PaymentAndSubmission from "../../containers/PaymentAndSubmission";
import PaymentAndSubmissionShield from "../../containers/PaymentAndSubmissionShield";
import Signature from "../../containers/Signature";

import {
  BOTTOM_SHEET_COLLAPSE_HEIGHT,
  BOTTOM_SHEET_PLAN_DETAILS_EXPAND_HEIGHT
} from "./constants";
import Theme from "../../../../theme";
import styles from "./styles";

const {
  SWITCHTAB,
  OPENSUPPORTINGDOCUMENT,
  CLOSEAPPLICATION,
  NEXTPAGE
} = EAPP.action;

const {
  checkCanShieldApplicationGoSignature,
  checkCanShieldApplicationGoPayment,
  checkHasPolicyNumber
} = utilities.application;
/**
 * ApplicationScreen
 * @description ApplicationScreen page.
 * @param {function} closeDialog - close dialog function
 * */

class ApplicationScreen extends PureComponent {
  constructor(props) {
    super(props);

    this.prepareProgressTabSteps = this.prepareProgressTabSteps.bind(this);
    this.showCrossAgeAlert = this.showCrossAgeAlert.bind(this);
    this.showSignatureExpiryAlert = this.showSignatureExpiryAlert.bind(this);
    this.handleCrossAgeAlert = this.handleCrossAgeAlert.bind(this);
    this.handleSignatureExpiryAlert = this.handleSignatureExpiryAlert.bind(
      this
    );
    this.invalidateApplicationLoading = this.invalidateApplicationLoading.bind(
      this
    );
    this.renderTitleBar = this.renderTitleBar.bind(this);
    this.renderForm = this.renderForm.bind(this);
    this.renderFooter = this.renderFooter.bind(this);

    this.state = {
      isLoading: false
    };
  }

  componentDidMount() {
    const { isAlertDelayed } = this.props;
    if (!isAlertDelayed) {
      this.handleSignatureExpiryAlert();
      this.handleCrossAgeAlert();
    }
  }

  componentDidUpdate(prevProps) {
    const { isAlertDelayed } = this.props;
    if (isAlertDelayed !== prevProps.isAlertDelayed && !isAlertDelayed) {
      this.handleSignatureExpiryAlert();
      this.handleCrossAgeAlert();
    }
  }

  static getDerivedStateFromProps(props) {
    const { application } = props;
    if (_.isEmpty(application)) {
      return {};
    }
    const iCidArray = application.iCidMapping;
    if (application.quotation.quotType === "SHIELD" && iCidArray) {
      const shieldPolicyNumber = [];
      Object.values(iCidArray).forEach(value => {
        value.forEach(profile => shieldPolicyNumber.push(profile.policyNumber));
      });
      return {
        shieldPolicyNumber
      };
    }
    return {};
  }

  get isShield() {
    const { application } = this.props;
    return application.quotation.quotType === "SHIELD";
  }

  get canGoSigature() {
    const { completedStep, error } = this.props;
    if (this.isShield) {
      return completedStep >= 0 || checkCanShieldApplicationGoSignature(error);
    }
    return completedStep >= 0;
  }

  get canGoPayment() {
    const { completedStep, application, error } = this.props;
    if (this.isShield) {
      return (
        completedStep >= 1 ||
        checkCanShieldApplicationGoPayment({ application, error })
      );
    }
    return completedStep >= 1;
  }

  get isDisableNextButton() {
    const { currentStep, completedStep } = this.props;
    if (this.isShield) {
      return (
        (currentStep === EAPP.tabStep[REDUCER_TYPES.APPLICATION] &&
          !this.canGoSigature) ||
        (currentStep === EAPP.tabStep[REDUCER_TYPES.SIGNATURE] &&
          !this.canGoPayment)
      );
    }
    return currentStep > completedStep;
  }

  get policyNumberExist() {
    const { application } = this.props;
    return checkHasPolicyNumber({ application });
  }

  get isDisableEappButton() {
    return this.props.isDisableEappButton;
  }

  invalidateApplicationLoading() {
    const {
      invalidateApplication,
      getApplicationList,
      closeDialog
    } = this.props;
    this.setState(
      {
        isLoading: true
      },
      () => {
        invalidateApplication(() => {
          this.setState(
            {
              isLoading: false
            },
            () => {
              setTimeout(() => {
                getApplicationList(closeDialog);
              }, 200);
            }
          );
        });
      }
    );
  }

  handleCrossAgeAlert() {
    const { isCrossAgeAlertShow, updateIsCrossAgeAlertShow } = this.props;
    if (isCrossAgeAlertShow) {
      updateIsCrossAgeAlertShow(false);
      setTimeout(() => {
        this.showCrossAgeAlert(LANGUAGE_TYPES.ENGLISH);
      }, 500);
    }
  }

  showCrossAgeAlert(language) {
    const { textStore, crossAge, updateApplicationBackdate } = this.props;
    const {
      crossedAge,
      allowBackdate,
      insuredStatus,
      proposerStatus,
      status
    } = crossAge;

    if (
      ![insuredStatus, proposerStatus, status].some(
        s =>
          s > EAPP.CROSSAGE_STATUS.NO_STATUS &&
          s !== EAPP.CROSSAGE_STATUS.CROSSED_AGE_NO_ACTION &&
          s !== EAPP.CROSSAGE_STATUS.WILL_CROSSAGE_NO_ACTION
      )
    ) {
      return;
    }

    let buttons = [];
    let title = "";
    let message = "";

    const titleReminder = EABi18n({
      language,
      textStore,
      path: "alert.reminder"
    });
    const titleWarning = EABi18n({
      language,
      textStore,
      path: "alert.warning"
    });
    const btnTextYES = EABi18n({
      language,
      textStore,
      path: "button.yes"
    });
    const btnTextNO = EABi18n({
      language,
      textStore,
      path: "button.no"
    });
    const btnTextOK = EABi18n({
      language,
      textStore,
      path: "button.ok"
    });
    const targetLA = EABi18n({
      language,
      textStore,
      path: "application.form.crossAge.targetLa"
    });
    const targetPH = EABi18n({
      language,
      textStore,
      path: "application.form.crossAge.targetPh"
    });
    const targetPHandLA = `${targetPH} ${EABi18n({
      language,
      textStore,
      path: "application.form.crossAge.and"
    })} ${targetLA}`;
    const willCrossMsg = EABi18n({
      language,
      textStore,
      path: "application.form.crossAge.willCrossMsg"
    });
    const crossedNotSignedMsg = EABi18n({
      language,
      textStore,
      path: "application.form.crossAge.crossedNotSignedMsg"
    });
    const crossedSignedMsg = EABi18n({
      language,
      textStore,
      path: "application.form.crossAge.crossedSignedMsg"
    });

    if (this.isShield) {
      switch (status) {
        case EAPP.CROSSAGE_STATUS.WILL_CROSSAGE:
          title = titleReminder;
          message = _.replace(willCrossMsg, "{1}", targetLA);
          buttons.push({
            text: btnTextOK,
            onPress: () => {}
          });
          break;
        case EAPP.CROSSAGE_STATUS.CROSSED_AGE_NOTSIGNED:
          title = titleReminder;
          message = _.replace(crossedNotSignedMsg, "{1}", targetLA);
          buttons.push({
            text: btnTextOK,
            onPress: this.invalidateApplicationLoading
          });
          break;
        case EAPP.CROSSAGE_STATUS.CROSSED_AGE_SIGNED:
          title = titleWarning;
          message = _.replace(crossedSignedMsg, "{1}", targetLA);
          buttons.push({
            text: btnTextOK,
            onPress: () => {}
          });
          break;
        default:
          title = "";
          message = "";
          buttons = [];
          break;
      }
    } else {
      let msgTarget = "";
      let mainMsg = "";
      let backdateMsg = "";

      if (insuredStatus === proposerStatus) {
        msgTarget = targetPHandLA;
      } else if (insuredStatus !== EAPP.CROSSAGE_STATUS.NO_STATUS) {
        msgTarget = targetLA;
      } else if (proposerStatus !== EAPP.CROSSAGE_STATUS.NO_STATUS) {
        msgTarget = targetPH;
      }

      if (
        [insuredStatus, proposerStatus].includes(
          EAPP.CROSSAGE_STATUS.WILL_CROSSAGE
        )
      ) {
        title = titleReminder;
        mainMsg = willCrossMsg;
      } else if (
        [insuredStatus, proposerStatus].includes(
          EAPP.CROSSAGE_STATUS.CROSSED_AGE_NOTSIGNED
        )
      ) {
        title = titleReminder;
        mainMsg = crossedNotSignedMsg;
      } else if (
        [insuredStatus, proposerStatus].includes(
          EAPP.CROSSAGE_STATUS.CROSSED_AGE_SIGNED
        )
      ) {
        title = titleWarning;
        mainMsg = crossedSignedMsg;
      }

      if (
        [insuredStatus, proposerStatus].includes(
          EAPP.CROSSAGE_STATUS.CROSSED_AGE_SIGNED
        )
      ) {
        buttons.push({
          text: btnTextOK,
          onPress: () => {}
        });
      } else if (allowBackdate) {
        backdateMsg = `\n\n${EABi18n({
          language,
          textStore,
          path: "application.form.crossAge.allowBackdate"
        })}`;
        buttons.push({
          text: btnTextYES,
          onPress: () => {
            this.setState(
              {
                isLoading: true
              },
              () => {
                updateApplicationBackdate(() => {
                  this.setState({
                    isLoading: false
                  });
                });
              }
            );
          }
        });
        buttons.push({
          text: btnTextNO,
          style: "cancel",
          onPress: () => {
            if (crossedAge) {
              this.invalidateApplicationLoading();
            }
          }
        });
      } else {
        buttons.push({
          text: btnTextOK,
          onPress: () => {
            if (crossedAge) {
              this.invalidateApplicationLoading();
            }
          }
        });
      }

      message = _.replace(`${mainMsg}${backdateMsg}`, "{1}", msgTarget);
    }

    Alert.alert(_.toUpper(title), message, buttons);
  }

  handleSignatureExpiryAlert() {
    const {
      isSignatureExpiryAlertShow,
      updateIsSignatureExpiryAlert
    } = this.props;
    if (isSignatureExpiryAlertShow) {
      updateIsSignatureExpiryAlert(false);
      setTimeout(() => {
        this.showSignatureExpiryAlert(LANGUAGE_TYPES.ENGLISH);
      }, 500);
    }
  }

  showSignatureExpiryAlert(language) {
    const { textStore, updateShownSignatureExpiryAlert } = this.props;
    Alert.alert(
      _.toUpper(
        EABi18n({
          language,
          textStore,
          path: "alert.warning"
        })
      ),
      EABi18n({
        textStore,
        language,
        path: "application.form.signatureExpiry"
      }),
      [
        {
          text: EABi18n({
            language,
            textStore,
            path: "button.ok"
          }),
          onPress: () => {
            updateShownSignatureExpiryAlert();
          }
        }
      ]
    );
  }

  prepareProgressTabSteps({ language, setEappLoading }) {
    const {
      textStore,
      completedStep,
      updateApplicationStep,
      currentStep,
      application,
      saveForm,
      changeTabShield,
      paymentError
    } = this.props;
    const paymentHasError = _.filter(paymentError, "hasError").length > 0;

    return [
      {
        key: "step_applicationForm",
        title: _.toUpper(
          EABi18n({
            path: "application.form.step.applicationForm",
            language,
            textStore
          })
        ),
        canPress: !this.isDisableEappButton,
        isCompleted: completedStep >= 0 && this.policyNumberExist,
        onPress: () => {
          setEappLoading(true);
          if (this.isShield) {
            changeTabShield({
              currentStep,
              nextStep: 0,
              callback: () => {
                setEappLoading(false, () => {
                  setTimeout(() => {
                    updateApplicationStep({
                      nextStep: 0,
                      isShield: this.isShield
                    });
                  }, 300);
                });
              }
            });
          } else {
            saveForm({
              genPDF: false,
              currentStep,
              nextStep: 0,
              actionType: SWITCHTAB,
              callback: () => {
                setEappLoading(false, () => {
                  setTimeout(() => {
                    updateApplicationStep({
                      nextStep: 0,
                      isShield: this.isShield
                    });
                  }, 300);
                });
              }
            });
          }
        }
      },
      {
        key: "step_signatures",
        title: _.toUpper(
          EABi18n({
            path: "application.form.step.signatures",
            language,
            textStore
          })
        ),
        canPress:
          this.canGoSigature &&
          this.policyNumberExist &&
          !this.isDisableEappButton,
        isCompleted: completedStep >= 1,
        onPress: () => {
          setEappLoading(true);
          if (this.isShield) {
            changeTabShield({
              currentStep,
              nextStep: 1,
              callback: () => {
                setEappLoading(false, () => {
                  setTimeout(() => {
                    updateApplicationStep({
                      nextStep: 1,
                      isShield: this.isShield
                    });
                  }, 300);
                });
              }
            });
          } else {
            saveForm({
              genPDF:
                currentStep === completedStep &&
                completedStep === 0 &&
                !application.isApplicationSigned,
              currentStep,
              nextStep: 1,
              actionType: SWITCHTAB,
              callback: () => {
                setEappLoading(false, () => {
                  setTimeout(() => {
                    updateApplicationStep({
                      nextStep: 1,
                      isShield: this.isShield
                    });
                  }, 300);
                });
              }
            });
          }
        }
      },
      {
        key: "step_paymentAndSubmission",
        title: _.toUpper(
          EABi18n({
            path: "application.form.step.paymentAndSubmission",
            language,
            textStore
          })
        ),
        canPress: this.canGoPayment && !this.isDisableEappButton,
        isCompleted: completedStep >= 2 && !paymentHasError,
        onPress: () => {
          setEappLoading(true);
          if (this.isShield) {
            changeTabShield({
              currentStep,
              nextStep: 2,
              callback: () => {
                setEappLoading(
                  false,
                  () => {
                    updateApplicationStep({
                      nextStep: 2,
                      isShield: this.isShield
                    });
                  },
                  5000
                );
              }
            });
          } else {
            saveForm({
              genPDF: false,
              currentStep,
              nextStep: 2,
              actionType: SWITCHTAB,
              callback: () => {
                setEappLoading(false, () => {
                  setTimeout(() => {
                    updateApplicationStep({
                      nextStep: 2,
                      isShield: this.isShield
                    });
                  }, 300);
                });
              }
            });
          }
        }
      }
    ];
  }

  renderSupportingDocument() {
    return (
      <ContextConsumer>
        {({ isShowingApplicationSupportingDocumentDialog }) => (
          <EABDialog isOpen={isShowingApplicationSupportingDocumentDialog}>
            <SupportingDocument screenProps={{ isShield: this.isShield }} />
          </EABDialog>
        )}
      </ContextConsumer>
    );
  }

  renderTitleBar({ setEappLoading }) {
    const {
      updateUiViewPlanDetails,
      saveForm,
      application,
      getApplicationList,
      currentStep,
      saveFormShield,
      getSupportingDocument,
      submission,
      counterCheckPolicyNumber,
      genFNA
    } = this.props;
    const { shieldPolicyNumber } = this.state;

    let titleBarTitle = application.id;
    if (!this.isShield && application.policyNumber) {
      titleBarTitle = application.policyNumber;
    }
    return (
      <ContextConsumer>
        {({
          showApplicationSupportingDocumentDialog,
          hideApplicationDialog
        }) => (
          <View style={styles.titleBar}>
            <View style={styles.titleBarViewLeft}>
              <EABButton
                isDisable={this.isDisableEappButton}
                testID="Application__btnViewPlanDetails"
                style={styles.titleBarButtonLeft}
                onPress={() => {
                  updateUiViewPlanDetails(true);
                }}
              >
                <TranslatedText
                  style={styles.titleBarButtonTitle}
                  path="application.form.button.viewPlanDetails"
                />
              </EABButton>
            </View>
            <View style={styles.titleBarViewMiddle}>
              <TranslatedText
                style={styles.titleBarTitle}
                path="application.form.title"
                replace={[
                  {
                    value: titleBarTitle,
                    isPath: false
                  }
                ]}
              />
            </View>
            {(!this.isShield && (submission && !submission.isSubmitted)) ||
            (this.isShield &&
              (!application.submitStatus ||
                application.submitStatus !== "SUCCESS")) ? (
              <View style={styles.titleBarViewRight}>
                {!application.isMandDocsAllUploaded ? (
                  <Image style={styles.titleBarIcon} source={icWarning} />
                ) : null}
                <EABButton
                  isDisable={this.isDisableEappButton}
                  testID="Application__btnSupportingDocuments"
                  style={styles.titleBarButtonRight}
                  onPress={() => {
                    const showDialog = showApplicationSupportingDocumentDialog;
                    setEappLoading(true);
                    if (this.isShield) {
                      saveFormShield({
                        currentStep,
                        actionType: OPENSUPPORTINGDOCUMENT,
                        callback: () => {
                          getSupportingDocument({
                            isShield: this.isShield,
                            appId: application.id,
                            // TODO
                            appStatus: EAPP.appStatus.APPLYING,
                            callback: () => {
                              getAttachmentWithID(
                                application.bundleId,
                                "fnaReport",
                                response => {
                                  if (!response) {
                                    genFNA(() => {
                                      setEappLoading(false, () => {
                                        setTimeout(showDialog, 300);
                                      });
                                    });
                                  } else {
                                    setEappLoading(false, () => {
                                      setTimeout(showDialog, 300);
                                    });
                                  }
                                }
                              );
                            }
                          });
                        }
                      });
                    } else {
                      saveForm({
                        genPDF: false,
                        currentStep,
                        actionType: OPENSUPPORTINGDOCUMENT,
                        callback: () => {
                          getSupportingDocument({
                            isShield: this.isShield,
                            appId: application.id,
                            // TODO
                            appStatus: EAPP.appStatus.APPLYING,
                            callback: () => {
                              getAttachmentWithID(
                                application.bundleId,
                                "fnaReport",
                                response => {
                                  if (!response) {
                                    genFNA(() => {
                                      setEappLoading(false, () => {
                                        setTimeout(showDialog, 300);
                                      });
                                    });
                                  } else {
                                    setEappLoading(false, () => {
                                      setTimeout(showDialog, 300);
                                    });
                                  }
                                }
                              );
                            }
                          });
                        }
                      });
                    }
                  }}
                >
                  <TranslatedText
                    style={styles.titleBarButtonTitle}
                    path="button.supportingDocuments"
                  />
                </EABButton>
                <EABButton
                  isDisable={this.isDisableEappButton}
                  testID="Application__btnDone"
                  style={styles.titleBarButtonRight}
                  onPress={() => {
                    setEappLoading(true);
                    if (this.isShield) {
                      if (shieldPolicyNumber.length) {
                        counterCheckPolicyNumber({
                          isShield: true,
                          shieldPolicyNum: shieldPolicyNumber,
                          nonShieldPolicyNum: []
                        });
                      }
                      saveFormShield({
                        currentStep,
                        actionType: CLOSEAPPLICATION,
                        callback: () => {
                          getApplicationList(() => {
                            setTimeout(() => {
                              hideApplicationDialog(() => {
                                setTimeout(() => {
                                  setEappLoading(false);
                                }, 1000);
                              });
                            }, 300);
                          });
                        }
                      });
                    } else {
                      if (application.policyNumber) {
                        counterCheckPolicyNumber({
                          isShield: false,
                          shieldPolicyNum: [],
                          nonShieldPolicyNum: [application.policyNumber]
                        });
                      }
                      saveForm({
                        genPDF: false,
                        currentStep,
                        actionType: CLOSEAPPLICATION,
                        callback: () => {
                          getApplicationList(() => {
                            setTimeout(() => {
                              hideApplicationDialog(() => {
                                setTimeout(() => {
                                  setEappLoading(false);
                                }, 1000);
                              });
                            }, 300);
                          });
                        }
                      });
                    }
                  }}
                >
                  <TranslatedText
                    style={styles.titleBarButtonTitle}
                    path="button.done"
                  />
                </EABButton>
              </View>
            ) : (
              <View style={styles.titleBarViewRight}>
                <EABButton
                  isDisable={this.isDisableEappButton}
                  style={styles.titleBarButtonRight}
                  onPress={() => {
                    setEappLoading(true);
                    if (this.isShield) {
                      if (shieldPolicyNumber.length) {
                        counterCheckPolicyNumber({
                          isShield: true,
                          shieldPolicyNum: shieldPolicyNumber,
                          nonShieldPolicyNum: []
                        });
                      }
                      saveFormShield({
                        currentStep,
                        actionType: CLOSEAPPLICATION,
                        callback: () => {
                          getApplicationList(() => {
                            setTimeout(() => {
                              hideApplicationDialog(() => {
                                setTimeout(() => {
                                  setEappLoading(false);
                                }, 1000);
                              });
                            }, 300);
                          });
                        }
                      });
                    } else {
                      if (application.policyNumber) {
                        counterCheckPolicyNumber({
                          isShield: false,
                          shieldPolicyNum: [],
                          nonShieldPolicyNum: [application.policyNumber]
                        });
                      }
                      saveForm({
                        genPDF: false,
                        currentStep,
                        actionType: CLOSEAPPLICATION,
                        callback: () => {
                          getApplicationList(() => {
                            setTimeout(() => {
                              hideApplicationDialog(() => {
                                setTimeout(() => {
                                  setEappLoading(false);
                                }, 1000);
                              });
                            }, 300);
                          });
                        }
                      });
                    }
                  }}
                >
                  <TranslatedText
                    style={styles.titleBarButtonTitle}
                    path="button.done"
                  />
                </EABButton>
              </View>
            )}
          </View>
        )}
      </ContextConsumer>
    );
  }

  renderForm(setEappLoading) {
    const { currentStep, showPlanDetailsView, application } = this.props;
    if (showPlanDetailsView) {
      return null;
    }

    if (currentStep === 0) {
      return <ApplicationForm />;
    } else if (currentStep === 1 && application.id && this.isShield) {
      return (
        <Signature isShield={this.isShield} setEappLoading={setEappLoading} />
      );
    } else if (currentStep === 1 && application.id) {
      return (
        <Signature
          isShield={this.isShield}
          applicationId={application.id}
          setEappLoading={setEappLoading}
        />
      );
    } else if (currentStep >= 2 && this.isShield) {
      return <PaymentAndSubmissionShield />;
    } else if (currentStep >= 2) {
      return <PaymentAndSubmission />;
    }
    return null;
  }

  renderFooter({ setEappLoading }) {
    const {
      currentStep,
      completedStep,
      showPlanDetailsView,
      updateUiViewPlanDetails,
      updateApplicationStep,
      application,
      saveForm,
      changeTabShield,
      counterCheckPolicyNumber
    } = this.props;
    const { shieldPolicyNumber } = this.state;

    if (showPlanDetailsView) {
      return (
        <EABBottomSheet
          containerStyle={styles.bottomSheetContainer}
          style={styles.bottomSheetView}
          isExpand={showPlanDetailsView}
          expandHeight={BOTTOM_SHEET_PLAN_DETAILS_EXPAND_HEIGHT}
          collapseHeight={BOTTOM_SHEET_COLLAPSE_HEIGHT}
        >
          <View style={styles.planDetailsContentView}>
            <View style={styles.planDetailsControlView}>
              <EABButton
                style={styles.planDetailsButton}
                onPress={() => {
                  updateUiViewPlanDetails(false);
                }}
              >
                <Image source={icCircleArrowDown} />
                <TranslatedText
                  style={styles.planDetailsButtonTitle}
                  path="button.hideDetails"
                />
              </EABButton>
            </View>
            <ScrollView
              style={styles.scrollView}
              contentContainerStyle={styles.scrollViewContent}
              showsVerticalScrollIndicator={false}
            >
              <View style={styles.planDetailsView}>
                {this.isShield ? (
                  <ApplicationShieldPlanDetails />
                ) : (
                  <ApplicationPlanDetails />
                )}
              </View>
            </ScrollView>
          </View>
        </EABBottomSheet>
      );
    }

    return (
      <View style={styles.footer}>
        {currentStep === 0 ? (
          <View />
        ) : (
          <EABButton
            isDisable={this.isDisableEappButton}
            style={styles.bottomSheetStepButtonLeft}
            buttonType={RECTANGLE_BUTTON_2}
            onPress={() => {
              updateApplicationStep({ nextStep: currentStep - 1 });
            }}
          >
            <Image style={Theme.iconStyle} source={icPrevious} />
            <TranslatedText style={Theme.title} path="button.previous" />
          </EABButton>
        )}
        {currentStep >= 2 ? (
          <View />
        ) : (
          <EABButton
            style={styles.bottomSheetStepButtonRight}
            buttonType={RECTANGLE_BUTTON_1}
            isDisable={this.isDisableNextButton || !this.policyNumberExist}
            onPress={() => {
              setEappLoading(true, () => {
                if (this.isShield) {
                  if (shieldPolicyNumber.length) {
                    counterCheckPolicyNumber({
                      isShield: true,
                      shieldPolicyNum: shieldPolicyNumber,
                      nonShieldPolicyNum: []
                    });
                  }
                  changeTabShield({
                    currentStep,
                    nextStep: currentStep + 1,
                    callback: () => {
                      setEappLoading(false, () => {
                        setTimeout(() => {
                          updateApplicationStep({ nextStep: currentStep + 1 });
                        }, 300);
                      });
                    }
                  });
                } else {
                  if (application.policyNumber) {
                    counterCheckPolicyNumber({
                      isShield: false,
                      shieldPolicyNum: [],
                      nonShieldPolicyNum: [application.policyNumber]
                    });
                  }
                  saveForm({
                    quotType: application.quotation.quotType || "",
                    genPDF:
                      currentStep === completedStep &&
                      completedStep === 0 &&
                      !application.isApplicationSigned,
                    currentStep,
                    actionType: NEXTPAGE,
                    callback: () => {
                      setEappLoading(false, () => {
                        setTimeout(() => {
                          updateApplicationStep({ nextStep: currentStep + 1 });
                        }, 300);
                      });
                    }
                  });
                }
              });
            }}
          >
            <TranslatedText style={Theme.title} path="button.next" />
            <Image style={Theme.iconStyle} source={icNext} />
          </EABButton>
        )}
      </View>
    );
  }

  render() {
    const { currentStep, application } = this.props;
    const { isLoading } = this.state;
    if (_.isEmpty(application)) {
      return null;
    }
    return (
      <ContextConsumer>
        {({ language, isEappLoading, setEappLoading }) => (
          <LinearGradient
            {...Theme.LinearGradientProps}
            style={styles.container}
          >
            <View style={styles.header}>
              {this.renderTitleBar({ setEappLoading })}
              <EASEProgressTabs
                steps={this.prepareProgressTabSteps({
                  language,
                  setEappLoading
                })}
                // the last index of ease web is 3 (4 pages), but ios only have 3 pages, so the index will be up to 2
                currentStepIndex={currentStep > 2 ? 2 : currentStep}
              />
            </View>
            <View style={styles.KeyboardAvoidingViewWrapper}>
              {this.renderForm(setEappLoading)}
            </View>
            {this.renderFooter({
              setEappLoading,
              steps: this.prepareProgressTabSteps({
                language,
                setEappLoading
              })
            })}
            {this.renderSupportingDocument()}
            <EABHUD isOpen={isEappLoading || isLoading} />
          </LinearGradient>
        )}
      </ContextConsumer>
    );
  }
}

ApplicationScreen.propTypes = {
  textStore: APP_REDUCER_TYPE_CHECK[TEXT_STORE].isRequired,
  currentStep: PropTypes.number.isRequired,
  completedStep: PropTypes.number.isRequired,
  showPlanDetailsView: PropTypes.bool.isRequired,
  hidePlanDetails: PropTypes.func.isRequired,
  updateApplicationStep: PropTypes.func.isRequired,
  changeTabShield: PropTypes.func.isRequired,
  getSupportingDocument: PropTypes.func.isRequired,
  updateUiViewPlanDetails: PropTypes.func.isRequired,
  paymentError:
    REDUCER_TYPE_CHECK[REDUCER_TYPES.PAYMENT_AND_SUBMISSION].error.isRequired,
  counterCheckPolicyNumber: PropTypes.func.isRequired,
  closeDialog: PropTypes.func.isRequired,
  saveForm: PropTypes.func.isRequired,
  genFNA: PropTypes.func.isRequired,
  saveFormShield: PropTypes.func.isRequired,
  getApplicationList: PropTypes.func.isRequired,
  updateIsCrossAgeAlertShow: PropTypes.func.isRequired,
  updateShownSignatureExpiryAlert: PropTypes.func.isRequired,
  updateApplicationBackdate: PropTypes.func.isRequired,
  updateIsSignatureExpiryAlert: PropTypes.func.isRequired,
  invalidateApplication: PropTypes.func.isRequired,
  application:
    REDUCER_TYPE_CHECK[REDUCER_TYPES.APPLICATION].rootValues.isRequired,
  error: REDUCER_TYPE_CHECK[REDUCER_TYPES.APPLICATION].error.isRequired,
  isAlertDelayed: PropTypes.bool.isRequired,
  isCrossAgeAlertShow: PropTypes.bool.isRequired,
  isSignatureExpiryAlertShow: PropTypes.bool.isRequired,
  crossAge: REDUCER_TYPE_CHECK[REDUCER_TYPES.APPLICATION].crossAge.isRequired,
  submission:
    REDUCER_TYPE_CHECK[REDUCER_TYPES.PAYMENT_AND_SUBMISSION].submission
      .isRequired,
  isDisableEappButton: PropTypes.bool.isRequired
};

export default ApplicationScreen;
