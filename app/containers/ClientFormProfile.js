import { ACTION_TYPES, REDUCER_TYPES } from "eab-web-api";
import { connect } from "react-redux";
import ClientFormProfile from "../components/ClientFormProfile";
import { TEXT_STORE } from "../constants/REDUCER_TYPES";

const { CLIENT, CLIENT_FORM, OPTIONS_MAP } = REDUCER_TYPES;

/**
 * <ClientFormProfile />
 * @requires ClientFormProfile - ClientFormContact UI
 * */
const mapStateToProps = state => ({
  isCreate: state[CLIENT_FORM].config.isCreate,
  isFamilyMember: state[CLIENT_FORM].config.isFamilyMember,
  isFromProfile: state[CLIENT_FORM].config.isFromProfile,
  isProposerMissing: state[CLIENT_FORM].config.isProposerMissing,
  isFromProduct: state[CLIENT_FORM].config.isFromProduct,
  isPDA: state[CLIENT_FORM].config.isPDA,
  relationship: state[CLIENT_FORM].relationship,
  isRelationshipError: state[CLIENT_FORM].isRelationshipError,
  relationshipOther: state[CLIENT_FORM].relationshipOther,
  isRelationshipOtherError: state[CLIENT_FORM].isRelationshipOtherError,
  givenName: state[CLIENT_FORM].givenName,
  isGivenNameError: state[CLIENT_FORM].isGivenNameError,
  surname: state[CLIENT_FORM].surname,
  otherName: state[CLIENT_FORM].otherName,
  hanYuPinYinName: state[CLIENT_FORM].hanYuPinYinName,
  haveSignDoc: state[CLIENT_FORM].profileBackUp.haveSignDoc,
  name: state[CLIENT_FORM].name,
  isNameError: state[CLIENT_FORM].isNameError,
  nameOrder: state[CLIENT_FORM].nameOrder,
  title: state[CLIENT_FORM].title,
  isTitleError: state[CLIENT_FORM].isTitleError,
  gender: state[CLIENT_FORM].gender,
  isGenderError: state[CLIENT_FORM].isGenderError,
  birthday: state[CLIENT_FORM].birthday,
  isBirthdayError: state[CLIENT_FORM].isBirthdayError,
  nationality: state[CLIENT_FORM].nationality,
  isNationalityError: state[CLIENT_FORM].isNationalityError,
  singaporePRStatus: state[CLIENT_FORM].singaporePRStatus,
  isSingaporePRStatusError: state[CLIENT_FORM].isSingaporePRStatusError,
  countryOfResidence: state[CLIENT_FORM].countryOfResidence,
  cityOfResidence: state[CLIENT_FORM].cityOfResidence,
  isCityOfResidenceError: state[CLIENT_FORM].isCityOfResidenceError,
  otherCityOfResidence: state[CLIENT_FORM].otherCityOfResidence,
  isOtherCityOfResidenceError: state[CLIENT_FORM].isOtherCityOfResidenceError,
  IDDocumentType: state[CLIENT_FORM].IDDocumentType,
  isIDDocumentTypeError: state[CLIENT_FORM].isIDDocumentTypeError,
  IDDocumentTypeOther: state[CLIENT_FORM].IDDocumentTypeOther,
  isIDDocumentTypeOtherError: state[CLIENT_FORM].isIDDocumentTypeOtherError,
  ID: state[CLIENT_FORM].ID,
  isIDError: state[CLIENT_FORM].isIDError,
  smokingStatus: state[CLIENT_FORM].smokingStatus,
  isSmokingError: state[CLIENT_FORM].isSmokingError,
  maritalStatus: state[CLIENT_FORM].maritalStatus,
  isMaritalStatusError: state[CLIENT_FORM].isMaritalStatusError,
  language: state[CLIENT_FORM].language,
  isLanguageError: state[CLIENT_FORM].isLanguageError,
  languageOther: state[CLIENT_FORM].languageOther,
  isLanguageOtherError: state[CLIENT_FORM].isLanguageOtherError,
  education: state[CLIENT_FORM].education,
  isEducationError: state[CLIENT_FORM].isEducationError,
  employStatus: state[CLIENT_FORM].employStatus,
  isEmployStatusError: state[CLIENT_FORM].isEmployStatusError,
  employStatusOther: state[CLIENT_FORM].employStatusOther,
  isEmployStatusOtherError: state[CLIENT_FORM].isEmployStatusOtherError,
  industry: state[CLIENT_FORM].industry,
  isIndustryError: state[CLIENT_FORM].isIndustryError,
  occupation: state[CLIENT_FORM].occupation,
  isOccupationError: state[CLIENT_FORM].isOccupationError,
  occupationOther: state[CLIENT_FORM].occupationOther,
  isOccupationOtherError: state[CLIENT_FORM].isOccupationOtherError,
  nameOfEmployBusinessSchool: state[CLIENT_FORM].nameOfEmployBusinessSchool,
  countryOfEmployBusinessSchool:
    state[CLIENT_FORM].countryOfEmployBusinessSchool,
  income: state[CLIENT_FORM].income,
  isIncomeError: state[CLIENT_FORM].isIncomeError,
  typeOfPass: state[CLIENT_FORM].typeOfPass,
  typeOfPassOther: state[CLIENT_FORM].typeOfPassOther,
  isTypeOfPassError: state[CLIENT_FORM].isTypeOfPassError,
  isTypeOfPassOtherError: state[CLIENT_FORM].isTypeOfPassOtherError,
  passExpiryDate: state[CLIENT_FORM].passExpiryDate,
  optionsMap: state[OPTIONS_MAP],
  textStore: state[TEXT_STORE],
  photo: state[CLIENT_FORM].photo,
  mainCid: state[CLIENT].profile.cid
});

const mapDispatchToProps = dispatch => ({
  relationshipOnChange: newRelationship => {
    dispatch({
      type: ACTION_TYPES[CLIENT_FORM].RELATIONSHIP_ON_CHANGE,
      newRelationship
    });
  },
  relationshipOtherOnChange: newRelationshipOther => {
    dispatch({
      type: ACTION_TYPES[CLIENT_FORM].RELATIONSHIP_OTHER_ON_CHANGE,
      newRelationshipOther
    });
  },
  givenNameOnChange: newGivenName => {
    dispatch({
      type: ACTION_TYPES[CLIENT_FORM].GIVEN_NAME_ON_CHANGE,
      newGivenName
    });
  },
  surnameOnChange: newSurname => {
    dispatch({
      type: ACTION_TYPES[CLIENT_FORM].SURNAME_ON_CHANGE,
      newSurname
    });
  },
  otherNameOnChange: newOtherName => {
    dispatch({
      type: ACTION_TYPES[CLIENT_FORM].OTHER_NAME_ON_CHANGE,
      newOtherName
    });
  },
  hanYuPinYinNameOnChange: newHanYuPinYinName => {
    dispatch({
      type: ACTION_TYPES[CLIENT_FORM].HAN_YU_PIN_YIN_ON_CHANGE,
      newHanYuPinYinName
    });
  },
  nameOnChange: newName => {
    dispatch({
      type: ACTION_TYPES[CLIENT_FORM].NAME_ON_CHANGE,
      newName
    });
  },
  nameOrderOnChange: newNameOrder => {
    dispatch({
      type: ACTION_TYPES[CLIENT_FORM].NAME_ORDER_ON_CHANGE,
      newNameOrder
    });
  },
  titleOnChange: newTitle => {
    dispatch({
      type: ACTION_TYPES[CLIENT_FORM].TITLE_ON_CHANGE,
      newTitle
    });
  },
  genderOnChange: newGender => {
    dispatch({
      type: ACTION_TYPES[CLIENT_FORM].GENDER_ON_CHANGE,
      newGender
    });
  },
  birthdayOnChange: newBirthday => {
    dispatch({
      type: ACTION_TYPES[CLIENT_FORM].BIRTHDAY_ON_CHANGE,
      newBirthday
    });
  },
  nationalityOnChange: newNationality => {
    dispatch({
      type: ACTION_TYPES[CLIENT_FORM].NATIONALITY_ON_CHANGE,
      newNationality
    });
  },
  singaporePRStatusOnChange: newSingaporePRStatus => {
    dispatch({
      type: ACTION_TYPES[CLIENT_FORM].SINGAPORE_PR_STATUS_ON_CHANGE,
      newSingaporePRStatus
    });
  },
  countryOfResidenceOnChange: newCountryOfResidence => {
    dispatch({
      type: ACTION_TYPES[CLIENT_FORM].COUNTRY_OF_RESIDENCE_ON_CHANGE,
      newCountryOfResidence
    });
    dispatch({
      type: ACTION_TYPES[CLIENT_FORM].CITY_STATE_ON_CHANGE,
      newCityState: ""
    });
  },
  cityOfResidenceOnChange: newCityOfResidence => {
    dispatch({
      type: ACTION_TYPES[CLIENT_FORM].CITY_OF_RESIDENCE_ON_CHANGE,
      newCityOfResidence
    });
    dispatch({
      type: ACTION_TYPES[CLIENT_FORM].OTHER_CITY_OF_RESIDENCE_ON_CHANGE,
      newOtherCityOfResidence: ""
    });
  },
  otherCityOfResidenceOnChange: newOtherCityOfResidence => {
    dispatch({
      type: ACTION_TYPES[CLIENT_FORM].OTHER_CITY_OF_RESIDENCE_ON_CHANGE,
      newOtherCityOfResidence
    });
  },
  IDDocumentTypeOnChange: ({
    newIDDocType,
    nationalityData,
    singaporePRStatusData
  }) => {
    dispatch({
      type: ACTION_TYPES[CLIENT_FORM].ID_DOCUMENT_TYPE_ON_CHANGE,
      newIDDocType,
      nationalityData,
      singaporePRStatusData
    });
  },
  IDDocumentTypeOtherOnChange: newIDDocTypeOther => {
    dispatch({
      type: ACTION_TYPES[CLIENT_FORM].ID_DOCUMENT_TYPE_OTHER_ON_CHANGE,
      newIDDocTypeOther
    });
  },
  IDOnChange: newID => {
    dispatch({
      type: ACTION_TYPES[CLIENT_FORM].ID_ON_CHANGE,
      newID
    });
  },
  validateID: ({ IDData, IDDocumentTypeData }) => {
    dispatch({
      type: ACTION_TYPES[CLIENT_FORM].VALIDATE_ID,
      IDData,
      IDDocumentTypeData
    });
  },
  smokingStatusOnChange: newSmokingStatus => {
    dispatch({
      type: ACTION_TYPES[CLIENT_FORM].SMOKING_STATUS_ON_CHANGE,
      newSmokingStatus
    });
  },
  maritalStatusOnChange: ({ newMaritalStatus, relationshipData }) => {
    dispatch({
      type: ACTION_TYPES[CLIENT_FORM].MARITAL_STATUS_ON_CHANGE,
      newMaritalStatus,
      relationshipData
    });
  },
  languageOnChange: newLanguage => {
    dispatch({
      type: ACTION_TYPES[CLIENT_FORM].LANGUAGE_ON_CHANGE,
      newLanguage
    });
  },
  languageOtherOnChange: newLanguageOther => {
    dispatch({
      type: ACTION_TYPES[CLIENT_FORM].LANGUAGE_OTHER_ON_CHANGE,
      newLanguageOther
    });
  },
  educationOnChange: newEducation => {
    dispatch({
      type: ACTION_TYPES[CLIENT_FORM].EDUCATION_ON_CHANGE,
      newEducation
    });
  },
  employStatusOnChange: newEmployStatus => {
    dispatch({
      type: ACTION_TYPES[CLIENT_FORM].EMPLOY_STATUS_ON_CHANGE,
      newEmployStatus
    });
  },
  employStatusOtherOnChange: newEmployStatusOther => {
    dispatch({
      type: ACTION_TYPES[CLIENT_FORM].EMPLOY_STATUS_OTHER_ON_CHANGE,
      newEmployStatusOther
    });
  },
  industryOnChange: newIndustry => {
    dispatch({
      type: ACTION_TYPES[CLIENT_FORM].INDUSTRY_ON_CHANGE,
      newIndustry
    });
  },
  occupationOnChange: ({ newOccupation, occupationCurrently }) => {
    dispatch({
      type: ACTION_TYPES[CLIENT_FORM].OCCUPATION_ON_CHANGE,
      newOccupation,
      occupationCurrently
    });
  },
  occupationOtherOnChange: newOccupationOther => {
    dispatch({
      type: ACTION_TYPES[CLIENT_FORM].OCCUPATION_OTHER_ON_CHANGE,
      newOccupationOther
    });
  },
  nameOfEmployBusinessSchoolOnChange: ({
    newNameOfEmployBusinessSchool,
    employStatusData,
    birthdayData
  }) => {
    dispatch({
      type: ACTION_TYPES[CLIENT_FORM].NAME_OF_EMPLOY_BUSINESS_SCHOOL_ON_CHANGE,
      newNameOfEmployBusinessSchool,
      employStatusData,
      birthdayData
    });
  },
  countryOfEmployBusinessSchoolOnChange: ({
    newCountryOfEmployBusinessSchool,
    employStatusData,
    birthdayData
  }) => {
    dispatch({
      type:
        ACTION_TYPES[CLIENT_FORM].COUNTRY_OF_EMPLOY_BUSINESS_SCHOOL_ON_CHANGE,
      newCountryOfEmployBusinessSchool,
      employStatusData,
      birthdayData
    });
  },
  typeOfPassOnChange: newTypeOfPass => {
    dispatch({
      type: ACTION_TYPES[CLIENT_FORM].TYPE_OF_PASS_ON_CHANGE,
      newTypeOfPass
    });
  },
  typeOfPassOtherOnChange: newTypeOfPassOther => {
    dispatch({
      type: ACTION_TYPES[CLIENT_FORM].TYPE_OF_PASS_OTHER_ON_CHANGE,
      newTypeOfPassOther
    });
  },
  passExpiryDateOnChange: newPassExpiryDate => {
    dispatch({
      type: ACTION_TYPES[CLIENT_FORM].PASS_EXPIRY_DATE_ON_CHANGE,
      newPassExpiryDate
    });
  },
  incomeOnChange: newIncome => {
    dispatch({
      type: ACTION_TYPES[CLIENT_FORM].INCOME_ON_CHANGE,
      newIncome
    });
  },
  photoOnChange: newPhoto => {
    dispatch({
      type: ACTION_TYPES[CLIENT_FORM].PHOTO_ON_CHANGE,
      newPhoto
    });
  },
  deleteClient(type, callback) {
    if (type === "DELETE") {
      dispatch({
        type: ACTION_TYPES[CLIENT_FORM].DELETE_CLIENT,
        callback
      });
    } else if (type === "UNLINK") {
      dispatch({
        type: ACTION_TYPES[CLIENT_FORM].UNLINK_CLIENT,
        callback
      });
    }
  },
  reloadContactList(callback) {
    dispatch({
      type: ACTION_TYPES[CLIENT].GET_CONTACT_LIST,
      callback
    });
  },
  getProfileData(cid, callback) {
    dispatch({
      type: ACTION_TYPES[CLIENT].GET_PROFILE,
      cid,
      callback
    });
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ClientFormProfile);
