import PropTypes from "prop-types";
import React, { Component } from "react";
import { Text, View } from "react-native";
import * as _ from "lodash";
import EABTextField from "../../../../components/EABTextField";
import EABStepper from "../../../../components/EABStepper";
import { ContextConsumer } from "../../../../context";
import styles from "./styles";

// TODO: THIS FILE WILL BE REMOVED AND BE REPLACED BY A NEW FILE IN TEMPLATES FOLDER
/**
 * AnalysisCriticalIllnessProTemplate
 * @description Analysis - Critical Illness Protection - Template
 * */
export default class AnalysisCriticalIllnessProTemplate extends Component {
  componentWillMount() {
    const data = this.checkDependant();
    this.setState({
      goalsArray: data.goals,
      goalsNumber: data.goals.length
    });
  }

  getContent(language) {
    const {
      // todo
      // textStore,
      selectedProfile,
      rule
    } = this.props;
    const {
      goalNameOnChange,
      timeHorizonOnChange,
      compFvOnChange,
      projMaturityOnChange
    } = this.props.dispatch;
    const { goalsArray, goalsNumber } = this.state;
    if (!goalsArray) {
      return null;
    }
    const validate = this.initValidate();
    const contentArr = [];
    goalsArray.forEach((data, index) => {
      contentArr.push(
        <View key={123} style={styles.analysisProductContent}>
          <View style={styles.analysisSectionBorderLine} />
          <Text style={styles.analysisProductPointTitle}>
            1. Lifestyle goal
          </Text>
          <EABTextField
            style={styles.analysisTextInputShort}
            value={data ? data.goalName : null}
            placeholder="Monthly Replacement Income"
            // TODO
            //   placeholder={EABi18n({
            //     language,
            //     textStore,
            //     path: "clientForm.otherField"
            //   })}
            isError={validate.goalName ? validate.goalName.hasError : false}
            hintText={
              validate.goalName ? validate.goalName.message[language] : ""
            }
            onChange={value =>
              goalNameOnChange(
                Object.assign(
                  {},
                  { selectedProfile, value, index, goalsArray },
                  rule.goalName ? { rule: rule.goalName } : { rule: null }
                )
              )
            }
          />
          <View style={styles.analysisSectionBorderLine} />
          <Text style={styles.analysisProductPointTitle}>
            2. Accumulation period
          </Text>
          <EABStepper
            value={data ? parseInt(data.timeHorizon, 10) : null}
            step={1}
            min={0}
            onChange={value =>
              timeHorizonOnChange({ selectedProfile, value, index, goalsArray })
            }
          />
          <View style={styles.analysisSectionBorderLine} />
          <Text style={styles.analysisProductPointTitle}>
            3. Total amount (Future value)
          </Text>
          <EABTextField
            style={styles.analysisTextInputShort}
            prefix="$"
            value={data ? data.compFv : 0}
            placeholder="Monthly Replacement Income"
            // TODO
            //   placeholder={EABi18n({
            //     language,
            //     textStore,
            //     path: "clientForm.otherField"
            //   })}
            isError={validate.compFv ? validate.compFv.hasError : false}
            hintText={validate.compFv ? validate.compFv.message[language] : ""}
            onChange={value =>
              compFvOnChange(
                Object.assign(
                  {},
                  { selectedProfile, value, index, goalsArray },
                  rule.compFv ? { rule: rule.compFv } : { rule: null }
                )
              )
            }
          />
          {/* TODO special handling assets !!!!! */}
          {/* {assetsSelector !== null ? (
            <View>
              <Text style={styles.analysisProductPointTitle}>
                4. Value of Assets/Regular Savings/Investments to be used (COMP
                FV) (Less):
              </Text>
              {assetsSelector}
              <View style={styles.analysisSectionBorderLine} />
            </View>
          ) : null} */}
          <View style={styles.analysisSectionBorderLine} />
          <Text style={styles.analysisProductPointTitle}>
            5. Estimated maturity value of existing insurance policies (Less)
          </Text>
          <EABTextField
            style={styles.analysisTextInputShort}
            prefix="$"
            value={data ? data.projMaturity : 0}
            placeholder="Monthly Replacement Income"
            // TODO
            //   placeholder={EABi18n({
            //     language,
            //     textStore,
            //     path: "clientForm.otherField"
            //   })}
            isError={
              validate.projMaturity ? validate.projMaturity.hasError : false
            }
            hintText={
              validate.projMaturity
                ? validate.projMaturity.message[language]
                : ""
            }
            onChange={value =>
              projMaturityOnChange(
                Object.assign(
                  {},
                  { selectedProfile, value, index, goalsArray },
                  rule.projMaturity
                    ? { rule: rule.projMaturity }
                    : { rule: null }
                )
              )
            }
          />
          <View style={styles.analysisSectionBorderLine} />
        </View>
      );
    });
    const emptryGoalsObj = {
      assets: [],
      cid: "",
      compFv: 0,
      goalName: "",
      projMaturity: 0,
      timeHorizon: "0",
      totShortfall: 0
    };
    contentArr.unshift(
      <View key={123} style={styles.analysisProductContent}>
        <Text style={styles.analysisProductPointTitle}>
          How many goals do you have
        </Text>
        <EABStepper
          value={goalsNumber}
          step={1}
          min={1}
          onChange={value => {
            const cloneGoalsArray = goalsArray;
            if (goalsNumber > value) {
              cloneGoalsArray.pop();
            } else {
              cloneGoalsArray.push(emptryGoalsObj);
            }
            this.setState({
              goalsArray: cloneGoalsArray,
              goalsNumber: value
            });
          }}
        />
      </View>
    );

    return contentArr;
  }

  checkDependant() {
    const { selectedProfile, data } = this.props;
    if (!data || _.isEmpty(data)) {
      return {};
    }
    if (selectedProfile !== "owner" && selectedProfile !== "spouse") {
      if (data.dependants && data.dependants.length) {
        return data.dependants.find(val => val.cid === selectedProfile);
      }
    } else {
      return data[selectedProfile];
    }
    return "";
  }

  initValidate() {
    const { selectedProfile, data } = this.props;
    if (!data || _.isEmpty(data)) {
      return {};
    }
    let targetProfile = {};
    if (selectedProfile !== "spouse" && selectedProfile !== "owner") {
      targetProfile = data.dependants.find(val => val.cid === selectedProfile);
    } else {
      targetProfile = data[selectedProfile];
    }
    if (!targetProfile.validate) {
      return {};
    }
    return Object.keys(targetProfile.validate).reduce((preValue, currIndex) => {
      preValue[currIndex] = targetProfile.validate[currIndex];
      return preValue;
    }, {});
  }

  render() {
    return (
      <ContextConsumer>
        {({ language }) => this.getContent(language)}
      </ContextConsumer>
    );
  }
}

AnalysisCriticalIllnessProTemplate.propTypes = {
  selectedProfile: PropTypes.string.isRequired,
  // TODO
  // textStore: PropTypes.oneOfType([PropTypes.object]).isRequired,
  data: PropTypes.oneOfType([PropTypes.object]).isRequired,
  rule: PropTypes.oneOfType([PropTypes.object]).isRequired
};
