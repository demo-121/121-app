import PropTypes from "prop-types";
import React, { Component } from "react";
import { Text, View, Image } from "react-native";
import LinearGradient from "react-native-linear-gradient";
import * as _ from "lodash";
import {
  PRODUCT_TYPES,
  REDUCER_TYPE_CHECK as WEB_API_REDUCER_TYPE_CHECK,
  REDUCER_TYPES
} from "eab-web-api";
import { TEXT_STORE } from "../../../../constants/REDUCER_TYPES";
import EABButton from "../../../../components/EABButton";
import REDUCER_TYPE_CHECK from "../../../../constants/REDUCER_TYPE_CHECK";
import EASEProgressTabs from "../../../../components/EASEProgressTabs";
import { ContextConsumer } from "../../../../context";
import Theme from "../../../../theme";
import styles from "./styles";
import EABi18n from "../../../../utilities/EABi18n";
import confirmationAlert from "../../../../utilities/confirmationAlert";
import Needs from "../../containers/Needs";
import ROI from "../../containers/ROI";
import Analysis from "../../containers/Analysis";
import Priority from "../../containers/Priority";
import ProductTypes from "../../containers/ProductTypes";
import icNext from "../../../../assets/images/icNext.png";
import icPrevious from "../../../../assets/images/icPrevious.png";
import {
  RECTANGLE_BUTTON_1,
  RECTANGLE_BUTTON_2
} from "../../../../components/EABButton/constants";
import TranslatedText from "../../../../containers/TranslatedText";

const { FNA, CLIENT, SIGNATURE, PRE_APPLICATION } = REDUCER_TYPES;

/**
 * <NeedsAnalysis />
 * */

export default class NeedsAnalysis extends Component {
  constructor(props) {
    super(props);
    const { na } = this.props;
    this.steps = [
      "na.tabname.first.title",
      "na.tabname.second.title",
      "na.tabname.third.title",
      "na.tabname.forth.title",
      "na.tabname.fifth.title"
    ];

    /**
     * @state {bool} isShowingCreateProfileDialog - an indicator on the create profile dialog
     * @state {bool} isLinkMode - an indicator for the ClientFormProfile
     * @state {bool} showProfileTab - an indicator for the Profile Tab
     * @state {number} currentStep - an indicator for showing the current step number
     * @state {number} noticesDescriptionHeight - height for the notice description
     * @state {bool} layoutFirstTime - an indicator for showing the first time
     */
    let currentStep = 0;

    if (na.completedStep < 4) {
      currentStep = na.completedStep + 1;
    } else if (na.completedStep === 4) {
      currentStep = 4;
    }
    this.state = {
      currentStep
    };
  }

  componentDidMount() {
    this.props.updateNaIsGloballyChanged(false);
  }
  /**
   * hvNextStep - shows the possible of next step based on current step number
   * */
  hvNextStep() {
    if (this.state.currentStep === 4) {
      return false;
    }
    return true;
  }

  /**
   * @description save NA after click yes in confirmation alert
   * */
  saveNaWithConfirm(callback) {
    this.props.saveNA({
      confirm: true,
      callback
    });
  }
  /**
   * @description save NA after clicking Done button
   * */
  saveNaWithoutConfirm({ language, callback }) {
    const {
      isChanged,
      isGloballyChanged,
      updateIsCompleted,
      textStore,
      saveNA,
      na,
      updateNA,
      pda,
      profile,
      fe,
      dependantProfiles
      // signature,
      // preApplication
    } = this.props;
    const { currentStep } = this.state;
    const naCompletedWithCkaRa =
      (na.productType.prodType.indexOf("investLinkedPlans") === -1 ||
        (na.productType.prodType.indexOf("investLinkedPlans") !== -1 &&
          na.ckaSection.isValid)) &&
      ((na.productType.prodType.indexOf("investLinkedPlans") === -1 &&
        na.productType.prodType.indexOf("participatingPlans") === -1) ||
        ((na.productType.prodType.indexOf("investLinkedPlans") !== -1 ||
          na.productType.prodType.indexOf("participatingPlans") !== -1) &&
          na.raSection.isValid));

    const naCompletedWithNoCkaRa =
      na.productType.prodType.indexOf("investLinkedPlans") === -1 &&
      na.productType.prodType.indexOf("participatingPlans") === -1 &&
      (na.productType.prodType.indexOf("nonParticipatingPlan") !== -1 ||
        na.productType.prodType.indexOf("accidentHealthPlans") !== -1);

    const saveNAFunction = () => {
      saveNA({
        confirm: false,
        callback: code => {
          if (code && isGloballyChanged) {
            setTimeout(() => {
              confirmationAlert({
                messagePath: `error.${code}`,
                language,
                textStore,
                yesOnPress: () => {
                  this.saveNaWithConfirm(callback);
                }
              });
            }, 200);
          } else {
            callback();
          }
        }
      });
    };

    // (currentStep === 4 && !_.isEmpty(na.productType.prodType)) for incomplete error #37056
    if (
      isChanged ||
      (currentStep === 4 && !_.isEmpty(na.productType.prodType))
    ) {
      if (naCompletedWithNoCkaRa) {
        updateIsCompleted(true);
      }
      saveNAFunction();
    } else if (
      currentStep === 4 &&
      na.isCompleted === false &&
      na.productType.prodType !== "" &&
      naCompletedWithCkaRa
    ) {
      updateNA({
        naData: {
          ...na,
          isCompleted: true
        },
        pdaData: pda,
        profileData: profile,
        feData: fe,
        dependantProfilesData: dependantProfiles,
        resetProdType: false
      });

      saveNAFunction();
    } else {
      callback();
    }
  }

  render() {
    const { textStore, completedStep } = this.props;
    const renderForm = language => {
      switch (this.state.currentStep) {
        case 0:
          return <Needs />;
        case 1:
          return <ROI />;
        case 2:
          return <Analysis language={language} />;
        case 3:
          return <Priority setScrollEnabled={this.setScrollEnabled} />;
        case 4:
          return <ProductTypes />;
        default:
          throw new Error("please check currentStep state");
      }
    };

    const renderEndButton = language => {
      const { na } = this.props;
      const prodTypeArr = na.productType.prodType.split(",");
      const {
        INVESTLINKEDPLANS,
        PARTICIPATINGPLANS,
        NONPARTICIPATINGPLAN,
        ACCIDENTHEALTHPLANS
      } = PRODUCT_TYPES;

      if (prodTypeArr.indexOf(INVESTLINKEDPLANS) > -1) {
        return (
          <EABButton
            testID="NeedsAnalysis__PT__btnContinueToCKA"
            buttonType={RECTANGLE_BUTTON_1}
            onPress={() => {
              this.saveNaWithoutConfirm({
                language,
                callback: () => {
                  this.props.openCKADialog();
                }
              });
            }}
          >
            <Text style={styles.footerNextButtonText}>
              {EABi18n({
                path: "na.footer.continueToCKA",
                language,
                textStore
              })}
            </Text>
            <Image style={Theme.iconStyle} source={icNext} />
          </EABButton>
        );
      } else if (prodTypeArr.indexOf(PARTICIPATINGPLANS) > -1) {
        return (
          <EABButton
            testID="NeedsAnalysis__PT__btnContinueToRA"
            buttonType={RECTANGLE_BUTTON_1}
            onPress={() => {
              this.saveNaWithoutConfirm({
                language,
                callback: () => {
                  this.props.openRADialog();
                }
              });
            }}
          >
            <Text style={styles.footerNextButtonText}>
              {EABi18n({
                path: "na.footer.continueToRA",
                language,
                textStore
              })}
            </Text>
            <Image style={Theme.iconStyle} source={icNext} />
          </EABButton>
        );
      } else if (
        prodTypeArr.indexOf(NONPARTICIPATINGPLAN) > -1 ||
        prodTypeArr.indexOf(ACCIDENTHEALTHPLANS) > -1
      ) {
        return (
          <EABButton
            testID="NeedsAnalysis__PT__btnNext"
            buttonType={RECTANGLE_BUTTON_1}
            onPress={() => {
              this.saveNaWithoutConfirm({
                language,
                callback: () => {
                  this.props.toProductPage();
                }
              });
            }}
          >
            <Text style={styles.footerNextButtonText}>
              {EABi18n({
                path: "button.next",
                language,
                textStore
              })}
            </Text>
            <Image style={Theme.iconStyle} source={icNext} />
          </EABButton>
        );
      }
      return <View />;
    };

    return (
      <ContextConsumer>
        {({ language }) => (
          <LinearGradient
            {...Theme.LinearGradientProps}
            style={styles.container}
          >
            <View style={styles.header}>
              <View style={styles.titleBar}>
                <EABButton
                  containerStyle={[
                    Theme.headerLeft,
                    { left: Theme.alignmentXL }
                  ]}
                  onPress={() => {
                    this.saveNaWithoutConfirm({
                      language,
                      callback: () => {
                        this.props.closeDialog();
                      }
                    });
                  }}
                >
                  <TranslatedText
                    style={Theme.textButtonLabelNormalEmphasizedAccent}
                    path="button.save"
                  />
                </EABButton>

                <Text style={Theme.title}>
                  {EABi18n({
                    path: "na.title",
                    language,
                    textStore
                  })}
                </Text>
                <EABButton
                  containerStyle={[
                    Theme.headerRight,
                    { right: Theme.alignmentXL }
                  ]}
                  onPress={() => {
                    this.saveNaWithoutConfirm({
                      language,
                      callback: () => {
                        this.props.closeDialog();
                      }
                    });
                  }}
                  isDisable={
                    _.isEmpty(this.props.na.productType.prodType) ||
                    completedStep !== 4
                  }
                >
                  <Text style={Theme.textButtonLabelNormalEmphasizedAccent}>
                    Done
                  </Text>
                </EABButton>
              </View>
              <EASEProgressTabs
                testID="NeedsAnalysis"
                steps={[
                  {
                    key: "a",
                    testID: "Needs",
                    title: EABi18n({
                      path: this.steps[0],
                      language,
                      textStore
                    }),
                    canPress: true,
                    isCompleted: completedStep >= 0,
                    onPress: () => {
                      this.saveNaWithoutConfirm({
                        language,
                        callback: () => {
                          this.setState({
                            currentStep: 0
                          });
                        }
                      });
                    }
                  },
                  {
                    key: "b",
                    testID: "ROI",
                    title: EABi18n({
                      path: this.steps[1],
                      language,
                      textStore
                    }),
                    canPress: completedStep >= 0,
                    isCompleted: completedStep >= 1,
                    onPress: () => {
                      this.saveNaWithoutConfirm({
                        language,
                        callback: () => {
                          this.setState({
                            currentStep: 1
                          });
                        }
                      });
                    }
                  },
                  {
                    key: "c",
                    testID: "Analysis",
                    title: EABi18n({
                      path: this.steps[2],
                      language,
                      textStore
                    }),
                    canPress: completedStep >= 1,
                    isCompleted: completedStep >= 2,
                    onPress: () => {
                      this.props.initialValidateNA({
                        naData: this.props.na,
                        profileData: this.props.profile,
                        dependantProfilesData: this.props.dependantProfiles,
                        feData: this.props.fe
                      });
                      this.saveNaWithoutConfirm({
                        language,
                        callback: () => {
                          this.setState({
                            currentStep: 2
                          });
                        }
                      });
                    }
                  },
                  {
                    key: "d",
                    testID: "Priority",
                    title: EABi18n({
                      path: this.steps[3],
                      language,
                      textStore
                    }),
                    canPress: completedStep >= 2,
                    isCompleted: completedStep >= 3,
                    onPress: () => {
                      this.saveNaWithoutConfirm({
                        language,
                        callback: () => {
                          this.setState({
                            currentStep: 3
                          });
                        }
                      });
                    }
                  },
                  {
                    key: "e",
                    testID: "ProductTypes",
                    title: EABi18n({
                      path: this.steps[4],
                      language,
                      textStore
                    }),
                    canPress: completedStep >= 3,
                    isCompleted: completedStep >= 4,
                    onPress: () => {
                      this.saveNaWithoutConfirm({
                        language,
                        callback: () => {
                          this.setState({
                            currentStep: 4
                          });
                        }
                      });
                    }
                  }
                ]}
                currentStepIndex={this.state.currentStep}
                currentStepOnPress={() => {}}
              />
            </View>
            <View style={styles.KeyboardAvoidingViewWrapper}>
              {renderForm(language)}
            </View>
            <View style={styles.footer}>
              {this.state.currentStep === 0 ? (
                <EABButton
                  testID="NeedsAnalysis__NS__btnBackToFE"
                  buttonType={RECTANGLE_BUTTON_2}
                  onPress={() => {
                    this.saveNaWithoutConfirm({
                      language,
                      callback: () => {
                        this.props.openPreviousDialog();
                      }
                    });
                  }}
                >
                  <Image style={Theme.iconStyle} source={icPrevious} />
                  <Text style={styles.footerPreviousButtonText}>
                    {EABi18n({
                      path: "na.footer.backToFE",
                      language,
                      textStore
                    })}
                  </Text>
                </EABButton>
              ) : (
                <EABButton
                  testID={`NeedsAnalysis__ST${
                    this.state.currentStep
                  }__btnPrevious`}
                  buttonType={RECTANGLE_BUTTON_2}
                  onPress={() => {
                    this.saveNaWithoutConfirm({
                      language,
                      callback: () => {
                        this.setState({
                          currentStep: this.state.currentStep - 1
                        });
                      }
                    });
                  }}
                >
                  <Image style={Theme.iconStyle} source={icPrevious} />
                  <Text style={styles.footerPreviousButtonText}>Previous</Text>
                </EABButton>
              )}

              {this.hvNextStep() ? (
                <EABButton
                  testID={`NeedsAnalysis__ST${this.state.currentStep}__btnNext`}
                  buttonType={RECTANGLE_BUTTON_1}
                  onPress={() => {
                    this.props.initialValidateNA({
                      naData: this.props.na,
                      profileData: this.props.profile,
                      dependantProfilesData: this.props.dependantProfiles,
                      feData: this.props.fe
                    });
                    this.saveNaWithoutConfirm({
                      language,
                      callback: () => {
                        this.setState({
                          currentStep: this.state.currentStep + 1
                        });
                      }
                    });
                  }}
                  isDisable={completedStep < this.state.currentStep}
                >
                  <Text style={styles.footerNextButtonText}>
                    {EABi18n({
                      path: "button.next",
                      language,
                      textStore
                    })}
                  </Text>
                  <Image style={Theme.iconStyle} source={icNext} />
                </EABButton>
              ) : (
                renderEndButton(language)
              )}
            </View>
          </LinearGradient>
        )}
      </ContextConsumer>
    );
  }
}

NeedsAnalysis.propTypes = {
  isChanged: PropTypes.bool.isRequired,
  isGloballyChanged: PropTypes.bool.isRequired,
  saveNA: PropTypes.func.isRequired,
  closeDialog: PropTypes.func.isRequired,
  openPreviousDialog: PropTypes.func.isRequired,
  openCKADialog: PropTypes.func.isRequired,
  openRADialog: PropTypes.func.isRequired,
  toProductPage: PropTypes.func.isRequired,
  profile: WEB_API_REDUCER_TYPE_CHECK[CLIENT].profile.isRequired,
  pda: WEB_API_REDUCER_TYPE_CHECK[FNA].pda.isRequired,
  na: WEB_API_REDUCER_TYPE_CHECK[FNA].na.isRequired,
  fe: WEB_API_REDUCER_TYPE_CHECK[FNA].fe.isRequired,
  completedStep: WEB_API_REDUCER_TYPE_CHECK[FNA].na.isRequired,
  textStore: REDUCER_TYPE_CHECK[TEXT_STORE].isRequired,
  initialValidateNA: PropTypes.func.isRequired,
  dependantProfiles:
    WEB_API_REDUCER_TYPE_CHECK[CLIENT].dependantProfiles.isRequired,
  updateNA: PropTypes.func.isRequired,
  signature: WEB_API_REDUCER_TYPE_CHECK[SIGNATURE].signature.isRequired,
  preApplication:
    WEB_API_REDUCER_TYPE_CHECK[PRE_APPLICATION].ApplicationItem.isRequired,
  updateNaIsGloballyChanged: PropTypes.func.isRequired,
  updateIsCompleted: PropTypes.func.isRequired
};
