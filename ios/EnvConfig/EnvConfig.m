#import "EnvConfig.h"

#import <React/RCTLog.h>

@implementation EnvConfig

RCT_EXPORT_MODULE()

RCT_EXPORT_METHOD(loadEnvConfig:(NSDictionary *) env)
{
  config = env;
}

+ (NSDictionary *)getConfig{
  return config;
}

@end
