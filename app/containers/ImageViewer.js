import { connect } from "react-redux";
import { REDUCER_TYPES, ACTION_TYPES } from "eab-web-api";
import ImageViewer from "../components/ImageViewer";

const { FNA } = REDUCER_TYPES;

/**
 * ImageViewer
 * @requires ImageViewer - ImageViewer UI
 * */
const mapStateToProps = () => ({});

const mapDispatchToProps = dispatch => ({
  FNAReportInitialize: callback => {
    dispatch({
      type: ACTION_TYPES[FNA].GET_FNA_REPORT,
      callback
    });
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ImageViewer);
