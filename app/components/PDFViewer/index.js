import React, { PureComponent } from "react";
import Pdf from "react-native-pdf";
import LinearGradient from "react-native-linear-gradient";
import EABHUD from "../../containers/EABHUD";
import styles from "./styles";
import { ContextConsumer } from "../../context";
import Theme from "../../theme";
/**
 * <PDFViewer />
 * @description handler the pdf layout
 * @param {string} title - the tag title
 * @param {string} textColor - title text color
 * @param {string} backgroundColor - tag background color
 * */
export default class PDFViewer extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <ContextConsumer>
        {({ isPDFLoading, hidePDFLoading, PDFSource }) => (
          <LinearGradient
            {...Theme.LinearGradientProps}
            style={styles.container}
          >
            <EABHUD isOpen={isPDFLoading} />
            <Pdf
              style={styles.pdf}
              source={{
                uri: PDFSource
              }}
              onLoadComplete={() => hidePDFLoading()}
            />
          </LinearGradient>
        )}
      </ContextConsumer>
    );
  }
}

PDFViewer.propTypes = {};
