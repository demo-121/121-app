import * as _ from "lodash";

import { utilities } from "eab-web-api";
import { trustedInd01TemplateOptionsMapping } from "./Constants";

import { getOptionValue } from "../../../utilities/getOptionList";
import { languageTypeConvert } from "../../../utilities/getEABTextByLangType";

const { dynamicApplicationForm } = utilities;

const _getValueByLookUpId = ({
  lookUpId,
  rootValues,
  path,
  language,
  optionsMap
}) => {
  if (_.includes(lookUpId, "@")) {
    return dynamicApplicationForm.getItemValueFunc({
      rootValues,
      valuePath: path,
      id: lookUpId
    });
  } else if (_.toUpper(lookUpId) === "TRUSTED_IND01") {
    const itemValue = _.get(
      rootValues,
      `${path}.${lookUpId}`,
      languageTypeConvert(language)
    );
    const optionValue = _.get(
      trustedInd01TemplateOptionsMapping,
      itemValue,
      languageTypeConvert(language)
    );
    return getOptionValue({
      optionMap: optionsMap.language,
      language,
      value: optionValue
    });
  }

  const newLookUpId = _.replace(lookUpId, "/", ".");
  const pathValue = _.get(rootValues, path, {});
  return _.get(pathValue, newLookUpId, "");
};

export const htmlTransform = ({ content }) => {
  if (_.isString(content) && content.indexOf("<br/>")) {
    const stringArr = _.split(content, "<br/>");
    return _.join(stringArr, "\n");
  }
  return content;
};

export const transformTrustedIndividualContent = ({
  content,
  rootValues,
  path,
  id,
  optionsMap,
  language
}) => {
  if (id !== "TRUSTED_IND04") {
    return content;
  }

  const arrSplitByStarting = _.split(content, "<b>");
  let newContent = content;

  _.each(arrSplitByStarting, itemString => {
    if (_.includes(itemString, "</b>")) {
      const index = itemString.indexOf("</b>");
      const stringToReplace = itemString.substring(0, index);
      const boldContent = stringToReplace.substring(1, index - 1);

      // Special handling for product Band Aid
      if (_.includes(boldContent, "> <")) {
        const lookUpIdArr = _.split(boldContent, "> <");
        const stringValueArr = _.map(lookUpIdArr, lookUpId =>
          _getValueByLookUpId({
            lookUpId,
            rootValues,
            path,
            language,
            optionsMap
          })
        );
        newContent = _.replace(
          newContent,
          stringToReplace,
          _.join(stringValueArr, " ")
        );
      } else {
        const stringValue = _getValueByLookUpId({
          lookUpId: boldContent,
          rootValues,
          path,
          language,
          optionsMap
        });
        newContent = _.replace(newContent, stringToReplace, stringValue);
      }
    }
  });

  return newContent;
};
