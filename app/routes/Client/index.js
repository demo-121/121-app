import React from "react";
import { Image, Text, View } from "react-native";
import {
  createBottomTabNavigator,
  NavigationActions,
  createAppContainer
} from "react-navigation";
import applicationsActive from "../../assets/images/applicationsActive.png";
import applicationsInactive from "../../assets/images/applicationsInactive.png";
import needsActive from "../../assets/images/needsActive.png";
import needsInactive from "../../assets/images/needsInactive.png";
import productsActive from "../../assets/images/productsActive.png";
import productsInactive from "../../assets/images/productsInactive.png";
import profileActive from "../../assets/images/profileActive.png";
import profileInactive from "../../assets/images/profileInactive.png";
import Theme from "../../theme";
import Applications from "../Applications";
import Needs from "../Needs";
import PreProducts from "../Products/containers/PreProducts";
import Profile from "../Profile";
import FnaWarningIcon from "../../containers/FnaWarningIcon";
import NavigationService from "../../utilities/NavigationService";

const ClientNavigator = createBottomTabNavigator(
  {
    Profile: {
      screen: Profile,
      navigationOptions: {
        tabBarLabel: tabParams => {
          const { focused } = tabParams;

          return (
            <View
              style={{
                marginLeft: "30%",
                flexDirection: "row",
                justifyContent: "center",
                alignItems: "center",
                height: "100%"
              }}
            >
              <Image
                style={{
                  marginRight: 10,
                  width: 28,
                  height: 28
                }}
                source={focused ? profileActive : profileInactive}
              />
              <Text
                style={
                  focused
                    ? Theme.tabBarLabelSelected
                    : Theme.tabBarLabelUnselected
                }
              >
                Profile
              </Text>
            </View>
          );
        }
      }
    },
    Needs: {
      screen: Needs,
      navigationOptions: {
        tabBarLabel: tabParams => {
          const { focused } = tabParams;

          return (
            <View
              style={{
                marginLeft: "15%",
                flexDirection: "row",
                alignItems: "center",
                justifyContent: "center",
                height: "100%"
              }}
            >
              <Image
                style={{
                  marginRight: 10,
                  width: 28,
                  height: 28
                }}
                source={focused ? needsActive : needsInactive}
              />
              <Text
                style={
                  focused
                    ? Theme.tabBarLabelSelected
                    : Theme.tabBarLabelUnselected
                }
              >
                Needs
                <FnaWarningIcon />
              </Text>
            </View>
          );
        }
      }
    },
    Products: {
      screen: PreProducts,
      navigationOptions: {
        tabBarLabel: tabParams => {
          const { focused } = tabParams;

          return (
            <View
              style={{
                marginRight: "15%",
                flexDirection: "row",
                alignItems: "center",
                justifyContent: "center",
                height: "100%"
              }}
            >
              <Image
                style={{
                  marginRight: 10,
                  width: 28,
                  height: 28
                }}
                source={focused ? productsActive : productsInactive}
              />
              <Text
                style={
                  focused
                    ? Theme.tabBarLabelSelected
                    : Theme.tabBarLabelUnselected
                }
              >
                Products
              </Text>
            </View>
          );
        },
        tabBarOnPress: ({ navigation }) => {
          const toProductPage = navigation.getParam("toProductPage", null);
          if (toProductPage) {
            toProductPage();
          }
          navigation.dispatch(
            NavigationActions.navigate({
              routeName: "Products"
            })
          );
        }
      }
    },
    Applications: {
      screen: Applications,
      navigationOptions: {
        tabBarLabel: tabParams => {
          const { focused } = tabParams;

          return (
            <View
              style={{
                marginRight: "30%",
                flexDirection: "row",
                alignItems: "center",
                justifyContent: "center",
                height: "100%"
              }}
            >
              <Image
                style={{
                  marginRight: 10,
                  width: 28,
                  height: 28
                }}
                source={focused ? applicationsActive : applicationsInactive}
              />
              <Text
                style={
                  focused
                    ? Theme.tabBarLabelSelected
                    : Theme.tabBarLabelUnselected
                }
              >
                Applications
              </Text>
            </View>
          );
        },
        tabBarOnPress: ({ navigation }) => {
          navigation.dispatch(
            NavigationActions.navigate({
              routeName: "Summary",
              params: {
                selectModeOn: false
              }
            })
          );
        }
      }
    }
  },
  {
    initialRouteName: "Needs"
  }
);

const Clients = createAppContainer(ClientNavigator);
export default class Client extends React.Component {
  render() {
    return (
      <Clients
        ref={navigatorRef => {
          NavigationService.setNavigator({
            topLevelNavigator: "Clients",
            ref: navigatorRef
          });
        }}
      />
    );
  }
}
