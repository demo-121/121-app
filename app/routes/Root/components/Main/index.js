import PropTypes from "prop-types";
import React, { Component } from "react";
import { View, NetInfo } from "react-native";
import Config from "react-native-config";
import Playground from "../../../../containers/Playground";
import { ContextConsumer } from "../../../../context";
import Client from "../../../Client";
import Landing from "../../../Landing";
import styles from "./styles";
import AppActivation from "../../components/AppActivation";
import AppPassword from "../../containers/AppPassword";
import AppPasswordLogin from "../../containers/AppPasswordLogin";
import OnlineAuth from "../../containers/OnlineAuth";
import OnlineAuthBackPage from "../../containers/OnlineAuthBackPage";
import Environment from "../../components/Environment";
import EABHUD from "../../../../containers/EABHUD";

import {
  APP_ACTIVATION,
  LOCAL_AUTH,
  ONLINE_AUTH
} from "../../../../constants/LOGIN_FLOW_TYPES";
/**
 * Main
 * @description main page container.
 * @reducer login
 * @requires Client - Client navigator
 * @requires Landing - Landing navigator
 * @param {object} navigation - react navigation props
 * @param {boolean} isLogin - login reducer state
 * @param {boolean} isLogout - login reducer state
 * @param {string} environment - login reducer environment
 * @param {string} paymentMethod - payment Method - To be removed later
 * */
export default class Main extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isOpeningPlayground: false,
      status: false
    };
    this.handleConnectionChange = this.handleConnectionChange.bind(this);
  }
  componentDidMount() {
    this.props.checkLoginStatus();
    NetInfo.isConnected.addEventListener(
      "connectionChange",
      this.handleConnectionChange
    );

    NetInfo.isConnected.fetch().done(isConnected => {
      this.setState({
        status: isConnected
      });
    });
  }

  componentWillUnmount() {
    NetInfo.isConnected.removeEventListener(
      "connectionChange",
      this.handleConnectionChange
    );
  }

  onlineAuthLoginFlow() {
    // This case occurs when fail login attempts > 3 and onlineAuthLoginFlow is required
    if (this.props.showNewAppPasswordScene) {
      if (this.props.showResetAppPasswordScene) {
        return (
          <View style={styles.appPasswordWrap}>
            <AppPassword text={this.props.text} />
            <EABHUD isOpen={this.props.isLoading} />
          </View>
        );
      }
      return (
        <View style={styles.appPasswordWrap}>
          <OnlineAuthBackPage
            navigateToOnlineLoginPage={this.props.navigateToOnlineLoginPage}
          />
          <EABHUD isOpen={this.props.isLoading} />
        </View>
      );
    }
    return (
      <View style={styles.onlineAuthWrap}>
        <OnlineAuth />
        <EABHUD isOpen={this.props.isLoading} />
      </View>
    );
  }
  handleConnectionChange(isConnected) {
    this.setState({ status: isConnected });
  }

  appActivationLoginFlow() {
    if (this.props.showNewAppPasswordScene) {
      return (
        <View style={styles.appPasswordWrap}>
          <AppPassword text={this.props.text} />
          <EABHUD isOpen={this.props.isLoading} />
        </View>
      );
    }
    return (
      <AppActivation
        isAppLocked={this.props.isAppLocked}
        checkAppLocked={this.props.checkAppLocked}
        environment={this.props.environment}
        setEnvironment={this.props.setEnvironment}
        onlineAuthCode={this.props.onlineAuthCode}
        onlineAuthCode2={this.props.onlineAuthCode2}
        isHandlingListener={this.props.isHandlingListener}
        LoginFailByOnlineAuth={this.props.LoginFailByOnlineAuth}
        disableAppActivationPage={this.props.disableAppActivationPage}
      />
    );
  }
  localAuthLoginFlow() {
    return (
      <View style={styles.appPasswordLoginWrap}>
        <AppPasswordLogin text={this.props.text} />
        <EABHUD isOpen={this.props.isLoading} />
      </View>
    );
  }
  environmentSetting() {
    return (
      <View style={styles.appPasswordLoginWrap}>
        <Environment
          isAppLocked={this.isAppLocked}
          checkAppLocked={this.props.checkAppLocked}
          setEnvironment={this.props.setEnvironment}
          setAppLocked={this.props.setAppLocked}
        />
        <EABHUD isOpen={this.props.isLoading} />
      </View>
    );
  }
  appPasswordExpired() {
    if (this.props.isAppPasswordExpired) {
      return <OnlineAuth />;
    }
    return (
      <View style={styles.appPasswordWrap}>
        <AppPassword text={this.props.text} />
        <EABHUD isOpen={this.props.isLoading} />
      </View>
    );
  }
  resetAppPasswordManually() {
    return (
      <View style={styles.appPasswordWrap}>
        <AppPassword text={this.props.text} />
        <EABHUD isOpen={this.props.isLoading} />
      </View>
    );
  }

  render() {
    let renderResult = null;
    if (this.props.isLogin) {
      if (this.props.showResetAppPasswordManuallyScene) {
        renderResult = this.resetAppPasswordManually();
      } else {
        renderResult = (
          <ContextConsumer>
            {({ isShowingClientNavigator, isBottomTabNavigatorLoading }) => (
              <View style={styles.container}>
                <EABHUD isOpen={isBottomTabNavigatorLoading} />
                {isShowingClientNavigator ? <Client /> : <Landing />}
              </View>
            )}
          </ContextConsumer>
        );
      }
    } else if (this.state.isOpeningPlayground) {
      renderResult = <Playground />;
    } else if (this.props.isAppPasswordExpired) {
      renderResult = this.appPasswordExpired();
    } else if (this.props.isExceedFailLoginAttemps) {
      renderResult = this.onlineAuthLoginFlow();
    } else if (this.props.loginFlow === APP_ACTIVATION) {
      if (this.state.status) {
        if (this.props.isAppLocked) {
          renderResult = this.appActivationLoginFlow();
        } else if (this.props.environment === "DEV") {
          renderResult = this.localAuthLoginFlow();
        } else if (
          this.props.environment === "DEV" ||
          this.props.environment === "SIT1_EAB" ||
          this.props.environment === "SIT2_EAB" ||
          this.props.environment === "SIT3_EAB" ||
          this.props.environment === "SIT_AXA" ||
          this.props.environment === "UAT" ||
          this.props.environment === "PRE_PROD"
        ) {
          if (this.props.isAppActivationDone) {
            renderResult = this.onlineAuthLoginFlow();
          } else {
            renderResult = this.appActivationLoginFlow();
          }
        } else if (Config.ENV_NAME === "PROD") {
          // this.props.setEnvironment("PROD");
          renderResult = this.appActivationLoginFlow();
        } else {
          renderResult = this.environmentSetting();
        }
      } else if (this.props.isAppLocked) {
        renderResult = this.appActivationLoginFlow();
      } else {
        renderResult = this.environmentSetting();
      }
    } else if (
      this.props.loginFlow === LOCAL_AUTH ||
      this.props.loginFlow === ONLINE_AUTH
    ) {
      if (this.props.changeToOnlineAuth) {
        renderResult = this.onlineAuthLoginFlow();
      } else if (this.state.status) {
        if (this.props.environment === "DEV") {
          renderResult = this.localAuthLoginFlow();
        } else if (
          this.props.environment === "SIT1_EAB" ||
          this.props.environment === "SIT2_EAB" ||
          this.props.environment === "SIT3_EAB"
        ) {
          renderResult = this.onlineAuthLoginFlow();
          // renderResult = this.localAuthLoginFlow();
        } else if (this.props.environment === "SIT_AXA") {
          renderResult = this.onlineAuthLoginFlow();
          // renderResult = this.localAuthLoginFlow();
        } else if (this.props.environment === "UAT") {
          renderResult = this.onlineAuthLoginFlow();
        } else if (this.props.environment === "PRE_PROD") {
          renderResult = this.onlineAuthLoginFlow();
        } else {
          renderResult = this.onlineAuthLoginFlow();
        }
      } else {
        renderResult = this.localAuthLoginFlow();
      }
    }
    return <View style={styles.container}>{renderResult}</View>;
  }
}

Main.propTypes = {
  checkAppLocked: PropTypes.func.isRequired,
  setAppLocked: PropTypes.func.isRequired,
  navigateToOnlineLoginPage: PropTypes.func,
  isAppLocked: PropTypes.bool,
  disableAppActivationPage: PropTypes.bool,
  loginFlow: PropTypes.string,
  changeToOnlineAuth: PropTypes.bool.isRequired,
  isLogin: PropTypes.bool.isRequired,
  isAppActivationDone: PropTypes.bool,
  isExceedFailLoginAttemps: PropTypes.bool.isRequired,
  environment: PropTypes.string.isRequired,
  isLoading: PropTypes.bool.isRequired,
  onlineAuthCode: PropTypes.func.isRequired,
  onlineAuthCode2: PropTypes.func.isRequired,
  isHandlingListener: PropTypes.bool.isRequired,
  LoginFailByOnlineAuth: PropTypes.func.isRequired,
  showNewAppPasswordScene: PropTypes.bool,
  showResetAppPasswordScene: PropTypes.bool.isRequired,
  showResetAppPasswordManuallyScene: PropTypes.bool.isRequired,
  isAppPasswordExpired: PropTypes.bool.isRequired,
  setEnvironment: PropTypes.func.isRequired,
  checkLoginStatus: PropTypes.func.isRequired,
  text: PropTypes.shape({
    passwordDoNotMeetPasswordPolicy: PropTypes.string.isRequired
  }).isRequired
};

Main.defaultProps = {
  isAppActivationDone: false,
  isAppLocked: false,
  disableAppActivationPage: false,
  showNewAppPasswordScene: false,
  loginFlow: "",
  navigateToOnlineLoginPage: ""
};
