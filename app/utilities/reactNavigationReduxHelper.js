import {
  createReactNavigationReduxMiddleware,
  createReduxBoundAddListener
} from "react-navigation-redux-helpers";
import { NAVIGATION } from "../constants/REDUCER_TYPES";

/**
 * navigationMiddleware
 * @description react navigation middle ware initial function. check this link
 *   to get more information
 *   {@see https://reactnavigation.org/docs/redux-integration.html}
 * @param {string} routerName - root router name
 * */
const navigationMiddleware = routerName =>
  createReactNavigationReduxMiddleware(
    routerName,
    state => state[NAVIGATION][routerName]
  );
/**
 * addListener
 * @description react navigation redux helper. check this link to get more
 *   information {@see https://reactnavigation.org/docs/redux-integration.html}
 * @param {string} routerName - helped router name
 * */
const addListener = routerName => createReduxBoundAddListener(routerName);

export { navigationMiddleware, addListener };
