// TODO: Config and axios Will be used
import axios from "axios";
import Config from "react-native-config";
// import { encryption, decryption } from "../apis/login";
/**
 ** submitApplication
 * @description call 121Online API to keep MasterKey as backup in OnlineDB
 * @param {string} authToken - authToken from AXA api provide
 * @param {string} docId - application id
 * @param {string} application -  application object
 * */
export const submitApplication = (authToken, docId, environment, lang) => {
  let submitResponse = {};
  let url = "";
  let baseURL = "";
  if (Config.ENV_NAME === "PROD") {
    url = Config.SUBMISSION_APPLICATION_URL;
    baseURL = Config.WEB_SERVICE_BASIC_URL;
  } else {
    url = Config[`${environment || "SIT3_EAB"}_SUBMISSION_APPLICATION_URL`];
    baseURL = Config[`${environment || "SIT3_EAB"}_WEB_SERVICE_BASIC_URL`];
  }

  return new Promise((resolve, reject) => {
    if (authToken && authToken.length > 0 && docId && docId.length > 0) {
      const AuthStr = `Bearer ${authToken}`;
      const options = {
        method: "post",
        headers: { Authorization: AuthStr },
        baseURL,
        url,
        data: {
          docId,
          lang
        }
      };
      // --> CR 197, disable encryption temporarily and then roll back
      // let aes256Key = "";
      // const encryptResult = encryption(options.data, authToken, environment);
      // encryptResult
      //   .then(callback => {
      //     if (callback.hasError) {
      //       submitResponse = {
      //         hasError: true,
      //         errorMsg: callback.errorMsg
      //           ? callback.errorMsg
      //           : "Cannot get Public Key"
      //       };
      //       reject(submitResponse);
      //     } else {
      //       aes256Key = callback.result.aesKey;
      //       options.data.d0 = callback.result.d0;
      //       options.data.dk = callback.result.dk;
      //     }
      //   })
      //   .then(() => {
      //     axios
      //       .request(options)
      //       .then(response => {
      //         const decryptionResult = decryption(response.data, aes256Key);
      //         decryptionResult.then(decryptedResponseBody => {
      //           response.data = JSON.parse(decryptedResponseBody);
      //           if (response.status === 200) {
      //             const responseResult = { ...response.data };
      //             if (responseResult.status === "success") {
      //               submitResponse = {
      //                 hasError: false,
      //                 result: response
      //               };
      //             } else {
      //               submitResponse = {
      //                 hasError: true,
      //                 errorMsg: "Fail to submit application"
      //               };
      //             }
      //           } else {
      //             submitResponse = {
      //               hasError: true,
      //               errorMsg: "Fail to submit application"
      //             };
      //           }
      //           resolve(submitResponse);
      //         });
      //       })
      //       .catch(error => {
      //         submitResponse = {
      //           hasError: true,
      //           errorMsg: error
      //         };
      //         resolve(submitResponse);
      //       });
      //   });

      axios
        .request(options)
        .then(response => {
          if (response.status === 200) {
            const responseResult = { ...response.data };
            if (responseResult.status === "success") {
              submitResponse = {
                hasError: false,
                result: response
              };
            } else {
              submitResponse = {
                hasError: true,
                errorMsg: "Fail to submit application"
              };
            }
          } else {
            submitResponse = {
              hasError: true,
              errorMsg: "Fail to submit application"
            };
          }
          resolve(submitResponse);
        })
        .catch(error => {
          submitResponse = {
            hasError: true,
            errorMsg: error
          };
          resolve(submitResponse);
        });
    } else {
      submitResponse = {
        hasError: true,
        errorMsg: "No authToken / docId / applicaton"
      };
      reject(submitResponse);
    }
  }).catch(error => {
    submitResponse = {
      hasError: true,
      errorMsg: error
    };
    return submitResponse;
  });
};

export default submitApplication;
