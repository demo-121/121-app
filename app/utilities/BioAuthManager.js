import { NativeModules } from "react-native";

const NativeBioAuth = NativeModules.BioAuth;
const ERRORS = {
  LAErrorAuthenticationFailed: {
    message:
      "Authentication was not successful because the user failed to provide valid credentials."
  },
  LAErrorUserCancel: {
    message:
      "Authentication was canceled by the user—for example, the user tapped Cancel in the dialog."
  },
  LAErrorUserFallback: {
    message:
      "Authentication was canceled because the user tapped the fallback button (Enter Password)."
  },
  LAErrorSystemCancel: {
    message:
      "Authentication was canceled by system—for example, if another application came to foreground while the authentication dialog was up."
  },
  LAErrorPasscodeNotSet: {
    message:
      "Authentication could not start because the passcode is not set on the device."
  },
  LAErrorTouchIDNotAvailable: {
    message:
      "Authentication could not start because Touch ID is not available on the device"
  },
  LAErrorTouchIDNotEnrolled: {
    message:
      "Authentication could not start because Touch ID has no enrolled fingers."
  },
  RCTTouchIDUnknownError: {
    message: "Could not authenticate for an unknown reason."
  },
  RCTTouchIDNotSupported: {
    message: "Device does not support Touch ID."
  }
};

function TouchIDError(name, details) {
  this.name = name || "TouchIDError";
  this.message = details.message || "Touch ID Error";
  this.details = details || {};
}

TouchIDError.prototype = Object.create(Error.prototype);
TouchIDError.prototype.constructor = TouchIDError;

const createError = error => {
  const details = ERRORS[error];
  details.name = error;

  return new TouchIDError(error, details);
};

/**
 * check if the device is support TouchID / FaceID or not
 * @returns {Promise} success if it is support TouchID / FaceID.
 */
export const isSupported = () =>
  new Promise((resolve, reject) => {
    NativeBioAuth.isSupported((error, biometryType) => {
      if (error) {
        return reject(createError(error.message));
      }
      return resolve(biometryType);
    });
  });

/**
 * prompt a dialog for waiting the TouchID / FaceID authentication
 * @param reason - The app-provided reason for requesting authentication, which displays in the
 * authentication dialog presented to the user.
 * @returns {Promise} success if it is authenticated by TouchID / FaceID successfully.
 */
export const authenticate = reason => {
  let authReason;

  // Set auth reason
  if (reason) {
    authReason = reason;
    // Set as empty string if no reason is passed
  } else {
    authReason = " ";
  }

  return new Promise((resolve, reject) => {
    NativeBioAuth.authenticate(authReason, error => {
      // Return error if rejected
      if (error) {
        return reject(createError(error.message));
      }
      return resolve(true);
    });
  });
};

export default {
  isSupported,
  authenticate
};
