import { connect } from "react-redux";
import { ACTION_TYPES, REDUCER_TYPES } from "eab-web-api";
import { TEXT_STORE, POLICYNUMBER } from "../../../constants/REDUCER_TYPES";
import ApplicationForm from "../components/ApplicationForm";
import APP_ACTION_TYPES from "../../../constants/ACTION_TYPES";

const { APPLICATION } = REDUCER_TYPES;

/**
 * ApplicationForm
 * @description ApplicationForm in Application page.
 * @requires ApplicationForm - ApplicationForm UI
 * */
const mapStateToProps = state => ({
  application: state[REDUCER_TYPES.APPLICATION].application,
  textStore: state[TEXT_STORE],
  template: state[APPLICATION].template,
  value: state[APPLICATION].application,
  selectedSectionKey: state[APPLICATION].component.selectedSectionKey,
  error: state[REDUCER_TYPES.APPLICATION].error
});

const mapDispatchToProps = dispatch => ({
  updateUiSelectedCid: cid => {
    dispatch({
      type: ACTION_TYPES[APPLICATION].UPDATE_UI_SELECTED_CID,
      cid
    });
  },
  updateUiSelectedSectionKey: newSelectedSectionKey => {
    dispatch({
      type: ACTION_TYPES[APPLICATION].SAGA_UPDATE_SELECTED_SECTION_KEY,
      newSelectedSectionKey
    });
  },
  saveForm: ({ genPDF, actionType, callback }) => {
    dispatch({
      type:
        ACTION_TYPES[REDUCER_TYPES.APPLICATION].SAVE_APPLICATION_FORM_BEFORE,
      actionType,
      genPDF,
      callback
    });
  },
  saveFormShield: ({ actionType, callback }) => {
    dispatch({
      type:
        ACTION_TYPES[REDUCER_TYPES.APPLICATION]
          .SAVE_APPLICATION_FORM_SHIELD_BEFORE,
      currentStep: 0,
      actionType,
      callback
    });
  },
  genPolicyNumber: ({ isShield, numOfShield, numOfNonShield, callback }) => {
    dispatch({
      type: APP_ACTION_TYPES[POLICYNUMBER].APPLY_POLICY_NUMBER,
      isShield,
      numOfShield,
      numOfNonShield,
      callback
    });
  },
  updatePolicyNumber: ({ policyNumber, isShield, callback }) => {
    dispatch({
      type: ACTION_TYPES[REDUCER_TYPES.APPLICATION].SAGA_UPDATE_POLICY_NUMBER,
      policyNumber,
      isShield,
      callback
    });
  },
  checkAndRefillPolicyNumber: (callback = () => {}) => {
    dispatch({
      type: APP_ACTION_TYPES[POLICYNUMBER].CHECK_POLICY_NUMBER,
      callback
    });
  },
  submitPolicyNumber: ({ isShield, policyNumber, callback }) => {
    dispatch({
      type: APP_ACTION_TYPES[POLICYNUMBER].SUBMIT_POLICY_NUMBER,
      isShield,
      shieldPNIntput: isShield ? policyNumber : [],
      nonShieldPNIntput: isShield ? [] : policyNumber,
      callback
    });
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ApplicationForm);
