import { NativeModules } from "react-native";
import * as _ from "lodash";
import { getDocumentWithID, getAttachmentWithID } from "./DataManager";

const contentTypeToFileExtension = {
  "application/pdf": "pdf",
  "image/jpeg": "jpg",
  "image/png": "png"
};

export const exportAttachmentFromCB = ({ docId, attachmentId, fileName }) =>
  new Promise(resolve => {
    getDocumentWithID(docId, resolve);
  })
    .then(
      doc =>
        new Promise(resolve => {
          const contentType = _.get(
            doc,
            `_attachments.${attachmentId}.content_type`,
            ""
          );
          getAttachmentWithID(docId, attachmentId, res => {
            resolve({ contentType, data: res.data });
          });
        })
    )
    .then(({ contentType, data }) => {
      _.each(contentTypeToFileExtension, extention => {
        const sliceLength = (extention.length + 1) * -1;
        if (fileName.slice(sliceLength) === `.${extention}`) {
          fileName = fileName.slice(0, sliceLength);
          return false;
        }
        return true;
      });
      const fileExtension = contentTypeToFileExtension[contentType];
      return NativeModules.RNDocumentPickerViewManager.export({
        data,
        fileExtension,
        fileName
      });
    });

export const importAttachmentToCB = options => {
  NativeModules.RNDocumentPickerViewManager.import(options);
};
