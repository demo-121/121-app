<?xml version="1.0" encoding="UTF-8"?><xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"><xsl:output method="html"></xsl:output><xsl:template match="/"><div class="pageContainer"><div class="pageMargin"><div class="header"><div class="global_intro">
  <div style="flex-grow: 1;"></div>
  <div class="sav_intro-logo"></div>
  <p style="text-align: left;"></p>
  <div style="text-align: center; width: 100%; padding: 20 0;"><strong>MAIN POLICY ILLUSTRATION </strong></div>
</div></div><div><div style="font-size: 10pt;">
  <div class="sav_intro-barcode" style="float: right; position: absolute; top: 2; right: 0;"> </div>
  <table class="detailTable" style="border-bottom: solid 1px;">
    <tbody>
      <tr style="height: 13px;">
        <td class="agentTableTitle1">Adviser Name</td>
        <td class="agentTableValue">:
          <xsl:value-of select="*/agent/name" /> </td>
        <td class="agentTableTitle2">Date</td>
        <td>:
          <xsl:value-of select="*/genDate" /> </td> </tr>
      <tr style="height: 13px;">
        <td style="height: 13px;">Adviser Company</td>
        <td colspan="3" style="height: 13px;">:
          <xsl:value-of select="*/agent/company" /> </td>  </tr>
      <tr style="height: 13px;">
        <td style="height: 13px;">Adviser Tel</td>
        <td colspan="3" style="height: 13px;">:
          <xsl:value-of select="*/agent/tel" /> </td> </tr>
      <tr style="height: 13px;">
        <td style="height: 13px;">Adviser HP</td>
        <td colspan="3" style="height: 13px;">:
          <xsl:value-of select="*/agent/mobile" /> </td> </tr>
      <tr style="height: 13px;">
        <td style="height: 13px;">Adviser Email</td>
        <td colspan="3" style="height: 13px;">:
          <xsl:value-of select="*/agent/email" /> </td>  </tr>
      <tr style="height: 13px;">
        <td style="height: 13px;"> </td>
        <td colspan="3" style="height: 13px;"> </td>  </tr>  </tbody></table>
  <br/>
  <xsl:if test="*/mainInfo/sameAs='N'">
    <div class=""><strong>Proposer Details:</strong></div>
    <table class="detailTable">
      <tbody>
        <tr style="height: 13px;">
          <td class="detailTableTitle">Name</td>
          <td class="detailTableValue">:
            <xsl:value-of select="*/mainInfo/pName" /> </td>
          <td class="detailTableTitle2">Gender</td>
          <td class="detailTableValue">:
            <xsl:value-of select="*/proposerGenderTitle" /> </td> </tr>
        <tr style="height: 13px;">
          <td style="height: 13px;">Age</td>
          <td style="height: 13px;">:
            <xsl:value-of select="*/mainInfo/pAge" /> (as at
            <xsl:value-of select="*/backDate" />) </td>
          <td style="height: 13px;">Smoking Status</td>
          <td style="height: 13px;">:
            <xsl:value-of select="*/proposerSmokerTitle" /> </td> </tr>
        <tr style="height: 13px;">
          <td style="height: 13px;">Date of Birth</td>
          <td style="height: 13px;">:
            <xsl:value-of select="*/proposerDob" /> </td>
          <td style="height: 13px;"> </td>
          <td style="height: 13px;"> </td> </tr>  </tbody></table>
    <br/>
    <br/> </xsl:if>
  <div class=""><strong>Life Assured Details:</strong></div>
  <table class="detailTable">
    <tbody>
      <tr style="height: 13px;">
        <td class="detailTableTitle" style="height: 13px;">Name</td>
        <td class="detailTableValue" style="height: 13px;">:
          <xsl:value-of select="*/mainInfo/name" /> </td>
        <td class="detailTableTitle2" style="height: 13px;">Gender</td>
        <td class="detailTableValue" style="height: 13px;">:
          <xsl:value-of select="*/insurerGenderTitle" /> </td> </tr>
      <tr style="height: 13px;">
        <td class="detailTableTitle" style="height: 13px;">Age</td>
        <td class="detailTableValue" style="height: 13px;">:
          <xsl:value-of select="*/mainInfo/age" /> (as at
          <xsl:value-of select="*/backDate" />)</td>
        <td class="detailTableTitle2" style="height: 13px;">Smoking Status</td>
        <td class="detailTableValue" style="height: 13px;">:
          <xsl:value-of select="*/insurerSmokerTitle" /> </td> </tr>
      <tr style="height: 13.6216px;">
        <td class="detailTableTitle" style="height: 13.6216px;">Date of Birth</td>
        <td class="detailTableValue" style="height: 13px;">:
          <xsl:value-of select="*/insurerDob" /> </td>
        <td style="height: 13px;"> </td>
        <td style="height: 13.6216px;"> </td> </tr>  </tbody></table>
  <br/>
  <br/>
  <table class="detailTable">
    <tbody>
      <tr style="height: 13px;">
        <td class="detailTableTitle">Payment Mode</td>
        <td class="detailTableValue">:
          <xsl:value-of select="*/paymentModeTitle" /> </td>
        <td class="detailTableTitle"> </td>
        <td class="detailTableValue"> </td> </tr>
      <tr style="height: 13px;">
        <td style="height: 13px;">Policy Currency</td>
        <td style="height: 13px;">:
          <xsl:value-of select="*/polCcy" /> </td>
        <td style="height: 13px;"> </td>
        <td style="height: 13px;"> </td> </tr> </tbody></table>
  <br />
  <br />
  <div class=""><strong> Your Plan</strong></div>
  <table style="table-layout: fixed;    width: 100%;    font-size: 10pt; text-align:right">
    <tbody>
      <tr style="height: 13px; vertical-align:top">
        <td class="planName" style="text-align:left">
          <p><strong>Plan Name </strong></p> </td>
        <td class="planNumberPlan">
          <p><strong>Policy Term</strong></p> </td>
        <td class="planNumberPlan">
          <p><strong>Premium Term</strong></p>
          <p> </p> </td>
        <td>
          <div><strong>Sum Assured/*<br/> Benefit</strong></div>  </td> </tr>
      <xsl:for-each select="*/planFields">
        <xsl:if test="planInd='B'">
          <tr style="height: 13px;">
            <td style="text-align:left">
              <xsl:value-of select="planName" /> </td>
            <td>
              <xsl:value-of select="policyTerm" /> </td>
            <td>
              <xsl:value-of select="permTerm" /> </td>
            <td>
              <xsl:value-of select="sumAssured" /> </td> </tr> </xsl:if> </xsl:for-each>
      <xsl:for-each select="*/planFields">
        <xsl:if test="planInd='R'">
          <tr style="height: 13px;">
            <td style="text-align:left">
              <xsl:value-of select="planName" /> </td>
            <td>
              <xsl:value-of select="policyTerm" /> </td>
            <td>
              <xsl:value-of select="permTerm" /> </td>
            <td> - </td> </tr> </xsl:if> </xsl:for-each> </tbody></table>
  <br />
  <br />
  <table style=" text-align:right; table-layout: fixed;    width: 100%;    font-size: 10pt;">
    <tbody>
      <tr style="height:13px; vertical-align:top">
        <td class="planName" style=" text-align:left; ">
          <div><strong>Your Premium</strong></div> </td>
        <td class="planNumberPremium">
          <div><strong>Annual<br/>Premium</strong></div>  </td>
        <td class="planNumberPremium">
          <div><strong>Semi-Annual<br/>Premium</strong></div>  </td>
        <td class="planNumberPremium">
          <div><strong>Quarterly<br/>Premium</strong></div>  </td>
        <td class="planNumberPremium">
          <div><strong>Monthly<br/>Premium</strong></div> </td> </tr>
      <tr style="height:13px; vertical-align:top">
        <td class="planName" style=" text-align:left; ">
          <div><strong>Plan Name</strong></div> </td>
        <td class="planNumberPremium"> </td>
        <td class="planNumberPremium"> </td>
        <td class="planNumberPremium"> </td>
        <td class="planNumberPremium"> </td> </tr>
      <xsl:for-each select="*/planInfos">
        <xsl:if test="planInd='B'">
          <tr style="height:13px">
            <td style=" text-align:left; ">
              <xsl:value-of select="planName" /> </td>
            <td style="">
              <xsl:value-of select="APremium" /> </td>
            <td style="">
              <xsl:value-of select="HPremium" /> </td>
            <td style="">
              <xsl:value-of select="QPremium" /> </td>
            <td style="">
              <xsl:value-of select="MPremium" /> </td> </tr> </xsl:if> </xsl:for-each>
      <xsl:for-each select="*/planInfos">
        <xsl:if test="planInd='R'">
          <tr style="height:13px">
            <td style=" text-align:left; ">
              <xsl:value-of select="planName" /> </td>
            <td style="">
              <xsl:value-of select="APremium" /> </td>
            <td style="">
              <xsl:value-of select="HPremium" /> </td>
            <td style="">
              <xsl:value-of select="QPremium" /> </td>
            <td style="">
              <xsl:value-of select="MPremium" /> </td> </tr> </xsl:if> </xsl:for-each>
      <tr style="height:13px">
        <td style="text-align:left;width:200px;">
          <p><strong>Total Premium </strong></p> </td>
        <td>
          <xsl:value-of select="*/mainInfo/yearlyPremium" /> </td>
        <td>
          <xsl:value-of select="*/mainInfo/halfYearlyPremium" /> </td>
        <td>
          <xsl:value-of select="*/mainInfo/quarterlyPremium" /> </td>
        <td>
          <xsl:value-of select="*/mainInfo/monthlyPremium" /> </td> </tr> </tbody></table>
  <br/>
  <div>*The Sum Assured is a notional value in the Benefit Illustration used for the calculation of guaranteed maturity value, Guaranteed Cash Payout, reversionary and terminal bonuses and is not the amount payable on the insured event. </div>
  <xsl:if test="*/mainInfo/quickQuote='Y'">
    <br/>
    <div><b>Declaration</b></div>
    <div>I / We confirm that I / We have received all pages of the Benefit Illustration for the basic plan / all riders listed above, where applicable.</div></xsl:if>
  <p> </p>
  <p> </p>
  <p> </p>
  <p> </p></div></div><div class="footer"><div class="global_intro"><xsl:if test="*/mainInfo/quickQuote='Y'">
    <table style="text-align: left; table-layout: fixed; width: 100%; font-size: 10pt; margin: 0 auto;">
      <tbody>
        <tr style="color: white;">
          <td style="padding-left: 50px;">
            <div style="border-bottom: solid 1px black; width: 80%;">1</div>
          </td>
          <td style="padding-left: 50px;">
            <div style="border-bottom: solid 1px black; width: 80%;">2</div>
          </td>
        </tr>
        <tr>
          <td style="padding-left: 50px; padding-top: 10px;">Adviser's Signature</td>
          <td style="padding-left: 50px; padding-top: 10px;">Proposer's Signature</td>
        </tr>
        <tr>
          <td style="padding-left: 50px;">Date:</td>
          <td style="padding-left: 50px;">Date:</td>
        </tr>
      </tbody>
    </table>
    <br/><br/></xsl:if>
  <table style="width: 100%; font-size: 10pt;">
    <tbody>
      <tr>
        <td style="width: 49.8018%;"><strong>Page 2 of 14</strong></td>
        <td style="text-align: right; width: 46.1982%;">Release Version <xsl:value-of select="*/releaseVersion"/></td>
      </tr>
    </tbody>
  </table>
  <p></p>
  <div><xsl:value-of select="*/planCodes"/></div>
  <div style="text-align: center;"><xsl:value-of select="*/compName"/> (Co. Reg. No. <xsl:value-of select="*/compRegNo"/>)</div>
  <div style="text-align: center;"><xsl:value-of select="*/compAddr"/></div>
  <div style="text-align: center;"><xsl:value-of select="*/compAddr2"/> TEL: <xsl:value-of select="*/compTel"/> FAX: <xsl:value-of select="*/compFax"/></div>
  <div style="text-align: center;"><xsl:value-of select="*/compWeb"/></div>
  </div></div></div></div></xsl:template></xsl:stylesheet>
