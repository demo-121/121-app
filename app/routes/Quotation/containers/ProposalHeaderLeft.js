import { ACTION_TYPES, REDUCER_TYPES } from "eab-web-api";
import { connect } from "react-redux";
import { TEXT_STORE } from "../../../constants/REDUCER_TYPES";
import ProposalHeaderLeft from "../components/ProposalHeaderLeft";

const { QUOTATION } = REDUCER_TYPES;

const mapStateToProps = state => ({
  textStore: state[TEXT_STORE],
  quotation: state[QUOTATION].quotation,
  planDetails: state[QUOTATION].planDetails
});

const mapDispatchToProps = dispatch => ({
  reQuote: ({ quotationId, callback }) => {
    dispatch({
      type: ACTION_TYPES[REDUCER_TYPES.PROPOSAL].SAGA_REQUOTE,
      quotationId,
      callback
    });
  },
  clone: ({ quotationId, cloneAlert, callback }) => {
    dispatch({
      type: ACTION_TYPES[REDUCER_TYPES.PROPOSAL].SAGA_CLONE,
      quotationId,
      callback: allowClone => {
        if (allowClone) {
          callback();
        } else {
          cloneAlert();
        }
      }
    });
  },
  closeSummaryProposal: () => {
    dispatch({
      type: ACTION_TYPES[REDUCER_TYPES.PROPOSAL].CLOSE_SUMMARY_PROPOSAL,
      openSummaryProposal: false
    });
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProposalHeaderLeft);
