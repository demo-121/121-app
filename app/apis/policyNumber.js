import Config from "react-native-config";
import axios from "axios";
// import { encryption, decryption } from "../apis/login";

/**
 ** submitApplication
 * @description call web service to get pool of policy number
 * @param {string} authToken - authToken from AXA api provide
 * @param {string} numOfShieldPolicyNumberRequired - num Of Shield Policy Number Required
 * @param {string} numOfNotShieldPolicyNumberRequired -  num Of Not Shield Policy Number Required
 * */
export const getPolicyNumber = (
  authToken,
  numOfShield,
  numOfNonShield,
  environment
) => {
  let url = "";
  let baseURL = "";
  // TODO: to be removed
  if (Config.ENV_NAME === "PROD") {
    url = Config.GET_POLICY_NUMBER_URL;
    baseURL = Config.WEB_SERVICE_BASIC_URL;
  } else {
    url = Config[`${environment || "SIT3_EAB"}_GET_POLICY_NUMBER_URL`];
    baseURL = Config[`${environment || "SIT3_EAB"}_WEB_SERVICE_BASIC_URL`];
  }

  return new Promise((resolve, reject) => {
    let getPolicyNumberAPIResult = {};
    if (!authToken || authToken.length === 0) {
      getPolicyNumberAPIResult = {
        hasError: true,
        errorMsg: "No valid authToken received while getting new policy number."
      };
      reject(getPolicyNumberAPIResult);
    }
    // TODO: To be removed
    if (
      (authToken && authToken.length > 0 && numOfShield > 0) ||
      (authToken && authToken.length > 0 && numOfNonShield > 0)
    ) {
      const shieldNumebr = numOfShield;
      const nonShieldNumber = numOfNonShield;
      const AuthStr = `Bearer ${authToken}`;
      const options = {
        method: "post",
        headers: { Authorization: AuthStr },
        baseURL,
        url,
        data: {
          numOfShield: shieldNumebr,
          numOfNonShield: nonShieldNumber
        }
      };
      // --> CR 197, disable encryption temporarily and then roll back
      // let aes256Key = "";
      // const encryptResult = encryption(options.data, authToken, environment);
      // encryptResult
      //   .then(callback => {
      //     if (callback.hasError) {
      //       getPolicyNumberAPIResult = {
      //         hasError: false,
      //         errorMsg: "Pool of policy number is sufficient."
      //       };
      //       resolve(getPolicyNumberAPIResult);
      //     } else {
      //       aes256Key = callback.result.aesKey;
      //       options.data.d0 = callback.result.d0;
      //       options.data.dk = callback.result.dk;
      //     }
      //   })
      //   .then(() => {
      //     axios
      //       .request(options)
      //       .then(response => {
      //         const decryptionResult = decryption(response.data, aes256Key);
      //         decryptionResult.then(decryptedResponseBody => {
      //           response.data = JSON.parse(decryptedResponseBody);
      //           getPolicyNumberAPIResult = {
      //             hasError: false,
      //             result: response
      //           };
      //           resolve(getPolicyNumberAPIResult);
      //         });
      //       })
      //       .catch(error => {
      //         getPolicyNumberAPIResult = {
      //           hasError: true,
      //           errorMsg: error.response.status
      //         };
      //         resolve(getPolicyNumberAPIResult);
      //       });
      //   });

      axios
        .request(options)
        .then(response => {
          response.data = response.data;
          getPolicyNumberAPIResult = {
            hasError: false,
            result: response
          };
          resolve(getPolicyNumberAPIResult);
        })
        .catch(error => {
          getPolicyNumberAPIResult = {
            hasError: true,
            errorMsg: error.response.status
          };
          resolve(getPolicyNumberAPIResult);
        });
    } else {
      getPolicyNumberAPIResult = {
        hasError: false,
        errorMsg: "Pool of policy number is sufficient."
      };
      resolve(getPolicyNumberAPIResult);
    }
  }).catch(error => {
    const getPolicyNumberAPIResult = {
      hasError: true,
      errorMsg: error
    };
    return getPolicyNumberAPIResult;
  });
};

export default getPolicyNumber;
