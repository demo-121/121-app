import {
  REDUCER_TYPE_CHECK as WEB_REDUCER_TYPE_CHECK,
  REDUCER_TYPES as WEB_REDUCER_TYPES
} from "eab-web-api";
import * as _ from "lodash";
import PropTypes from "prop-types";
import React, { Component } from "react";
import { ScrollView, Text, View } from "react-native";
import EABButton from "../../../../components/EABButton";
import CheckBox from "../../../../components/EABCheckBox";
import Dialog from "../../../../components/EABDialog";
import { FORM } from "../../../../components/EABDialog/constants";
import REDUCER_TYPES_CHECK from "../../../../constants/REDUCER_TYPE_CHECK";
import { TEXT_STORE } from "../../../../constants/REDUCER_TYPES";
import { ContextConsumer } from "../../../../context";
import EABi18n from "../../../../utilities/EABi18n";
import {
  getDisabledRiderList,
  getInclusiveRiders,
  removeDisabledRiders
} from "../../utilities/riderHandler";
import styles from "./styles";
import Theme from "../../../../theme";

const { QUOTATION } = WEB_REDUCER_TYPES;

/**
 * @description rider selector dialog. HAVE TO BE AWARE, this component will
 *   have different behavior in basic quotation or shield quotation
 * @param subInputConfigs - input config for target quotation
 * @param subQuotation - target quotation that plan to select riders
 * @param {boolean} isOpen - indicate dialog is open or close
 * @param {function} onClose - function that will be fire when closing dialog
 * @param {function} onSave - function that will be fire with params covCodes
 *   when saving the choices.
 * */
export default class RiderDialog extends Component {
  constructor(props) {
    super(props);

    const { setSelectedRiders } = RiderDialog;
    const { subQuotation, planDetails } = this.props;

    this.state = {
      prevSubQuotation: subQuotation, // eslint-disable-line react/no-unused-state
      prevPlanDetails: planDetails, // eslint-disable-line react/no-unused-state
      selectedRiders: setSelectedRiders({ subQuotation, planDetails })
    };
  }
  // ===========================================================================
  // life circles
  // ===========================================================================
  // Refactored: getDerivedStateFromProps has breaking changes in the latest version
  // #https://reactjs.org/blog/2018/06/07/you-probably-dont-need-derived-state.html
  static getDerivedStateFromProps(props, state) {
    const { setSelectedRiders } = RiderDialog;

    if (
      props.subQuotation !== state.prevSubQuotation ||
      props.planDetails !== state.prevPlanDetails
    ) {
      return {
        prevSubQuotation: props.subQuotation,
        prevPlanDetails: props.planDetails,
        selectedRiders: setSelectedRiders({
          subQuotation: props.subQuotation,
          planDetails: props.planDetails
        })
      };
    }

    return null;
  }
  // ===========================================================================
  // getters
  // ===========================================================================
  /**
   * @requires getDisabledRiderList
   * @return {array} list of rider checkboxes
   * */
  get getRiderList() {
    const { getSelectedCovCodes } = RiderDialog;
    const { subQuotation, planDetails, subInputConfigs } = this.props;
    const { selectedRiders } = this.state;
    const { riderList } = subInputConfigs[subQuotation.baseProductCode];
    const bpDetail = planDetails[subQuotation.baseProductCode];
    const disabledRiders = getDisabledRiderList({
      selectedCovCodes: getSelectedCovCodes(selectedRiders),
      quotation: subQuotation,
      bpDetail,
      riderList
    });
    const options = [];

    _.map(riderList, rider => {
      const plan = planDetails[rider.covCode];
      let text = "";

      // TODO change to language type
      if (rider.covName) {
        text = rider.covName.en;
      } else {
        text = plan.covName.en;
      }

      if (
        !_.get(rider, "hiddenInSelectRiderDialog") ||
        (_.get(rider, "hiddenInSelectRiderDialog") &&
          !_.isEqual(rider.hiddenInSelectRiderDialog, "Y"))
      ) {
        const isDisabled =
          rider.compulsory === "Y" ||
          disabledRiders.indexOf(rider.covCode) > -1;
        options.push({
          key: rider.covCode,
          text,
          isSelected: !!selectedRiders[rider.covCode],
          isDisabled,
          rider
        });
      }
    });
    // TODO check box should support isDisabled
    return (
      <ScrollView style={{ marginLeft: 6 }}>
        <CheckBox
          options={options}
          onPress={option => {
            if (!option.isDisabled) {
              const selectedRidersBackUp = _.cloneDeep(selectedRiders);
              const value = !option.isSelected;
              selectedRidersBackUp[option.rider.covCode] = value;
              const inclusiveRiders = getInclusiveRiders({
                covCode: option.rider.covCode,
                quotation: subQuotation,
                bpDetail
              });
              Object.keys(selectedRidersBackUp).forEach(key => {
                if (
                  Object.prototype.hasOwnProperty.call(
                    selectedRidersBackUp,
                    key
                  ) &&
                  inclusiveRiders.indexOf(key) >= 0
                ) {
                  selectedRidersBackUp[key] = value;
                }
              });
              let newSelectedCovCodes = getSelectedCovCodes(
                selectedRidersBackUp
              );
              newSelectedCovCodes = removeDisabledRiders({
                selectedCovCodes: newSelectedCovCodes,
                quotation: subQuotation,
                bpDetail,
                riderList
              });
              const newSelectedRiders = {};
              _.each(newSelectedCovCodes, c => {
                newSelectedRiders[c] = true;
              });
              const newSelectedCovCodesBackup = getSelectedCovCodes(
                newSelectedRiders
              );
              if (
                newSelectedCovCodesBackup.indexOf(option.rider.covCode) !==
                  -1 ||
                (newSelectedCovCodesBackup.indexOf(option.rider.covCode) ===
                  -1 &&
                  !value)
              ) {
                this.setState({ selectedRiders: newSelectedRiders });
              }
            }
          }}
        />
      </ScrollView>
    );
  }
  // ===========================================================================
  // class methods
  // ===========================================================================
  /**
   * @description get selected rider list
   * @return {array} selected rider list
   * */
  static getSelectedCovCodes(selectedRiders) {
    const covCodes = [];

    _.each(selectedRiders, (selected, covCode) => {
      if (selected) {
        covCodes.push(covCode);
      }
    });

    return covCodes;
  }
  /**
   * @description get selected rider list
   * @param {object} subQuotation - quotation data
   * @param {object} planDetails - plan config data
   * @return {object} rider list
   * */
  static setSelectedRiders({ subQuotation, planDetails }) {
    const selectedRiders = {};

    _.each(subQuotation.plans, rider => {
      selectedRiders[rider.covCode] = true;
    });
    _.each(planDetails, plan => {
      const { covCode } = plan;
      if (
        covCode !== subQuotation.baseProductCode &&
        selectedRiders[covCode] === undefined
      ) {
        selectedRiders[covCode] = false;
      }
    });

    return selectedRiders;
  }

  render() {
    // =========================================================================
    // variables
    // =========================================================================
    const { getSelectedCovCodes } = RiderDialog;
    const { getRiderList } = this;
    const { textStore, isOpen, onSave, onClose } = this.props;
    const { selectedRiders } = this.state;

    return (
      <ContextConsumer>
        {({ language }) => (
          <Dialog type={FORM} isOpen={isOpen}>
            <View style={styles.riderDialogToolBar}>
              <View style={styles.toolBarSectionA}>
                <EABButton onPress={onClose}>
                  <Text style={Theme.textButtonLabelNormalAccent}>
                    {EABi18n({
                      language,
                      textStore,
                      path: "button.cancel"
                    })}
                  </Text>
                </EABButton>
              </View>
              <View style={styles.toolBarSectionB}>
                <Text style={Theme.title}>
                  {EABi18n({
                    language,
                    textStore,
                    path: "quotation.button.selectRiders"
                  })}
                </Text>
              </View>
              <View style={styles.toolBarSectionC}>
                <EABButton
                  onPress={() => {
                    onClose();
                    setTimeout(() => {
                      onSave(getSelectedCovCodes(selectedRiders));
                    }, 200);
                  }}
                >
                  <Text style={Theme.textButtonLabelNormalEmphasizedAccent}>
                    {EABi18n({
                      language,
                      textStore,
                      path: "button.save"
                    })}
                  </Text>
                </EABButton>
              </View>
            </View>
            <ScrollView
              style={styles.riderDialogScrollView}
              contentContainerStyle={styles.riderDialogContentContainerStyle}
              showsVerticalScrollIndicator={false}
            >
              {getRiderList}
            </ScrollView>
          </Dialog>
        )}
      </ContextConsumer>
    );
  }
}

RiderDialog.propTypes = {
  // ===========================================================================
  // reducer states
  // ===========================================================================
  planDetails: WEB_REDUCER_TYPE_CHECK[QUOTATION].planDetails.isRequired,
  textStore: REDUCER_TYPES_CHECK[TEXT_STORE].isRequired,
  // ===========================================================================
  // props
  // ===========================================================================
  subInputConfigs: WEB_REDUCER_TYPE_CHECK[QUOTATION].inputConfigs.isRequired,
  subQuotation: WEB_REDUCER_TYPE_CHECK[QUOTATION].quotation.isRequired,
  isOpen: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
  onSave: PropTypes.func.isRequired
};
