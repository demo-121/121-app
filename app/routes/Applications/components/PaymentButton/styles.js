import { StyleSheet } from "react-native";
import Theme from "../../../../theme";

const IPPColumnFirst = {
  paddingVertical: Theme.alignmentXXS,
  width: 70,
  alignItems: "center"
};

export default StyleSheet.create({
  container: {
    marginLeft: Theme.alignmentL
  },
  IPP: {
    marginLeft: Theme.alignmentXS
  },
  IPPWrapper: {
    padding: Theme.alignmentXL
  },
  IPPHint: {
    ...Theme.toolTipLabel,
    marginBottom: Theme.alignmentL
  },
  IPPTable: {
    alignItems: "flex-start"
  },
  IPPRow: {
    flexDirection: "row",
    borderWidth: 1,
    borderColor: Theme.white
  },
  IPPColumnFirst,
  IPPColumn: {
    ...IPPColumnFirst,
    borderLeftWidth: 1,
    borderColor: Theme.white
  },
  webView: {
    flex: 1,
    width: "100%"
  }
});
