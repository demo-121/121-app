import { StyleSheet, Dimensions } from "react-native";

export default StyleSheet.create({
  container: {
    width: "100%",
    height: "100%",
    justifyContent: "center",
    alignItems: "center"
  },
  mainView: {
    flex: 1,
    width: "100%",
    height: "100%"
  },
  pdfContainer: {
    flex: 1,
    justifyContent: "flex-start",
    alignItems: "center"
  },
  pdf: {
    flex: 1,
    width: Dimensions.get("window").width
  }
});
