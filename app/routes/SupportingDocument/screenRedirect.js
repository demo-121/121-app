import React, { PureComponent } from "react";
import { REDUCER_TYPES, REDUCER_TYPE_CHECK } from "eab-web-api";
import SupportingDocumentDetails from "./containers/SupportingDocumentDetails";
import SupportingDocumentShieldDetails from "./containers/SupportingDocumentShieldDetails";

export default class ScreenRedirect extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { isShield } = this.props.screenProps;
    const { navigation } = this.props;
    if (isShield) {
      return <SupportingDocumentShieldDetails navigation={navigation} />;
    }
    return <SupportingDocumentDetails navigation={navigation} />;
  }
}

ScreenRedirect.propTypes = {
  screenProps:
    REDUCER_TYPE_CHECK[REDUCER_TYPES.SUPPORTING_DOCUMENT].screenProps.isRequired
};
