//
//  RNPopoverHostViewManager.m
//  EASE
//
//  Created by Osric Wong on 17/4/2018.
//  Copyright © 2018 Facebook. All rights reserved.
//

#import "RNPopoverHostViewManager.h"

#import "RNPopoverHostView.h"
#import "RNPopoverHostViewController.h"
#import "RNTopViewController.h"

#import <React/RCTBridge.h>
#import <React/RCTUIManager.h>
#import <React/RCTShadowView.h>
#import <React/RCTUtils.h>
#import <React/RCTLog.h>

@interface RNPopoverHostViewManager() <RNPopoverHostViewInteractor>

@property (nonatomic, copy) RCTPromiseResolveBlock dismissResolve;
@property (nonatomic, assign) BOOL userDismiss;
@property (nonatomic, assign) BOOL dismissAnimated;

@end

@implementation RNPopoverHostViewManager  {
    NSHashTable *_hostViews;
}

@synthesize bridge = _bridge;

- (dispatch_queue_t)methodQueue {
  return dispatch_get_main_queue();
}

RCT_EXPORT_MODULE()

RCT_EXPORT_VIEW_PROPERTY(animated, BOOL)
RCT_EXPORT_VIEW_PROPERTY(popoverBackgroundColor, UIColor)
RCT_EXPORT_VIEW_PROPERTY(sourceViewReactTag, NSInteger)
RCT_EXPORT_VIEW_PROPERTY(sourceView, NSNumber)
RCT_EXPORT_VIEW_PROPERTY(sourceViewTag, NSInteger)
RCT_EXPORT_VIEW_PROPERTY(sourceViewGetterTag, NSInteger)
RCT_EXPORT_VIEW_PROPERTY(sourceRect, NSArray)
RCT_EXPORT_VIEW_PROPERTY(permittedArrowDirections, NSArray)
RCT_EXPORT_VIEW_PROPERTY(preferredContentSize, NSArray)
RCT_EXPORT_VIEW_PROPERTY(onShow, RCTDirectEventBlock)
RCT_EXPORT_VIEW_PROPERTY(onHide, RCTDirectEventBlock)

- (UIView *)view {
    RNPopoverHostView *view = [[RNPopoverHostView alloc] initWithBridge:self.bridge];
    view.delegate = self;
    if (!_hostViews) {
        _hostViews = [NSHashTable weakObjectsHashTable];
    }
    [_hostViews addObject:view];
    return view;
}

RCT_REMAP_METHOD(dismiss,
                 dismissWithReactTag:(nonnull NSNumber *)reactTag animated:(BOOL)animated Resolver:(RCTPromiseResolveBlock)resolve rejecter:(RCTPromiseRejectBlock)reject)
{
    __weak typeof(self) weakSelf = self;
    [self.bridge.uiManager addUIBlock:^(RCTUIManager *uiManager, NSDictionary<NSNumber *,UIView *> *viewRegistry) {
        __kindof UIView *view = viewRegistry[reactTag];
        if ([view isKindOfClass:[RNPopoverHostView class]]) {
            RNPopoverHostView *hostView = view;
            if (hostView.presented) {
                weakSelf.userDismiss = YES;
                weakSelf.dismissAnimated = animated;
                weakSelf.dismissResolve = resolve;
                [hostView dismissViewController];
            } else {
                resolve(nil);
            }
        } else {
            reject(@"parameters error", [NSString stringWithFormat:@"invalid parameter: %@", [reactTag stringValue]], nil);
        }
    }];
}

#pragma mark - RCTInvalidating

- (void)invalidate {
    for (RNPopoverHostView *hostView in _hostViews) {
        [hostView invalidate];
    }
    [_hostViews removeAllObjects];
}

#pragma mark - RNPopoverHostViewInteractor

- (void)presentPopoverHostView:(RNPopoverHostView *_Nullable)popoverHostView withViewController:(RNPopoverHostViewController *_Nonnull)viewController animated:(BOOL)animated {
    dispatch_block_t completionBlock = ^{
        if (popoverHostView.onShow) {
            popoverHostView.onShow(nil);
        }
    };
    
    [[RNTopViewController getTopViewController] presentViewController:viewController animated:animated completion:completionBlock];
}

- (void)dismissPopoverHostView:(RNPopoverHostView *_Nullable)popoverHostView withViewController:(RNPopoverHostViewController *_Nullable)viewController animated:(BOOL)animated {
    __weak typeof(self) weakSelf = self;
    [viewController dismissViewControllerAnimated: self.userDismiss ? self.dismissAnimated : animated completion:^{
        if (weakSelf.dismissResolve) {
            weakSelf.dismissResolve(nil);
            weakSelf.dismissResolve = nil;
        }
        if (popoverHostView.onHide) {
            popoverHostView.onHide(nil);
        }
        weakSelf.userDismiss = NO;
    }];
}


@end
