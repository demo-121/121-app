import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import { REDUCER_TYPE_CHECK, REDUCER_TYPES } from "eab-web-api";
import EABButton from "../../../../components/EABButton";
import TranslatedText from "../../../../containers/TranslatedText";

import Theme from "../../../../theme";

export default class ApplicationSelectButton extends PureComponent {
  render() {
    const { navigation, updateUiSelectMode, applicationsList } = this.props;
    const selectModeOn = navigation.getParam("selectModeOn", false);
    const showHeaderRight = navigation.getParam(
      "showHeaderRight",
      applicationsList.length > 0
    );
    if (!showHeaderRight) {
      return null;
    }
    return (
      <EABButton
        style={{
          marginRight: Theme.alignmentXL
        }}
        onPress={() => {
          navigation.setParams({ selectModeOn: !selectModeOn });
          updateUiSelectMode({ selectModeOn: !selectModeOn });
        }}
      >
        <TranslatedText
          style={Theme.textButtonLabelNormalAccent}
          path={selectModeOn ? "button.cancel" : "button.select"}
        />
      </EABButton>
    );
  }
}

ApplicationSelectButton.propTypes = {
  updateUiSelectMode: PropTypes.func.isRequired,
  applicationsList: PropTypes.arrayOf(
    REDUCER_TYPE_CHECK[REDUCER_TYPES.PRE_APPLICATION].ApplicationItem.value
      .isRequired
  ).isRequired
};
