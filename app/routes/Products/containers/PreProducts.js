import { ACTION_TYPES, REDUCER_TYPES } from "eab-web-api";
import { connect } from "react-redux";
import PreProducts from "../components/PreProducts";

const { PRODUCTS } = REDUCER_TYPES;

/**
 * PreProducts
 * @description PreProducts page.
 * @requires PreProducts - PreProducts UI
 * */
const mapStateToProps = () => ({});

const mapDispatchToProps = dispatch => ({
  toProductPage(callback) {
    dispatch({
      type: ACTION_TYPES[PRODUCTS].PRODUCT_TAB_BAR_SAGA,
      callback
    });
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PreProducts);
