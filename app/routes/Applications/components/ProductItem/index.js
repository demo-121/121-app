import React, { PureComponent } from "react";
import {
  View,
  Image,
  Text,
  TouchableWithoutFeedback,
  Alert
} from "react-native";
import * as _ from "lodash";
import PropTypes from "prop-types";
import APP_REDUCER_TYPE_CHECK from "../../../../constants/REDUCER_TYPE_CHECK";
import { TEXT_STORE } from "../../../../constants/REDUCER_TYPES";

import { ContextConsumer } from "../../../../context";
import EABHUD from "../../../../containers/EABHUD";
import confirmationAlert from "../../../../utilities/confirmationAlert";
import EABi18n from "../../../../utilities/EABi18n";
import getEABTextByLangType from "../../../../utilities/getEABTextByLangType";
import {
  getDocumentWithID,
  getAttachmentWithID
} from "../../../../utilities/DataManager";
import icAddMini from "../../../../assets/images/icAddMini.png";
import styles from "./styles";

/**
 * ProductItem
 * @requires icAddMini - @/assets/images/icAddMini.png
 * @description item in ProductList inside Application Summary page.
 * @param {string} attachmentId - attachment Id for image returned from api
 * @param {string} productId - id returned from api
 * @param {object|string} productName - name returned from api
 * */
class ProductItem extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
      imageSource: ""
    };
  }

  async componentDidMount() {
    const { productId, attachmentId } = this.props;

    getDocumentWithID(productId, product => {
      const contentType = _.get(
        product,
        `_attachments.${attachmentId}.content_type`
      );

      if (!_.isEmpty(contentType)) {
        const imageType = `data:${contentType}`;

        getAttachmentWithID(productId, attachmentId, attachment => {
          this.setState({
            imageSource: `${imageType};base64,${attachment.data}`
          });
        });
      }
    });
  }
  /**
   * @description function for query quotation with confirmation
   * @param {string} productId - product id
   * @param {function} showQuotationNavigator - open quotation dialog function
   * @param {string} language
   * */
  queryQuotationWithConfirm({ productId, language, showQuotationNavigator }) {
    const { textStore, getQuotation } = this.props;
    const { queryQuotationWithConfirm } = this;
    this.setState(
      {
        isLoading: true
      },
      () => {
        getQuotation({
          productID: productId,
          alert: message => {
            this.setState(
              {
                isLoading: false
              },
              () => {
                setTimeout(() => {
                  this.getWarningAlert({ message, language });
                }, 200);
              }
            );
          },
          confirmRequest: messagePath => {
            this.setState(
              {
                isLoading: false
              },
              () => {
                setTimeout(() => {
                  Alert.alert(
                    EABi18n({
                      textStore,
                      language,
                      path: "alert.confirm"
                    }),
                    EABi18n({
                      textStore,
                      language,
                      path: messagePath
                    }),
                    [
                      {
                        text: EABi18n({
                          textStore,
                          language,
                          path: "button.no"
                        }),
                        onPress: () => {}
                      },
                      {
                        text: EABi18n({
                          textStore,
                          language,
                          path: "button.yes"
                        }),
                        onPress: queryQuotationWithConfirm({
                          confirm: true,
                          productId,
                          language
                        })
                      }
                    ]
                  );
                }, 200);
              }
            );
          },
          openQuotationFunction: () => {
            this.setState(
              {
                isLoading: false
              },
              showQuotationNavigator
            );
          },
          confirm: true
        });
      }
    );
  }
  /**
   * @description function for query quotation without confirmation
   * @param {string} productId - product id
   * @param {function} showQuotationNavigator - open quotation dialog function
   * @param {string} language
   * */
  queryQuotationWithoutConfirm({
    productId,
    language,
    showQuotationNavigator
  }) {
    const { textStore, getQuotation } = this.props;
    this.setState(
      {
        isLoading: true
      },
      () => {
        getQuotation({
          productID: productId,
          alert: message => {
            this.setState(
              {
                isLoading: false
              },
              () => {
                setTimeout(() => {
                  this.getWarningAlert({ message, language });
                }, 200);
              }
            );
          },
          confirmRequest: messagePath => {
            this.setState(
              {
                isLoading: false
              },
              () => {
                setTimeout(() => {
                  confirmationAlert({
                    titlePath: "alert.confirm",
                    messagePath,
                    language,
                    textStore,
                    yesOnPress: () => {
                      this.setState(
                        {
                          isLoading: true
                        },
                        () => {
                          this.queryQuotationWithConfirm({
                            productId,
                            language
                          });
                        }
                      );
                    }
                  });
                }, 200);
              }
            );
          },
          openQuotationFunction: () => {
            this.setState(
              {
                isLoading: false
              },
              showQuotationNavigator
            );
          },
          confirm: false
        });
      }
    );
  }

  render() {
    const { productName, productId } = this.props;
    const { isLoading, imageSource } = this.state;
    return (
      <ContextConsumer>
        {({ language, showQuotationNavigator }) => (
          <View style={styles.item}>
            <TouchableWithoutFeedback
              onPress={() => {
                this.queryQuotationWithoutConfirm({
                  productId,
                  language,
                  showQuotationNavigator
                });
              }}
            >
              <View style={styles.productImageView}>
                <Image
                  style={styles.productImage}
                  source={!_.isEmpty(imageSource) ? { uri: imageSource } : {}}
                />
                <View style={styles.productAddIconView}>
                  <Image style={styles.productAddIcon} source={icAddMini} />
                </View>
              </View>
            </TouchableWithoutFeedback>
            <Text style={styles.productText}>
              {getEABTextByLangType({
                data: productName,
                language
              })}
            </Text>
            <EABHUD isOpen={isLoading} />
          </View>
        )}
      </ContextConsumer>
    );
  }
}

ProductItem.propTypes = {
  productId: PropTypes.string.isRequired,
  attachmentId: PropTypes.string.isRequired,
  productName: PropTypes.oneOfType([PropTypes.object, PropTypes.string])
    .isRequired,
  getQuotation: PropTypes.func.isRequired,
  textStore: APP_REDUCER_TYPE_CHECK[TEXT_STORE].isRequired
};

export default ProductItem;
